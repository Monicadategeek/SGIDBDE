<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/Presupuesto.php"); 
	require_once("../Modelo/Becarios.php");

	require_once("../Modelo/Inscripcionesbecarios.php"); 
	require_once("../Modelo/SinceracionBecas.php");  
	require_once("../Modelo/Terceros/estudiantes.php");




	class InscripcionesController{
	


		public function index(){
			$consulta= new SinceracionBecas();
			$becasdisp=$consulta->get_SinceracionBecas_Inscripciones();		
			if (count($becasdisp)>0) {
				$consulta= new Presupuesto();
				$presupuestomax=$consulta->get_Presupuesto_Inscripciones();
				$consulta= new Becarios();
				$becarios=$consulta->get_Becarios_Inscripciones($presupuestomax);
				require_once("../views/inscripciones/index.php");
			}
			echo "Disculpe, no hay un presupuesto registrado o activo";
			
		}

		public function create(){ //nuevo
			$consulta= new SinceracionBecas();
			$becasdisp=$consulta->get_SinceracionBecas_Inscripciones();	

			$becario=$_GET["becario"];
			$consulta= new Becarios();
			$becario=$consulta->get_Becarios_show($becario, $_SESSION['ID']);

			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($becario["Estudiante"], $_SESSION['ID']);
			$consulta= new Becarios();
			$historial=$consulta->get_Becarios_activos_show($becario["IdBecarios"])[0];
			require_once("../views/inscripciones/new.php");			
		}

		public function store(){//registrar

			$inscripcion["Becario"]=$_POST["becario"];
			$inscripcion["BecaAsociada"]=$_POST["BecaAsociada"];
			$inscripcion["Responsable"]=$_SESSION['ID'];
			$inscripcion["Tipo_Inscripcion"]=2;
			$consulta= new Inscripcionesbecarios();
			return $consulta->set_Inscripciones_store($inscripcion);


		}

		public function show($id){
			

		}

		public function edit($id){//modificar		
			
		}

		public function update(){//editar
			
			
		}

		public function destroy($id){
			
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new InscripcionesController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new InscripcionesController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new InscripcionesController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new InscripcionesController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new InscripcionesController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new InscripcionesController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new InscripcionesController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>