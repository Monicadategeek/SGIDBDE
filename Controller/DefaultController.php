<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/FechasEncuestas.php");	
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EstudiantesEncuestados.php");


	class DefaultController{
	
		

		public function index(){
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				$consulta= new Usuarios();
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
				$consulta= new FechasEncuestas();
				$ultfecha=$consulta->get_FechasEncuestas_Ult();
				if (date('Y-m-d')>=$ultfecha["Fechainicio"] and  date('Y-m-d')<=$ultfecha["Fechafin"]) {
					$ultfecha["estudiante"]=$persona["num_est"];		
					$consulta= new EstudiantesEncuestados();
					$encuesta=$consulta->get_FechasEncuestas($ultfecha);

					if (is_array($encuesta) and count($encuesta)>0) {
						if ($encuesta["socioeconomicos"]=='' or $encuesta["ambientales"]=='' or $encuesta["culturales"]=='' or $encuesta["interesAyu"]=='') {
							$encuesta=0;
						}
						else{
							$encuesta=1;
						}
					}
					else{
						$encuesta=0;
					}

				}

			}

			require_once("../views/contenido.php");


		}

		public function create(){ //nuevo			
			require_once("../views/fechasencuestas/new.php");
		}

		public function store(){//registrar
			
			$feechasencuestas["Fechainicio"]=$_POST["Fechainicio"];
			$feechasencuestas["Fechafin"]=$_POST["Fechafin"];
			$feechasencuestas["Responsable"]=$_SESSION['ID'];

			$consulta= new FechasEncuestas();
			return $consulta->set_FechasEncuestas_store($feechasencuestas);

		}

		public function show($id){

		}

		public function edit($id){//modificar

			$consulta= new FechasEncuestas();
			$feechasencuesta=$consulta->get_FechasEncuestas_edit($id);

			require_once("../views/fechasencuestas/edit.php");

		}

		public function update(){//editar
			$feechasencuestas["Fechainicio"]=date('Y-m-d', strtotime($_POST["Fechainicio"]));
			$feechasencuestas["Fechafin"]=date('Y-m-d', strtotime($_POST["Fechafin"]));
			$feechasencuestas["Id"]=$_POST["Id"];
			$feechasencuestas["Responsable"]=$_SESSION['ID'];
			$consulta= new FechasEncuestas();
			$feechasencuesta=$consulta->get_FechasEncuestas_edit($_POST["Id"]);

			if (date('Y-m-d', strtotime($feechasencuesta["Fechainicio"]))<=date('Y-m-d')) {
				$feechasencuestas["Fechainicio"]=date('Y-m-d', strtotime($feechasencuesta["Fechainicio"]));
			}
			$consulta= new FechasEncuestas();
			return $consulta->set_FechasEncuestas_update($feechasencuestas);
		}

	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new DefaultController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new DefaultController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new DefaultController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new DefaultController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new DefaultController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new DefaultController;			
		$rs = $conectar->update();
		echo $rs;
	}

	


?>