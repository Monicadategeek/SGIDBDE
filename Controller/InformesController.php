<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/FechasEncuestas.php");	
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EstudiantesEncuestados.php");
	require_once("../Modelo/Citas.php");
	require_once("../Modelo/SinceracionBecas.php");	
	require_once("../Modelo/EstudioSocioeconomico.php");
	require_once("../Modelo/Becarios.php");
	require_once("../Modelo/Inscripcionesbecarios.php");
	require_once("../Modelo/GestionCitas.php");

	require_once("../Modelo/Informes.php");	
	require_once("../Modelo/encuest_odont_pregu.php");	
	require_once("../Modelo/encuesta_odontologica.php");
	require_once("../Modelo/informe_odontologico.php");	
	require_once("../Modelo/Informesmedicos.php");


	require_once("../Modelo/Referenciales/enfermedades.php");
	require_once("../Modelo/Referenciales/tratamientos.php");

	require_once("../Modelo/Terceros/grupo_familiar_est.php");
	require_once("../Modelo/Terceros/estudiantes.php");
	require_once("../Modelo/Terceros/dat_acad_est.php");
	require_once("../Modelo/Terceros/dat_socio_est.php");
	require_once("../Modelo/Terceros/dat_inter_est.php");
	require_once("../Modelo/Terceros/ayuda_econ_x_estudiante.php");
	require_once("../Modelo/Terceros/gastos_x_estudiante.php");
	require_once("../Modelo/Terceros/dat_fisic_ambient_est.php");
	require_once("../Modelo/Terceros/actividades_t_l_x_estudiante.php");
	require_once("../Modelo/Terceros/deportes_x_estudiante.php");
	require_once("../Modelo/Terceros/habilidades_x_estudiante.php");
	require_once("../Modelo/Terceros/eventos_x_estudiantes.php");


	require_once("../Modelo/Referenciales/egreso.php");


	class InformesController{
	

		public function show($idCita){
			$consulta=new GestionCitas();
			$cita=$consulta->get_Citas_show($idCita);
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($cita["IdEstudiante"], $_SESSION['ID']);

			if ($cita["numTipoDepto"]==2) { //Odontologia
				$cita["tipoInforme"]='Informe Odontológico';
				$consulta= new EncuestaOdontologica();
				$EncOdont=$consulta->set_EncuestaOdontologica_show($cita["IdCitas"], $_SESSION['ID']);

				$consulta= new EncuestOdontPregu();
				$PreguntasOdontologicas=$consulta->set_EncuestOdontPregu_show($cita["IdCitas"]);
				$mitad=round(count($PreguntasOdontologicas)/2);

				$consulta= new Informes();
				$informe=$consulta->get_Informes_show($cita["IdCitas"]);

				if (count($informe)>0) {
					$cita["Responsable"]=$informe[0]["Responsable"];
				}
				else{
					$cita["Responsable"]='';
				}

				$consulta= new InformeOdontologico();
				$informe=$consulta->get_InformeOdontologico_show($cita["IdCitas"]);

				require_once("../views/informes/odontologia.php");
			}
			else if ($cita["numTipoDepto"]==1){//Psicologica
				$cita["tipoInforme"]='Informe Psicologico';
				$consulta= new Informes();
				$informe=$consulta->get_Informes_show($cita["IdCitas"]);
				if (count($informe)>0) {
					$cita["Responsable"]=$informe[0]["Responsable"];
					$cita["Descripcion"]=$informe[0]["Descripcion"];

				}

				require_once("../views/informes/psicologia.php");
			}

			else if ($cita["numTipoDepto"]==3){//Medicina				
				$consulta= new estudiantes();
				$estudiante=$consulta->get_Estudiantes_Show($cita["IdEstudiante"], $_SESSION['ID']);
				$consulta= new InformesMedicos();
				$informe=$consulta->get_InformesMedicos_show($idCita, $_SESSION['ID']);
				require_once("../views/informes/medicinageneral.php");
			
			}

			else if ($cita["numTipoDepto"]==5){//Estudios Socioeconomicos
				$consulta=new EstudiantesEncuestados();
				$encuesta=$consulta->get_EstudiantesEncuestados($idCita);	

				$consulta=new EstudioSocioeconomico();
				$estudio=$consulta->set_EstudioSocioeconomico_show($idCita, $_SESSION['ID']);

							
				$cita["tipoInforme"]='Informe Estudios Socioeconomicos';
				$consulta= new DatosAcadEst();
				$datosacademicos=$consulta->get_datos_Academicos($cita["IdEstudiante"])[0];

				#Datos socioeconomicos
				$consulta= new DatosSocEcon();
				$datossocioeco=$consulta->get_DatosSocEco_Est($encuesta["socioeconomicos"]);
				$consulta= new Egreso();
				$egresos=$consulta->get_egreso();
				$consulta= new DatosSocEcon();
				$datossocioeco=$consulta->get_DatosSocEco_Est($encuesta["socioeconomicos"]);

				$consulta= new AyudaEconxEstudiante();
				$ayudaseconomica=$consulta->get_AyudaEconxEstudiante($encuesta["Id"]);
				if (count($ayudaseconomica)>0) {
					$datossocioeco["ayudaseconomica"]="SI";
				}
				else{
					$datossocioeco["ayudaseconomica"]="NO";
				}
				$consulta= new GrupoFamiliarEst();
				$familiares=$consulta->get_GrupoFamiliarEst_index($encuesta["estudiante"]);
				$consulta= new GastosxEstudiante();
				$gastosindividuales=$consulta->get_GrupoFamiliarEst_gastosindividuales($encuesta["Id"]);
				$consulta= new GastosxEstudiante();
				$gastosfamiliares=$consulta->get_GrupoFamiliarEst_gastosfamiliares($encuesta["Id"]);

				#Datos Ambientales
				$consulta= new DatosFisAmbEst();
				$datosambientales=$consulta->get_DatosFisAmbEst_show($encuesta["ambientales"]);


				#Datos Culturales
				$consulta= new ActivxEst();
				$actividadesest=$consulta->get_ActivxEst_show($encuesta["Id"]);

				$consulta= new DeportesxEstudiante();
				$deportesest=$consulta->get_deportesxest_show($encuesta["Id"]);

				$consulta= new HabilidadesxEstudi();
				$habilidadesest=$consulta->get_HabilidadesxEstudi_show($encuesta["Id"]);

				$consulta= new EventosxEstud();
				$eventosest=$consulta->get_EventosxEstud_show($encuesta["Id"]);


				#Datos Interez estudiantil
				$consulta= new DatInterEst();
				$datosinteressocioeconomico=$consulta->get_DatInterEst_show($encuesta["interesAyu"]);
				require_once("../views/informes/estudiossocioeconomicos.php");
			
			}

		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	
	if ($accion == 'show')
	{
	 	$conectar = new InformesController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	

	


?>