<?php 
	session_start();

	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/FechasEncuestas.php");	
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EstudiantesEncuestados.php");
	require_once("../Modelo/Citas.php");
	require_once("../Modelo/Informes.php");


	require_once("../Modelo/Terceros/dat_acad_est.php");
	require_once("../Modelo/Terceros/dat_socio_est.php");
	require_once("../Modelo/Terceros/dat_inter_est.php");
	require_once("../Modelo/Terceros/ayuda_econ_x_estudiante.php");
	require_once("../Modelo/Terceros/gastos_x_estudiante.php");
	require_once("../Modelo/Terceros/dat_fisic_ambient_est.php");
	require_once("../Modelo/Terceros/actividades_t_l_x_estudiante.php");
	require_once("../Modelo/Terceros/deportes_x_estudiante.php");
	require_once("../Modelo/Terceros/habilidades_x_estudiante.php");
	require_once("../Modelo/Terceros/eventos_x_estudiantes.php");


	require_once("../Modelo/Referenciales/TipoBeca.php");

	require_once("../Modelo/Referenciales/menciones_bach.php");
	require_once("../Modelo/Referenciales/egreso.php");
	require_once("../Modelo/Referenciales/ayuda_economica.php");
	require_once("../Modelo/Referenciales/estados.php");
	require_once("../Modelo/Referenciales/parroquias.php");
	require_once("../Modelo/Referenciales/municipios.php");
	require_once("../Modelo/Referenciales/ciudades.php");
	require_once("../Modelo/Referenciales/tipo_vivienda.php");
	require_once("../Modelo/Referenciales/tipos_pared.php");
	require_once("../Modelo/Referenciales/tipos_piso.php");
	require_once("../Modelo/Referenciales/tipos_techo.php");	
	require_once("../Modelo/Referenciales/tenecia_vivienda.php");
	require_once("../Modelo/Referenciales/act_tiempo_libre.php");
	require_once("../Modelo/Referenciales/categorias_deportes.php");
	require_once("../Modelo/Referenciales/habilidades_est.php");
	require_once("../Modelo/Referenciales/eventos_iutoms.php");

	


	class EncuestaController{
	


		public function index(){
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				$consulta= new Usuarios();
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
				require_once("../views/estudiantes/encuesta.php");

			}

		}

		public function encuestar(){
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$consulta= new DatosAcadEst();
			if ($consulta->get_datos_Academicos_Encuesta($persona["num_est"])==0) {
				$consulta= new MencionesBach();
				$MencionesBachs=$consulta->get_MencionesBach();				
				require_once("../views/estudiantes/encuesta/DatosAcademicos.php");

			}
			else{				
				$consulta= new FechasEncuestas();
				$ultfecha=$consulta->get_FechasEncuestas_Ult();
				$consulta= new EstudiantesEncuestados();
				$ultfecha["estudiante"]=$persona["num_est"];
				$encuesta=$consulta->get_FechasEncuestas($ultfecha);
				$consulta=new Citas();
				$cita=$consulta->get_EstudianteEncuestas_Citas($persona["num_est"]);
				
				
				
				if (is_array($encuesta) and count($encuesta)>0) {
					if ($encuesta["socioeconomicos"]=='' ) {
						$this->DatosSocioeconomicos ();						
					}
					else if ($encuesta["miembrosfamiliares"]=='' ) {
						require_once("MiembrosFamiliaresController.php");
						$consulta= new MiembrosFamiliaresController();
						return $consulta->index();			
					}
					else if ($encuesta["ambientales"]=='') {
						$this->DatosAmbientales();
					}
					else if ($encuesta["culturales"]=='') {
						$this->DatosCulturales();
					}
					else if ($encuesta["interesAyu"]=='') {
						$this->DatosInteres();
					}
					else{
						require_once("../views/estudiantes/encuesta/Completado.php");
					}
				}
				else if ($cita==0) {
					$this->DatosSocioeconomicos();
				}
				else{
					$this->DatosSocioeconomicos();
				}

			}
			
		}


		public function CitasEncuesta_DatosSocioeconomicos(){ //Es l encargada de crear la encuesta a traves de la cita 
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);

			$consulta= new AyudaEconomica();
			$ayudaseconomica=$consulta->get_ayudaseconomica();
			$consulta= new Egreso();
			$egresos=$consulta->get_egreso();

			require_once("../views/estudiantes/encuesta/DatosSocioeconomicos.php");
		}


		public function DatosSocioeconomicos(){ //Formulario de Datos Socioeconomicos

			$consulta= new AyudaEconomica();
			$ayudaseconomica=$consulta->get_ayudaseconomica();
			$consulta= new Egreso();
			$egresos=$consulta->get_egreso();
			require_once("../views/estudiantes/encuesta/DatosSocioeconomicos.php");

		}

		public function DatosInteres(){ //Formulario de Datos Interes
			$consulta= new TipoBeca();
			$becas=$consulta->get_TipoBeca();
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$consulta= new TipoBeca();
			$becas=$consulta->get_TipoBeca();

			require_once("../views/estudiantes/encuesta/DatosInteres.php");

		}

		
		public function DatosAmbientales(){ //Formulario de Datos Ambientales

			$consulta= new Estados();
			$estados=$consulta->get_estados();

			$consulta= new TipoVivienda();
			$tpviviendas=$consulta->get_TipoVivienda();

			$consulta= new TeneciaVivienda();
			$tenecviv=$consulta->get_TeneciaVivienda();

			$consulta= new TiposPared();
			$tpparedes=$consulta->get_TiposPared();

			$consulta= new TiposPiso();
			$tppisos=$consulta->get_TiposPiso();

			$consulta= new TiposTecho();
			$tptechos=$consulta->get_TiposTecho();
			
			require_once("../views/estudiantes/encuesta/DatosAmbientales.php");

		}


		public function DatosCulturales(){ //Formulario de Datos Culturales

			$consulta= new ActTiempoLibre();
			$actividades=$consulta->get_ActTiempoLibre();

			$consulta= new CategDeportes();
			$deportes=$consulta->get_CategDeportes();


			$consulta= new HabilidadesEst();
			$habilidades=$consulta->get_HabilidadesEst();

			$consulta= new EventosIUTOMS();
			$eventos=$consulta->get_EventosIUTOMS();
			
			require_once("../views/estudiantes/encuesta/DatosCulturales.php");

		}

		public function MiembrosFamiliares(){ //Formulario de Miembros Familiares 
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$consulta= new FechasEncuestas();
			$ultfecha=$consulta->get_FechasEncuestas_Ult();
			$consulta= new EstudiantesEncuestados();
			$ultfecha["estudiante"]=$persona["num_est"];
			$encuesta=$consulta->get_FechasEncuestas($ultfecha);
			$encuesta["campo"]="miembrosfamiliares";
			$encuesta["miembrosfamiliares"]=1;			
			$consulta= new EstudiantesEncuestados();
			$encuesta=$consulta->set_FechasEncuestas_update_column($encuesta);
			if (is_numeric($encuesta)) {
				$this->encuestar();
			}
			else{
				return $encuesta;
			}

		}

		

		


		public function registrar($form){ //nuevo
			
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);

			$consulta= new FechasEncuestas();
			$ultfecha=$consulta->get_FechasEncuestas_Ult();
			$consulta= new EstudiantesEncuestados();
			$ultfecha["estudiante"]=$persona["num_est"];
			$encuesta=$consulta->get_FechasEncuestas($ultfecha);

			//Registro de los datos por formulario

			if ($form=='datos-academicos') {
				
				$datosacadests["id_tit_bach"]=$_POST["id_tit_bach"];
				if (isset($_POST["id_tit_tsu"])) {
					$datosacadests["id_tit_tsu"]=$_POST["id_tit_tsu"];
				}
				else{
					$datosacadests["id_tit_tsu"]=10;
				}
				$datosacadests["tipo_inst"]=$_POST["tipo_inst"];
				$datosacadests["prom_notas_bach"]=$_POST["prom_notas_bach"];
				$datosacadests["motivo_pnf"]=$_POST["motivo_pnf"];
				$datosacadests["identidad_pnf"]=$_POST["identidad_pnf"];
				$datosacadests["te_gusta"]=$_POST["te_gusta"];
				$datosacadests["num_est"]=$persona["num_est"];
				$consulta= new DatosAcadEst();
				$resul=$consulta->set_DatosAcadEst($datosacadests);
				if ($resul==1) {
					$this->encuestar();
				}
				else{
					echo "Ha ocurrido un error, Continue con la encuesta mas tarde";
				} 

			}
			else if ($form=='datos-interes') {
				
				if ($_POST["ayudaeco"]==1) {
					$datosinteres["consideracion"]=$_POST["consideracion"];
					$datosinteres["id_ayud"]=$_POST["id_ayud"];
				}				
				$datosinteres["comedor"]=$_POST["comedor"];
				$datosinteres["num_est"]=$persona["num_est"];
				$consulta= new DatInterEst();
				$encuesta["interesAyu"]=$consulta->set_DatInterEst($datosinteres);
				$encuesta["campo"]="interesAyu";

				$consulta=new Citas();
				$encuesta["cita"]=$consulta->get_EstudianteEncuestas_Citas_id($datosinteres["num_est"]);

				$consulta= new EstudiantesEncuestados();
				$encuesta=$consulta->set_FechasEncuestas_update_column($encuesta);
				if (is_numeric($encuesta)) {
					$this->encuestar();
				}
				else{
					return $encuesta;
				}
				
			}
			else if ($form=='datos-culturales') {
				
				
				$encuesta["campo"]="culturales";
				$encuesta["culturales"]=1;
				$datosculturales["actividades"]=$_POST["actividades"];
				$datosculturales["idencuesta"]=$encuesta["Id"];			
				$datosculturales["num_est"]=$persona["num_est"];
				if (count($datosculturales["actividades"])>0) {
					$consulta= new ActivxEst();
					$consulta->set_ActivxEst($datosculturales);
				}

				if ($_POST["pracdeporte"]==1) {
					$datosculturales["deportes"]=$_POST["deportes"];
					$consulta= new DeportesxEstudiante();
					$consulta->set_DeportesxEstudiante($datosculturales);
				}
				
				if ($_POST["posehaar"]==1) {
					$datosculturales["habilidades"]=$_POST["habilidades"];
					$consulta= new HabilidadesxEstudi();
					$consulta->set_HabilidadesxEstudi($datosculturales);
				}
				
				if ($_POST["actIUTOMS"]==1) {
					$datosculturales["eventos"]=$_POST["eventos"];
					$consulta= new EventosxEstud();
					$consulta->set_EventosxEstud($datosculturales);
				}
				$consulta= new EstudiantesEncuestados();
				$encuesta=$consulta->set_FechasEncuestas_update_column($encuesta);
				if (is_numeric($encuesta)) {
					$this->encuestar();
				}
				else{
					return $encuesta;
				}
				
			}
			else if ($form=='datos-ambientales'){
				$datossambient["id_estado"]=$_POST["id_estado"];
				$datossambient["id_parroquia"]=$_POST["id_parroquia"];
				$datossambient["id_ciudad"]=$_POST["id_ciudad"];
				$datossambient["id_tip_viv"]=$_POST["id_tip_viv"];
				$datossambient["id_ten_viv"]=$_POST["id_ten_viv"];
				$datossambient["constr_viv"]=$_POST["constr_viv"];
				$datossambient["id_tipo_pared"]=$_POST["id_tipo_pared"];
				$datossambient["id_tipo_techo"]=$_POST["id_tipo_techo"];
				$datossambient["id_tipo_piso"]=$_POST["id_tipo_piso"];
				$datossambient["sala"]=$_POST["sala"];
				$datossambient["cocina"]=$_POST["cocina"];
				$datossambient["banios"]=$_POST["banios"];
				$datossambient["dormitorios"]=$_POST["dormitorios"];
				$datossambient["luz"]=$_POST["luz"];
				$datossambient["agua"]=$_POST["agua"];
				$datossambient["aseo_urb"]=$_POST["aseo_urb"];
				$datossambient["telf_local"]=$_POST["telf_local"];
				$datossambient["n_habitaciones"]=$_POST["dormitorios"];
				$datossambient["num_est"]=$persona["num_est"];

				$encuesta["campo"]="ambientales";
				$consulta= new DatosFisAmbEst();
				$encuesta["ambientales"]=$consulta->set_DatosFisAmbEst($datossambient);
				if (is_numeric($encuesta["ambientales"])) {
					$consulta= new EstudiantesEncuestados();
					$encuesta=$consulta->set_FechasEncuestas_update_column($encuesta);
					if (is_numeric($encuesta)) {
						$this->encuestar();
					}
					else{
						return $encuesta;
					}

				}

			}
			else if ($form=='datos-socioeconomicos') {

				$datossocioeco["problemas_traslado"]=$_POST["problemas_traslado"];
				$datossocioeco["tipo_traslado_iutoms"]=$_POST["tipo_traslado_iutoms"];
				$datossocioeco["Trabaja"]=$_POST["Trabaja"];
				$datossocioeco["HaTrabajado"]=$_POST["Trabajo"];
				if ($datossocioeco["HaTrabajado"]==1 || $datossocioeco["Trabaja"]==1) {
					$datossocioeco["cargo_trabajo"]=$_POST["NomOrga"];
					$datossocioeco["nombre_trabajo"]=$_POST["Cargo"];
					$datossocioeco["sueldo_mensual"]=$_POST["Sueldo"];
					$datossocioeco["horario_trabajo"]=$_POST["inic"]." ".$_POST["inicg"]." ".$_POST["fin"]." ".$_POST["fing"];	
					$datossocioeco["donde_trabaja"]=$_POST["donde_trabaja"];
					if ($datossocioeco["HaTrabajado"]==1) {
						$datossocioeco["RazRetTrab"]=$_POST["RazRetTrab"];
					}
					
				}
				$datossocioeco["nro_comidas"]=$_POST["tres_comidas"];
				$datossocioeco["tres_comidas"]=$_POST["tres_comidas"];
				if ($datossocioeco["tres_comidas"]==1) {
					$datossocioeco["tres_comidas"]='una comida, '.$_POST["exptres_comidas"];
				}
				if ($datossocioeco["tres_comidas"]==2) {
					$datossocioeco["tres_comidas"]='dos comidas, '.$_POST["exptres_comidas"];
				}
				if ($datossocioeco["tres_comidas"]==3) {
					$datossocioeco["tres_comidas"]='tres comidas';
				}
				if ($datossocioeco["tres_comidas"]>3) {
					$datossocioeco["tres_comidas"]='mas de tres comidas';
				}
				$datossocioeco["hijos"]=$_POST["tiehijos"];
				if ($datossocioeco["hijos"]>0) {
					$datossocioeco["hijos"]=$_POST["nrohijos"]; 
					$datossocioeco["viven_contigo"]=$_POST["viven_contigo"]; 
					if ($datossocioeco["viven_contigo"]==0) {
						$datossocioeco["viven_contigo"]='No, '.$_POST["Razviven_contigo"];
					}
					else{
						$datossocioeco["viven_contigo"]='Si';
					}

				}
				$datossocioeco["pareja"]=$_POST["pareja"];
				$datossocioeco["pers_dep"]=$_POST["pers_dep"];
				if ($datossocioeco["pers_dep"]>0) {					
					$datossocioeco["pers_dep"]=$_POST["nropers_dep"];
				}
			
				$datossocioeco["TotIngEst"]=$_POST["TotIngEst"];
				$datossocioeco["TotIngFam"]=$_POST["TotIngFam"];
				$datossocioeco["num_est"]=$persona["num_est"];

				$consulta= new DatosSocEcon();
				$ayudasecoest["id_ayud_econ"]=$consulta->set_DatosSocEco($datossocioeco);

				
				
				if (is_numeric($ayudasecoest["id_ayud_econ"])) {
					$encuestas["socioeconomicos"]=$ayudasecoest["id_ayud_econ"];
					$consulta= new EstudiantesEncuestados();
					$encuestas["estudiante"]=$persona["num_est"];
					$ayudasecoest["idencuesta"]=$consulta->set_FechasEncuestas($encuestas);
					if (is_numeric($ayudasecoest["idencuesta"])) {
						$resul=$ayudasecoest["idencuesta"];
						$ayudasecoest["num_est"]=$persona["num_est"];
						
						if ($_POST["AyuEcon"]==1) {
							

							$consulta= new AyudaEconomica();
							$ayudaseconomica=$consulta->get_ayudaseconomica();
							$arrayayudaeco=array();

							foreach ($ayudaseconomica as $ayudaeconomica) {
								if ($_POST["ayud_econ".$ayudaeconomica["id_ayud_econ"]]!=0) {

									$respuesta["id_ayud_econ"]= $ayudaeconomica["id_ayud_econ"];
									$respuesta["monto"]= $_POST["ayud_econ".$ayudaeconomica["id_ayud_econ"]];
									$respuesta["num_est"]= $persona["num_est"];
									$respuesta["idencuesta"]= $resul;
									$arrayayudaeco[]=$respuesta;

									
								}					
							}

							$consulta= new AyudaEconxEstudiante();
							$resul=$consulta->set_AyudaEconxEstudiante($arrayayudaeco);

						}
						if (is_numeric($resul)) {
							$consulta= new Egreso();
							$egresos=$consulta->get_egreso();

							foreach ($egresos as $egreso) {
								if (!isset($_POST["egrind".$egreso["IdEgreso"]])) {
									break;
								}	
								$egresosestudi["egrind".$egreso["IdEgreso"]]=$_POST["egrind".$egreso["IdEgreso"]];	
							}

							foreach ($egresos as $egreso) {
								if (!isset($_POST["egrfam".$egreso["IdEgreso"]])) {
									break;
								}	
								$egresosestudi["egrfam".$egreso["IdEgreso"]]=$_POST["egrfam".$egreso["IdEgreso"]];	
							}
							if (count($egresosestudi)>0) {
								$consulta= new Egreso();
								$egresos=$consulta->get_egreso();

								
								foreach ($egresos as $egreso) {
									if (!isset($_POST["egrind".$egreso["IdEgreso"]])) {
										break;
									}	

									$egresosestudi["egrind".$egreso["IdEgreso"]]=$_POST["egrind".$egreso["IdEgreso"]];	
								}

								foreach ($egresos as $egreso) {
									if (!isset($_POST["egrfam".$egreso["IdEgreso"]])) {
										break;
									}	
									$egresosestudi["egrfam".$egreso["IdEgreso"]]=$_POST["egrfam".$egreso["IdEgreso"]];	
								}
								if (count($egresosestudi)>0) {
									$egresosestudi["num_est"]=$persona["num_est"];
									$egresosestudi["idencuesta"]=$ayudasecoest["idencuesta"];


									$consulta= new GastosxEstudiante();
									$resul= $consulta->set_GastosxEstudiante($egresosestudi, $egresos);
									if (is_numeric($resul)) {
										$this->encuestar();
									}
								}
							}

						}

					}

					return $ayudasecoest["idencuesta"];
				}
				return $ayudasecoest["id_ayud_econ"];
			
			}

		}

		//Funcion para modificar los datos del estudiante encuestado. se manda desde controlador AtencionCitasController.php
		public function AtencionCitas(){ 
			//Modificar los datos de la encuesta
			$consulta=new EstudiantesEncuestados();
			$encuesta=$consulta->get_EstudiantesEncuestados($_POST["IdCita"]);

			//Parte Socioeconomica 
			$datossocioeco["TotIngEst"]=$_POST["TotIngEst"];
			$datossocioeco["TotIngFam"]=$_POST["TotIngFam"];
			$datossocioeco["id_dat_socio_est"]=$encuesta["socioeconomicos"];

			$consulta= new DatosSocEcon();
			$consulta->set_DatosSocEco_update($datossocioeco);

			$consulta= new Egreso();
			$egresos=$consulta->get_egreso();	

			$egresosestudi=array();
			foreach ($egresos as $egreso) {
				$fila["monto"]=$_POST["egrind".$egreso["IdEgreso"]];	
				$fila["tipo"]='Individual';
				$fila["id_concepto"]=$egreso["IdEgreso"];
				$fila["idencuesta"]=$encuesta["Id"];
				$egresosestudi[]=$fila;
			}

			foreach ($egresos as $egreso) {
				$fila["monto"]=$_POST["egrfam".$egreso["IdEgreso"]];	
				$fila["tipo"]='Familiar';
				$fila["id_concepto"]=$egreso["IdEgreso"];
				$fila["idencuesta"]=$encuesta["Id"];
				$egresosestudi[]=$fila;
			}

			$consulta= new GastosxEstudiante();
			$consulta->set_GastosxEstudiante_update($egresosestudi);


			//Parte Socioeconomica 
			
			$datossambient["id_estado"]=$_POST["id_estado"];
			$datossambient["id_parroquia"]=$_POST["id_parroquia"];
			$datossambient["id_ciudad"]=$_POST["id_ciudad"];
			$datossambient["id_tip_viv"]=$_POST["id_tip_viv"];
			$datossambient["id_ten_viv"]=$_POST["id_ten_viv"];
			$datossambient["constr_viv"]=$_POST["constr_viv"];
			$datossambient["id_tipo_pared"]=$_POST["id_tipo_pared"];
			$datossambient["id_tipo_techo"]=$_POST["id_tipo_techo"];
			$datossambient["id_tipo_piso"]=$_POST["id_tipo_piso"];
			$datossambient["sala"]=$_POST["sala"];
			$datossambient["cocina"]=$_POST["cocina"];
			$datossambient["banios"]=$_POST["banios"];
			$datossambient["dormitorios"]=$_POST["dormitorios"];
			$datossambient["luz"]=$_POST["luz"];
			$datossambient["agua"]=$_POST["agua"];
			$datossambient["aseo_urb"]=$_POST["aseo_urb"];
			$datossambient["telf_local"]=$_POST["telf_local"];
			$datossambient["n_habitaciones"]=$_POST["dormitorios"];
			$datossambient["id_dat_fisic_amb"]=$encuesta["ambientales"];

			$consulta= new DatosFisAmbEst();
			$consulta->set_DatosFisAmbEst_update($datossambient);



			//Parte Cultural
			$datosculturales["idencuesta"]=$encuesta["Id"];			
			$datosculturales["num_est"]=$encuesta["estudiante"];
			
			$datosculturales["actividades"]=$_POST["actividades"];

			$consulta= new ActivxEst();
			$actividadesest=$consulta->get_ActivxEst_edit($encuesta["Id"]);
			$add=array();
			$delete=array();
			$fila=array();
			$val=0;

			if (count($datosculturales["actividades"])>0) {			

				for ($i=0; $i < count($datosculturales["actividades"]) ; $i++) { 
					$val=0;
					foreach ($actividadesest as $actividadest) {
						if ($actividadest["id_act_t_l"]==$datosculturales["actividades"][$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $datosculturales["actividades"][$i]);					
					}
				}

				$add["actividades"]=$fila;
				$actividades=$datosculturales["actividades"];
				$datosculturales["actividades"]=$add["actividades"];
				

				if (count($add["actividades"])>0) {
					$consulta= new ActivxEst();
					$consulta->set_ActivxEst($datosculturales);
				}		

				$fila=array();

				foreach ($actividadesest as $actividadest) {
					$val=0;
					for ($i=0; $i < count($actividades) ; $i++){
						if ($actividadest["id_act_t_l"]==$actividades[$i]) {
							$val=1;
						}
					}

					if ($val==0) {
						array_push($fila, $actividadest["id_act_t_l"]);								
					}
				}

				$delete["actividades"]=$fila;

				$datosculturales["actividades"]=$delete["actividades"];
				if (count($delete["actividades"])>0) {
					$consulta= new ActivxEst();
					$consulta->set_ActivxEst_Delete($datosculturales);
				}

			}

			if ($_POST["pracdeporte"]==1) {
				$add=array();
				$delete=array();
				$fila=array();
				$val=0;
				
				$consulta= new DeportesxEstudiante();
				$deportesest=$consulta->get_DeportesxEstudiante_edit($encuesta["Id"]);

				$datosculturales["deportes"]=$_POST["deportes"];

				//buscar los deportes agregados a la encuesta
				for ($i=0; $i < count($datosculturales["deportes"]) ; $i++) { 
					$val=0;
					foreach ($deportesest as $deportest) {
						if ($deportest["id_deporte"]==$datosculturales["deportes"][$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $datosculturales["deportes"][$i]);					
					}
				}

				$add["deportes"]=$fila;	
				$deportes=$datosculturales["deportes"];
				$datosculturales["deportes"]=$add["deportes"];

				//agregar los deportes
				if (count($add["deportes"])>0) {
					$consulta= new DeportesxEstudiante();
					$consulta->set_DeportesxEstudiante($datosculturales);
				}	

				$fila=array();
				//buscar los deportes removidos a la encuesta
				foreach ($deportesest as $deporte) {
					$val=0;
					for ($i=0; $i < count($deportes) ; $i++){
						if ($deporte["id_deporte"]==$deportes[$i]) {
							$val=1;
						}
					}

					if ($val==0) {
						array_push($fila, $deporte["id_deporte"]);								
					}
				}

				$delete["deportes"]=$fila;
				$datosculturales["deportes"]=$delete["deportes"];
				//eliminar los deportes
				if (count($delete["deportes"])>0) {
					$consulta= new DeportesxEstudiante();
					$consulta->set_DeportesxEstudiante_Delete($datosculturales);
				}

			}
			else{
				//Removerlos todos
				$consulta= new DeportesxEstudiante();
				$consulta->set_DeportesxEstudiante_DeleteAll($datosculturales);
			
			}

			if ($_POST["posehaar"]==1) {
				$datosculturales["habilidades"]=$_POST["habilidades"];

				$consulta= new HabilidadesxEstudi();
				$habilidadesest=$consulta->get_HabilidadesxEstudi_edit($encuesta["Id"]);

				//buscar los habilidades agregados a la encuesta
				for ($i=0; $i < count($datosculturales["habilidades"]) ; $i++) { 
					$val=0;

					foreach ($habilidadesest as $habilidadest) {
						if ($habilidadest["id_habil"]==$datosculturales["habilidades"][$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $datosculturales["habilidades"][$i]);					
					}
				}

				$add["habilidades"]=$fila;
				$habilidades=$datosculturales["habilidades"];
				$datosculturales["habilidades"]=$add["habilidades"];
				//agregar los habilidades
				if (count($add["habilidades"])>0) {
					$consulta= new HabilidadesxEstudi();
					$consulta->set_HabilidadesxEstudi($datosculturales);
				}	
				$fila=array();
				//buscar los habilidades removidos a la encuesta
				foreach ($habilidadesest as $habilidade) {
					$val=0;
					for ($i=0; $i < count($habilidades) ; $i++){
						if ($habilidade["id_habil"]==$habilidades[$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $habilidade["id_habil"]);								
					}
				}

				$delete["habilidades"]=$fila;
				$datosculturales["habilidades"]=$delete["habilidades"];
				//eliminar los habilidades
				if (count($delete["habilidades"])>0) {
					$consulta= new HabilidadesxEstudi();
					$consulta->set_HabilidadesxEstudi_Delete($datosculturales);
				}				

			}
			else{
				//Removerlos todos
				$consulta= new HabilidadesxEstudi();
				$consulta->get_HabilidadesxEstudi_DeleteAll($datosculturales);
			
			}

			if ($_POST["actIUTOMS"]==1) {
				$consulta= new EventosxEstud();
				$eventosest=$consulta->get_EventosxEstud_edit($encuesta["Id"]);	

				$datosculturales["eventos"]=$_POST["eventos"];

				//buscar los eventos agregados a la encuesta
				for ($i=0; $i < count($datosculturales["eventos"]) ; $i++) { 
					$val=0;

					foreach ($eventosest as $eventost) {
						if ($eventost["id_evento"]==$datosculturales["eventos"][$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $datosculturales["eventos"][$i]);					
					}
				}

				$add["eventos"]=$fila;
				$eventos=$datosculturales["eventos"];
				$datosculturales["eventos"]=$add["eventos"];
				//agregar los eventos
				if (count($add["eventos"])>0) {
					$consulta= new EventosxEstud();
					$consulta->set_EventosxEstud($datosculturales);
				}	

				$fila=array();
				//buscar los habilidades removidos a la encuesta
				foreach ($eventosest as $evento) {
					$val=0;
					for ($i=0; $i < count($eventos) ; $i++){
						if ($evento["id_evento"]==$eventos[$i]) {
							$val=1;
						}
					}
					if ($val==0) {
						array_push($fila, $evento["id_evento"]);								
					}
				}

				$delete["eventos"]=$fila;
				$datosculturales["eventos"]=$delete["eventos"];
				//eliminar los eventos
				if (count($delete["eventos"])>0) {
					$consulta= new EventosxEstud();
					$consulta->set_EventosxEstud_Delete($datosculturales);
				}	

			}
			else{
				//Removerlos todos
				$consulta= new EventosxEstud();
				$consulta->set_EventosxEstud_DeleteAll($datosculturales);
			
			}

			//Parte Interes	
			$datosinteres["consideracion"]=$_POST["consideracion"];
			$datosinteres["id_ayud"]=$_POST["id_ayud"];
			$datosinteres["id_dat_int_est"]=$encuesta["interesAyu"];


			$consulta= new DatInterEst();
			$consulta->set_DatInterEst_update($datosinteres);


			//Datos del informe
			$informe["grupofamiliar"]=$_POST["grupofamiliar"];
			$informe["OBSegrind"]=$_POST["OBSegrind"];
			$informe["OBSegrfam"]=$_POST["OBSegrfam"];
			$informe["dtrbHabitaciones"]=$_POST["dtrbHabitaciones"];
			$informe["areaficicoambiental"]=$_POST["areaficicoambiental"];
			$informe["diagnosticosocial"]=$_POST["diagnosticosocial"];

			//escala de Graffar
			$informe["usseg"]=$_POST["usseg"];
			$informe["ireconomico"]=$_POST["ireconomico"];
			$informe["oreconomico"]=$_POST["oreconomico"];
			$informe["fireconomico"]=$_POST["fireconomico"];
			$informe["cvpadres"]=$_POST["cvpadres"];
			$informe["uvpadres"]=$_POST["uvpadres"];
			$informe["conclurecomen"]=$_POST["conclurecomen"];
			$informe["BecaAsociada"]=$_POST["BecaAsociada"];
			$informe["agregarProg"]=$_POST["agregarprog"];

			if (isset($_POST["prepayu"])) {
				$informe["prepayu"]=$_POST["prepayu"];
			}	


			$informe["CodInforme"]=$encuesta["cita"];
			$informe["Estudiante"]=$encuesta["estudiante"];
			$informe["TipoInforme"]=4;
			$informe["Responsable"]=$_SESSION['ID'];


			require_once("AtencionCitasController.php");
			$consulta= new AtencionCitasController();
			return $consulta->informe($informe);	

			


		}



	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new EncuestaController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'encuestar')
	{
	 	$conectar = new EncuestaController;			
		$rs = $conectar->encuestar();
	}
	else if ($accion == 'registrar')
	{
	 	$conectar = new EncuestaController;	
		$rs = $conectar->registrar($_POST["form"]);	
	}
	else if ($accion == 'MiembrosFamiliares')
	{
	 	$conectar = new EncuestaController;			
		$rs = $conectar->MiembrosFamiliares();
	}

	else if ($accion == 'AtencionCitas')
	{
	 	$conectar = new EncuestaController;	
		echo $conectar->AtencionCitas();	
	}

	



	else if ($accion == 'edit')
	{
	 	$conectar = new EncuestaController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new EncuestaController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new EncuestaController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>