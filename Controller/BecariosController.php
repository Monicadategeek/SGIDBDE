<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");

	require_once("../Modelo/departamentos.php");	
	require_once("../Modelo/Becarios.php");	
	require_once("../Modelo/SinceracionBecas.php");

	require_once("../Modelo/Inscripcionesbecarios.php");
	require_once("../Modelo/Egresos.php");

	
	require_once("../Modelo/Terceros/estudiantes.php");
	require_once("../Modelo/Terceros/dat_acad_est.php");

	require_once("../Modelo/Referenciales/tipoDepartamento.php");



	class BecariosController{
	
		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}	

			else if (($_SESSION['Tipo_Usuario']==264 and (strpos($_SESSION['TipoPersonal'], '5') === FALSE or strpos($_SESSION['TipoPersonal'], '6') === FALSE))) {
				die("Acceso Denegado :)");
			}
			else if ($_SESSION['Tipo_Usuario']!=265 and $_SESSION['Tipo_Usuario']!=264) {
				die("Acceso Denegado :)");
			}
		}


		public function index($tipo){
			$consulta= new Becarios();
			if ($tipo==1) {
				$becarios=$consulta->get_Becarios_activos_index($_SESSION['ID']);
				require_once("../views/becarios/index_activos.php");
			}
			elseif ($tipo==2) {
				$becarios=$consulta->get_Becarios_inactivos_index($_SESSION['ID']);
				require_once("../views/becarios/index_inactivos.php");
			}
			
			
		}
		
		


		public function create(){ //nuevo
			$consulta= new TipoDepartamento();
			$departamentos=$consulta->get_TipoDepartamento();
			$consulta= new Sedes();
			$sedes=$consulta->get_Sedes();
			require_once("../views/departamentos/new.php");
		}

		public function store(){//registrar
			$departamento["TipoDepto"]=$_POST["tipodepto"];
			$departamento["Sede"]=$_POST["sede"];
			$departamento["AsistenciEstud"]=$_POST["AsistenciEstud"];
			$departamento["Responsable"]=$_SESSION['ID'];
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_store($departamento, $_SESSION['ID']);
		}

		public function show($id){
			$consulta= new Becarios();
			$becario=$consulta->get_Becarios_show($id, $_SESSION['ID']);

			$consulta= new Becarios();
			$historialinscripciones=$consulta->get_Becarios_activos_show($id);

			$consulta= new Becarios();
			$historialegresos=$consulta->get_Becarios_inactivos_show($id, $_SESSION['ID']);


			$consulta= new Becarios();
			$inscripciones=$consulta->get_InscripcionesporBecario($becario["IdBecarios"], $_SESSION['ID']);
			

			$consulta= new DatosAcadEst();
			$datosacademicos=$consulta->get_datos_Academicos($becario["Estudiante"]);
			
			
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($becario["Estudiante"], $_SESSION['ID']);
			

			
			$consulta= new estudiantes();
			$notas=$consulta->get_Estudiantes_Notas($becario["Estudiante"], $_SESSION['ID']);

			require_once("../views/becarios/show.php");

		}

		public function edit($id){//modificar		

			$consulta= new Becarios();
			$becario=$consulta->get_Becarios_edit($id);
			$consulta= new SinceracionBecas();
			$becas=$consulta->get_Becarios_SinceracionBecas_edit($becario["BecaAsociada"]);		
			require_once("../views/becarios/edit.php"); 
		}

		public function update(){//editar
			

			$becario["CtaBancaria"]=$_POST["CtaBancaria"];
			$becario["FechaCtaBanca"]=date('Y-m-d');
			$becario["Responsable"]=$_SESSION['ID'];			
			$becario["IdInscripciones"]=$_POST["IdInscripciones"];
			$becario["IdBecarios"]=$_POST["IdBecarios"];
			$becario["BecaAsociada"]=$_POST["BecaAsociada"];
			
			if (!empty($_POST["egreso"])) {	
				
				
				$becario["Inscripcion"]=$_POST["IdInscripciones"];
				$becario["descripcion"]=$_POST["egreso"];
				$becario["Estado"]=0;
				$consulta= new Inscripcionesbecarios();
				$consulta->set_Inscripcionesbecarios_update($becario);

				$consulta= new Egresos();
				$consulta->set_Egresos_store($becario);
				
			}
			else{
				$becario["Estado"]=1;
			}
			$consulta= new Becarios();
			$consulta->set_Becarios_update($becario);



			return '1';
		}

		public function destroy($id){
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new BecariosController;			
		$rs = $conectar->index($_GET["tipo"]);
	}
	else if ($accion == 'create')
	{
	 	$conectar = new BecariosController;			
		$rs = $conectar->create();
	}
		
	else if ($accion == 'store')
	{
	 	$conectar = new BecariosController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new BecariosController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new BecariosController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new BecariosController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new BecariosController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>