<?php 
	session_start();
	require_once("../Modelo/articulo.php");
	require_once("../Modelo/tagxblog.php");
	require_once("../Modelo/Referenciales/TipoDepartamento.php");
	require_once("../Modelo/Referenciales/tags.php");



	class articulocontroller{

		public function seguridad()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}	

			if ($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') === FALSE) {
				die("Acceso Denegado :(");
			}
			else if ($_SESSION['Tipo_Usuario']!=265 and $_SESSION['Tipo_Usuario']!=264) {
				die("Acceso Denegado :)");
			}
		}


		public function index(){//principal
			$this->seguridad();
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new articulo();
				$articulo=$consulta->get_articulo_index($_SESSION['ID']);
				
			}
			if ($_SESSION['Tipo_Usuario']==264) {
				$consulta= new articulo();
				$articulo=$consulta->get_articulo_index_in($_SESSION['TipoDepto']);
			}
			require_once("../views/blog/index.php");
		}

		public function blog(){//principal
			$consulta= new articulo();
			$articulos=$consulta->get_articulo_blog();
			$consulta= new TagxBlog();
			$Tags=$consulta->get_TagxBlog_principal();		

			require_once("../views/blog/blog.php");
		}
			
		
		


		public function create(){ //nuevo
			$this->seguridad();
			$consulta= new Tags();
			$Tags=$consulta->get_Tags();
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new TipoDepartamento();
				$departamentos=$consulta->get_TipoDepartamento();
				
			}
			if ($_SESSION['Tipo_Usuario']==264) {
				$consulta= new TipoDepartamento();
				$departamentos=$consulta->get_TipoDepartamento_in($_SESSION['TipoDepto']);
			}
			
			require_once("../views/blog/new.php");
		}

		public function store(){//registrar
			$this->seguridad();

			$articulo["Titulo"]=$_POST["titulo"];
			$articulo["Subtitulo"]=$_POST["subtitulo"];
			$articulo["Contenido"]=$_POST["articuloeditor"];
			$articulo["Responsable"]=$_SESSION['ID'];
			$articulo["Estado"]=$_POST["estado"];
			$articulo["DptoAsociado"]=$_POST["DptoAsociado"];



			if (isset($_FILES['imagenprincipal']['tmp_name'])) {
				$extension=explode(".", $_FILES["imagenprincipal"]["name"])[1];
				$articulo["ImagenPrincipal"]=rand(1000, 9999).date('d').date('m').date('Y').".".$extension;
			}

			$carpeta = "../web/articulos/"; 

			if (!file_exists($carpeta)) {
			    mkdir($carpeta, 0777, true);
			}
			$ruta=$carpeta."/".$articulo["ImagenPrincipal"];
        	move_uploaded_file($_FILES["imagenprincipal"]["tmp_name"], $ruta);


			$consulta= new articulo();
			$articulo=$consulta->set_articulo_store($articulo);
			
			if (is_numeric($articulo["Id"])) {
				$tags=$_POST["tags"];

				for ($i=0; $i < count($tags) ; $i++) { 
					$articulo["tags"][$i]=$tags[$i];
				}

				$consulta= new TagxBlog();
				$consulta->set_TagxBlog_store($articulo);
			}
			return 1;			 
		}
 
		public function show($id){//ver articulo
			//$this->seguridad();
			$consulta= new articulo();
			$articulo=$consulta->get_articulo_show($id);
			$consulta= new TagxBlog();
			$tags=$consulta->set_TagxBlog_show($id);

			
			require_once("../views/blog/show.php");

		}

		public function edit($id){//modificar
			$this->seguridad();
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new TipoDepartamento();
				$departamentos=$consulta->get_TipoDepartamento();
				
			}
			if ($_SESSION['Tipo_Usuario']==264) {
				$consulta= new TipoDepartamento();
				$departamentos=$consulta->get_TipoDepartamento_in($_SESSION['TipoDepto']);
			}

			$consulta= new Tags();
			$Tags=$consulta->get_Tags();
			$consulta= new TagxBlog();
			$tagxblog=$consulta->get_TagxBlog($id);
			$consulta= new articulo();
			$articulo=$consulta->get_articulo_edit($id);
			require_once("../views/blog/edit.php");


		}

		public function update(){//editar
			$this->seguridad();
			$consulta= new articulo();
			$imagenprincipal=$consulta->get_articulo_edit($_POST["id"])["ImagenPrincipal"];

			$articulo["Id"]=$_POST["id"];
			$articulo["Titulo"]=$_POST["titulo"];
			$articulo["Subtitulo"]=$_POST["subtitulo"];
			$articulo["Contenido"]=$_POST["articuloeditor"];			
			$articulo["DptoAsociado"]=$_POST["DptoAsociado"];
			$articulo["Responsable"]=$_SESSION['ID'];

			$articulo["Estado"]=$_POST["Estado"];
			if (!empty($_FILES['imagenprincipal']['name'])) {
				
				$carpeta = "../web/articulos/"; 
				if (file_exists($carpeta.$imagenprincipal)) {
		          unlink($carpeta.$imagenprincipal);
		        }
				$extension=explode(".", $_FILES["imagenprincipal"]["name"])[1];
				$articulo["ImagenPrincipal"]=rand(1000, 9999).date('d').date('m').date('Y').".".$extension;
				if (!file_exists($carpeta)) {
				    mkdir($carpeta, 0777, true);
				}
				$ruta=$carpeta."/".$articulo["ImagenPrincipal"];
	        	move_uploaded_file($_FILES["imagenprincipal"]["tmp_name"], $ruta);


			}
			else{
				$articulo["ImagenPrincipal"]=$imagenprincipal;
			}

			$consulta= new articulo();
			$consulta->set_articulo_update($articulo);			


			$tags = $_POST["tags"];
			$consulta= new TagxBlog();
			$consulta->set_TagxBlog_delete($articulo["Id"]);

			$tags=$_POST["tags"];

			for ($i=0; $i < count($tags) ; $i++) { 
				$articulo["tags"][$i]=$tags[$i];
			}

			$consulta= new TagxBlog();
			$consulta->set_TagxBlog_store($articulo);

			return 1;


		}

		public function destroy($id){
			$this->seguridad();
			$consulta= new articulo();
			return $consulta->set_articulo_destroy($id, $_SESSION['ID']);
		}
	}

			if (isset($_GET["accion"])) {
				$accion = $_GET["accion"];
			}
			else if(isset($_POST["accion"])){
				$accion = $_POST["accion"];
			}
			else{
				$accion = 'index';
			}

			if ($accion == 'index'){
				$conectar = new articulocontroller;			
				$rs = $conectar->index();
			}
			if ($accion == 'blog'){
				$conectar = new articulocontroller;			
				$rs = $conectar->blog();
			}
			else if ($accion == 'create')
			{
			 	$conectar = new articulocontroller;			
				$rs = $conectar->create();
			}
			else if ($accion == 'store')
			{
			 	$conectar = new articulocontroller;			
				$rs = $conectar->store();
				echo $rs;
			}
			else if ($accion == 'show')
			{
			 	$conectar = new articulocontroller;	
				$rs = $conectar->show($_GET["id"]);	
			}
			else if ($accion == 'edit')
			{
			 	$conectar = new articulocontroller;	
				$rs = $conectar->edit($_GET["id"]);	
			}
			else if ($accion == 'update')
			{
			 	$conectar = new articulocontroller;			
				$rs = $conectar->update();
				echo $rs;
			}
			else if ($accion == 'destroy')
			{
			 	$conectar = new articulocontroller;			
				$rs = $conectar->destroy($_GET["id"]);
				echo $rs;
			}


?> 

