<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/Terceros/estudiantes.php");
	require_once("../Modelo/FechasEncuestas.php");
	require_once("../Modelo/EstudiantesEncuestados.php");
	require_once("../Modelo/Presupuesto.php");
	require_once("../Modelo/Inscripcionesbecarios.php");

	require_once("../Modelo/Terceros/dat_acad_est.php");
	require_once("../Modelo/Terceros/estudiantes.php");
	require_once("../Modelo/Terceros/dat_socio_est.php");
	require_once("../Modelo/Terceros/grupo_familiar_est.php");
	require_once("../Modelo/Terceros/gastos_x_estudiante.php");
	require_once("../Modelo/Terceros/dat_fisic_ambient_est.php");
	require_once("../Modelo/Terceros/actividades_t_l_x_estudiante.php");
	require_once("../Modelo/Terceros/deportes_x_estudiante.php");
	require_once("../Modelo/Terceros/habilidades_x_estudiante.php");
	require_once("../Modelo/Terceros/eventos_x_estudiantes.php");
	require_once("../Modelo/Terceros/dat_inter_est.php");
	require_once("../Modelo/Terceros/ayuda_econ_x_estudiante.php");
 
 	require_once("../Modelo/Referenciales/egreso.php");
	 

	class EstadisticasEstudiantilesController{
		
		public function index(){
			require_once("../views/estadisticas/estudiantes/index.php");		
		}

		public function show($id){
			$consulta=new EstudiantesEncuestados();
			$encuesta=$consulta->get_EstudiantesEncuestados_Id($id);
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($encuesta["estudiante"], $_SESSION['ID']);

	
			$cita["tipoInforme"]='Informe Estudios Socioeconomicos';
				$consulta= new DatosAcadEst();
				$datosacademicos=$consulta->get_datos_Academicos($encuesta["estudiante"])[0];

			#Datos socioeconomicos
				$consulta= new DatosSocEcon();
				$datossocioeco=$consulta->get_DatosSocEco_Est($encuesta["socioeconomicos"]);
				$consulta= new Egreso();
				$egresos=$consulta->get_egreso();
				$consulta= new DatosSocEcon();
				$datossocioeco=$consulta->get_DatosSocEco_Est($encuesta["socioeconomicos"]);

				$consulta= new AyudaEconxEstudiante();
				$ayudaseconomica=$consulta->get_AyudaEconxEstudiante($encuesta["Id"]);
				if (count($ayudaseconomica)>0) {
					$datossocioeco["ayudaseconomica"]="SI";
				}
				else{
					$datossocioeco["ayudaseconomica"]="NO";
				}
				$consulta= new GrupoFamiliarEst();
				$familiares=$consulta->get_GrupoFamiliarEst_index($encuesta["estudiante"]);
				$consulta= new GastosxEstudiante();
				$gastosindividuales=$consulta->get_GrupoFamiliarEst_gastosindividuales($encuesta["Id"]);
				$consulta= new GastosxEstudiante();
				$gastosfamiliares=$consulta->get_GrupoFamiliarEst_gastosfamiliares($encuesta["Id"]);

				#Datos Ambientales
				$consulta= new DatosFisAmbEst();
				$datosambientales=$consulta->get_DatosFisAmbEst_show($encuesta["ambientales"]);


				#Datos Culturales
				$consulta= new ActivxEst();
				$actividadesest=$consulta->get_ActivxEst_show($encuesta["Id"]);

				$consulta= new DeportesxEstudiante();
				$deportesest=$consulta->get_deportesxest_show($encuesta["Id"]);

				$consulta= new HabilidadesxEstudi();
				$habilidadesest=$consulta->get_HabilidadesxEstudi_show($encuesta["Id"]);

				$consulta= new EventosxEstud();
				$eventosest=$consulta->get_EventosxEstud_show($encuesta["Id"]);


				#Datos Interez estudiantil
				$consulta= new DatInterEst();
				$datosinteressocioeconomico=$consulta->get_DatInterEst_show($encuesta["interesAyu"]);
				require_once("../views/informes/estudiossocioeconomicos.php");

		}

		public function estudiantes($tipo){
			if ($tipo==1) {

				$consulta=new estudiantes;
				$annos=$consulta->get_Estudiantes_Annos();
				require_once("../views/estadisticas/estudiantes/academicos/index.php");
			}
			elseif ($tipo==2) {
				require_once("../views/estadisticas/estudiantes/socioeconomicos/index.php");
			}
			elseif ($tipo==3) {
				require_once("../views/estadisticas/estudiantes/ambientales/index.php");
			}	
			elseif ($tipo==4) {
				require_once("../views/estadisticas/estudiantes/culturales/index.php");
			}
			elseif ($tipo==5) {
				require_once("../views/estadisticas/estudiantes/InteresEstud/index.php");
			}			
		}

		public function academicos($tipo){
			if ($tipo==1) {
				$anno=$_GET["anno"];
				$consulta= new estudiantes();
				$poblacion=$consulta->get_Estudiantes_Total($anno)["total"];

				$consulta= new DatosAcadEst();
				$muestra=$consulta->get_Estadisticas_Total($anno)["total"];

				
				if ($muestra==0) {
					die("Error no tenemos datos de ese año");
				}
				$consulta= new estudiantes();
				$formasingresos=$consulta->get_Estudiantes_Forma_Ingreso($anno);
				
				$consulta= new DatosAcadEst();
				$titulobach=$consulta->get_Estadisticas_TituloBachiller($anno);
				$consulta= new DatosAcadEst();
				$tipoinst=$consulta->get_Estadisticas_TipoInstitucion($anno);
				$consulta= new DatosAcadEst();
				$promnot=$consulta->get_Estadisticas_PromedioNotas($anno);
				$consulta= new DatosAcadEst();
				$identpnf=$consulta->get_Estadisticas_IdentificadoPNF($anno);
				$consulta= new DatosAcadEst();
				$gustapnf=$consulta->get_Estadisticas_GustaPNF($anno);
				require_once("../views/estadisticas/estudiantes/academicos/academicos.php");
			}
		}



		public function fechas($tipo){
			$consulta= new FechasEncuestas();
			$fechas=$consulta->get_Estadisticas_FechasEncuestas_fecha();
			$consulta= new FechasEncuestas();
			$poblacion=$consulta->get_Estadisticas_FechasEncuestas_fecha_maxmin();
			if ($tipo<4) {
				require_once("../views/estadisticas/estudiantes/socioeconomicos/fechas.php");
			}
			else if ($tipo==4 || $tipo==5 ) {
				require_once("../views/estadisticas/estudiantes/ambientales/fechas.php");
			}
			else if ($tipo==6) {
				require_once("../views/estadisticas/estudiantes/culturales/fechas.php");
			}
			else if ($tipo==7) {
				require_once("../views/estadisticas/estudiantes/InteresEstud/fechas.php");
			}
			else if ($tipo==8) {
				$consulta= new Presupuesto();
				$presupuestos=$consulta->get_Presupuesto_Estadisticas();
				require_once("../views/estadisticas/estudiantes/InteresEstud/presupuesto.php");
			}
		}
 

		public function socioeconomico($fechas, $tipo){
			$fecha = explode(",", $fechas);

			$inicio=date('Y-m-d', strtotime($fecha[0]));
			$fin=date('Y-m-d', strtotime($fecha[1]));
			$consulta= new EstudiantesEncuestados();
			$poblacion=$consulta->get_Estadisticas_Estudiantes_total($inicio, $fin);
			if ($poblacion==0) {
				die("Disculpe, no hay estudiantes encuentado en ese rango de fecha");
			}
			if ($tipo==1) //Traslado y Trabajo
			{
				$consulta= new DatosSocEcon();
				$probtrasla=$consulta->get_DatosSocEco_Est_Estadisticas_probTraslado($inicio, $fin);
				$consulta= new DatosSocEcon();
				$comotrasla=$consulta->get_DatosSocEco_Est_Estadisticas_comTraslada($inicio, $fin);
				$consulta= new DatosSocEcon();
				$trabaja=$consulta->get_DatosSocEco_Est_Estadisticas_trabaja($inicio, $fin);
				$consulta= new DatosSocEcon();
				$trabajado=$consulta->get_DatosSocEco_Est_Estadisticas_trabajado($inicio, $fin);
				require_once("../views/estadisticas/estudiantes/socioeconomicos/TrasladoyTrabajo.php");
				
			}
			else if ($tipo==2) //Personal y Familia
			{
				$consulta= new DatosSocEcon();
				$hijos=$consulta->get_DatosSocEco_Est_Estadisticas_TieneHijos($inicio, $fin);
				$consulta= new DatosSocEcon();
				$hijosvivenc=$consulta->get_DatosSocEco_Est_Estadisticas_Hijos_VivenContigo($inicio, $fin);
				$consulta= new DatosSocEcon();
				$comidas=$consulta->get_DatosSocEco_Est_Estadisticas_ComidasAlDia($inicio, $fin);
				$consulta= new DatosSocEcon();
				$parejas=$consulta->get_DatosSocEco_Est_Estadisticas_Pareja($inicio, $fin);
				$consulta= new DatosSocEcon();
				$personasdependiente=$consulta->get_DatosSocEco_Est_Estadisticas_PersonasDependen($inicio, $fin);

				$consulta= new GrupoFamiliarEst();
				$familias=$consulta->get_GrupoFamiliarEst_Estadisticas($inicio, $fin);

				require_once("../views/estadisticas/estudiantes/socioeconomicos/PersonalyFamilia.php");
				
			}
			else if ($tipo==3) //Ingresos y Gastos
			{
				
				$consulta= new DatosSocEcon();
				$presupuesto=$consulta->get_DatosSocEco_Est_Estadisticas_Presupuesto($inicio, $fin);

				$consulta= new GastosxEstudiante();
				$gastosInd=$consulta->get_DatosSocEco_Est_Estadisticas_Presupuesto($inicio, $fin, 'Individual');
				
				$consulta= new GastosxEstudiante();
				$gastosFam=$consulta->get_DatosSocEco_Est_Estadisticas_Presupuesto($inicio, $fin, 'Familiar');

				require_once("../views/estadisticas/estudiantes/socioeconomicos/IngresosyGastos.php");

			}

		}


		public function ambiental($fechas, $tipo){
			$fecha = explode(",", $fechas);

			$inicio=date('Y-m-d', strtotime($fecha[0]));
			$fin=date('Y-m-d', strtotime($fecha[1]));
			$consulta= new EstudiantesEncuestados();
			$poblacion=$consulta->get_Estadisticas_Estudiantes_total($inicio, $fin);
			if ($poblacion==0) {
				die("Disculpe, no hay estudiantes encuentado en ese rango de fecha");
			}
			if ($tipo==4)
			{
				$consulta= new DatosFisAmbEst();
				$residencias=$consulta->get_DatosFisAmbEst_Estadisticas_Residencia($inicio, $fin);
				require_once("../views/estadisticas/estudiantes/ambientales/Residencias.php");
				
			}
			else if ($tipo==5) 
			{

				$consulta= new DatosFisAmbEst();
				$viviendas=$consulta->get_DatosFisAmbEst_Estadisticas_Vivienda($inicio, $fin);

				$consulta= new GrupoFamiliarEst();
				$familias=$consulta->get_GrupoFamiliarEst_Estadisticas_Familiares($inicio, $fin);
				

				$consulta= new DatosFisAmbEst();
				$tenviviendas=$consulta->get_DatosFisAmbEst_Estadisticas_TenenciaVivienda($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$construccion=$consulta->get_DatosFisAmbEst_Estadisticas_Construccion($inicio, $fin);

				$consulta= new DatosFisAmbEst();
				$tipos_pared=$consulta->get_DatosFisAmbEst_Estadisticas_TipoPared($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$tipos_piso=$consulta->get_DatosFisAmbEst_Estadisticas_TipoPiso($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$tipos_techo=$consulta->get_DatosFisAmbEst_Estadisticas_TipoTecho($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$habitacion=$consulta->get_DatosFisAmbEst_Estadisticas_Habitaciones($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$luz=$consulta->get_DatosFisAmbEst_Estadisticas_Servicios_Luz($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$agua=$consulta->get_DatosFisAmbEst_Estadisticas_Servicios_agua($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$aseo_urb=$consulta->get_DatosFisAmbEst_Estadisticas_Servicios_aseo_urb($inicio, $fin);
				$consulta= new DatosFisAmbEst();
				$telf_local=$consulta->get_DatosFisAmbEst_Estadisticas_Servicios_telf_local($inicio, $fin);

				require_once("../views/estadisticas/estudiantes/ambientales/viviendas.php");
				//echo '<img align="center" width="100%" class="img-responsive avatar-view" src="../web/images/Web-en-construccion.jpg">';
				
			}
			

		}


		public function interes($fechas, $tipo){
			
			
			if ($tipo==7)
			{
				$fecha = explode(",", $fechas);
				$inicio=date('Y-m-d', strtotime($fecha[0]));
				$fin=date('Y-m-d', strtotime($fecha[1]));
				$consulta= new EstudiantesEncuestados();
				$poblacion=$consulta->get_Estadisticas_Estudiantes_total($inicio, $fin);
				if ($poblacion==0) {
					die("Disculpe, no hay estudiantes encuentado en ese rango de fecha");
				}

				$consulta= new DatInterEst();
				$becas=$consulta->get_DatInterEst_Estadisticas_Becas($inicio, $fin, $poblacion);

				$consulta= new DatInterEst();
				$comedor=$consulta->get_DatInterEst_Estadisticas_Becas_comedor($inicio, $fin, $poblacion);

				$consulta= new DatInterEst();
				$estudiantes=$consulta->get_DatInterEst_Estadisticas_BecariosPotenciales($inicio, $fin);

				require_once("../views/estadisticas/estudiantes/InteresEstud/interes.php");

			}
			else if ($tipo==8) 
			{
				if (!is_numeric($fechas)) {
					$fecha = explode(",", $fechas);
					$inicio=date('Y-m-d', strtotime($fecha[0]));
					$fin=date('Y-m-d', strtotime($fecha[1]));
					echo "explode";
				}
				
				else{
					$consulta=new Inscripcionesbecarios();
					$becarios=$consulta->get_Inscripcionesbecarios_Estadisticas_Becarios($fechas);

					$consulta=new Inscripcionesbecarios();
					$becarios=$consulta->get_Inscripcionesbecarios_Estadisticas_Becarios_Egresados($fechas);

					require_once("PresupuestoController.php");

					$conectar = new PresupuestoController;	
					$conectar->show($fechas);
					require_once("../views/estadisticas/estudiantes/InteresEstud/becarios.php");
				}

				//echo '<img align="center" width="100%" class="img-responsive avatar-view" src="../web/images/Web-en-construccion.jpg">';
				
			}
			

		}




		public function cultural($fechas){
			$fecha = explode(",", $fechas);

			$inicio=date('Y-m-d', strtotime($fecha[0]));
			$fin=date('Y-m-d', strtotime($fecha[1]));
			$consulta= new EstudiantesEncuestados();
			$poblacion=$consulta->get_Estadisticas_Estudiantes_total($inicio, $fin);
			if ($poblacion==0) {
				die("Disculpe, no hay estudiantes encuentado en ese rango de fecha");
			}
			
			$consulta= new ActivxEst();
			$actividades=$consulta->get_ActivxEst_Estadistica($inicio, $fin);
			$consulta= new DeportesxEstudiante();
			$deportes=$consulta->get_DeportesxEstudiante_Estadistica($inicio, $fin);
			$consulta= new HabilidadesxEstudi();
			$habilidades=$consulta->get_HabilidadesxEstudi_Estadistica($inicio, $fin);
			$consulta= new EventosxEstud();
			$eventos=$consulta->get_EventosxEstud_Estadistica($inicio, $fin);


			require_once("../views/estadisticas/estudiantes/culturales/cultural.php");
			//echo '<img align="center" width="100%" class="img-responsive avatar-view" src="../web/images/Web-en-construccion.jpg">';

		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}
	if ($accion == 'index'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->index();
	}
	
	else if ($accion == 'estudiantes'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->estudiantes($_GET["tipoestadisticas"]);
	}
	else if ($accion == 'academicos'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->academicos($_GET["tipo"]);
	}


	else if ($accion == 'socioeconomico'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->socioeconomico($_GET["fechas"], $_GET["tipoestadistica"]);
	}
	else if ($accion == 'ambiental'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->ambiental($_GET["fechas"], $_GET["tipoestadistica"]);
	}
	else if ($accion == 'cultural'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->cultural($_GET["fechas"]);
	}
	else if ($accion == 'interes'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->interes($_GET["fechas"], $_GET["tipoestadistica"]);
	}

	
	else if ($accion == 'fechas'){
		$conectar = new EstadisticasEstudiantilesController;			
		$rs = $conectar->fechas($_GET["tipoestadistica"]);
	}



	


	


?>