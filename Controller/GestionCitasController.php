
<?php 
	session_start();
	
	require_once("../Modelo/GestionCitas.php");
	require_once("../Modelo/departamentos.php");	
	require_once("../Modelo/Horarios.php");	
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/CancelacionCitas.php");
	require_once("../Modelo/Referenciales/TipoAsistencias.php");
	require_once("../Modelo/EnviarEmail.php");
	

	class GestionCitasController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}
			
		}

		/**
 		 * página de inicio en Atención de Citas 
 		 * La funcion para consultar varia dependiendo el nivel de usuario, si es SuperAdministrador get_GestionCitas_index_admin(), si es Jefe de departamento get_Trabajadores_Jefe_Depto()
 		*/
		public function index($fecha){
			$consulta= new GestionCitas();

			if ($_SESSION['Tipo_Usuario']==265) {
				$citas=$consulta->get_GestionCitas_index_admin($fecha, $_SESSION['ID']);

			}

			else if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') !== false) or $_SESSION['Tipo_Usuario']==265) {
				$jefe="";
				$citas=$consulta->get_GestionCitas_index($fecha,$_SESSION['DeptoAsociado'], $_SESSION['ID']);
			}		

			else if ($_SESSION['Tipo_Usuario']==264 and isset($_SESSION['DeptoAsociado'])){
				
				$citas=$consulta->get_GestionCitas_index($fecha,$_SESSION['DeptoAsociado'], $_SESSION['ID']);
			}
			else{
				die("Usted no Esta asociado a ningun Servicio");
			}

			$value=0;
			foreach ($citas as $cita) {
				if ($cita["Asistencia"]==1) {
					$value=$value+1;
				}
			}

			require_once("../views/GestionCitas/index.php");
				
		}

		
		/**
 		 * Formulario para cancelar citas del dia
 		*/
		public function cancelar($fecha){
			$consulta= new Despartamentos();
			$departamentos=$consulta->get_Departamentos_GestionCitas_cancelar($_SESSION['DeptoAsociado']);
			require_once("../views/GestionCitas/cancelar.php");				
		}

		/**
 		* Metodo POST para modificar la asistencia de las citas de un dia para ser canceladas
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function citaCancelada(){
					
			$citas["Responsable"]=$_SESSION['ID'];
			$citas["Tipo_Cancelacion"]=$_POST["Tipo_Cancelacion"];
			$citas["Servicio"]=$_POST["Depto"];
			$citas["Razon"]=$_POST["razon"];
			$citas["Fecha_Inic"]=date("Y-m-d",strtotime($_POST["Fecha_Inic"]));
			$citas["Fecha_Fin"]=date("Y-m-d",strtotime($_POST["Fecha_Inic"]));
			
			$consulta= new CancelacionCitas();
			$consulta->set_CancelacionCitas_store($citas);

			$citas["Asistencia"]=$_POST["Tipo_Cancelacion"];
			$citas["Servicio"]=$_POST["Depto"];

			$citas["Fecha"]=date("Y-m-d",strtotime($citas["Fecha_Inic"]));
			$consulta= new GestionCitas();
			$consulta->set_GestionCitas_Cancelar($citas);
			$consulta= new TipoAsistencias();
				$citas["DescripcionCancelar"]=$consulta->get_TipoAsistencias()[$cancelaciones["Tipo_Cancelacion"]]["Descripcion"];
					
			/** 
			 * function get_Citas_Cancelar() consulta los datos de los estudiantes que se les fue cancelada la cita
			 */
			 
			$consulta= new CancelacionCitas();
			$estudiantes=$consulta->get_Citas_Cancelar($citas);			
			/** 
			 * Crear los respectivos correos de informacion a cada uno de los estudiantes
			 */
			foreach ($estudiantes as $estudiante) {
				
				$body="<p aling='justify'>Se le notifica al estudiante ".$estudiante["nombres"]." ".$estudiante["apellidos"]." portador de la cédula ".$estudiante["cedula"]." que su cita para el día ".date("d-m-Y", strtotime($citas["Fecha"]))." ha sido cancelada</p><br> <p aling='left'><b>Tipo de Cancelación </b>".$citas["DescripcionCancelar"]." </p><br><p aling='left'><b>Razón</b> ".$cancelaciones["Razon"]." </p>";
				$consulta= new EnviarEmail();
				$consulta->enviarEmail($estudiante["correo"], $body, 'Notificación de Cancelación Cita' );
			}
			return 1;
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new GestionCitasController;	
		if (isset($_GET["fecha"])) {
			$fecha = date("Y-m-d",strtotime($_GET["fecha"]));
		}
		else{
			setlocale(LC_TIME, 'es_VE');
			date_default_timezone_set('America/Caracas');
			$fecha = date("Y-m-d");
		}

		

		$rs = $conectar->index($fecha);
	}
	else if ($accion == 'cancelar')
	{
	 	$conectar = new GestionCitasController;			
		$rs = $conectar->cancelar($_GET["fecha"]);
		echo $rs;
	}
	else if ($accion == 'citaCancelada')
	{
	 	$conectar = new GestionCitasController;			
		$rs = $conectar->citaCancelada();
		echo $rs;
	}

	else if ($accion == 'atender') {
		$conectar = new GestionCitasController;			
		$rs = $conectar->atender($_GET["tipo"], $_GET["id"]);
		echo $rs;
	}



	
?>