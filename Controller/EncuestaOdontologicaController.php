<?php 
	session_start();
	//require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/Usuarios.php");	
	require_once("../Modelo/encuest_odont_pregu.php");	
	require_once("../Modelo/encuesta_odontologica.php");	


	require_once("../Modelo/Referenciales/preguntas_odontologicas.php");



	class EncuestaOdontologicaController{
	


		public function index(){
		


			
		}

		public function create($id){ //nuevo
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				$consulta= new Usuarios();
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
				$consulta= new PreguntasOdontologicas();
				$totalpreguntas=$consulta->get_PreguntasOdontologicas_numrows();
				$mitad=round($totalpreguntas/2);
				$consulta= new PreguntasOdontologicas();
				$preguntas=$consulta->get_PreguntasOdontologicas();


				require_once("../views/encuestaodontologica/new.php");

			}

		}

		public function store(){//registrar
			$encuenta["AntFami"]=$_POST["AntFami"];
			$encuenta["MotCons"]=$_POST["MotCons"];
			$encuenta["Dolor"]=$_POST["Dolor"];
			$encuenta["Control"]=$_POST["Control"];
			$encuenta["SangEnc"]=$_POST["SangEnc"];
			$encuenta["RestProt"]=$_POST["RestProt"];
			$encuenta["EnferAct"]=$_POST["EnferAct"];

			$encuenta["Otro"]=$_POST["Otro"];
			$encuenta["id"]=$_POST["id"];

			$consulta= new EncuestaOdontologica();
			$consulta->set_EncuestaOdontologica_update($encuenta);


			$consulta= new PreguntasOdontologicas();
			$preguntas=$consulta->get_PreguntasOdontologicas();
			$respuesta=array();
			foreach ($preguntas as $pregunta) {		
				
				if ($pregunta["tiporesp"]==1) {
					$respuestas["idencuesta"]=$encuenta["id"];
					$respuestas["idpregunta"]=$pregunta["id"];
					$respuestas["respuesta"]=$_POST["preg".$pregunta["id"]];
					$respuestas["nro"]='null';
					$respuesta[]=$respuestas;
				}
				else if ($pregunta["tiporesp"]==2) {

					$respuestas["idencuesta"]=$encuenta["id"];
					$respuestas["idpregunta"]=$pregunta["id"];
					$respuestas["respuesta"]=$_POST["preg".$pregunta["id"]];
					$respuestas["nro"]=$_POST["resp".$pregunta["id"]];

					if (empty($_POST["resp".$pregunta["id"]])) {
						$respuestas["nro"]='null';
					}
					
					$respuesta[]=$respuestas;
				}

			}
			if (count($respuesta)>0) {
				$consulta= new EncuestOdontPregu();
				$consulta->set_EncuestOdontPregu_store($respuesta);
			}

			require_once("../views/encuestaodontologica/completado.php");


		}

		public function show($id){
			

		}

		public function edit($id){//modificar		
			
		}

		public function update(){//editar
			
			
		}

		public function destroy($id){
			
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'create';
	}

	if ($accion == 'index'){
		$conectar = new EncuestaOdontologicaController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new EncuestaOdontologicaController;			
		$rs = $conectar->create($_GET["id"]);
	}
	else if ($accion == 'store')
	{
	 	$conectar = new EncuestaOdontologicaController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new EncuestaOdontologicaController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new EncuestaOdontologicaController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new EncuestaOdontologicaController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new EncuestaOdontologicaController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>