<?php 	
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/Usuarios.php");

	require_once("../Modelo/Terceros/grupo_familiar_est.php");

	require_once("../Modelo/Referenciales/estado_civil.php");
	require_once("../Modelo/Referenciales/parentescos.php");
	require_once("../Modelo/Referenciales/grado_instruccion.php");
	require_once("../Modelo/Referenciales/prof_ocup.php");

	class MiembrosFamiliaresController{


		public function index(){

			$consulta= new Usuarios();
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
				$consulta= new GrupoFamiliarEst();
				$familiares=$consulta->get_GrupoFamiliarEst_index($persona["num_est"]);

				require_once("../views/miembrosfamiliares/index.php");

			}
			else{
				$persona=$consulta->get_Usuarios_num_est($_GET['estudiante']);
				$estudiante=$persona["num_est"];
				$consulta= new GrupoFamiliarEst();
				$familiares=$consulta->get_GrupoFamiliarEst_index($persona["num_est"]);
				require_once("../views/EstudioSocioAcademico/miembrosfamiliares.php");
			}			
		}

		public function create(){ //nuevo
			if ($_SESSION['Tipo_Usuario']!='Estudiante') {
				$estudiante=$_GET['estudiante'];
			}
			$consulta= new GradoInstruccion();
			$gradinst=$consulta->get_GradoInstruccion();			
			$consulta= new EstadoCivil();
			$estadosciviles=$consulta->get_EstadoCivil();
			$consulta= new Parentescos();
			$parentescos=$consulta->get_Parentescos();
			$consulta= new ProfOcup();
			$profocup=$consulta->get_ProfOcup();
			require_once("../views/miembrosfamiliares/new.php");
		}

		public function store(){//registrar
			$consulta= new Usuarios();			
			if ($_SESSION['Tipo_Usuario']=='Estudiante') 
			{				
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			}
			else{
				$persona=$consulta->get_Usuarios_num_est($_POST['estudiante']);
				
			}
			$grupofamiliarest["id_parentesco"]=$_POST["id_parentesco"];
			$grupofamiliarest["apellidos"]=$_POST["apellidos"];
			$grupofamiliarest["nombres"]=$_POST["nombres"];
			$grupofamiliarest["fec_nac"]=$_POST["fec_nac"];
			$grupofamiliarest["id_edo_civ"]=$_POST["id_edo_civ"];
			$grupofamiliarest["genero"]=$_POST["genero"];
			$grupofamiliarest["id_grado_instr"]=$_POST["id_grado_instr"];
			$grupofamiliarest["id_prof_ocup"]=$_POST["id_prof_ocup"];
			$grupofamiliarest["ingreso_mensual"]=$_POST["ingreso_mensual"];
			$grupofamiliarest["num_est"]=$persona["num_est"];
			$consulta= new GrupoFamiliarEst();
			return $consulta->set_GrupoFamiliarEst_store($grupofamiliarest);
		}

		public function show($id){

		}

		public function edit($id){//modificar			
			$consulta= new GradoInstruccion();
			$gradinst=$consulta->get_GradoInstruccion();			
			$consulta= new EstadoCivil();
			$estadosciviles=$consulta->get_EstadoCivil();
			$consulta= new Parentescos();
			$parentescos=$consulta->get_Parentescos();
			$consulta= new ProfOcup();
			$profocup=$consulta->get_ProfOcup();
			$consulta= new GrupoFamiliarEst();
			$grupofamiliarest=$consulta->get_GrupoFamiliarEst_edit($id);

			if ($_SESSION['Tipo_Usuario']!='Estudiante') {
				$estudiante=$grupofamiliarest["num_est"];
			}

			require_once("../views/miembrosfamiliares/edit.php");

		}

		public function update(){//editar
			$grupofamiliarest["id_grupo_fam_est"]=$_POST["id_grupo_fam_est"];
			$grupofamiliarest["id_parentesco"]=$_POST["id_parentesco"];
			$grupofamiliarest["apellidos"]=$_POST["apellidos"];
			$grupofamiliarest["nombres"]=$_POST["nombres"];
			$grupofamiliarest["fec_nac"]=$_POST["fec_nac"];
			$grupofamiliarest["id_edo_civ"]=$_POST["id_edo_civ"];
			$grupofamiliarest["genero"]=$_POST["genero"];
			$grupofamiliarest["id_grado_instr"]=$_POST["id_grado_instr"];
			$grupofamiliarest["id_prof_ocup"]=$_POST["id_prof_ocup"];
			$grupofamiliarest["ingreso_mensual"]=$_POST["ingreso_mensual"];
			$consulta= new GrupoFamiliarEst();
			return $consulta->set_GrupoFamiliarEst_update($grupofamiliarest);
		}

		public function destroy($id){
			$consulta= new Usuarios();
			if ($_SESSION['Tipo_Usuario']=='Estudiante') 
			{				
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			}
			else{
				$persona=$consulta->get_Usuarios_num_est($_GET['estudiante']);
				
			}	
			


			$consulta= new GrupoFamiliarEst();
			return $consulta->set_GrupoFamiliarEst_destroy($id, $persona["num_est"]);
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new MiembrosFamiliaresController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new MiembrosFamiliaresController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new MiembrosFamiliaresController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new MiembrosFamiliaresController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new MiembrosFamiliaresController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new MiembrosFamiliaresController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new MiembrosFamiliaresController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>