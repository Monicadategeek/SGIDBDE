<?php 
	session_start();
	require_once("../Modelo/Departamentos.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/Trabajadores.php");
	require_once("../Modelo/EnviarEmail.php");
	require_once("../Modelo/Security.php");

	require_once("../Modelo/Terceros/estudiantes.php");
 
	class MyUserController{

		/**
 		 * perfil del Usuario
 		*/
		public function index(){
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				$consulta= new estudiantes();
				$estudiante=$consulta->get_Consultas_Estudiantes($_SESSION['cedula_est']);
			require_once("../views/MyUser/index.php");

			}
			else{
				$consulta= new Usuarios();
				$usuario=$consulta->get_Usuarios_MyUser_show($_SESSION['ID']);

				$consulta= new Trabajadores();
				$departamentos=$consulta->get_Trabajadores_MyUser($_SESSION['ID']);
				
				require_once("../views/MyUser/index_user.php");	
			}
		}

		/**
 		* Consulta el registro y genera el Formulario de usuarios, para modificar el perfil o el estado del usuario
 		* @param [integer] $id
 		*/
		public function edit($id){

			require_once("../views/MyUser/edit.php");

		}

		/**
 		* Metodo POST para modificar un Usuario
 		* Modifica la foto de la persona y modifica la persona en BD 
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			$clave=$_POST["clave"];
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				$consulta=new Security();
				return $consulta->set_usuario_estudiante_update($_SESSION['ID'], $clave);

			}
			else{
				$consulta=new Security();
				return $consulta->set_usuario_update($_SESSION['ID'], $clave);
			}
	

			
		}

		
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new MyUserController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new MyUserController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new MyUserController;			
		$rs = $conectar->update();
		echo $rs;
	}


	


?>