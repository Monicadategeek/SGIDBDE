<?php 
	session_start();
	require_once("../Modelo/Departamentos.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EnviarEmail.php");
 
	class UsuariosController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']!='265') {
				die("Acceso Denegado :)");
			}
		}

		/**
 		 * página de inicio en Usuarios
 		*/
		public function index(){
			$consulta= new Usuarios();
			$usuarios=$consulta->get_Usuarios_user_index($_SESSION['ID']);
			require_once("../views/usuarios/index.php");
		}

		/**
 		* Consulta el registro y genera el Formulario de usuarios, para modificar el perfil o el estado del usuario
 		* @param [integer] $id
 		*/
		public function edit($id){

			$consulta= new Usuarios();
			$usuario=$consulta->get_Usuarios_edit($id);
			require_once("../views/usuarios/edit.php");

		}

		/**
 		* Metodo POST para modificar un Usuario
 		* Modifica la foto de la persona y modifica la persona en BD 
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			
			$usuario["Num_Usuario"]=$_POST["Num_Usuario"];
			$usuario["Id_Tipo_Usuario"]=$_POST["Id_Tipo_Usuario"];
			$usuario["Cod_Estatus"]=$_POST["Cod_Estatus"];
			$usuario["Responsable"]=$_SESSION['ID'];
			if ($usuario["Id_Tipo_Usuario"]==265) {
				$consulta= new Usuarios();
				$administrador=$consulta->get_Usuarios_edit($_SESSION['ID']);
				$administrador["Responsable"]=$_SESSION['ID'];
				$usuario["clave"]=$_POST["clave"];
				if ($usuario["clave"]!=$administrador["clave"]) {
					die('La Contraseña que introdujo es erronea');
				}
				$administrador["Id_Tipo_Usuario"]=264;
				$administrador["Cod_Estatus"]=1;	
				$usuario["Cod_Estatus"]=0;
				$consulta= new Usuarios();
				$consulta->set_Usuarios_User_update($administrador);
				$consulta= new Usuarios();
				$consulta->set_Usuarios_User_update($usuario);
				return 0;
				

			}	
			$consulta= new Usuarios();
			return $consulta->set_Usuarios_User_update($usuario);		

			
		}

		
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new UsuariosController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new UsuariosController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new UsuariosController;			
		$rs = $conectar->update();
		echo $rs;
	}


	


?>