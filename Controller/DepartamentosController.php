<?php 
	session_start();
	require_once("../Modelo/departamentos.php");	
	require_once("../Modelo/Referenciales/tipoDepartamento.php");
	require_once("../Modelo/Referenciales/Sedes.php");

	class DepartamentosController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']!='265') {
				die("Acceso Denegado :)");
			}
		}
 
 		/**
 		 * página de inicio en Departamentos
 		*/
		public function index(){
			$consulta= new Despartamentos();
			$departamentos["Responsable"]=$_SESSION['ID'];
			$departamentos=$consulta->get_Departamentos_index($_SESSION['ID']);
			require_once("../views/departamentos/index.php");
		}

		/**
 		* Formulario para crear un nuevo Departamento
 		*/
		public function create(){ 
			$consulta= new TipoDepartamento();
			$departamentos=$consulta->get_TipoDepartamento();
			$consulta= new Sedes();
			$sedes=$consulta->get_Sedes();
			require_once("../views/departamentos/new.php");
		}

		/**
 		* Metodo POST para registrar un Departamento
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function store(){ //registrar
			$departamento["informacion"]=$_POST["informacion"];
			$departamento["TipoDepto"]=$_POST["tipodepto"];
			$departamento["Sede"]=$_POST["sede"];
			$departamento["AsistenciEstud"]=$_POST["AsistenciEstud"];
			$departamento["Responsable"]=$_SESSION['ID'];
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_store($departamento);
		}

		/**
 		* Muestra detalles de un registro
 		* @param [integer] $id
 		*/
		public function show($id){
			$consulta= new Despartamentos();
			$departamento=$consulta->get_Departamentos_show($id, $_SESSION['ID']);
			
			require_once("../views/departamentos/show.php");

		}

		/**
 		* Consulta el registro y genera el Formulario del Departamento
 		* @param [integer] $id
 		*/
		public function edit($id){
			
			$consulta= new TipoDepartamento();
			$departamentos=$consulta->get_TipoDepartamento();
			$consulta= new Sedes();
			$sedes=$consulta->get_Sedes();
			$consulta= new Despartamentos();
			$Depto=$consulta->get_Departamentos_edit($id);
			require_once("../views/departamentos/edit.php");

		}

		/**
 		* Metodo POST para modificar un Departamento
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			$departamento["informacion"]=$_POST["informacion"];
			$departamento["TipoDepto"]=$_POST["tipodepto"];
			$departamento["Estatus"]=$_POST["Estatus"];
			$departamento["IdDepartamentos"]=$_POST["IdDepartamentos"];
			$departamento["Sede"]=$_POST["sede"];
			$departamento["AsistenciEstud"]=$_POST["AsistenciEstud"];
			$departamento["Responsable"]=$_SESSION['ID'];
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_update($departamento);
		}

		/**
 		* Elimina al registro del Departamento
 		* @param [integer] $id
 		* @return [integer] 
 		*/
		public function destroy($id){
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_destroy($id, $_SESSION['ID']);
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new DepartamentosController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new DepartamentosController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new DepartamentosController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new DepartamentosController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new DepartamentosController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new DepartamentosController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new DepartamentosController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>