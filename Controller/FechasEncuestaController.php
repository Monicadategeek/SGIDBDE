<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/FechasEncuestas.php");


	class FechasEncuestaController{
	
		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']!='265') {
				die("Acceso Denegado :)");
			}
			
		}

		public function index(){
			$consulta= new FechasEncuestas();
			$FechasEncuestas=$consulta->get_FechasEncuestas_index($_SESSION['ID']);
			$consulta= new FechasEncuestas();
			$ultfecha=$consulta->get_FechasEncuestas_Ult();

			require_once("../views/fechasencuestas/index.php");


		}

		public function create(){ //nuevo			
			require_once("../views/fechasencuestas/new.php");
		}

		public function store(){//registrar
			
			$feechasencuestas["Fechainicio"]=$_POST["Fechainicio"];
			$feechasencuestas["Fechafin"]=$_POST["Fechafin"];
			$feechasencuestas["Responsable"]=$_SESSION['ID'];
			$consulta= new FechasEncuestas();
			return $consulta->set_FechasEncuestas_store($feechasencuestas);

		}

		public function show($id){

		}

		public function edit($id){//modificar

			$consulta= new FechasEncuestas();
			$feechasencuesta=$consulta->get_FechasEncuestas_edit($id);

			require_once("../views/fechasencuestas/edit.php");

		}

		public function update(){//editar
			$feechasencuestas["Fechainicio"]=date('Y-m-d', strtotime($_POST["Fechainicio"]));
			$feechasencuestas["Fechafin"]=date('Y-m-d', strtotime($_POST["Fechafin"]));
			$feechasencuestas["Id"]=$_POST["Id"];
			$feechasencuestas["Responsable"]=$_SESSION['ID'];
			$consulta= new FechasEncuestas();
			$feechasencuesta=$consulta->get_FechasEncuestas_edit($_POST["Id"]);

			if (date('Y-m-d', strtotime($feechasencuesta["Fechainicio"]))<=date('Y-m-d')) {
				$feechasencuestas["Fechainicio"]=date('Y-m-d', strtotime($feechasencuesta["Fechainicio"]));
			}
			$consulta= new FechasEncuestas();
			return $consulta->set_FechasEncuestas_update($feechasencuestas);
		}

	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new FechasEncuestaController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new FechasEncuestaController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new FechasEncuestaController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new FechasEncuestaController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new FechasEncuestaController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new FechasEncuestaController;			
		$rs = $conectar->update();
		echo $rs;
	}

	


?>