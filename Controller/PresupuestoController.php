<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once("../Modelo/OperacionesSistemicas.php");
	
	require_once("../Modelo/Presupuesto.php");
	require_once("../Modelo/SinceracionBecas.php");
	require_once("../Modelo/Trabajadores.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/Inscripcionesbecarios.php");


	require_once("../Modelo/Referenciales/TipoBeca.php");

	require_once("../Modelo/imprimirPDF.php");
	require_once("../Modelo/NumberToLetterConverter.php");

	

	

	class PresupuestoController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}	

			else if (($_SESSION['Tipo_Usuario']==264 and (strpos($_SESSION['TipoPersonal'], '5') === FALSE or strpos($_SESSION['TipoPersonal'], '6') === FALSE))) {
				die("Acceso Denegado :)");
			}
			else if ($_SESSION['Tipo_Usuario']!=265 and $_SESSION['Tipo_Usuario']!=264) {
				die("Acceso Denegado :)");
			}
		}


		public function index(){ 
			$consulta= new Presupuesto();
			$presupuestos1=$consulta->get_Presupuesto_index_1($_SESSION['ID']);
			$consulta= new Presupuesto();
			$presupuestos2=$consulta->get_Presupuesto_index_2($_SESSION['ID']);
			$consulta= new Presupuesto();

			$MaxFecha_Comienzo=$consulta->get_Presupuesto_max();
			
			require_once("../views/presupuesto/index.php");
		}


		public function reportes(){ 
			$consulta= new Presupuesto();
			$presupuesto=$consulta->get_Presupuesto_max_min();


			require_once("../views/presupuesto/reportes.php");
		}
		
		public function mostrar_reportes($fecha){ 
			$fecha=date('Y-m-d', strtotime($fecha));

			$consulta= new Presupuesto();
			$presupuesto=$consulta->get_Presupuesto_imprimir();

			foreach ($presupuesto as $presupuesto) {
				if ($fecha>=$presupuesto["Fecha_Comienzo"]) {
					$IdPresupuesto=$presupuesto["IdPresupuesto"];
				}

				if ($presupuesto["Fecha_Comienzo"]>$fecha) {
					break;
				}
			}

			$consulta=new SinceracionBecas(); 
			$sinceracionbecas=$consulta->get_SinceracionBecas_reporte($IdPresupuesto);

			
			foreach ($sinceracionbecas as $sinceracion) {
				$consulta=new Inscripcionesbecarios(); 

				$TiposBecas[]=$consulta->get_Inscripcionesbecarios_reporte($sinceracion["IdSinceracionBecas"], $fecha);
				$consulta=new Inscripcionesbecarios(); 
				$NvoIngresos[]=$consulta->get_Inscripcionesbecarios_reporte_nuevingreso($sinceracion["IdSinceracionBecas"], $fecha);
				//var_dump($NvoIngresos);
				//echo "<br><br><br>";
			}

			$mes=$this->meses(date('m', strtotime($fecha)));
			$anno=date('Y', strtotime($fecha));
			$consulta= new usuarioS();
			$usuario=$consulta->get_Usuarios_show($_SESSION['ID']);			
			require_once("../views/presupuesto/nomina.php");

			
		}

		public function imprimir_reportes($fecha){ 
			$fecha=date('Y-m-d', strtotime($fecha));

			$consulta= new Presupuesto();
			$presupuesto=$consulta->get_Presupuesto_imprimir();

			foreach ($presupuesto as $presupuesto) {
				if ($fecha>=$presupuesto["Fecha_Comienzo"]) {
					$IdPresupuesto=$presupuesto["IdPresupuesto"];
				}

				if ($presupuesto["Fecha_Comienzo"]>$fecha) {
					break;
				}
			}

			$consulta=new SinceracionBecas(); 
			$sinceracionbecas=$consulta->get_SinceracionBecas_reporte($IdPresupuesto);

			
			foreach ($sinceracionbecas as $sinceracion) {
				$consulta=new Inscripcionesbecarios(); 

				$TiposBecas[]=$consulta->get_Inscripcionesbecarios_reporte($sinceracion["IdSinceracionBecas"], $fecha);
				$consulta=new Inscripcionesbecarios(); 
				$NvoIngresos[]=$consulta->get_Inscripcionesbecarios_reporte_nuevingreso($sinceracion["IdSinceracionBecas"], $fecha);
				//var_dump($NvoIngresos);
				//echo "<br><br><br>";
			}

			$mes=$this->meses(date('m', strtotime($fecha)));
			$anno=date('Y', strtotime($fecha));
			$consulta= new usuarioS();
			$usuario=$consulta->get_Usuarios_show($_SESSION['ID']);			
			require_once("../views/presupuesto/imprimir_nomina.php");

			
		}



		

		public function create(){ //nuevo
			$consulta= new TipoBeca();

			$tipobecas=$consulta->get_TipoBeca($_SESSION['ID']);

			$consulta= new Presupuesto();
			$presupuestos=$consulta->get_Presupuesto_create();

			require_once("../views/presupuesto/new.php");
		}

		public function store(){//registrar
			
			$presupuesto["TipoPresupuesto"]=$_POST["TipoPresupuesto"];
			if ($presupuesto["TipoPresupuesto"]==1) {
				$presupuesto["PresupuestoAsociado"]='null';
			}
			else{
				if ($_POST["PresupuestoAsociado"]) {
					$presupuesto["PresupuestoAsociado"]=$_POST["PresupuestoAsociado"];
				}
				else{
					die("No ha seleccionado el presupuesto asociado");
				}
			}
			$presupuesto["TiempoBeca"]=$_POST["TiempoBeca"];
			$presupuesto["Descripcion"]=$_POST["Descripcion"];
			$date= new DateTime($_POST["Fecha_Comienzo"]);
			$presupuesto["Fecha_Comienzo"]=$date->format('Y-m-d');
			$presupuesto["Monto"]=$_POST["Monto"];
			$presupuesto["NroTotalBecarios"]=$_POST["NroTotalBecarios"];

			$presupuesto["Responsable"]=$_SESSION['ID'];
			
			$consulta= new Presupuesto();
			$sinceracionbecas["PresupuestoAsociado"]=$consulta->set_Presupuesto_store($presupuesto);
			if (is_numeric($sinceracionbecas["PresupuestoAsociado"])) {
				$total=$_POST["total"];

				//Obtener el Desarrollo de las Becas
				for ($i=1; $i <=$total ; $i++) { 
					if (!(empty($_POST["NroCupos".$i]))) {
						$sinceracionbecas["Monto".$i]=$_POST["mont".$i];
						$sinceracionbecas["NroCupos".$i]=$_POST["NroCupos".$i];
						$sinceracionbecas["TipoBeca".$i]=$i;
					}				
				}
				$sinceracionbecas["total"]=$total;
				$sinceracionbecas["Responsable"]=$_SESSION['ID'];
				//Registrar el Desarrollo de las Becas
				$consulta= new SinceracionBecas();
				return $consulta->set_SinceracionBecas_store($sinceracionbecas);
			}
			else{
				return $sinceracionbecas["PresupuestoAsociado"];
			}			
		}

		public function imprimir($id){

			$consulta= new Presupuesto();
			$presupuesto1=$consulta->get_Presupuesto_show($id, $_SESSION['ID']);
			$consulta= new SinceracionBecas(); 
			$sinceracionbecas1=$consulta->get_SinceracionBecas_show($presupuesto1["IdPresupuesto"]);
			$consulta= new OperacionesSistemicas();
			$responsable1=$consulta->get_OperacionesSistemicas_show($presupuesto1["Responsable"], 1);
			
			if ($presupuesto1["TipoPresupuesto"]==2) {

				$presupuesto2=$presupuesto1;
				$sinceracionbecas2=$sinceracionbecas1;
				$responsable2=$responsable1;

				$consulta= new Presupuesto();
				$presupuesto1=$consulta->get_Presupuesto_show($presupuesto2["PresupuestoAsociado"], $_SESSION['ID']);
				$consulta= new SinceracionBecas();
				$sinceracionbecas1=$consulta->get_SinceracionBecas_show($presupuesto2["IdPresupuesto"]);

				$consulta= new OperacionesSistemicas();
				$responsable1=$consulta->get_OperacionesSistemicas_show($presupuesto2["Responsable"], 1);
			}

			
			//require_once("../views/presupuesto/show.php");

			require_once("../views/presupuesto/imprimir.php");

			


		}	


		public function show($id){

			$consulta= new Presupuesto();
			$presupuesto1=$consulta->get_Presupuesto_show($id, $_SESSION['ID']);
			$consulta= new SinceracionBecas(); 
			$sinceracionbecas1=$consulta->get_SinceracionBecas_show($presupuesto1["IdPresupuesto"]);
			$consulta= new OperacionesSistemicas();
			$responsable1=$consulta->get_OperacionesSistemicas_show($presupuesto1["Responsable"], 1);
			$id=$presupuesto1["IdPresupuesto"];
			if ($presupuesto1["TipoPresupuesto"]==2) {

				$presupuesto2=$presupuesto1;
				$sinceracionbecas2=$sinceracionbecas1;
				$responsable2=$responsable1;

				$consulta= new Presupuesto();
				$presupuesto1=$consulta->get_Presupuesto_show($presupuesto2["PresupuestoAsociado"], $_SESSION['ID']);
				$consulta= new SinceracionBecas();
				$sinceracionbecas1=$consulta->get_SinceracionBecas_show($presupuesto2["IdPresupuesto"]);

				$consulta= new OperacionesSistemicas();
				$responsable1=$consulta->get_OperacionesSistemicas_show($presupuesto2["Responsable"], 1);
			}

			
			require_once("../views/presupuesto/show.php");

		}

		public function edit($id){//modificar
			$consulta= new Presupuesto();
			$presupuestos=$consulta->get_Presupuesto_create();

			$consulta= new Presupuesto();
			$presupuesto1=$consulta->get_Presupuesto_edit($id);
			if ($presupuesto1["TipoPresupuesto"]==2) {
				$consulta= new Presupuesto();
				$presupuesto2=$consulta->get_Presupuesto_edit($presupuesto1["PresupuestoAsociado"]);
			}
			$consulta= new SinceracionBecas();
			$sinceracionbecas1=$consulta->get_SinceracionBecas_show($presupuesto1["IdPresupuesto"]);
			require_once("../views/presupuesto/edit.php");

		}

		public function update(){//editar
			$presupuesto["IdPresupuesto"]=$_POST["IdPresupuesto"];

			$presupuesto["TipoPresupuesto"]=$_POST["TipoPresupuesto"];
			if ($presupuesto["TipoPresupuesto"]==1) {
				$presupuesto["PresupuestoAsociado"]='null';
			}
			else{
				if (isset($_POST["PresupuestoAsociado"])) {
					$presupuesto["PresupuestoAsociado"]=$_POST["PresupuestoAsociado"];
				}
				else{
					die("No ha seleccionado el presupuesto asociado");
				}
				
			}
			$presupuesto["TiempoBeca"]=$_POST["TiempoBeca"];
			$presupuesto["Descripcion"]=$_POST["Descripcion"];
			$date= new DateTime($_POST["Fecha_Comienzo"]);
			$presupuesto["Fecha_Comienzo"]=$date->format('Y-m-d');
			$presupuesto["Monto"]=$_POST["Monto"];
			$presupuesto["NroTotalBecarios"]=$_POST["NroTotalBecarios"];
			$presupuesto["Responsable"]=$_SESSION['ID'];
			$consulta= new Presupuesto();
			$sinceracionbecas["PresupuestoAsociado"]=$consulta->set_Presupuesto_update($presupuesto);
			if (is_numeric($sinceracionbecas["PresupuestoAsociado"])) {
			
				$total=$_POST["total"];
				for ($i=1; $i <=$total ; $i++) { 
					if (!(empty($_POST["NroCupos".$i]))) {
						$sinceracionbecas["Monto".$i]=$_POST["mont".$i];
						$sinceracionbecas["NroCupos".$i]=$_POST["NroCupos".$i];
						$sinceracionbecas["TipoBeca".$i]=$i;
					}				
				}
				$sinceracionbecas["total"]=$total;
				$sinceracionbecas["Responsable"]=$_SESSION['ID'];

				$consulta= new SinceracionBecas();
				return $consulta->set_SinceracionBecas_update($sinceracionbecas);
			}
			else{
				return $sinceracionbecas["PresupuestoAsociado"];
			}

		}

		public function destroy($id){

			$consulta= new SinceracionBecas();
			$consulta->set_SinceracionBecas_destroy($id);
			$consulta= new Presupuesto();
			return $consulta->set_Presupuesto_destroy($id, $_SESSION['ID']);
		}


		private function meses($id){
			$meses=array('1' => 'ENERO', '2' => 'FEBRERO', '3' => 'MARZO', '4' => 'ABRIL', '5' => 'MAYO', '6' => 'JUNIO', '7' => 'JULIO', '8' => 'AGOSTO', '9' => 'SEPTIEMBRE', '10' => 'OCTUBRE', '11' => 'NOVIEMBRE', '12' => 'DICIEMBRE');
			return $meses[$id];
		}


		
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new PresupuestoController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'reportes')
	{
	 	$conectar = new PresupuestoController;			
		$rs = $conectar->reportes();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new PresupuestoController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new PresupuestoController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new PresupuestoController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'imprimir')
	{
	 	$conectar = new PresupuestoController;	
		$rs = $conectar->imprimir($_GET["id"]);	
	}
	else if ($accion == 'imprimir_reportes')
	{
	 	$conectar = new PresupuestoController;	
		$rs = $conectar->imprimir_reportes($_GET["fecha"]);	
	}
	else if ($accion == 'mostrar_reportes')
	{
	 	$conectar = new PresupuestoController;	
		$rs = $conectar->mostrar_reportes($_GET["fecha"]);	
	}


	
	else if ($accion == 'edit')
	{
	 	$conectar = new PresupuestoController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new PresupuestoController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new PresupuestoController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>