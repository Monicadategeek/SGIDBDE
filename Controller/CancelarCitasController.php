<?php 
	session_start();
	//require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/CancelacionCitas.php");
	require_once("../Modelo/Departamentos.php");
	
	require_once("../Modelo/Referenciales/TipoAsistencias.php");
	require_once("../Modelo/EnviarEmail.php");


	class CancelarCitasController{
	

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}	

			if ($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') === FALSE) {
				die("Acceso Denegado :(");
			}
			else if ($_SESSION['Tipo_Usuario']!=265 and $_SESSION['Tipo_Usuario']!=264) {
				die("Acceso Denegado :)");
			}
		}
		

		public function index(){
			$consulta= new CancelacionCitas();
			$cancelaciones=$consulta->get_CancelacionCitas_index($_SESSION['ID']);
			require_once("../views/cancelacioncitas/index.php");
			
			
		}

		public function create(){ //nuevo
			$consulta= new Despartamentos();
			if (isset($_SESSION['DeptoAsociado'])) {

				
				$departamentos=$consulta->get_Departamentos_Consultas_index_groupby($_SESSION['DeptoAsociado']);
			}
			else{
				$departamentos=$consulta->get_Departamentos_CancelarCitas_cancelar();
			}
			
			
			require_once("../views/cancelacioncitas/new.php");
		}

		public function store(){//registrar
			
			$cancelaciones["Fecha_Inic"]=$_POST["Fecha_Inic"];
			$cancelaciones["Fecha_Fin"]=$_POST["Fecha_Fin"];
			$cancelaciones["Razon"]=$_POST["razon"];
			$cancelaciones["Servicio"]=$_POST["Depto"];
			$cancelaciones["Tipo_Cancelacion"]=$_POST["Tipo_Cancelacion"];
			$cancelaciones["Responsable"]=$_SESSION['ID'];
			//Inser Into en CancelacionCitas
			$consulta= new CancelacionCitas();
			$resul=$consulta->set_CancelacionCitas_store($cancelaciones);


			if ($resul==1) {
				//Para cancelar las citas y enviarle el correo a los pacientes cancelados
				$citas["Servicio"]=$cancelaciones["Servicio"];
				$citas["Asistencia"]=$cancelaciones["Tipo_Cancelacion"];
				$citas["Responsable"]=$cancelaciones["Responsable"];

				$consulta= new TipoAsistencias();
				$citas["DescripcionCancelar"]=$consulta->get_TipoAsistencias()[$cancelaciones["Tipo_Cancelacion"]]["Descripcion"];
				//Si el dia de incicio y el de finalizacion son iguales
				if ($cancelaciones["Fecha_Inic"]==$cancelaciones["Fecha_Fin"]) {
						$dia=$cancelaciones["Fecha_Inic"];						
						$citas["Fecha"]=date("Y-m-d", strtotime($cancelaciones["Fecha_Inic"]));		
						//UPDATE en Citas del dia respectivo	
						$consulta= new CancelacionCitas();
						$consulta->set_GestionCitas_Cancelar($citas);
						//consultar los datos de los estudiantes que se les fue cancelada la cita
						$consulta= new CancelacionCitas();
						$estudiantes=$consulta->get_Citas_Cancelar($citas);

						//Crear los respectivos correos de informacion a cada uno de los estudiantes
						foreach ($estudiantes as $estudiante) {
							$body="<p aling='justify'>Se le notifica al estudiante ".$estudiante["nombres"]." ".$estudiante["apellidos"]." portador de la cédula ".$estudiante["cedula"]." que su cita para el día ".date("d-m-Y", strtotime($citas["Fecha"]))." ha sido cancelada</p><br> <p aling='left'><b>Tipo de Cancelación </b>".$citas["DescripcionCancelar"]." </p><br><p aling='left'><b>Razón</b> ".$cancelaciones["Razon"]." </p>";

							$consulta= new EnviarEmail();
							$consulta->enviarEmail( $estudiante["correo"], $body, 'Notificación de Cancelación Cita' );
						}
					}
					else {
						//Si el dia de incicio y el de finalizacion son diferentes
							$dia=$cancelaciones["Fecha_Inic"];
							//contar el nro de dias que fueron cancelados
							$nrodias=(strtotime($cancelaciones["Fecha_Fin"])-strtotime($cancelaciones["Fecha_Inic"]))/86400;
		                    if ($nrodias==0) {
		                       $nrodias=1;
		                    }

							for ($i=0; $i <= $nrodias; $i++) { 

								$citas["Fecha"]=date("Y-m-d", strtotime($dia));
								//UPDATE en Citas del dia respectivo	
								$consulta= new CancelacionCitas();
								$consulta->set_GestionCitas_Cancelar($citas);
								//consultar los datos de los estudiantes que se les fue cancelada la cita
								$consulta= new CancelacionCitas();
								$estudiantes=$consulta->get_Citas_Cancelar($citas);
								//Crear los respectivos correos de informacion a cada uno de los estudiantes
								
								foreach ($estudiantes as $estudiante) {
									
									$body="<p aling='justify'>Se le notifica al estudiante ".$estudiante["nombres"]." ".$estudiante["apellidos"]." portador de la cédula ".$estudiante["cedula"]." que su cita para el día ".date("d-m-Y", strtotime($citas["Fecha"]))." ha sido cancelada</p><br> <p aling='left'><b>Tipo de Cancelación </b>".$citas["DescripcionCancelar"]." </p><br><p aling='left'><b>Razón</b> ".$cancelaciones["Razon"]." </p>";
									$consulta= new EnviarEmail();
									$consulta->enviarEmail($estudiante["correo"], $body, 'Notificación de Cancelación Cita' );
									//$consulta->enviarEmail( $estudiante["correo"], $body, 'Notificación de Cancelación Cita' );
								}
								//Sumarle 1 dia a la fecha
								$dia = date("Y-m-d", strtotime($dia." + 1 days"));	
							}
						}


					

					return '1';

			}
			else{
				return $resul;
			}


		}

		public function show($id){
			$consulta= new CancelacionCitas();
			$cancelacion=$consulta->set_CancelacionCitas_show($id, $_SESSION['ID']);
			require_once("../views/cancelacioncitas/show.php");

		}

		public function edit($id){//modificar

			
			$consulta= new TipoDepartamento();
			$departamentos=$consulta->get_TipoDepartamento();
			$consulta= new Sedes();
			$sedes=$consulta->get_Sedes();
			$consulta= new Despartamentos();
			$Depto=$consulta->get_consulta_Departamentos_edit($id);
			require_once("../views/departamentos/edit.php");

		}

		public function update(){//editar
			
			$departamento["TipoDepto"]=$_POST["tipodepto"];
			$departamento["Estatus"]=$_POST["Estatus"];
			$departamento["IdDepartamentos"]=$_POST["IdDepartamentos"];
			$departamento["Sede"]=$_POST["sede"];
			$departamento["AsistenciEstud"]=$_POST["AsistenciEstud"];
			$departamento["Responsable"]=$_SESSION['ID'];
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_update($departamento);
		}

		public function destroy($id){
			$consulta= new Despartamentos();
			return $consulta->set_Departamentos_destroy($id, $_SESSION['ID']);
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new CancelarCitasController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new CancelarCitasController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new CancelarCitasController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new CancelarCitasController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new CancelarCitasController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new CancelarCitasController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new CancelarCitasController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>