

<?php 
	session_start();
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/departamentos.php");	
	require_once("../Modelo/Trabajadores.php");	
	require_once("../Modelo/Consultas.php");	
	require_once("../Modelo/Informesmedicos.php");
	require_once("../Modelo/AntecedentesMedicos.php");
	require_once("../Modelo/Citas.php");
	require_once("../Modelo/Informes.php");
	require_once("../Modelo/informe_odontologico.php");
	require_once("../Modelo/EstudioSocioeconomico.php");	



	require_once("../Modelo/Terceros/estudiantes.php");

	require_once("../Modelo/Referenciales/tipoDepartamento.php");
	require_once("../Modelo/Referenciales/enfermedades.php");
	require_once("../Modelo/Referenciales/tratamientos.php");

	
	

	class ConsultasController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}
			
		}

		public function index(){
			$depto='';
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new TipoDepartamento();
				$departamentos=$consulta->get_TipoDepartamento();

			}
			else if (isset($_SESSION['DeptoAsociado'])) {
				$depto=$_SESSION['DeptoAsociado'];
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Departamentos_Consultas_index_groupby($depto);
				
			}	

			foreach ($departamentos as $departamento) {
				if ($departamento["nrotipodepto"]==3) {
					setlocale(LC_TIME, 'es_VE');
					date_default_timezone_set('America/Caracas');
					$consulta= new InformesMedicos();
					$pacientes=$consulta->get_InformesMedicos_PacientesAsistidos($depto, date('Y-m-d'), $_SESSION['Tipo_Usuario']);
				}
			}
			
				
			require_once("../views/consultas/index.php");
				
		}

		public function PacientesTratados($fecha){
			
			$fecha=date('Y-m-d', strtotime($fecha));
			if ($_SESSION['Tipo_Usuario']==265) {
				$depto='';
			}
			else if (isset($_SESSION['DeptoAsociado'])) {
				$depto=$_SESSION['DeptoAsociado'];
			}

			$consulta= new InformesMedicos();
			$pacientes=$consulta->get_InformesMedicos_PacientesAsistidos($depto, $fecha, $_SESSION['Tipo_Usuario']);

			require_once("../views/consultas/pacientes.php");
		}

		public function buscar($estudiante, $depto){			
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Consultas_Estudiantes($estudiante);	


			$consulta= new Despartamentos();
			$departamentos=$consulta->get_Consultas_Departamentos($depto);
			$iddepto="";
			foreach ($departamentos as $departamento) {
				$iddepto=$iddepto.$departamento["IdDepartamentos"].',';
			}
			$iddepto = substr($iddepto, 0, -1);
			
			if ($depto==1) {//Psicologia	
				$consulta= new Informes();
				$seguimientos=$consulta->get_Informes_Psicologico_list($estudiante["num_est"]);
				require_once("../views/consultas/buscar/psicologia.php");
			}
			else if ($depto==2) {//Odontologia	
				$consulta= new InformeOdontologico();
				$seguimientos=$consulta->set_InformeOdontologico_list($estudiante["num_est"]);
				require_once("../views/consultas/buscar/odontologia.php");
			}
			else if ($depto==3) {//Consultar para el depto Medicina General
				$consulta= new AntecedentesMedicos();
				$antecedente=$consulta->get_AntecedentesMedicos_index($estudiante["num_est"], $_SESSION['ID']);
				$consulta= new InformesMedicos();
				$consultas=$consulta->get_InformesMedicos_index($estudiante["cedula"], $iddepto);
				$consulta= new Despartamentos();
				$deptomed=$consulta->get_Departamentos_Consultas_Trab_Hor_DeptoMed($_SESSION['ID']);
				if (count($deptomed)>0) {
					$deptos=$deptomed[0]["IdDepartamentos"];
				}
				else{ 
					$deptos="";
				}
				require_once("../views/consultas/buscar/medicinageneral.php");
			}
			else if ($depto==5) {//Consultar para el depto Estudios Socioeconomicos
				$consulta= new EstudioSocioeconomico();
				$seguimientos=$consulta->set_EstudioSocioeconomico_list($estudiante["num_est"]);
				require_once("../views/consultas/buscar/estudiossocioeconomicos.php");
			}

			else{
				echo '<img align="center" width="100%" class="img-responsive avatar-view" src="../web/images/Web-en-construccion.jpg">';
			}
		}

		

		public function create(){ //Solo para depto de Medicina General
			$estudiante=$_POST["estudiante"];
			$cedula=$_POST["cedula"];
			$depto=$_POST["depto"];
			$consulta= new AntecedentesMedicos();
			$antecedentes=$consulta->get_AntecedentesMedicos_num($estudiante);

			$consulta= new Enfermedades();
			$enfermedades=$consulta->get_enfermedades();

			$consulta= new Tratamientos();
			$tratamientos=$consulta->get_tratamientos();

			$consulta= new Despartamentos();
			$deptomed=$consulta->get_Departamentos_Consultas_Trab_Hor_DeptoMed($_SESSION['ID']);
			if (count($deptomed)==0) {
				die("Disculpe, su horario de atención ha finalizado");
			}

			if ($deptomed[0]["TipoPersonal"]==4 || $deptomed[0]["TipoPersonal"]==7) {
				$consulta= new Trabajadores();
				$doctores=$consulta->get_Trabajador_Consultas_Doctor($depto);
				if (count($doctores)==0) {
					die("Disculpe, No hay un doctor presente en el departamento");
				}				
			}
			else if ($deptomed[0]["TipoPersonal"]!=2) {
				die("Disculpe, su tipo de Ocupación no es apta para este proceso");
			} 

			require_once("../views/consultas/new.php");

			
		}

		public function store(){//registrar Solo para depto de Medicina General

			if (isset($_POST["doctor"])) {
				$doctor=$_POST["doctor"];
			}
			else{
				$doctor=$_SESSION['ID'];
			}

			if (isset($_POST["antecfamiliares"])) {
				$antecedentes["antecfamiliares"]=$_POST["antecfamiliares"];
				$antecedentes["antpersonales"]=$_POST["antpersonales"];
				$antecedentes["nota"]=$_POST["nota"];
				$antecedentes["estudiante"]=$_POST["estudiante"];
				$antecedentes["responsable"]=$doctor;
				$consulta= new AntecedentesMedicos();
				$consulta->set_AntecedentesMedicos_store($antecedentes);
			}
	
			$cita["Cedula"]=$_POST["cedula"];
			$cita["Servicio"]=$_POST["servicio"];
			$cita["IdEstudiante"]=$_POST["estudiante"];
			$consulta= new Citas();		

			$informe["CodInforme"]=$consulta->set_Citas_Consultas_store($cita);
			$informe["Estudiante"]=$_POST["estudiante"];
			$informe["TipoInforme"]=2;
			$informe["Responsable"]=$_SESSION['ID'];

			$consulta= new Informes();		
			$informemedico["IdInformeMedico"]=$consulta->set_Informes_store($informe);

			$informemedico["Descripcion"]=$_POST["Descripcion"];

			$informemedico["Enfermedad"]=$_POST["Enfermedad"];

			//comprobar si el array tiene un solo elemento
			if (count($informemedico["Enfermedad"])==1) {
				//comprobar elemento vale 0 = Otros
				if ($informemedico["Enfermedad"][0]==0) {

					//llamar la variable nuevaenfermedad
					$informemedico["nuevaenfermedad"]=$_POST["nuevaenfermedad"];
					//evaluar que la variable nuevaenfermedad no este vacia
					if (!empty($informemedico["nuevaenfermedad"])) {

						//dividir la variable
						$enfermedades = explode(",", $informemedico["nuevaenfermedad"]);
						$informemedico["Enfermedad"]="";
						//Entro en un ciclo donde consulto y registro las enfermedades y guardo en la variable Enfermedad el resultado seguido de , 
						for ($i=0; $i <count($enfermedades) ; $i++) { 
							$consulta= new Enfermedades();		
							$informemedico["Enfermedad"]=$informemedico["Enfermedad"].$consulta->set_enfermedades_store($enfermedades[$i]).",";
						}
						//Al terminar retiro la ultima coma
						$informemedico["Enfermedad"] = substr($informemedico["Enfermedad"], 0, -1);
					}
					else{
						$informemedico["Enfermedad"]="";
					}
				}
				else{
					$informemedico["Enfermedad"]=$informemedico["Enfermedad"][0];
				}
			}
			//comprobar si el array tiene mas de un elemento
			else if (count($informemedico["Enfermedad"])>1) {
				$enfermedades=$informemedico["Enfermedad"];
				$informemedico["Enfermedad"]="";
				//recorro el array y guardo los datos en la variale Enfermedad seguido de , 
				for ($i=0; $i <count($enfermedades) ; $i++) { 	
					if ($enfermedades[$i]!=0) {
						$informemedico["Enfermedad"]=$informemedico["Enfermedad"].$enfermedades[$i].",";
					}	
					
				}
				//limpio la ultima coma
				$informemedico["Enfermedad"] = substr($informemedico["Enfermedad"], 0, -1);

			}
			else{
				$informemedico["Enfermedad"]="";
			}

			$informemedico["Tratamiento"]=$_POST["Tratamiento"];
			//comprobar si el array tiene un solo elemento
			if (count($informemedico["Tratamiento"])==1) {
				//comprobar elemento vale 0 = Otros
				if ($informemedico["Tratamiento"][0]==0) {
					//llamar la variable nuevatratamiento
					$informemedico["nuevatratamiento"]=$_POST["nuevatratamiento"];
					//evaluar que la variable nuevatratamiento no este vacia
					if (!empty($informemedico["nuevatratamiento"])) {
						//dividir la variable
						$enfermedades = explode(",", $informemedico["nuevatratamiento"]);
						$informemedico["Tratamiento"]="";
					//Entro en un ciclo donde consulto y registro las enfermedades y guardo en la variable Tratamiento el resultado seguido de , 
					for ($i=0; $i <count($enfermedades) ; $i++) { 
						$consulta= new Tratamientos();		
						$informemedico["Tratamiento"]=$informemedico["Tratamiento"].$consulta->set_tratamientos_store($enfermedades[$i]).",";
					}
					//Al terminar retiro la ultima coma
					$informemedico["Tratamiento"] = substr($informemedico["Tratamiento"], 0, -1);
					}
					else{
						$informemedico["Tratamiento"]="";
					}
				}
				else{
					$informemedico["Tratamiento"]=$informemedico["Tratamiento"][0];
				}
			}
			//comprobar si el array tiene mas de un elemento
			else if (count($informemedico["Tratamiento"])>1) {
				$enfermedades=$informemedico["Tratamiento"];
				$informemedico["Tratamiento"]="";
				//recorro el array y guardo los datos en la variale Tratamiento seguido de , 
				for ($i=0; $i <count($enfermedades) ; $i++) { 	
					if ($enfermedades[$i]!=0) {
						$informemedico["Tratamiento"]=$informemedico["Tratamiento"].$enfermedades[$i].",";
					}	
					
				}
				//limpio la ultima coma
				$informemedico["Tratamiento"] = substr($informemedico["Tratamiento"], 0, -1);

			}
			else{
				$informemedico["Tratamiento"]="";
			}		


			$informemedico["doctor"]=$doctor;

			$consulta= new InformesMedicos();	

			return $consulta->set_InformesMedicos_store($informemedico);

			


		}

		public function show($idAntecedente){
			$consulta= new AntecedentesMedicos();
			$antecedente=$consulta->get_AntecedentesMedicos_show($idAntecedente);
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($antecedente["estudiante"], $_SESSION['ID']);
			require_once("../views/consultas/show/antecedentesmedicos.php");

			
 
		}

		public function edit($idAntecedente){//modificar Solo para depto de Medicina General (Solo se puede modificar Ant Medicos)
			$depto=3;
			$consulta= new AntecedentesMedicos();
			$antecedente=$consulta->get_AntecedentesMedicos_edit($idAntecedente);
			require_once("../views/consultas/edit.php");
		}

		public function update(){//modificar Solo para depto de Medicina General (Solo se puede modificar Ant Medicos)
			$antecedentes["antecfamiliares"]=$_POST["antecfamiliares"];
			$antecedentes["antpersonales"]=$_POST["antpersonales"];
			$antecedentes["nota"]=$_POST["nota"];
			$antecedentes["idAntecedente"]=$_POST["idAntecedente"];
			$antecedentes["responsable"]=$_SESSION['ID'];
			$consulta= new AntecedentesMedicos();
			return $antecedente=$consulta->set_AntecedentesMedicos_update($antecedentes);
			
		}


	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new ConsultasController;	
		$rs = $conectar->index();
	}
	
	else if ($accion == 'buscar')
	{

	 	$conectar = new ConsultasController;	
		$rs = $conectar->buscar($_GET["cedula"], $_GET["departamento"]);	
	}
	else if ($accion == 'PacientesTratados')
	{

	 	$conectar = new ConsultasController;	
		$rs = $conectar->PacientesTratados($_GET["fecha"]);	
	}
	else if ($accion == 'create')
	{
		$conectar = new ConsultasController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new ConsultasController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new ConsultasController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new ConsultasController;	
	 	

		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new ConsultasController;			
		$rs = $conectar->update();
		echo $rs;
	}
	



	
?>