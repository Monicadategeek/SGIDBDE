<?php

	require(dirname(__FILE__) ."/../Modelo/Security.php");
	require(dirname(__FILE__) ."/../Modelo/EnviarEmail.php");
	


	class inicioSesion{
		public $pass;
		public $username;

		public function iniciar(){
			$this->username = $_POST['username'];
			$this->pass = $_POST['password'];
			
			$result=$this->consultarUsuCom();
			if ($result!=1) {
				$result2=$this->consultarUsuEstu();
				if ($result==$result2) {
					die($result2);
				}
				else if ($result2==4) {
					die($result);
				}
				else if ($result2==2) {
					die($result2);
				}

				if ($result2==3) {
					die("3");
				}
				die($result2);
				
			}
			else{
				die("1");
			}

			
	}

	public function consultarUsuCom(){
		# Usuarios Comunes
		$consulta= new Security();
		$usuarios=$consulta->get_Usuarios($this->username);
		if (is_array($usuarios) && count($usuarios)>0) {
			if ($usuarios['Cod_Estatus']==1) {
				return("2");
			}
			if ($usuarios['clave']!=$this->pass) {
				return("3");
			}
			session_start();
			$_SESSION['USUARIO'] = $usuarios['Nombres'].' '.$usuarios['Apellidos']; 
			$_SESSION['ID']= $usuarios['Num_Usuario'];
			$_SESSION['Foto_Usuario']= $usuarios['Foto_Usuario'];
			$_SESSION['Tipo_Usuario']= $usuarios['Id_Tipo_Usuario'];
			$consulta= new Security();
			$deptos=$consulta->get_Depto_usuario($usuarios['Num_Usuario']);
			$departamento = "";
			$TipoPersonal = "";
			$TipoDepto = "";
			
			if (is_array($usuarios) && count($usuarios)>0){

				foreach ($deptos as $depto ) {
					$departamento=$departamento.$depto["Depto"].",";
					$TipoPersonal=$TipoPersonal.$depto["TipoPersonal"].",";
					$TipoDepto=$TipoDepto.$depto["TipoDepto"].",";

				}
				$departamento = substr($departamento, 0, -1);
				$TipoPersonal = substr($TipoPersonal, 0, -1);
				$_SESSION['DeptoAsociado']= $departamento;
				$_SESSION['TipoPersonal']= $TipoPersonal;
				$_SESSION['TipoDepto']= $TipoDepto;
			}
			return("1");
			
		}
		return("4");
	}


	public function consultarUsuEstu(){
		# Usuarios Estudiante

		$consulta= new Security();
		$usuarios=$consulta->get_Usuarios_Estudiantes($this->username);
		if (is_array($usuarios) && count($usuarios)>0) {
			if ($usuarios['estatus']==1) {
				return("2");
			}
			if ($usuarios['clave']!=$this->pass) {
				return("3");
			}
			session_start();
			$_SESSION['USUARIO'] = $usuarios['Nombres'].' '.$usuarios['Apellidos']; 
			$_SESSION['ID']= $usuarios['num_usu_e'];
			$_SESSION['cedula_est']= $this->username;
			$_SESSION['Tipo_Usuario']='Estudiante';
			$_SESSION['Foto_Usuario']= $usuarios['foto'];
			return("1");				
		}
		return("4");
	}




	public function recuperar($correo){
		$nuevaclave=rand(100, 999).date('d').date('m').date('Y');
		$consulta= new Security();
		$usuario=$consulta->get_usuario_correo($correo);
		if ($usuario==0) {
			$consulta= new Security();
			$usuario=$consulta->get_usuario_estudiante_correo($correo);
			$consulta= new Security();
			$result=$consulta->set_usuario_estudiante_update($usuario["num_usu_e"], md5($nuevaclave));
			if ($result!=1) {
				return $result;
			}

		}
		else{
			$consulta= new Security();
			$result=$consulta->set_usuario_update($usuario["Num_Usuario"], md5($nuevaclave));
			if ($result!=1) {
				return $result;
			}
		}

		$body='<p><b>Saludos </b> '.$usuario["Nombres"].' </p><br><p>Ha Solicitado la renovación de sus datos de acceso al Sistema Integrado de Admision y Control de Estudios y Evaluación SIACE</p>

		<table>
			<tr>
				<td>Su usuario:  '.$usuario["cedula"].'  </td>
			</tr>
			<tr>
				<td>Su Clave:  '.$nuevaclave.' </td>
			</tr>
		</table>
		<br>
		Puede acceder ahora al Sistema a traves de este enlace: 
		<a href="http://localhost/SGIDBDE">SGIDBDE</a>

		<p>El Sistema SIACE. Hasta Luego </p><br>
		<p><i>LA EDUCACION ES UN CAUDAL MUCHO MAYOR QUE LA FORTUNA</i><br>
		MARISCAL ANTONIO JOSE DE SUCRE</p>';
		$consulta= new EnviarEmail();

		$usuario=$consulta->enviarEmail($correo, $body, 'Recuperar contraseña IUTOMS');
		return 1;

	}
}

	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	
	if ($accion == 'conectar'){
		$conectar = new inicioSesion;			
		$rs = $conectar->iniciar();
		echo $rs;
		
		
	}
	else if ($accion == 'recuperar'){
		$conectar = new inicioSesion;			
		$rs = $conectar->recuperar($_POST['mail']);
		echo $rs;
		
		
	}