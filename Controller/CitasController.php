<?php 
	session_start();
	require_once("../Modelo/Citas.php");
	require_once("../Modelo/Departamentos.php");
	require_once("../Modelo/CancelacionCitas.php");	
	require_once("../Modelo/Horarios.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EstudiantesEncuestados.php");

	require_once("../Modelo/Referenciales/Sedes.php");	
	require_once("../Modelo/Referenciales/tipoDepartamento.php");
	require_once("../Modelo/Referenciales/Dias.php");

	require_once("../Modelo/EnviarEmail.php");

	class CitasController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']!='Estudiante') {
				die("Acceso Denegado :)");
			}
			
		}
		
		/**
 		* página de inicio en Citas
 		*/
		public function index(){
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);		
			
			$consulta= new Citas();
			$citas=$consulta->get_Citas_index($persona["num_est"]);
			require_once("../views/citas/index.php");	
		}

		/**
 		* Muestra el historial citas registradas del trabajador 
 		*/
		public function history(){
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);

			$consulta= new Citas();
			$citas=$consulta->get_Citas_history($persona["num_est"]);
			require_once("../views/citas/history.php");	
		} 

		/**
 		* Formulario para crear una cita 
 		* Nivel 0: Selección del servicio
 		* Nivel 1: Selección de la Sede
 		* Nivel 2: Selección del Día
 		* Nivel 3: Selección de la Hora
 		*/
		public function create($nivel){ 
			
			if ($nivel==0) //Selección del servicio
			{
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Departamentos_Citas_Create();
				require_once("../views/citas/new.php");
			}
			elseif ($nivel==1) //Selección de la Sede
			{
				$consulta= new Sedes();
				$sedes=$consulta->get_Sedes_Citas_Solicitud($_GET["departamento"]);
				$consulta= new Usuarios();
				$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);

				$citas["Servicio"]=$_GET["departamento"];
				$citas["Cedula"]=$persona["cedula"];
				$citas["IdEstudiante"]=$persona["num_est"];

				$consulta= new Citas();
				$departamentos="";
				foreach ($sedes as $sede)
				{
					$departamentos=$departamentos.$sede["IdDepartamentos"].",";
				}
				$citas["Servicio"]=substr($departamentos, 0, -1);

				if (($consulta->get_Citas_Solicitud($citas))>0) {
					
					die("<p><b>Error!</b> Usted Ya Tiene Una Cita Activa en el Servicio </p>");
				}

				require_once("../views/citas/solicitud/sede.php");

			}
			elseif ($nivel==2) {//Selección del dia
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Departamentos_edit($_GET["departamento"]);				
				$consulta= new Dias();
				$dias=$consulta->get_Dias_Citas_Solicitud($_GET["departamento"]);

				$consulta= new CancelacionCitas();
				$citas=$consulta->get_Citas_Canceladas($_GET["departamento"]);

				require_once("../views/citas/solicitud/dia.php");
			}
			elseif ($nivel==3) {//Selección de la hora

				$consulta= new Horarios();
				$horas=$consulta->get_Horarios_Citas_Solicitud($_GET["departamento"], date("w",strtotime($_GET["fecha"])));
				$consulta= new Dias();
				$dia=$consulta->get_Dias()[(date("w",strtotime($_GET["fecha"]))-1)]["Descripcion"];
				$consulta= new Citas();
				$citas=$consulta->get_Citas_Solicitadas($_GET["departamento"], date("Y-m-d",strtotime($_GET["fecha"])));

				require_once("../views/citas/solicitud/hora.php");
			}
		}

		/**
 		* Metodo POST para registrar una cita
 		* Se envia un correo informandole la creación de la cita al Estudiante
 		* Si el Tipo de Servicio es 2 (Odontologia) o 5 (Estudios SocioEconomicos) deberá crearse un formulario respectivamento por el servicio
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function store(){
			$citas["Servicio"]=$_GET["departamento"];
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$citas["TipSer"]=$_GET["servicio"];
			$citas["Hora"]=$_GET["hora"];
			$citas["Fecha"]=date("Y-m-d",strtotime($_GET["fecha"]));
			$citas["Cedula"]=$persona["cedula"];
			$citas["IdEstudiante"]=$persona["num_est"];
			$consulta= new Despartamentos();
			$departamentos=$consulta->get_Despartamentos_Citas_Store($_GET["departamento"]);
			$consulta= new Citas();
			$citas["id"]=$consulta->set_Citas_store($citas);
			if (is_numeric($citas["id"])) 
			{ 
				if ($citas["TipSer"]==2) {
					$consulta= new EncuestaOdontologica();
					$consulta->set_EncuestaOdontologica_store($citas);
				}
				if ($citas["TipSer"]==5) {
					$consulta= new EstudiantesEncuestados();
					$consulta->get_Citas_Encuesta_Estudiantes($citas["IdEstudiante"], $citas["id"]);
				}
				
				require_once("../views/citas/solicitud/completado.php");
				$consulta= new EnviarEmail();
				$consulta->enviarEmail( $persona["correo"], $body, 'Acuse de Solicitud de Cita' );
			}
			else{
				echo "<p><b>Error!</b> Usted Ya Registro Una Cita en el Servicio de ".$departamentos["TipoDepto"]."</p>";
			}
		}


		/**
 		* Consulta el registro y genera el Formulario para modificar una cita 
 		* Nivel 0: Selección de la Sede
 		* Nivel 1: Selección del Día
 		* Nivel 2: Selección de la Hora
 		*/
		public function edit($id, $nivel){
			
			if ($nivel==0) //Selección de la sede
			{
				$consulta= new Citas();
				$cita=$consulta->get_Citas_show($id);
				$consulta= new Citas();
				$citas_update=$consulta->get_Citas_edit($id);
				
				$consulta= new Sedes();
				$sedes=$consulta->get_Sedes_Citas_Solicitud($citas_update["TipoDepto"]);

				require_once("../views/citas/edit.php");
			}
			elseif ($nivel==1) { //Selección del dia
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Departamentos_edit($_GET["departamento"]);

				$consulta= new CancelacionCitas();
				$citas=$consulta->get_Citas_Canceladas($_GET["departamento"]);
				$consulta= new Dias();
				$dias=$consulta->get_Dias_Citas_Solicitud($_GET["departamento"]);
				require_once("../views/citas/edicion/dia.php");
			}
			elseif ($nivel==2) {//Selección del hora

				$consulta= new Horarios();
				$horas=$consulta->get_Horarios_Citas_Solicitud($_GET["departamento"], date("w",strtotime($_GET["fecha"])));
				$consulta= new Dias();
				$dia=$consulta->get_Dias()[(date("w",strtotime($_GET["fecha"]))-1)]["Descripcion"];
				$consulta= new Citas();
				$citas=$consulta->get_Citas_Solicitadas($_GET["departamento"], date("Y-m-d",strtotime($_GET["fecha"])));

				require_once("../views/citas/edicion/hora.php");
			}
		}


		/**
 		* Metodo POST para modificar la cita
 		* Se envia un correo informandole la modificación de la cita al Estudiante
 		* 
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			$citas["Servicio"]=$_GET["departamento"];
			$citas["hora"]=$_GET["hora"];
			$citas["fecha"]=date("Y-m-d",strtotime($_GET["fecha"]));
			$citas["IdCitas"]=$_GET["id"];
			
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$consulta= new Despartamentos();
			$departamentos=$consulta->get_Despartamentos_Citas_Store($_GET["departamento"]);
			$consulta= new Citas();
			$result=$consulta->set_Citas_update($citas);
			if ($result==1) 
			{
				require_once("../views/citas/edicion/completado.php");
				$consulta= new EnviarEmail();
				$consulta->enviarEmail( $persona["correo"], $body, 'Acuse de Modificación de Cita' );
				return 1;
			}
			else{
				return $result;
			}

		}

		/**
 		* Cancelar cita al registro del Trabajador
 		* Envia un Correo al estudiante informando la cancelación de la cita
 		* @param [integer] $id
 		* @return [integer] 
 		*/
		public function cancelar($id){
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Citas_Solicitud($_SESSION['ID']);
			$consulta= new Citas();
			$citas_update=$consulta->get_Citas_edit($id);
			$consulta= new Despartamentos();
			$departamentos=$consulta->get_Despartamentos_Citas_Store($citas_update["Servicio"]);
			require_once("../views/citas/edicion/CancelacionCompletado.php");
				$consulta= new EnviarEmail();
				$consulta->enviarEmail( $persona["correo"], $body, 'Acuse de Cancelación de Cita' );

			$consulta= new Citas();
			return $consulta->set_Citas_cancelar($id);
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new CitasController;			
		
		

		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
		if (isset($_GET["nivel"])) {
			$nivel = $_GET["nivel"];
		}
		else{
			$nivel = 0;
		}

	 	$conectar = new CitasController;			
		$rs = $conectar->create($nivel);
	}
	else if ($accion == 'store')
	{
	 	$conectar = new CitasController;			
		$rs = $conectar->store();
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new CitasController;	
	 	if (isset($_GET["nivel"])) {
			$nivel = $_GET["nivel"];
		}
		else{
			$nivel = 0;
		}

		$rs = $conectar->edit($_GET["id"], $nivel);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new CitasController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new CitasController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}
	else if ($accion == 'cancelar')
	{
	 	$conectar = new CitasController;			
		$rs = $conectar->cancelar($_GET["id"]);
		echo $rs;
	}
	else if ($accion == 'history')
	{
	 	$conectar = new CitasController;			
		$rs = $conectar->history();
		echo $rs;
	}


	
?>