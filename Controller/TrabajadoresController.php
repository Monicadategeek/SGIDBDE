<?php 
	session_start();
	require_once("../Modelo/Referenciales/TipoPersonal.php");
	require_once("../Modelo/Departamentos.php");
	require_once("../Modelo/Trabajadores.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EnviarEmail.php");
  

	class TrabajadoresController{

 		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='Estudiante') {
				die("Acceso Denegado :)");
			}	

			if ($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') === FALSE) {
				die("Acceso Denegado :(");
			}
			else if ($_SESSION['Tipo_Usuario']!=265 and $_SESSION['Tipo_Usuario']!=264) {
				die("Acceso Denegado :)");
			}
		}


		/**
 		 * página de inicio en Trabajadores
 		 * La funcion para consultar varia dependiendo el nivel de usuario, si es SuperAdministrador get_Trabajadores_index(), si es Jefe de departamento get_Trabajadores_Jefe_Depto()
 		*/
		public function index(){
			
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new Trabajadores();
				$trabajadores=$consulta->get_Trabajadores_index($_SESSION['ID']);
			}
			else{
				$consulta= new Trabajadores();
				$deptos=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
				if (count($deptos)==0) {
					die("Acceso Denegado :)");
				}

				$departamento = "";
				$title = "";
				foreach ($deptos as $depto ) {
					$title=$title." ".$depto["TipoDepto"]." en ".$depto["Sede"].",";
					$departamento=$departamento.$depto["Depto"].",";
				}

				$title = substr($title, 0, -1);
				$departamento = substr($departamento, 0, -1);
				
				$consulta= new Trabajadores();
				$trabajadores=$consulta->get_Trabajadores_index_Jefe_Depto($departamento, $_SESSION['ID']);

			}
			
			require_once("../views/trabajadores/index.php");
		}


		/**
 		 * página de Trabajadores Inactivos 		
 		*/
		public function inactivos(){
			
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new Trabajadores();
				$trabajadores=$consulta->get_Trabajadores_index_inactivos($_SESSION['ID']);
				require_once("../views/trabajadores/inactive.php");
			} 
			else{
				die("Acceso Denegado :)");
			}
			
			
		}

		/**
 		* Formulario para crear un nuevo Trabajador
 		*/
		public function create(){ 
			$consulta= new Usuarios();
			$personas=$consulta->get_Usuarios_Trabajadores_create();
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Despartamentos_Trabajadores_create();
			}
			else{
				$consulta= new Trabajadores();
				$departamentos=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
			}
			
			$consulta= new TipoPersonal();
			$tipopersonal=$consulta->get_TipoPersonal();

			require_once("../views/trabajadores/new.php");
		}


		/**
 		* Metodo POST para registrar un Trabajador
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function store(){
			if ($_SESSION['Tipo_Usuario']==264) {
				$consulta= new Trabajadores();
				$departamentos=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
				
				foreach($departamentos as $departamento){
					if (in_array($_POST["Depto"], $departamento)) {
						$valor=1;
					}				
				}

				if (!isset($valor)) {
					die("El Departamento Asociado que introdujo esta fuera de su dominio");
				}

			}

			$trabajador["Persona"]=$_POST["persona"];
			$trabajador["Depto"]=$_POST["Depto"];
			$trabajador["TipoPersonal"]=$_POST["TipoPersonal"];
			$date= new DateTime($_POST["FechaIngreso"]);
			$trabajador["FechaIngreso"]=$date->format('Y-m-d');
			$trabajador["Responsable"]=$_SESSION['ID'];			
			$consulta= new Trabajadores();
			return $consulta->set_Trabajadores_store($trabajador);			
		}


		/**
 		* Muestra detalles de un registro
 		* @param [integer] $id
 		*/
		public function show($id){

			$consulta= new Trabajadores();
			$trabajador=$consulta->get_Trabajadores_show($id, $_SESSION['ID']);

			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_show($trabajador["Persona"], $_SESSION['ID']);

			require_once("../views/trabajadores/show.php");

		}

		/**
 		* Consulta el registro y genera el Formulario del Trabajador
 		* @param [integer] $id
 		*/
		public function edit($id){//modificar
			if ($_SESSION['Tipo_Usuario']==265) {
				$consulta= new Despartamentos();
				$departamentos=$consulta->get_Despartamentos_Trabajadores_create();
			}
			else{
				$consulta= new Trabajadores();
				$departamentos=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
			}
			$consulta= new TipoPersonal();
			$tipopersonal=$consulta->get_TipoPersonal();
			$consulta= new Trabajadores();
			$trabajador=$consulta->get_consulta_Trabajadores_edit($id);
			require_once("../views/trabajadores/edit.php");

		}

		/**
 		* Metodo POST para modificar un Trabajador
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			if ($_SESSION['Tipo_Usuario']==264) {
				$consulta= new Trabajadores();
				$departamentos=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
				
				foreach($departamentos as $departamento){
					if (in_array($_POST["Depto"], $departamento)) {
						$valor=1;
					}				
				}

				if (!isset($valor)) {
					die("El Departamento Asociado que introdujo esta fuera de su dominio");
				}

			}
			$trabajador["IdTrabajador"]=$_POST["IdTrabajador"];
			$trabajador["Depto"]=$_POST["Depto"];
			$trabajador["TipoPersonal"]=$_POST["TipoPersonal"];
			$date= new DateTime($_POST["FechaIngreso"]);
			$trabajador["FechaIngreso"]=$date->format('Y-m-d');			
			$trabajador["Responsable"]=$_SESSION['ID'];
			$consulta= new Trabajadores();
			return $consulta->set_Trabajadores_update($trabajador);
		}


		/**
 		* Desactiva al registro del Trabajador
 		* @param [integer] $id
 		* @return [integer] 
 		*/
		public function destroy($id){
			
			if (isset($_GET["formulario"])) {
				
				$consulta= new Trabajadores();
				return $consulta->set_Trabajadores_destroy($id, $_SESSION['ID']);

			}
			
			$boton='<button class="btn btn-danger btn-large" title="Desactivar" onclick="'."cuadropregunta('info', 'fa fa-exclamation', '¿Esta seguro que desea desactivar al trabajador?', '../Controller/TrabajadoresController.php', {'accion':'destroy', 'id': ".$id.", 'formulario':'si'});".'">Desactivar</button>';
			$consulta= new Trabajadores();
			$trabajador=$consulta->get_Trabajadores_show($id, $_SESSION['ID']);

			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_show($trabajador["Persona"], $_SESSION['ID']);

			require_once("../views/trabajadores/show.php");

			
		}

		/**
 		* Activa al registro del Trabajador
 		* Envia un 
 		* @param [integer] $id
 		* @return [integer] 
 		*/
		public function activate($id){
			
			if (isset($_GET["formulario"])) {
				
				$consulta= new Trabajadores();
				return $consulta->set_Trabajadores_activate($id, $_SESSION['ID']);

			}
			$boton='<button class="btn btn-success btn-large" title="Activar" onclick="'."cuadropregunta('success', 'fa fa-check-square-o', '¿Esta seguro que desea activar al trabajador?', '../Controller/TrabajadoresController.php', {'accion':'activate', 'id': ".$id.", 'formulario':'si'});".'">Activar</button>';
			$consulta= new Trabajadores();
			$trabajador=$consulta->get_Trabajadores_show($id, $_SESSION['ID']);

			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_show($trabajador["Persona"], $_SESSION['ID']);

			require_once("../views/trabajadores/show.php");
		}

		
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}


	if ($accion == 'index'){
		$conectar = new TrabajadoresController;			
		$rs = $conectar->index();
	}
	if ($accion == 'inactivos'){
		$conectar = new TrabajadoresController;			
		$rs = $conectar->inactivos();
	}
	
	else if ($accion == 'create')
	{
	 	$conectar = new TrabajadoresController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new TrabajadoresController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new TrabajadoresController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new TrabajadoresController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new TrabajadoresController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new TrabajadoresController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}
	else if ($accion == 'activate')
	{
	 	$conectar = new TrabajadoresController;			
		$rs = $conectar->activate($_GET["id"]);
		echo $rs;
	}



	


?>