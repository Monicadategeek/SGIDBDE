<?php 	
	session_start();
	require_once("../Modelo/Usuarios.php");	
	require_once("../Modelo/Trabajadores.php");
	require_once("../Modelo/Horarios.php");
	
	class HorariosController{ 

		
		/**
 		 * Muestra las Horas registradas en el o los departamentos asociados a un trabajador pero sin tener posibilidad de modificarlo
 		*/
		public function mostrar($id){ 
			
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Horarios_create($id);
			$consulta= new Trabajadores();
			$trabajadores=$consulta->get_Trabajadores_Horarios_create($persona["Num_Usuario"]);

			$consulta= new Horarios();
			$horarios=$consulta->get_Horarios($trabajadores);


			require_once("../views/horarios/show.php");
		}

		/**
 		 * Muestra las Horas registradas en el o los departamentos asociados a un trabajador
 		*/
		public function create($id){
			
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_Horarios_create($id);
			$consulta= new Trabajadores();
			$trabajadores=$consulta->get_Trabajadores_Horarios_create($persona["Num_Usuario"]);

			$consulta= new Horarios();
			$horarios=$consulta->get_Horarios($trabajadores);
			require_once("../views/horarios/new.php");
		}

		/**
 		* Metodo POST para registrar un Horario
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function store(){

			$horarios["IdEmpleado"]=$_POST["IdEmpleado"];
			$horarios["Dia"]=$_POST["dia"];
			$horarios["HorarioInic"]=$_POST["HorarioInic"];
			$horarios["HorarioFinal"]=$_POST["HorarioFinal"];
			$horarios["Responsable"]=$_SESSION['ID'];
			$consulta= new Horarios();
			return $consulta->set_Horarios_store($horarios);
		}		

		/**
 		* Metodo POST para modificar un Horario
 		* @return [integer] 1 
 		*/
		public function update(){
			
			$horarios["Dia"]=$_POST["dia"];
			$horarios["IdHorarios"]=$_POST["Idhorario"];
			$horarios["HorarioInic"]=$_POST["HorarioInic"];
			$horarios["HorarioFinal"]=$_POST["HorarioFinal"];
			$horarios["Responsable"]=$_SESSION['ID'];
			$consulta= new Horarios();
			return $consulta->set_Horarios_update($horarios);
		}

		/**
 		* Elimina al registro del Horario
 		* @param [integer] $id
 		* @return [integer] 1
 		*/
		public function destroy($id){
			$consulta= new Horarios();
			return $consulta->set_Horarios_destroy($id, $_SESSION['ID']);
		}


	}

if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}

	if ($accion == 'create')
	{
	 	$conectar = new HorariosController;			
		$rs = $conectar->create($_GET["id"]);
	}
	else if ($accion == 'mostrar')
	{
	 	$conectar = new HorariosController;			
		$rs = $conectar->mostrar($_GET["id"]);
	}	
	else if ($accion == 'store')
	{
	 	$conectar = new HorariosController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'update')
	{
	 	$conectar = new HorariosController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new HorariosController;			
		$rs = $conectar->destroy($_POST["Idhorario"]);
		echo $rs;
	}