
<?php 
	session_start();
	
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/Referenciales/municipios.php");
	require_once("../Modelo/Referenciales/parroquias.php");
	require_once("../Modelo/Referenciales/ciudades.php");
	

	class EdoMunParrCiuController{


		
		public function ciudad($estado){
			$consulta= new Ciudades();
			$ciudades=$consulta->get_ciudades($estado);
			require_once("../views/estudiantes/encuesta/estados/show_ciudades.php");

			
		}

		public function municipio($estado){
			$consulta= new Municipios();
			$municipios=$consulta->get_municipios($estado);
			require_once("../views/estudiantes/encuesta/estados/show_municipios.php");

			
		}

		public function parroquia($municipio){
			$consulta= new Parroquias();
			$parroquias=$consulta->get_parroquias($municipio);
			require_once("../views/estudiantes/encuesta/estados/show_parroquias.php");

			
		}
	}

	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	
	if ($accion == 'municipio')
	{
	 	$conectar = new EdoMunParrCiuController;	
		$rs = $conectar->municipio($_GET["estado"]);	
	}
	else if ($accion == 'parroquia')
	{
	 	$conectar = new EdoMunParrCiuController;	
		$rs = $conectar->parroquia($_GET["municipio"]);	
	}
	else if ($accion == 'ciudad')
	{
	 	$conectar = new EdoMunParrCiuController;	
		$rs = $conectar->ciudad($_GET["estado"]);	
	}
	
?>