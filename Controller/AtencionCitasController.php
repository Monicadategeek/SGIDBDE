<?php 
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once("../Modelo/OperacionesSistemicas.php");
	require_once("../Modelo/FechasEncuestas.php");	
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EstudiantesEncuestados.php");
	require_once("../Modelo/Citas.php");
	require_once("../Modelo/SinceracionBecas.php");	
	require_once("../Modelo/EstudioSocioeconomico.php");
	require_once("../Modelo/Becarios.php");
	require_once("../Modelo/Inscripcionesbecarios.php");
	require_once("../Modelo/GestionCitas.php");

	require_once("../Modelo/Informes.php");	
	require_once("../Modelo/encuest_odont_pregu.php");	
	require_once("../Modelo/encuesta_odontologica.php");
	require_once("../Modelo/informe_odontologico.php");	


	require_once("../Modelo/Terceros/grupo_familiar_est.php");
	require_once("../Modelo/Terceros/estudiantes.php");
	require_once("../Modelo/Terceros/dat_acad_est.php");
	require_once("../Modelo/Terceros/dat_socio_est.php");
	require_once("../Modelo/Terceros/dat_inter_est.php");
	require_once("../Modelo/Terceros/ayuda_econ_x_estudiante.php");
	require_once("../Modelo/Terceros/gastos_x_estudiante.php");
	require_once("../Modelo/Terceros/dat_fisic_ambient_est.php");
	require_once("../Modelo/Terceros/actividades_t_l_x_estudiante.php");
	require_once("../Modelo/Terceros/deportes_x_estudiante.php");
	require_once("../Modelo/Terceros/habilidades_x_estudiante.php");
	require_once("../Modelo/Terceros/eventos_x_estudiantes.php");

	require_once("../Modelo/Referenciales/TipoBeca.php");
	require_once("../Modelo/Referenciales/menciones_bach.php");
	require_once("../Modelo/Referenciales/egreso.php");
	require_once("../Modelo/Referenciales/ayuda_economica.php");
	require_once("../Modelo/Referenciales/estados.php");
	require_once("../Modelo/Referenciales/parroquias.php");
	require_once("../Modelo/Referenciales/municipios.php");
	require_once("../Modelo/Referenciales/ciudades.php");
	require_once("../Modelo/Referenciales/tipo_vivienda.php");
	require_once("../Modelo/Referenciales/tipos_pared.php");
	require_once("../Modelo/Referenciales/tipos_piso.php");
	require_once("../Modelo/Referenciales/tipos_techo.php");	
	require_once("../Modelo/Referenciales/tenecia_vivienda.php");
	require_once("../Modelo/Referenciales/act_tiempo_libre.php");
	require_once("../Modelo/Referenciales/categorias_deportes.php");
	require_once("../Modelo/Referenciales/habilidades_est.php");
	require_once("../Modelo/Referenciales/eventos_iutoms.php");
	


	class AtencionCitasController{
	

		public function index($tipo, $idCita){
			//Segun el Tipo de Departamento
			if ($tipo==1) //Psicologia
			{
				$this->Psicologia($idCita);
			}
			else if ($tipo==2) //Odontologia
			{
				$this->Odontologia($idCita);
			}
			else if ($tipo==4) 
			{
				
			}
			else if ($tipo==5) //Estudio SocioEconomico
			{
				$this->ESE($idCita);
			}

			
		}

		public function Psicologia($idCita)
		{ 

			$consulta=new GestionCitas();
			$cita=$consulta->get_Citas_show($idCita);	
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($cita["IdEstudiante"], $_SESSION['ID']);

			$consulta= new Informes();
			$seguimientos=$consulta->get_Informes_Psicologico_list($cita["IdEstudiante"]);

			require_once("../views/atencioncitas/psicologia/identificacion.php");
			require_once("../views/atencioncitas/psicologia/diagnostico.php");
			
		}

		public function Odontologia($idCita)
		{ 
			$consulta=new GestionCitas();
			$cita=$consulta->get_Citas_show($idCita);	
			$consulta= new estudiantes();
			$estudiante=$consulta->get_Estudiantes_Show($cita["IdEstudiante"], $_SESSION['ID']);
			require_once("../views/atencioncitas/odontologia/identificacion.php");

			$consulta= new EncuestaOdontologica();
			$EncOdont=$consulta->set_EncuestaOdontologica_show($cita["IdCitas"], $_SESSION['ID']);

			$consulta= new EncuestOdontPregu();
			$PreguntasOdontologicas=$consulta->set_EncuestOdontPregu_show($cita["IdCitas"]);
			$mitad=round(count($PreguntasOdontologicas)/2);

			$consulta= new InformeOdontologico();
			$seguimientos=$consulta->set_InformeOdontologico_list($cita["IdEstudiante"]);

			
			require_once("../views/atencioncitas/odontologia/encuestaodontologica.php");
			require_once("../views/atencioncitas/odontologia/diagnostico.php");

		}

		public function DBDE()//Division de Bienestar y Desarrollo Estudiantil
		{ 

		}
		public function ESE($idCita)//Estudio SocioEconomico
		{ 
			$consulta=new EstudiantesEncuestados();
			$encuesta=$consulta->get_EstudiantesEncuestados($idCita);		

			if (count($encuesta)>0) {
				
				$consulta= new estudiantes();
				$estudiante=$consulta->get_Estudiantes_Show($encuesta["estudiante"], $_SESSION['ID']);

				$consulta= new DatosAcadEst();
				$datosacademicos=$consulta->get_datos_Academicos($encuesta["estudiante"])[0];

				$consulta= new estudiantes();
				$estudiante["creditos"]=$consulta->get_Estudiantes_IndiceAcademico($encuesta["estudiante"]);

				$consulta= new estudiantes();
				$notas=$consulta->get_Estudiantes_Notas($encuesta["estudiante"], $_SESSION['ID']);

				require_once("../views/atencioncitas/EstudioSocioAcademico/identificacion.php");
				require_once("../views/atencioncitas/EstudioSocioAcademico/area_academica.php");

				
				$consulta= new AyudaEconomica();
				$ayudaseconomica=$consulta->get_ayudaseconomica();

				
				$consulta= new Egreso();
				$egresos=$consulta->get_egreso();

				$consulta= new DatosSocEcon();
				$datossocioeco=$consulta->get_DatosSocEco_Est($encuesta["socioeconomicos"]);



				$consulta= new AyudaEconxEstudiante();
				$ayudaseconomica=$consulta->get_AyudaEconxEstudiante($encuesta["Id"]);
				if (count($ayudaseconomica)>0) {
					$datossocioeco["ayudaseconomica"]="SI";
				}
				else{
					$datossocioeco["ayudaseconomica"]="NO";
				}

				$consulta= new GrupoFamiliarEst();
				$familiares=$consulta->get_GrupoFamiliarEst_index($encuesta["estudiante"]);
				$estudiante=$encuesta["estudiante"];

				

				$consulta= new GastosxEstudiante();
				$gastosindividuales=$consulta->get_GrupoFamiliarEst_gastosindividuales($encuesta["Id"]);


				$consulta= new GastosxEstudiante();
				$gastosfamiliares=$consulta->get_GrupoFamiliarEst_gastosfamiliares($encuesta["Id"]);

				require_once("../views/atencioncitas/EstudioSocioAcademico/socioeconomica.php");

				$consulta= new DatosFisAmbEst();
				$datosambientales=$consulta->get_DatosFisAmbEst_edit($encuesta["ambientales"]);

				$consulta= new Estados();
				$estados=$consulta->get_estados();

				$consulta= new Municipios();
				$municipios=$consulta->get_municipios($datosambientales["id_estado"]);

				$consulta= new Parroquias();
				$parroquias=$consulta->get_parroquias($datosambientales["id_municipio"]);

				$consulta= new Ciudades();
				$ciudades=$consulta->get_ciudades($datosambientales["id_estado"]);
				
				$consulta= new TipoVivienda();
				$tpviviendas=$consulta->get_TipoVivienda();

				$consulta= new TeneciaVivienda();
				$tenecviv=$consulta->get_TeneciaVivienda();

				$consulta= new TiposPared();
				$tpparedes=$consulta->get_TiposPared();

				$consulta= new TiposPiso();
				$tppisos=$consulta->get_TiposPiso();

				$consulta= new TiposTecho();
				$tptechos=$consulta->get_TiposTecho();

				require_once("../views/atencioncitas/EstudioSocioAcademico/datosambientales.php");


				$consulta= new ActTiempoLibre();
				$actividades=$consulta->get_ActTiempoLibre();
				$consulta= new CategDeportes();
				$deportes=$consulta->get_CategDeportes();

				$consulta= new HabilidadesEst();
				$habilidades=$consulta->get_HabilidadesEst();

				$consulta= new EventosIUTOMS();
				$eventos=$consulta->get_EventosIUTOMS();

				$consulta= new ActivxEst();
				$actividadesest=$consulta->get_ActivxEst_edit($encuesta["Id"]);
				$consulta= new DeportesxEstudiante();
				$deportesest=$consulta->get_DeportesxEstudiante_edit($encuesta["Id"]);
				$consulta= new HabilidadesxEstudi();
				$habilidadesest=$consulta->get_HabilidadesxEstudi_edit($encuesta["Id"]);
				$consulta= new EventosxEstud();
				$eventosest=$consulta->get_EventosxEstud_edit($encuesta["Id"]);	

				require_once("../views/atencioncitas/EstudioSocioAcademico/datosculturales.php");


				$consulta= new DatInterEst();
				$datosinteressocioeconomico=$consulta->get_DatInterEst_edit($encuesta["interesAyu"]);

				$consulta= new TipoBeca();
				$becas=$consulta->get_TipoBeca();

				require_once("../views/atencioncitas/EstudioSocioAcademico/datosinteresAyu.php");

				$consulta= new SinceracionBecas();
				$becasdisp=$consulta->get_SinceracionBecas_Inscripciones();	
				require_once("../views/atencioncitas/EstudioSocioAcademico/diagnostico.php");


			}
			else{
				echo "El estudiante no ha realizado la encuesta, es necesario que el estudiante realize la encuesta para realizar la entrevista";
			}

		}

		public function AtencionCitas(){

			$informe["TipoInforme"]=$_POST["TipoInforme"];
			$informe["CodInforme"]=$_POST["IdCita"];
			$informe["Estudiante"]=$_POST["Estudiante"];
			$informe["Responsable"]=$_SESSION['ID'];
			return $this->informe($informe);
		}

		private function informe($informe){ 
			$informe["IdCitas"]=$informe["CodInforme"];	
			$informe["IdEstudio"]=$informe["CodInforme"];
			$informe["Asistencia"]=2;	
			$informe["Estudiante"];
			$informe["TipoInforme"];
			$informe["Responsable"];
			if (isset($_POST["Observaciones"])) {
				$informe["Observaciones"]=$_POST["Observaciones"];
			}
			
			$consulta= new Informes();
			$consulta->set_Informes_store($informe);
			//Segun el Tipo de Informe
			if ($informe["TipoInforme"]==4) {//Estudio SocioEconomico
				$consulta= new EstudioSocioeconomico();
				$consulta->set_EstudioSocioeconomico_store($informe);
				if ($informe["agregarProg"]==1) {
					$consulta= new Becarios();
					$informe["Becario"]=$consulta->set_Becarios_store($informe);
					if (is_numeric($informe["Becario"])) {	

						$consulta= new Inscripcionesbecarios();
						$consulta->set_Inscripciones_store($informe);

					}	
					$consulta= new GestionCitas();
					return $consulta->set_GestionCitas_Atender($informe);
					
				}
			}
			else if ($informe["TipoInforme"]==3) {//Odontologico 
				$informe["AspPac"]=$_POST["AspPac"];
				if (isset($_POST["examen"])) {
					$informe["examen"]=$_POST["examen"];					
				}			
				else{
					$informe["examen"]='';	
				}						
				$informe["Cara"]=$_POST["Cara"];
				$informe["LabyCom"]=$_POST["LabyCom"];			
				$informe["PalpaGangl"]=$_POST["PalpaGangl"];
				$informe["Ganglios"]=$_POST["Ganglios"];			
				$informe["ATM"]=$_POST["ATM"];
				$informe["Ojeras"]=$_POST["Ojeras"];			
				$informe["RegHiotTiroidea"]=$_POST["RegHiotTiroidea"];
				$informe["Carrillos"]=$_POST["Carrillos"];			
				$informe["Mucosa"]=$_POST["Mucosa"];
				$informe["Encia"]=$_POST["Encia"];			
				$informe["Lengua"]=$_POST["Lengua"];
				$informe["Paladar"]=$_POST["Paladar"];			
				$informe["Laboratorio"]=$_POST["Laboratorio"];
				$informe["Modelo"]=$_POST["Modelo"];			
				$informe["TensionArterial"]=$_POST["TensionArterial"];
				$informe["Observaciones"]=$_POST["Observaciones"];			
				$informe["Diagnostico"]=$_POST["Diagnostico"];
				$informe["Tratamiento"]=$_POST["Tratamiento"];			
				$informe["DescrTrat"]=$_POST["DescrTrat"];
				$informe["odontograma"]=$_POST["odontograma"];
				$consulta= new InformeOdontologico();
				$result=$consulta->set_InformeOdontologico_store($informe);
				$consulta= new GestionCitas();
				echo $consulta->set_GestionCitas_Atender($informe);



			}			
			else if ($informe["TipoInforme"]==1) {//Psicologico
				$consulta= new GestionCitas();
				echo $consulta->set_GestionCitas_Atender($informe);
			}

		}



	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new AtencionCitasController;			
		$rs = $conectar->index($_GET["tipo"], $_GET["id"]);
	}
	else if ($accion == 'AtencionCitas')
	{
	 	$conectar = new AtencionCitasController;			
		$rs = $conectar->AtencionCitas();
	}
	else if ($accion == 'show')
	{
	 	$conectar = new AtencionCitasController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	
	else if ($accion == 'store')
	{
	 	$conectar = new AtencionCitasController;			
		$rs = $conectar->store();
		echo $rs;
	}
	
	

	


?>