<?php 
	session_start();
	require_once("../Modelo/Departamentos.php");
	require_once("../Modelo/Usuarios.php");
	require_once("../Modelo/EnviarEmail.php");
 
	class PersonasController{

		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']!='265') {
				die("Acceso Denegado :)");
			}
		}

		/**
 		 * página de inicio en Personas
 		*/
		public function index(){
			$consulta= new Usuarios();
			$usuarios=$consulta->get_Usuarios_index($_SESSION['ID']);
			require_once("../views/personas/index.php");
		}		

		/**
 		* Formulario para crear un nueva Persona
 		*/
		public function create(){ 	
			require_once("../views/personas/new.php");
		}

		/**
 		* Metodo POST para registrar una Persona
 		* Guarda la foto de la persona, registra la persona y le envia un correo informandole el ingreso
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function store(){

			$usuario["cedula"]=$_POST["Cedula_Usu"];
			$usuario["sexo"]=$_POST["Sexo_Usu"];
			$usuario["nombre"]=$_POST["Nombre_1"];
			$usuario["nombre2"]=$_POST["Nombre_2"];
			$usuario["apellido"]=$_POST["Apellido_1"];
			$usuario["apellido2"]=$_POST["Apellido_2"];
			$usuario["correo"]=$_POST["Correo_Usu"];
			$usuario["telefono"]=$_POST["Telf_Usu"];
			$usuario["celular"]=$_POST["Cel_Usu"];
			
			if (isset($_FILES['Foto_Usuario']['tmp_name'])) {
				$usuario["Foto_Usuario"]=$usuario["cedula"]."_".date('d-m-y').".".explode(".", $_FILES["Foto_Usuario"]["name"])[0];
				$ruta = "../web/images/fotos/".$usuario["Foto_Usuario"]; 
        		move_uploaded_file($_FILES["Foto_Usuario"]["tmp_name"], $ruta); 
				
			}
			else{
				$usuario["Foto_Usuario"]='';
			}

			$usuario["Responsable"]=$_SESSION['ID'];
			$consulta= new Usuarios();
			$result= $consulta->set_Usuarios_store($usuario);
			if ($result==1) {
				$body="<h2>Bienvenido ".$usuario["nombre"]." ".$usuario["apellido"]." al Sistema para la División de Bienestar y Desarrollo Estudiantil</h2> <br><br>
			 <p>Brevemente sera asignado a un departamento de la división</p><br><br><p><b>Importante!</b> para ingresar al sistema ingrese al <a href='http://localhost/SGIDBDE'> enlace </a>
			  con su numero de cedula y como contraseña inicial su numero de cedula.</p><br><br><b>Le recomendamos que cuando ingrese al sistema modifique su contraseña</b>";
			$consulta= new EnviarEmail();
			$consulta->enviarEmail( $usuario["correo"], $body, 'Registro de Usuario' );
			}
			return $result;
			
		}

		public function show($id){


		}

		/**
 		* Consulta el registro y genera el Formulario de personas
 		* @param [integer] $id
 		*/
		public function edit($id){

			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_edit($id);
			require_once("../views/personas/edit.php");

		}


		/**
 		* Metodo POST para modificar una Persona
 		* Modifica la foto de la persona y modifica la persona en BD 
 		* @return [integer] 1 || [string] Mensaje de Error 
 		*/
		public function update(){
			
			$usuario["Num_Usuario"]=$_POST["Num_Usuario"];
			$consulta= new Usuarios();
			$persona=$consulta->get_Usuarios_edit($usuario["Num_Usuario"]);

			$usuario["cedula"]=$_POST["Cedula_Usu"];
			$usuario["sexo"]=$_POST["Sexo_Usu"];
			$usuario["nombre"]=$_POST["Nombre_1"];
			$usuario["nombre2"]=$_POST["Nombre_2"];
			$usuario["apellido"]=$_POST["Apellido_1"];
			$usuario["apellido2"]=$_POST["Apellido_2"];
			$usuario["correo"]=$_POST["Correo_Usu"];
			$usuario["telefono"]=$_POST["Telf_Usu"];
			$usuario["celular"]=$_POST["Cel_Usu"];
			$usuario["Responsable"]=$_SESSION['ID'];
			if (!empty($_FILES['Foto_Usuario']['tmp_name'])) {
				
				$ruta = "../web/images/fotos/"; 
				if (!empty($persona["Foto_Usuario"])) {

					
					if (file_exists($ruta.$persona["Foto_Usuario"])) {
			          unlink($ruta.$persona["Foto_Usuario"]);
			        }
				}

				$usuario["Foto_Usuario"]=$usuario["cedula"]."_".date('d-m-y').".".explode(".", $_FILES["Foto_Usuario"]["name"])[0];
				$ruta = "../web/images/fotos/".$usuario["Foto_Usuario"]; 
        		move_uploaded_file($_FILES["Foto_Usuario"]["tmp_name"], $ruta); 
				
			}
			else{
				$usuario["Foto_Usuario"]=$persona["Foto_Usuario"];
			}
			

			$consulta= new Usuarios();
			return $consulta->set_Usuarios_update($usuario);
		}

		
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}

	if ($accion == 'index'){
		$conectar = new PersonasController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'create')
	{
	 	$conectar = new PersonasController;			
		$rs = $conectar->create();
	}
	else if ($accion == 'store')
	{
	 	$conectar = new PersonasController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new PersonasController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new PersonasController;			
		$rs = $conectar->update();
		echo $rs;
	}


	


?>