<?php 
	session_start();
	require_once("../Modelo/Trabajadores.php");
	require_once("../Modelo/Referenciales/act_tiempo_libre.php");
	require_once("../Modelo/Referenciales/ayuda_economica.php");
	require_once("../Modelo/Referenciales/categorias_deportes.php");
	require_once("../Modelo/Referenciales/egreso.php");
	require_once("../Modelo/Referenciales/enfermedades.php");
	require_once("../Modelo/Referenciales/estado_civil.php");
	require_once("../Modelo/Referenciales/eventos_iutoms.php");
	require_once("../Modelo/Referenciales/grado_instruccion.php");
	require_once("../Modelo/Referenciales/habilidades_est.php");
	require_once("../Modelo/Referenciales/menciones_bach.php");
	require_once("../Modelo/Referenciales/parentescos.php");
	require_once("../Modelo/Referenciales/prof_ocup.php");
	require_once("../Modelo/Referenciales/tenecia_vivienda.php");
	require_once("../Modelo/Referenciales/tipo_vivienda.php");
	require_once("../Modelo/Referenciales/tipos_pared.php");
	require_once("../Modelo/Referenciales/tipos_piso.php");
	require_once("../Modelo/Referenciales/tipos_techo.php");
	require_once("../Modelo/Referenciales/enfermedades.php");
	require_once("../Modelo/Referenciales/tratamientos.php");
	require_once("../Modelo/Referenciales/preguntas_odontologicas.php");
	




	class TablasReferencialesController{
	
		private $categorias=array( 			
			array("nro"=>"2", "categoria"=>'Referentes a Odontologia'), 
			array("nro"=>"3", "categoria"=>'Referentes a Salud'),
			array("nro"=>"4", "categoria"=>'Referentes a datos Estudiantiles', "asociado"=>"2,3,5"),
			array("nro"=>"5", "categoria"=>'Referentes a Estudios Socioeconómicos', "asociado"=>"4")	
			);


		private $tables=array( 

			array("nro"=>"1", "categoria"=>"4", "tabla"=>'Actividades del Tiempo Libre', "clase"=> 'ActTiempoLibre', "index"=>'get_ActTiempoLibre',  "store"=>'set_ActTiempoLibre_store', "edit"=>'get_ActTiempoLibre_edit', "update"=>'set_ActTiempoLibre_update', "destroy"=>'set_ActTiempoLibre_destroy'), 

			array("nro"=>"2", "categoria"=>"5", "tabla"=>'Tipos de Ayudas Sociecómica', "clase"=>'AyudaEconomica', "index"=>'get_ayudaseconomica', "store"=>'set_ayudaseconomica_store', "edit"=>'get_ayudaseconomica_edit', "update"=>'set_ayudaseconomica_update', "destroy"=>'set_ayudaseconomica_destroy'),

			array("nro"=>"3", "categoria"=>"4", "tabla"=>'Deportes', "clase"=> 'CategDeportes', "index"=>'get_CategDeportes',  "store"=>'set_CategDeportes_store', "edit"=>'get_CategDeportes_edit', "update"=>'set_CategDeportes_update', "destroy"=>'set_CategDeportes_destroy'),

			array("nro"=>"4", "categoria"=>"5", "tabla"=>'Tipos de Egresos', "clase"=> 'Egreso', "index"=>'get_egreso',  "store"=>'set_egreso_store', "edit"=>'get_egreso_edit', "update"=>'set_egreso_update', "destroy"=>'set_egreso_destroy'),

			array("nro"=>"5", "categoria"=>"4", "tabla"=>'Estados Civiles', "clase"=> 'EstadoCivil', "index"=>'get_EstadoCivil',  "store"=>'set_EstadoCivil_store', "edit"=>'get_EstadoCivil_edit', "update"=>'set_EstadoCivil_update', "destroy"=>'set_EstadoCivil_destroy'),

			array("nro"=>"6", "categoria"=>"4", "tabla"=>'Eventos IUTOMS', "clase"=> 'EventosIUTOMS', "index"=>'get_EventosIUTOMS',  "store"=>'set_EventosIUTOMS_store', "edit"=>'get_EventosIUTOMS_edit', "update"=>'set_EventosIUTOMS_update', "destroy"=>'set_EventosIUTOMS_destroy'),

			array("nro"=>"7", "categoria"=>"5", "tabla"=>'Grado Instrucción (Grup Familiar)', "clase"=> 'GradoInstruccion', "index"=>'get_GradoInstruccion',  "store"=>'set_GradoInstruccion_store', "edit"=>'get_GradoInstruccion_edit', "update"=>'set_GradoInstruccion_update', "destroy"=>'set_GradoInstruccion_destroy'),

			array("nro"=>"8", "categoria"=>"4", "tabla"=>'Habilidades Estudiantiles', "clase"=> 'HabilidadesEst', "index"=>'get_HabilidadesEst',  "store"=>'set_HabilidadesEst_store', "edit"=>'get_HabilidadesEst_edit', "update"=>'set_HabilidadesEst_update', "destroy"=>'set_HabilidadesEst_destroy'),

			array("nro"=>"9", "categoria"=>"4", "tabla"=>'Menciones de Bachiller', "clase"=> 'MencionesBach', "index"=>'get_MencionesBach',  "store"=>'set_MencionesBach_store', "edit"=>'get_MencionesBach_edit', "update"=>'set_MencionesBach_update', "destroy"=>'set_MencionesBach_destroy'),

			array("nro"=>"10", "categoria"=>"5", "tabla"=>'Parentesco Familiares', "clase"=> 'Parentescos', "index"=>'get_Parentescos',  "store"=>'set_Parentescos_store', "edit"=>'get_Parentescos_edit', "update"=>'set_Parentescos_update', "destroy"=>'set_Parentescos_destroy'),

			array("nro"=>"11", "categoria"=>"5", "tabla"=>'Profeciones u Oficios (Grup Familiar)', "clase"=> 'ProfOcup', "index"=>'get_ProfOcup',  "store"=>'set_ProfOcup_store', "edit"=>'get_ProfOcup_edit', "update"=>'set_ProfOcup_update', "destroy"=>'set_ProfOcup_destroy'),

			array("nro"=>"12", "categoria"=>"5", "tabla"=>'Tipos Tenencias de la vivienda', "clase"=> 'TeneciaVivienda', "index"=>'get_TeneciaVivienda',  "store"=>'set_TeneciaVivienda_store', "edit"=>'get_TeneciaVivienda_edit', "update"=>'set_TeneciaVivienda_update', "destroy"=>'set_TeneciaVivienda_destroy'),

			array("nro"=>"13", "categoria"=>"5", "tabla"=>'Tipos de viviendas', "clase"=> 'TipoVivienda', "index"=>'get_TipoVivienda',  "store"=>'set_TipoVivienda_store', "edit"=>'get_TipoVivienda_edit', "update"=>'set_TipoVivienda_update', "destroy"=>'set_TipoVivienda_destroy'),

			array("nro"=>"14", "categoria"=>"5", "tabla"=>'Tipos de paredes', "clase"=> 'TiposPared', "index"=>'get_TiposPared',  "store"=>'set_TiposPared_store', "edit"=>'get_TiposPared_edit', "update"=>'set_TiposPared_update', "destroy"=>'set_TiposPared_destroy'),

			array("nro"=>"15", "categoria"=>"5", "tabla"=>'Tipos de pisos', "clase"=> 'TiposPiso', "index"=>'get_TiposPiso',  "store"=>'set_TiposPiso_store', "edit"=>'get_TiposPiso_edit', "update"=>'set_TiposPiso_update', "destroy"=>'set_TiposPiso_destroy'),

			array("nro"=>"16", "categoria"=>"5", "tabla"=>'Tipos de techos', "clase"=> 'TiposTecho', "index"=>'get_TiposTecho',  "store"=>'set_TiposTecho_store', "edit"=>'get_TiposTecho_edit', "update"=>'set_TiposTecho_update', "destroy"=>'set_TiposTecho_destroy'),

			array("nro"=>"17", "categoria"=>"3", "tabla"=>'Enfermedades', "clase"=> 'Enfermedades', "index"=>'get_Enfermedades',  "store"=>'set_Enfermedades_store_tb', "edit"=>'get_Enfermedades_edit', "update"=>'set_Enfermedades_update', "destroy"=>'set_Enfermedades_destroy'),

			array("nro"=>"18", "categoria"=>"3", "tabla"=>'Tratamientos', "clase"=> 'tratamientos', "index"=>'get_tratamientos',  "store"=>'set_tratamientos_store_tb', "edit"=>'get_tratamientos_edit', "update"=>'set_tratamientos_update', "destroy"=>'set_tratamientos_destroy'),

			array("nro"=>"19", "categoria"=>"2", "tabla"=>'Preguntas Odontologicas', "clase"=> 'PreguntasOdontologicas', "index"=>'get_PreguntasOdontologicas',  "store"=>'set_PreguntasOdontologicas_store', "edit"=>'get_PreguntasOdontologicas_edit', "update"=>'set_PreguntasOdontologicas_update', "destroy"=>'set_PreguntasOdontologicas_destroy')
			 
		);

		public $catmostrar=array();
		public $tabmostrar=array();


		public function __construct()
		{
			if ($_SESSION['Tipo_Usuario']=='264') {
				//consulto los departamenos asociados al usuario que es jefe de departamento
				$consulta= new Trabajadores();
				$depto=$consulta->get_Trabajadores_Jefe_Depto($_SESSION['ID']);
				

				foreach ($depto as $departamento) {//recorrido de los departamentos que es jefe de departamento el usuario
					foreach ($this->categorias as $categoria) { //recorrido de las categorias registradas

						if ($departamento["nrotipodepto"]==$categoria["nro"]) { //si el nro del array es igual al nro de tipo de departamento

							if (isset($categoria["asociado"])) {//si la categoria poseea asociado

								$porcion=explode(",", $categoria["asociado"]);//divido el asociado
								$break=0;
								for ($i=0; $i <count($porcion) ; $i++) { 
									$break=0;
									foreach ($this->catmostrar as $cat) {//recorro el array que mostrare al usuario
										if ($cat["nro"]==$porcion[$i]) {//si ya se encuentra en el array
											$break=1;
											break;
										}
									}
									if ($break==0) {
										foreach ($this->categorias as $cat) {//si no se encontro la categoria, se busca y se agrega al array que mostrare al usuario
											if ($porcion[$i]==$cat["nro"]) {
												$this->catmostrar[]=$cat;
												$this->agrTablas($cat["nro"]);
											}
										}										
									}
								}
							}

							$break=0;
							foreach ($this->catmostrar as $cat) {//recorro el array que mostrare al usuario
								if ($cat["nro"]==$categoria["nro"]) {//si ya se encuentra en el array
									$break=1;
									break;
								}
							}
							if ($break==0) {
								$this->catmostrar[]=$categoria;
								$this->agrTablas($categoria["nro"]);
							}

						}
					}

				}
				
			}
			else if ($_SESSION['Tipo_Usuario']=='265') {
				$this->catmostrar=$this->categorias;
				foreach ($this->catmostrar as $cat) {//recorro el array que mostrare al usuario
					$this->agrTablas($cat["nro"]);
				}
			}
		}


		private function agrTablas($categoria){
			foreach ($this->tables as $table) {
				if ($table["categoria"]==$categoria) {
					$this->tabmostrar[]=$table;
				}
			}
		}
		

		public function index(){
			require_once("../views/tablasreferenciales/index.php");
		}

		public function buscar($categoria){
			require_once("../views/tablasreferenciales/tablas.php");
		}

		public function mostrar($tabla){
			$tb=$tabla;
			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}

			if (count($tabla)>0) {
				
				if ($tabla["nro"]==19) {
					$consulta= new $tabla["clase"]();//llamo la clase		
					$tabla=$consulta->$tabla["index"]();//llamo la funcion Index
					require_once("../views/tablasreferenciales/mostrar_preg_odontologicas.php");
				}
				else{
					$consulta= new $tabla["clase"]();//llamo la clase		
					$tabla=$consulta->$tabla["index"]();//llamo la funcion Index
					require_once("../views/tablasreferenciales/mostrar.php");
				}
				
			}
			
		}


		public function create($tabla){ 

			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}

			if ($tabla["nro"]==19) {
				require_once("../views/tablasreferenciales/new_preg_odontologicas.php");
			}
			else{
				require_once("../views/tablasreferenciales/new.php");
			}
			
		}

		public function store(){
			$tabla=$_POST["tabla"];
			$dato=$_POST["dato"];
			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}

			if ($tabla["nro"]==19) {
				$array["Descripcion"]=$dato;
				$array["tiporesp"]=$_POST["tiporesp"];
				$dato=$array;
			}


			$consulta= new $tabla["clase"]();//llamo la clase		
			return $tabla=$consulta->$tabla["store"]($dato);//llamo la funcion store
			
		}


		public function edit($id){//modificar		
			$porcion=explode(",", $id);//divido el asociado
			$id=$porcion[0];
			$tabla=$porcion[1];
			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}

			$consulta= new $tabla["clase"]();//llamo la clase		
			$dato=$consulta->$tabla["edit"]($id);//llamo la funcion edit
			require_once("../views/tablasreferenciales/edit.php");

		}

		public function update(){
			$tabla=$_POST["tabla"];
			$dato["dato"]=$_POST["dato"];
			$dato["id"]=$_POST["id"];

			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}

			$consulta= new $tabla["clase"]();//llamo la clase		
			return $tabla=$consulta->$tabla["update"]($dato);//llamo la funcion update
			
		}

		public function destroy($id){

			$porcion=explode(",", $id);//divido el asociado
			$id=$porcion[0];
			$tabla=$porcion[1];
			foreach ($this->tables as $table) {
				if ($table["nro"]==$tabla) {
					$tabla=$table;
				}
			}
			$consulta= new $tabla["clase"]();//llamo la clase		
			return $tabla=$consulta->$tabla["destroy"]($id);//llamo la funcion destroy
		}
	}

	
	if (isset($_GET["accion"])) {
		$accion = $_GET["accion"];
	}
	else if(isset($_POST["accion"])){
		$accion = $_POST["accion"];
	}
	else{
		$accion = 'index';
	}


	if ($accion == 'index'){
		$conectar = new TablasReferencialesController;			
		$rs = $conectar->index();
	}
	else if ($accion == 'buscar')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->buscar($_GET["categoria"]);
	}
	else if ($accion == 'mostrar')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->mostrar($_GET["tabla"]);
	}
	
	else if ($accion == 'create')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->create($_GET["tabla"]);
	}
	else if ($accion == 'store')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->store();
		echo $rs;
	}
	else if ($accion == 'show')
	{
	 	$conectar = new TablasReferencialesController;	
		$rs = $conectar->show($_GET["id"]);	
	}
	else if ($accion == 'edit')
	{
	 	$conectar = new TablasReferencialesController;	
		$rs = $conectar->edit($_GET["id"]);	
	}
	else if ($accion == 'update')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->update();
		echo $rs;
	}
	else if ($accion == 'destroy')
	{
	 	$conectar = new TablasReferencialesController;			
		$rs = $conectar->destroy($_GET["id"]);
		echo $rs;
	}

	


?>