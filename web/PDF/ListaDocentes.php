<?php
	ob_start();
	include '../php/conectar.php';
$datosinst="SELECT CODIGO_DEA, Nombredelainst, Direccioncompleta, Direcciongeneral, Parroquia from DatosdelaInstitucion";
$resul=mysqli_query($conn, $datosinst);
$inst=mysqli_fetch_assoc($resul);
$director="SELECT PerfixUsuar.cedula, Personas.nombres, Personas.apellidos, Personas.sexo, nacionalidad.descripcion as nacionalidad, Emailxpersona.email from PerfixUsuar, Personas, nacionalidad, Emailxpersona where perfil=1  and PerfixUsuar.cedula=Personas.cedula and PerfixUsuar.cedula=Emailxpersona.cedula and Personas.nacionalidad=nacionalidad.cod_nacionalidad";
$resul=mysqli_query($conn, $director);
$dir=mysqli_fetch_assoc($resul);
?>

<page backtop="10mm" backbottom="10mm" backleft="5mm" backright="5mm">

    <page_header>
<div align="center">
<img src="../img/encabezadoplanilla2.png" style="width:750px; height:70px;"> 
</div>
    </page_header>
<br>
<br>
<br>
<br>
<div align="center">
<?php
echo $inst["Nombredelainst"];
?><br>
<?php echo $inst["Parroquia"]?><br>
CÓDIGO DEA: <?php echo $CODIGO_DEA=$inst["CODIGO_DEA"]?>     
</div>                                                                                                 
<br>
<br>
<br>
		<h2 align="center">Lista de Docentes</h2><br><br>
		<div align="center" style="width:750px;">

		<?php
			$sql = "SELECT docentes.ceduladoc as cedula, formaciones.descripcion as formacion, Personas.nombres, Personas.apellidos  FROM  docentes, Personas, formaciones  where docentes.ceduladoc=Personas.cedula and docentes.formacion=formaciones.Nro and docentes.activo=1";
			$result = mysqli_query($conn, $sql);
			if (mysqli_num_rows($result) == 0) {
	 			echo "<br><br><br><br>"."<b>*** NO HAY DATOS REGISTRADOS ***</b>"."<br><br><br><br><br>";
			}
			if (mysqli_num_rows($result) > 0) {

			echo '<table id="tabladocentes" class="display" cellspacing="0" width="100%" border="1">
			<thead>
			<tr>
				<th></th>
				<th>Cédula</th>
				<th>Nombres</th>
				<th>Apellidos</th>
				<th>Formación</th>
				<th>Telefono</th>
				<th>Celular</th>
				<th>Correo</th>
			</tr>
			</thead>';

    			// salida de datos por cada registro
    			while($fila = mysqli_fetch_assoc($result)) {
    			$cedula=$fila["cedula"];
    			
				
			echo '<tr>
				<td align="right">';
				
				if ($fila["cedula"] <= 999) {
				echo ($fila["cedula"]);
				}
				if ($fila["cedula"] >= 1000 and $fila["cedula"] <= 9999) {
				echo substr($fila["cedula"],0,1).".".substr($fila["cedula"],1,3);
				}
				if ($fila["cedula"] >= 10000 and $fila["cedula"] <= 99999) {
				echo substr($fila["cedula"],0,2).".".substr($fila["cedula"],2,3);
				} 
				if ($fila["cedula"] >= 100000 and $fila["cedula"] <= 999999) {
				echo substr($fila["cedula"],0,3).".".substr($fila["cedula"],3,3);
				}
				if ($fila["cedula"] >= 1000000 and $fila["cedula"] <= 9999999) {
				echo substr($fila["cedula"],0,1).".".substr($fila["cedula"],1,3).".".substr($fila["cedula"],4,3);
				}
				if ($fila["cedula"] >= 10000000 and $fila["cedula"] <= 99999999) {
				echo substr($fila["cedula"],0,2).".".substr($fila["cedula"],2,3).".".substr($fila["cedula"],5,3);
				}

				echo '</td>
				<td>' . $fila["nombres"] . '</td>
				<td>' . $fila["apellidos"] . '</td>
				<td>' . $fila["formacion"] . '</td>
				'; 
				$cons="SELECT cod_telf, telefono from Telefonosxpersona where cedula=$cedula and cod_telf=212";
				$resul=mysqli_query($conn, $cons);
				if (mysqli_num_rows($resul)>0) {
					$cel=mysqli_fetch_assoc($resul);
					echo '<td> ('.$cel["cod_telf"].') '.$cel["telefono"].'</td> ';
				}
				else{
					echo '<td></td>';
				}
				$cons="SELECT cod_telf, telefono from Telefonosxpersona where cedula=$cedula and cod_telf!=212 limit 1";
				$resul=mysqli_query($conn, $cons);
				if (mysqli_num_rows($resul)>0) {
					$cel=mysqli_fetch_assoc($resul);
					echo '<td> ('.$cel["cod_telf"].') '.$cel["telefono"].'</td> ';
				}
				else{
					echo '<td></td>';
				}
				
				
				$consl="SELECT email from Emailxpersona where cedula=$cedula";
				$resu=mysqli_query($conn, $consl);
				$email=mysqli_fetch_assoc($resu);
				echo '<td>' . $email["email"] . '</td></tr>';
			 }	
			echo '</table>
			<br>';
   			}
		?>
   			<page_footer>
<div style="width:750px;" align="center">
<h3 align="center"><i>AÑO 2015: “200 AÑOS DE LA CARTA DE JAMAICA”.</i></h3></div>
<div style="width:750px;" align="center">
<?php echo $inst["Direccioncompleta"]?> Telf. 
<?php 
$telefono="SELECT cod_telf, telefono from telefonosporinstitucion where CODIGO_DEA='$CODIGO_DEA'";
$resul=mysqli_query($conn, $telefono);
$tota=mysqli_num_rows($resul);
$i=1;
while ($tlf=mysqli_fetch_assoc($resul)) {
		if ($i==1) {
			echo "(".$tlf["cod_telf"].") ".$tlf["telefono"];	
				
		}
		else if ($i==$tota) {
			echo "  / (".$tlf["cod_telf"].") ".$tlf["telefono"];	
					
		}
		else{
			echo " / (".$tlf["cod_telf"].") ".$tlf["telefono"];	
			
		}
		$i=$i+1;
	}

?> 
<br>Correo: <?php echo $dir["email"];?>       
</div>
</page_footer> 
 
		
		</div>

</page>

<?php

	$content = ob_get_clean();
	require_once('html2pdf/html2pdf.class.php');
	$pdf = new HTML2PDF('P','LETTER','es','UTF-8');
	$pdf->writeHTML($content);
//	$pdf->pdf->IncludeJS('print(TRUE)');
	$pdf->output('Reporte01.pdf');

?>