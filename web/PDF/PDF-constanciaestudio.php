<?php
	ob_start();
?>

<page backtop="10mm" backbottom="10mm" backleft="5mm" backright="5mm">

    <page_header>
<div align="center">
<img src="../img/encabezadoplanilla2.png" style="width:750px; height:70px;"> 
</div>
    </page_header>
<div>
<br>
<br>
<br>
<br>
<div style="width:750px;" align="center">
<?php
$cod_constancia=$_GET['cod_constancia'];
include '../php/conectar.php';
$datosinst="SELECT CODIGO_DEA, Nombredelainst, Direccioncompleta, Direcciongeneral, Parroquia from DatosdelaInstitucion";
$resul=mysqli_query($conn, $datosinst);
$inst=mysqli_fetch_assoc($resul);
$director="SELECT PerfixUsuar.cedula, Personas.nombres, Personas.apellidos, Personas.sexo, nacionalidad.descripcion as nacionalidad, Emailxpersona.email from PerfixUsuar, Personas, nacionalidad, Emailxpersona where perfil=1  and PerfixUsuar.cedula=Personas.cedula and PerfixUsuar.cedula=Emailxpersona.cedula and Personas.nacionalidad=nacionalidad.cod_nacionalidad";
$resul=mysqli_query($conn, $director);
$dir=mysqli_fetch_assoc($resul);

$select="SELECT Personas.nombres, Personas.apellidos, Personas.cedula as cedulaest, codigo,
 DATE_FORMAT (Personas.FechaNam , '%d-%m-%Y') as FechaNam, DATE_FORMAT (Personas.FechaNam , '%Y') as AnoNam, 
 nacionalidad.descripcion as nacionalidad, LugaresdeNac.descripcion as Lugar_nacimiento,
  Estudiantes.cedula, FechaNam, solic_entr_constancias.fecha_solicitud from solic_entr_constancias, Inscripciones, Personas, Estudiantes, LugaresdeNac, nacionalidad 
 where solic_entr_constancias.cod_constanci=$cod_constancia and
solic_entr_constancias.cod_inscripcion=Inscripciones.cod_inscripcion and
Inscripciones.cedulaest=Personas.cedula and Personas.nacionalidad=nacionalidad.cod_nacionalidad and 
Inscripciones.cedulaest=Estudiantes.cedulaest and 
Estudiantes.Lugar_nacimiento=LugaresdeNac.cod_lugnac";
$resul=mysqli_query($conn, $select);
$fila=mysqli_fetch_assoc($resul);
$codigo=$fila["codigo"];
echo $inst["Nombredelainst"];
?><br>
<?php echo $inst["Parroquia"]?><br>
CÓDIGO DEA: <?php echo $CODIGO_DEA=$inst["CODIGO_DEA"]?>                                                                                                      
</div>
<br>
<br>
<br>
<div style="width:750px;">
<h1 align="center">CONSTANCIA DE ESTUDIO</h1>
<br>
<br>
</div>
<div style="width:740px;" align="center">
<br>
<p style="font-size:18px; text-align:justify;">
Quien suscribe, <u><?php 
//funcion para primer nombre y primer apellido 
					$nombres = $dir["nombres"];
			        $segnombre = strstr($nombres," ");
			        $prinombre = str_replace($segnombre, "", $nombres);
			        $apellidos = $dir["apellidos"];
			        $segapellido = strstr($apellidos," ");
			        $priapellido = str_replace($segapellido, "", $apellidos);

 echo $prinombre." ".$priapellido?></u> portador, 
<?php 
if ($dir["sexo"]==2) {
	echo "a";
}?> de la cédula de identidad Nº <u><?php echo $dir["nacionalidad"].$dir["cedula"] ?></u>
Director<?php 
if ($dir["sexo"]==2) {
	echo "a";
}?>  de la <u><?php
echo $inst["Nombredelainst"];
?>
</u> ubicada en la <?php
echo $inst["Direcciongeneral"];
?>, hace constar por medio de la presente que el
(la) niño (a): <u><?php echo $fila["nombres"]." ".$fila["apellidos"]; ;?></u> portador(a) de la Cédula de Identidad o Cédula Escolar
Nº <u><?php
if (!(empty($fila["cedula"]))) {
	echo $fila["nacionalidad"].$fila["cedula"];
}
else{
	echo $fila["cedulaest"];
}
$cedulaest=$fila["cedulaest"];

$anos="SELECT anosescolares.Nro, anosescolares.descripcionnumerica, definicionomina.periodoescolar from Inscripciones, docexsec, definicionomina, anosescolares
where Inscripciones.cedulaest=$cedulaest and Inscripciones.anoyseccion=docexsec.Nro and docexsec.Nrodefinicionomina=definicionomina.Nro and
 definicionomina.anoescolar=anosescolares.Nro order by Inscripciones.cod_inscripcion desc limit 1";
$resul=mysqli_query($conn, $anos);
$tota=mysqli_num_rows($resul);
?></u> de <u><?php echo (date('Y')-$fila["AnoNam"])?></u> años de edad, nacido(a) en la Ciudad de <u><?php echo $fila["Lugar_nacimiento"]?></u> de fecha: <u><?php echo $fila["FechaNam"]?></u>
 Regular del plantel y cursa el   <u><?php 
while ($ano=mysqli_fetch_assoc($resul)) {
	for ($i=1; $i<=$tota ; $i++) { 
		if (($ano["Nro"]>3) and ($i!=1)) {
			echo "Nivel de Prescolar, ";
		}
		if ($i==$tota) {
			echo $ano["descripcionnumerica"]." Grado del Nivel de Educación Primaria.";			
		}
		else{
			echo ", ".$ano["descripcionnumerica"];
		}
	}
} ?></u>
Durante el año escolar: <u><?php
$resul=mysqli_query($conn, $anos);
while ($ano=mysqli_fetch_assoc($resul)) {
	for ($i=1; $i<=$tota ; $i++) { 
		if ($i==$tota) {
			echo $ano["periodoescolar"]."-".($ano["periodoescolar"]+1);			
		}
		else{
			echo "/".$ano["periodoescolar"]."-".($ano["periodoescolar"]+1);
		}
	}
}
$fecha=$fila["fecha_solicitud"];
$nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
$dia = date ( 'd', $nuevafecha );
$mes = date ( 'm', $nuevafecha );
$año = date ( 'Y', $nuevafecha );
 ?>
</u> En un horario comprendido desde la 07:00am hasta 03:00pm  
<br>
<br>
Constancia que se expide a petición de la parte interesada en la ciudad de Caracas, a los <u><?php echo $dia ?></u>
días del mes de <u><?php 
switch ($mes) {
	case '1':
		echo "Enero";
		break;
	case '2':
	echo "Febrero";
	break;
	case '3':
	echo "Marzo";
	break;
	case '4':
	echo "Abril";
	break;
	case '5':
		echo "Mayo";
		break;
	case '6':
		echo "Junio";
		break;
	case '7':
		echo "Julio";
		break;
	case '8':
		echo "Agosto";
		break;
	case '9':
		echo "Septiembre";
		break;
	case '10':
		echo "Octubre";
		break;
	case '11':
		echo "Noviembre";
		break;
	case '12':
	echo "Diciembre";
	break;
}





?></u> de <u><?php echo $año ?></u>.<br></p>
</div>
<br>
<br>
<br>
<br>
<br>
<div style="font-size:20px;width:750px;" align="center">
Atentamente.<br>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<div style="font-size:20px;width:750px;">
	<div align="center"	style="font-size:20px; display: inline-block;">
_________________________
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
_______________________<br>
.-Docente-.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
.-Directora-.
	</div>
</div>
<br>
<br>
<br>
<div style="font-size:20px;width:750px;" align="center">
Sello del Plantel<br>
</div>
<br>
<br>
</div>	
<page_footer>
<div style="width:750px;">
<h3 align="center"><i>AÑO 2015: “200 AÑOS DE LA CARTA DE JAMAICA”.</i></h3></div>
<div style="width:750px;" align="center">
<?php echo $inst["Direccioncompleta"]?> Telf. 
<?php 
$telefono="SELECT cod_telf, telefono from telefonosporinstitucion where CODIGO_DEA='$CODIGO_DEA'";
$resul=mysqli_query($conn, $telefono);
$tota=mysqli_num_rows($resul);
$i=1;
while ($tlf=mysqli_fetch_assoc($resul)) {
		if ($i==1) {
			echo "(".$tlf["cod_telf"].") ".$tlf["telefono"];	
				
		}
		else if ($i==$tota) {
			echo "  / (".$tlf["cod_telf"].") ".$tlf["telefono"];	
					
		}
		else{
			echo " / (".$tlf["cod_telf"].") ".$tlf["telefono"];	
			
		}
		$i=$i+1;
	}

?> 
<br>Correo: <?php echo $dir["email"];?>       
</div>
<div align="left">Codigo de verificación: <?php echo $codigo;?> </div>  
</page_footer> 

</page>
<?php
	$content = ob_get_clean();
	require_once('html2pdf/html2pdf.class.php');
	$html2pdf = new HTML2PDF('P', 'LETTER', 'es', 'UTF-8');
	$html2pdf->writeHTML($content);
	$html2pdf->pdf->SetDisplayMode('fullpage');
	//$pdf->pdf->IncludeJS('print(TRUE)');
	$html2pdf->output('ConstanciaEstudio'.$fila["nombres"].$fila["apellidos"].'.pdf');

?>