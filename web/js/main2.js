
$('#agregar').on('hidden.bs.modal', function (e) {
  $("#contenidomodalagregar").html('');
});
$('#modificar').on('hidden.bs.modal', function (e) {
  $("#contenidomodalmodificar").html('');
});
$('#ver').on('hidden.bs.modal', function (e) {
  $("#contenidomodalver").html('');
});


function principal(){
        $.ajax({
            url: '../Controller/defaultController.php',
            contentType: "charset=utf-8", 
            success:function(data){

               $("#body").html(data);
            }
        })
}
    
function redireccion(valor){
        $.ajax({
            url: "modulos/"+valor+".php",
            contentType: "charset=utf-8", 
            success:function(data){

               $("#contenido").html(data);
            }
        })
}


function VerBecarios(url, id){
    var modificar="show";
    datos={"id":id, "accion":modificar};
    $.ajax({
        url: url,
        contentType: "charset=utf-8", 
        data:datos,
        type : 'GET',
        success:function(data){
            $("#contenido").html(data);
           
        }
    })
}

function redireccionar(valor){
    $.ajax({
        url: valor,
        contentType: "charset=utf-8", 
        success:function(data){

           $("#encuesta").html(data);
        }
    })
}

function nuevo(valor){
    
    $.ajax({
        url: valor,
        contentType: "charset=utf-8", 
        success:function(data){
        $("#contenidomodalagregar").html(data);
           $('#agregar').modal('toggle');
        }
    })
}

function cancelarcita(url, fecha){
    $.ajax({
        url: url,
        data:{'fecha':fecha},
        contentType: "charset=utf-8", 
        success:function(data){
        $("#contenidomodalagregar").html(data);
        $('#agregar').modal('toggle');
        }
    })
}

function modificar(url, id){
    var modificar="edit";
    datos={"id":id, "accion":modificar};
    $.ajax({
            url: url,
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){
            $("#contenidomodalmodificar").html(data);
            $('#modificar').modal('toggle');
               
            }
        })
}

function horarios(url, id){
    var modificar="create";
    datos={"id":id, "accion":modificar};
    $.ajax({
            url: url,
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){
            $("#contenidomodalmodificar").html(data);
            $('#modificar').modal('toggle');
               
            }
        })
}




function ver(url, id){
    var modificar="show";
    datos={"id":id, "accion":modificar};
    $.ajax({
            url: url,
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){
            $("#contenidomodalver").html(data);
            $('#ver').modal('toggle');
               

            }
        })
}

function eliminar(url, id){
    var modificar="destroy";
    datos={"id":id, "accion":modificar};
    $.ajax({
            url: url,
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){
                respuesta = parseInt(data);
                if (respuesta==1) {
                    notificacion(3,'fa fa-check','Completado!','Se ha eliminado el registro');
                    redireccionar(url);
                }
                else{
                    notificacion(2, 'fa fa-times-circle','Error!', data);
                    return false;
                }
            
               
            }
        })
}


function atender(tipo, id){
    datos={"id":id, "tipo":tipo};
    $.ajax({
            url: '../Controller/GestionCitasController.php?accion=atender',
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){
                respuesta = parseInt(data);
                if (respuesta==1) {
                    redireccionar('../Controller/GestionCitasController.php?accion=index');
                }
            
               
            }
        })
}

function enviar(url, datos, div){

        $.ajax({
            url: url,
            contentType: "charset=utf-8", 
            data:datos,
            type : 'GET',
            success:function(data){

            $("#"+div).html(data);
               
            }
        })
    }

function notificacion(tipo, icono, titulo, mensaje){
    if (tipo == 1) {
        tipomsg = "infomsg";
    }
    else if (tipo == 2) {
        tipomsg = "errormsg";
    }
    else if (tipo == 3) {
        tipomsg = "successmsg";
    }
    new PNotify({
        title: titulo,
        text: mensaje,
        addclass: tipomsg,
        icon: icono,
        mouse_reset: false,
        nonblock: {
            nonblock: true
        },
        animate: {
            animate: true,
            in_class: 'slideInDown',
            out_class: 'slideOutUp'
        }
    });
}

function cuadroeliminar(url, id){
    new PNotify({
    title: '¿Esta seguro que desea eliminarlo?',
    icon: 'fa fa-exclamation',
    hide: false,
    type: 'info',
    confirm: {
        confirm: true
    },
    buttons: {
        closer: false,
        sticker: false
    },
    history: {
        history: false
    },
    addclass: 'stack-modal',
    stack: {
        'dir1': 'down',
        'dir2': 'right',
        'modal': true
    }
}).get().on('pnotify.confirm', function() {

    eliminar(url, id);
}).on('pnotify.cancel', function() {
    
});
}


  
