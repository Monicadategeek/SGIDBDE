<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Registro de Trabajador</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="agregar" action="../Controller/TrabajadoresController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="store">
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="newnewtrabajadores">
      <thead> 
        <th></th>
        <th >Cédula</th>
        <th >Nombres</th>
        <th >Apellidos</th>
        <th >Sexo</th> 
      </thead>
      <tfoot>
        <th></th>
        <th >Cédula</th>
        <th >Nombres</th>
        <th >Apellidos</th>
        <th >Sexo</th>
      </tfoot>
      <tbody>
        <?php 
          if (is_array($personas) || is_object($personas))
          {
              
              foreach ($personas as $personas)
              {
                  echo "<tr>
                  <td><input type='radio' name='persona' value='".$personas["Num_Usuario"]."'></td>
                  <td>".$personas["cedula"]."</td>
                  <td>".$personas["Nombres"]."</td>
                  <td>".$personas["Apellidos"]."</td>
                  <td>".$personas["sexo"]."</td>
                  </tr>"; 
              }
          }
      ?>
      </tbody>
    </table>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
  <div class="form-group col-xs-4 col-md-4 col-xl-4">
    <label class="control-label" for="Depto">Departamento Asociado</label>
      <select id="Depto" name="Depto" class="form-control selectpicker" required="" title="-Seleccione-" required="">
      <?php 
      if (is_array($departamentos) || is_object($departamentos))
      {
          foreach ($departamentos as $departamento)
          {
              echo "<option value='".$departamento["Depto"]."'>".$departamento["TipoDepto"]." en ".$departamento["Sede"]."</option>";
          }
      }
      ?>
      </select>
  </div>
  <div class="form-group col-xs-4 col-md-4 col-xl-4">
    <label class="control-label" for="f">Ocupación</label>
    <select id="TipoPersonal" name="TipoPersonal" class="form-control selectpicker" required="" title="-Seleccione-" required="">
    <?php 
    if (is_array($tipopersonal) || is_object($tipopersonal))
    {
        foreach ($tipopersonal as $tipopersona)
        {
            echo "<option value='".$tipopersona["IdTipoPersonal"]."'>".$tipopersona["Descripcion"]."</option>";
        }
    }
    ?>
    </select>
  </div>
  <div class="col-xs-4 col-md-4 col-xl-4">
    <label class="control-label" for="FechaIngreso">Fecha de Ingreso</label>
    <input type="text" name="FechaIngreso" id="FechaIngreso" class="form-control datepicker" data-date-end-date='0d' title="Ingrese la Fecha de Ingreso del Trabajadore" required="">
  </div>
  </div>


    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>
  <script type="text/javascript">
  
    $(document).ready(function(){
      $('#agregar').on('shown.bs.modal', function (e) {
      $('.selectpicker').selectpicker('refresh');
     $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es',
        });
      $('#newnewtrabajadores').DataTable({
        "language": {
          "lengthMenu": "Mostrar _MENU_ Registros por página",
          "zeroRecords": "Disculpe, No existen registros de Personas",
          "info": "Mostrando paginas _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(Filtrado de _MAX_ registros totales)",
          "decimal": ",",
          "thousands": "."
        },
        "scrollY":        "200px",
        retrieve: true,
        "scrollCollapse": true,
        "paging":         false
      });
      // Setup - add a text input to each footer cell
      $('#newtrabajadores tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
      } );
   
      // DataTable
      var table = $('#newtrabajadores').DataTable();
   
      // Apply the search
      table.columns().every( function () {
          var that = this;
   
          $( 'input', this.footer() ).on( 'keyup change', function () {
              if ( that.search() !== this.value ) {
                  that
                      .search( this.value )
                      .draw();
              }
          } );
      } );


       table.columns(4).every( function () {
          
          var column = this;
          var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
            .appendTo($(column.footer()).empty())
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );

              column
                .search(val ? '^' + val + '$' : '', true, false)
                .draw();
            });

          column.data().unique().sort().each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>')
          });

      });
     $('.selectpicker').selectpicker('refresh');
});
});


</script>

<script type="text/javascript">
  $('#formulario').on('submit', '#agregar', function (e) {

    if (!$('input:radio[name=persona]:checked').val()) {
        notificacion(2, 'fa fa-times-circle','Error!','Debe selecionar la persona que trabajara en el Departamento');
        return false;
      }

    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/TrabajadoresController.php?accion=index');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado el Trabajador, ingrese en horarios para indicar el horario del trabajador');
              notificacion(1,'fa fa-exclamation-circle','Información!','Ingrese en el boton del trabajador llamado horarios para indicar el horario del trabajador');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>