<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Ver Registro del trabajador</h4>
</div>
<div class="modal-body row" >
  <div class="col-xs-12 col-md-12 col-xl-12" align="left">
    <h3>Datos del Personales</h3>
    <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12" id="persona" > 
      <tr>
          <th>Cédula</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Sexo</th> 
      </tr>
      <?php
        echo "<tr>
        <td>".$persona["cedula"]."</td>
        <td>".$persona["nombre"]." ".$persona["nombre2"]."</td>
        <td>".$persona["apellido"]." ".$persona["apellido2"]."</td>
        <td>".$persona["sexo"]."</td>
        ";     
        echo "</tr>";
      ?>
      <tr> 
        <td colspan="4"></td>
      </tr>
      <tr> 
        <th>Estado</th>
        <th>Correo</th>
        <th>Telefono</th>
        <th>Celular</th>  
      </tr>
      <?php
        echo "<tr>
        <td>".$persona["Cod_Estatus"]."</td>
        <td>".$persona["correo"]."</td>
        <td>".$persona["telefono"]."</td>
        <td>".$persona["celular"]."</td>
        ";     
        echo "</tr>";
      ?>
    </table> 

    <h3>Datos del trabajador</h3>

    <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
      <thead> 
        <th>Nro Trabajador</th>
        <th>Departamento</th>
        <th>Sede</th>
        <th>Ocupación</th>      
        <th>Fecha Ingreso</th> 
        <?php if (!empty($trabajador["FechaEgreso"])) {
          echo "<th>Fecha Egreso</th>";           
        }?>
        <th>Estatus</th> 
      </thead>
      <tbody>
        <?php
          $date= new DateTime($trabajador["FechaIngreso"]);
          echo "<tr>
          <td>".$trabajador["IdTrabajador"]."</td>
          <td>".$trabajador["TipoDepto"]."</td>
          <td>".$trabajador["Sede"]."</td>
          <td>".$trabajador["TipoPersonal"]."</td>
          <td>".$trabajador["FechaIngreso"]."</td>";
          if (!empty($trabajador["FechaEgreso"])) {
            echo "<td>".$trabajador["FechaEgreso"]."</td>";           
          }
          echo "<td>".$trabajador["Estatus"]."</td>";
          echo "</tr>";
          
        ?>
      </tbody>
    </table> 

    <h3>Datos del Sistema</h3>

    <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
      <thead> 
        <th>Fecha de Registro</th>
        <th>Última Modificación</th>
        <th>Administrador</th>
      </thead>
      <tbody>
        <?php
          echo "<tr>
          <td>".$trabajador["FechaRegistro"]."</td>
          <td>".$trabajador["UltModif"]."</td>
          <td>".$trabajador["Responsable"]."</td>";
          echo "</tr>";
        ?>
      </tbody>
    </table> 
  </div>
</div>
<div class="modal-footer" >
  <?php 
    if (isset($boton)) {
      echo $boton;
    }
  ?>
  <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
</div>



