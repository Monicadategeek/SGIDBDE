<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificar Registro del Trabajador</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="agregar" action="../Controller/TrabajadoresController.php" method="POST">
  <input type="hidden" name="IdTrabajador" id="IdTrabajador" value="<?php echo $trabajador["IdTrabajador"] ?>">
  <input type="hidden" name="accion" id="accion" value="update">
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead> 
        <th >Cédula</th>
        <th >Nombres</th>
        <th >Apellidos</th>
      </thead>
      <tbody>
        <?php 
          echo "<tr>
                  <td>".$trabajador["CedulaPersona"]."</td>
                  <td>".$trabajador["NombrePersona"]."</td>
                  <td>".$trabajador["ApellidoPersona"]."</td>";
                  echo "</tr>";
      ?>
      </tbody>
    </table>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
  <div class="form-group col-xs-4 col-md-4 col-xl-4">
    <label class="control-label" for="Depto">Departamento Asociado</label>
      <select id="Depto" name="Depto" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($departamentos) || is_object($departamentos))
      {
          foreach ($departamentos as $departamento)
          {
              if ($departamento["Depto"]==$trabajador["Depto"]) {
                echo "<option value='".$departamento["Depto"]."' selected>".$departamento["TipoDepto"]." en ".$departamento["Sede"]."</option>";
              }
              else{
                echo "<option value='".$departamento["Depto"]."'>".$departamento["TipoDepto"]." en ".$departamento["Sede"]."</option>";
              }
              
          }
      }
      ?>
      </select>
  </div>
  <div class="form-group col-xs-4 col-md-4 col-xl-4">
    <label class="control-label" for="f">Ocupación</label>
    <select id="TipoPersonal" name="TipoPersonal" class="form-control selectpicker" required="" title="-Seleccione-">
    <?php 
    if (is_array($tipopersonal) || is_object($tipopersonal))
    {
        foreach ($tipopersonal as $tipopersona)
        {
            if ($tipopersona["IdTipoPersonal"]==$trabajador["TipoPersonal"]) {
               echo "<option value='".$tipopersona["IdTipoPersonal"]."' selected>".$tipopersona["Descripcion"]."</option>";
            }
            else{
               echo "<option value='".$tipopersona["IdTipoPersonal"]."'>".$tipopersona["Descripcion"]."</option>";
            }
           
        }
    }
    ?>
    </select>
    <?php
    $date= new DateTime($trabajador["FechaIngreso"]);

    ?>
    
  </div>
  <div class="col-xs-4 col-md-4 col-xl-4">
      <label class="control-label" for="FechaIngreso">Fecha de Ingreso</label>
      <input type="text" name="FechaIngreso" id="FechaIngreso" class="form-control datepicker"  title="Ingrese la Fecha de Ingreso del Trabajadore" value="<?php echo $date->format('d-m-Y') ?>">
    </div>
  </div>
  

    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>
  
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker('refresh');
     $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es',
        getStartDate :'+1d'
        });
  });


  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/TrabajadoresController.php?accion=index');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado el Trabajador, ingrese en horarios para indicar el horario del trabajador');
              notificacion(1,'fa fa-exclamation-circle','Información!','Ingrese en el boton del trabajador llamado horarios para indicar el horario del trabajador');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>