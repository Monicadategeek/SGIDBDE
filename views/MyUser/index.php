     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Mi Perfil
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
       

        <div class="x_content row">
             <div class="col-xs-12 col-md-12 col-xl-12">
            
                <div class=" col-md-3 col-xl-12" style="margin-top: 12px;">
                    <?php 
                    if ($estudiante["foto"]!=0) {
                    echo '<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$estudiante["foto"].'" alt="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'" title="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'">';
                    }
                    else{
                    echo '<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
                    } ?>
                    <ul class="list-unstyled user_data">
                        <li  title="Carrera"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $estudiante["cod_carrera"] ?>
                        </li>
                        <li>
                          <i class="fa fa-book"></i> <b title="Trayecto">Trayecto</b> <?php echo $estudiante["trayecto"] ?> &nbsp; &nbsp; <i class="fa fa-book"></i> <b title="Trimestre">Trimestre</b>  <?php echo $estudiante["trimestre"] ?>
                        </li>
                        <li>
                          <i class="fa fa-book"></i> <b title="Sección">Sección</b> <?php echo $estudiante["seccion"] ?> 
                          <i class="fa fa-clock-o"></i> <b title="Turno">Turno</b> <?php echo $estudiante["turno"] ?>
                        </li>
                        <li title="Es el estatus frente a la universidad">
                          <i class="fa fa-user"></i> <b >Estado</b> <?php echo $estudiante["cod_estatus"] ?>
                        </li>
                      </ul>
                </div>

                <div class=" col-md-9 col-xl-12">
                    <h3>Mis Datos</h3>
                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Cédula</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Sexo</th>
                          <th>Fecha Nacimiento</th>
                          <th>Edad</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><?php echo $estudiante["nacionalidad"]." ".$estudiante["cedula"] ?></td>
                            <td><?php echo $estudiante["nombres"] ?></td>
                            <td><?php echo $estudiante["apellidos"] ?></td>
                            <td><?php echo $estudiante["sexo"] ?></td>
                            <td><?php echo $estudiante["fec_nac"] ?></td>                        
                            <td><?php echo $estudiante["edad"] ?></td>
                        </tr>
                      </tbody>
                    </table>
                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Dirección</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><?php echo $estudiante["direccion"] ?></td> 
                        </tr>
                      </tbody>
                    </table>

                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Dirección de Correo Electrónico</th>
                          <th>Telefono</th>
                          <th>Celular</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><?php echo $estudiante["correo"] ?></td>
                            <td><?php echo $estudiante["celular"] ?></td>
                            <td><?php echo $estudiante["tel_hab"] ?></td> 
                        </tr>
                      </tbody>
                    </table>

                    <button class="btn btn-large btn-primary" onclick="modificar('../Controller/MyUserController.php', <?php  echo $estudiante["cedula"] ?>);"><i class="fa fa-lock" aria-hidden="true"></i> Modificar mi Contraseña</button>
                </div>
            </div>
        </div>
    </div>
</div>


