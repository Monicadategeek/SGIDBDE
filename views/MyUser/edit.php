<style>
.valid{
  color: green;
}
.invalid{
  color: red;
}
</style>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de Contraseña</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="editar" action="../Controller/MyUserController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="update">
    <div class="col-xs-12 col-md-12 col-xl-12">
      <div class="form-group col-xs-12 col-md-6 col-xl-6">
        <label for="inputPassword" class="control-label">Contraseña</label>
        <input type="password" data-minlength="6" maxlength="14" class="form-control" id="clave" name="clave" placeholder="Contraseña" required>
        <div class="help-block">Minimo 6 caracteres</div>
      </div>
      <div class="form-group col-xs-12 col-md-6 col-xl-6" id="repetir">
        <label for="inputPassword" class="control-label">Repita la Contraseña</label>
        <input type="password" data-minlength="6" class="form-control" id="clave2" name="clave2" placeholder="Repita la Contraseña" required>
        <div class="help-block" id="coinciden">No Coinciden</div>
      </div>
    </div>

    <div class="form-group col-xs-12 col-md-12 col-xl-12" id="pswd_info">
     <h4>La contraseña debería cumplir con los siguientes requerimientos:</h4>
     
        <p id="letter">Al menos deberá tener <strong>una letra</strong></p>
        <p id="especial">Al menos deberá tener <strong>un caracter especial ( - / = . $ # * )</strong></p>
        <p id="espacios">No debe tener <strong> espacios en blanco </strong></p>
        <p id="number">Al menos debería tener <strong>un número</strong></p>
        <p id="length">Debería tener <strong>8 carácteres</strong> como mínimo</p>
     
    </div>

    
    <div class="form-group col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $('#boton').hide();
  $('#repetir').hide();
  $('#coinciden').hide();
  $(document).ready(function() {
    $('#clave2').keyup(function() {
      if ($('#clave').val()==$('#clave2').val()) {
        $('#coinciden').hide();
        $('#boton').show();
      }
      else{
        $('#coinciden').show();
        $('#boton').hide();
      }
    })

    $('#clave').keyup(function() {
        // set password variable
        var pswd = $(this).val();
        //validate the length
        if ( pswd.length < 8 ) {

          $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length').removeClass('invalid').addClass('valid');
        }

        //validate letter
        if ( pswd.match(/[A-z]/) ) {
            $('#letter').removeClass('invalid').addClass('valid');
        } else {
            $('#letter').removeClass('valid').addClass('invalid');
        }

        //validate capital letter
        if ( pswd.match(/[A-Z]/) ) {
            $('#capital').removeClass('invalid').addClass('valid');
        } else {
            $('#capital').removeClass('valid').addClass('invalid');
        }


        //validate espacios en blanco
        var espacio_blanco    = /[a-z]/i;  //Expresión regular

        if(!espacio_blanco.test(pswd))
        {
          $('#espacios').removeClass('valid').addClass('invalid');
        }
        else{
          $('#espacios').removeClass('invalid').addClass('valid');
        }


        if(pswd.indexOf('*') != -1 || pswd.indexOf('$') != -1 || pswd.indexOf('#') != -1 || pswd.indexOf('.') != -1|| pswd.indexOf('=') != -1 || pswd.indexOf('/') != -1 || pswd.indexOf('-') != -1){
             $('#especial').removeClass('invalid').addClass('valid');
        }
        else{
          $('#especial').removeClass('valid').addClass('invalid');
        }

        //validate number
        if ( pswd.match(/\d/) ) {
            $('#number').removeClass('invalid').addClass('valid');
        } else {
            $('#number').removeClass('valid').addClass('invalid');
        }

        if ($('#letter').hasClass('valid') && $('#especial').hasClass('valid') && $('#espacios').hasClass('valid') && $('#number').hasClass('valid') && $('#length').hasClass('valid')){
            $('#repetir').show();
        }else{
            $('#repetir').hide();
            $('#clave2').val('');
        }
    




    }).focus(function() {
        $('#pswd_info').show();
    }).blur(function() {
        $('#pswd_info').hide();
    });

});

  $('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();
    if ($('#clave').val() != $('#clave2').val()) {
      notificacion(2,'fa fa-times-circle','Error!','Las Contraseñas no Coinciden');
      return false;
    }
    var clave=$('#clave').val();
    $('#clave').val(md5(clave));
    var clave=$('#clave2').val();
    $('#clave2').val(md5(clave));
    document.getElementById("boton").disabled = true;         
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/MyUserController.php');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado la contraseña del usuario');
            }
            else{
              notificacion(2,'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>