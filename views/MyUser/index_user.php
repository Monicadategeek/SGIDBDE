     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Mi Perfil
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        

        <div class="x_content row">
             <div class="col-xs-12 col-md-12 col-xl-12">
            
                <div class=" col-md-3 col-xl-12" style="margin-top: 12px;">
                    <?php echo $usuario["Foto_Usuario"];
                    ?>
                    <button class="btn btn-large btn-primary" style="margin-top: 10px;" onclick="modificar('../Controller/MyUserController.php', <?php  echo $usuario["cedula"] ?>);"><i class="fa fa-lock" aria-hidden="true"></i> Modificar mi Contraseña</button>
                </div>

                <div class=" col-md-9 col-xl-12">
                    <h3>Mis Datos</h3>
                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Cédula</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Sexo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><?php echo $usuario["cedula"] ?></td>
                            <td><?php echo $usuario["Nombres"] ?></td>
                            <td><?php echo $usuario["Apellidos"] ?></td>
                            <td><?php echo $usuario["sexo"] ?></td>
                        </tr>
                      </tbody>
                    </table>

                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Dirección de Correo Electrónico</th>
                          <th>Telefono</th>
                          <th>Celular</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><?php echo $usuario["correo"] ?></td>
                            <td><?php echo $usuario["celular"] ?></td>
                            <td><?php echo $usuario["telefono"] ?></td> 
                        </tr>
                      </tbody>
                    </table>

                    <h3>Mi Cargo</h3>
                    <table class="data table table-striped">
                      <thead>
                        <tr>
                          <th>Departamento</th>
                          <th>Sede</th>
                          <th>Ocupación</th>
                          <th>Horarios</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          foreach ($departamentos as $departamento) {
                            echo " <tr>
                                    <td>".$departamento["TipoDepto"]."</td>
                                    <td>".$departamento["Sede"]."</td>
                                    <td>".$departamento["TipoPersonal"]."</td>
                                    <td>".'<button class="btn btn-primary btn-xs" title="Horario" onclick="mostrarhorarios('.$departamento["IdTrabajador"].');"><i class="fa fa-clock-o"></i></button>'."</td>                            
                                </tr>";
                          }


                        ?>
                      </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="horariosModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg2" role="document">

    <div class="modal-content" style="padding: 20px;" id="horarioscontent">

      
    </div>
  </div>
</div>