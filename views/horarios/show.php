<?php
$fechas = array('0' => '2017-01-01', '1' => '2016-12-26', '2' => '2016-12-27', '3' => '2016-12-28', '4' => '2016-12-29', '5' => '2016-12-30', '6' => '2016-12-31',);
$array='[';

foreach ($horarios as $horario) {
	$array=$array.'
	{
            "id":"'.$horario["IdHorarios"].'",
            "title":"'.$horario["TipoPersonal"]." Depto ".$horario["TipoDepto"]." en ".$horario["Sede"].'",
            "start":"'.$fechas[$horario["Dia"]].'T'.$horario["HorarioInic"].'",
            "end":"'.$fechas[$horario["Dia"]].'T'.$horario["HorarioFinal"].'"
    },';
}
if ($horarios) {
    $array = substr($array, 0, -1); 
}

$array=$array.']';
 
?>
<input type="hidden" name="arrayeventos" id="arrayeventos" value='<?php echo $array ?>'>
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
<h1 style="text-align: center;">Horario <?php
 echo $persona["nombres"]." ".$persona["apellidos"]?></h1>


<div id="calendar" style="width:100%;"></div>