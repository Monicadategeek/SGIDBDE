			<div role="tabpanel" class="tab-pane fade" id="tab_content2">
				<div class="col-xs-12 col-md-12 col-xl-12 row">
					<h5>ENCUESTA ODONTOLÓGICA: </h5> <br>
					<table class="table table-striped">
						<thead>
							<th align="center">Antecedentes Familiares</th>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $EncOdont["AntFami"] ?></td>
							</tr>
							
						</tbody>
					</table>

					<table class="table table-striped">
						<thead>
							<th>Motivo de la consulta</th>
							<th>Dolor</th>
							<th>Control</th>
							<th>Sangramiento en las encias</th>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $EncOdont["MotCons"] ?></td>
								<td><?php echo $EncOdont["Dolor"] ?></td>
								<td><?php echo $EncOdont["Control"] ?></td>
								<td><?php echo $EncOdont["SangEnc"] ?></td>
							</tr>									
						</tbody>
					</table>

					<table class="table table-striped">								
						<thead>									
							<th>Restauracion prostésica</th>
							<th>Otro indique cual</th>
							<th>Enfermedad actual</th>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $EncOdont["RestProt"] ?></td>
								<td><?php echo $EncOdont["Otro"] ?></td>
								<td><?php echo $EncOdont["EnferAct"] ?></td>
							</tr>									
						</tbody>

					</table>

					<div class="col-xs-12 col-md-6 col-xl-6">
						<table class="table table-bordered table-hover">
			        		<tbody>
			                <?php 
			                $count=0;
			                    foreach ($PreguntasOdontologicas as $pregunta) {
			                    	$count=$count+1;
			                        if ($count<=$mitad) {
			                        	echo "<tr>
			                        		<td>".$pregunta["Descripcion"]."</td> <td>";

			                            if ($pregunta["tiporesp"]==1) {
			                                echo $pregunta["respuesta"];
			                            }
			                            else if ($pregunta["tiporesp"]==2) {
			                                 echo $pregunta["respuesta"].' '.$pregunta["nro"];
			                            }

			                            
			                            echo "</td></tr>";
			                        }
			                    }
			                ?>
			                </tbody>
			            </table>							
					</div>

					<div class="col-xs-12 col-md-6 col-xl-6">
						<table class="table table-bordered table-hover">
			        		<tbody>
			                <?php 
			                $count=0;
			                    foreach ($PreguntasOdontologicas as $pregunta) {
			                    	$count=$count+1;
			                        if ($count>$mitad) {
			                        	echo "<tr>
			                        			<td>".$pregunta["Descripcion"]."</td> <td>";

			                            if ($pregunta["tiporesp"]==1) {
			                                echo $pregunta["respuesta"];
			                            }
			                            else if ($pregunta["tiporesp"]==2) {
			                                 echo $pregunta["respuesta"].' '.$pregunta["nro"];
			                            }

			                            
			                            echo "</td></tr>";
			                        }
			                    }
			                ?>
			                </tbody>
			            </table>							
					</div>	
				</div>
			</div>

			