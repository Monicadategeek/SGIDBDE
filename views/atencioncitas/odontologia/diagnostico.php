				<div role="tabpanel" class="tab-pane fade" id="tab_content3">
					<div class="col-xs-12 col-md-12 col-xl-12 row" id="formulario">
						<form id="atencioncitas-odontologia" action="../Controller/AtencionCitasController.php" method="POST">
							<h5>DIAGNOSTICO Y TRATAMIENTO </h5> <br>
							<div class="col-xs-12 col-md-12 col-xl-12">
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Aspecto del Paciente</label>
									<input type="text" name="AspPac" class="form-control">							
									
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Cara</label>
									<input type="text" name="Cara" class="form-control">	
									
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Labios y Comisura</label>
									<input type="text" name="LabyCom" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Palpación de ganglios</label>
									<input type="text" name="PalpaGangl" class="form-control">								
								</div>
								
							</div>
							<div class="col-xs-12 col-md-12 col-xl-12">
								
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Ganglios</label>
									<input type="text" name="Ganglios" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>ATM</label> <small>(articulación temporamandibular)</small>
									<input type="text" name="ATM" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Ojeras</label>
									<input type="text" name="Ojeras" class="form-control">								
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Región hipodea y tiroidea</label>
									<input type="text" name="RegHiotTiroidea" class="form-control">
								</div>
							</div>			

							<div class="col-xs-12 col-md-12 col-xl-12">
								<h6>EXAMEN CLÍNICO INTRABUCAL:</h6>
								<div class="form-group col-xs-12 col-md-2 col-xl-2">
									<label>Carrillos</label>
									<input type="text" name="Carrillos" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-2 col-xl-2">
									<label>Mucosa</label> 
									<input type="text" name="Mucosa" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-2 col-xl-2">
									<label>Encia</label>
									<input type="text" name="Encia" class="form-control">								
								</div>
								<div class="form-group col-xs-12 col-md-2 col-xl-2">
									<label>Lengua</label>
									<input type="text" name="Lengua" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-2 col-xl-2">
									<label>Paladar</label>
									<input type="text" name="Paladar" class="form-control">
									
								</div>
							</div>				

							<div class="col-xs-12 col-md-12 col-xl-12">
								<h6>EXAMENES COMPLEMENTARIOS</h6>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Rx</label> 
									<div class="radio">									
		                                <label>
		                                    <input type="radio" name="examen" value="Panoramica" >Panoramica
		                                </label>
		                                 <label>
		                                    <input type="radio" name="examen" value="Coronal" >Coronal
		                                </label>
		                                 <label>
		                                    <input type="radio" name="examen" value="Periodontal" >Periodontal
		                                </label>
									</div>		
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Laboratorio</label>
									<input type="text" name="Laboratorio" class="form-control">								
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Modelo</label>
									<input type="text" name="Modelo" class="form-control">
									
								</div>
								<div class="form-group col-xs-12 col-md-3 col-xl-3">
									<label>Tensión arterial</label>
									<input type="text" name="TensionArterial" class="form-control">
									
								</div>
							</div>


							<div class="col-xs-12 col-md-12 col-xl-12">
								<div class="form-group col-xs-12 col-md-12 col-xl-12">
									<label>Observaciones</label>
									 <textarea class="form-control textarea_estile" name="Observaciones"></textarea>
								</div>
							</div>						
							<div class="col-xs-12 col-md-12 col-xl-12">
								<h6>ODONTODIAGRAMA</h6>
								<div id="odontogramageneral">
								<center ng-controller="dientes" class="ng-scope row">
									<div class="col-xs-12 col-md-12 col-xl-12">
										<opcionescanvas class="ng-isolate-scope row">
									    	<div class="col-xs-8 col-md-8 col-xl-8">
											<table align="center" class="table table-bordered" id="tablaodotograma">
												<tbody>
												<tr>
													<th>Amalgama</th>
													<th>Caries</th>
													<th>Endodoncia</th>
													<th>Ausente</th>
													<th>Resina</th>
													<th>Implante</th>
													<th>Sellante</th>
													<th>Corona</th>
													<th>Normal</th>
												</tr>
												<tr>
													<td><center><div class="color" value="1" style="background-color:#f44336;width:35px;height:35px"></div></center></td>

													<td><center><div class="color" value="2" style="background-color:#FFEB3B;width:35px;height:35px"></div></center></td>

													<td><center><div class="color" value="3" style="margin: -10px 0 0 -7px;">
														<svg height="50" width="50"><circle cx="30" cy="30" r="20" estado="3" value="9" class="marcadoNaranja"></circle></svg>
													</div></center></td>

													<td><center><div class="color" value="4" style="margin: -10px 0 0 -7px;">
														<svg height="50" width="50"><polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="marcadoTomate"></polygon>
														<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="marcadoTomate"></polygon></svg>
													</div></center></td>

													<td><center><div class="color" value="5" style="background-color:#9c5915;width:35px;height:35px"></div></center></td>

													<td> <center><div class="color" value="6" style="margin: -10px 0 0 -7px;">
														<svg height="50" width="50"><polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="marcadoMorado"></polygon></svg>
													</div></center></td>

													<td><center><div class="color" value="7" style="background-color:#4caf50;width:35px;height:35px"></div></center></td>

													<td><center><div class="color" value="8" style="margin: -10px 0 0 -7px;">
														<svg height="50" width="50"><circle cx="30" cy="30" r="16" estado="8" value="8" class="marcadoAzul"></circle></svg></div></center></td>

													<td><center><div class="color" value="9" style="background-color:#fff;width:35px;height:35px;border: 1px solid #607D8B;"></div></center></td>
												</tr>
												</tbody></table>
												</div>
												<div class="col-xs-4 col-md-4 col-xl-4" id="botones">
												<center>
													<button type="button" class="btn btn-info btn-sm" id="ver">
						                                <i class="fa fa-download" aria-hidden="true"></i> Cargar
						                            </button>
						                            <button type="button" class="btn btn-success btn-sm" id="agregar">
						                                <i class="fa fa-floppy-o" aria-hidden="true"></i> Agregar
						                            </button>
						                            <button type="button" class="btn btn-warning btn-sm" id="limpiar">
						                                <i class="fa fa-repeat" aria-hidden="true"></i> Reiniciar
						                            </button>
												<h6><b>Modo</b></h6>
													<input type="radio" id="Decidua" name="tipo" value="1" checked=""> Permanente
													<input type="radio" id="Niños" name="tipo" value="2"> Decidua
													<input type="radio" id="Mixta" name="tipo" value="3"> Mixta
												</center>
												</div>
										</opcionescanvas>
										<br>
									    <div>
									        <!-- ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="1">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="2">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="3">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="4">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="5">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="6">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="7">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="8">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="9">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="10">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="11">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="12">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="13">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="14">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="15">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva --><canvasodontograma info="i" ng-repeat="i in adultoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="16">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoArriva -->
									    </div>
									    <div>
									        <!-- ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="33">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="34">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="35">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="36">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="37">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="38">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="39">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="40">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="41">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva --><canvasodontograma info="i" ng-repeat="i in ninoArriva" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="42">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoArriva -->
									    </div>
									    <div>
									        <!-- ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="43">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="44">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="45">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="46">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="47">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="48">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="49">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="50">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="51">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo --><canvasodontograma info="i" ng-repeat="i in ninoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="nino" width="50" id="52">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in ninoAbajo -->
									    </div>
									    <div>
									        <!-- ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="17">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="18">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="19">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="20">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="21">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="22">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="23">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="24">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="25">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="26">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="27">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="28">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua mixta" width="50" id="29">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="desabilitado"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="desabilitado"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="desabilitado"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="desabilitado"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="desabilitado"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="30">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="31">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo --><canvasodontograma info="i" ng-repeat="i in adultoAbajo" class="ng-scope ng-isolate-scope"><svg height="50" class="decidua" width="50" id="32">
											  	<polygon points="10,15 15,10 50,45 45,50" estado="4" value="6" class="ausente"></polygon>
									  			<polygon points="45,10 50,15 15,50 10,45" estado="4" value="7" class="ausente"></polygon>
									  			<circle cx="30" cy="30" r="16" estado="8" value="8" class="corona"></circle>
									  			<circle cx="30" cy="30" r="20" estado="3" value="9" class="endodoncia"></circle>
									  			<polygon points="50,10 40,10 10,26 10,32 46,32 10,50 20,50 50,36 50,28 14,28" estado="6" value="10" class="implante"></polygon>
									  			<polygon points="10,10 50,10 40,20 20,20" estado="0" value="1" class="diente"></polygon>
									  			<polygon points="50,10 50,50 40,40 40,20" estado="0" value="2" class="diente"></polygon>
									  			<polygon points="50,50 10,50 20,40 40,40" estado="0" value="3" class="diente"></polygon>
									  			<polygon points="10,50 20,40 20,20 10,10" estado="0" value="4" class="diente"></polygon>
									  			<polygon points="20,20 40,20 40,40 20,40" estado="0" value="5" class="diente"></polygon>
									</svg>

									</canvasodontograma><!-- end ngRepeat: i in adultoAbajo -->
									    </div>
									</div>
									</center>
									<br><br>
								</div>
								



							</div>
							<div class="col-xs-12 col-md-12 col-xl-12">
								<div class="form-group col-xs-12 col-md-12 col-xl-12">
									<label>Descripción del tratamiento</label> <small>Breve descripción para indicar cual sera el tratamiento</small>
									<input type="text" name="DescrTrat" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-md-6 col-xl-6">
									<label>Diagnostico</label>
									 <textarea class="form-control textarea_estile" name="Diagnostico"></textarea>

								</div>	
								<div class="form-group col-xs-12 col-md-6 col-xl-6">
									<label>Plan de Tratamiento</label>
									 <textarea class="form-control textarea_estile" name="Tratamiento"></textarea>

								</div>							 

							</div>
							<input type="hidden" name="accion" id="accion" value="AtencionCitas">
						    
						    <input type="hidden" name="IdCita" id="IdCita" value="<?php echo $idCita ?>">
						    <input type="hidden" name="TipoInforme" id="TipoInforme" value="3">
						    <input type="hidden" name="odontograma" id="odontograma" value="">
						    <input type="hidden" name="Estudiante" id="Estudiante" value="<?php echo $cita["IdEstudiante"] ?>">
							<input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton" style="padding-bottom: 12px; width: 100%;">
						</form>

						<div class="col-xs-12 col-md-12 col-xl-12">
							<div class="form-group col-xs-12 col-md-12 col-xl-12">
								<label>Secuencia del Tratamiento</label>
								 <table class="table table-striped" id="tratamientos">
									<thead>
										<th>Fecha</th>
										<th>Hora</th>
										<th>Descripción</th>
										<th></th>
									</thead>
									<tbody>
										<?php 
					                        if (is_array($seguimientos) || is_object($seguimientos))
					                        {
					                            
					                            foreach ($seguimientos as $seguimiento)
					                            {
					                            	echo "<tr>
							                                <td>".$seguimiento["Fecha"]."</td>
							                                <td>".$seguimiento["Hora"]."</td>
							                                <td>".$seguimiento["DescrTrat"]."</td>
							                                <td>".$seguimiento["boton"]."</td>
							                             </tr>";
					                            }
					                        }

					                     ?>									
										
									</tbody>
								</table>
							</div>							 
						</div>

					</div>
				</div>
				<!-- Continuacion del cierre de etiquetas de la pagina Identificacion.php -->
			</div>
		</div>
	
</div>

   
        <!-- /.row -->
 <script type="text/javascript">
    $(document).ready(function(){
$('#tratamientos').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de tratamientos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
   

    $('.selectpicker').selectpicker('refresh');
});




</script>

<script type="text/javascript">

	

	$('#formulario').on('submit', '#atencioncitas-odontologia', function (e) {
	    e.preventDefault();


	    document.getElementById("boton").disabled = true;
	    odontogramageneral = $('#odontogramageneral').html();
	    $('#odontograma').val(odontogramageneral);
	    var parametros= new FormData($(this)[0]);
	    $.ajax({
	        type: $(this).attr('method'),
	        url: $(this).attr('action'),
	        data: parametros,
	        contentType:false,
	        processData:false,
	        success:function(data){
	          respuesta = parseInt(data);
	          
	           if (respuesta==1) {
	              redireccionar('../Controller/GestionCitasController.php');
	              notificacion(3,'fa fa-check','Completado!','Se ha atendido al Estudiante');
	            }
	            else{
	              notificacion(2, 'fa fa-times-circle','Error!',data);
	            }
	            
	           
	            
	        }
	    });
	    document.getElementById("boton").disabled = false;
	  });

		//datos de prueba
	var color = 1;
	var datos = [{diente:1,cara:1,estado:8,tipo:1},
				 {diente:2,cara:2,estado:2,tipo:1},
				 {diente:2,cara:5,estado:1,tipo:1},
				 {diente:3,cara:2,estado:6,tipo:1},
				 {diente:4,cara:2,estado:4,tipo:1}];

	

	$(document).ready(function(){

		$('polygon').on('click',function(){
			var desabilitado = $(this).attr('class');
			if (desabilitado != 'desabilitado' ) {
				pintar(color,this)
			}
			
		});
		$('input:radio').change(function(){
			var tipo = $(this).attr('value');
			cambiarOdontograma(tipo);
			
		});

		$('.color').click(function(){
			color = $(this).attr('value');
		});

		$('#ver').click(function(){
		var saltarEachCirculo = null;
		var saltarEachPoligono = null;
		limpiar();
		cambiarOdontograma(datos[0].tipo)	
		for (var i = 0; i < datos.length; i++) {
			if (datos[0].tipo == 1) {
				$('#Decidua').prop('checked',true);
			}
			else if (datos[0].tipo == 2) {
				$('#Niños').prop('checked',true);
			}
			else
			{
				$('#Mixta').prop('checked',true);
			}
			
			saltarEachCirculo = false;
			saltarEachPoligono = false;
			if (datos[i].estado == 3 || datos[i].estado ==  8) {
				$('#'+datos[i].diente +' circle').each(function(){
					if (saltarEachCirculo == false) {
						pintar(datos[i].estado,this);
						saltarEachCirculo = true;
					}
				});
			}
			else if (datos[i].estado == 4 || datos[i].estado ==  6) 
			{
				$('#'+datos[i].diente +' polygon').each(function(){
					if (saltarEachPoligono == false) {
						pintar(datos[i].estado,this);
						saltarEachPoligono = true;
					}
				});	
			}
			else
			{
				$('#'+datos[i].diente +' polygon').each(function(){

					if (datos[i].cara == $(this).attr('value')) {
						pintar(datos[i].estado,this);
					}
				});	
			}

			
		}
			
				
	});
	$('#agregar').click(function(){
		datos = [];
		$('svg').each(function(){
			var dienteD = $(this).attr('id');
			entrarEach = false;
			$(this).find('.marcado').each(function(){
				var caraD = $(this).attr('value');
				var estadoD = $(this).attr('estado');
				var tipoD = $('input:radio[name=tipo]:checked').val();
				if ((estadoD == 3 || estadoD == 4 || estadoD == 6 || estadoD == 8)) {
					datos.push({diente:dienteD,cara:caraD,estado:estadoD,tipo:tipoD});
					return false;
				}
				else 
				{
					datos.push({diente:dienteD,cara:caraD,estado:estadoD,tipo:tipoD});
				}
				
			});
		});
	});

	$('#limpiar').click(function(){
		limpiar();
	});


		cambiarOdontograma($('input:radio[name=tipo]:checked').val());
	});
	
	function activar_Desactivar_Dientes(lado,ad,valor)
	{
		if (ad == 1 && valor < 6) {
			$(lado).attr('class','diente');
		}
		else if (ad == 0 && valor < 6) {
			$(lado).attr('class','desabilitado');
		}
	}

	function cambiarOdontograma(tipo)
	{	var valor=null;
		if (tipo == 1) {

			$('.decidua polygon').each(function(){
				valor = $(this).attr('value');
				activar_Desactivar_Dientes(this,1,valor);
				quitarEspecial(this);
			});
			$('.nino polygon').each(function(){
				valor = $(this).attr('value');
				activar_Desactivar_Dientes(this,0,valor);
				quitarEspecial(this);
			});
		}
		else if (tipo == 2) {
			$('.decidua polygon').each(function(){
				valor = $(this).attr('value');
				activar_Desactivar_Dientes(this,0,valor);
				quitarEspecial(this);
			});
			$('.nino polygon').each(function(){
				valor = $(this).attr('value');
				activar_Desactivar_Dientes(this,1,valor);
				quitarEspecial(this);
			});
		}
		else if (tipo == 3) {
			$('.decidua').each(function(){
				var id = $(this).attr('id');
				if ((id > 3 && id < 14) ||  (id > 19 && id < 30) ) {
					$(this).find('polygon').each(function(){
						valor = $(this).attr('value');
						activar_Desactivar_Dientes(this,0,valor);	
						quitarEspecial(this);						
					});
				}
				else
				{
					$(this).find('polygon').each(function(){
						valor = $(this).attr('value');
						activar_Desactivar_Dientes(this,1,valor);	
						quitarEspecial(this);						
					});
				}	
			});
			$('.nino polygon').each(function(){
				valor = $(this).attr('value');
				activar_Desactivar_Dientes(this,1,valor);
				quitarEspecial(this);
			});
		}		
	}

	function limpiar(){
		$('svg').each(function(){
			$(this).find('.marcado').each(function(){
				var estado = $(this).attr('estado');

				if (estado != 3 && estado != 4 && estado != 6 && estado != 8) {
					$(this).attr('class','diente');
				}
				else if(estado == 3) {
					$(this).attr('class','endodoncia');
				}
				else if(estado == 4) {
					$(this).attr('class','ausente');
				}
				else if(estado == 6) {
					$(this).attr('class','implante');
				}
				else if(estado == 8) {
					$(this).attr('class','corona');
				}
			});
		});
	}

	function quitarEspecial(objeto)
	{
		$(objeto).parent().find('polygon').each(function(){
				if ($(this).attr('value') >= 6 && $(this).attr('value') <= 7  ) {
					$(this).attr('class','ausente');
				}
				else if ($(this).attr('value') == 10) {
					$(this).attr('class','implante');
				}
		});	
		$(objeto).parent().find('circle').each(function(){
				if ($(this).attr('value') == 8 ) {
					$(this).attr('class','corona');
				}
				else if ($(this).attr('value') == 9) {
					$(this).attr('class','endodoncia');
				}
				
		});
	}

	function limpiarLados(objeto)
	{
		$(objeto).parent().find('polygon').each(function(){
				if ($(this).attr('value') < 6 ) {
					$(this).attr({class:'diente',
								  estado:0});
				}
			});	
	}

	function pintar(color,objeto){
		if (color == 1) {
			quitarEspecial(objeto);		
			$(objeto).attr({class:'marcadoRojo marcado',
							estado:color});		}
		else if(color == 2){
			quitarEspecial(objeto);	
			$(objeto).attr({class:'marcadoAmarillo marcado',
							estado:color});
		}
		else if(color == 3){
			limpiarLados(objeto);
			quitarEspecial(objeto);	
			$(objeto).parent().find('.endodoncia').each(function(){
				$(this).attr({class:'marcadoNaranja marcado',
							estado:color});
			});
		}
		else if(color == 4){
			limpiarLados(objeto);
			quitarEspecial(objeto);	
			$(objeto).parent().find('.ausente').each(function(){
				$(this).attr({class:'marcadoTomate marcado',
							estado:color});
			});

		}
		else if(color == 5){
			quitarEspecial(objeto);	
			$(objeto).attr({class:'marcadoMarron marcado',
							estado:color});

		}
		else if(color == 6){
			limpiarLados(objeto);
			quitarEspecial(objeto);	
			$(objeto).parent().find('.implante').each(function(){
				$(this).attr({class:'marcadoMorado marcado',
							estado:color});
			});
		}
		else if(color == 7){
			quitarEspecial(objeto);	
			$(objeto).attr({class:'marcadoVerde marcado',
							estado:color});
		}
		else if(color == 8){
			limpiarLados(objeto);
			quitarEspecial(objeto);	
			$(objeto).parent().find('.corona').each(function(){
				$(this).attr({class:'marcadoAzul marcado',
							estado:color});
			});
		}
		else if(color == 9){
			quitarEspecial(objeto);	
			$(objeto).attr({class:'diente',
							estado:color});
		}
	}





</script>