<div class="col-xs-12 col-md-12 col-xl-12 row" >
	<h4 align="center"><u>Informe Psicologico</u></h4>
	<br>
		
	<div class="" role="tabpanel" data-example-id="togglable-tabs" >
		<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
			<li role="presentation" class="">
				<a href="#tab_content1" id="home-tab" role="tab" title="IDENTIFICACÓN DEL ESTUDIANTE" data-toggle="tab" aria-expanded="false">IDENTIFICACÓN</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content2" role="tab" id="profile-tab" title="DESCRIPCIÓN PSICOLOGICO" data-toggle="tab" aria-expanded="false">DESCRIPCIÓN PSICOLOGICO</a>
			</li>						
		</ul>
		
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
				<div class="col-xs-12 col-md-12 col-xl-12 row">					
						
					<h5>IDENTIFICACÓN DEL ESTUDIANTE: </h5> <br>
					<div  class="col-xs-12 col-md-3 col-xl-3">
						<?php echo $estudiante["foto"]?>
					</div>
					<div  class="col-xs-12 col-md-9 col-xl-9">
						
						<table class="table table-striped">
							<thead>

								<th>Nacionalidad</th>
			                    <th>Cédula</th>
			                    <th>Nombres</th>
			                    <th>Apellidos</th>
			                    <th>Sexo</th>
			                    <th>Fecha Nacimiento</th>			                    
								<th>Edad</th>
			                </thead>
			                <tbody>
			                  <tr>
								  <td><?php echo $estudiante["nacionalidad"] ?></td>
			                      <td><?php $estudiante["cedula"] ?></td>
			                      <td><?php echo $estudiante["nombres"] ?></td>
			                      <td><?php echo $estudiante["apellidos"] ?></td>
			                      <td><?php echo $estudiante["sexo"] ?></td>
			                      <td><?php echo $estudiante["fec_nac"] ?></td>
			                      <td><?php echo $estudiante["edad"] ?></td>
			                  </tr>
			                </tbody>
			              </table>

			              <table class="data table table-striped">
			                <thead>
			                  <tr>
			                    <th>Dirección</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <tr>
			                      <td><?php echo $estudiante["direccion"] ?></td> 
			                  </tr>
			                </tbody>
			              </table>

			              <table class="data table table-striped">
			                <thead>
			                  <tr>
			                    <th>Dirección de Correo Electrónico</th>
			                    <th>Telefono</th>
			                    <th>Celular</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <tr>
			                      <td><?php echo $estudiante["correo"] ?></td>
			                      <td><?php echo $estudiante["celular"] ?></td>
			                      <td><?php echo $estudiante["tel_hab"] ?></td> 
			                  </tr>
			                </tbody>
						</table>


						<table class="data table table-striped">
			                <thead>
			                  <tr>
			                    <th>Carrera</th>
			                    <th>Trayecto</th>
			                    <th>Trimestre</th>
			                    <th>Sección</th>
			                    <th>Turno</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <tr>
			                      <td><?php echo $estudiante["cod_carrera"] ?></td>
			                      <td><?php echo $estudiante["trayecto"] ?></td>
			                      <td><?php echo $estudiante["trimestre"] ?></td> 
			                      <td><?php echo $estudiante["seccion"] ?></td>
			                      <td><?php echo $estudiante["turno"] ?></td> 

			                  </tr>
			                </tbody>
						</table>
					</div>
				</div>
			</div>


				