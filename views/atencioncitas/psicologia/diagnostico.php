				<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="home-tab">
					<div class="col-xs-12 col-md-12 col-xl-12 row" id="formulario">
						<form id="atencioncitas-psicologico" action="../Controller/AtencionCitasController.php" method="POST">
							<h5>DESCRIPCIÓN PSICOLOGICO </h5> <br>
							<div class="col-xs-12 col-md-12 col-xl-12">
								<div class="form-group col-xs-12 col-md-12 col-xl-12">
									<label>Observaciones</label>
									 <textarea class="form-control textarea_estile" name="Observaciones" style="min-height: 290px !important; "></textarea>
	
									 <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton" style="padding-bottom: 12px; width: 100%; margin-top: 32px;">
								</div>


							</div>						
							
							<input type="hidden" name="accion" id="accion" value="AtencionCitas">
						    <input type="hidden" name="IdCita" id="IdCita" value="<?php echo $idCita ?>">
						    <input type="hidden" name="TipoInforme" id="TipoInforme" value="1">
						    <input type="hidden" name="Estudiante" id="Estudiante" value="<?php echo $cita["IdEstudiante"] ?>">
							
						</form>

						<div class="col-xs-12 col-md-12 col-xl-12">
							<div class="form-group col-xs-12 col-md-12 col-xl-12">
								<h5>Citas Atendidas</h5>
								 <table class="table table-striped" id="tratamientos">
									<thead>
										<th>Fecha</th>
										<th>Hora</th>
										<th></th>
									</thead>
									<tbody>
										<?php 
					                        if (is_array($seguimientos) || is_object($seguimientos))
					                        {
					                            
					                            foreach ($seguimientos as $seguimiento)
					                            {
					                            	echo "<tr>
							                                <td>".$seguimiento["Fecha"]."</td>
							                                <td>".$seguimiento["Hora"]."</td>
							                                <td>".$seguimiento["boton"]."</td>
							                             </tr>";
					                            }
					                        }

					                     ?>									
										
									</tbody>
								</table>
							</div>							 
						</div>

					</div>
				</div>
				<!-- Continuacion del cierre de etiquetas de la pagina Identificacion.php -->
			</div>
		</div>
	
</div>

   
        <!-- /.row -->
 <script type="text/javascript">
    $(document).ready(function(){
$('#tratamientos').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de tratamientos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
   

    $('.selectpicker').selectpicker('refresh');
});




</script>

<script type="text/javascript">

	

	$('#formulario').on('submit', '#atencioncitas-psicologico', function (e) {
	    e.preventDefault();


	    document.getElementById("boton").disabled = true;
	    var parametros= new FormData($(this)[0]);
	    $.ajax({
	        type: $(this).attr('method'),
	        url: $(this).attr('action'),
	        data: parametros,
	        contentType:false,
	        processData:false,
	        success:function(data){
	          respuesta = parseInt(data);
	          
	           if (respuesta==1) {
	              redireccionar('../Controller/GestionCitasController.php');
	              notificacion(3,'fa fa-check','Completado!','Se ha atendido al Estudiante');
	            }
	            else{
	              notificacion(2, 'fa fa-times-circle','Error!',data);
	            }
	            
	           
	            
	        }
	    });
	    document.getElementById("boton").disabled = false;
	  });


</script>