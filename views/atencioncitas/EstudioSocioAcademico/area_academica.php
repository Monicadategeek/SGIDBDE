			<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="home-tab">
				<div class="col-xs-12 col-md-12 col-xl-12 row">
					
					<h5>2.- ÁREA ACADÉMICA: </h5>
					<br>
					<div  class="col-xs-12 col-md-6 col-xl-6">
						<table class="table table-striped table-bordered">
							<tr>
								<th width="30%">2.1 Título de Bachiller</th>
								<th width="50%">Tipo de Institución donde cursó bachillerato</th>
								<th width="30%">2.2 Promedio de Notas del Bachillerato:</th>
							</tr>
							<tr>
								<td><?php if (isset($datosacademicos["mencion"])) { echo $datosacademicos["mencion"];}?></td>
								<td><?php if (isset($datosacademicos["tipo_inst"])) { echo $datosacademicos["tipo_inst"];}?></td>
								<td><?php if (isset($datosacademicos["prom_notas_bach"])) { echo $datosacademicos["prom_notas_bach"];}?></td>
							</tr>							
						</table>

						<table class="table table-striped table-bordered">
							<tr>
								<th width="50%">2.3 Carrera que cursa en el IUTOMS</th>
								<td width="50%">Que le motivo escoger esa carrera</td>
								
							</tr>
							<tr>
								<td><?php echo $estudiante["cod_carrera"] ?></td>
								<td><?php if (isset($datosacademicos["motivo_pnf"])) { echo $datosacademicos["motivo_pnf"];}?></td>
								
							</tr>
							<tr>
								<td>¿Se siente identificado con el PNF? </td>
								<td>¿Le gusta el Programa Nacional de Formación de su carrera?</td>
							</tr>

							<tr>
								<td><?php if (isset($datosacademicos["identidad_pnf"])) { echo $datosacademicos["identidad_pnf"];}?></td>
								<td><?php if (isset($datosacademicos["te_gusta"])) { echo $datosacademicos["te_gusta"];}?></td>
							</tr>
						</table>

						<table class="table table-striped table-bordered">	

							<tr>
								<th>2.4 Turno</th>
								<td>N° Expediente</td>
								<td>Fecha de Ingreso</td>
								<td>Forma de Ingreso</td>
							</tr>
							<tr>
								<td><?php echo $estudiante["turno"] ?></td>
								<td><?php echo $estudiante["cedula"] ?></td>
								<td><?php echo $estudiante["fec_ingreso"] ?></td>
								<td><?php echo $estudiante["forma_ingreso"] ?></td>
							</tr>
						</table>
							
						<table class="table table-striped table-bordered">	
							<tr>
								<th>2.5 Trayecto</th>
								<td>Trimestre</td>
								<td>Indice Académico</td>
							</tr>
							<tr>
								<td><?php echo $estudiante["trayecto"] ?></td>
								<td><?php echo $estudiante["trimestre"] ?>  </td>
								<td><?php echo $estudiante["creditos"] ?></td>
							</tr>

						</table>
						

					</div>	

					<div class="col-xs-12 col-md-6 col-xl-6">
						<table class="data table table-striped">
			                <thead>
			                  <tr>
			                    <th>Trayecto</th>
			                    <th>Codigo</th>
			                    <th>Unidad Curricular</th>
			                    <th>Creditos</th>
			                    <th>Nota</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <?php
			                  $trayecto="";
			                  if (count($notas)>0) {
			                     foreach ($notas as $nota) {
			                      if ($nota["trayecto"]!='') {
			                        echo "<tr>";
			                        if ($trayecto!=$nota["trayecto"]) {
			                            $trayecto=$nota["trayecto"];
			                            echo "<td>".$trayecto."</td>";
			                        }
			                        else{
			                            echo "<td></td>";
			                        }
			                        echo "
			                        <td>".$nota["cod_catedra"]."</td>
			                        <td>".$nota["nombremateria"]."</td>
			                        <td>".$nota["creditos"]."</td>
			                        <td>".$nota["nota"]."</td>

			                        <td></td>
			                        ";
			                        echo "</tr>";
			                      }                        
			                    }
			                  }
			                  else{
			                    echo "<tr><td colspan='5'>Disculpe, el IUTOMS Siace no tiene registrada notas del estudiante</td></tr>";
			                  }
			                  ?>
			                </tbody>
			              </table>
					</div>
				</div>	
			</div>


	