<div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="home-tab">
<div class="col-xs-12 col-md-12 col-xl-12 row">
	<div  class="col-xs-12 col-md-12 col-xl-12">
		<h5>6.- ÁREA DE INTERES PARA EL ESTUDIANTE: </h5>
		<div class="form-group col-xs-12 col-md-12" >
            <label for="consideracion">6.1 Por qué considera que requiere alguna de las ayudas socioeconómicas que brinda el IUTOMS</label>
            <textarea class="form-control textarea_estile" name="consideracion"><?php echo $datosinteressocioeconomico["consideracion"];?></textarea>
         </div>

         <div class="form-group col-xs-12 col-md-12" >
            <label for="consideracion">6.2 Modalidad de ayuda Socioeconómica que usted requiere</label>
            <select id="id_ayud" name="id_ayud" class="form-control selectpicker" title="-Seleccione-">
              <?php 
              if (is_array($becas) || is_object($becas))
              {
                  foreach ($becas as $beca)
                  {
                  	if ($datosinteressocioeconomico["id_ayud"]==$beca["IdTipoBeca"]) {
                  		echo "<option value='".$beca["IdTipoBeca"]."' selected>".$beca["Descripcion"]."</option>";
                  	}
                  	else{
                  		echo "<option value='".$beca["IdTipoBeca"]."'>".$beca["Descripcion"]."</option>";
                      
                  	}
                  }
              }
              ?>
              </select>
         </div>
	
		<div class="form-group col-xs-12 col-md-12" >
            <label for="prepayu">6.3 En caso de quedar en la Ayudantia o la Preparaduria esta de acuerdo en cumplir las horas que se piden para esta modalidad de Ayuda Socioeconómica</label>
            <div class="radio">
              <label>
                  <input type="radio" name="prepayu" id="prepayu1" value="1">Si
              </label>
              <label>
                  <input type="radio" name="prepayu"  id="prepayu2" value="0" >No
              </label>
          	</div>
          	<p>Declaro que la información y datos suministrados en este estudio son verdaderos y autorizo la comprobación de los mismos. De comprobarse que ha incurrido en falsedad esto, dará motivo a la anulación de la solicitud de la ayuda socioeconómica</p>
         </div>
	</div>	
</div>

</div>