<div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="home-tab">
<div class="col-xs-12 col-md-12 col-xl-12 row">
	<div  class="col-xs-12 col-md-12 col-xl-12">
		<h5>7.- SOLO PARA EL USO DEL (LA) TRABAJADOR (A) SOCIAL</h5>
		<div class="form-group col-xs-12 col-md-12" >
            <label for="consideracion">7.1 Diagnostico Social:</label>
            <textarea class="form-control textarea_estile" name="diagnosticosocial" required=""></textarea>
         </div>
		<div class="form-group col-xs-12 col-md-12" >
			<div class="col-xs-12 col-md-9 col-xl-9">
		        <table border class="table table-bordered table-hover">
		        	
		            <tbody>
						<tr>
							<td width="50%"><label>7.2 Urbanización social segun la escala de Graffar</label></td>
							<td>
								<select name="usseg" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>

						<tr>
							<td><label>7.2.1 Institución del responsable económico</label></td>
							<td>
								<select name="ireconomico" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>

						<tr>
							<td><label>7.2.2 Ocupación del responsable económico</label></td>
							<td>
								<select name="oreconomico" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>

						<tr>
							<td><label>7.2.3 Fuentes de ingreso del responsable económico</label></td>
							<td>
								<select name="fireconomico" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>

						<tr>
							<td><label>7.2.4 Condiciones de la vivienda de los padres</label></td>
							<td>
								<select name="cvpadres" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>


						<tr>
							<td><label>7.2.5 Ubicación de la vivienda de los padres</label></td>
							<td>
								<select name="uvpadres" class="form-control selectpicker" required="" title="-Seleccione-">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>

							</td>
						</tr>

						<tr>
							<td><label>7.3 Conclusiones y Recomendaciones</label></td>
							<td>
								 <textarea class="form-control textarea_estile" required="" name="conclurecomen" style="min-width: 100% !important;"></textarea>

							</td>
						</tr>
		            </tbody>
	            </table>
	         </div>
	    
			  <div class="form-group col-xs-12 col-md-6 col-xl-3">
		      <label class="control-label" for="BecaAsociada">Modalidad de la beca</label>
		      <select id="BecaAsociada" name="BecaAsociada" class="form-control selectpicker"  required="" title="-Seleccione-">
		      <?php 
		      foreach ($becasdisp as $becadisp) {
		        if (($becadisp["NroCupos"]-$becadisp["Inscritos"])>0) {
		           echo '<option data-subtext="'." Disp: ".($becadisp["NroCupos"]-$becadisp["Inscritos"]).'" value="'.$becadisp["IdSinceracionBecas"].'">'.$becadisp["TipoBeca"].'</option>';
		        }       
		       }
		      ?>
		      <option value="0">Ninguno</option>
		      </select>
		    </div>
		    <input type="hidden" name="accion" id="accion" value="AtencionCitas">
		    <input type="hidden" name="agregarprog" id="agregarprog" >
		    
		    <input type="hidden" name="IdCita" id="IdCita" value="<?php echo $encuesta["cita"]; ?>">
		    <input type="hidden" name="encuesta" id="encuesta" value="<?php echo $encuesta["Id"]; ?>">

		    <div class="form-group  col-md-6 col-xs-12">
		      <label class="control-label" for="BecaAsociada">Agregar al Programa</label>

		      <button type="submit" class="btn btn-success "  id="botonsi" onclick="agregarProg(1);">Si</button>
		      <button type="submit" class="btn btn-danger " id="botonno" onclick="agregarProg(2);">No</button>
		    </div>

			</div>


	</div>
</div>


</div>



 </div>
  </div>
</div>

</form>

<script type="text/javascript">
	function agregarProg(i) {

		$('#agregarprog').val(i);
	}


	

	$('#formulario').on('submit', '#atencioncitas-estudiossocioeconomicos', function (e) {
	    e.preventDefault();


	    document.getElementById("botonsi").disabled = true;
		document.getElementById("botonno").disabled = true;
	    var parametros= new FormData($(this)[0]);
	    $.ajax({
	        type: $(this).attr('method'),
	        url: $(this).attr('action'),
	        data: parametros,
	        contentType:false,
	        processData:false,
	        success:function(data){
	          respuesta = parseInt(data);
	          
	           if (respuesta==1) {
	              redireccionar('../Controller/GestionCitasController.php');
	              notificacion(3,'fa fa-check','Completado!','Se ha atendido al Estudiante');
	            }
	            else{
	              notificacion(2, 'fa fa-times-circle','Error!',data);
	            }
	            
	           
	            
	        }
	    });
	    document.getElementById("botonsi").disabled = false;
		document.getElementById("botonno").disabled = false;
	  });


</script>