

<div class="col-xs-12 col-md-12 col-xl-12 row">
	<h4 align="center"><u>ESTUDIO SOCIACADÉMICO PARA LA ASIGNACIÓN DE AYUDAS ECONÓMICAS</u></h4>
	<br><br>

	<div class="" role="tabpanel" data-example-id="togglable-tabs" id="formulario">
		<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
			<li role="presentation" class="">
				<a href="#tab_content1" id="home-tab" role="tab" title="IDENTIFICACÓN DEL ESTUDIANTE" data-toggle="tab" aria-expanded="false">IDENTIFICACÓN</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">ACADÉMICA</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">SOCIOECONOMICA</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content4" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">FISICO - AMBIENTAL</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content5" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">CULTURAL - DEPORTIVA</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content6" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"> INTERES</a>
			</li>
			<li role="presentation" class="">
				<a href="#tab_content7" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"> DIAGNOSTIGO</a>
			</li>

		</ul>

		<form id="atencioncitas-estudiossocioeconomicos" action="../Controller/EncuestaController.php" method="POST">

		
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
				<div class="col-xs-12 col-md-12 col-xl-12 row">					
						
					<h5>1.- IDENTIFICACÓN DEL ESTUDIANTE: </h5> <br>
					
					<div  class="col-xs-12 col-md-3 col-xl-3">
						<?php echo $estudiante["foto"]?>
					</div>
					<div  class="col-xs-12 col-md-9 col-xl-9">
						
						<table class="table table-striped table-bordered">
							<tr>
								<th width="20%">Apellidos</th>
								<th width="20%">Nombres</th>
								<th width="10%">Nacionalidad</th>
								<th width="20%">Cédula de Identidad</th>
								<th width="10%">Sexo</th>
								<th width="15%">Estado Civil</th>
								<th width="5%">Edad</th>
							</tr>

							<tr>
								<td><?php echo $estudiante["apellidos"]?></td>
								<td><?php echo $estudiante["nombres"]?></td>
								<td><?php echo $estudiante["nacionalidad"]?></td>
								<td><?php echo $estudiante["cedula"]?></td>
								<td><?php echo $estudiante["sexo"]?></td>
								<td><?php echo $estudiante["edo_civil"]?></td>
								<td><?php echo $estudiante["edad"]?></td>
							</tr>
							
							<tr>
								<th colspan="7">Dirección de Habitación</th>
							</tr>
							<tr>
								<th colspan="7"><?php echo $estudiante["direccion"]?></th>
							</tr>

							<tr>
								<th colspan="2">Teléfono Local</th>
								<th colspan="2">Teléfono Celular</th>
								<th colspan="3">Correo Electrónico</th>
							</tr>

							<tr>
								<td colspan="2"><?php echo $estudiante["tel_hab"]?></td>
								<td colspan="2"><?php echo $estudiante["celular"]?></td>
								<td colspan="3"><?php echo $estudiante["correo"]?></td>
							</tr>

						</table>
					</div>
				</div>
			</div>  
			
