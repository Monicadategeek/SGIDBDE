<div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="home-tab">
<div class="col-xs-12 col-md-12 col-xl-12 row">
	<div  class="col-xs-12 col-md-12 col-xl-12">
		<h5>4.- ÁREA FISICO - AMBIENTAL: </h5>

		 <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="col-md-12">
         <label>4.1 Identificación de la comunidad donde resides</label>
        </div>
        <div class="form-group col-xs-12 col-md-3" id="estado">
          <label class="control-label" for="id_estado">Estado </label>
          <select id="id_estado" name="id_estado" class="form-control selectpicker" required="" title="-Seleccione-">
          <?php 
          if (is_array($estados) || is_object($estados))
          {
              foreach ($estados as $estados)
              {
              	if ($estados["id_estado"]==$datosambientales["id_estado"]) {
              		 echo "<option value='".$estados["id_estado"]."' selected>".$estados["estado"]."</option>";
              	}
              	else{
              		 echo "<option value='".$estados["id_estado"]."'>".$estados["estado"]."</option>";
                 
              	}
              }

          }
          ?>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-3" id="municipio">
          <label class="control-label" for="id_municipio">Municipio </label>
  			  <select id="id_municipio" name="id_municipio" class="form-control selectpicker" required="" title="-Seleccione-">
  			  <?php 
  			  if (is_array($municipios) || is_object($municipios))
  			  {
  			      foreach ($municipios as $municipio)
  			      {
  			      	if ($municipio["id_municipio"]==$datosambientales["id_municipio"]) {
            		 	echo "<option value='".$municipio["id_municipio"]."' selected>".$municipio["municipio"]."</option>";
              	}
              	else{
              		echo "<option value='".$municipio["id_municipio"]."'>".$municipio["municipio"]."</option>";
                 
              	}    				          
  			      }
  			  }
  			  ?>
  			  </select>
        </div>
        <div class="form-group col-xs-12 col-md-3" id="parroquia">
          <label class="control-label" for="id_parroquia">Parroquia </label>
          <select id="id_parroquia" name="id_parroquia" class="form-control selectpicker" required="" title="-Seleccione-">
          <?php 
          if (is_array($parroquias) || is_object($parroquias))
          {
              foreach ($parroquias as $parroquia)
              {
                if ($parroquia["id_parroquia"]==$datosambientales["id_parroquia"]) {
                  echo "<option value='".$parroquia["id_parroquia"]."' selected>".$parroquia["parroquia"]."</option>";
                }
                else{
                  echo "<option value='".$parroquia["id_parroquia"]."'>".$parroquia["parroquia"]."</option>";
                 
                }

                  
              }
          }
          ?>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-3" id="ciudad">
          <label class="control-label" for="id_ciudad">Ciudad </label>
          <select id="id_ciudad" name="id_ciudad" class="form-control selectpicker" required="" title="-Seleccione-">
          <?php 
          if (is_array($ciudades) || is_object($ciudades))
          {
              foreach ($ciudades as $ciudad)
              {
                if ($ciudad["id_ciudad"]==$datosambientales["id_ciudad"]) {
                  echo "<option value='".$ciudad["id_ciudad"]."' selected>".$ciudad["ciudad"]."</option>";
                }
                else{
                  echo "<option value='".$ciudad["id_ciudad"]."'>".$ciudad["ciudad"]."</option>";
                 
                }

              }
          }
          ?>
          </select>
        </div>
    </div>


    <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tip_viv">4.2 Tipo de Vivienda</label>
              <select id="id_tip_viv" name="id_tip_viv" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tpviviendas) || is_object($tpviviendas))
              {
                  foreach ($tpviviendas as $tpvivienda)
                  {
                    if ($tpvivienda["id_tipo_viv"]==$datosambientales["id_tip_viv"]) {
                      echo "<option value='".$tpvivienda["id_tipo_viv"]."' selected>".$tpvivienda["desc_tipo_viv"]."</option>";
                    }
                    else{
                      echo "<option value='".$tpvivienda["id_tipo_viv"]."'>".$tpvivienda["desc_tipo_viv"]."</option>";
                     
                    }                     
                  }
              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_ten_viv">4.3 Tenencia de la Vivienda</label>
              <select id="id_ten_viv" name="id_ten_viv" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tenecviv) || is_object($tenecviv))
              {
                  foreach ($tenecviv as $tenecviv)
                  {
                    if ($tenecviv["id_ten_viv"]==$datosambientales["id_ten_viv"]) {
                     echo "<option value='".$tenecviv["id_ten_viv"]."' selected>".$tenecviv["desc_ten_viv"]."</option>";
                    }
                    else{
                      echo "<option value='".$tenecviv["id_ten_viv"]."'>".$tenecviv["desc_ten_viv"]."</option>";
                     
                    } 

                      
                  }

              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="constr_viv">4.4 Construcción</label>
              <select id="constr_viv" name="constr_viv" class="form-control selectpicker" required="" title="-Seleccione-">
                <?php 
                  if ($datosambientales["constr_viv"]==1) {
                    echo '<option value="1" selected>Totalmente Acabada</option>
                          <option value="2">En proceso de Construcción</option>';
                  }
                  else{
                    echo '<option value="1" >Totalmente Acabada</option>
                          <option value="2" selected>En proceso de Construcción</option>';
                  }
                ?>
              </select>
            </div>
        </div>


        <div class="col-xs-12 col-md-12 col-xl-12">
            <p>4.5 Características de la Vivienda</p>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_pared">Tipo de Pared</label>
              <select id="id_tipo_pared" name="id_tipo_pared" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tpparedes) || is_object($tpparedes))
              {
                  foreach ($tpparedes as $tppared)
                  {
                    if ($tppared["id_tipo_pared"]==$datosambientales["id_tipo_pared"]) {
                     echo "<option value='".$tppared["id_tipo_pared"]."' selected>".$tppared["desc_tipo_pared"]."</option>";
                    }
                    else{
                      echo "<option value='".$tppared["id_tipo_pared"]."'>".$tppared["desc_tipo_pared"]."</option>";
                     
                    }                      

                  }
              }
              ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_piso">Tipo de Piso</label>
              <select id="id_tipo_piso" name="id_tipo_piso" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tppisos) || is_object($tppisos))
              {
                  foreach ($tppisos as $tppiso)
                  {
                    if ($tppiso["id_tipo_piso"]==$datosambientales["id_tipo_piso"]) {
                     echo "<option value='".$tppiso["id_tipo_piso"]."' selected>".$tppiso["desc_tipo_piso"]."</option>";
                    }
                    else{
                      echo "<option value='".$tppiso["id_tipo_piso"]."'>".$tppiso["desc_tipo_piso"]."</option>";
                     
                    }                         
                  }

              }
              ?>
              </select>
            </div>            

            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_techo">Tipo de Techo</label>
              <select id="id_tipo_techo" name="id_tipo_techo" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tptechos) || is_object($tptechos))
              {
                  foreach ($tptechos as $tptecho)
                  {
                    if ($tptecho["id_tipo_techo"]==$datosambientales["id_tipo_techo"]) {
                     echo "<option value='".$tptecho["id_tipo_techo"]."' selected>".$tptecho["desc_tipo_techo"]."</option>";
                    }
                    else{
                      echo "<option value='".$tptecho["id_tipo_techo"]."' >".$tptecho["desc_tipo_techo"]."</option>";
                     
                    }

                      
                  }
              }
              ?>
              </select>
            </div>
        </div>

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-3 col-xl-3">
                <label for="identidad_pnf">Espacios que componen su vivienda</label>
                <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                    <tbody>
                        <tr>
                            <th width="50%">Sala</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="sala" title="" required="nro de sala" value="<?php echo $datosambientales["sala"]?>">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Cocina</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="cocina" title="nro de cocina" required="" value="<?php echo $datosambientales["cocina"]?>">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Baños</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="banios" title="nro de baños" required="" value="<?php echo $datosambientales["banios"]?>">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Dormitorios</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="dormitorios" title="nro de dormitorios" required="" value="<?php echo $datosambientales["dormitorios"]?>">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr> 
                    </tbody>
                    
                </table>                
            </div>
            <div class="form-group col-xs-12 col-md-3 col-xl-3">
                <label for="identidad_pnf">Servicios publicos con los que cuenta su vivienda su vivienda</label>
                <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                    <tbody>
                        <tr>
                            <th width="50%">Luz</th>
                            <td>
                                <div class="radio">
                                  <?php 
                                    if ($datosambientales["luz"]==1) {
                                      echo '<label>
                                                <input type="radio" name="luz"  value="1" checked="" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="luz" value="0" >No
                                            </label>';
                                    }
                                    else{
                                      echo '<label>
                                                <input type="radio" name="luz"  value="1" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="luz" value="0" checked="" >No
                                            </label>';
                                    }
                                  ?>                                    
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Agua</th>
                            <td>
                                <div class="radio">
                                  <?php 
                                    if ($datosambientales["agua"]==1) {
                                      echo '<label>
                                                <input type="radio" name="agua"  value="1" checked="" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="agua" value="0" >No
                                            </label>';
                                    }
                                    else{
                                      echo '<label>
                                                <input type="radio" name="agua"  value="1" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="agua" value="0" checked="" >No
                                            </label>';
                                    }
                                  ?>  
                                   
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Aseo Urbano</th>
                            <td>
                                <div class="radio">
                                  <?php 
                                    if ($datosambientales["aseo_urb"]==1) {
                                      echo '<label>
                                                <input type="radio" name="aseo_urb"  value="1" checked="" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="aseo_urb" value="0" >No
                                            </label>';
                                    }
                                    else{
                                      echo '<label>
                                                <input type="radio" name="aseo_urb"  value="1" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="aseo_urb" value="0" checked="" >No
                                            </label>';
                                    }
                                  ?>                                   
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Telefono Local</th>
                            <td>
                                <div class="radio">
                                  <?php 
                                    if ($datosambientales["telf_local"]==1) {
                                      echo '<label>
                                                <input type="radio" name="telf_local"  value="1" checked="" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="telf_local" value="0" >No
                                            </label>';
                                    }
                                    else{
                                      echo '<label>
                                                <input type="radio" name="telf_local"  value="1" required>Si
                                            </label>
                                            <label>
                                                <input type="radio" name="telf_local" value="0" checked="" >No
                                            </label>';
                                    }
                                  ?>       
                                </div>
                            </td>
                        </tr> 
                    </tbody>
                    
                </table>                
            </div>
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
              <label for="consideracion">Distribución de las habitaciones: </label>
              <textarea class="form-control textarea_estile" name="dtrbHabitaciones" style="min-height: 247px !important;"></textarea>

            </div>
          </div>

          <div class="form-group">
          <label for="consideracion">Observaciones </label>
          <textarea class="form-control textarea_estile" name="areaficicoambiental" id="ficicoambiental"></textarea>
        </div>


        

  



	</div>	
</div>
</div>






<script type="text/javascript">
  $(document).ready(function()
    {
        $("#id_estado").change(function () {
            var  datos={"accion":'municipio', "estado":$('#id_estado option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'municipio');
            $('#municipio').show();

             var  datos={"accion":'ciudad', "estado":$('#id_estado option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'ciudad');
            $('#ciudad').show();          

            $('#parroquia').hide();    
        });

        $("#id_municipio").change(function () {
            var  datos={"accion":'parroquia', "municipio":$('#id_municipio option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'parroquia');
            $('#parroquia').show();            
            //$('#divhora').hide(); 
            //$('#completado').hide();    
        });
        
    });

    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
      $('.nro').mask('00');
     
    });
    

   

    

</script>