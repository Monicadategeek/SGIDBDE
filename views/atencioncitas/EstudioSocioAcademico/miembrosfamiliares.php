
	<div align="right" style="padding: 10px">
		<button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/MiembrosFamiliaresController.php?accion=create&estudiante=<?php echo $estudiante?>')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Miembro</button>
        
	</div> 
<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0 !important;">

		<table class="table table-striped table-bordered" cellspacing="0" width="100%" id="familias"> 
	        <thead>
	            <th>N°</th>
	        	<th>Nombres</th>
	        	<th>Apellidos</th>        	
	            <th>Parentesto</th>      	
	            <th>Edad</th>
	            <th>Edo. Civil</th>
	            <th>Sexo</th>
	            <th>Nivel Instrucción</th>
	            <th>Profesión u Oficio</th>
	            <th>Ingreso Mensual</th>
	            <th>Acciones</th>
	        </thead>

	        <tbody>
	            <?php 
	                if (is_array($familiares) || is_object($familiares))
	                {
	                	$cont=0;
	                	$total=0;
	                    foreach ($familiares as $familia)
	                    {
	                        $cont=$cont+1;
	                        echo "<tr>
	                        <td>".$cont."</td>
	                        <td>".$familia["nombres"]."</td>
	                        <td>".$familia["apellidos"]."</td>
	                        <td>".$familia["id_parentesco"]."</td> 
	                        <td>".$familia["edad"]."</td>
	                        <td>".$familia["id_edo_civ"]."</td>
	                        <td>".$familia["genero"]."</td>
	                        <td>".$familia["id_grado_instr"]."</td>
	                        <td>".$familia["id_prof_ocup"]."</td>
	                        <td>".$familia["ingreso_mensual"]."</td>";   
	                        $total=$total+$familia["ingreso_mensual_num"];
	                        echo '<td><button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/MiembrosFamiliaresController.php?',".$familia["id_grupo_fam_est"].');"><i class="fa fa-pencil"></i></button>';
	                         echo '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="eliminar_miembrosfamiliares('.$familia["id_grupo_fam_est"].', '.$estudiante.');" style="margin-left: 3px;;"><i class="fa fa-trash-o"></i></button>';                                                             
	                        echo "</td></tr>";
	                    }
	                }
	            ?>
	        </tbody>
		</table>  
	</div>