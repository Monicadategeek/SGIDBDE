<div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="home-tab">


  <div class="col-xs-12 col-md-12 col-xl-12 row">
    
    <h5>3.- ÁREA SOCIOECONOMICA DEL ESTUDIANTE: </h5>
    <br>

    <table class="table table-striped table-bordered">
      <tr> 
          <th>3.1 Trabaja</th>
          <th>Ha trabajado</th>
          <th>3.2 Nombre de la empresa</th>
          <th>Cargo</th>
      </tr>
      <tr>
        <td><?php echo $datossocioeco["Trabaja"] ?></td>
        <td><?php echo $datossocioeco["HaTrabajado"] ?></td>
        <td><?php echo $datossocioeco["nombre_trabajo"] ?></td>
        <td><?php echo $datossocioeco["cargo_trabajo"] ?></td>
      </tr>
      <tr>
          <th>3.3 En donde trabaja</th>
          <th>3.4 Cuanto gana</th>
          <th>Horario</th>
          <th></th>
      </tr>
      <tr>
        <td><?php echo $datossocioeco["donde_trabaja"] ?></td>
        <td><?php echo $datossocioeco["sueldo_mensual"] ?></td>
        <td><?php echo $datossocioeco["horario_trabajo"] ?></td>
        <td></td>
      </tr>
      <?php
        if ($datossocioeco["Trabaja"]=='NO' and $datossocioeco["HaTrabajado"]=='SI') {
          echo '<tr>
                <th colspan="4">Razón del retiro del trabajo</th>
              </tr>
              <tr>
                <td colspan="4">'.$datossocioeco["RazRetTrab"].'</td>
              </tr>';
        }
      ?>
      <tr>
          <th>3.5 Presenta problemas para trasladarse al IUTOMS</th>
          <th>¿Como se traslada al IUTOMS?</th>
          <th>3.6 Consume las tres comidas</th>
          <th>De ser negativo porqué</th>
      </tr>
      <tr>
        <td><?php echo $datossocioeco["problemas_traslado"] ?></td>
        <td><?php echo $datossocioeco["tipo_traslado_iutoms"] ?></td>
        <td><?php echo $datossocioeco["tres_comidas"] ?></td>
        <td><?php echo $datossocioeco["tres_comidas_neg"] ?></td>
      </tr>
    </table>

    <div  class="col-xs-12 col-md-12 col-xl-12">
      <p>3.7 Recibes ayuda económica: <u><?php echo $datossocioeco["ayudaseconomica"] ?></u>  </p>
      <?php 
      if (count($ayudaseconomica)>0) {
        ?>
        <p>De ser afirmativo quien te ayuda:</p>
        <table class="table table-bordered table-hover">
          <tr>
          <th>Origen de la Ayuda</th>
          <th>Monto en Bs</th>
          </tr>
          <?php
          foreach ($ayudaseconomica as $ayudaeconomica) {
            echo "<tr>
            <td>".$ayudaeconomica["ayud_econ"]."</td>
            <td>".$ayudaeconomica["monto"]."</td>
            </tr>"	;
          }?>

        </table>
        <?php
      }
      ?>
      <p>3.8 Tienes hijos: <u><?php echo $datossocioeco["Thijos"] ?></u> 
      <?php
      if ($datossocioeco["hijos"]>0) {
        echo 'Cuantos: <u>'.$datossocioeco["hijos"] .'</u>  Viven contigo: <u>'. $datossocioeco["viven_contigo"];
      }
      echo '</u>  Tienes pareja: <u>'.$datossocioeco["pareja"].'</u>'.'</u> Alguna persona depende de Usted: <u>'.$datossocioeco["pers_dep"].'</u>' ;
      if ($datossocioeco["pers_dep_nro"]>0) {
        echo '</u> de ser positivo cuantas: <u>'.$datossocioeco["pers_dep_nro"] .'</u>';    
      } 
      echo '</p>';   
      ?> 

      <h5>3.9 Grupo Familiar con quien reside actualmente: </h5>
      <div id="miembros">
        <?php require_once("miembrosfamiliares.php"); ?>
      </div>
      <div class="form-group">
        <label for="consideracion">Observación </label>
        <textarea class="form-control textarea_estile" name="grupofamiliar" id="grupofamiliar"></textarea>
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="form-group col-xs-12 col-md-4 col-xl-4">
          <label for="TotIngEst">Total Aprox Ingreso del Estudiante (Mensual)</label>
          <input type="text" class="form-control bs" id="TotIngEst" name="TotIngEst" title="Total Aprox Ingreso del Estudiante (Mensual)" required value="<?php echo $datossocioeco["TotIngEst"] ?>">
          <div class="help-block with-errors" ></div>
        </div>
        <div class="form-group col-xs-12 col-md-4 col-xl-4">
           <label for="TotIngEst">Total de Egresos del Estudiante (Mensual)</label>
          <input type="text" class="form-control bs" id="Ttotalind" readonly="" >
          <div class="help-block with-errors" ></div>
        </div>
        
    </div>   

    <div class="col-xs-12 col-md-12 col-xl-12">
      <h3 align="center">EGRESOS MENSUALES DEL GRUPO INDIVIDUAL</h3>
      <div class="form-group col-xs-12 col-md-4 col-xl-4">

        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
          <thead>
            <th>Concepto</th>
            <th width="50%">Bolivares</th>                    
          </thead>
          <tbody>
            <?php 
            foreach ($gastosindividuales as $gasto) {
              echo '<tr>
              <td>'.$gasto["Descripcion"].'</td><td>';

              if ($gasto["Descripcion"]=='TOTAL') {
                
                echo '<input type="text" readonly="" class="form-control bs" id="totalind" value="'.$gasto["monto"].'"';
              }
              else{
                echo '
                <input type="hidden" id="valprevegrind'.$gasto["IdEgreso"].'" value="'.$gasto["monto"].'">
                <input type="text" class="form-control bs" id="egrind'.$gasto["IdEgreso"].'" name="egrind'.$gasto["IdEgreso"].'" title="'.$gasto["Descripcion"].' Individual" required="" value="'.$gasto["monto"].'" onchange="sumar('."'egrind".$gasto["IdEgreso"]."', 'totalind', this.value);".'"><div class="help-block with-errors" ></div>';
              }
              
              
              echo "</td>
              </tr>";
            }

            ?>                
          </tbody>
        </table>   

      </div>
      <div class="form-group col-xs-12 col-md-8 col-xl-8">
        <label for="consideracion">OBSERVACIÓN:</label>
        <textarea class="form-control textarea_estile"  name="OBSegrind" id="OBSegrind" style="min-height: 622px !important;">Balance Egreso/Ingreso 
        N° Personas Económicamente  Dependiente:
        </textarea>
      </div>
    </div>


    <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="form-group col-xs-12 col-md-4 col-xl-4">
          <label for="TotIngFam">Total Aprox Ingreso Familiar (Mensual)</label>
          <input type="text" class="form-control bs" id="TotIngFam" name="TotIngFam" title="Total Aprox Ingreso Familiar (Mensual)" required value="<?php echo $datossocioeco["TotIngFam"] ?>">
          <div class="help-block with-errors" ></div>
        </div>
        <div class="form-group col-xs-12 col-md-4 col-xl-4">
           <label for="TotIngEst">Total de Egresos Familiar (Mensual)</label>
          <input type="text" class="form-control bs" id="Ttotalfam" readonly="" >
          <div class="help-block with-errors" ></div>
        </div>
        
    </div>  


    <div class="col-xs-12 col-md-12 col-xl-12">
      <h3 align="center">EGRESOS MENSUALES DEL GRUPO FAMILIAR</h3>
      <div class="form-group col-xs-12 col-md-4 col-xl-4">

        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
          <thead>
            <th>Concepto</th>
            <th width="50%">Bolivares</th>                    
          </thead>
          <tbody>
            <?php 
            foreach ($gastosfamiliares as $gasto) {
              echo '<tr>
              <td>'.$gasto["Descripcion"].'</td><td>';

              if ($gasto["Descripcion"]=='TOTAL') {
                
                echo '<input type="text" readonly="" class="form-control bs" id="totalfam" value="'.$gasto["monto"].'"';
              }
              else{
                echo '
                <input type="hidden" id="valprevegrfam'.$gasto["IdEgreso"].'" value="'.$gasto["monto"].'">
                <input type="text" class="form-control bs" id="egrfam'.$gasto["IdEgreso"].'" name="egrfam'.$gasto["IdEgreso"].'" title="'.$gasto["Descripcion"].' Individual" required="" value="'.$gasto["monto"].'" onchange="sumar('."'egrfam".$gasto["IdEgreso"]."', 'totalfam', this.value);".'"><div class="help-block with-errors" ></div>';
              }             
              

            }

            ?>                
          </tbody>
        </table>            
      </div>
      <div class="form-group col-xs-12 col-md-8 col-xl-8">

        <label for="consideracion">OBSERVACIÓN:</label>
        <textarea class="form-control textarea_estile" name="OBSegrfam" id="OBSegrfam" style="min-height: 622px !important;">Balance Egreso/Ingreso 
        N° Personas Económicamente  Dependiente:
        </textarea>
      </div>
    </div>

  </div>
</div>
</p>

 

	<script type="text/javascript">
  $(document).ready(function() {
    
    $('#Ttotalind').val($('#totalind').val());
    $('#Ttotalfam').val($('#totalfam').val());

    $("input").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });
});

		$(document).ready(function(){
    	$('.bs').mask('0000000.00', {reverse: true});
      $('.selectpicker').selectpicker('refresh');
     
    });

    function sumar(input, hidden, value){
        
        total=$('#'+hidden).val();       

        total=total-$('#valprev'+input).val();
        total=parseFloat(total) +parseFloat(value);


        $('#T'+hidden).val(total);

        $('#'+hidden).val(total);
        $('#'+input).val(value);
        $('#valprev'+input).val(value);

    }


	</script>

