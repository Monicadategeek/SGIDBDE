<div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="home-tab">
<div class="col-xs-12 col-md-12 col-xl-12 row">
  <div  class="col-xs-12 col-md-12 col-xl-12">
    <h5>5.- ÁREA CULTURAL - DEPORTIVA: </h5>
    <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="actividades"></i>5.1 ¿En que Ocupas tu Tiempo Libre? <small>Puedes elegir multiple max(5)</small></label>
              <select id="actividades" name="actividades[]" class="form-control selectpicker" required="" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="5">
              <?php 
              $validador=0;
              if (is_array($actividades) || is_object($actividades))
              {
               
                foreach ($actividades as $actividad)
                {
                    $validador=0;
                    foreach ($actividadesest as $value) {
                        if ($value["id_act_t_l"]==$actividad["id_act_t_l"]) {
                            echo "<option value='".$actividad["id_act_t_l"]."' selected>".$actividad["desc_act_t_l"]."</option>";
                            $validador=1;
                            break;
                        }
                    }                   
                    if ($validador==0) {
                        echo "<option value='".$actividad["id_act_t_l"]."'>".$actividad["desc_act_t_l"]."</option>";
                    }
                }                
              }
              ?>
              </select>
          </div>
       </div>

       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="pracdeporte">5.2 ¿Practicas algun deporte?</label>
              <div class="radio">
              <?php 
                if (count($deportesest)>0) {
                    echo '<label>
                              <input type="radio" name="pracdeporte" id="pracdeporte1" value="1" checked="" required>Si
                          </label>
                          <label>
                              <input type="radio" name="pracdeporte"  id="pracdeporte2" value="0" >No
                          </label>';
                }
                else{
                    echo '<label>
                              <input type="radio" name="pracdeporte" id="pracdeporte1" value="1"  required>Si
                          </label>
                          <label>
                              <input type="radio" name="pracdeporte"  id="pracdeporte2" value="0" checked="">No
                          </label>';
                }
              ?>
                  
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="divdeportes">
              <label for="deportes">¿Cual Practicas? <small>Puedes elegir multiple max(4)</small></label>
              <select id="deportes" name="deportes[]" class="form-control selectpicker" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="4 ">
              <?php 
              if (is_array($deportes) || is_object($deportes))
              {
                  foreach ($deportes as $deporte)
                  {
                    $validador=0;
                    foreach ($deportesest as $value) {
                        if ($value["id_deporte"]==$deporte["id_cat_dep"]) {
                            echo "<option value='".$deporte["id_cat_dep"]."' selected>".$deporte["desc_cat_dep"]."</option>";
                            $validador=1;
                            break;
                        }
                    }                   
                    if ($validador==0) {
                       echo "<option value='".$deporte["id_cat_dep"]."'>".$deporte["desc_cat_dep"]."</option>";
                    }

                    
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>

       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="posehaar">5.3 ¿Posees Habilidades Artísticas?</label>
              <div class="radio">
                <?php 
                if (count($habilidadesest)>0) {
                    echo '<label>
                              <input type="radio" name="posehaar" id="posehaar1" value="1"  checked="" required>Si
                          </label>
                          <label>
                              <input type="radio" name="posehaar"  id="posehaar2" value="0" >No
                          </label>';
                }
                else{
                    echo '<label>
                              <input type="radio" name="posehaar" id="posehaar1" value="1"   required>Si
                          </label>
                          <label>
                              <input type="radio" name="posehaar"  id="posehaar2" value="0" checked="">No
                          </label>';
                }
              ?>

                  
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="divhabilidades">
              <label for="habilidades">¿Cual? <small>Puedes elegir multiple max(5)</small></label>
              <select id="habilidades" name="habilidades[]" class="form-control selectpicker" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="5">
              <?php 
              if (is_array($habilidades) || is_object($habilidades))
              {
                  foreach ($habilidades as $habilidad)
                  {
                     $validador=0;
                    foreach ($habilidadesest as $value) {
                        if ($value["id_habil"]==$habilidad["id_habil"]) {
                            echo "<option value='".$habilidad["id_habil"]."' selected>".$habilidad["desc_habilidad"]."</option>";

                            
                            $validador=1;
                            break;
                        }
                    }                   
                    if ($validador==0) {
                       echo "<option value='".$habilidad["id_habil"]."'>".$habilidad["desc_habilidad"]."</option>";
                    }

                      
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>
       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="actIUTOMS">5.4 ¿Has Participado en alguna actividad del IUTOMS?</label>
              <div class="radio">
                <?php  
                if (count($eventosest)>0) {
                    echo '<label>
                              <input type="radio" name="actIUTOMS" id="actIUTOMS1" value="1" checked="" required>Si
                          </label>
                          <label>
                              <input type="radio" name="actIUTOMS"  id="actIUTOMS2" value="0" >No
                          </label>';
                }
                else{
                    echo '<label>
                              <input type="radio" name="actIUTOMS" id="actIUTOMS1" value="1"  required>Si
                          </label>
                          <label>
                              <input type="radio" name="actIUTOMS"  id="actIUTOMS2" value="0" checked="">No
                          </label>';
                }
              ?>

                  
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="diveventos">
              <label for="eventos">¿Cual? <small>Puedes elegir multiple max(3)</small></label>
              <select id="eventos" name="eventos[]" class="form-control selectpicker"  title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="3">
              <?php 
              if (is_array($eventos) || is_object($eventos))
              {
                  foreach ($eventos as $evento)
                  { 
                     $validador=0;
                    foreach ($eventosest as $value) {
                        if ($value["id_evento"]==$evento["id_evento"]) {
                            echo "<option value='".$evento["id_evento"]."' selected>".$evento["desc_evento"]."</option>";
                            $validador=1;
                            break;
                        }
                    }                   
                    if ($validador==0) {
                       echo "<option value='".$evento["id_evento"]."'>".$evento["desc_evento"]."</option>";
                    }                      
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>

  </div>  
</div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
      

      if ($('input:radio[name=pracdeporte]:checked').val()==1) {
        $('#divdeportes').show();
      }
      else{
        $('#divdeportes').hide();
      }

      if ($('input:radio[name=posehaar]:checked').val()==1) {
        $('#divhabilidades').show();
      }
      else{
        $('#divhabilidades').hide();
      }

      if ($('input:radio[name=actIUTOMS]:checked').val()==1) {
        $('#diveventos').show();
      }
      else{
        $('#diveventos').hide();
      }
    });


    $('#pracdeporte1').change(function () {
        $('#divdeportes').show();        

     });
      $('#pracdeporte2').change(function () {
        $('#divdeportes').hide();
     });

      $('#posehaar1').change(function () {
        $('#divhabilidades').show();        

     });
      $('#posehaar2').change(function () {
        $('#divhabilidades').hide();
     });
      $('#actIUTOMS1').change(function () {
        $('#diveventos').show();        

     });
      $('#actIUTOMS2').change(function () {
        $('#diveventos').hide();
     });
</script>