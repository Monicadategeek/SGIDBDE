
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Consulta Médica</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/ConsultasController.php" method="POST">
  	<input type="hidden" name="accion" id="accion" value="store">
  	<input type="hidden" name="estudiante" id="estudiante" value="<?php echo $estudiante ?>">
  	<input type="hidden" name="cedula" id="cedula" value="<?php echo $cedula?>">

  	<input type="hidden" name="servicio" id="servicio" value="<?php echo $depto?>">
  	

  	<div class="col-xs-12 col-md-8 col-xl-8 col-md-offset-2  col-xl-offset-2">
  		<table class="table table-striped">
  			<tr>
  				<th>Sede</th>
  				<td><?php echo $deptomed[0]["Sede"] ?></td>
  				<th>Doctor</th> 
  				<td align="center">
  					<?php 
						if ($deptomed[0]["TipoPersonal"]==2) {
							echo $deptomed[0]["datopersona"];
						}
						else{
							?>
							<select name="doctor" id="doctor" placeholder="Seleccione" required="" title="Seleccione" class="form-control selectpicker">
								<?php
									if (is_array($doctores) || is_object($doctores))
									{
										foreach ($doctores as $doctor)
										{
											echo "<option value='".$doctor["Persona"]."'>".$doctor["datopersona"]."</option>";
										}
									}
								?>
							</select>
							<?php
						}
					?>  					
				</td>
  			</tr>
  		</table> 
  	</div>
  	<?php 

	if ($antecedentes==0) {
		?>
			<div class="col-xs-12 col-md-12 col-xl-12">
	  		<input type="hidden" name="antecedentes" id="antecedentes" value="1">
		  	<h3>Historia Clinica</h3>

		  	<div class="form-group col-xs-12 col-md-12">
		      <label class="control-label" for="antecfamiliares">Antecedentes Familiares</label>
		      <textarea class="form-control textarea_estile" name="antecfamiliares" id="antecfamiliares" maxlength="700"></textarea> 
		    </div>

		    <div class="form-group col-xs-12 col-md-12">
		      <label class="control-label" for="antpersonales">Antecedentes Personales</label>
		      <textarea class="form-control textarea_estile" name="antpersonales" id="antpersonales" maxlength="700"></textarea> 
		    </div>
		    <div class="form-group col-xs-12 col-md-12">
		      <label class="control-label" for="nota">Nota</label>
		      <textarea class="form-control textarea_estile" name="nota" id="nota" maxlength="700"></textarea> 
		    </div>
	  	</div>
		<?php 
	}
  	?>
  	<div class="col-xs-12 col-md-12 col-xl-12">
  		
  		<div class="col-xs-12 col-md-12 col-xl-12" style="padding:0;">
		    <div class="form-group col-xs-12 col-md-8">
		  		<label class="control-label" for="Enfermedad">Enfermedad o Síntoma</label> <small>Puede seleccionar mas de uno a la vez</small>
				<select name="Enfermedad" id="Enfermedad" show-menu-arrowplaceholder="Seleccione" required="" title="Seleccione" class="form-control selectpicker show-menu-arrow" multiple data-live-search="true">
					<?php
					if (is_array($enfermedades) || is_object($enfermedades))
					{
						foreach ($enfermedades as $enfermedad)
						{
							echo "<option value='".$enfermedad["idEnfermedad"]."'>".$enfermedad["descripcion"]."</option>";
						}
					}
					?>
					<option value="0">Otro</option>
				</select>			
			</div>
			<div class="col-xs-12 col-md-4" id="otraenfermedad">
				<label class="control-label" for="nuevaenfermedad">Nueva Enfermedad</label> <small>Si agregará mas de una es importante que su separación sea de una coma (,) </small>
				<input type="text" class="form-control" name="nuevaenfermedad" id="nuevaenfermedad">
					
			</div>
			<div id="informacionenf" class="col-md-12"><p>Solo podrá agregar enfermedades si selecciona unicamente la opción de otros. Si selecciona otra opción, otros quedara nula</p></div>
		</div>
		<div class="col-xs-12 col-md-12 col-xl-12" style="padding:0;">
			<div class="form-group col-xs-12 col-md-8">
		  		<label class="control-label" for="Tratamiento">Tratamiento</label> <small>Puede seleccionar más de uno a la vez o ninguna</small>
				<select name="Tratamiento[]" id="Tratamiento" show-menu-arrowplaceholder="Seleccione" title="Seleccione" class="form-control selectpicker show-menu-arrow" multiple data-live-search="true">
					<?php
					if (is_array($tratamientos) || is_object($tratamientos))
					{
						foreach ($tratamientos as $tratamiento)
						{
							echo "<option value='".$tratamiento["idTratamiento"]."'>".$tratamiento["descripcion"]."</option>";
						}
					}
					?>
					<option value="0">Otro</option>

				</select>

			</div>
			<div class="col-xs-12 col-md-4" id="otrotratamiento">
				<label class="control-label" for="nuevatratamiento">Nuevo Tratamiento</label> <small>Si agregará mas de una es importante que su separación sea de una coma (,) </small>
				<input type="text" class="form-control" name="nuevatratamiento" id="nuevatratamiento">

			</div>
			<div class="col-md-12" id="informaciontra"><p>Solo podrá agregar tratamientos si selecciona unicamente la opción de otros. Si selecciona otra opción, otros quedara nula</p></div>
		</div>

		<div class="form-group col-xs-12 col-md-12">
	      <label class="control-label" for="Descripcion">Descripción</label>
	      <textarea class="form-control textarea_estile" name="Descripcion" id="Descripcion" maxlength="250"></textarea> 
	    </div>
	    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
	      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
	      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
	    </div>

  	</div>

  	
  </form>
</div>


<script type="text/javascript">

	$('#otraenfermedad').hide(); 

	$('#otrotratamiento').hide(); 
	$('#informacionenf').hide(); 

	$('#informaciontra').hide(); 
	


 	$(document).ready(function(){
		$('.selectpicker').selectpicker('refresh');
		$("#Enfermedad").change(function () { 
			if ($('#Enfermedad option:selected').val()==0) {

				disabled
				$('#otraenfermedad').show();
				$('#informacionenf').show();  			
			}
			else{
				$('#otraenfermedad').hide();  	
			}

		});

      	$("#Tratamiento").change(function () { 
      		if ($('#Tratamiento option:selected').val()==0) {
      			$('#otrotratamiento').show(); 
      			$('#informaciontra').show();
      			
      		}
      		else{
      			$('#otrotratamiento').hide();

      		}
    	});

    });

  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              	var  datos={"accion":'buscar', "cedula":$('#cedula').val(), "departamento":$('#departamento').val() };
            	enviar('../Controller/ConsultasController.php', datos, 'informacion');

              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha Completado la atención Médica');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>