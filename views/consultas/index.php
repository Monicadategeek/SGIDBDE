
     <!-- Page Heading/Breadcrumbs -->

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Consultas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title" align="center"> 
            <h4>Buscar Estudiante</h4>
            <br>
            <div class="col-md-6 col-sm-12 col-xs-6">
            
                <label>Cédula</label>
                <input type="text" id="cedest" name="cedest" class="form-control" style="width: 85%;display:inline-block;">
            </div>
                
            <?php 
            if (count($departamentos)==1) {
                echo "<input type='hidden' id='departamento' value='".$departamentos[0]["nrotipodepto"]."'>";
            }
            else{
            ?>
               
                <div class="col-md-6 col-sm-12 col-xs-6">
                    <label>Servicio</label>
                    <div style="width: 85%;display:inline-block;">
                        <select name="departamento" id="departamento" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
                            <?php
                            if (is_array($departamentos) || is_object($departamentos))
                            {
                                foreach ($departamentos as $departamento)
                                {
                                    if ($departamento["nrotipodepto"]!=4) {
                                        echo "<option value='".$departamento["nrotipodepto"]."'>".$departamento["TipoDepto"]."</option>";
                                    }

                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            <?php
            }
            ?>              
                 
            <br><br><br><br>
            <div align="center">
                <button type="button" class="btn btn-primary btn-sm" id="boton" onclick="consultar();"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
            </div>    
        </div>      
   
        <div class="x_content" id="informacion">
             

            <?php
                if (isset($pacientes)) {
                   require_once("pacientes.php");
                }
            ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('.selectpicker').selectpicker('refresh');

    function consultar(){

        if ($('#cedest').val().length == 0) {
            notificacion(2,'fa fa-times-circle','Error!','Debe introducir la cédula del estudiante');
            return false;
        }
         if (!$('#departamento').val()) {
            notificacion(2,'fa fa-times-circle','Error!','Debe seleccionar el servicio que desea consultar');
            return false;
        }

        var  datos={"accion":'buscar', "cedula":$('#cedest').val(), "departamento":$('#departamento').val() };
            enviar('../Controller/ConsultasController.php', datos, 'informacion');
            $('#informacion').show();              

    }

</script>

   
