
<div class="col-md-12 col-sm-12 col-xs-12">
    <h2>Medicina General</h2>

    <div class="modal-body row">
        <input type="hidden" name="estudiante" id="estudiantenum" value="<?php echo $estudiante["num_est"] ?>">
        <input type="hidden" name="cedula" id="cedulaest" value="<?php echo $estudiante["cedula"] ?>">
        <input type="hidden" name="depto" id="deptotrab" value="<?php echo $deptos ?>">
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos Estudiantiles</a>
                  </li>
                  <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Historia Clinica</a>
                  </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                        <label>Datos del Estudiante</label>
                        <div class=" col-md-3 col-xl-12">
                            <?php 
                            if ($estudiante["foto"]!=0) {
                            echo '<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$estudiante["foto"].'" alt="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'" title="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'">';
                            }
                            else{
                            echo '<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
                            } ?>
                            <ul class="list-unstyled user_data">
                                <li  title="Carrera"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $estudiante["cod_carrera"] ?>
                                </li>
                                <li>
                                  <i class="fa fa-book"></i> <b title="Trayecto">Trayecto</b> <?php echo $estudiante["trayecto"] ?> &nbsp; &nbsp; <i class="fa fa-book"></i> <b title="Trimestre">Trimestre</b>  <?php echo $estudiante["trimestre"] ?>
                                </li>
                                <li>
                                  <i class="fa fa-book"></i> <b title="Sección">Sección</b> <?php echo $estudiante["seccion"] ?> 
                                  <i class="fa fa-clock-o"></i> <b title="Turno">Turno</b> <?php echo $estudiante["turno"] ?>
                                </li>
                                <li title="Es el estatus frente a la universidad">
                                  <i class="fa fa-user"></i> <b >Estado</b> <?php echo $estudiante["cod_estatus"] ?>
                                </li>
                              </ul>
                        </div>

                        <div class=" col-md-9 col-xl-12">
                            <table class="data table table-striped">
                              <thead>
                                <tr>
                                  <th>Cédula</th>
                                  <th>Nombres</th>
                                  <th>Apellidos</th>
                                  <th>Sexo</th>
                                  <th>Fecha Nacimiento</th>
                                  <th>Edad</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><?php echo $estudiante["nacionalidad"]." ".$estudiante["cedula"] ?></td>
                                    <td><?php echo $estudiante["nombres"] ?></td>
                                    <td><?php echo $estudiante["apellidos"] ?></td>
                                    <td><?php echo $estudiante["sexo"] ?></td>
                                    <td><?php echo $estudiante["fec_nac"] ?></td>                        
                                    <td><?php echo $estudiante["edad"] ?></td>
                                </tr>
                              </tbody>
                            </table>
                            <table class="data table table-striped">
                              <thead>
                                <tr>
                                  <th>Dirección</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><?php echo $estudiante["direccion"] ?></td> 
                                </tr>
                              </tbody>
                            </table>

                            <table class="data table table-striped">
                              <thead>
                                <tr>
                                  <th>Dirección de Correo Electrónico</th>
                                  <th>Telefono</th>
                                  <th>Celular</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><?php echo $estudiante["correo"] ?></td>
                                    <td><?php echo $estudiante["celular"] ?></td>
                                    <td><?php echo $estudiante["tel_hab"] ?></td> 
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <div class="col-xs-12 col-md-12 col-xl-12" >

                            
                             <?php 
                                if (is_array($antecedente) && count($antecedente)>0)
                                {
                                        echo '<div align="left" class="col-xs-6 col-md-6 col-xl-6">
                                            <button type="button" class="btn btn-info btn-sm" title="Ver Informe"  onclick="ver('."'../Controller/ConsultasController.php',".$antecedente["idAntecedente"].');" ><i class="fa fa-book"></i> Ver Informe</button>
                                        </div> <div align="right" class="col-xs-6 col-md-6 col-xl-6">
                                            <button type="button" class="btn btn-warning btn-sm" title="Modificar"  onclick="modificar('."'../Controller/ConsultasController.php',".$antecedente["idAntecedente"].');" ><i class="fa fa-pencil"></i> Modificar</button>
                                        </div>';
                                    
                                        echo '<table class="table table-bordered">';
                                        echo "<tr>
                                        <th width='20%'>Antecedentes Familiares</th>
                                        <td width='80%'>".$antecedente["antecfamiliares"]."</td>
                                        </tr>
                                        <tr>
                                        <th>Antecedentes Personales</th>
                                        <td>".$antecedente["antpersonales"]."</td>
                                        </tr>
                                        <tr>
                                        <th>Nota</th>
                                        <td>".$antecedente["nota"]."</td></tr>"
                                        ;
                                        echo "</table>";
                                        echo '<table class="table table-bordered">
                                                <tr>
                                                    <th>Responsable</th>
                                                    <td>Dr. '.$antecedente["responsable"].'</td>
                                                    <th>Fecha Ultima Modificación</th>
                                                    <td>'.$antecedente["ultmodificacion"]  .'</td>
                                                </tr>
                                            </table>';
                                    
                                }
                                else{
                                     echo '<table class="table table-bordered">';
                                        echo "<tr>
                                        <th>Antecedentes Familiares</th>
                                        <td></td>
                                        </tr>
                                        <tr>
                                        <th>Antecedentes Personales</th>
                                        <td></td>
                                        </tr>
                                        <tr>
                                        <th>Nota</th>
                                        <td></td></tr>"
                                        ;
                                        echo "</table>";
                                        echo '<table class="table table-bordered">
                                                <tr>
                                                    <th>Responsable</th>
                                                    <td></td>
                                                    <th>Fecha Ultima Modificación</th>
                                                    <td></td>
                                                </tr>
                                            </table>';
                                }
                            ?>

                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <hr  style="margin: 0 auto; width:100%;">
        <div class="col-xs-12 col-md-12 col-xl-12">

            <h3>Atenciones al Estudiante</h3>
            <?php
                if (count($deptomed)>0) {
                    ?>

                     <div align="right">
                        <button type="button" class="btn btn-primary btn-sm"  onclick="atencionmedica('../Controller/ConsultasController.php')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva Consulta</button>
                    </div>
                    <?php

                }
                else{
                    echo '<div align="right"><p>Usted se encuentra fuera del horario de atención de pacientes</p>  </div>';
                }
                
            ?>
           
            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="consultas"> 
                <thead> 
                    <th>Sede</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Enfermedad</th>
                    <th>Tratamiento</th> 
                    <th title="Responsable del registro del informe">Responsable</th>
                    <th>Doctor</th>
                    <th>Acciones</th>
                </thead>
                <tfoot>
                    <th>Sede</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Enfermedad</th>
                    <th>Tratamiento</th>
                    <th>Responsable</th>
                    <th>Doctor</th>  
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($consultas) || is_object($consultas))
                        {
                            
                            foreach ($consultas as $consulta)
                            {
                                echo "<tr>
                                <td>".$consulta["Sede"]."</td>
                                <td>".$consulta["Fecha"]."</td>
                                <td>".$consulta["Hora"]."</td>
                                <td>".$consulta["Enfermedad"]."</td>
                                <td>".$consulta["Tratamiento"]."</td>
                                <td>".$consulta["Responsable"]."</td>
                                <td>".$consulta["Doctor"]."</td>"
                                ;
                                echo '<td>';    
                                echo '<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$consulta["IdCitas"].');"><i class="fa fa-book"></i></button>  ';

                                echo "</td></tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>
         
        <!-- /.row -->

    <script type="text/javascript">
    function atencionmedica(url){

    var  datos={"accion":"create", "estudiante":$('#estudiantenum').val(), 'cedula':$('#cedulaest').val(), "depto":$('#deptotrab').val()};
    $.post({
        url: url, 
        data:datos,
        success:function(data){
        $("#contenidomodalagregar").html(data);
           $('#agregar').modal('toggle');
        }
    })
}

    $(document).ready(function(){

    $('#consultas').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de consultas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        
        responsive: true,
    });
// Setup - add a text input to each footer cell
    $('#consultas tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#consultas').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([0,5,6]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>