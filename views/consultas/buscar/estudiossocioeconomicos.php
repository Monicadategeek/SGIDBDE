
<div class="col-md-12 col-sm-12 col-xs-12">
    <h2>Estudios Socio Económicos</h2>

    <div class="modal-body row">
        <input type="hidden" name="estudiante" id="estudiantenum" value="<?php echo $estudiante["num_est"] ?>">
        <input type="hidden" name="cedula" id="cedulaest" value="<?php echo $estudiante["cedula"] ?>">
        <div class="col-xs-12 col-md-12 col-xl-12" style="margin-bottom: 10px;">
            
            <div class=" col-md-3 col-xl-12">
                <?php 
                if ($estudiante["foto"]!=0) {
                echo '<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$estudiante["foto"].'" alt="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'" title="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'">';
                }
                else{
                echo '<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
                } ?>
                <ul class="list-unstyled user_data">
                    <li  title="Carrera"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $estudiante["cod_carrera"] ?>
                    </li>
                    <li>
                      <i class="fa fa-book"></i> <b title="Trayecto">Trayecto</b> <?php echo $estudiante["trayecto"] ?> &nbsp; &nbsp; <i class="fa fa-book"></i> <b title="Trimestre">Trimestre</b>  <?php echo $estudiante["trimestre"] ?>
                    </li>
                    <li>
                      <i class="fa fa-book"></i> <b title="Sección">Sección</b> <?php echo $estudiante["seccion"] ?> 
                      <i class="fa fa-clock-o"></i> <b title="Turno">Turno</b> <?php echo $estudiante["turno"] ?>
                    </li>
                    <li title="Es el estatus frente a la universidad">
                      <i class="fa fa-user"></i> <b >Estado</b> <?php echo $estudiante["cod_estatus"] ?>
                    </li>
                  </ul>
            </div>

            <div class=" col-md-9 col-xl-12">
            	<h3>Datos del Estudiante</h3>
                <table class="data table table-striped">
                  <thead>
                    <tr>
                      <th>Cédula</th>
                      <th>Nombres</th>
                      <th>Apellidos</th>
                      <th>Sexo</th>
                      <th>Fecha Nacimiento</th>
                      <th>Edad</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td><?php echo $estudiante["nacionalidad"]." ".$estudiante["cedula"] ?></td>
                        <td><?php echo $estudiante["nombres"] ?></td>
                        <td><?php echo $estudiante["apellidos"] ?></td>
                        <td><?php echo $estudiante["sexo"] ?></td>
                        <td><?php echo $estudiante["fec_nac"] ?></td>                        
                        <td><?php echo $estudiante["edad"] ?></td>
                    </tr>
                  </tbody>
                </table>
                <table class="data table table-striped">
                  <thead>
                    <tr>
                      <th>Dirección</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td><?php echo $estudiante["direccion"] ?></td> 
                    </tr>
                  </tbody>
                </table>

                <table class="data table table-striped">
                  <thead>
                    <tr>
                      <th>Dirección de Correo Electrónico</th>
                      <th>Telefono</th>
                      <th>Celular</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td><?php echo $estudiante["correo"] ?></td>
                        <td><?php echo $estudiante["celular"] ?></td>
                        <td><?php echo $estudiante["tel_hab"] ?></td> 
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
        <hr  style="margin: 5px;width:100%;">
        <div class="col-xs-12 col-md-12 col-xl-12">

            <h3>Entrevistas Realizadas</h3>           
           
            <table class="table table-striped" id="entrevistas">
				<thead>
					<th width="20%">Fecha</th>
					<th width="20%">Hora</th>
					<th width="25%">Trabajador Social</th>
					<th width="20%"></th>
				</thead>
        <tfoot>
          <th>Fecha</th>
          <th>Hora</th>
          <th>Trabajador Social</th>
          <td></td>
        </tfoot>
				<tbody>
					<?php 
            if (is_array($seguimientos) || is_object($seguimientos))
            {
                
                foreach ($seguimientos as $seguimiento)
                {
                	echo "<tr>
                        <td>".$seguimiento["Fecha"]."</td>
                        <td>".$seguimiento["Hora"]."</td>
                        <td>".$seguimiento["Responsable"]."</td>
                        <td>".$seguimiento["boton"]."</td>
                     </tr>";
                }
            }
          ?>									
					
				</tbody>
			</table>
        </div>
    </div>
</div>
         
        <!-- /.row -->

    <script type="text/javascript">

    $(document).ready(function(){

    $('#entrevistas').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de entrevistas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        
        responsive: true,
    });
// Setup - add a text input to each footer cell
    $('#entrevistas tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#entrevistas').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([2]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>