<?php 
    if (!isset($fecha)) {
       $fecha=date('Y-m-d');
    }
    $titulofecha=date('d-m-Y', strtotime($fecha));
    if (date('Y-m-d', strtotime($fecha))==date('Y-m-d')) {
        $titulofecha="Hoy ".$titulofecha;
    }

?>
<div class="row">
    <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="col-xs-8 col-md-8">
            <h3>Pacientes Atendidos en Medicina General <small><?php echo $titulofecha?></small></h3>
        </div>      

        <div class="col-xs-4 col-md-4">
            <div class="pull-right" style="    width: 50%; ">
                <div><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <input type="text" id="fecha" name="fecha" class="form-control" style="width: 85%;display:inline-block;" value="<?php echo date('d-m-Y', strtotime($fecha))?>">
                </div>
            </div>
        </div>
    </div>



    <div class="col-xs-12 col-md-12 col-xl-12">
        <table class="table table-striped table-bordered col-xs-12 col-md-12" width="100%" id="pacientes" > 
            <thead> 
                <th width="8%">Sede</th>
                <th width="8%">Cédula</th>
                <th width="14%">Nombres</th>
                <th width="14%">Apellidos</th>
                <th width="14%">Hora</th>
                <th width="14%">Doctor</th>
                <th width="14%"></th>
            </thead>
            <tfoot>
                <th>Sede</th>
                <th>Cédula</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Hora</th>
                <th>Doctor</th>
                <td></td>
            </tfoot>
            <tbody >
                <?php 

                if (is_array($pacientes) || is_object($pacientes))
                {
                    foreach ($pacientes as $paciente)
                    {
                        echo "<tr>
                        <td>".$paciente["Sede"]."</td>
                        <td>".$paciente["cedula"]."</td>
                        <td>".$paciente["nombres"]."</td>
                        <td>".$paciente["apellidos"]."</td>
                        <td>".$paciente["Hora"]."</td>
                        <td>".$paciente["Doctor"]."</td>
                        <td>".$paciente["botones"]."</td>
                        </tr>";
                    }
                }
                ?>
            </tbody>
        </table>        
    </div>

</div>


        <!-- /.row -->
 <script type="text/javascript">
    $(function () {
        $("#fecha").datepicker({
            language: 'es',
            format:'dd-mm-yyyy',
            todayHighlight:true,
            autoclose:true,
           
        });
    });

    $(document).ready(function(){
        $('#pacientes').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de pacientes para el día seleccionado",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        dom: "Bfrtip",
        buttons: [
        {
            extend: 'pdfHtml5',
            text: '<i class="fa fa-print" ></i> Imprimir',
            filename:"Pacientes - SGIDBDE",
            title:"Listado de Pacientes",
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5],
                        search: 'applied',
                        order: 'applied'
            },
            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    customize: function (doc) {
                        doc.content[1].table.widths = 
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.content.splice(0,0);
                        //Remove the title created by datatTables
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
                        var now = new Date();
                        var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                        // Logo converted to base64
                        // var logo = getBase64FromImageUrl('https://datatables.net/media/images/logo.png');
                        // The above call should work, but not when called from codepen.io
                        // So we use a online converter and paste the string in.
                        // Done on http://codebeautify.org/image-to-base64-converter
                        // It's a LONG string scroll down to see the rest of the code !!!
                        // A documentation reference can be found at
                        // https://github.com/bpampuch/pdfmake#getting-started
                        // Set page margins [left,top,right,bottom] or [horizontal,vertical]
                        // or one number for equal spread
                        // It's important to create enough space at the top for a header !!!
                        doc.pageMargins = [20,100,20,30];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 9;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 9;
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        image: logo,
                                        width: 150
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 12,
                                        text: 'Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil'
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages
                        doc['footer']=(function(page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        text: ['Generado el: ', { text: jsDate.toString() }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['página ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['hLineWidth'] = function(i) { return .5; };
                        objLayout['vLineWidth'] = function(i) { return .5; };
                        objLayout['hLineColor'] = function(i) { return '#aaa'; };
                        objLayout['vLineColor'] = function(i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function(i) { return 4; };
                        objLayout['paddingRight'] = function(i) { return 4; };
                        doc.content[0].layout = objLayout;
                        
                }

            }
        ],
        responsive: true
    });
// Setup - add a text input to each footer cell
    $('#pacientes tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" />' );
    } );
 
    // DataTable
    var table = $('#pacientes').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $('input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );



   $( "#fecha" ).datepicker("refresh");
    $("#fecha").change(function () {
        
        var  datos={"accion":'PacientesTratados', 'fecha':$('#fecha').val()};
        enviar('../Controller/ConsultasController.php', datos, 'informacion');
                     
    });
});



</script>