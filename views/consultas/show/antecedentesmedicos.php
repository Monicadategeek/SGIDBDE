<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <a class="btn btn-default btn-sm buttons-pdf" href="#" onclick="tomarcaptura('contenidomodalver .modal-body');" style="float: left;"><i class="fa fa-print"></i> Imprimir</a>
  <h4 class="modal-title" style="text-align: center;">Ver Informe Médico</h4>

</div>
<div class="modal-body row">
  <div class="col-xs-12 col-md-12 col-xl-12 row">
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="<?php include('../views/logo.php');?>" style="width: 50%;"></div>
        <div class="col-xs-8 col-md-8 col-xl-8" style="text-align: center;">REPÚBLICA BOLIVARIANA DE VENEZUELA<br>
        MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN SUPERIOR<br>
        INSTITUTO UNIVERSITARIO DE TECNOLIGIA DEL OESTE<br> 
        "MARISCAL SUCRE"<br>
        Caracas.
        </div>
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="../web/images/mppes.png" style="width: 50%;" ></div>
    <div class="col-xs-12 col-md-12 col-xl-12">
      <br>
       <h5 align="center">UNIDAD MÉDICA IUTOMS</h5>
       <h5 align="center"><u>HISTORIA CLINICA</u></h5>
      <br><br>
      <div class="row">
        <div class="col-xs-12 col-md-12">
            <table class="col-xs-12 col-md-12 tablatdborder">
              <tr>
                <th width="20%">Nombre y Apellido:</th>
                <td width="80%"><?php echo $estudiante["nombres"]." ".$estudiante["apellidos"]?></td>              
              </tr>
            </table>
        </div>          
      </div>  

      <div class="row">
        <div class="col-xs-12 col-md-12">
            <table class="col-xs-12 col-md-12 tablatdborder">
              <tr>
                <th width="7%">Edad:</th>
                <td width="10%"><?php echo $estudiante["edad"]?></td>
                <th width="83%"></th>
              </tr>
            </table>
        </div>          
      </div>

      <div class="row">
       <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
          <table class="col-xs-12 col-md-12 tablatdborder">                           
            <tr>
              <th width="10%">Dirección:</th>
              <td width="90%" colspan="2"><div class="border-bottom"><?php echo $estudiante["direccion"]?></td>
            </tr>
          </table>
        </div>                   
      </div>    

      <div class="row">
        <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
          <table class="col-xs-12 col-md-12 tablatdborder">                           
              <tr>
                <th width="15%">Teléfono Hab:</th>
                <td width="15%"><?php echo $estudiante["tel_hab"] ?></td>  
                <th width="8%">Celular:</th>
                <td width="20%"><?php echo $estudiante["celular"] ?></td>
                <th width="42%"></th>
              </tr>
          </table>
        </div>
      </div>  
      
      <div class="col-xs-12 col-md-12" style="min-height: 55vh">
      <div class="row">
        <div class="col-xs-12 col-md-12" style="margin-top: 10px;">
            <label>Ant. Familiares</label>
            <p align="justify">&nbsp;&nbsp;&nbsp;<u><?php echo $antecedente["antecfamiliares"]?></u> </p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-12" style="margin-top: 10px;">
            <label>Ant. Personales</label>
            <p align="justify">&nbsp;&nbsp;&nbsp;<u><?php echo $antecedente["antpersonales"]?></u> </p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-12" style="margin-top: 10px;">
            <label>Nota</label>
            <p align="justify">&nbsp;&nbsp;&nbsp;<u><?php echo $antecedente["nota"]?></u> </p>
        </div>
      </div>
      </div>
       <div class="row">
        <div class="col-xs-4 col-xs-offset-4 col-md-4 col-md-offset-4" style="margin-top: 50px;">
          <table class="col-md-12 tablatdborder">                           
            <tr>
              <td width="50%"></td> 
            </tr>
            <tr>
              <th style=" text-align: center !important;">Firma del Médico</th> 
            </tr>
          </table>
        </div>
      </div>

      <div class="row">
          <div class="col-xs-4 col-md-4">
           <label>Fecha: </label><div class="border-bottom"><?php echo $antecedente["ultmodificacion"]?></div>
          </div>
      </div> 

    </div>
  </div>   
</div>

<div class="modal-footer">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-xl-12" align="right">
            <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
        </div>  
    </div>
</div>






