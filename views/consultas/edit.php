<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Moficar Historia Clinica</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="editar" action="../Controller/ConsultasController.php" method="POST">
  <input type="hidden" name="cedula" id="cedula" value="<?php echo $antecedente["cedula"] ?>">

  	<input type="hidden" name="servicio" id="servicio" value="<?php echo $depto?>">

  	<input type="hidden" name="accion" id="accion" value="update">
	<div class="col-xs-12 col-md-12 col-xl-12">
  		<input type="hidden" name="idAntecedente" id="idAntecedente" value="<?php echo $antecedente["idAntecedente"] ?>">
	  	<h3></h3>

	  	<div class="form-group col-xs-12 col-md-12">
	      <label class="control-label" for="antecfamiliares">Antecedentes Familiares</label>
	      <textarea class="form-control textarea_estile" name="antecfamiliares" id="antecfamiliares" maxlength="700"><?php echo $antecedente["antecfamiliares"] ?></textarea> 
	    </div>

	    <div class="form-group col-xs-12 col-md-12">
	      <label class="control-label" for="antpersonales">Antecedentes Personales</label>
	      <textarea class="form-control textarea_estile" name="antpersonales" id="antpersonales"  maxlength="700"><?php echo $antecedente["antpersonales"] ?></textarea> 
	    </div>
	    <div class="form-group col-xs-12 col-md-12">
	      <label class="control-label" for="nota">Nota</label>
	      <textarea class="form-control textarea_estile" name="nota" id="nota"  maxlength="700"><?php echo $antecedente["nota"] ?></textarea> 
	    </div>
  	</div>

  <div class="col-xs-12 col-md-12 col-xl-12" align="center">
	      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
	      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
	    </div>

  	</div>

  	
  </form>
</div>

<script type="text/javascript">
	$('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              	var  datos={"accion":'buscar', "cedula":$('#cedula').val(), "departamento":$('#servicio').val() };
            	enviar('../Controller/ConsultasController.php', datos, 'informacion');

              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha Modificado la Historia Clinica');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>