<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificar Miembro Familiar</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="editar" action="../Controller/MiembrosFamiliaresController.php" method="POST">
  <?php 
    if (isset($estudiante)) {
      echo '<input type="hidden" name="estudiante" id="estudiante" value="'.$estudiante.'">';
    }
  ?>
  <input type="hidden" name="accion" id="accion" value="update">
  <input type="hidden" name="id_grupo_fam_est" id="id_grupo_fam_est" value="<?php echo $grupofamiliarest["id_grupo_fam_est"];?>">

  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_parentesco">Parentesco</label>
      <select id="id_parentesco" name="id_parentesco" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($parentescos) || is_object($parentescos))
      {
          foreach ($parentescos as $parentesco)
          {
            if ($grupofamiliarest["id_parentesco"]==$parentesco["id_parentesco"]) {
              echo "<option value='".$parentesco["id_parentesco"]."' selected>".$parentesco["desc_parentesco"]."</option>";
            }
            else
            {
              echo "<option value='".$parentesco["id_parentesco"]."'>".$parentesco["desc_parentesco"]."</option>";
            }
          }
      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="apellidos">Apellidos</label>
      <input type="text" class="form-control" name="apellidos" id="apellidos" maxlength="30" required="" value="<?php echo $grupofamiliarest["apellidos"];?>">
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="nombres">Nombres</label>
      <input type="text" class="form-control" name="nombres" id="nombres" maxlength="30" required="" value="<?php echo $grupofamiliarest["nombres"];?>">
    </div>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="fec_nac">Fecha de Nacimiento</label>
      <input type="text" class="form-control" data-date-end-date="0d" name="fec_nac" id="fec_nac" required="" value="<?php echo $grupofamiliarest["fec_nac"];?>">
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_edo_civ">Estado Civil</label>
      <select id="id_edo_civ" name="id_edo_civ" class="form-control selectpicker"  required="" title="-Seleccione-">
      <?php 
      if (is_array($estadosciviles) || is_object($estadosciviles))
      {
        foreach ($estadosciviles as $estadocivil)
        {
          if ($grupofamiliarest["id_edo_civ"]==$estadocivil["id_edo_civ"]){
            echo "<option value='".$estadocivil["id_edo_civ"]."' selected>".$estadocivil["desc_edo_civ"]."</option>";
          }
          else{
            echo "<option value='".$estadocivil["id_edo_civ"]."'>".$estadocivil["desc_edo_civ"]."</option>";
          }              
        }
      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label for="genero">Género</label>
      <div class="radio">
        <?php 
        if ($grupofamiliarest["id_edo_civ"]==1) {
          echo '<label>
                  <input type="radio" name="genero"  value="1" checked>Femenino
                </label>
                <label>
                  <input type="radio" name="genero" value="2">Masculino
                </label>';
        }
        else{
          echo '<label>
                  <input type="radio" name="genero"  value="1" >Femenino
                </label>
                <label>
                  <input type="radio" name="genero" value="2" checked>Masculino
                </label>';
        }
        ?>
           
      </div>
      <div class="help-block with-errors"></div>
    </div>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_grado_instr">Grado de Instrucción </label>
      <select id="id_grado_instr" name="id_grado_instr" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($gradinst) || is_object($gradinst))
      {
          foreach ($gradinst as $gradinst)
          {
            if ($grupofamiliarest["id_grado_instr"]==$gradinst["id_grado_instr"]){
              echo "<option value='".$gradinst["id_grado_instr"]."' selected>".$gradinst["desc_grado_instr"]."</option>";
            }
            else{
              echo "<option value='".$gradinst["id_grado_instr"]."'>".$gradinst["desc_grado_instr"]."</option>";
            }
            
          }

      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_prof_ocup">Profesión u Ocupación</label>
      <select id="id_prof_ocup" name="id_prof_ocup" class="form-control selectpicker" data-live-search="true" required="" title="-Seleccione-">
      <?php 
      if (is_array($profocup) || is_object($profocup))
      {
          foreach ($profocup as $profocup)
          {
            if ($grupofamiliarest["id_prof_ocup"]==$profocup["id_prof_ocup"]){
              echo "<option value='".$profocup["id_prof_ocup"]."' selected>".$profocup["desc_prof_ocup"]."</option>";
            }
            else{
              echo "<option value='".$profocup["id_prof_ocup"]."'>".$profocup["desc_prof_ocup"]."</option>";
           
            }
          }

      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="ingreso_mensual">Ingreso Mensual</label>
      <input type="text" class="form-control bs" name="ingreso_mensual" id="ingreso_mensual" required=""  value="<?php echo $grupofamiliarest["ingreso_mensual"];?>">
    </div>
  </div>



   

    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $(document).ready(function(){
      $('.bs').mask('0000000.00', {reverse: true});
      $('.selectpicker').selectpicker('refresh');

      $("#fec_nac").datepicker({
      language: 'es',
      format:'dd-mm-yyyy'
       });

    });

  $('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
               if ($('#estudiante')) {
                var  datos={"accion":'index', "estudiante":$('#estudiante').val()};
                enviar('../Controller/MiembrosFamiliaresController.php', datos, 'miembros');

                notificacion(3,'fa fa-check','Completado!','Se ha modificado el Miembro de la Familia');
              }
              else{
                redireccionar('../Controller/MiembrosFamiliaresController.php?accion=index');
                notificacion(3,'fa fa-check','Completado!','Se ha modificado el Miembro de mi Familia');
              }

              $('#modificar').modal('hide');
              
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
            alert("Gafa");
        }
    })
  });
</script>