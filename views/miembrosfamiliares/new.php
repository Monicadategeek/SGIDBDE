<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Nuevo Miembro Familiar</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/MiembrosFamiliaresController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="store">
  <?php 
    if (isset($estudiante)) {
      echo '<input type="hidden" name="estudiante" id="estudiante" value="'.$estudiante.'">';
    }
  ?>
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_parentesco">Parentesco</label>
      <select id="id_parentesco" name="id_parentesco" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($parentescos) || is_object($parentescos))
      {
          foreach ($parentescos as $parentesco)
          {
              echo "<option value='".$parentesco["id_parentesco"]."'>".$parentesco["desc_parentesco"]."</option>";
          }
      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="apellidos">Apellidos</label>
      <input type="text" class="form-control" name="apellidos" id="apellidos" maxlength="30" required="" >
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="nombres">Nombres</label>
      <input type="text" class="form-control" name="nombres" id="nombres" maxlength="30" required=""  >
    </div>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="fec_nac">Fecha de Nacimiento</label>
      <input type="text" class="form-control" data-date-end-date="0d" name="fec_nac" id="fec_nac" required="" >
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_edo_civ">Estado Civil</label>
      <select id="id_edo_civ" name="id_edo_civ" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($estadosciviles) || is_object($estadosciviles))
      {
          foreach ($estadosciviles as $estadocivil)
          {
              echo "<option value='".$estadocivil["id_edo_civ"]."'>".$estadocivil["desc_edo_civ"]."</option>";
          }
      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label for="genero">Género</label>
      <div class="radio">
        <label>
          <input type="radio" name="genero"  value="2" required>Femenino
        </label>
        <label>
          <input type="radio" name="genero" value="1">Masculino
        </label>   
      </div>
      <div class="help-block with-errors"></div>
    </div>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_grado_instr">Grado de Instrucción</label>
      <select id="id_grado_instr" name="id_grado_instr" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($gradinst) || is_object($gradinst))
      {
          foreach ($gradinst as $gradinst)
          {
              echo "<option value='".$gradinst["id_grado_instr"]."'>".$gradinst["desc_grado_instr"]."</option>";
          }

      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="id_prof_ocup">Profesión u Ocupación</label>
      <select id="id_prof_ocup" name="id_prof_ocup" data-live-search="true" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      if (is_array($profocup) || is_object($profocup))
      {
          foreach ($profocup as $profocup)
          {
              echo "<option value='".$profocup["id_prof_ocup"]."'>".$profocup["desc_prof_ocup"]."</option>";
          }

      }
      ?>
      </select>
    </div>
    <div class="form-group col-xs-12 col-md-4">
      <label class="control-label" for="ingreso_mensual">Ingreso Mensual</label>
      <input type="text" class="form-control bs" name="ingreso_mensual" id="ingreso_mensual" required=""  value="0">
    </div>
  </div>



   

    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $(document).ready(function(){
      $('.bs').mask('0000000.00', {reverse: true});
      $('.selectpicker').selectpicker('refresh');

      $("#fec_nac").datepicker({
      language: 'es',
      format:'dd-mm-yyyy'
       });

    });

  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              if ($('#estudiante')) {
                var  datos={"accion":'index', "estudiante":$('#estudiante').val()};
                enviar('../Controller/MiembrosFamiliaresController.php', datos, 'miembros');
                notificacion(3,'fa fa-check','Completado!','Se ha registrado el Miembro de la Familia');

              }
              else{
                redireccionar('../Controller/MiembrosFamiliaresController.php?accion=index');
                notificacion(3,'fa fa-check','Completado!','Se ha registrado el Miembro de la Familia');
              }
              
              $('#agregar').modal('hide');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!', data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>