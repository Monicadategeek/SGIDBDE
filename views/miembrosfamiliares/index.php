<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="95" style="width:40%"></div>
</div>
<h2><i class="fa fa-users" aria-hidden="true"></i> Mis Miembros Familiares</h2>
<small> Grupo Familiar con quien reside actualmente</small>
<hr>
<div align="left" style="padding: 10px">
        <button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/MiembrosFamiliaresController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Miembro</button>
                
</div> 
<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 0 !important;">
             

    <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="familias"> 
        <thead>
            <th>Parentesto</th>
            <th>Apellidos</th>
            <th>Nombres</th>
            <th>Fecha de Nacimiento</th>
            <th>Edo. Civil</th>
            <th>Genero</th>
            <th>Nivel Instrucción</th>
            <th>Profesión u Oficio</th>
            <th>Ingreso Mensual</th>
            <th>Acciones</th>
        </thead>
        
        <tbody>
            <?php 
                if (is_array($familiares) || is_object($familiares))
                {
                    foreach ($familiares as $familia)
                    {
                        echo "<tr>
                        <td>".$familia["id_parentesco"]."</td>
                        <td>".$familia["apellidos"]."</td>
                        <td>".$familia["nombres"]."</td>
                        <td>".$familia["fec_nac"]."</td>
                        <td>".$familia["id_edo_civ"]."</td>
                        <td>".$familia["genero"]."</td>
                        <td>".$familia["id_grado_instr"]."</td>
                        <td>".$familia["id_prof_ocup"]."</td>
                        <td>".$familia["ingreso_mensual"]."</td>";
                        echo '<td><button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/MiembrosFamiliaresController.php',".$familia["id_grupo_fam_est"].');"><i class="fa fa-pencil"></i></button>';
                         echo '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/MiembrosFamiliaresController.php',".$familia["id_grupo_fam_est"].');" style="margin-left: 3px;;"><i class="fa fa-trash-o"></i></button>';                                                             
                        echo "</td></tr>";
                    }
                }
            ?>
        </tbody>
    </table>    

    <div align="center">
        <button type="button" class="btn btn-success btn-large"  onclick="miembros('../Controller/EncuestaController.php?accion=MiembrosFamiliares');" > Ya he terminado de Agregar a mis Miembros Familiares</button>
                
    </div> 

</div>

<!-- Modal Agregar-->
   <div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalagregar">
        </div>

      </div>
    </div>

    <!-- Modal Modificar-->
   <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalmodificar">
        </div>

      </div>
    </div>

    <script type="text/javascript">
        
        function miembros(url){
            $.ajax({
                url: url,             
                type : 'GET',
                contentType: "charset=utf-8", 
                success:function(data){

                   $("#encuesta").html(data);
                }
            })
        }
    </script>


           
        <!-- /.row -->
 