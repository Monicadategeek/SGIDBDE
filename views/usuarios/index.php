
     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Control de Usuarios
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Lista de Usuarios de los Departamentos</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="usuarios" > 
                <thead> 
                    <th width="15%">Cédula</th>
                    <th width="20%">Nombres</th>
                    <th width="20%">Apellidos</th>
                    <th width="20%">Correo</th>                       
                    <th width="10%">Nivel</th>
                    <th width="15%">Estado</th>
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Cédula</th>                    
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Correo</th>
                    <th>Nivel</th>
                    <th>Estado</th>
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($usuarios) || is_object($usuarios))
                        {
                            
                            foreach ($usuarios as $usuario)
                            {

                                echo "<tr>
                                <td>".$usuario["cedula"]."</td>
                                <td>".$usuario["Nombres"]."</td>
                                <td>".$usuario["Apellidos"]."</td>
                                <td>".$usuario["correo"]."</td>
                                <td>".$usuario["TipoUsuario"]."</td>
                                <td>".$usuario["Cod_Estatus"]."</td>
                                <td style='text-align: center !important;'>".$usuario["boton"]."</td>
                                </tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="horariosModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg2" role="document">

    <div class="modal-content" style="padding: 20px;" id="horarioscontent">

      
    </div>
  </div>
</div>
           
        <!-- /.row -->

    <script type="text/javascript">
    $(document).ready(function(){

$('#usuarios').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Citas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#usuarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#usuarios').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([4,5]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>