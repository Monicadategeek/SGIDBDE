<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación del Usuario</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="editar" action="../Controller/UsuariosController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="update">
  <input type="hidden" name="Num_Usuario" id="Num_Usuario" value="<?php echo $usuario["Num_Usuario"]?>">
  <div class="col-xs-12 col-md-12 col-xl-12" style="margin-bottom: 10px;">
            
    <div class=" col-md-3 col-xl-12">
        <?php 
        echo $usuario["Foto_Usuario"];
        ?>
    </div>

    <div class=" col-md-9 col-xl-12">
      <h3>Datos de la usuario</h3>
        <table class="data table table-striped">
          <thead>
            <tr>
              <th>Cédula</th>
              <th>Nombres</th>
              <th>Apellidos</th>
              <th>Sexo</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td><?php echo $usuario["cedula"] ?></td>
                <td><?php echo $usuario["nombres"] ?></td>
                <td><?php echo $usuario["apellidos"] ?></td>
                <td><?php echo $usuario["descrip_sexo"] ?></td>
            </tr>
          </tbody>
        </table>


        <table class="data table table-striped">
          <thead>
            <tr>
              <th>Dirección de Correo Electrónico</th>
              <th>Telefono</th>
              <th>Celular</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td><?php echo $usuario["correo"] ?></td>
                <td><?php echo $usuario["telefono"] ?></td>
                <td><?php echo $usuario["celular"] ?></td> 
            </tr>
          </tbody>
        </table>
    </div>
</div>
<hr>

  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Id_Tipo_Usuario">Nivel de Usuario</label>
      <select id="Id_Tipo_Usuario" name="Id_Tipo_Usuario" class="form-control selectpicker" required="" title="-Seleccione-" readonly>
        <?php
        if ($usuario["Id_Tipo_Usuario"]==265) {
          echo '<option value="264" >Usuario</option>
                <option value="265" selected>Administrador</option>';
        }
        else if ($usuario["Id_Tipo_Usuario"]==264) {
          echo '<option value="264" selected>Usuario</option>
                <option value="265">Administrador</option>';
        }
        ?>
      </select>
    </div>

    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Cod_Estatus">Estado</label>
      <select id="Cod_Estatus" name="Cod_Estatus" class="form-control selectpicker" required="" title="-Seleccione-" readonly>
        <?php
        if ($usuario["Cod_Estatus"]==0) {
          echo '<option value="0" selected>Activo</option>
                <option value="1">Inactivo</option>';
        }
        else{ 
          echo '<option value="0" >Activo</option>
                <option value="1" selected>Inactivo</option>';
        }
        ?>
      </select>
    </div>
    <label align="justify"><b>Nota: </b> Si selecciona en nivel de usuario la opción Administrador, sera desabilitada su cuenta, y su nivel sera modificado a usuario. Hasta que el nuevo Aministrador lo Habilite</label>
  </div>  
  
  <div class="form-group col-xs-12 col-md-12 col-xl-12" id="contrasena">
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="clave">Contraseña</label>
      <p align="justify">Se requiere la contraseña para poder modificar el nivel Administrador</p>
      <input type="password" id="clave" name="clave" class="form-control">
    </div>
  </div> 


    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
$('#contrasena').hide();
$('.selectpicker').selectpicker('refresh');

  $(document).ready(function(){
    $("#Id_Tipo_Usuario").change(function (){
        if ($('#Id_Tipo_Usuario option:selected').val() == '265') {
            $('#contrasena').show();
        }
        else{
            $('#contrasena').hide();
        }
    
    });
  });


  $('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    var parametros= new FormData($(this)[0]);
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: parametros,
        contentType:false,
        processData:false,
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==0) {
               notificacion(3,'fa fa-check','Completado!','Se ha modificado al usuario');
              setTimeout(function(){ 
                        location.href ="../";
                    }, 3000);
              
            }
            else if (respuesta==1) {
              redireccionar('../Controller/UsuariosController.php');
              $('#modificar').modal('hide');             
              notificacion(3,'fa fa-check','Completado!','Se ha modificado al usuario');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>