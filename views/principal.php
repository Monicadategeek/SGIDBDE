<?php session_start(); //abre sesion 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
  <meta charset="utf-8">
	<link href='../web/images/logosolo.png' rel='shortcut icon' type='image/png'>

    <title>Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil</title>
          <style>
      #contpreloader{
        top: 0;
        left: 0;
        z-index: 100000000;
        position: fixed;
        width: 100%;
        height: 100%;
        background: #fff;
      }
        .loader {
            border: 0px solid #c4c3c1;
        border-top: 1rem solid #ff473e;
        border-bottom: 1rem solid #0086dd;
        border-left: 3rem solid #ff5a00;
        border-radius: 50%;
        width: 150%;
        height: 105%;
        left: -25%;
        top: 8%;
            animation: spin 2s linear infinite;
        }
        .loader2 {
            border: 0px solid #c4c3c1;
        border-top: 1rem solid #ff473e;
        border-bottom: 1rem solid #0086dd;
        border-left: 3rem solid #ff5a00;
        border-radius: 50%;
        width: 114%;
        height: 82%;
        left: -18%;
        top: 25%;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .cssload-loader {
          position: relative;
          width: 296px;
          height: 296px;
          border-radius: 50%;
            -o-border-radius: 50%;
            -ms-border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
          perspective: 3700px;
        }

        .cssload-loader img {
          width: 35%;
          left: 32%;
          top: 30%;
          position: absolute;
        }

            

        .cssload-inner {
          position: absolute;
          width: 100%;
          height: 100%;
          box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
          border-radius: 50%;
            -o-border-radius: 50%;
            -ms-border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;  
        }

        .cssload-inner.cssload-one {
          left: 0%;
          top: 0%;
          animation: cssload-rotate-one 1.15s linear infinite;
            -o-animation: cssload-rotate-one 1.15s linear infinite;
            -ms-animation: cssload-rotate-one 1.15s linear infinite;
            -webkit-animation: cssload-rotate-one 1.15s linear infinite;
            -moz-animation: cssload-rotate-one 1.15s linear infinite;
          border-bottom: 14px solid rgb(255,71,62);
        }

        .cssload-inner.cssload-two {
          right: 0%;
          top: 0%;
          animation: cssload-rotate-two 1.15s linear infinite;
            -o-animation: cssload-rotate-two 1.15s linear infinite;
            -ms-animation: cssload-rotate-two 1.15s linear infinite;
            -webkit-animation: cssload-rotate-two 1.15s linear infinite;
            -moz-animation: cssload-rotate-two 1.15s linear infinite;
          border-right: 14px solid rgb(0,134,221);
        }

        .cssload-inner.cssload-three {
          right: 0%;
          bottom: 0%;
          animation: cssload-rotate-three 1.15s linear infinite;
            -o-animation: cssload-rotate-three 1.15s linear infinite;
            -ms-animation: cssload-rotate-three 1.15s linear infinite;
            -webkit-animation: cssload-rotate-three 1.15s linear infinite;
            -moz-animation: cssload-rotate-three 1.15s linear infinite;
          border-top: 14px solid rgb(255,90,0);
        }







        @keyframes cssload-rotate-one {
          0% {
            transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
          }
          100% {
            transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
          }
        }

        @-o-keyframes cssload-rotate-one {
          0% {
            -o-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
          }
          100% {
            -o-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
          }
        }

        @-ms-keyframes cssload-rotate-one {
          0% {
            -ms-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
          }
          100% {
            -ms-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
          }
        }

        @-webkit-keyframes cssload-rotate-one {
          0% {
            -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
          }
          100% {
            -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
          }
        }

        @-moz-keyframes cssload-rotate-one {
          0% {
            -moz-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
          }
          100% {
            -moz-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
          }
        }

        @keyframes cssload-rotate-two {
          0% {
            transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
          }
          100% {
            transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
          }
        }

        @-o-keyframes cssload-rotate-two {
          0% {
            -o-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
          }
          100% {
            -o-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
          }
        }

        @-ms-keyframes cssload-rotate-two {
          0% {
            -ms-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
          }
          100% {
            -ms-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
          }
        }

        @-webkit-keyframes cssload-rotate-two {
          0% {
            -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
          }
          100% {
            -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
          }
        }

        @-moz-keyframes cssload-rotate-two {
          0% {
            -moz-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
          }
          100% {
            -moz-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
          }
        }

        @keyframes cssload-rotate-three {
          0% {
            transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
          }
          100% {
            transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
          }
        }

        @-o-keyframes cssload-rotate-three {
          0% {
            -o-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
          }
          100% {
            -o-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
          }
        }

        @-ms-keyframes cssload-rotate-three {
          0% {
            -ms-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
          }
          100% {
            -ms-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
          }
        }

        @-webkit-keyframes cssload-rotate-three {
          0% {
            -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
          }
          100% {
            -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
          }
        }

        @-moz-keyframes cssload-rotate-three {
          0% {
            -moz-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
          }
          100% {
            -moz-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
          }
        }
        </style>


    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../web/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../web/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../web/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../web/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../web/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="../web/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../web/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../web/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href='../web/css/fullcalendar.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../web/css/perfect-scrollbar.css" />
    <link href="../web/css/pnotify.custom.css" rel="stylesheet" />
    <link href="../web/css/animate.css" rel="stylesheet" />
    <link href="../web/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">  
    <link href="../web/css/bootstrap-datepicker.css" rel="stylesheet">  
    <link href="../web/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../web/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../web/css/owl.theme.default.min.css">
     <!-- jQuery custom content scroller -->
    <link href="../web/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../web/css/export.css" type="text/css" media="all" />

   

    <!-- Custom Theme Style -->
    <link href="../web/build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md" onload="principal();">


  <div id="contpreloader">
          <div style="position: relative;
    padding-top: 10%;
    width: 23%;
    margin: auto;">
            <div class="cssload-loader">
              <img src="<?php include('logo.php');?>">
              <div class="cssload-inner cssload-one"></div>
              <div class="cssload-inner cssload-two"></div>
              <div class="cssload-inner cssload-three"></div>
            </div>
            
          </div>
          
          
          <br><p class="title3" style="text-align: center; color: #333;font-size: 3rem;font-weight: bold;font-size: 3rem;line-height: normal;">Cargando...<br> Por favor espere.</p><br>
        </div>
<!-- Modal-->
<!-- Modal Cargando-->
 <div class="modal fade" id="CargandoModal" role="dialog" aria-labelledby="CargandoModalLabel" aria-hidden="true" tabindex="-1" role="dialog" class="modal fade ng-isolate-scope in" modal-in-class="in" style="z-index: 1045;">
    <div class="modal-dialog modal-sm" role="document" style="top:25%;z-index: 1060;">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div >
          <div style="position: relative;padding-top: 15%;width: 50%;margin: auto;">
            <div class="loader" style="position: absolute;"></div>
            <img style="width: 100%;" src="../web/images/Logosolo.png">
          </div>
          
          
          <br><br><br><p class="title3" style="text-align: center; color: #333;font-size: 2rem;font-weight: bold;">Cargando...<br> Por favor espere.</p><br>
        </div>

      </div>

      </div>
      </div>
      <div class="modal-backdrop fade in" modal-in-class="in" id="cargandobackdrop" style="display:none;z-index: 1050;opacity: 0;" 
      ></div>

<input type="hidden" id="actual-background" value="Fondo14">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed" id="main-menu">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title">
              	<img src="../web/images/logo.png" style="width: 100%;" id="logoimg1">
              	<img src="../web/images/logosolo.png" style="width: 100%;" id="logoimg2">
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">

                <a onclick="redireccionar('../Controller/MyUserController.php');"><img src="<?php echo $_SESSION['Foto_Usuario']; ?>" alt="<?php $nombre = $_SESSION['USUARIO']; echo ucwords(strtolower($nombre));?>" class="img-circle profile_img"></a> 
              </div>
              <div class="profile_info">
                <a onclick="redireccionar('../Controller/MyUserController.php');"><p><?php $nombre = $_SESSION['USUARIO']; echo ucwords(strtolower($nombre));?></p></a>
              </div>
            </div>
            <!-- /menu profile quick info -->


            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="#" onclick="redireccionar('../Controller/articulocontroller.php?accion=blog');"><i class="fa fa-book"></i>Blog</a></li>
                  <?php 
                  if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') !== false) or $_SESSION['Tipo_Usuario']==265) {
                  ?>
                    <li><a><i class="fa fa-building"></i> Departamentos <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <?php
                        if ($_SESSION['Tipo_Usuario']==265) {
                        ?>                     
                          <li><a onclick="redireccionar('../Controller/DepartamentosController.php');">Registro de Departamentos</a></li>
                          <li><a onclick="redireccionar('../Controller/PersonasController.php');">Personas</a></li>
                        <?php
                        }
                        ?>
                        <li><a onclick="redireccionar('../Controller/TrabajadoresController.php');">Trabajadores</a></li>
                      </ul>
                    </li>
                  <?php
                  }
                  ?>
                  <li><a><i class="fa fa-list-alt"></i> Servicios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php 
                      if ($_SESSION['Tipo_Usuario']=='Estudiante') {
                        ?>
                        <li><a onclick="redireccionar('../Controller/CitasController.php');">Solicitud de Citas</a></li>
                      <?php
                      }
                      else
                      {
                      ?>
                        <li><a onclick="redireccionar('../Controller/GestionCitasController.php');">Atención de Citas</a></li>
                        <li><a onclick="redireccionar('../Controller/ConsultasController.php');">Consultas</a></li>
                        <?php 
                        if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') !== false) or $_SESSION['Tipo_Usuario']==265) {
                        ?>
                          <li><a onclick="redireccionar('../Controller/CancelarCitasController.php');">Suspención de Servicios</a></li>
                        <?php
                        }
                      }
                      ?>
                    </ul>
                  </li>
                  <?php 
                  if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoDepto'], '5') !== false) or $_SESSION['Tipo_Usuario']==265) {
                  ?>

                  <li><a><i class="fa fa-money"></i> Control de Becas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php 
                        if (($_SESSION['Tipo_Usuario']==264 and (strpos($_SESSION['TipoPersonal'], '5') !== false or strpos($_SESSION['TipoPersonal'], '6') !== false)) or $_SESSION['Tipo_Usuario']==265) {
                        ?>                    
                          <li><a onclick="redireccionar('../Controller/PresupuestoController.php');">Presupuesto</a></li>
                          <li><a onclick="redireccionar('../Controller/InscripcionesController.php');">Inscripciones</a></li>
                        <?php
                          }                        
                        ?>                      
                      <li><a onclick="redireccionar('../Controller/BecariosController.php?tipo=1');">Control Becarios</a></li>
                      <li><a onclick="redireccionar('../Controller/BecariosController.php?tipo=2');">Becarios Inactivos</a></li>
                      <li><a onclick="redireccionar('../Controller/PresupuestoController.php?accion=reportes');">Reporte Mensual</a></li>
                    </ul>
                  </li>
                  <?php
                    }  
                  if ($_SESSION['Tipo_Usuario']!='Estudiante') {
                    ?>
                  <li><a><i class="fa fa-bar-chart"></i> Estadisticas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccionar('../Controller/EstadisticasEstudiantilesController.php');">Estudiantiles</a></li>
                      <li><a onclick="redireccion('servicios');">Servicios</a></li>
                      <li><a onclick="redireccion('departamentos');">Departamentos</a></li>
                    </ul>
                  </li>
                  <?php
                    } 
                    if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') !== false) or $_SESSION['Tipo_Usuario']==265)                     
                   {
                    ?>

                  <li><a><i class="fa fa-table"></i> Mantenimiento <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <?php 
                      if ($_SESSION['Tipo_Usuario']==265){ ?>
                        <li><a onclick="redireccionar('../Controller/UsuariosController.php');">Usuarios</a></li>
                      <?php
                      }
                      if ($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') !== false  or $_SESSION['Tipo_Usuario']==265 ) { ?>
                        <li><a onclick="redireccionar('../Controller/TablasReferencialesController.php');">Tablas Referenciales</a></li>
                      <?php
                      }
                      if (($_SESSION['Tipo_Usuario']==264 and strpos($_SESSION['TipoPersonal'], '5') and  strpos($_SESSION['TipoDepto'], '4') !== false ) or $_SESSION['Tipo_Usuario']==265)  { ?>
                        <li><a onclick="redireccionar('../Controller/FechasEncuestaController.php');">Fechas Encuestas</a></li>
                        <?php
                      }
                    ?>  
                      <li><a onclick="redireccionar('../Controller/articulocontroller.php');">Blog</a></li>
                       <?php 
                      if ($_SESSION['Tipo_Usuario']==265){ ?>
                        <li><a onclick="redireccion('opiniones');">Opiniones</a></li>
                      <?php
                      }                      
                      ?> 
                                            
                    </ul>
                  </li>
                  <?php
                    } 
                  ?>

                </ul>
              </div>       

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a title="Cambiar Tema" href="#" data-toggle="modal" data-target="#Cambiar-Tema">
                <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
              </a>
              <a title="Reloj" href="#" data-toggle="modal" data-target="#main-clock">
                <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
              </a>
              <a title="Pantalla Completa" onclick="fullscreen()">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a title="Salir" href="security/login.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav" >
          <div class="nav_menu" id="top-menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="../web/javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $_SESSION['Foto_Usuario']; ?>" alt="<?php $nombre = $_SESSION['USUARIO']; echo ucwords(strtolower($nombre));?>" class="img-circle">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a onclick="redireccionar('../Controller/MyUserController.php');"> Perfil</a></li>
                    <li>
                      <a href="#" data-toggle="modal" data-target="#Cambiar-Tema">
                        Cambiar Tema
                      </a>
                    </li>
                    <li><a href="../web/javascript:;">Ayuda</a></li>
                    <li><a href="security/login.php"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown" id="body">
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" id="contenido">


         
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <small class="block">&copy; 2017 IUTOMS.</small> 
            <small class="block">Desarrollado por grupo N° 7</small>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Modal Agregar-->
   <div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalagregar">
        </div>

      </div>
    </div>

    <!-- Modal Modificar-->
   <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalmodificar">
        </div>

      </div>
    </div>

     <!-- Modal Ver-->
   <div class="modal fade" id="ver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalver">
        </div>

      </div>
    </div>



     <!-- Modal Cambiar-Tema-->
   <div class="modal fade" id="Cambiar-Tema" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h3 class="modal-title" id="myLargeModalLabel">Cambiar Tema</h3> </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
              <h4>Fondo Menu - Color</h4>
              <div class="owl-carousel owl-theme">
                  <div class="item"><a href="#" class="item-action" value="fondocolor1"><div class="item-fondo" style="background: #f44336"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor2"><div class="item-fondo" style="background: #e91e63"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor3"><div class="item-fondo" style="background: #9c27b0"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor4"><div class="item-fondo" style="background: #673ab7"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor5"><div class="item-fondo" style="background: #3f51b5"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor6"><div class="item-fondo" style="background: #2196f3"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor7"><div class="item-fondo" style="background: #03a9f4"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor8"><div class="item-fondo" style="background: #00bcd4"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor9"><div class="item-fondo" style="background: #009688"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor10"><div class="item-fondo" style="background: #4caf50"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor11"><div class="item-fondo" style="background: #8bc34a"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor12"><div class="item-fondo" style="background: #cddc39"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor13"><div class="item-fondo" style="background: #ffeb3b"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor14"><div class="item-fondo" style="background: #ffc107"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor15"><div class="item-fondo" style="background: #ff9800"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor16"><div class="item-fondo" style="background: #ff5722"></div></a></div>
                  <div class="item"><a href="#" class="item-action" value="fondocolor17"><div class="item-fondo" style="background: #795548"></div></a></div>
              </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
              <h4>Fondo Menu - Imagen</h4>
              <div class="owl-carousel owl-theme">
                  <div class="item"><a href="#" class="item-action2" value="Fondo14"><img src="../web/images/Fondos/Fondo14.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo1"><img src="../web/images/Fondos/Fondo1.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo2"><img src="../web/images/Fondos/Fondo2.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo3"><img src="../web/images/Fondos/Fondo3.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo4"><img src="../web/images/Fondos/Fondo4.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo5"><img src="../web/images/Fondos/Fondo5.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo6"><img src="../web/images/Fondos/Fondo6.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo7"><img src="../web/images/Fondos/Fondo7.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo8"><img src="../web/images/Fondos/Fondo8.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo9"><img src="../web/images/Fondos/Fondo9.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo10"><img src="../web/images/Fondos/Fondo10.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo11"><img src="../web/images/Fondos/Fondo11.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo12"><img src="../web/images/Fondos/Fondo12.jpg" class="item-fondo"></a></div>
                  <div class="item"><a href="#" class="item-action2" value="Fondo13"><img src="../web/images/Fondos/Fondo13.jpg" class="item-fondo"></a></div>
              </div>
            </div>
            </div>
          </div>
        </div>

      </div>
    </div>


         <!-- Modal main-clock-->
   <div class="modal fade" id="main-clock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      

            <div id="clock" class="light">
              <div class="display">
                <div class="weekdays"></div>
                <div class="ampm"></div>
                <div class="alarm"></div>
                <div class="digits"></div>
              </div>
            </div>

      </div>
    </div>

    <input type="hidden" id="printcss" value='<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../web/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../web/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../web/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../web/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../web/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="../web/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../web/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../web/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../web/css/fullcalendar.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../web/css/perfect-scrollbar.css" />
    <link href="../web/css/pnotify.custom.css" rel="stylesheet" />
    <link href="../web/css/animate.css" rel="stylesheet" />
    <link href="../web/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../web/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">  
    <link href="../web/css/bootstrap-datepicker.css" rel="stylesheet">  
    <link href="../web/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../web/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../web/css/owl.theme.default.min.css">
     <!-- jQuery custom content scroller -->
    <link href="../web/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

   

    <!-- Custom Theme Style -->
    <link href="../web/build/css/custom.css" rel="stylesheet">'>

    <!-- jQuery -->
    <script src="../web/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../web/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../web/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../web/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../web/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../web/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../web/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../web/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../web/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../web/vendors/Flot/jquery.flot.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../web/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../web/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../web/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../web/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../web/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../web/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../web/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../web/vendors/moment/min/moment.min.js"></script>
    <script src="../web/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../web/js/bootstrap-select.min.js"></script>

    

    <script src="../web/js/queryloader2.min.js"></script>
    <script src="../web/js/pnotify.custom.js" type="text/javascript"></script>
    <script src="../web/js/jquery.mask.min.js"></script>

    <!-- Datatables -->
    <script src="../web/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../web/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../web/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../web/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../web/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../web/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../web/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../web/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../web/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../web/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src='../web/js/jquery-ui.min.js'></script>
    <script src="../web/js/moment.js" type="text/javascript"></script>
    <script src="../web/js/fullcalendar.js" type="text/javascript"></script>
    <script src="../web/js/locale-all.js" type="text/javascript"></script>
    <script src="../web/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="../web/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    
    <!-- Resources -->
    <script src="../web/js/jquery.nicescroll.min.js"></script>
    <script src="../web/js/owl.carousel.min.js"></script>
    <script src="../web/js/amcharts.js"></script>
    <script src="../web/js/pie.js"></script>
    <script src="../web/js/export.min.js"></script>
    <script src="../web/js/fabric.min.js"></script>
    <script src="../web/js/FileSaver.min.js"></script>
    
    <script src="../web/js/serial.js"></script>
    
    <script src="../web/js/light.js"></script>
    <script src="../web/ckeditor/ckeditor.js"></script>


<!-- jQuery custom content scroller -->
    <script src="../web/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>


   
   
    
     <script src="../web/js/bootstrap-datepicker.js" type="text/javascript"></script>

     <script src="../web/js/bootstrap-datepicker.es.js" type="text/javascript"></script>
     <script type="text/javascript">
     $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          nav:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:3
              },
              1000:{
                  items:5
              }
          }
      })
     $.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mie", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "mm-dd-yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
  };
   </script>
     
    <!-- Custom Theme Scripts -->
    <script src="../web/build/js/custom.js"></script>
    <script src="../web/js/md5.min.js"></script>
    <script src="../web/js/main.js"></script>
    
    <script type="text/javascript">
      redireccionar('../Controller/articulocontroller.php?accion=blog');

     
    </script>
  </body>
</html>

