
     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Control de Becarios 
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Becarios Activos</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="becarioestudiante" > 
                <thead> 
                    <th>Cédula</th>
                    <th>Apellidos</th>
                    <th>Nombres</th>
                    <th>Sexo</th>
                    <th>PNF</th>
                    <th>Modalidad</th>
                    <th>Fecha de Inscripción</th>
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Cédula</th>
                    <th>Apellidos</th>
                    <th>Nombres</th>
                    <th>Sexo</th>
                    <th>PNF</th>
                    <th>Modalidad</th>
                    <th>Fecha de Inscripción</th>
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($becarios) || is_object($becarios))
                        {
                            
                            foreach ($becarios as $becario)
                            {                                
                                echo "<tr>
                                <td>".$becario["cedula"]."</td>
                                <td>".$becario["nombres"]."</td> 
                                <td>".$becario["apellidos"]."</td>
                                <td>".$becario["sexo"]."</td>                                
                                <td>".$becario["cod_carrera"]."</td>
                                <td>".$becario["TipoBeca"]."</td>
                                <td>".$becario["FechaRegistro"]."</td>";
                                echo '<td>';
                                echo '<button class="btn btn-info btn-xs" title="Ver" onclick="VerBecarios('."'../Controller/BecariosController.php',".$becario["Becario"].');"><i class="fa fa-book"></i></button>';                                         
                                echo "</td></tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="horariosModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg2" role="document">

    <div class="modal-content" style="padding: 20px;" id="horarioscontent">

      
    </div>
  </div>
</div>
           
        <!-- /.row -->

    <script type="text/javascript">
    $(document).ready(function(){

$('#becarioestudiante').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Citas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        dom: "Bfrtip",
        buttons: [
        {
            extend: 'pdfHtml5',
            text: '<i class="fa fa-print" ></i> Imprimir',
            filename:"Becarios Activos - SGIDBDE",
            title:"Listado de Becarios Activos",
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6],
                        search: 'applied',
                        order: 'applied'
            },
            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    customize: function (doc) {
                        doc.content[1].table.widths = 
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.content.splice(0,0);
                        //Remove the title created by datatTables
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
                        var now = new Date();
                        var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                        // Logo converted to base64
                        // var logo = getBase64FromImageUrl('https://datatables.net/media/images/logo.png');
                        // The above call should work, but not when called from codepen.io
                        // So we use a online converter and paste the string in.
                        // Done on http://codebeautify.org/image-to-base64-converter
                        // It's a LONG string scroll down to see the rest of the code !!!
                        // A documentation reference can be found at
                        // https://github.com/bpampuch/pdfmake#getting-started
                        // Set page margins [left,top,right,bottom] or [horizontal,vertical]
                        // or one number for equal spread
                        // It's important to create enough space at the top for a header !!!
                        doc.pageMargins = [20,100,20,30];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 9;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 9;
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        image: logo,
                                        width: 150
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 12,
                                        text: 'Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil'
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages
                        doc['footer']=(function(page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        text: ['Generado el: ', { text: jsDate.toString() }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['pagina ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['hLineWidth'] = function(i) { return .5; };
                        objLayout['vLineWidth'] = function(i) { return .5; };
                        objLayout['hLineColor'] = function(i) { return '#aaa'; };
                        objLayout['vLineColor'] = function(i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function(i) { return 4; };
                        objLayout['paddingRight'] = function(i) { return 4; };
                        doc.content[0].layout = objLayout;
                        
                }

            }
        ],
        responsive: true
    });
// Setup - add a text input to each footer cell
    $('#becarioestudiante tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#becarioestudiante').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([3,4,5]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>