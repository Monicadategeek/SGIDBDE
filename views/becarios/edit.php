<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificar datos de la Beca del Estudiante</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/BecariosController.php" method="POST">
    <input type="hidden" name="IdBecarios" id="IdBecarios" value="<?php echo $becario["IdBecarios"];?>">
  <input type="hidden" name="IdInscripciones" id="IdInscripciones" value="<?php echo $becario["IdInscripciones"];?>">

  <input type="hidden" name="accion" id="accion" value="update">
  <div class="col-md-12" align="center">
    <div class="col-md-12">
      <label>Datos del Estudiante</label>
      <table class="data table table-striped">
        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Cédula</th>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $becario["nombres"];?></td>
            <td><?php echo $becario["apellidos"];?></td>
            <td><?php echo $becario["cedula"];?></td>
          </tr>
        </tbody>    
      </table>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group col-xs-12 col-md-6"> 
      <label for="CtaBancaria">Nro Cuenta Bancaria</label>
      <input type="text" class="form-control" name="CtaBancaria" id="CtaBancaria" value="<?php echo $becario["CtaBancaria"];?>">
    </div>
    <div class="form-group col-xs-12 col-md-6"> 
      <label for="BecaAsociada">Tipo de Beca</label>
      <select name="BecaAsociada" id="BecaAsociada" class="form-control selectpicker" required="">
        <?php 
              if (is_array($becas) || is_object($becas))
              {
                  foreach ($becas as $beca)
                  {
                    if ($beca["IdSinceracionBecas"]==$becario["BecaAsociada"]) {
                      echo "<option value='".$beca["IdSinceracionBecas"]."' selected>".$beca["TipoBeca"]."</option>";
                    }
                    else{
                      echo "<option value='".$beca["IdSinceracionBecas"]."'>".$beca["TipoBeca"]."</option>";                      
                    }
                  }
              }
              ?>
      </select>
    </div>
  </div>
  <div class="col-md-12" align="center" id="egreso">
    <input type="hidden" name="egresar" id="egresar" value="0">
    <label for="egreso">Ingrese Brevemente la razón del egreso del Estudiante</label>
    <textarea id="egreso" class="form-control" name="egreso"></textarea>
  </div> 

    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="margin-top: 23px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <button class="btn btn-danger btn-large" id="botonegreso" type="button" onclick="egresarestudiante();">Egresar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>
<script type="text/javascript">

  $('#egreso').hide();

  function egresarestudiante(){
    $('#egreso').show();
    $('#egresar').val("1");
    document.getElementById("botonegreso").disabled = true;
    

  }

  $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
     
    });

  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              VerBecarios('../Controller/BecariosController.php', $('#IdBecarios').val());
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado los datos de la beca');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>