     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Control de Becarios
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
        <h2>Visualización de Becarios</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
        <div class="profile_img">
          <div id="crop-avatar">
            <!-- Current avatar -->
            <?php 
            if ($estudiante["foto"]!=0) {
                echo '<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$estudiante["foto"].'" alt="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'" title="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'">';
            }
            else{
                echo '<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
            } ?>
          </div>
        </div>
        <h3><?php echo $estudiante["nombres"]." ".$estudiante["apellidos"]?></h3>

        <ul class="list-unstyled user_data">
          <li>
            <i class="fa fa-graduation-cap" aria-hidden="true"></i> <?php echo $estudiante["cod_carrera"] ?>
          </li>
          <li>
            <i class="fa fa-book"></i> <b>Trayecto</b> <?php echo $estudiante["trayecto"] ?>
          </li>
          <li>
            <i class="fa fa-book"></i> <b>Trimestre</b> <?php echo $estudiante["trimestre"] ?>
          </li>
          <li>
            <i class="fa fa-book"></i> <b>Sección</b> <?php echo $estudiante["seccion"] ?>
          </li>
          <li>
            <i class="fa fa-clock-o"></i> <b>Turno</b> <?php echo $estudiante["turno"] ?>
          </li>
        </ul>
        <br/>
      </div>
      <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos Estudiantiles</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Datos Academicos</a>
            </li>
            <li role="presentation" class="">
              <a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Datos Becarios</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              <!-- start user projects -->
              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Cédula</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Sexo</th>
                    <th>Fecha Nacimiento</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td><?php echo $estudiante["nacionalidad"]." ".$estudiante["cedula"] ?></td>
                      <td><?php echo $estudiante["nombres"] ?></td>
                      <td><?php echo $estudiante["apellidos"] ?></td>
                      <td><?php echo $estudiante["sexo"] ?></td>
                      <td><?php echo $estudiante["fec_nac"] ?></td>
                  </tr>
                </tbody>
              </table>

              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Dirección</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td><?php echo $estudiante["direccion"] ?></td> 
                  </tr>
                </tbody>
              </table>

              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Dirección de Correo Electrónico</th>
                    <th>Telefono</th>
                    <th>Celular</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td><?php echo $estudiante["correo"] ?></td>
                      <td><?php echo $estudiante["celular"] ?></td>
                      <td><?php echo $estudiante["tel_hab"] ?></td> 
                  </tr>
                </tbody>
              </table>

              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Titulo de Bachiller</th>
                    <th>Tipo de institución</th>
                    <th>Promedio de notas (Bachillerato)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td><?php 
                          if (isset($datosacademicos["mencion"])) {
                              echo $datosacademicos["mencion"];
                          } ?>
                      </td>
                      <td><?php 
                          if (isset($datosacademicos["tipo_inst"])) {
                              echo $datosacademicos["tipo_inst"];
                          } ?>
                      </td>
                      <td><?php 
                          if (isset($datosacademicos["prom_notas_bach"])) {
                              echo $datosacademicos["prom_notas_bach"];
                          }
                      ?></td>                                    
                  </tr>
                </tbody>
              </table>

              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Carrera</th>
                    <th>Forma de Ingreso</th>
                    <th>Fecha de Ingreso</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td><?php echo $estudiante["cod_carrera"] ?></td>                                 
                      <td><?php echo $estudiante["forma_ingreso"] ?></td>
                      <td><?php echo $estudiante["fec_ingreso"] ?></td>
                  </tr>
                </tbody>
              </table>
              
              <!-- end user projects -->
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Trayecto</th>
                    <th>Codigo</th>
                    <th>Unidad Curricular</th>
                    <th>Creditos</th>
                    <th>Nota</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $trayecto="";
                  if (count($notas)>0) {
                     foreach ($notas as $nota) {
                      if ($nota["trayecto"]!='') {
                        echo "<tr>";
                        if ($trayecto!=$nota["trayecto"]) {
                            $trayecto=$nota["trayecto"];
                            echo "<td>".$trayecto."</td>";
                        }
                        else{
                            echo "<td></td>";
                        }
                        echo "
                        <td>".$nota["cod_catedra"]."</td>
                        <td>".$nota["nombremateria"]."</td>
                        <td>".$nota["creditos"]."</td>
                        <td>".$nota["nota"]."</td>

                        <td></td>
                        ";
                        echo "</tr>";
                      }                        
                    }
                  }
                  else{
                    echo "<tr><td colspan='5'>Disculpe, el IUTOMS Siace no tiene registrada notas del estudiante</td></tr>";
                  }
                  ?>
                </tbody>
              </table>
              <!-- end user projects -->
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">                        
              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Nro Becario</th>
                    <th>Cuenta Bancaria</th>
                    <th title="Fecha en que se registro la cuenta Bancaria">Fecha de Registro</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $becario["IdBecarios"] ?></td>
                    <?php 
                    if ($becario["CtaBancaria"]!='') {
                      echo "<td>".$becario["CtaBancaria"]."</td><td>".$becario["FechaCtaBanca"]."</td>";
                    }
                    else{
                      echo "<td colspan='2'>Cobre por Cheque</td>";
                    }
                      ?>
                  </tr>
                </tbody>
              </table>

              <h3>Inscripciones</h3>
              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Nro Inscripción</th>
                    <th>Tipo_Inscripcion</th>
                    <th>Fecha Registro</th>
                    <th>Modalidad</th>
                    <th>Estado</th>
                    <th title="Responsable del registro">Responsable</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    if (count($historialinscripciones)>0) {
                     foreach ($historialinscripciones as $historial) {           
                        echo "
                        <td>".$historial["IdInscripciones"]."</td>
                        <td>".$historial["Tipo_Inscripcion"]."</td>
                        <td>".$historial["FechaRegistro"]."</td>
                        <td>".$historial["TipoBeca"]."</td>
                        <td>".$historial["Estado"]."</td>
                        <td>".$historial["Responsable"]."</td><td>";

                        if ($historial["Estado"]=='Activo') {
                          echo '<button class="btn btn-warning btn-xs" title="Modificar datos de la Beca" onclick="modificar('."'../Controller/BecariosController.php'".', '.$historial["IdInscripciones"].');" ><i class="fa fa-pencil"></i> </button>';;
                        }                         
                        echo "</td></tr>";
                      }
                        
                    }                              
                  ?>
                </tbody>
              </table>


              <h3>Egresos</h3>
              <table class="data table table-striped">
                <thead>
                  <tr>
                    <th>Inscripción</th>
                    <th>FechaRegistro</th>
                    <th title="Responsable del registro">Responsable</th>
                    <th>Razón del Egreso</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if (count($historialegresos)>0) {
                   foreach ($historialegresos as $historial) {           
                      echo "
                      <td>".$historial["Inscripcion"]."</td>
                      <td>".$historial["FechaRegistro"]."</td>
                      <td>".$historial["Responsable"]."</td>
                      <td>".$historial["descripcion"]."</td>
                      ";
                      echo "</tr>";
                    }                      
                  }  
                ?>                  
                </tbody>
              </table>
            </div>                          
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
           
        <!-- /.row -->
