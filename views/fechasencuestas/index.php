<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><i class="fa fa-table"></i> Mantenimiento
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Registro de las Fechas para las Encuestas Estudiantiles</h2>

            <?php

            	if (is_array($ultfecha) && count($ultfecha)>0 ) {
            		if (date('Y-m-d')>$ultfecha["Fechafin"]) {
            			?>
            			<div align="right">
			            	<button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/FechasEncuestaController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Registro</button> 
			            </div>
			            <?php
            		}
            	}
            	else{ ?>
            		<div align="right">
		            	<button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/FechasEncuestaController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Registro</button> 
		            </div>

            	<?php
            	}
            ?>
            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="FechasEncuestas"> 
                <thead> 
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>Responsable</th>
                    <th>Fecha Registro</th>
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>Responsable</th>
                    <th>Fecha Registro</th>
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($FechasEncuestas) || is_object($FechasEncuestas))
                        {
                            
                            foreach ($FechasEncuestas as $FechasEncuesta)
                            {



                                echo "<tr>
                                <td>".date('d-m-Y', strtotime($FechasEncuesta["Fechainicio"]))."</td>
                                <td>".date('d-m-Y', strtotime($FechasEncuesta["Fechafin"]))."</td>
                                <td>".$FechasEncuesta["Responsable"]."</td>
                                <td>".date('d-m-Y g:ia', strtotime($FechasEncuesta["FechaRegistro"]))."</td>"
                                ;

                                if (date('Y-m-d')>=$ultfecha["Fechainicio"] and date('Y-m-d')<=$FechasEncuesta["Fechafin"] or $FechasEncuesta["Fechafin"]>date('Y-m-d')) {                        
                                    echo '<td>
                                    <button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/FechasEncuestaController.php',".$FechasEncuesta["Id"].');"><i class="fa fa-pencil"></i></button></td>
                                    ';
                                }
                                else{
                                	echo "<td></td>";
                                }

                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>


    <script type="text/javascript">
    $(document).ready(function(){

$('#FechasEncuestas').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Fechas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#FechasEncuestas tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#FechasEncuestas').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns(2).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>



   
