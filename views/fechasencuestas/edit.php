 
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación del registro de Fecha</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="editar" action="../Controller/FechasEncuestaController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="update">
  <input type="hidden" name="Id" id="Id" value="<?php echo $feechasencuesta["Id"]?>">
    
    <div class="col-xs-12 col-md-12 col-xl-12">
    	<p align="justify"><b>Nota</b> el registro de fechas, es el encargado de habilitarle a los estudiantes el registro de encuestas para la division de bienestar y desarrollo estudiantil. El rango mas amplio de fechas es de 3 meses exactos. Evite periodos largos de registros</p>
    	<?php
    		if ($feechasencuesta["Fechainicio"]<=date('Y-m-d') ) {
    			echo '<div class="form-group col-xs-6 col-md-6 col-xl-6">
			    		<label class="control-label" for="Fechainicio">Fecha Incio</label>
						<input type="text" name="Fechainicio" id="Fechainicio" class="form-control datepicker" disabled="" value="'.date('d-m-Y', strtotime($feechasencuesta["Fechainicio"])).'">    		
			    	</div>';
			    echo '<div class="form-group col-xs-6 col-md-6 col-xl-6">
			    		<label class="control-label" for="Fechafin">Fecha Fin</label>
						<input type="text" name="Fechafin" id="Fechafin" class="form-control datepicker" value="'.date('d-m-Y', strtotime($feechasencuesta["Fechafin"])).'">
			    	</div>';
    		}
    		else{
    			?>
    			<div  class="col-xs-10 col-md-10 col-xl-10" style="margin-top:25px;  margin-bottom: 17px">
				        <div class=" input-daterange input-group" id="datepicker">
				          <span class="input-group-addon">Desde</span>
				          <input type="text" class="input-sm form-control" name="Fechainicio" id="Fechainicio" maxlength="10" value="<?php echo date('d-m-Y', strtotime($feechasencuesta["Fechainicio"]))?>"/>
				          <span class="input-group-addon">Hasta</span>
				          <input type="text" class="input-sm form-control" name="Fechafin" id="Fechafin" maxlength="10" value="<?php echo date('d-m-Y', strtotime($feechasencuesta["Fechafin"]))?>"/>
				        </div>
				      </div>
				<?php

    		}
    	?>
    	
    	
    </div>



    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
$('.input-daterange').datepicker({
    format: 'dd-mm-yyyy',
	startDate:'+0d',
    language: 'es',

});

$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
		startDate:'+0d',
		endDate :'+2m',
        language: 'es',

});



  $('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();

    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/FechasEncuestaController.php?accion=index');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado las fechas para las encuestas');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!', data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>