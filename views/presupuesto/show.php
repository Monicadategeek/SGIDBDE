
<div class="modal-header" id="modalpresupuestotitulo">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h3 class="modal-title">Ver Presupuesto</h3>
  <a class="btn btn-default btn-sm buttons-pdf" href="#" onclick="tomarcaptura('contenidomodalver .modal-body');" style="float: left;"><i class="fa fa-print"></i> Imprimir</a>
</div>
<div class="modal-body">
  <h3>Presupuesto Planificado</h3>

  <div class="row Imprimir" style="padding: 0px 20px;">
 
  <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
    <tr>
      <th>Id</th>
      <th>Comienzo</th>      
      <th>Duración</th>
    </tr>
    <tr>
    <td>
        <?php echo $presupuesto1["IdPresupuesto"];?>
      </td>
      <td> 
        <?php echo $presupuesto1["Fecha_Comienzo"];    ?>
      </td>
      <td>
        <?php echo $presupuesto1["TiempoBeca"];    ?>
        <?php 
        ?>
      </td>

    </tr>
     <tr>
      <th colspan="3">Descripción</th>
    </tr>
    <tr>
      <td colspan="3">
        <?php echo $presupuesto1["Descripcion"];?>
      </td>
    </tr>
    <tr>
      <th>Fecha Registro</th>
      <th>Ultima Modificación</th>
      <th>Responable</th>
    </tr>
    <tr>
      <td> 
        <?php echo $presupuesto1["FechaRegistro"];    ?>
      </td>
       <td> 
        <?php echo $responsable1["FechaRegistro"];    ?>
      </td>
      <td>
        <?php echo $responsable1["Responsable"];?>
      </td> 
    </tr>
  </table> 

  <?php 

  if (isset($presupuesto2)) {?>
    <h3>Presupuesto Definitivo</h3>
  <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
    <tr>
      <th>Id</th>
      <th>Comienzo</th>      
      <th>Duración</th>
    </tr>
    <tr>
      <td>
        <?php echo $presupuesto2["IdPresupuesto"];?>
      </td>
      <td> 
        <?php echo $presupuesto2["Fecha_Comienzo"];    ?>
      </td>
      <td>
        <?php echo $presupuesto2["TiempoBeca"];    ?>
        <?php 
        ?>
      </td>
    </tr>
     <tr>
      <th colspan="3">Descripción</th>
    </tr>
    <tr>
      <td colspan="3">
        <?php echo $presupuesto2["Descripcion"];?>
      </td>
    </tr>
    <tr>
      <th>Fecha Registro</th>
      <th>Ultima Modificación</th>
      <th>Responable</th>
    </tr>
    <tr>
      <td>
        <?php echo $presupuesto1["FechaRegistro"];   ?>  
      </td>
      <td>  
        <?php echo $responsable2["FechaRegistro"];   ?> 
      </td>
      <td>
        <?php echo $responsable2["Responsable"];?>
      </td> 
    </tr>
  </table>

  <?php
  }
  ?>

  <h3>Desarrollo de las Becas</h3>
  <div class="col-xs-12 col-md-12 col-xl-12">
  <?php if (isset($presupuesto2)) {?>
    <div class="col-xs-6 col-md-6 col-xl-6">
  <?php
    }
    else{
  ?>
    <div class="col-xs-12 col-md-12 col-xl-12">
  <?php
    }
  ?>
    
      <table class="table table-striped">
        <thead>  
          <th colspan="3">Presupuesto Planificado</th>
        </thead>
        <tbody>
        <tr>
          <td>Modalidad</td>
          <td>Monto</td>
          <td>Nro de Becarios</td>
        </tr>
        <?php foreach ($sinceracionbecas1 as $sinceracionbeca1) {
          echo "<tr>";
          echo "<td>".$sinceracionbeca1["TipoBeca"]."</td>";
          echo "<td>".$sinceracionbeca1["Monto"]."</td>";
          echo "<td>".$sinceracionbeca1["NroCupos"]."</td>";
          echo "</tr>";
         
        }
        ?>

        </tbody>
    </table>
      </div>
      <?php if (isset($presupuesto2)) {?>
        <div class="col-xs-6 col-md-6 col-xl-6">
      <table class="table table-striped">
        <thead>  
          <th colspan="3">Presupuesto Definitivo</th>
        </thead>
        <tbody>
        <tr>
          <td>Modalidad</td>
          <td>Monto</td>
          <td>Nro de Becarios</td>
          <td>Inscritos</td>
        </tr>
        <?php foreach ($sinceracionbecas2 as $sinceracionbeca2) {
          echo "<tr>";
          echo "<td>".$sinceracionbeca2["TipoBeca"]."</td>";          
          echo "<td>".$sinceracionbeca1["Monto"]."</td>";
          echo "<td>".$sinceracionbeca2["NroCupos"]."</td>";
          echo "<td>".$sinceracionbeca2["Inscritos"]."</td>";
          echo "</tr>";
         
        }
        ?>

        </tbody>
    </table>
      </div>

         <?php
          }
        ?>

  </div>
  </div>
  
</div>
<div class="modal-footer" id="modalpresupuestofooter">
<div class="row">
<div class="col-xs-12 col-md-12 col-xl-12" align="right">
    <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
  </div>
</div>
</div>

<script type="text/javascript">
  $('#modalpresupuestotitulo').show();
   $('#modalpresupuestofooter').show();
</script>

