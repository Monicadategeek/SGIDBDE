<?php
	ob_start();
?>
<style type="text/css">

	table{
		width: 100%;
	}
	table td{
		padding: 5px;
		border: 1px solid #333;
	}

	table th{
		background: #e9e9e9;
		padding: 10px 5px 5px 5px;
		border: 1px solid #333;
	}
	h1, h2{
		width: 100%;
		text-align: center;
		text-transform: uppercase;
		font-size: 15px;
		margin-bottom: 0;
		margin-top: 0;
	}
	h2{
		margin-top: 30px;
		margin-bottom: 20px;
	}
	h4{
		margin-bottom: 0px;
		font-size: 14px;
	}
</style>

<page backtop="7mm" backottom="7mm" backleft="10mm" backright="10mm">
	<h2>Presupuesto Planificado</h2>
	<table cellspacing='0' width="100%">
	    <tr>
	      <th style="width:200px;">Id</th>
	      <th style="width:200px;">Comienzo</th>      
	      <th style="width:200px;">Duración</th>
	    </tr>
	    <tr>
	      <td>
	        <?php echo $presupuesto1["IdPresupuesto"];?>
	      </td>
	        <?php 
	        $date= new DateTime($presupuesto1["Fecha_Comienzo"]);
	        echo "<td>".$date->format('d-m-Y ')."</td>";
	        ?>
	      <td>
	        <?php 
	        if ($presupuesto1["TiempoBeca"]==1) {
	            echo "1 mes";
	        }
	        else{
	          echo $presupuesto1["TiempoBeca"]." Meses";
	        }
	        ?>
	      </td>
	    </tr>
	     <tr>
	      <th colspan="3">Descripción</th>
	    </tr>
	    <tr>
	      <td colspan="3">
	        <?php echo $presupuesto1["Descripcion"];?>
	      </td>
	    </tr>
	    <tr>
	      <th>Fecha Registro</th>
	      <th>Ultima Modificación</th>
	      <th>Responable</th>
	    </tr>
	    <tr>
	      <td>
	        <?php
	        $date= new DateTime($presupuesto1["FechaRegistro"]);
	        echo $date->format('d F Y g:ia');
	        ?>  
	      </td>
	      <td>  
	        <?php
	        $date= new DateTime($responsable1["FechaRegistro"]);
	        echo $date->format('d F Y g:ia');
	        ?>  
	      </td>
	      <td>

	        <?php echo $responsable1["Responsable"];?>
	      </td> 
	    </tr>
  	</table> 

  <?php 

  if (isset($presupuesto2)) {?>
    <h2>Presupuesto Definitivo</h2>
  	<table cellspacing='0' width="100%">
	    <tr>
	      <th style="width:200px;">Id</th>
	      <th style="width:200px;">Comienzo</th>      
	      <th style="width:200px;">Duración</th>
	    </tr>
	    <tr>
	      <td>
	        <?php echo $presupuesto2["IdPresupuesto"];?>
	      </td>
	        <?php 
	        $date= new DateTime($presupuesto2["Fecha_Comienzo"]);
	        echo "<td>".$date->format('d-m-Y ')."</td>";
	        ?>
	      <td>
	        <?php 
	        if ($presupuesto2["TiempoBeca"]==1) {
	            echo "1 mes";
	        }
	        else{
	          echo $presupuesto2["TiempoBeca"]." Meses";
	        }
	        ?>
	      </td>
	    </tr>
	     <tr>
	      <th colspan="3">Descripción</th>
	    </tr>
	    <tr>
	      <td colspan="3">
	        <?php echo $presupuesto2["Descripcion"];?>
	      </td>
	    </tr>
	    <tr>
	      <th>Fecha Registro</th>
	      <th>Ultima Modificación</th>
	      <th>Responable</th>
	    </tr>
	    <tr>
	      <td>
	        <?php
	        $date= new DateTime($presupuesto1["FechaRegistro"]);
	        echo $date->format('d F Y g:ia');
	        ?>  
	      </td>
	      <td>  
	        <?php
	        $date= new DateTime($responsable2["FechaRegistro"]);
	        echo $date->format('d F Y g:ia');
	        ?> 
	      </td>
	      <td>
	        <?php echo $responsable2["Responsable"];?>
	      </td> 
	    </tr>
  	</table>
  <?php
  }
  ?>
  <h2>Desarrollo de las Becas</h2>
  <table cellspacing='0' style="margin-left:215px">
        <tr>  
          <th colspan="3" style="width:200px;">Presupuesto Planificado</th>
        </tr>
        
        <tr>
          <td style="width:100px;">Modalidad</td>
          <td style="width:50px;">Monto</td>
          <td style="width:50px;">Nro de Becarios</td>
        </tr>
        <?php foreach ($sinceracionbecas1 as $sinceracionbeca1) {
          echo "<tr>";
          echo "<td>".$sinceracionbeca1["TipoBeca"]."</td>";
          echo "<td>".number_format($sinceracionbeca1["Monto"], 2, '.', '')."</td>";
          echo "<td>".$sinceracionbeca1["NroCupos"]."</td>";
          echo "</tr>";
         
        }
        ?>
       </table>
       <br><br> 
 	<?php if (isset($presupuesto2)) {?>
 	<table cellspacing='0' style="margin-left:175px">
	        <tr>  
	          <th colspan="4" style="width:250px;">Presupuesto Definitivo</th>
	        </tr>
		        <tr>
		          <td style="width:100px;">Modalidad</td>
		          <td style="width:50px;">Monto</td>
		          <td style="width:50px;">Nro de Becarios</td>
		          <td style="width:50px;">Inscritos</td>
		        </tr>
		        <?php foreach ($sinceracionbecas2 as $sinceracionbeca2) {
		          echo "<tr>";
		          echo "<td>".$sinceracionbeca2["TipoBeca"]."</td>";
		          echo "<td>".number_format($sinceracionbeca2["Monto"], 2, '.', '')."</td>";
		          echo "<td>".$sinceracionbeca2["NroCupos"]."</td>";
		          echo "<td>".$sinceracionbeca2["Inscritos"]."</td>";
		          echo "</tr>";
		         
		        }
		        ?>
    	</table>

         <?php
          }
          else{
          ?>

        <?php  	
          }
        ?>


</page>

<?php

	$content = ob_get_clean();
	require_once('../web/html2pdf/html2pdf.class.php');
	$pdf = new HTML2PDF('P','LETTER','es','UTF-8');
	$pdf->writeHTML($content);
//	$pdf->pdf->IncludeJS('print(TRUE)');
	$pdf->output('imprimir.pdf');

?>
