<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Presupuesto
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Listado de los Presupuesto</h2>
            <div align="right">
            <?php 
                $nuevafecha = strtotime ( '+'.$MaxFecha_Comienzo["TiempoBeca"].' month' , strtotime ( $MaxFecha_Comienzo["Fecha_Comienzo"] ) ) ;
                $month = date('m', $nuevafecha);
                $year = date('Y', $nuevafecha);
                $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
                $nuevafecha= date('Y-m-d', mktime(0,0,0, $month, $day, $year));
                if ($nuevafecha<date('Y-m-d')) {?>
                    <button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/PresupuestoController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Presupuesto</button>
                <?php 
                }

            ?>
                
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Planificación</a>
                </li>
                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Presupuesto Definitivo</a>
                </li>
                
              </ul>
              <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                    <h4>Listado del Presupuesto Planificado por el Departametno de Estudios SocioEconómicos</h4>
                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="presupuesto1" > 
                        <thead> 
                            <th width="5%">Nro</th>
                            <th>Fecha Registro</th>
                            <th>Monto</th>
                            <th>Total Becarios</th>
                            <th>Duración</th>
                            <th>Comienzo</th>
                            <th>Acciones</th>

                        </thead>
                        <tfoot>                        
                            <td></td>
                            <th>Fecha Registro</th>
                            <th>Monto</th>
                            <th>Total Becarios</th>
                            <th>Duración</th>
                            <th>Comienzo</th>
                            <td></td>
                        </tfoot>
                        <tbody>
                            <?php 
                                if (is_array($presupuestos1) || is_object($presupuestos1))
                                {
                                    $cont=0;
                                    foreach ($presupuestos1 as $presupuesto1)
                                    {
                                        $cont=$cont+1;

                                        echo "<tr>                                        
                                        <td>".$cont."</td>
                                        <td>".$presupuesto1["FechaRegistro"]."</td>
                                        <td>".$presupuesto1["Monto"]."</td>
                                        <td>".$presupuesto1["NroTotalBecarios"]."</td>
                                        <td>".$presupuesto1["TiempoBeca"]."</td>
                                        <td>".$presupuesto1["Fecha_Comienzo"]."</td>";
                                        echo "<td>";

                                        if (is_null($presupuesto1["PresupuestoAsociado"])) {
                                            echo '<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/PresupuestoController.php',".$presupuesto1["IdPresupuesto"].');"><i class="fa fa-book"></i></button>';
                                           echo '<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/PresupuestoController.php',".$presupuesto1["IdPresupuesto"].');"><i class="fa fa-pencil"></i></button><button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/PresupuestoController.php',".$presupuesto1["IdPresupuesto"].', '."'del presupuesto planificado'".');"><i class="fa fa-trash-o"></i></button>';                                            
                                        }
                                        else {
                                            echo '<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/PresupuestoController.php',".$presupuesto1["PresupuestoAsociado"].');"><i class="fa fa-book"></i></button>';

                                        }

                                        echo "</td></tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                  <h4>Listado del Presupuesto Emitido por el Departamento de Presupuesto</h4>
                    <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="presupuesto2" > 
                        <thead> 
                            <th width="5%">Nro</th>
                            <th>Fecha Registro</th>
                            <th>Monto</th>
                            <th>Total Becarios</th>
                            <th>Duración</th>
                            <th>Comienzo</th>
                            <th>Acciones</th>

                        </thead>
                        <tfoot>
                            <td></td>
                            <th>Fecha Registro</th>
                            <th>Monto</th>
                            <th>Total Becarios</th>
                            <th>Duración</th>
                            <th>Comienzo</th>
                            <td></td>
                        </tfoot>
                        <tbody>
                            <?php 
                            $cont=0;
                                if (is_array($presupuestos2) || is_object($presupuestos2))
                                {
                                    
                                    foreach ($presupuestos2 as $presupuesto2)
                                    {
                                        $cont=$cont+1;
                                        echo "<tr>
                                        <td>".$cont."</td>
                                        <td>".$presupuesto2["FechaRegistro"]."</td>
                                        <td>".$presupuesto2["Monto"]."</td>
                                        <td>".$presupuesto2["TiempoBeca"]."</td>
                                        <td>".$presupuesto2["TiempoBeca"]."</td>
                                        <td>".$presupuesto2["Fecha_Comienzo"]."</td><td>";

                                        
                                        if ($presupuesto2["Fecha_Activa"]>date('Y-m-d')) {
                                            echo '<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/PresupuestoController.php',".$presupuesto2["IdPresupuesto"].');"><i class="fa fa-pencil"></i></button>';
                                        }
                                        

                                        echo '<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/PresupuestoController.php',".$presupuesto2["IdPresupuesto"].');"><i class="fa fa-book"></i></button>';

                                        echo "</td></tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>

        </div>
    </div>
</div>
           
        <!-- /.row -->

    <script type="text/javascript">
$(document).ready(function(){
$('#presupuesto1').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de presupuestos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#presupuesto1 tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#presupuesto1').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns(3).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });

     $('#presupuesto2').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de presupuestos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#presupuesto2 tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#presupuesto2').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns(3).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>