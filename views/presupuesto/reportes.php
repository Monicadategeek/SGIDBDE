

     <!-- Page Heading/Breadcrumbs -->

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Reportes Mensuales
            <small></small>
        </h1>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="col-md-12" align="center">
            <br>
            <input type="hidden" id="startDate" value="<?php echo $presupuesto?>">
            <div class="col-md-6 col-sm-12 col-xs-6">
            	<label>Seleccione la Fecha</label>
            	<input type="text" id="fecha" name="fecha" class="form-control datepicker" data-date-end-date='0d'>
            </div>
        </div>
        <div class="x_content" id="informacion">     
            <div id="reporte"></div>
            
        </div>
    </div>
</div>

<script type="text/javascript">
	$('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es',
        startDate: '-'+$('#startDate').val()+'d', 
        endDate:'1d',
    });	

    $('.selectpicker').selectpicker('refresh');

   $("#fecha").change(function () { 
		var  datos={"accion":'mostrar_reportes', "fecha":$('#fecha').val() };
		enviar('../Controller/PresupuestoController.php', datos, 'reporte');
		$('#reporte').show(); 	


	});


</script>

   
