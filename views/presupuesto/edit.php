<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de Presupuesto</h4>
</div>
<div class="modal-body row" id="formulario">
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
  <p align="justify"><b>Nota:</b> En este registro se procesan los presupuestos tanto planificados, como definitivos</p>
  
  </div>
  <form id="agregar" action="../Controller/PresupuestoController.php" method="POST">
  <input type="hidden" name="IdPresupuesto" id="IdPresupuesto" value="<?php echo $presupuesto1["IdPresupuesto"]?>">
  <input type="hidden" name="accion" id="accion" value="update">
    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <label class="control-label" for="TipoPresupuesto">Tipo Presupuesto</label>
      <select id="TipoPresupuesto" name="TipoPresupuesto" class="form-control" required="" title="-Seleccione-">
        <?php
          if ($presupuesto1["TipoPresupuesto"]==1) {
        ?>
            <option value="1" selected>Planificación</option>
            <option value="2">Definitivo</option>
         <?php
          }
          else{
            ?>
            <option value="1" >Planificación</option>
            <option value="2" selected>Definitivo</option>
         <?php
          }
        ?>
      </select>
    </div>

    <div class="form-group col-xs-12 col-md-12 col-xl-12" id="presupuestohidden">
      <p align="justify"><b>Nota:</b> Es importante que el presupuesto se enlace con un presupuesto Planificado.</p>
      <label class="control-label" for="PresupuestoAsociado">Enlazado Presupuesto</label>
      <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"> 
        <thead> 
            <th></th>
            <th>Fecha Registro</th>
            <th>Monto</th>
            <th>Total Becarios</th>
            <th>Duración</th>
            <th>Comienzo</th>

        </thead>
        <tfoot>
            <td></td>
            <th>Fecha Registro</th>
            <th>Monto</th>
            <th>Total Becarios</th>
            <th>Duración</th>
            <th>Comienzo</th>
            
        </tfoot>
        <tbody>
        <?php if (isset($presupuesto2)) {
          $date= new DateTime($presupuesto2["FechaRegistro"]);
                      
                      echo "<tr>
                      <td>".'<input type="radio" name="PresupuestoAsociado" value="'.$presupuesto2["IdPresupuesto"].'" checked="">'."</td>
                      <td>".$date->format('d-m-Y ')."</td>
                      <td>".number_format($presupuesto2["Monto"], 2, '.', '')."</td>
                      <td>".$presupuesto2["NroTotalBecarios"]."</td>";
                      if ($presupuesto2["TiempoBeca"]==1) {
                          $AsistenciEstud="1 mes";
                      }
                      else{
                          $AsistenciEstud=$presupuesto2["TiempoBeca"]." Meses";
                      }
                      $date= new DateTime($presupuesto2["Fecha_Comienzo"]);
                      echo "<td>".$AsistenciEstud."</td>";
                      echo "<td>".$date->format('d-m-Y ')."</td>";
                     
                      echo "</tr>";

            }
          ?>


          <?php 
              if (is_array($presupuestos) || is_object($presupuestos))
              {
                  
                  foreach ($presupuestos as $presupuesto)
                  {
                    if ($presupuesto1["IdPresupuesto"]!=$presupuesto["IdPresupuesto"]) {
                      $date= new DateTime($presupuesto["FechaRegistro"]);
                      
                      echo "<tr>
                      <td>".'<input type="radio" name="PresupuestoAsociado" value="'.$presupuesto["IdPresupuesto"].'">'."</td>
                      <td>".$date->format('d-m-Y ')."</td>
                      <td>".number_format($presupuesto["Monto"], 2, '.', '')."</td>
                      <td>".$presupuesto["NroTotalBecarios"]."</td>";
                      if ($presupuesto["TiempoBeca"]==1) {
                          $AsistenciEstud="1 mes";
                      }
                      else{
                          $AsistenciEstud=$presupuesto["TiempoBeca"]." Meses";
                      }
                      $date= new DateTime($presupuesto["Fecha_Comienzo"]);
                      echo "<td>".$AsistenciEstud."</td>";
                      echo "<td>".$date->format('d-m-Y ')."</td>";
                     
                      echo "</tr>";
                    }
                  }
              }
          ?>
      </tbody>
      </table>
    </div>

    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <label class="control-label" for="Descripcion">Descripción</label>
      <textarea class="form-control" id="Descripcion" name="Descripcion" maxlength="400"><?php echo $presupuesto1["Descripcion"]?></textarea>
    </div>


    <div class="col-xs-12 col-md-12 col-xl-12"> 
      <div class="form-group col-xs-4 col-md-4 col-xl-4">
      <input type="hidden" name="TiempoBeca" id="TiempoBeca" value="1">
        <label for="mesesselect">¿Cuantos meses cubrira la beca?</label>
        <select id="mesesselect" name="mesesselect" class="form-control">
          <?php 
            if ($presupuesto1["TiempoBeca"]==1) {
              echo '<option value="1" selected>Un Mes</option>
                <option value="3">Un Trimestre</option>
                <option value="6">Un Semestre</option>
                <option value="12">Un Año</option>
                <option value="99">Otro Tiempo</option> ';
            }
            elseif ($presupuesto1["TiempoBeca"]==3) {
              echo '<option value="1" >Un Mes</option>
                <option value="3" selected>Un Trimestre</option>
                <option value="6">Un Semestre</option>
                <option value="12">Un Año</option>
                <option value="99">Otro Tiempo</option> ';
            }
            elseif ($presupuesto1["TiempoBeca"]==6) {
              echo '<option value="1" >Un Mes</option>
                <option value="3" >Un Trimestre</option>
                <option value="6" selected>Un Semestre</option>
                <option value="12">Un Año</option>
                <option value="99">Otro Tiempo</option> ';
            }
            elseif ($presupuesto1["TiempoBeca"]==12) {
              echo '<option value="1" >Un Mes</option>
                <option value="3" >Un Trimestre</option>
                <option value="6">Un Semestre</option>
                <option value="12" selected>Un Año</option>
                <option value="99">Otro Tiempo</option> ';
            }
            else{
              echo '<option value="1" >Un Mes</option>
                <option value="3" >Un Trimestre</option>
                <option value="6">Un Semestre</option>
                <option value="12" >Un Año</option>
                <option value="99" selected>Otro Tiempo</option> ';
                $tipobeca=$presupuesto1["TiempoBeca"];
            }
          ?>
                   
        </select>
      </div>
      <div class="form-group col-xs-4 col-md-4 col-xl-4" id="meseshidden">
        <label for="nromeses">Ingrese el tiempo en meses</label>
        <input type="text" name="nromeses" id="nromeses" class="form-control nro" value="<?php
          if (isset($tipobeca)) {
            echo $tipobeca;
          }
         ?>">
      </div>

      <div class="form-group col-xs-4 col-md-4 col-xl-4">
        <label for="Fecha_Comienzo">Fecha que Comienza a Correr el Presupuesto</label>
        <input class="datepicker form-control " name="Fecha_Comienzo" id="Fecha_Comienzo" data-date-format="mm/dd/yyyy" data-date-start-date="0d" value="<?php $date= new DateTime($presupuesto1["Fecha_Comienzo"]);
        echo $date->format('d-m-Y')?>">
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12"> 
      <div class="form-group col-xs-4 col-md-4 col-xl-4">
        <label for="NroTotalBecarios">Nro Total de Becarios</label>
        <input type="text" name="NroTotalBecarios" id="NroTotalBecarios" disabled="true" class="form-control" value="<?php echo $presupuesto1["NroTotalBecarios"]?>">
      </div>
      <div class="form-group col-xs-4 col-md-4 col-xl-4">
        <label for="Monto">Presupuesto Total</label>
        <input type="text" name="Monto" id="Monto" disabled="true" class="form-control" value="<?php echo number_format($presupuesto1["Monto"], 2, '.', '');?>">
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12"> 
      <label>Desarrollo de las Becas</label>
      <br>
      <table class="table table-striped">
          <thead>
              <th>Modalidad</th>
              <th>Monto</th>
              <th>Nro de Becarios</th>
              <?php
              if ($presupuesto1["TipoPresupuesto"]==2) {
                echo "<th>Inscritos</th>";
              }
              ?>
          </thead>
          <tbody>
            <?php
              foreach ($sinceracionbecas1 as $tipobeca) {
                echo "<tr>";
                echo "<td>".$tipobeca["TipoBeca"]."</td>";
                echo "<td>".'<input class="valor form-control" type="text" name="mont'.$tipobeca["IdTipoBeca"].'" id="mont'.$tipobeca["IdTipoBeca"].'" value="'.number_format($tipobeca["Monto"], 2, '.', '').'">'."</td>";



                echo "<td>".'<input type="text" class="nrob form-control" name="NroCupos'.$tipobeca["IdTipoBeca"].'" id="NroCupos'.$tipobeca["IdTipoBeca"].'" value="'.$tipobeca["NroCupos"].'">'."</td>";
                if ($presupuesto1["TipoPresupuesto"]==2) {
                echo "<td>".'<input type="hidden" name="NroCuposHidden'.$tipobeca["IdTipoBeca"].'" id="NroCuposHidden'.$tipobeca["IdTipoBeca"].'" value="'.$tipobeca["Inscritos"].'">'.$tipobeca["Inscritos"]."</td>";
              }

                echo "</tr>";
                $id=$tipobeca["IdTipoBeca"];
              }
            ?>
          </tbody>
      </table>
    </div>

    


    <input type="hidden" name="total" id="total" value="<?php echo $id ?>">


    
    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">

  $(document).ready(function()
  {
    $("#Fecha_Comienzo").datepicker({
      language: 'es',
      format:'dd-mm-yyyy',
      startDate:'+0d',
       });
    $('.selectpicker').selectpicker('refresh');

    $('.valor').mask('000000.00', {reverse: true});
    $('.nrob').mask('000');
    $('.nro').mask('00');

      if ($('#TipoPresupuesto option:selected').val() == 1) {
        $('#presupuestohidden').hide();  

        
      }
      else{
        $('#presupuestohidden').show();
      }

      if ($('#mesesselect option:selected').val() == 99) {
          $('#meseshidden').show();
      }
      else{
        $('#meseshidden').hide();      
       
      }


    $("#TipoPresupuesto").change(function () { 
      if ($('#TipoPresupuesto option:selected').val() == 1) {
        $('#presupuestohidden').hide();      
        
      }
      else{
        $('#presupuestohidden').show();
      }
    });

    $("#mesesselect").change(function () { 
      if ($('#mesesselect option:selected').val() == 99) {
          $('#meseshidden').show();
      }
      else{
        $('#meseshidden').hide();      
        $('#nromeses').val("");
        $('#TiempoBeca').val($('#mesesselect option:selected').val());  
        calcular(); 
      }
    });

   $("#nromeses").change(function () {
    $('#TiempoBeca').val($('#nromeses').val());
    calcular();
   });

    $(".nrob").change(function () { 
      calcular();
      if ($('#TipoPresupuesto option:selected').val() == 2) {
        
          evaluar();
      }
    });

    $(".valor").change(function () { 
      calcular();

    });

    function evaluar(){
      for (var i = $('#total').val(); i >= 1; i--) {
        //alert($('#NroCupos'+i).val()+" < "+$('#NroCuposHidden'+i).val());
        if ($('#NroCupos'+i).val()<$('#NroCuposHidden'+i).val()) {

          document.getElementById("boton").disabled = true;
          notificacion(2, 'fa fa-times-circle','Error!','El Nro de Becarios no puede ser menor que el número de Inscritos');
          return false;

        }
      }
      document.getElementById("boton").disabled = false;

    }

    
    function calcular(){
      var total=0;
      var mult=0;
      var Becarios=0;
      for (var i = $('#total').val(); i >= 1; i--) {
        mult=($('#mont'+i).val()*$('#NroCupos'+i).val())*$('#TiempoBeca').val();
        total=total+mult;
        if (parseInt($('#NroCupos'+i).val())) {
          beca=parseInt($('#NroCupos'+i).val());
          Becarios=Becarios+beca;
        } 
      }
      if (total % 1 == 0) { //Consultar si el precio es un número entero ejm 120 => 120.00
          total=total+ '.00';
      }
      else if (dec = String(total).split(".")) { //Consultar si el precio es un número entero, con un decimal ejm 120.1 => 120.10
          if (dec.pop()==1) {
             total=total+ '.0';
          }
      }
      $('#NroTotalBecarios').val(Becarios);
      $('#Monto').val(total);
    }



  });
  $('#formulario').on('submit', '#agregar', function (e) {
    $('#Monto').removeAttr('disabled');
    $('#NroTotalBecarios').removeAttr('disabled');
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/PresupuestoController.php?accion=index');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado el presupuesto exitosamente');
            }
            else if (respuesta==2) {
              notificacion(2, 'fa fa-times-circle','Error!','Ya existe otro presupuesto con esas características');
            }
            else if (respuesta==3) {
              notificacion(2, 'fa fa-times-circle','Error!','Debe Seleccionar un Presupuesto para enlazar');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!', data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>