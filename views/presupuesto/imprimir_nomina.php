<?php
	ob_start();
?>
<style type="text/css">
	table{
		width: 100%;
	}
	table td{
		padding: 5px;
		border: 1px solid #333;
	}

	table th{
		padding: 10px 5px 5px 5px;
		border: 1px solid #333;
		text-align: center;
	}
	h1, h2{
		width: 100%;
		text-align: center;
		text-transform: uppercase;
		font-size: 15px;
		margin-bottom: 0;
		margin-top: 0;
	}
	h2{
		margin-top: 30px;
		margin-bottom: 5px;
	}
	h4{
		margin-bottom: 0px;
		font-size: 14px;
	}
</style>

<page backtop="7mm" backottom="7mm" backleft="10mm" backright="10mm">
<h1>Nomina de becarios correspondiente al mes de enero 2017</h1>
<?php
	$forma=0;
	$descripcion='';
	$estudiantes=0;
	$presupuesto=0;
	foreach ($sinceracionbecas as $sinceracion) {
	
		?>

		
			<?php
			echo "<h2>".$sinceracion["TipoBeca"]."</h2>";
			echo "<table cellspacing='0' width='100%'>
					<tr>
					<th style='width:10px;'>N°</th>
					<th style='width:260px;'>APELLIDOS Y NOMBRES</th>
					<th style='width:80px;'>EXPEDIENTE</th>
					<th style='width:130px;'>N° DE CTA. BANCARIA</th>
					<th style='width:80px;'>MONTO A PAGAR</th>
					</tr>";
					$con=0;
			foreach ($TiposBecas as $Becas) {
				
				foreach ($Becas as $Beca) {
					
					foreach ($Beca as $becario) {

						if ($becario["BecaAsociada"]==$sinceracion["IdSinceracionBecas"]) {
							if ($forma!=$becario["forma"]) {
								$forma=$becario["forma"];
								if ($becario["forma"]==1) {
								}
								else if ($becario["forma"]==2){
									$descripcion=$sinceracion["TipoBeca"]." PAGO POR CHEQHE";
									echo "<tr><td colspan='5' align='center'><b>".$descripcion."</b></td></tr>";
								}
								else{
									$descripcion=$sinceracion["TipoBeca"]."<b>PAGO POR CHEQHE Y AHORTA PASA A PAGO POR CUENTA</b> ";
									echo "<tr><td colspan='5' align='center'><b>".$descripcion."</b></td></tr>";
								}
								
							}

							$con=$con+1;
							echo '<tr>
								<td align="center" style="font-weight: bold;">'.$con.'</td>
								<td align="left">'.$becario["apellidos"].'</td>
								<td align="center">'.$becario["cedula"].'</td>
								<td align="left">'.$becario["tipopago"].'</td>
								<td align="right">'.number_format($sinceracion["Monto"], 2, ',', '.').'</td>
							</tr>';
						}
					}

					
				}

							
			}
			if ($con>0) {
				$estudiantes=$estudiantes+$con;
				$presupuesto=$presupuesto+($con*$sinceracion["Monto"]);	
				echo "<tr><td colspan='3' style='border:0px;'></td><td align='center'><b>SUBTOTAL</b></td><td bgcolor='#cccccc' align='right' >".number_format(($con*$sinceracion["Monto"]), 2, ',', '.')."</td></tr>";
			}
			/*if ($becario["forma"]!=1 and $becario["forma"]=!2) {
				echo "<tr><td colspan='3'></td><td>TOTAL</td><td  align='center'>".($con*$sinceracion["Monto"])."</td></tr>";
			}*/
		echo "</table>";
	}

	foreach ($sinceracionbecas as $sinceracion) {
		
		$con=0;
		foreach ($NvoIngresos as $Becas) {
			$val=$Becas;
			foreach ($Becas as $becario) {
				

				if ($becario["BecaAsociada"]==$sinceracion["IdSinceracionBecas"]) {
					$con=$con+1;
				}
			}			
		}

		$sinceracion["total"]=$con;
		$nuevasinceracionbecas[]=$sinceracion;
	}

	if (count($val)>0) {
		echo "<h2>NOMINA DE BECARION DE NUEVO INGRESO AL MES DE ".$mes." ".$anno."</h2>";
		foreach ($nuevasinceracionbecas as $sinceracion) {

		if ($sinceracion["total"]>0) {
			echo "<h2>".$sinceracion["TipoBeca"]."</h2>";
			echo "<table border='1'>
				<tr>
				<th>N°</th>
				<th>APELLIDOS Y NOMBRES</th>
				<th>EXPEDIENTE</th>
				<th>N° DE CTA. BANCARIA</th>
				<th >MONTO A PAGAR</th>
				</tr>";
				$con=0;
			foreach ($NvoIngresos as $Becas) {
				
				foreach ($Becas as $becario) {
					

						if ($becario["BecaAsociada"]==$sinceracion["IdSinceracionBecas"]) {
							$con=$con+1;
							echo '<tr>
								<td align="center">'.$con.'</td>
								<td align="left">'.$becario["apellidos"].'</td>
								<td align="center">'.$becario["cedula"].'</td>
								<td align="left">'.$becario["tipopago"].'</td>
								<td align="right">'.number_format($sinceracion["Monto"], 2, ',', '.').'</td>
							</tr>';
						}
				}

							
			}
			if ($con>0) {
				$estudiantes=$estudiantes+$con;

				$presupuesto=$presupuesto+($con*$sinceracion["Monto"]);
				echo "<tr><td colspan='3'></td><td align='center'><b>SUBTOTAL</b></td><td bgcolor='#cccccc' align='right' >".number_format(($con*$sinceracion["Monto"]), 2, ',', '.')."</td></tr>";
			}
			
			$ultimo=array_pop($nuevasinceracionbecas);
			if ($ultimo["IdTipoBeca"]==$sinceracion["IdTipoBeca"]) {
				echo "<tr><td colspan='3'></td><td align='center'><b>SUBTOTAL</b></td><td bgcolor='#cccccc' align='right' >".number_format($presupuesto, 2, ',', '.')."</td></tr>";
			}
			echo "</table>";
		}
			
	}



	}
	
	$consulta= new NumberToLetterConverter();

	$cifra=strtoupper($consulta->to_word($presupuesto));
	echo "<h2 align='center'>TOTAL GENERAL DEL MES DE ".$mes."  ".$anno.": ".$cifra." (Bs. ".number_format($presupuesto, 2, ',', '.').") EN ".$estudiantes." BECARIOS</h2>";

	echo "<h4 align='left'><b>NOTA:</b> Elaborado por Lic. ".$usuario["nombre"]." ".$usuario["apellido"]."</h4>";
	echo "<h4 align='left'><b>Firma:</b></h4>";
?>

</page>

<?php

	$content = ob_get_clean();
	require_once('../web/html2pdf/html2pdf.class.php');
	$pdf = new HTML2PDF('P','LETTER','es','UTF-8');
	$pdf->writeHTML($content);
//	$pdf->pdf->IncludeJS('print(TRUE)');
	$pdf->output('imprimir.pdf');

?>