<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Solicitud de Citas
            <small></small>
        </h1>
    </div>
</div>
	<div class="col-xs-12 col-md-12 col-xl-12">
	<div class="x_panel">
        <div class="x_title">
            <h2>Nueva Cita</h2>
            <div class="clearfix"></div>
        </div>
     	<div class="x_content">
		<div class="form-group col-xs-6 col-md-6 col-xl-6">
			<label class="control-label" for="departamento">Seleccione la consulta</label>
			<select name="departamento" id="departamento" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
				<?php
				if (is_array($departamentos) || is_object($departamentos))
				{
					foreach ($departamentos as $departamento)
					{
						echo "<option value='".$departamento["IdDepartamento"]."'>".$departamento["Descripcion"]."</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-xs-6 col-md-6 col-xl-6" id="divsede">
			
		</div>
	</div>

	<div class="col-xs-12 col-md-12 col-xl-12">
		<div class="form-group col-xs-6 col-md-6 col-xl-6" id="divdia">
		</div>

		<div class="form-group col-xs-6 col-md-6 col-xl-6" id="divhora">
		</div>
	</div>

	<div class="col-xs-12 col-md-12 col-xl-12" id="completado">
	</div>
</div>
</div>

	
<script type="text/javascript">
	
	$(document).ready(function()
  	{
  		$('.selectpicker').selectpicker('refresh');
  		$("#departamento").change(function () { 
  			var  datos={"accion":'create', "nivel":1, "departamento":$('#departamento option:selected').val() };
  			enviar('../Controller/CitasController.php', datos, 'divsede');
        	$('#divsede').show(); 
        	$('#divdia').hide(); 
        	$('#divhora').hide(); 
        	$('#completado').hide();	


    	});

    	


  	});


	

	
</script>