<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de la Cita</h4>
</div>
<div class="modal-body row" id="formulario">
  <h3>Datos Actuales</h3> 
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <input type="hidden" id="IdCitas" name="IdCitas" value="<?php echo $cita["IdCitas"] ?>">
    <table class="table table-bordered table-striped" border="1">
      <tr>
        <th>Servicio</th>
        <th>Sede</th>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Opción</th>
      </tr>
        <?php

        echo "<td>".$cita["TipoDepto"]."</td>
              <td>".$cita["Sede"]."</td>
              <td>".date('d-m-Y', strtotime($cita["Fecha"]))."</td>";
              echo "<td>".date('g:ia', strtotime($cita["Hora"]))."</td>";
              ?>
              <td align="center"><button class="btn btn-sm btn-danger" title="Cancelar Cita" onclick="cuadrocancelarcita('../Controller/CitasController.php', <?php echo $cita["IdCitas"] ?>);"><i class="fa fa-ban" aria-hidden="true"></i> </button></td>
      </tr>
    </table>
  </div>
  
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-6 col-md-6 col-xl-6" id="divsede">
      <?php

      if (is_array($sedes) && count($sedes)>0)
      {
      ?>  
        <label class="control-label" for="sede">Seleccione la Sede</label>
        <select name="sede" id="sede" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
          <?php
          foreach ($sedes as $sede)
          {
            if ($citas_update["Sede"]==$sede["IdDepartamentos"]) {
              echo "<option value='".$sede["IdDepartamentos"]."' seleted>".$sede["Descripcion"]."</option>";
            }
            else{
              echo "<option value='".$sede["IdDepartamentos"]."'>".$sede["Descripcion"]."</option>";
            }
          
          }
          ?>
        </select>
        <?php
        }
        else
        { ?>

        <p align="justify"><b>Disculpe,</b> Actualmente no se encuentra disponible ninguna Sede para el Servicio</p>
        <?php
        }
        ?>
      </div>
    <div class="form-group col-xs-6 col-md-6 col-xl-6" id="divdia">
    </div>
  </div> 
<br><br><br><br>
  <div class="form-group col-xs-12 col-md-12 col-xl-12" id="divhora">
  </div>


</div>
<div class="modal-footer">
   <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
</div>


<script type="text/javascript">
  $(document).ready(function()
    {
      $('.selectpicker').selectpicker('refresh');
      $("#sede").change(function () {
        var  datos={"accion":'edit', "nivel":1, "departamento":$('#sede option:selected').val(), "id":$('#IdCitas').val() };
        enviar('../Controller/CitasController.php', datos, 'divdia');
          $('#divdia').show();            
          $('#divhora').hide(); 
      });


    });


  
</script>