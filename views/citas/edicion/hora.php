<label>Hora</label>
<?php
	if (is_array($horas) && count($horas)>0)
	{
		?>

		<table class="table table-bordered table-striped">
			<thead>
				<th>Hora</th>
				<th>Día <?php echo $dia;?></th>
			</thead>

			<tbody>

		<?php

		foreach ($horas as $hora)
		{
			$date= new DateTime($hora["HorarioInic"]);
			$HorarioInic=$date->format('g:ia');
			$date= new DateTime($hora["HorarioFinal"]);
			$HorarioFinal=$date->format('g:ia');
			echo "<tr><td colspan='2'><center>".$hora["TipoProfesional"]." ".$hora["apellido"].", ".substr($hora["nombre"], 0, 1)."</center></td></tr>";
			while (true) {

				echo "<tr>
				<td>".$HorarioInic."-".date('g:ia', strtotime ( '+45 minute' , strtotime ( $HorarioInic )))."</td><td>";
				if (is_array($citas) && count($citas)>0)
				{

					foreach ($citas as $cita) {

						if ($cita["Hora"]==$HorarioInic) {
							echo "ocupado";	
							break;
						}
						else{
							echo '<input type="radio" name="Hora" value="'.date('H:m', strtotime ( $HorarioInic )).'"> Elegir<br>';
						}
					}
				}
				else{
					
					
					echo '<input type="radio" name="Hora" value="'.date('H:m', strtotime ( $HorarioInic )).'"> Elegir<br>';
					
				}
				$HorarioInic=date('g:ia',strtotime ( '+50 minute' , strtotime ( $HorarioInic )));
				echo "</td></tr>";
				if (date('H:m', strtotime ( $HorarioInic ))<date('H:m', strtotime ( $HorarioFinal ))) {
					
				}
				else{
					break;
				}

			}
			
			
			
		}

		?>
		</tbody>
	</table>
	<div align="center" id="divboton"><button id="boton" class="btn btn-primary btn-large" type=button >Guardar</button></div>
	
		<?php
	
	}
	else{
		echo '<p align="justify"><b>Disculpe,</b> Actualmente no se encuentra disponible ninguna Hora para este día</p>';
	}
?>

<script type="text/javascript">
	
	$(document).ready(function()
	{
		$("#boton").click(function () {	 

        		
			$('#divboton').hide();
			if (!$('input:radio[name=Hora]:checked').val()) {
				alert("Debes seleccionar la Hora");
				return false;
			}

			var  datos={"accion":'update', "departamento":$('#sede option:selected').val(), 'fecha':$('#fecha').val(), "hora":$('input:radio[name=Hora]:checked').val(), "id":$('#IdCitas').val()};

			$.ajax({
	            url: '../Controller/CitasController.php',
	            contentType: "charset=utf-8", 
	            data:datos,
	            type : 'GET',
	            success:function(data){
	            	respuesta = parseInt(data);
		            if (respuesta==1) {
		              redireccionar('../Controller/CitasController.php?accion=index');
		              $('#modificar').modal('hide');
		              notificacion(3,'fa fa-check','Completado!','Se ha modificado la cita');
		            }
		            else{
		              notificacion(2, 'fa fa-times-circle','Error!','Ha ocurrido un inconveniente durante el proceso de registro, intentelo mas tarde');
		            }
	            
	               
	            }
        	});
        	
        	$('#divboton').show();		

		});
			
	});



	

</script>