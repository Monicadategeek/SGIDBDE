<?php

	if (is_array($sedes) && count($sedes)>0)
	{
		?>	
		<label class="control-label" for="sede">Seleccione la Sede</label>
		<select name="sede" id="sede" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">

			<?php

			foreach ($sedes as $sede)
			{
				echo "<option value='".$sede["IdDepartamentos"]."'>".$sede["Descripcion"]."</option>";
			}
		?>
		</select>
		<?php
	}
	else
	{ ?>

		<p align="justify"><b>Disculpe,</b> Actualmente no se encuentra disponible ninguna Sede para el Servicio</p>
	<?php
	}
	?>

<script type="text/javascript">
$(document).ready(function()
  	{
  		$('.selectpicker').selectpicker('refresh');
    	$("#sede").change(function () {
  			var  datos={"accion":'create', "nivel":2, "departamento":$('#sede option:selected').val() };
  			enviar('../Controller/CitasController.php', datos, 'divdia');
        	$('#divdia').show();            
        	$('#divhora').hide(); 
        	$('#completado').hide();	
    	});


  	});
	
</script>