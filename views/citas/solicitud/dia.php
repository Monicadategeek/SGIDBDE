<?php
	$diasem="";
	$diasinac="";
	
	if (is_array($dias) && count($dias)>0)
	{
		if (is_array($citas) && count($citas)>0)
		{
		?>	
			<?php
			$dia="";
			foreach ($citas as $citas)
			{
				if ($citas["Fecha_Inic"]==$citas["Fecha_Fin"]) {
					$dia=$citas["Fecha_Inic"];
					$diasinac=$diasinac.date("m-d-Y", strtotime($citas["Fecha_Inic"])).",";
				}
				elseif ($dia!=$citas["Fecha_Inic"]) {

					$dia=$citas["Fecha_Inic"];
					$diasinac=$diasinac.date("m-d-Y", strtotime($citas["Fecha_Inic"])).',';
					
					 $nrodias=(strtotime($citas["Fecha_Fin"])-strtotime($citas["Fecha_Inic"]))/86400;


                    if ($nrodias==0) {
                       $nrodias=1;
                    }
                            
					for ($i=1; $i <= $nrodias; $i++) { 
						$dia = strtotime($dia." + 1 days");
						$dia = date("Y-m-d",$dia);

						$diasinac=$diasinac.date("m-d-Y",strtotime($dia)).',';
					}

				}
			}

				if ($citas) {
				
				$diasinac = substr($diasinac, 0, -1);
			}
			 
		?>
			
			
		<?php
		}
		$dia="";

		if ($departamentos["informacion"]!='') {
			echo '<p><b>Información:</b> '.$departamentos["informacion"].'</p>';
		}
		?>	
		
			<label for="fecha">Seleccione el Día</label>
			<?php
			$id=true;
			for ($i=0; $i < 7 ; $i++) { 
				foreach ($dias as $dia)
				{
					if ($i==$dia["Dia"]) {
						$id=false;
						break;		
					}
					$id=true;
				}
				if ($id) {
					$diasem=$diasem.$i.",";
				}
			}
			if ($dias) {
				
				$diasem = substr($diasem, 0, -1);
			}
			 
		?>
			<input type="hidden" id="diasinac" value='<?php echo $diasinac;?>'>
			<input type="hidden" id="dia" value='<?php echo $diasem;?>'>
			<input type="text" id="fecha" name="fecha" class="form-control">
		<?php
	}
	else
	{ ?>

		<p align="justify"><b>Disculpe,</b> Actualmente no se encuentra disponible Solicitar cita para esta Sede</p>
	<?php
	}
	?>

<script type="text/javascript">
stringdays= $("#diasinac").val();
var disableddates= stringdays.split(",");
console.log(disableddates);



 /** Days to be disabled as an array */
//var disableddates = ["10-11-2017", "10-12-2017", "10-13-2017", "10-14-2017"];

function DisableSpecificDates(date) {
 
 var m = date.getMonth();
 var d = date.getDate();
 var y = date.getFullYear();
 
 // First convert the date in to the mm-dd-yyyy format 
 // Take note that we will increment the month count by 1 
 var currentdate = (m + 1) + '-' + d + '-' + y ;
 
  // We will now check if the date belongs to disableddates array 
 for (var i = 0; i < disableddates.length; i++) {
 
 // Now check if the current date is in disabled dates array. 
 if ($.inArray(currentdate, disableddates) != -1 ) {
 return false;
 }
 }
 
}
	$(function () {
		$("#fecha").datepicker({
			language: 'es',
			startDate:'+0d',
			format:'dd-mm-yyyy',
			beforeShowDay: DisableSpecificDates,
			daysOfWeekDisabled: $("#dia").val(),
			singleDatePicker: true,
			  singleClasses: "picker_4"
			}, function(start, end, label) {
			  console.log(start.toISOString(), end.toISOString(), label);
			});
	});

	$(document).ready(function()
  	{
    	$("#fecha").change(function () {
    		
  			var  datos={"accion":'create', "nivel":3, "departamento":$('#sede option:selected').val(), 'fecha':$('#fecha').val()};
  			enviar('../Controller/CitasController.php', datos, 'divhora');
        	$('#divhora').show();   
        	$('#completado').hide();	         
    	});


  	});

</script>


