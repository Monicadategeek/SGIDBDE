
     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Solicitud de Citas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title"> 
            <h2>Citas Registradas</h2>
            <div align="right">
                <button type="button" class="btn btn-default btn-sm"  onclick="redireccionar('../Controller/CitasController.php?accion=history')" ><i class="fa fa-list" aria-hidden="true"></i> Historial de citas</button>

                <button type="button" class="btn btn-primary btn-sm"  onclick="redireccionar('../Controller/CitasController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva Cita</button>
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            
        
            <table class="table table-striped table-bordered dt-responsive nowrap table-hover" cellspacing="0" width="100%" id="citasestudiante" > 
                <thead> 
                    <th>Servicio</th>
                    <th>Sede</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Estado</th>
                    <th>¿Completar Formulario?</th>
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Servicio</th>
                    <th>Sede</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Estado</th>                    
                    <th>¿Completar Formulario?</th>
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($citas) || is_object($citas))
                        {
                            
                            foreach ($citas as $citas)
                            {

                                echo "<tr>
                                <td>".$citas["TipoDepto"]."</td>
                                <td>".$citas["Sede"]."</td>
                                <td>".$citas["Fecha"]."</td>
                                <td>".$citas["Hora"]."</td>
                                <td>".$citas["TipoAsistencia"]."</td>
                                <td>".$citas["formulario"]."</td>
                                <td>";
                                if ($citas["Asistencia"]==1) {
                                    echo '
                                    <button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/CitasController.php',".$citas["IdCitas"].');"><i class="fa fa-pencil"></i></button>
                                    ';
                                } 
                                echo "</td></tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="horariosModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg2" role="document">


    <div class="modal-content" style="padding: 20px;" id="horarioscontent">

      
    </div>
  </div>
</div>
           
        <!-- /.row -->

    <script type="text/javascript">


    $(document).ready(function(){

$('#citasestudiante').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Citas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#citasestudiante tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#citasestudiante').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([0,1,4,5]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>