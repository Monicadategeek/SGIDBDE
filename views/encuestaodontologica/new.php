
<!DOCTYPE html>
<html>
<head>
    <title>Encuesta Estudiantil</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='../web/images/logosolo.png' rel='shortcut icon' type='image/png'>

    <!-- Include Bootstrap CSS -->

    <link href="../web/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../web/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="../web/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../web/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../web/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <link href="../web/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../web/css/pnotify.custom.css" rel="stylesheet" />
    <link href="../web/css/animate.css" rel="stylesheet" />
    <link href="../web/css/bootstrap-datepicker.css" rel="stylesheet">  

    
    <!-- Optional Bootstrap theme -->
    <link href="../web/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />

    <!-- Include SmartWizard CSS -->
    <link href="../web/css/smart_wizard.css" rel="stylesheet" type="text/css" />
    
    <!-- Optional SmartWizard theme -->
    <link href="../web/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />
    <link href="../web/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="container">
<div class="row">
    <div class="col-xs-12 col-md-12 col-xl-12 row">
        
        <div class="col-xs-12 col-md-12 col-xl-12">
            <h2>Bienvenid@ <?php echo ucwords(strtolower($persona["Nombres"]))." ".ucwords(strtolower($persona["Apellidos"])) ?></h2>


            <div class="col-xs-12 col-md-12 col-xl-12" id="encuesta">
                <?php require_once(dirname(__FILE__) ."/encuesta.php"); ?>


            </div>
            
        </div>
</div>
</div>
    

     
    </div>   

    <script src="../web/js/jquery.min.js"></script>
    <!-- Include jQuery Validator plugin -->
    <script src="../web/js/validator.min.js"></script>
    <script src="../web/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../web/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script src="../web/js/jquery.mask.min.js" type="text/javascript"></script>

    <script src="../web/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../web/vendors/nprogress/nprogress.js"></script>
    <script src="../web/js/pnotify.custom.js" type="text/javascript"></script>
    <!-- bootstrap-progressbar -->
    <script src="../web/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../web/vendors/iCheck/icheck.min.js"></script>

    

    <!-- Include SmartWizard JavaScript source -->
    <script type="text/javascript" src="../web/js/jquery.smartWizard.min.js"></script>
    

    <script src="../web/js/bootstrap-datepicker.js" type="text/javascript"></script>

     <script src="../web/js/bootstrap-datepicker.es.js" type="text/javascript"></script>
     <script type="text/javascript">
     $.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mie", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "mm-dd-yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
  };
   </script>
   <script src="../web/js/main2.js"></script>
    <script type="text/javascript">
        
        function encuesta(){
            $.ajax({
                url: '../Controller/EncuestaController.php',
                data:{"accion":'encuestar'},                
                type : 'GET',
                contentType: "charset=utf-8", 
                success:function(data){

                   $("#encuesta").html(data);
                }
            })
        }
    </script>

    <script type="text/javascript">
    $(document).ready(function(){
        
      $('.selectpicker').selectpicker('refresh');
     
    });



    $('#formulario-odontologico').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){
              $("#encuesta").html(data);
            }
        })
        document.getElementById("boton").disabled = false;
    }) 

</script>
</body>
</html>






