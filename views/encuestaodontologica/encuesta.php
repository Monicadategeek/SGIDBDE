<h2><img src="../web/images/diente.svg" style="width: 30px; height: 30px;"> Formulario Odontológico</h2>
<hr>
<div id="formulario">
    <form id="formulario-odontologico" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaOdontologicaController.php">
        <input type="hidden" name="accion" id="accion" value="store">
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
        

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-12 col-xl-12">
                <label for="AntFami">Antecedentes Familiares</label>
                <input type="text" class="form-control" id="AntFami" name="AntFami" title="Antecedentes Familiares">
            </div>
            <div class="form-group col-xs-12 col-md-12 col-xl-12">
                <label for="MotCons">Motivo de la Consulta</label>
                <input type="text" class="form-control" id="MotCons" name="MotCons" title="Ingrese el Motivo de la Consulta">
            </div>
        </div>
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="Dolor">Dolor</label>
                <input type="text" class="form-control" id="Dolor" name="Dolor" title="Ingrese si tiene Dolor">
            </div>
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="Control">Control</label>
                <input type="text" class="form-control" id="Control" name="Control" title="Ingrese si tiene algun Tratamiento">
            </div>
        </div>

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="SangEnc">Sangramiento de las encias</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="SangEnc"  value="1" required>Si
                    </label>
                    <label>
                        <input type="radio" name="SangEnc"  value="0" required>No
                    </label>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="RestProt">Restauración Prostética</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="RestProt"  value="1" required>Si
                    </label>
                    <label>
                        <input type="radio" name="RestProt"  value="0" required>No
                    </label>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="Otro">Otro indique cual: </label>
                <input type="text" class="form-control" id="Otro" name="Otro">
            </div>
        </div>
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-12 col-xl-12">
                <label for="EnferAct">Enfermedad Actual</label>
                 <input type="text" id="EnferAct" name="EnferAct" class="form-control" title="Ingrese la Enfermedad Actual">
            </div>
        </div>

        <div class="col-xs-12 col-md-12 col-xl-12">
     
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
            	<table class="table table-bordered table-hover">
            		<tbody>
                    <?php
                        foreach ($preguntas as $pregunta) {
                            if ($pregunta["id"]<=$mitad) {
                                echo "<tr>
                                        <td>".$pregunta["Descripcion"]."</td> <td>";

                                if ($pregunta["tiporesp"]==1) {
                                    echo '<div class="radio">
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="1" required>Si
                                                </label>
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="0" required checked="">No
                                                </label>
                                            </div>';
                                }
                                else if ($pregunta["tiporesp"]==2) {
                                     echo '<div class="radio">
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="1" required>Si
                                                </label>
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="0" required checked="">No
                                                </label>
                                                <label>
                                                    <input type="text" class="form-control" name="resp'.$pregunta["id"].'">

                                                </label>
                                            </div>';
                                }

                                
                                echo "</td></tr>";
                            }
                        }
                    ?>
            			
            		</tbody>
            	</table>
            </div>

            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <table class="table table-bordered table-hover">
                    <tbody>
                    <?php
                        foreach ($preguntas as $pregunta) {
                            if ($pregunta["id"]>$mitad) {
                                echo "<tr>
                                        <td>".$pregunta["Descripcion"]."</td> <td>";

                                if ($pregunta["tiporesp"]==1) {
                                    echo '<div class="radio">
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="1" required>Si
                                                </label>
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="0" required checked="">No
                                                </label>
                                            </div>';
                                }
                                else if ($pregunta["tiporesp"]==2) {
                                     echo '<div class="radio">
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="1" required>Si

                                                </label>
                                                <label>
                                                    <input type="radio" name="preg'.$pregunta["id"].'"  value="0" required checked="">No
                                                </label>
                                                <label>

                                                    <input type="text" class="form-control" name="resp'.$pregunta["id"].'">

                                                </label>
                                            </div>';
                                }

                                
                                echo "</td></tr>";
                            }
                        }
                    ?>
                        
                    </tbody>
                </table>
            </div>
        </div>



                        
           

        
        <button type="submit" id="boton" class="btn btn-success btn-sm">Guardar</button>         
    </form>
</div>
