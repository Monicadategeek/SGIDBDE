<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Reinscripción de becarios</h4>
</div>
<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/InscripcionesController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="store">
  <div class="col-xs-12 col-md-12 col-xl-12">

  <label>Datos del Estudiante</label>

  <div class=" col-md-3 col-xl-12">
  <?php 
    if ($estudiante["foto"]!=0) {
        echo '<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$estudiante["foto"].'" alt="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'" title="'.$estudiante["nombres"]." ".$estudiante["apellidos"].'">';
    }
    else{
        echo '<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
    } ?>
        
  </div>
  <div class=" col-md-9 col-xl-12">
  <table class="table table-striped table-bordered">
    <tr>
      <th>Nombres</th>
      <th>Apellidos</th>
      <th>Carrera</th>
      <th>Tray</th>      
      <th>Trim</th>
      <th>Sección</th>      
      <th>Turno</th>
    </tr>
      <td><?php echo ucwords(strtolower($estudiante["nombres"]))?></td>
      <td><?php echo ucwords(strtolower($estudiante["apellidos"]))?></td>
      <td><?php echo $estudiante["cod_carrera"] ?></td>
      <td><?php echo $estudiante["trayecto"] ?></td>
      <td><?php echo $estudiante["trimestre"] ?></td>
      <td><?php echo $estudiante["seccion"] ?></td>
      <td><?php echo $estudiante["turno"] ?></td>
      
    </tr>
  </table>
   <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nro Becario</th>
          <th>Cuenta Bancaria</th>
          <th title="Fecha en que se registro la cuenta Bancaria">Fecha de Registro</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td><?php echo $becario["IdBecarios"] ?></td>
            <?php if ($becario["CtaBancaria"]!='') {
                        echo "<td>".$becario["CtaBancaria"]."</td><td>".$becario["FechaCtaBanca"]."</td>";
                      }
                      else{
                        echo "<td colspan='2'>Cobre por Cheque</td>";
                      }
              ?>
        </tr>
      </tbody>
    </table>
    <table class="data table table-striped">
      <thead>
        <tr>
          <th>Nro Inscripción</th>
          <th>Tipo_Inscripcion</th>
          <th>Fecha Registro</th>
          <th>Modalidad</th>
          <th title="Responsable del registro">Responsable</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php                  
            echo "
            <td>".$historial["IdInscripciones"]."</td>
            <td>".$historial["Tipo_Inscripcion"]."</td>
            <td>".$historial["FechaRegistro"]."</td>
            <td>".$historial["TipoBeca"]."</td>
            <td>".$historial["Responsable"]."</td><td> ";
            echo "</td></tr>";
                                     
      ?>
    </tbody>
    </table>

  </div>
  </div>
  <div class="col-xs-12 col-md-12 col-xl-12" align="center">
  <input type="hidden" name="becario" value="<?php echo $becario["IdBecarios"] ?>">
    <div class="form-group  col-md-6 col-xs-12">
      <label class="control-label" for="BecaAsociada">Modalidad de la beca</label>
      <select id="BecaAsociada" name="BecaAsociada" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
      foreach ($becasdisp as $becadisp) {
        if (($becadisp["NroCupos"]-$becadisp["Inscritos"])>0) {
           echo '<option value="'.$becadisp["IdSinceracionBecas"].'">'.$becadisp["TipoBeca"].'</option>';
        }       
       }
      ?>
      </select>
    </div>
    </div>


    <div class="col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
    });

  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/InscripcionesController.php');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado la inscripción del becario');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>