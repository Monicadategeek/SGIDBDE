
     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Control de Becas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Inscripción de Becarios</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            
        <p>Listado de los becarios del presupuesto anterior, estos pueden ser inscritos inmediatamente al nuevo presupuesto. Los otros becarios que no aparezcan deberan tomar cita y hacer la prueba socioeconómica</p>
        <table class="table table-striped table-bordered"><tr><th>Cupos Disponibles</th>
        <?php
            foreach ($becasdisp as $becadisp) {

                echo "  <td>".$becadisp["TipoBeca"]."</td>
                        <td><b>".($becadisp["NroCupos"]-$becadisp["Inscritos"])."</b></td>
                    ";
            }
        ?>
            </tr>
        </table>
        <h3>Listado de Becarios del Presupuesto Anterior</h3>
        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="becarioestudiante"> 
            <thead> 
                <th>Cédula</th>
                <th>Apellidos</th>
                <th>Nombres</th>
                <th>Sexo</th>
                <th>PNF</th>
                <th>Acciones</th>
            </thead>
            <tfoot>
                <th>Cédula</th>
                <th>Apellidos</th>
                <th>Nombres</th>
                <th>Sexo</th>
                <th>PNF</th>
                <td></td>
            </tfoot>
            <tbody >
                <?php 
                    if (is_array($becarios) || is_object($becarios))
                    {
                        
                        foreach ($becarios as $becario)
                        {

                            echo "<tr>
                            <td>".$becario["cedula"]."</td>
                            <td>".ucwords(strtolower($becario["nombres"]))."</td>
                            <td>".ucwords(strtolower($becario["apellidos"]))."</td>
                            <td>".$becario["sexo"]."</td>                                
                            <td>".$becario["cod_carrera"]."</td>";
                            echo '<td>';    
                            echo '<button class="btn btn-success btn-xs" title="Agregar" onclick="nuevo('."'../Controller/InscripcionesController.php?accion=create&becario=".$becario["Becario"]."'".');"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>  ';

                            echo "</td></tr>";
                        }
                    }
                ?>
            </tbody >
        </table>
        </div>
    </div>
</div>
         
        <!-- /.row -->

    <script type="text/javascript">
    $(document).ready(function(){

$('#becarioestudiante').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Citas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        
        responsive: true,
    });
// Setup - add a text input to each footer cell
    $('#becarioestudiante tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#becarioestudiante').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([3,4]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>