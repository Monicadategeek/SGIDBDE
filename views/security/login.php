<?php 
if(isset($_SESSION['ID']))
   {
       session_destroy();

        
   }
   ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../../web/images/apple-icon.png">
    <link href='../../web/images/logosolo.png' rel='shortcut icon' type='image/png'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="../../web/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../web/css/pnotify.custom.css" rel="stylesheet" />
    <link href="../../web/css/animate.css" rel="stylesheet" />
    <link href="../../web/css/now-ui-kit.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../../web/css/demo.css" rel="stylesheet" />
</head>

<body class="login-page">

    <div class="page-header" filter-color="orange" style="margin:0;">
        <div class="page-header-image" style="background-image:url(../../web/images/fondoiutoms2.jpg)"></div>
        <div class="container" style="padding-top: 20px;">
            <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-3">
                    <div class="header header-primary text-center">
                        <div class="logo-container">
                            <img src="../../web/images/logosolo.png" alt="" style="width: 100%;">
                        </div>
                    </div>  
                </div>
                <div class="col-md-9">
                    <h3>Sistema de Gestión Integral de la División de Bienestar y Desarrollo Estudiantil</h3>
                </div>
            </div>
            </div>
            <div class="col-md-4 content-center" style="padding-top: 170px;">
                <div class="card card-login card-plain">
                    <form  id="formulario" class="form" action="#" method="POST" autocomplete="off">
                        
                        <div class="content">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="text" maxlength="8" title="Ingrese su número de cédula" placeholder="Cedula" class="form-control" id="username" name="username" autocomplete="off">
                            </div>
                            <div class="input-group form-group-no-border input-lg" style="margin-bottom:0;">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" maxlength="14" title="Ingrese su contraseña" placeholder="Contraseña" class="form-control" id="password" name="password" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="footer text-center">
                            <a href="#" class="btn btn-primary btn-round btn-lg btn-block" id="boton-inicio">Iniciar Sesión</a>
                        </div>
                        <div align="Center">
                            <h6>
                                <a href="../web/javascript:;" data-toggle="modal" data-target="#Forgot-Password" class="link">Olvidaste tu contraseña?</a>
                            </h6>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="http://iutoms.net" target="_blank">
                                IUTOMS
                            </a>
                        </li>
                        <li>
                            <a href="http://iutoms.net/siace/" target="_blank">
                                SIACE IUTOMS
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mppeuct.gob.ve/" target="_blank">
                                MPPEUCT
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script> IUTOMS todos los derechos reservados, Desarrollado por
                    <a href="#">Grupo Nº7</a>.
                </div>
            </div>
        </footer>
    </div>


      <!-- Modal Cambiar-Tema-->
   <div class="modal fade" id="Forgot-Password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> <h3 class="modal-title" id="myLargeModalLabel" style="text-align: center;">Recuperar Contraseña</h3> </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-8 col-md-offset-2" style="text-align: center;">
                <label >Ingrese el correo electronico asociado a su cuenta en el SIACE</label>
                <input type="text" name="mailforgot" id="mailforgot" class="form-control" placeholder="Correo Electronico" maxlength="100">
                <br>
            </div>
            </div>
            <div class="row">
            <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                <a href="javascript:void(0)" class="btn btn-primary btn-round btn-lg btn-block" id="boton-forgot">Enviar</a>
            </div>
            </div>
          </div>
        </div>

      </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../../web/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="../../web/js/core/tether.min.js" type="text/javascript"></script>
<script src="../../web/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../../web/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../../web/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="../../web/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="../../web/js/now-ui-kit.js" type="text/javascript"></script>
<script src="../../web/js/pnotify.custom.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {

    $("#username").val("");
    $("#password").val("");
});

</script>
<script src="../../web/js/jquery.mask.min.js"></script>
    <script src="../../web/js/md5.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#username').mask('00000000');
        });

        
    //contrasena=sha1(contrasena);
    //alert(contrasena);

    $( "#boton-inicio" ).click(function(e) {
        e.preventDefault();
        $( "#boton-inicio" ).addClass( "btn-loading" );
        $("#boton-inicio").html('<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>');
        
        usuario = document.getElementById('username').value;
        contrasena = document.getElementById('password').value;
        if (usuario == null || usuario.length < 7) {
            $( "#boton-inicio" ).removeClass( "btn-loading" );
            $( "#boton-inicio" ).addClass( "btn-error" );
            $("#boton-inicio").html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
            new PNotify({
                title: 'Error',
                text: 'No ha ingresado su número de cédula.',
                addclass: 'errormsg',
                icon: 'fa fa-times-circle',
                mouse_reset: false,
                nonblock: {
                    nonblock: true
                },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
            });
            setTimeout(function(){ 
                $( "#boton-inicio" ).removeClass( "btn-error" );
                $("#boton-inicio").html('Iniciar Sesion');
             }, 3000);
            $('#username').focus();
            return false;
        }
        if (contrasena == 0 || contrasena.length < 5) {
            $( "#boton-inicio" ).removeClass( "btn-loading" );
            $( "#boton-inicio" ).addClass( "btn-error" );
            $("#boton-inicio").html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
            new PNotify({
                title: 'Error',
                text: 'No ha ingresado la contraseña.',
                addclass: 'errormsg',
                icon: 'fa fa-times-circle',
                mouse_reset: false,
                nonblock: {
                    nonblock: true
                },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
            });
            setTimeout(function(){ 
                $( "#boton-inicio" ).removeClass( "btn-error" );
                $("#boton-inicio").html('Iniciar Sesion');
             }, 3000);
            $('#password').focus();
            return false;
        }
        contrasena=md5(contrasena);
        var  datos={"username":usuario, "password":contrasena};
        $.ajax({
            type: $('#formulario').attr('method'),
            url:'../../Controller/SecurityController.php?accion=conectar',
            data: datos,
            success:function(data){
                respuesta = parseInt(data);
                
                if (respuesta == 1) {
                    new PNotify({
                        title: 'Bienvenid@',
                        text: 'Sera redireccionado a la página principal.',
                        addclass: 'successmsg',
                        icon: 'fa fa-smile-o',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                    $( "#boton-inicio" ).removeClass( "btn-loading" );
                    $( "#boton-inicio" ).addClass( "btn-check" );
                    $("#boton-inicio").html('<i class="fa fa-check" aria-hidden="true"></i>');
                    setTimeout(function(){ 
                        location.href ="../principal.php";
                    }, 3000);
                    
                }
                else if (respuesta == 2) {
                    $( "#boton-inicio" ).removeClass( "btn-loading" );
                    $( "#boton-inicio" ).addClass( "btn-error" );
                    $("#boton-inicio").html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
                    new PNotify({
                        title: 'Error',
                        text: 'Su Usuario esta Bloqueado, contacte con su administrador de servicios.',
                        addclass: 'errormsg',
                        icon: 'fa fa-times-circle',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                    setTimeout(function(){ 
                        $( "#boton-inicio" ).removeClass( "btn-error" );
                        $("#boton-inicio").html('Iniciar Sesion');
                     }, 3000);
                }
                else if (respuesta == 3) {
                    $( "#boton-inicio" ).removeClass( "btn-loading" );
                    $( "#boton-inicio" ).addClass( "btn-error" );
                    $("#boton-inicio").html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
                    new PNotify({
                        title: 'Error',
                        text: 'La Contraseña que introdujo es erronea.',
                        addclass: 'errormsg',
                        icon: 'fa fa-times-circle',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                    setTimeout(function(){ 
                        $( "#boton-inicio" ).removeClass( "btn-error" );
                        $("#boton-inicio").html('Iniciar Sesion');
                     }, 3000);
                }
                else if (respuesta == 4) {
                    $( "#boton-inicio" ).removeClass( "btn-loading" );
                    $( "#boton-inicio" ).addClass( "btn-error" );
                    $("#boton-inicio").html('<i class="fa fa-times-circle" aria-hidden="true"></i>');
                    new PNotify({
                        title: 'Error',
                        text: 'El número de cédula que introdujo no esta registrado en el sistema.',
                        addclass: 'errormsg',
                        icon: 'fa fa-times-circle',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                    setTimeout(function(){ 
                        $( "#boton-inicio" ).removeClass( "btn-error" );
                        $("#boton-inicio").html('Iniciar Sesion');
                     }, 3000);
                }
            }
        })
        });






        $( "#boton-forgot" ).click(function(e) {
        e.preventDefault();
        
        mail = document.getElementById('mailforgot').value;
        if( mail != null || mail.length > 0) {
          if( !(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(mail)) ) {
            new PNotify({
                title: 'Error',
                text: 'No ha ingresado un direccion de correo electronico valida.',
                addclass: 'errormsg',
                icon: 'fa fa-times-circle',
                mouse_reset: false,
                nonblock: {
                    nonblock: true
                },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
            });
            $('#mailforgot').focus();
            return false;
          }
        }
        $.post({
            url:'../../Controller/SecurityController.php?accion=recuperar',
            data: {"mail":mail},
            success:function(data){
                respuesta = parseInt(data);
                
                if (respuesta == 1) {
                    new PNotify({
                        title: 'Completado',
                        text: 'Se le ha enviado un correo.',
                        addclass: 'successmsg',
                        icon: 'fa fa-check',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                    $('#Forgot-Password').modal('toggle');
                    
                }
                else {
                    new PNotify({
                        title: 'Error',
                        text: data,
                        addclass: 'errormsg',
                        icon: 'fa fa-times-circle',
                        mouse_reset: false,
                        nonblock: {
                            nonblock: true
                        },
                        animate: {
                            animate: true,
                            in_class: 'slideInDown',
                            out_class: 'slideOutUp'
                        }
                    });
                }
            }
        })
        });


    
$('#Forgot-Password').on('hidden.bs.modal', function (e) {
    $('#mailforgot').val('');
})
    


  
    

    </script>

</html>