<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de Departamento</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="editar" action="../Controller/DepartamentosController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="update">
    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <div class="col-xs-6 col-md-6 col-xl-6">
        <input type="hidden" name="IdDepartamentos" id="IdDepartamentos" value="<?php echo $Depto['IdDepartamentos'] ?>">
        <label class="control-label" for="tipodepto">Tipo de Departamento</label>
        <select id="tipodepto" name="tipodepto" class="form-control" required="" title="-Seleccione-">
        <?php 
        if (is_array($departamentos) || is_object($departamentos))
        {
            foreach ($departamentos as $departamento)
            {
                if ($Depto["TipoDepto"]==$departamento["IdDepartamento"]) {
                  echo "<option value='".$departamento["IdDepartamento"]."' selected>".$departamento["Descripcion"]."</option>";
                }
                else{
                  echo "<option value='".$departamento["IdDepartamento"]."'>".$departamento["Descripcion"]."</option>";
                }
                
            }
        }
        ?>
        </select>
      </div>
      <div class=" col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="sede">Sede</label>
        <select id="sede" name="sede" class="form-control" required="" title="-Seleccione-">
          <?php 
          if (is_array($sedes) || is_object($sedes))
          {
            foreach ($sedes as $sede)
            {
              if ($Depto["Sede"]==$sede["IdSede"]) {
                echo "<option value='".$sede["IdSede"]."' selected>".$sede["Descripcion"]."</option>";
              }
              else{
                echo "<option value='".$sede["IdSede"]."'>".$sede["Descripcion"]."</option>";
              } 
            }
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <div class="col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="AsistenciEstud">¿Presta Asistencia Directa al Estudiante?</label>
        <?php
         if ($Depto["AsistenciEstud"]==1): ?>
          <p>
            Si <input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="1" checked="" required /> 
            No<input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="0" />
          </p>
        <?php elseif ($Depto["AsistenciEstud"]==0): ?>
          <p>
            Si <input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="1"  required /> 
            No<input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="0" checked="" />
        </p>
        <?php endif ?> 
      </div>
      <div class="col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="AsistenciEstud"> Estatus </label>
        <?php if ($Depto["Estatus"]==1): ?>
          <p>
            Activo <input type="radio" class="flat" name="Estatus" id="Estatus" value="1" checked="" required /> 
            Inactivo<input type="radio" class="flat" name="Estatus" id="Estatus" value="0" />
          </p>
        <?php elseif ($Depto["Estatus"]==0): ?>
          <p>
            Activo <input type="radio" class="flat" name="Estatus" id="Estatus" value="1"  required /> 
            Inactivo<input type="radio" class="flat" name="Estatus" id="Estatus" value="0" checked="" />
        </p>
        <?php endif ?> 
      </div>

    </div>

    <div class="col-xs-12 col-md-12 col-xl-12" align="left">
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="AsistenciEstud">Cuadro de Información <small>Se muestra en la solicitud de citas para los estudiantes</small></label>
        <textarea id="informacion" name="informacion" class="form-control"><?php echo $Depto["informacion"]?></textarea>
      </div>
    </div>
    <div class="form-group col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">

  $('#formulario').on('submit', '#editar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;         
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/DepartamentosController.php?accion=index');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado el registro del departamento');
            }
            else{
              notificacion(2,'fa fa-times-circle','Error!','Ya existe otro departamento con esas características');
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>