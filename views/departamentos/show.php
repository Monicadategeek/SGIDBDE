<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Ver Registro del Departamento</h4>
</div>
<div class="modal-body row" >
  <div class="col-xs-12 col-md-12 col-xl-12" align="left">
    <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
      <tr>
        <th>Id</th>
        <th>Tipo de Departamento</th>
        <th>Sede</th>
        <th>¿Asiste al Estudiante?</th>
      </tr>
      <tr>
        <td>
          <?php echo $departamento["IdDepartamentos"];?>
        </td>
        <td>
          <?php echo $departamento["TipoDepto"];?>
        </td>
        <td>
          <?php echo $departamento["Sede"];?>
        </td>
        <td>
          <?php echo $departamento["AsistenciEstud"];?>
        </td>
      </tr>
      </table>
      <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
        <tr>
          <th>Información</th>
        </tr>
        <tr>
          <td align="center">
            <?php echo $departamento["informacion"];?>
          </td>
        </tr>

      </table>
      <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
      <tr>
        <th>Estatus</th>
        <th>Fecha de Registro</th>
        <th>Ultima Modificación</th>
        <th>Responsable</th>
        
      </tr>
      <tr>
        <td>
          <?php echo $departamento["Estatus"];?>
        </td>
        <td>
          <?php echo $departamento["FechaRegistro"]; ?>  
        </td>
        <td>  
          <?php echo $departamento["UltModif"]; ?> 
        </td>
        <td>

          <?php echo $departamento["Responsable"];?>
        </td>
        
      </tr>
    </table>
  </div>
</div>
<div class="modal-footer" >
  <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
</div>



