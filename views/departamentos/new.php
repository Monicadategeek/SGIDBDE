<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Registro de Departamento</h4>
</div>

<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/DepartamentosController.php" method="POST">
    <input type="hidden" name="accion" id="accion" value="store">
    <div class="col-xs-12 col-md-12 col-xl-12" align="left">
      <div class="form-group col-xs-12 col-md-4 col-xl-4">
        <label class="control-label" for="tipodepto">Tipo de Departamento</label>
        <select id="tipodepto" name="tipodepto" class="form-control selectpicker" title="-Seleccione-" required>
          <?php 
          if (is_array($departamentos) || is_object($departamentos))
          {
              foreach ($departamentos as $departamento)
              {
                  echo "<option value='".$departamento["IdDepartamento"]."'>".$departamento["Descripcion"]."</option>";
              }
          }
          ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-md-4 col-xl-4">
        <label class="control-label" for="sede">Sede</label>
        <select id="sede" name="sede" class="form-control selectpicker"  title="-Seleccione-" required>
          <?php 
          if (is_array($sedes) || is_object($sedes))
          {
              foreach ($sedes as $sede)
              {
                  echo "<option value='".$sede["IdSede"]."'>".$sede["Descripcion"]."</option>";
              }
          }
          ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-md-4 col-xl-4">
        <label class="control-label" for="AsistenciEstud">¿Presta Asistencia Directa al Estudiante?</label>
        <p>
          Si <input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="1" required /> 
          No<input type="radio" class="flat" name="AsistenciEstud" id="AsistenciEstud" value="0" />
        </p>
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12" align="left">
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="AsistenciEstud">Cuadro de Información <small>Se muestra en la solicitud de citas para los estudiantes</small></label>
        <textarea id="informacion" name="informacion" class="form-control"></textarea>
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $('.selectpicker').selectpicker('refresh');
  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/DepartamentosController.php?accion=index');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado el departamento');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!', data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>