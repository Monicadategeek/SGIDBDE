
     <!-- Page Heading/Breadcrumbs -->

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Atención de Citas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12" >
    <?php 
        $titulofecha=date('d-m-Y', strtotime($fecha));
        if (date('Y-m-d', strtotime($fecha))==date('Y-m-d')) {
            $titulofecha="Hoy ".$titulofecha;
        }

    ?> 

    <div class="x_panel">
        <div class="row x_title">
            <div class="col-md-6">
                <h3>Listado de Citas <small><?php echo $titulofecha?></small></h3>
                <br>
                <?php
                    if ($value>0) {
                        if ((date('Y-m-d', strtotime($fecha))>=date('Y-m-d')) && count($citas)>0) 
                        { ?>
                       
                            <button class="btn btn-danger btn-xs" onclick="cancelarcita('../Controller/GestionCitasController.php?accion=cancelar', '<?php echo date('Y-m-d', strtotime($fecha))?>')" title="Cancelar citas"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar citas</button>
                     <?php 
                        }
                    }
                ?>
            </div>
            <div class="col-md-6">
                <div class="pull-right" style="    width: 50%; ">
                    <div><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <input type="text" id="fecha" name="fecha" class="form-control" style="width: 85%;display:inline-block;" value="<?php echo date('d-m-Y', strtotime($fecha))?>">
                    </div>
                </div>
            </div>                    
        </div>

        <div class="x_content" >
            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="citasdepto" > 
                <thead> 
                    <th width="15%">Servicio</th>
                    <th width="15%">Sede</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Hora</th>
                    <th>Estado</th>
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Servicio</th>
                    <th>Sede</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Hora</th>
                    <th>Estado</th>
                    <td></td>

                </tfoot>
                <tbody >
                    <?php 

                    if (is_array($citas) || is_object($citas))
                    {
                        foreach ($citas as $citas)
                        {
                            echo "<tr>
                            <td>".$citas["TipoDepto"]."</td>
                            <td>".$citas["Sede"]."</td>
                            <td>".$citas["nombres"]."</td>
                            <td>".$citas["apellidos"]."</td>
                            <td>".$citas["Hora"]."</td>
                            <td>".$citas["TipoAsistencia"]."</td>
                            <td>".$citas["botones"]."</td>
                            </tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


           
        <!-- /.row -->

    <script type="text/javascript">
    $(document).ready(function(){

    $('#citasdepto').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de Citas",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
    // Setup - add a text input to each footer cell
    $('#citasdepto tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#citasdepto').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([0,1,5]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});

    $(function () {
        $("#fecha").datepicker({
            language: 'es',
            format:'dd-mm-yyyy',
            todayHighlight:true,
            autoclose:true,
           
        });
    });

    $(document).ready(function()
    {
        $( "#fecha" ).datepicker("refresh");
        $("#fecha").change(function () {
            
            var  datos={"accion":'index', 'fecha':$('#fecha').val(), "id":$('#IdCitas').val()};
            enviar('../Controller/GestionCitasController.php', datos, 'contenido');
                         
        });


    });
</script>
