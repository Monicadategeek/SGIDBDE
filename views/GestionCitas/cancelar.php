<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Cancelar Citas</h4>
</div>
<div class="modal-body" id="formulario">
  <div class="form-group col-xs-12 col-md-12 col-xl-12" id="formulario"><p align="left"><b>Nota:</b> Serán canceladas todas las citas del día</p></div>
  
  <form id="cancelar" action="../Controller/GestionCitasController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="citaCancelada">
  <div class="col-xs-12 col-md-12 col-xl-12">
    <div class="form-group col-xs-12 col-md-4 col-xl-4">
      <label class="control-label" >Fecha de Cancelación</label>
      <input type="text" class="form-control"  value="<?php echo date("d-m-Y",strtotime($fecha)) ?>" disabled="">
  </div>
  <div class="form-group col-xs-12 col-md-4 col-xl-4">
    <input type="hidden" name="Fecha_Inic" id="Fecha_Inic" value="<?php echo $fecha ?>">     
    <label class="control-label" for="Tipo_Cancelacion ">Tipo de Cancelación</label>
    <select id="Tipo_Cancelacion" name="Tipo_Cancelacion" class="form-control selectpicker" required="" title="-Seleccione-">
      <option value="4">Cancelada por el Departamento</option>
      <option value="5">Inasistencia Médica</option>
    </select>
  </div>
  <div class="form-group col-xs-12 col-md-4 col-xl-4">
    <label class="control-label" for="Depto">Servicio</label>
    <select id="Depto" name="Depto" class="form-control selectpicker" required="" title="-Seleccione-">
      <?php 
        if (is_array($departamentos) || is_object($departamentos))
        {
            foreach ($departamentos as $departamento)
            {
                echo "<option value='".$departamento["IdDepartamentos"]."'>".$departamento["TipoDepto"]." en ".$departamento["Sede"]."</option>";
            }
        }
      ?>
    </select>
  </div>


  </div>
   
      
      
    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <p><b>Nota:</b> Es Importante que escriba la razón de cancelación de las citas, ya que se enviara por correo a los estudiantes asignados</p>
      <label for="razon">Razón de cancelación</label>
      <textarea id="razon" name="razon" class="form-control"></textarea>
      
    </div>
    <div class="form-group col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
$('.selectpicker').selectpicker('refresh');
  $('#formulario').on('submit', '#cancelar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;         
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/GestionCitasController.php?accion=index');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha cancelado las citas');
            }
            else{
              notificacion(2,'fa fa-times-circle','Error!','Ha Ocurrido un Error: '+data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>