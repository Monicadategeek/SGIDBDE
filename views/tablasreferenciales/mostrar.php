
<div align="right" style="margin-top: 13px;">
    <button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/TablasReferencialesController.php?accion=create&tabla=<?php echo $tb?>')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Registro</button>
    
</div>
<table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" id="tablasreferenciales"> 
    <thead>
        <th>Id</th>
        <th>Descripción</th>
        <th></th>
    </thead>
    <tfoot>
        <th>Id</th>
        <th>Descripción</th>
        <td></td>
    </tfoot>
    <tbody>
    <?php 
    if (is_array($tabla) || is_object($tabla))
    {

        foreach ($tabla as $columna)
        {
            echo "<tr>
            <td>".$columna["id"]."</td>
            <td>".$columna["descripcion"]."</td>
            <td style='text-align: center !important;'>".$columna["boton"]."</td>
            </tr>";
        }
    }
    ?> 
    </tbody>
</table>


    <script type="text/javascript">

    $(document).ready(function(){

    $('#tablasreferenciales').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de tablasreferenciales",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        
        responsive: true,
    });
// Setup - add a text input to each footer cell
    $('#tablasreferenciales tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#tablasreferenciales').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

});


</script>