<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Registro de Datos Referenciales</h4>
</div>

<div class="modal-body row" id="formulario">
  
  <form id="agregar" action="../Controller/TablasReferencialesController.php" method="POST">
    <input type="hidden" name="accion" id="accion" value="store">
    <input type="hidden" name="tabla" id="tabla" value="<?php echo $tabla['nro']?>">
    <div class="col-xs-12 col-md-12 col-xl-12" align="left">
      <div class="form-group col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="tipodepto">Tabla Referencial </label>
        <input type="text"  class="form-control" name="" id=""  value="<?php echo $tabla['tabla']?>" readonly>
      </div>
      <div class="form-group col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="tiporesp">Tipo de Pregunta</label>
        <select id="tiporesp" name="tiporesp" class="form-control selectpicker" title="-Seleccione-" required>
          <option value="0">Sin Respuesta</option>
          <option value="1">Respuesta Cerrada</option>
          <option value="2">Respuesta (Si o No) más una caja de texto</option>
        </select>
      </div>
      <p align="justify"><b>NOTA</b> Una vez guardado el tipo de pregunta, no podra modificarlo.</p>
      <p align="justify"><b>*</b> Las Opción Sin Respuesta Sirve para realizar una pregunta General que se deba desglosar, ejem: alergias.</p>
      <p align="justify"><b>*</b> No se puede ordenar las preguntas sea cuidadoso al registrarlas.</p>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12" align="left">
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="dato">Dato</label>
        <input type="text"  class="form-control" name="dato" id="dato" maxlength="100" style="text-transform:uppercase;">
      </div>
    </div>

    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
  $('.selectpicker').selectpicker('refresh');
  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              var  datos={"accion":'mostrar', "tabla":$('#tabla').val() };
              enviar('../Controller/TablasReferencialesController.php', datos, 'informacion');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado el departamento');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!', data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>