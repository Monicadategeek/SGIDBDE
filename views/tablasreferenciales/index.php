     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tablas Referenciales
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">       
            
           <div class="col-xs-6 col-md-6">
                <h3>Listado de los Tablas Referenciales</h3>
            </div>      

            

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            
            <div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="col-xs-6 col-md-6">
                <label for="categoria">Categoria</label>
                    <select  name="categoria" id="categoria" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
                    <?php 
                    foreach ($this->catmostrar as $categoria) {
                        echo "<option value='".$categoria["nro"]."'>".$categoria["categoria"]."</option>";
                    }
                    ?>
                </select>
                </div>
                <div class="col-xs-6 col-md-6" id="tablas">
                
                </div>

            </div>
             <div class="col-md-12 col-sm-12 col-xs-12" id="informacion"></div>
        </div>
    </div>
</div>



           
        <!-- /.row -->
 <script type="text/javascript">
    $(document).ready(function(){

        $('.selectpicker').selectpicker('refresh');
    
        
        $("#categoria").change(function () { 
            var  datos={"accion":'buscar', "categoria":$('#categoria option:selected').val() };
            enviar('../Controller/TablasReferencialesController.php', datos, 'tablas');
               
        });
    });



</script>