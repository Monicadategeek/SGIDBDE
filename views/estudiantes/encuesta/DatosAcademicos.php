<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="95" style="width:1%"></div>
</div>
<h2><i class="fa fa-university" aria-hidden="true"></i> Datos Académicos</h2>
<hr>
<div id="formulario">
    <form id="datos-academicos" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaController.php">
        <input type="hidden" name="accion" id="accion" value="registrar">
        <input type="hidden" name="form" id="form" value="datos-academicos">
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="id_tit_bach">Título de Bachiller</label>
                <select id="id_tit_bach" name="id_tit_bach" class="form-control selectpicker" title="-Seleccione-" required="">
                <?php
                    foreach ($MencionesBachs as $MencionesBach) {
                        echo '<option value="'.$MencionesBach["id_mencion_bach"].'">'.$MencionesBach["desc_mencion"].'</option>';
                    }
                ?>
                </select>  
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="tipo_inst">Tipo de Institución de Estudios Superiores</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="tipo_inst"  value="1">Pública
                    </label>
                    <label>
                        <input type="radio" name="tipo_inst"  value="2">Privada
                    </label>
                </div>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="prom_notas_bach">Promedio de Notas (Bachillerato)</label>
                <select id="prom_notas_bach" name="prom_notas_bach" class="form-control selectpicker" title="-Seleccione-" required="">
                    <option value="10">10</option>
                    <option value="11-12">11-12</option>
                    <option value="13-14">13-14</option>
                    <option value="15-16">15-16</option>
                    <option value="17-18">17-18</option>
                    <option value="19-20">19-20</option>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="identidad_pnf">¿Se siente identificado con los PNF?</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="identidad_pnf"  value="1">Si
                    </label>
                    <label>
                        <input type="radio" name="identidad_pnf"  value="2">No
                    </label>
                </div>
                <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="te_gusta">¿Le gusta el Programa Nacional de Formación de su carrera?</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="te_gusta"  value="1">Si
                    </label>
                    <label>
                        <input type="radio" name="te_gusta"  value="2">No
                    </label>
                </div>
                <div class="help-block with-errors"></div>
            </div> 
        </div>   

        <div class="form-group col-xs-12 col-md-12 col-xl-12">
            <label for="motivo_pnf">¿Qué le motivo a estudiar la carrera?</label>
            <textarea id="motivo_pnf" name="motivo_pnf" class="form-control" required="" maxlength="200"></textarea>
            <div class="help-block with-errors"></div>
        </div>
        
        <button type="submit" id="boton" class="btn btn-success btn-sm">Siguiente</button>         
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
     
    });

    $('#datos-academicos').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){
              $("#encuesta").html(data);
              
            }
        })
        document.getElementById("boton").disabled = false;
    })


    

</script>