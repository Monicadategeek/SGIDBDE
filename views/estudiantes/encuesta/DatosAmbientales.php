<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="95" style="width:60%"></div>
</div>
<h2><i class="fa fa-home" aria-hidden="true"></i> Datos Físico - Ambientales</h2>
<hr>
<div id="formulario">
    <form id="datos-ambientales" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaController.php">
        <input type="hidden" name="accion" id="accion" value="registrar">
        <input type="hidden" name="form" id="form" value="datos-ambientales">

       
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="col-md-12">
             <label>Lugar de Procedencia</label>
            </div>
            <div class="form-group col-xs-12 col-md-3" id="estado">
              <label class="control-label" for="id_estado">Estado </label>
              <select id="id_estado" name="id_estado" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($estados) || is_object($estados))
              {
                  foreach ($estados as $estados)
                  {
                      echo "<option value='".$estados["id_estado"]."'>".$estados["estado"]."</option>";
                  }

              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-3" id="municipio">
              
            </div>
            <div class="form-group col-xs-12 col-md-3" id="parroquia">
              
            </div>
            <div class="form-group col-xs-12 col-md-3" id="ciudad">
              
            </div>


        </div>

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tip_viv">Tipo de Vivienda</label>
              <select id="id_tip_viv" name="id_tip_viv" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tpviviendas) || is_object($tpviviendas))
              {
                  foreach ($tpviviendas as $tpvivienda)
                  {
                      echo "<option value='".$tpvivienda["id_tipo_viv"]."'>".$tpvivienda["desc_tipo_viv"]."</option>";
                  }
              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_ten_viv">Tenencia de la Vivienda</label>
              <select id="id_ten_viv" name="id_ten_viv" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tenecviv) || is_object($tenecviv))
              {
                  foreach ($tenecviv as $tenecviv)
                  {
                      echo "<option value='".$tenecviv["id_ten_viv"]."'>".$tenecviv["desc_ten_viv"]."</option>";
                  }

              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="constr_viv">Construcción</label>
              <select id="constr_viv" name="constr_viv" class="form-control selectpicker" required="" title="-Seleccione-">
                <option value="1">Totalmente Acabada</option>
                <option value="2">En proceso de Construcción</option>
              </select>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 col-xl-12">
            
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_piso">Tipo de Piso</label>
              <select id="id_tipo_piso" name="id_tipo_piso" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tppisos) || is_object($tppisos))
              {
                  foreach ($tppisos as $tppiso)
                  {
                      echo "<option value='".$tppiso["id_tipo_piso"]."'>".$tppiso["desc_tipo_piso"]."</option>";
                  }

              }
              ?>
              </select>
            </div>
            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_pared">Tipo de Pared</label>
              <select id="id_tipo_pared" name="id_tipo_pared" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tpparedes) || is_object($tpparedes))
              {
                  foreach ($tpparedes as $tppared)
                  {
                      echo "<option value='".$tppared["id_tipo_pared"]."'>".$tppared["desc_tipo_pared"]."</option>";

                  }
              }
              ?>
              </select>
            </div>

            <div class="form-group col-xs-12 col-md-4">
              <label class="control-label" for="id_tipo_techo">Tipo de Techo</label>
              <select id="id_tipo_techo" name="id_tipo_techo" class="form-control selectpicker" required="" title="-Seleccione-">
              <?php 
              if (is_array($tptechos) || is_object($tptechos))
              {
                  foreach ($tptechos as $tptecho)
                  {
                      echo "<option value='".$tptecho["id_tipo_techo"]."'>".$tptecho["desc_tipo_techo"]."</option>";
                  }
              }
              ?>
              </select>
            </div>


        </div>
        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="identidad_pnf">Espacios que componen su vivienda</label>
                <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                    <tbody>
                        <tr>
                            <th width="50%">Sala</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="sala" title="" required="" value="1">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Cocina</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="cocina" title="" required="" value="1">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Baños</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="banios" title="" required="" value="1">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Dormitorios</th>
                            <td>
                                <input type="text" class="form-control nro" id="" name="dormitorios" title="" required="" value="1">
                                <div class="help-block with-errors"></div>
                            </td>
                        </tr> 
                    </tbody>
                    
                </table>                
            </div>
            <div class="form-group col-xs-12 col-md-6 col-xl-6">
                <label for="identidad_pnf">Servicios publicos con los que cuenta su vivienda su vivienda</label>
                <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                    <tbody>
                        <tr>
                            <th width="50%">Luz</th>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="luz"  value="1" checked="" required>Si
                                    </label>
                                    <label>
                                        <input type="radio" name="luz" value="0" >No
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Agua</th>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="agua"  value="1" checked="" required>Si
                                    </label>
                                    <label>
                                        <input type="radio" name="agua" value="0" >No
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Aseo Urbano</th>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="aseo_urb"  value="1" checked="" required>Si
                                    </label>
                                    <label>
                                        <input type="radio" name="aseo_urb" value="0" >No
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Telefono Local</th>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="telf_local"  value="1" checked="" required>Si
                                    </label>
                                    <label>
                                        <input type="radio" name="telf_local" value="0" >No
                                    </label>
                                </div>
                            </td>
                        </tr> 
                    </tbody>
                    
                </table>                
            </div>
        </div>   
        
        <button type="submit" id="boton" class="btn btn-success btn-sm">Siguiente</button>         
    </form>
</div>
<script type="text/javascript">
$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
        $("#id_estado").change(function () {
            var  datos={"accion":'municipio', "estado":$('#id_estado option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'municipio');
            $('#municipio').show();

             var  datos={"accion":'ciudad', "estado":$('#id_estado option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'ciudad');
            $('#ciudad').show();          

            $('#parroquia').hide();    
        });
    });


    
</script>


<script type="text/javascript">
    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
      $('.nro').mask('00');
     
    });
    $('#datos-ambientales').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){
              $("#encuesta").html(data);
            }
        })
        document.getElementById("boton").disabled = false;
    })

   

    

</script>