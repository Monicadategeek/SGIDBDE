<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="95" style="width:100%"></div>
</div>
<h2><i class="fa fa-check-square-o" aria-hidden="true"></i> Completado!</h2>
<hr>
<div class="col-xs-12 col-md-12 col-xl-12">
	<p align="center">Gracias por participar en la encuesta de la División de Bienestar y Desarrollo Estudiantil del IUTOMS<br><br>
	<button type="button" class="btn btn-success btn-lg" onclick="closeOpenedWindow();"> Terminar</button>
	</p>
	
</div>

<script type="text/javascript">
	function closeOpenedWindow()
	{
	  window.close();
	}
</script>