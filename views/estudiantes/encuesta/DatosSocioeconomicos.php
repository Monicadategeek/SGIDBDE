
<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="95" style="width:20%"></div>
</div>
<h2><i class="fa fa-money" aria-hidden="true"></i> Datos Socioeconómicos</h2>
<hr>
<div id="formulario">
    <form id="datos-socioeconomicos" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaController.php">
        <input type="hidden" name="accion" id="accion" value="registrar">
        <input type="hidden" name="form" id="form" value="datos-socioeconomicos">
        

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-3 col-xl-3">
                <label for="tres_comidas">¿Cuantas comidas consume al dia?</label>
                
                <select id="tres_comidas" name="tres_comidas" class="form-control selectpicker" title="-Seleccione-">
                    <option value="1">Una Comida</option>
                    <option value="2">Dos Comidas</option>
                    <option value="3">Tres Comidas</option>
                    <option value="4">Mas de Tres Comidas</option>
                </select>
            </div>
            <div class="form-group col-xs-12 col-md-9 col-xl-9" id="divtres_comidas">
                <label for="exptres_comidas">Por favor, Explique brevemente</label>
                <textarea id="exptres_comidas" name="exptres_comidas" class="form-control textarea_estile"></textarea>
            </div>

        </div>

        <div class="col-xs-12 col-md-12 col-xl-12">
            <div class="form-group col-xs-12 col-md-3 col-xl-3">
                <label for="problemas_traslado">¿Presenta Problemas para trasladarse al IUTOMS?</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="problemas_traslado"  value="Si" required>Si
                    </label>
                    <label>
                        <input type="radio" name="problemas_traslado"  value="No" required>No
                    </label>
                </div>
                <div class="help-block with-errors" ></div>
            </div> 

        	<div class="form-group col-xs-12 col-md-3 col-xl-3">
	            <label for="tipo_traslado_iutoms">¿Como se traslada al IUTOMS?</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="tipo_traslado_iutoms"  value="1" required>Transporte Particular
	                </label><br>
	                <label>
	                    <input type="radio" name="tipo_traslado_iutoms"  value="2" >Transporte Público
	                </label><br>
                    <label>
                        <input type="radio" name="tipo_traslado_iutoms"  value="3" >Caminando
                    </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>
	        
	        <div class="form-group col-xs-12 col-md-3 col-xl-3">
	            <label for="Trabaja">Trabaja</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="Trabaja" id="Trabaja1" value="1" required>Si
	                </label>
	                <label>
	                    <input type="radio" name="Trabaja"  id="Trabaja2" value="2" >No
	                </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>

	        <div class="form-group col-xs-12 col-md-3 col-xl-3" id="hatrabajado">
	            <label for="Trabajo">¿Ha Trabajado?</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="Trabajo" id="Trabajo1" value="1" >Si
	                </label>
	                <label>
	                    <input type="radio" name="Trabajo"  id="Trabajo2" value="2" checked="">No
	                </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>
        </div>
        <div class="col-xs-12 col-md-12 col-xl-12" id="trabajo">
        	<div class="form-group col-xs-12 col-md-4 col-xl-4">
             	<label for="NomOrga">Nombre de la Organización que trabaje o trabajo</label>
            	<input type="text" class="form-control" id="NomOrga" name="NomOrga" title="Razón del retiro de la materia">
            	<div class="help-block with-errors" ></div>
        	</div>
        	<div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="Cargo">Cargo en la Organización que trabaje o trabajo</label>
                <input type="text" class="form-control" id="Cargo" name="Cargo" title="Razón del retiro de la materia">
                <div class="help-block with-errors" ></div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="Sueldo">Sueldo en la Organización que trabaje o trabajo</label>
                <input type="text" class="form-control bs" id="Sueldo" name="Sueldo" title="Sueldo en la Organización que trabaje o trabajo">
                <div class="help-block with-errors" ></div>
            </div>
            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <div class="col-md-12">
                    <label for="horario">Horario</label>
                </div>
                <div class="col-md-3" style="padding:0;">
                    <select id="inic" name="inic" class="form-control selectpicker" title="Hora">
                        <option value="1:00">1:00</option>
                        <option value="2:00">2:00</option>
                        <option value="3:00">3:00</option>
                        <option value="4:00">4:00</option>
                        <option value="5:00">5:00</option>
                        <option value="6:00">6:00</option>
                        <option value="7:00">7:00</option>
                        <option value="8:00">8:00</option>
                        <option value="9:00">9:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding:0;">
                    <select id="inicg" name="inicg" class="form-control selectpicker" >
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding:0;">
                    <select id="fin" name="fin" class="form-control selectpicker" title="Hora">
                        <option value="1:00">1:00</option>
                        <option value="2:00">2:00</option>
                        <option value="3:00">3:00</option>
                        <option value="4:00">4:00</option>
                        <option value="5:00">5:00</option>
                        <option value="6:00">6:00</option>
                        <option value="7:00">7:00</option>
                        <option value="8:00">8:00</option>
                        <option value="9:00">9:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding:0;">
                    <select id="fing" name="fing" class="form-control selectpicker">
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
                </div>              
            </div>
            <div class="form-group col-xs-12 col-md-8 col-xl-8">
                <label for="donde_trabaja">Dirección</label>
                <input type="text" class="form-control" id="donde_trabaja" name="donde_trabaja" title="Dirección del trabajo" max="200">
                <div class="help-block with-errors" ></div>
            </div>

            <div class="form-group col-xs-12 col-md-12 col-xl-12" id="sintrabajo">
                <label for="RazRetTrab">Razón de porque se fue del trabajo</label>
                <input type="text" class="form-control" id="RazRetTrab" name="RazRetTrab" title="Razón de porque se fue del trabajo" max="200">
                <div class="help-block with-errors" ></div>
            </div>
        </div>    

        <div class="col-xs-12 col-md-12 col-xl-12">
                        
            <div class="form-group col-xs-12 col-md-12 col-xl-12">
                <label for="AyuEcon">¿Recibe Ayuda Económica?</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="AyuEcon" id="AyuEcon1"  value="1" required>Si
                    </label>
                    <label>
                        <input type="radio" name="AyuEcon" id="AyuEcon2" value="2" required>No
                    </label>
                </div>
                <div class="help-block with-errors" ></div>
            </div>

            <div class="form-group col-xs-12 col-md-12 col-xl-12" id="divAyuEcon">
            	<table class="table table-bordered table-hover">
            		<thead>
            			<th>Origen de la Ayuda</th>
            			<th>Monto en Bs</th>
            		</thead>
            		<tbody>
            			<?php 
                             foreach ($ayudaseconomica as $ayudaeconomica) {
                        echo '<tr>
                            <td>'.$ayudaeconomica["desc_ayud_econ"].'</td>
                            <td><input type="text" class="form-control bs" id="ayud_econ'.$ayudaeconomica["id_ayud_econ"].'" name="ayud_econ'.$ayudaeconomica["id_ayud_econ"].'" title="Ayuda de '.$ayudaeconomica["desc_ayud_econ"].' Individual" value="0"><div class="help-block with-errors" ></div></td>                      

                        </tr>';
                    }

                        ?>
            		</tbody>
            	</table>
            </div>
        </div>

        <div class="col-xs-12 col-md-12 col-xl-12"> 
	         <div class="form-group col-xs-12 col-md-4 col-xl-4">
	            <label for="tiehijos">Tiene Hijos</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="tiehijos" id="tiehijos1" value="1" required>Si
	                </label>
	                <label>
	                    <input type="radio" name="tiehijos" id="tiehijos2" value="0" >No
	                </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>
            <div id="divhijos">
                <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="nrohijos">¿Cuantos Tiene?</label>
                <select id="nrohijos" name="nrohijos" class="form-control selectpicker" title="-Hijos-">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <div class="help-block with-errors" ></div>
            </div>

            <div class="form-group col-xs-12 col-md-4 col-xl-4">
                <label for="viven_contigo">Viven Contigo</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="viven_contigo" id="viven_contigo1"  value="1">Si
                    </label>
                    <label>
                        <input type="radio" name="viven_contigo" id="viven_contigo2"  value="0" >No
                    </label>
                </div>
                <div class="help-block with-errors" ></div>
            </div>
            <div class="form-group col-xs-12 col-md-12 col-xl-12" id="divviven_contigo">
                    <label for="Razviven_contigo">De ser negativo. ¿Porqué?</label>
                    <input type="text" class="form-control" id="Razviven_contigo" name="Razviven_contigo" title="Razón de porque la que tus hijos no viven contigo" max="200">
                    <div class="help-block with-errors" ></div>
              </div>
            </div>
	        
          </div>
          <div class="col-xs-12 col-md-12 col-xl-12"> 
	         <div class="form-group col-xs-12 col-md-4 col-xl-4">
	            <label for="pareja">Tiene Pareja</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="pareja"  value="1" required>Si
	                </label>
	                <label>
	                    <input type="radio" name="pareja"  value="2" required>No
	                </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>

	        <div class="form-group col-xs-12 col-md-4 col-xl-4">
	            <label for="pers_dep">¿Alguna persona depende de Usted?</label>
	            <div class="radio">
	                <label>
	                    <input type="radio" name="pers_dep" id="pers_dep1" value="1" required>Si
	                </label>
	                <label>
	                    <input type="radio" name="pers_dep"  id="pers_dep2" value="0" >No
	                </label>
	            </div>
	            <div class="help-block with-errors" ></div>
	        </div>

	        <div class="form-group col-xs-12 col-md-4 col-xl-4" id="divpers_dep">
	            <label for="nropers_dep">¿Cuantas?</label>
	            <select id="nropers_dep" name="nropers_dep" class="form-control selectpicker" title="-Nro-">
	            	<option value="1">1</option>
	            	<option value="2">2</option>
	            	<option value="3">3</option>
	            	<option value="4">4</option>
	            	<option value="5">5</option>
	            	<option value="6">6</option>
	            	<option value="7">7</option>
	            	<option value="8">8</option>
	            	<option value="9">9</option>
	            	<option value="10">10</option>
	            </select>
	            <div class="help-block with-errors" ></div>
	        </div>

	    </div>

	    <div class="form-group col-xs-12 col-md-6 col-xl-6">
            <label for="TotIngEst">Total Aprox Ingreso del Estudiante (Mensual)</label>
            <input type="text" class="form-control bs" id="TotIngEst" name="TotIngEst" title="Total Aprox Ingreso del Estudiante (Mensual)" required>
            <div class="help-block with-errors" ></div>
        </div>
        <div class="form-group col-xs-12 col-md-6 col-xl-6">
            <label for="TotIngFam">Total Aprox Ingreso Familiar (Mensual)</label>
            <input type="text" class="form-control bs" id="TotIngFam" name="TotIngFam" title="Total Aprox Ingreso Familiar (Mensual)" required>
            <div class="help-block with-errors" ></div>
        </div>
        <div class="form-group col-xs-12 col-md-12 col-xl-12">
            <label >Sinceracion de Egresos</label>
            <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                <thead>
                    <th>Individuales</th>
                    <th>Bolivares</th>
                    <th>Familiares</th>
                    <th>Bolivares</th>
                </thead>
                <tbody>
                <?php 
                foreach ($egresos as $egreso) {
                	echo '<tr>
                        <td>'.$egreso["Descripcion"].'</td>
                        <td><input type="text" class="form-control bs" id="egrind'.$egreso["IdEgreso"].'" name="egrind'.$egreso["IdEgreso"].'" title="'.$egreso["Descripcion"].' Individual" required="" value="12.00"><div class="help-block with-errors" ></div></td>
                        <td>'.$egreso["Descripcion"].'</td>
                        <td><input type="text" class="form-control bs" id="egrfam'.$egreso["IdEgreso"].'" name="egrfam'.$egreso["IdEgreso"].'" title="'.$egreso["IdEgreso"].'" Familiar" required="" value="13.00"><div class="help-block with-errors" ></div></td>
                    </tr>';
                }
                
                ?>                
                </tbody>
            </table>            
        </div>
        



                        
           

        
        <button type="submit" id="boton" class="btn btn-success btn-sm">Siguiente</button>         
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    	$('.bs').mask('0000000.00', {reverse: true});
      $('.selectpicker').selectpicker('refresh');
     
    });


    $('#trabajo').hide();
    $('#sintrabajo').hide();
    $('#hatrabajado').hide();
    $('#divAyuEcon').hide();
    $('#divhijos').hide();
    $('#divviven_contigo').hide();
    $('#divpers_dep').hide();
    $('#divtres_comidas').hide();




     $('#Trabaja1').change(function () {
        $('#trabajo').show();        
        $('#sintrabajo').hide();
        $('#hatrabajado').hide();

     });
      $('#Trabaja2').change(function () {
        $('#trabajo').hide();
        $('#hatrabajado').show();
     });

    $('#Trabajo1').change(function () {
        $('#trabajo').show();        
        $('#sintrabajo').show();

     });
      $('#Trabajo2').change(function () {
        $('#trabajo').hide();
        $('#sintrabajo').hide();
     });

      $('#AyuEcon1').change(function () {
        $('#divAyuEcon').show();  

     });
      $('#AyuEcon2').change(function () {
        $('#divAyuEcon').hide();
     });

      $('#tiehijos1').change(function () {
        $('#divhijos').show();    

     });
      $('#tiehijos2').change(function () {
        $('#divhijos').hide();
        $('#divviven_contigo').hide();
     });

       $('#viven_contigo1').change(function () {
            $('#divviven_contigo').hide();
            

     });
      $('#viven_contigo2').change(function () {
        $('#divviven_contigo').show();
     });

     $('#pers_dep1').change(function () {
        $('#divpers_dep').show();    

     });
      $('#pers_dep2').change(function () {
        $('#divpers_dep').hide();
     });
      
    $("#tres_comidas").change(function () { 
        if ($('#tres_comidas option:selected').val()<3) {
            
            $('#divtres_comidas').show();
        }
        else{
            $('#divtres_comidas').hide();
        }
    });

    $('#datos-socioeconomicos').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){

              $("#encuesta").html(data);
            }
        })
        document.getElementById("boton").disabled = false;
    }) 

</script>