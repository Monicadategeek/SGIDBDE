<label class="control-label" for="id_municipio">Municipio </label>
  <select id="id_municipio" name="id_municipio" class="form-control selectpicker" required="" title="-Seleccione-">
  <?php 
  if (is_array($municipios) || is_object($municipios))
  {
      foreach ($municipios as $municipio)
      {
          echo "<option value='".$municipio["id_municipio"]."'>".$municipio["municipio"]."</option>";
      }
  }
  ?>
  </select>

  <script type="text/javascript">
$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
        $("#id_municipio").change(function () {
            var  datos={"accion":'parroquia', "municipio":$('#id_municipio option:selected').val() };
            enviar('../Controller/EdoMunParrCiuController.php', datos, 'parroquia');
            $('#parroquia').show();          
        });
    });
    
</script>