<label class="control-label" for="id_ciudad">Ciudad </label>
  <select id="id_ciudad" name="id_ciudad" class="form-control selectpicker" required="" title="-Seleccione-">
  <?php 
  if (is_array($ciudades) || is_object($ciudades))
  {
      foreach ($ciudades as $ciudad)
      {
          echo "<option value='".$ciudad["id_ciudad"]."'>".$ciudad["ciudad"]."</option>";
      }
  }
  ?>
  </select>

  <script type="text/javascript">
$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
});    
</script>