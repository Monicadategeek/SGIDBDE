<label class="control-label" for="id_parroquia">Parroquia </label>
  <select id="id_parroquia" name="id_parroquia" class="form-control selectpicker" required="" title="-Seleccione-">
  <?php 
  if (is_array($parroquias) || is_object($parroquias))
  {
      foreach ($parroquias as $parroquia)
      {
          echo "<option value='".$parroquia["id_parroquia"]."'>".$parroquia["parroquia"]."</option>";
      }
  }
  ?>
  </select>

  <script type="text/javascript">
$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
});    
</script>