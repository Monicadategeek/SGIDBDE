<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="96" style="width:99%"></div>
</div>
<h2><i class="fa fa-search" aria-hidden="true"></i> Datos de Interes</h2>
<hr>
<div id="formulario">
    <form id="datos-culturales" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaController.php">
        <input type="hidden" name="accion" id="accion" value="registrar">
        <input type="hidden" name="form" id="form" value="datos-interes">


        <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-4">
            <label for="ayudaeco">¿Considera que requiere una ayudas socioeconómicas?</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="ayudaeco" id="ayudaeco1"  value="1" required>Si
                    </label>
                    <label>
                        <input type="radio" name="ayudaeco"  id="ayudaeco2"  value="0" required>No
                    </label>
                </div>
          </div>
          <div class="form-group col-xs-12 col-md-8" id="razayu">
            <label for="consideracion">¿Usted Considera que Requiere alguna de las ayudas socioeconómicas que brinda el IUTOMS? </label>
            <textarea class="form-control textarea_estile" name="consideracion"></textarea>
          </div>
        </div>
       

       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-4" id="modoayu">
              <label for="id_ayud">Modalidad de la Ayuda Socioecónomica</label>
              <select id="id_ayud" name="id_ayud" class="form-control selectpicker" title="-Seleccione-">
              <?php 
              if (is_array($becas) || is_object($becas))
              {
                  foreach ($becas as $beca)
                  {
                      echo "<option value='".$beca["IdTipoBeca"]."'>".$beca["Descripcion"]."</option>";
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-8">
              <label for="comedor">¿Esta Usted de Acuerdo con la Instalación de un comedor en el IUTOMS?</label>
              <div class="radio">
                  <label>
                      <input type="radio" name="comedor" value="1" required>Si
                  </label>
                  <label>
                      <input type="radio" name="comedor"  value="2" >No
                  </label>
              </div>
              <div class="help-block with-errors" ></div>
          </div>
       </div>       
        <button type="submit" id="boton" class="btn btn-success btn-sm">Siguiente</button>         
    </form>
</div>



<script type="text/javascript">
    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
      $('#razayu').hide();
      $('#modoayu').hide();
    });


    $('#ayudaeco1').change(function () {
        $('#razayu').show();  
        $('#modoayu').show();       

     });
      $('#ayudaeco2').change(function () {
        $('#modoayu').hide();
        $('#razayu').hide();
     });

      





    $('#datos-culturales').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){
              $("#encuesta").html(data);
            }
        })
        document.getElementById("boton").disabled = false;
    })

   

    

</script>