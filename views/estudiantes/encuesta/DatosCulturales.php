<div class="progress">
    <div class="progress-bar progress-bar-success" data-transitiongoal="96" style="width:80%"></div>
</div>
<h2><i class="fa fa-paper-plane-o" aria-hidden="true"></i><i class="fa fa-futbol-o" aria-hidden="true"></i> Datos Culturales y Deportivos</h2>
<hr>
<div id="formulario">
    <form id="datos-culturales" role="form" data-toggle="validator" class="row" method="POST">
        <input type="hidden" id="action" name="action" value="http://localhost/SGIDBDE/Controller/EncuestaController.php">
        <input type="hidden" name="accion" id="accion" value="registrar">
        <input type="hidden" name="form" id="form" value="datos-culturales">
        <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="actividades"></i> ¿En que Ocupas tu Tiempo Libre? <small>Puedes elegir multiple max(5)</small></label>
              <select id="actividades" name="actividades[]" class="form-control selectpicker" required="" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="5">
              <?php 
              if (is_array($actividades) || is_object($actividades))
              {
                  foreach ($actividades as $actividad)
                  {
                      echo "<option value='".$actividad["id_act_t_l"]."'>".$actividad["desc_act_t_l"]."</option>";
                  }
              }
              ?>
              </select>
          </div>
       </div>

       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="pracdeporte"> ¿Practicas algun deporte?</label>
              <div class="radio">
                  <label>
                      <input type="radio" name="pracdeporte" id="pracdeporte1" value="1" required>Si
                  </label>
                  <label>
                      <input type="radio" name="pracdeporte"  id="pracdeporte2" value="0" >No
                  </label>
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="divdeportes">
              <label for="deportes">¿Cual Practicas? <small>Puedes elegir multiple max(4)</small></label>
              <select id="deportes" name="deportes[]" class="form-control selectpicker" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="4 ">
              <?php 
              if (is_array($deportes) || is_object($deportes))
              {
                  foreach ($deportes as $deporte)
                  {
                      echo "<option value='".$deporte["id_cat_dep"]."'>".$deporte["desc_cat_dep"]."</option>";
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>

       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="posehaar">¿Posees Habilidades Artísticas?</label>
              <div class="radio">
                  <label>
                      <input type="radio" name="posehaar" id="posehaar1" value="1" required>Si
                  </label>
                  <label>
                      <input type="radio" name="posehaar"  id="posehaar2" value="0" >No
                  </label>
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="divhabilidades">
              <label for="habilidades">¿Cual? <small>Puedes elegir multiple max(5)</small></label>
              <select id="habilidades" name="habilidades[]" class="form-control selectpicker" title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="5">
              <?php 
              if (is_array($habilidades) || is_object($habilidades))
              {
                  foreach ($habilidades as $habilidad)
                  {
                      echo "<option value='".$habilidad["id_habil"]."'>".$habilidad["desc_habilidad"]."</option>";
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>
       <div class="col-xs-12 col-md-12 col-xl-12">
          <div class="form-group col-xs-12 col-md-6">
              <label for="actIUTOMS">¿Has Participado en alguna actividad del IUTOMS?</label>
              <div class="radio">
                  <label>
                      <input type="radio" name="actIUTOMS" id="actIUTOMS1" value="1" required>Si
                  </label>
                  <label>
                      <input type="radio" name="actIUTOMS"  id="actIUTOMS2" value="0" >No
                  </label>
              </div>
              <div class="help-block with-errors" ></div>
          </div>
          <div class="form-group col-xs-12 col-md-6" id="diveventos">
              <label for="eventos">¿Cual? <small>Puedes elegir multiple max(3)</small></label>
              <select id="eventos" name="eventos[]" class="form-control selectpicker"  title="-Seleccione-" multiple data-live-search="true" multiple data-max-options="3">
              <?php 
              if (is_array($eventos) || is_object($eventos))
              {
                  foreach ($eventos as $evento)
                  {
                      echo "<option value='".$evento["id_evento"]."'>".$evento["desc_evento"]."</option>";
                  }
              }
              ?>
              </select>
              <div class="help-block with-errors" ></div>
          </div>
       </div>
        
        <button type="submit" id="boton" class="btn btn-success btn-sm">Siguiente</button>         
    </form>
</div>



<script type="text/javascript">
    $(document).ready(function(){
      $('.selectpicker').selectpicker('refresh');
      $('#divdeportes').hide();
      $('#divhabilidades').hide();
      $('#diveventos').hide();
    });


    $('#pracdeporte1').change(function () {
        $('#divdeportes').show();        

     });
      $('#pracdeporte2').change(function () {
        $('#divdeportes').hide();
     });

      $('#posehaar1').change(function () {
        $('#divhabilidades').show();        

     });
      $('#posehaar2').change(function () {
        $('#divhabilidades').hide();
     });
      $('#actIUTOMS1').change(function () {
        $('#diveventos').show();        

     });
      $('#actIUTOMS2').change(function () {
        $('#diveventos').hide();
     });





    $('#datos-culturales').validator().on('submit', function (e) {
        e.preventDefault();
        document.getElementById("boton").disabled = true;
        $.ajax({
            type: $(this).attr('method'),
            url: $('#action').val(),
            data: $(this).serialize(),
            success:function(data){
              $("#encuesta").html(data);
            }
        })
        document.getElementById("boton").disabled = false;
    })

   

    

</script>