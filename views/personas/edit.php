<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de la Persona</h4>
</div>
<div class="modal-body row" id="formulario">
  <form id="agregar" action="../Controller/PersonasController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="update">
  <input type="hidden" name="Num_Usuario" id="Num_Usuario" value="<?php echo $persona["Num_Usuario"]?>">
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Cedula_Usu">Cédula</label>
      <input type="text" name="Cedula_Usu" id="Cedula_Usu" class="form-control" required="" title="Ingrese el Nro de Cédula del Trabajador" maxlength="8" value="<?php echo $persona["cedula"]?>">
    </div>
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Sexo_Usu">Sexo</label>
      <select id="Sexo_Usu" name="Sexo_Usu" class="form-control " required="" title="-Seleccione-">
        <?php
        if ($persona["sexo"]==2) {
          echo '<option value="2" selected>Masculino</option>
                <option value="1">Femenino</option>';
        }
        else{ 
          echo '<option value="2" >Masculino</option>
                <option value="1" selected>Femenino</option>';
        }
        ?>
      </select>
    </div>
  </div>

  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Nombre_1">Primer Nombre</label>
      <input type="text" name="Nombre_1" id="Nombre_1" class="form-control" required="" title="Ingrese el Primer Nombre del Trabajador" maxlength="15" value="<?php echo $persona["nombre"]?>">
    </div>
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Nombre_2">Segundo Nombre</label>
      <input type="text" name="Nombre_2" id="Nombre_2" class="form-control" title="Ingrese el Segundo Nombre del Trabajador" maxlength="15" value="<?php echo $persona["nombre2"]?>">
    </div>
  </div>
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Apellido_1">Primer Apellido</label>
      <input type="text" name="Apellido_1" id="Apellido_1" class="form-control" required="" title="Ingrese el Primer Apellido del Trabajador" maxlength="15" value="<?php echo $persona["apellido"]?>">
    </div>
    <div class="col-xs-6 col-md-6 col-xl-6">
      <label class="control-label" for="Apellido_2">Segundo Apellido</label>
      <input type="text" name="Apellido_2" id="Apellido_2" class="form-control" title="Ingrese el Segundo Apellido del Trabajador" maxlength="15" value="<?php echo $persona["apellido2"]?>">
    </div>
  </div>
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <div class="col-xs-6 col-md-6 col-xl-6">
      <input type="hidden" name="Telf_Usu" id="Telf_Usu">
      <label class="control-label" for="telefono">Teléfono de Habitación</label>
      <input type="text" name="telefono" id="telefono" class="form-control" required="" title="Ingrese el Nro de Teléfono de Habitación del Trabajador" value="<?php echo $persona["telefono"]?>">
    </div>
    <div class="col-xs-6 col-md-6 col-xl-6">
      <input type="hidden" name="Cel_Usu" id="Cel_Usu">
      <label class="control-label" for="celular">Teléfono Móvil</label>
      <input type="text" name="celular" id="celular" class="form-control" title="Ingrese el Nro de Teléfono Móvil del Trabajador" value="<?php echo $persona["celular"]?>">
    </div>
  </div>
  <div class="form-group col-xs-12 col-md-12 col-xl-12">
    <div class="col-xs-6 col-md-6 col-xl-6">
    <label class="control-label" for="Correo_Usu">Correo Electrónico</label>
      <input type="email" name="Correo_Usu" id="Correo_Usu" class="form-control" title="Ingrese la Dirección de Correo Electrónico de la Persona" required="" value="<?php echo $persona["correo"]?>">
    </div>
    <div class="col-xs-6 col-md-6 col-xl-6">
    <label class="control-label" for="Foto_Usuario">Foto</label>

      <input type="file" name="Foto_Usuario" id="Foto_Usuario" class="form-control" title="Ingrese la imagen de la Persona" >
       <p>Si no desea cambiar la imagen no modifique este campo</p>
    </div>
  </div>
  

    <div class="col-xs-12 col-md-12 col-xl-12" align="center" style="border-top: 1px solid #e5e5e5; padding-top: 14px;">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">
$(document).ready(function()
  {
    $('.selectpicker').selectpicker('refresh');

    $('#Cedula_Usu').mask('00000000');
    $('#telefono').mask('(000) 000-00-00');
    $('#celular').mask('(000) 000-00-00');
    $('#Foto_Usuario').change(function(){
      var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".png"];
      var archivos = this.files;
      if (archivos.length > 0) {
        for (var i = 0; i < archivos.length; i++) {
            sFileName = archivos[i];
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.name.substr(sFileName.name.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                }
            }                     
            if (!blnValid) {
                mensaje=sFileName.name + " es invalido, las extensiones validas son: " + _validFileExtensions.join(", ");
                notificacion(2, 'fa fa-times-circle','Error!',mensaje);
                this.value = "";
                return false;
            }
            if (sFileName.size/1024/1024 >= 2) {
                notificacion(2, 'fa fa-times-circle','Error!', 'El tamaño de la Foto_Usuarioe debe ser menor o igual a 2 MB');
                this.value = "";
                return false;
            }
        }
      }
      });
    

  });
  $('#formulario').on('submit', '#agregar', function (e) {
     e.preventDefault();

    div=$('#telefono').val().split('-');
      constructor=div[1]+div[2];
      div=div[0].split(' ');
      constructor=div[1]+constructor;
      div=div[0].split('(');
      div=div[1].split(')');
      constructor=div[0]+constructor;
      $('#Telf_Usu').val(constructor);
      
      div=$('#celular').val().split('-');
      constructor=div[1]+div[2];
      div=div[0].split(' ');
      constructor=div[1]+constructor;
      div=div[0].split('(');
      div=div[1].split(')');
      constructor=div[0]+constructor;
      $('#Cel_Usu').val(constructor);
   
    document.getElementById("boton").disabled = true;
    var parametros= new FormData($(this)[0]);
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: parametros,
        contentType:false,
        processData:false,
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/PersonasController.php');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado la persona');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!',data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>