    <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Personas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Listado de los Personas</h2>
            <div align="right">
                <button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/PersonasController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Registrar Persona</button>
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered dt-responsive nowrap table-hover" cellspacing="0" width="100%" id="usuarios" > 
                <thead> 
                    <th>Cédula</th>                    
                    <th width="10%">Foto</th>     
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Sexo</th>
                    <th>Estado</th>
                    <th>Correo</th>
                    <th>Telefono</th> 
                    <th>Celular</th>                
                    <th>Acciones</th>

                </thead>
                <tfoot>
                    <th>Cédula</th>
                    <th>Foto</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Sexo</th>
                    <th>Estado</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th>Celular</th>
                    
                    <td></td>
                </tfoot>
                <tbody >
                    <?php 
                        if (is_array($usuarios) || is_object($usuarios))
                        {
                            
                            foreach ($usuarios as $usuarios)
                            {
                                echo "<tr>
                                <td>".$usuarios["cedula"]."</td>                                
                                <td>".$usuarios["Foto_Usuario"]."</td>
                                <td>".$usuarios["Nombres"]."</td>
                                <td>".$usuarios["Apellidos"]."</td>
                                <td>".$usuarios["sexo"]."</td>
                                <td>".$usuarios["Cod_Estatus"]."</td>
                                <td>".$usuarios["correo"]."</td>
                                <td>".$usuarios["telefono"]."</td>
                                <td>".$usuarios["celular"]."</td>";

                                
                                echo '<td><button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/PersonasController.php',".$usuarios["Num_Usuario"].');"><i class="fa fa-pencil"></i></button></td>';

                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody >
            </table>
        </div>
    </div>
</div>
           
        <!-- /.row -->
    <script type="text/javascript">
    $(document).ready(function(){
        $('#usuarios').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de usuarios",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
             
        responsive: true
    });
// Setup - add a text input to each footer cell
    $('#usuarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" style="width:100%"/>' );
    } );
 
    // DataTable
    var table = $('#usuarios').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

});


</script>
