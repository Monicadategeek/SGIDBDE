             <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Division de Bienestar y Desarrollo Estudiantil
                    <small>Blog de Noticias</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#">Inicio</a>
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <!-- First Blog Post -->
                <h2>
                    <a href="#">Titulo Post 1</a>
                </h2>
                <p class="lead">
                    por <a href="../web/index.php">Alejandro Hernandez</a>
                </p>
                <p><i class="fa fa-clock-o"></i> Posteado el  28 Agosto de 2013 a las 10:00 PM</p>
                <hr>
                <a href="../web/blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/900x300" alt="">
                </a>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
                <a class="btn btn-primary" href="#">Leer Mas <i class="fa fa-angle-right"></i></a>

                <hr>

                <!-- Second Blog Post -->
                <h2>
                    <a href="#">Titulo Post 2</a>
                </h2>
                <p class="lead">
                    por <a href="../web/index.php">Fidelmar Gutierrez</a>
                </p>
                <p><i class="fa fa-clock-o"></i> Posteado el  28 Agosto de 2013 a las 10:00 PM</p>
                <hr>
                <a href="../web/blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/900x300" alt="">
                </a>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, quasi, fugiat, asperiores harum voluptatum tenetur a possimus nesciunt quod accusamus saepe tempora ipsam distinctio minima dolorum perferendis labore impedit voluptates!</p>
                <a class="btn btn-primary" href="#">Leer Mas <i class="fa fa-angle-right"></i></a>

                <hr>

                <!-- Third Blog Post -->
                <h2>
                    <a href="#">Titulo Post 3</a>
                </h2>
                <p class="lead">
                    por <a href="../web/index.php">Anyelson Espinoza</a>
                </p>
                <p><i class="fa fa-clock-o"></i> Posteado el  28 Agosto de 2013 a las 10:00 PM</p>
                <hr>
                <a href="../web/blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/900x300" alt="">
                </a>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, voluptates, voluptas dolore ipsam cumque quam veniam accusantium laudantium adipisci architecto itaque dicta aperiam maiores provident id incidunt autem. Magni, ratione.</p>
                <a class="btn btn-primary" href="#">Leer Mas <i class="fa fa-angle-right"></i></a>

                <hr>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Anteriores</a>
                    </li>
                    <li class="next">
                        <a href="#">Nuevos &rarr;</a>
                    </li>
                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Buscador Blog</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categorias Blog</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Panel</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>

        </div>
        <!-- /.row -->