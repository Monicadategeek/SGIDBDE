<link href="../web/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../web/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../web/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../web/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../web/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">     
     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Departamentos
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Listado de los Departamentos</h2>
            <div align="right">
                <button type="button" class="btn btn-primary"  onclick="nuevo('../Controller/DepartamentosController.php?accion=nuevo')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Registro</button>
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="departamentos"> 
                <thead>
                    <th>Id</th>
                    <th>Tipo de Depto</th>
                    <th>Sede</th>
                    <th>¿Asiste al Estudiante?</th>
                    <th>Ultima Modificación</th>
                    <th>Responable</th>
                    <th>Estatus</th>

                </thead>
                <tfoot>
                    <td></td>
                    <th>Tipo de Depto</th>
                    <th>Sede</th>
                    <th>¿Asiste al Estudiante?</th>
                    <th>Ultima Modificación</th>
                    <th>Responable</th>
                    <th >Estatus</th>
                </tfoot>
                <tbody>
                    <?php 
                        if (is_array($departamentos) || is_object($departamentos))
                        {
                            
                            foreach ($departamentos as $departamento)
                            {
                                echo "<tr>
                                <td>".$departamento["IdDepartamentos"]."</td>
                                <td>".$departamento["TipoDepto"]."</td>
                                <td>".$departamento["Sede"]."</td>";
                                if ($departamento["AsistenciEstud"]==1) {
                                    $AsistenciEstud="Si";
                                }
                                else{
                                    $AsistenciEstud="No";
                                }
                                echo "<td>".$AsistenciEstud."</td>";
                                echo "<td>".date('d-m-Y', strtotime($departamento["UltModif"]))."</td>";
                                if ($departamento["Estatus"]==1) {
                                    $Estatus="Activo";
                                }
                                else{
                                    $Estatus="Inactivo";
                                }
                                echo "<td>".$departamento["Responsable"]."</td>
                                <td>".$Estatus."</td>
                                </tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
           
        <!-- /.row -->
<!-- Datatables -->
    <script src="../web/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../web/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../web/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../web/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../web/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../web/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../web/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../web/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
$('#departamentos').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de departamentos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#departamentos tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" />' );
    } );
 
    // DataTable
    var table = $('#departamentos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([1,2,3,4,5,6]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>