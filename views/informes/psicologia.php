<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
    <a class="btn btn-default btn-sm buttons-pdf" href="#" onclick="tomarcaptura('contenidomodalver .modal-body');" style="float: left;"><i class="fa fa-print"></i> Imprimir</a>

  <h4 class="modal-title" style="text-align: center;">Ver <?php echo $cita["tipoInforme"]?></h4>
</div>
<div class="modal-body row">
	<div class="col-xs-12 col-md-12 col-xl-12 row">
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="<?php include('../views/logo.php');?>" style="width: 50%;"></div>
        <div class="col-xs-8 col-md-8 col-xl-8" style="text-align: center;">REPÚBLICA BOLIVARIANA DE VENEZUELA<br>
        MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN SUPERIOR<br>
        INSTITUTO UNIVERSITARIO DE TECNOLIGIA DEL OESTE<br> 
        "MARISCAL SUCRE"<br>
        Caracas.
        </div>
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="../web/images/mppes.png" style="width: 50%;" ></div>
		<div class="col-xs-12 col-md-12 col-xl-12">
        <br><br>
            
            <div class="row">
                <div class="col-md-4">
		             <label>Fecha: </label><div class="border-bottom"><?php echo $cita["Fecha"]?></div>
                </div>
            </div>              

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <table class="col-xs-12 col-md-12 tablatdborder">
                        <tr>
                            <th width="20%">Nombre y Apellido:</th>
                            <td width="45%"><?php echo $estudiante["nombres"]." ".$estudiante["apellidos"]?></td>
                            <th width="7%">Edad:</th>
                            <td width="10%"><?php echo $estudiante["edad"]?></td>
                            <th width="18%" style=" text-align: center !important;" ><?php echo $estudiante["sexo"]?></th>

                        </tr>
                    </table>
                </div>
            </div>  

            <div class="row">
               <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="5%">C.I.:</th>
                            <td width="10%"><?php echo $estudiante["cedula"]?></td>

                            <th width="10%">Dirección:</th>
                            <td width="75%" colspan="2"><div class="border-bottom"><?php echo $estudiante["direccion"]?></td>

                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                 <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="10%">Carrera:</th>
                            <td width="25%"><?php echo $estudiante["cod_carrera"] ?></td>
                            <th width="8%">Trayecto:</th>
                            <td width="8%"><?php echo $estudiante["trayecto"] ?></td>
                            <th width="8%">Trimestre:</th>
                            <td width="9%"><?php echo $estudiante["trimestre"] ?></td>
                            <th width="8%">Seccion:</th>
                            <td width="8%"><?php echo $estudiante["seccion"] ?></td>
                            <th width="8%">Turno:</th>
                            <td width="8%"><?php echo $estudiante["turno"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="20%">Lugar de nacimiento:</th>
                            <td width="20%"><?php echo $estudiante["edo_nac"] ?></td>                                
                            <th width="20%"></th>
                            <th width="20%">Fecha de Nacimiento:</th>
                            <td width="20%"><?php echo $estudiante["fec_nac"] ?></td>
                            
                        </tr>
                    </table>
                </div>   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="15%">Teléfono Hab:</th>
                            <td width="15%"><?php echo $estudiante["tel_hab"] ?></td>  
                            <th width="10%">Celular:</th>
                            <td width="20%"><?php echo $estudiante["celular"] ?></td>
                            <th width="20%">Psicologo. Tratante:</th>
                            <td width="20%"><?php echo $cita["Responsable"] ?></td>
                            
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 20px;">
                    <h4>Observaciones</h4>

                    <p align="justify">&nbsp;&nbsp;&nbsp;<?php echo $cita["Descripcion"]?> </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-4 col-xs-offset-4 col-md-4 col-md-offset-4" style="margin-top: 50px;">
                    <table class="col-md-12 tablatdborder">                           
                        <tr>
                            <td width="50%"></td> 
                        </tr>
                        <tr>
                            <th style=" text-align: center !important;">Firma del Doctor</th> 
                        </tr>
                    </table>
                </div>
            </div>


		</div>
    </div> 	
</div>

<div class="modal-footer">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-xl-12" align="right">
            <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
        </div>  
    </div>
</div>






