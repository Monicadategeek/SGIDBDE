<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
    <a class="btn btn-default btn-sm buttons-pdf" href="#" onclick="tomarcaptura('contenidomodalver .modal-body');" style="float: left;"><i class="fa fa-print"></i> Imprimir</a>

  <h4 class="modal-title" style="text-align: center;">Ver <?php echo $cita["tipoInforme"]?></h4>
</div>
<div class="modal-body row">
	<div class="col-xs-12 col-md-12 col-xl-12 row">
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="<?php include('../views/logo.php');?>" style="width: 50%;"></div>
        <div class="col-xs-8 col-md-8 col-xl-8" style="text-align: center;">REPÚBLICA BOLIVARIANA DE VENEZUELA<br>
        MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN SUPERIOR<br>
        INSTITUTO UNIVERSITARIO DE TECNOLIGIA DEL OESTE<br> 
        "MARISCAL SUCRE"<br>
        Caracas.
        </div>
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="../web/images/mppes.png" style="width: 50%;" ></div>
		<div class="col-xs-12 col-md-12 col-xl-12">
        <br><br>
            <h5 align="center"><u>ESTUDIO SOCIOACADÉMICO PARA LA ASIGNACIÓN DE AYUDAS ECONÓMICAS </u></h5>            
            <br>
        
            <h5 align="left">1.- IDENTIFICACIÓN DEL ESTUDIANTE:</h5>

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <table class="col-xs-12 col-md-12 tablatdborder">
                        <tr>
                            <th width="20%">Nombre y Apellido:</th>
                            <td width="30%"><?php echo $estudiante["nombres"]." ".$estudiante["apellidos"]?></td>
                            <th width="10%">Nacionalidad:</th>
                            <td width="10%"><?php echo $estudiante["nacionalidad"]?></td>
                            <th width="20%">Cédula de Identidad:</th>
                            <td width="10%"><?php echo $estudiante["cedula"]?></td>
                            
                        </tr>
                    </table>
                </div>
            </div> 

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <table class="col-xs-12 col-md-12 tablatdborder">
                        <tr>
                            <th width="7%">Sexo:</th>
                            <td width="7%"><?php echo $estudiante["sexo"]?></td>
                            <th width="15%">Estado Civil:</th>
                            <td width="10%"><?php echo $estudiante["edo_civil"] ?></td>
                            <th width="6%">Edad:</th>
                            <td width="6%"><?php echo $estudiante["edad"] ?></td>  
                            <th width="15%">Teléfono Hab:</th>
                            <td width="10%"><?php echo $estudiante["tel_hab"] ?></td>  
                            <th width="9%">Celular:</th>
                            <td width="10%"><?php echo $estudiante["celular"] ?></td>
                            <th width="5%"></th>
                        </tr>
                    </table>
                </div>
            </div>  

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="10%">Dirección:</th>
                            <td width="90%"><div class="border-bottom"><?php echo $estudiante["direccion"]?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="20%">Correo Electrónico:</th>
                            <td width="80%"><?php echo $estudiante["direccion"]?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <h5 align="left">2.- ÁREA ACADÉMICA:</h5>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="25%">2.1 Titulo de Bachiller en:</th>
                            <td width="20%"><?php if (isset($datosacademicos["mencion"])) { echo $datosacademicos["mencion"];}?></td> 
                            <th width="45%">Tipo de Institución donde cursó el Bachillerato:</th>
                            <td width="10%"><?php if (isset($datosacademicos["tipo_inst"])) { echo $datosacademicos["tipo_inst"];}?></td>
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="40%">2.2 Promedio de Notas del Bachillerato:</th>
                            <td width="10%"><?php if (isset($datosacademicos["prom_notas_bach"])) { echo $datosacademicos["prom_notas_bach"];}?></td> 
                            <th width="50%"></th>
                        </tr>
                    </table>
                </div>                   
            </div>


            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="35%">2.3 Carrera que cursa en el IUTOMS:</th>
                            <td width="65%"><?php echo $estudiante["cod_carrera"] ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="35%">Que le motivo a escoger esta carrera </th>
                            <td width="65%"><?php if (isset($datosacademicos["motivo_pnf"])) { echo $datosacademicos["motivo_pnf"];}?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="35%">¿Se siente identificado con el PNF? </th>
                            <td width="65%"><?php if (isset($datosacademicos["identidad_pnf"])) { echo $datosacademicos["identidad_pnf"];}?></td>
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="55%">¿Le gusta el Programa Nacional de Formación de su carrera? </th>
                            <td width="45%"><?php if (isset($datosacademicos["te_gusta"])) { echo $datosacademicos["te_gusta"];}?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="10%">2.4 Turno</th>
                            <td width="10%"><?php echo $estudiante["turno"] ?></td>
                            <th width="15%">N° Expediente</th>
                            <td width="10%"><?php echo $estudiante["cedula"] ?></td>
                            <th width="17%">Fecha de Ingreso</th>
                            <td width="10%"><?php echo $estudiante["fec_ingreso"] ?></td>                      
                            <th width="17%">Forma de Ingreso</th>
                            <td width="10%"><?php echo $estudiante["forma_ingreso"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="15%">2.5 Trayecto</th>
                            <td width="10%"><?php echo $estudiante["trayecto"] ?></td>
                            <th width="15%">Trimestre</th>
                            <td width="10%"><?php echo $estudiante["trimestre"] ?></td>
                            <th width="20%">Indice Académico</th>
                            <td width="10%"><?php echo $estudiante["creditos"] ?></td>  
                            <th width="19%"></th> 
                        </tr>
                    </table>
                </div>                   
            </div>

            <h5 align="left">3.- AREA SOCIOECONOMICA DEL ESTUDIANTE:</h5>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="12%">3.1 Trabaja</th>
                            <td width="5%"><?php echo $datossocioeco["Trabaja"] ?></td>  
                            <th width="12%">Ha trabajado</th>
                            <td width="5%"><?php echo $datossocioeco["HaTrabajado"] ?></td>
                            <th width="25%">3.2 Nombre de la empresa</th>
                            <td width="20%"><?php echo $datossocioeco["nombre_trabajo"] ?></td>
                            <th width="5%">Cargo</th>
                            <td width="15%"><?php echo $datossocioeco["cargo_trabajo"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="15%">3.3 En donde trabaja</th>
                            <td width="35%"><?php echo $datossocioeco["Trabaja"] ?></td>  
                            <th width="10%">3.4 Cuanto gana</th>
                            <td width="15%"><?php echo $datossocioeco["HaTrabajado"] ?></td>
                            <th width="10%">Horario</th>
                            <td width="15%"><?php echo $datossocioeco["nombre_trabajo"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>           

            <?php
            if ($datossocioeco["Trabaja"]=='NO' and $datossocioeco["HaTrabajado"]=='SI') {
              echo '<div class="row">
                        <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                            <table class="col-xs-12 col-md-12 tablatdborder">                           
                                <tr>
                                    <th width="30%">Razón del retiro del trabajo</th>
                                    <td width="70%">'.$datossocioeco["RazRetTrab"].'</td>  
                                </tr>
                            </table>
                        </div>                   
                    </div>';
            }
            ?>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="45%">3.5 Presenta problemas para trasladarse al IUTOMS</th>
                            <td width="10%"><?php echo $datossocioeco["problemas_traslado"] ?></td> 
                            <th width="30%">¿Como se traslada al IUTOMS?</th>
                            <td width="15%"><?php echo $datossocioeco["tipo_traslado_iutoms"] ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div> 

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">3.6 Consume las tres comidas:</th>
                            <td width="10%"><?php echo $datossocioeco["tres_comidas"] ?></td> 
                            <th width="25%">De ser negativo porqué</th>
                            <td width="35%"><?php echo $datossocioeco["tres_comidas_neg"] ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div> 

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">3.7 Recibes ayuda económica:</th>
                            <td width="10%"><?php echo $datossocioeco["ayudaseconomica"] ?></td> 
                            <th width="35%">De ser afirmativo quien te ayuda:</th>
                            <td width="25%"></td> 
                        </tr>
                    </table>
                </div>                   
            </div>

            <?php 
              if (count($ayudaseconomica)>0) {
            ?>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="table table-bordered table-hover">
                            <tr>
                              <th>Origen de la Ayuda</th>
                              <th>Monto en Bs</th>
                            </tr>
                            <?php
                            foreach ($ayudaseconomica as $ayudaeconomica) {
                                echo "<tr>
                                <td>".$ayudaeconomica["ayud_econ"]."</td>
                                <td>".$ayudaeconomica["monto"]."</td>
                                </tr>"  ;
                            }?>
                        </table>
                    </div>                   
                </div>
            <?php
              }
            ?>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="15%">3.8 Tienes hijos:</th>
                            <td width="5%"><?php echo $datossocioeco["Thijos"] ?></td>                            
                             <?php
                              if ($datossocioeco["hijos"]>0) {
                                echo '<th width="10%">Cuantos:</th>
                                        <td width="5%">'.$datossocioeco["hijos"].'</td>
                                        <th width="15%">Viven contigo:</th>
                                        <td width="45%">'.$datossocioeco["viven_contigo"].'</td>';
                              }
                              else{
                                echo '<th width="80%"></th> ';
                              }
                            ?>
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="15%">Tienes pareja:</th>
                            <td width="5%"><?php echo $datossocioeco["pareja"] ?></td>   
                            <th width="30%">Alguna persona depende de Usted:</th>    
                            <td width="5%"><?php echo $datossocioeco["pers_dep"] ?></td>  
                                 
                             <?php
                              if ($datossocioeco["pers_dep_nro"]>0) {
                                echo '<th width="25%">De ser positivo cuantas:</th>        
                                    <td width="5%">'.'$datossocioeco["pers_dep_nro"]'.'</td> 
                                    <th width="15%"></th>        ';
                              }
                              else{
                                echo '<th width="45%"></th> ';
                              }
                            ?>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <label>3.9 Grupo Familiar con quien reside actualmente: </label>
                    <table class="table table-bordered" cellspacing="0" width="100%" id="familias"> 
                        <thead>
                            <th width="5%">N°</th>
                            <th>Parentesto</th>
                            <th>Apellidos</th>
                            <th>Nombres</th>
                            <th>Fecha de Nacimiento</th>
                            <th>Edo. Civil</th>
                            <th>Genero</th>
                            <th>Nivel Instrucción</th>
                            <th>Profesión u Oficio</th>
                            <th>Ingreso Mensual</th>
                        </thead>
        
                        <tbody>
                            <?php 
                                if (is_array($familiares) || is_object($familiares))
                                {
                                    $count=0;
                                    $total=0;
                                    foreach ($familiares as $familia)
                                    {   
                                        $count=$count+1;
                                        $total=$total+$familia["ingreso_mensual_num"];
                                        echo "<tr>
                                        <td>".$count."</td>
                                        <td>".$familia["id_parentesco"]."</td>
                                        <td>".$familia["apellidos"]."</td>
                                        <td>".$familia["nombres"]."</td>
                                        <td>".$familia["fec_nac"]."</td>
                                        <td>".$familia["id_edo_civ"]."</td>
                                        <td>".$familia["genero"]."</td>
                                        <td>".$familia["id_grado_instr"]."</td>
                                        <td>".$familia["id_prof_ocup"]."</td>
                                        <td style='text-align: right !important;'>".$familia["ingreso_mensual"]."</td>";                                             
                                        echo "</tr>";
                                    }
                                    echo "<tr><td colspan='9' style='text-align: right !important;'>Total</td><td>".number_format($total, 2, ',', '.')."</td></tr>";
                                }
                            ?>
                            <tr><th colspan="2">Observación</th><td colspan="8"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["grupofamiliar"];}?></td></tr>
                        </tbody>
                    </table> 
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="35%">Total Aprox Ingreso del Estudiante (Mensual)</th>
                            <td width="15%"><?php echo $datossocioeco["TotIngEst"] ?></td> 
                            <th width="35%">Total de Egresos del Estudiante (Mensual)</th>
                            <td width="15%"><?php echo $datossocioeco["TotIngFam"] ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <label align="left">EGRESOS MENSUALES DEL GRUPO INDIVIDUAL</label><br><br>
                    <div class="form-group col-xs-4 col-md-4 col-xl-4">
                        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                            <thead>
                                <th>Concepto</th>
                                <th width="50%">Bolivares</th>                    
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($gastosindividuales as $gasto) {
                                    echo '<tr>
                                    <td>'.$gasto["Descripcion"].'</td><td style="text-align: right !important;">';

                                    if ($gasto["Descripcion"]=='TOTAL') {
                                        echo $gasto["monto"];
                                    }
                                    else{
                                        echo $gasto["monto"];
                                    }
                                    echo "</td>
                                    </tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group col-xs-8 col-md-8 col-xl-8">
                        <label for="consideracion">OBSERVACIÓN:</label>
                        <p align="justify"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["OBSegrind"];}?></p>
                    </div>

                </div>                   
            </div>


            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <label align="left">EGRESOS MENSUALES DEL GRUPO FAMILIAR</label><br><br>
                    <div class="form-group col-xs-4 col-md-4 col-xl-4">
                        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                            <thead>
                                <th>Concepto</th>
                                <th width="50%">Bolivares</th>                    
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($gastosfamiliares as $gasto) {
                                    echo '<tr>
                                    <td>'.$gasto["Descripcion"].'</td><td style="text-align: right !important;">';

                                    if ($gasto["Descripcion"]=='TOTAL') {
                                        echo $gasto["monto"];
                                    }
                                    else{
                                        echo $gasto["monto"];
                                    }
                                    echo "</td>
                                    </tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group col-xs-8 col-md-8 col-xl-8">
                        <label for="consideracion">OBSERVACIÓN:</label>
                        <p align="justify"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["OBSegrfam"];}?></p>
                    </div>

                </div>                   
            </div>

            <h5 align="left">4.- ÁREA FISICO - AMBIENTAL:</h5>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <label>4.1 Identificación de la comunidad donde resides</label>
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="10%">Estado:</th>
                            <td width="15%"><?php echo $datosambientales["estado"] ?></td> 
                            <th width="10%">Municipio:</th>
                            <td width="15%"><?php echo $datosambientales["municipio"] ?></td>
                            <th width="10%">Parroquia:</th>
                            <td width="15%"><?php echo $datosambientales["parroquia"] ?></td> 
                            <th width="10%">Ciudad:</th>
                            <td width="15%"><?php echo $datosambientales["ciudad"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>


            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="16%">4.2 Tipo de Vivienda:</th>
                            <td width="15%"><?php echo $datosambientales["tip_viv"] ?></td> 
                            <th width="22%">4.3 Tenencia de la Vivienda:</th>
                            <td width="15%"><?php echo $datosambientales["ten_viv"] ?></td>
                            <th width="16%">4.4 Construcción:</th>
                            <td width="17%"><?php echo $datosambientales["constr_viv"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <label>4.5 Características de la Vivienda</label>
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="17%">Tipo de Pared:</th>
                            <td width="16%"><?php echo $datosambientales["tipo_pared"] ?></td> 
                            <th width="17%">Tipo de Piso:</th>
                            <td width="16%"><?php echo $datosambientales["tipo_piso"] ?></td>
                            <th width="17%">Tipo de Techo:</th>
                            <td width="16%"><?php echo $datosambientales["tipo_techo"] ?></td>
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <div class="form-group col-xs-2 col-md-3 col-xl-3">
                        <label>Espacios que componen su vivienda</label>
                        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                            <tbody>
                                <tr>
                                    <th width="50%">Sala</th>
                                    <td><?php echo $datosambientales["sala"]?></td>
                                </tr>
                                <tr>
                                    <th>Cocina</th>
                                    <td><?php echo $datosambientales["cocina"]?></td>
                                </tr>
                                <tr>
                                    <th>Baños</th>
                                    <td><?php echo $datosambientales["banios"]?></td>
                                </tr>
                                <tr>
                                    <th>Dormitorios</th>
                                    <td><?php echo $datosambientales["dormitorios"]?></td>
                                </tr> 
                            </tbody>
                            
                        </table>      
                    </div>
                    <div class="form-group col-xs-3 col-md-3 col-xl-3">
                        <label for="identidad_pnf">Servicios publicos con los que cuenta su vivienda su vivienda</label>
                        <table border class="table table-bordered table-hover col-xs-12 col-md-12 col-xl-12">
                            <tbody>
                                <tr>
                                    <th width="50%">Luz</th>
                                    <td><?php echo $datosambientales["luz"]?></td>
                                </tr>
                                <tr>
                                    <th>Agua</th>
                                    <td><?php echo $datosambientales["agua"]?></td>
                                </tr>
                                <tr>
                                    <th>Aseo Urbano</th>
                                    <td><?php echo $datosambientales["aseo_urb"]?></td>
                                </tr>
                                <tr>
                                    <th>Telefono Local</th>
                                    <td><?php echo $datosambientales["telf_local"]?></td>
                                </tr> 
                            </tbody>                            
                        </table>                
                    </div>
                    <div class="form-group col-xs-7 col-md-6 col-xl-6">
                        <label for="consideracion">Distribución de las Habitaciones:</label>
                        <p align="justify"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["dtrbHabitaciones"];}?></p>
                    </div>
                    <div class="form-group col-xs-12 col-md-12 col-xl-12">
                        <label for="consideracion">OBSERVACIONES:</label>
                        <p align="justify"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["areaficicoambiental"];}?></p>
                    </div>
                </div>                   
            </div>

			<br><br>
            <h5 align="left">5.- ÁREA CULTURAL - DEPORTIVA:</h5>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">5.1 ¿En que Ocupas tu Tiempo Libre?:</th>
                            <td width="70%"><?php if (count($actividadesest)>0) { echo $actividadesest[0]["actividad"]; }  ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">5.2 ¿Practicas algun deporte?:</th>
                            <td width="70%"><?php if (count($deportesest)>0) { echo $deportesest[0]["deporte"]; }  ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">5.3 ¿Posees Habilidades Artísticas?:</th>
                            <td width="70%"><?php if (count($habilidadesest)>0) { echo $habilidadesest[0]["habilidad"]; }  ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    <table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="30%">5.4 ¿Has Participado en alguna actividad del IUTOMS?:</th>
                            <td width="70%"><?php if (count($eventosest)>0) { echo $eventosest[0]["evento"]; }  ?></td> 
                        </tr>
                    </table>
                </div>                   
            </div>

			<br>
            <h5 align="left">6.- ÁREA DE INTERES PARA EL ESTUDIANTE:</h5>
		
            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                	<table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th>6.1 Por qué considera que requiere alguna de las ayudas socioeconómicas que brinda el IUTOMS:</th>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><?php echo $datosinteressocioeconomico["consideracion"];?></td> 
                        </tr>
                    </table>                  
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                	<table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th width="45%">6.2 Modalidad de ayuda Socioeconómica que usted requiere:</th>
                            <td width="15%" style="text-align: left;"><?php echo $datosinteressocioeconomico["tipoayuda"];?></td>
                            <th width="50%"></th>
                        </tr>
                    </table>                    
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                	<table class="col-xs-12 col-md-12 tablatdborder">                           
                        <tr>
                            <th colspan="2">6.3 En caso de quedar en la Ayudantia o la Preparaduria esta de acuerdo en cumplir las horas que se piden para esta modalidad de Ayuda Socioeconómica: <u><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["prepayu"];}?></u></th>
                            
                        </tr>
                    </table>                    
                </div>                   
            </div>


            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                	<div class="col-xs-12 col-md-12">
                		<p align="justify">Declaro que la información y datos suministrados en este estudio son verdaderos y autorizo la comprobación de los mismos. De comprobarse que ha incurrido en falsedad esto, dará motivo a la anulación de la solicitud de la ayuda socioeconómica</p>
                	</div>                  
                </div>                   
            </div>


            <br>

            <h5>7.- SOLO PARA EL USO DEL (LA) TRABAJADOR (A) SOCIAL</h5>   
			<div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                	<div class="col-xs-12 col-md-12">
                		<label for="consideracion">7.1 Diagnostico Social:</label>
                		<p align="justify"><?php if (isset($estudio) and count($estudio)>0){ echo $estudio["diagnosticosocial"];}?> <?php echo $estudio["diagnosticosocial"];?></p>
                	</div>                  
                </div>                   
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                    
            	   <div class="col-xs-8 col-md-8 col-xl-8 col-xs-offset-2 col-md-offset-2">
            		
                        <table border class="table table-bordered">                 
                            <tbody>
                                <tr>
                                    <td width="70%"><label>7.2 Urbanización social segun la escala de Graffar</label></td>
                                    <td>
                                        <?php echo $estudio["usseg"];?> 
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>7.2.1 Institución del responsable económico</label></td>
                                    <td>
                                        <?php echo $estudio["ireconomico"];?>   
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>7.2.2 Ocupación del responsable económico</label></td>
                                    <td>
                                        <?php echo $estudio["oreconomico"];?>                                   
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>7.2.3 Fuentes de ingreso del responsable económico</label></td>
                                    <td>
                                        <?php echo $estudio["fireconomico"];?>                                      
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>7.2.4 Condiciones de la vivienda de los padres</label></td>
                                    <td>
                                        <?php echo $estudio["cvpadres"];?>                              
                                    </td>
                                </tr>


                                <tr>
                                    <td><label>7.2.5 Ubicación de la vivienda de los padres</label></td>
                                    <td>
                                        <?php echo $estudio["uvpadres"];?>                                      
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2"><label>7.3 Conclusiones y Recomendaciones</label></td>
                                    
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php echo $estudio["conclurecomen"];?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>       
                    </div>
                	                 
                </div>                   
            </div>


		</div>
    </div> 	
</div>

<div class="modal-footer">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-xl-12" align="right">
            <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
        </div>  
    </div>
</div>






