<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <a class="btn btn-default btn-sm buttons-pdf" href="#" onclick="tomarcaptura('contenidomodalver .modal-body');" style="float: left;"><i class="fa fa-print"></i> Imprimir</a>
  <h4 class="modal-title" style="text-align: center;">Ver <?php echo $cita["tipoInforme"]?></h4>

</div>
<div class="modal-body row">
	<div class="col-xs-12 col-md-12 col-xl-12 row">
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="<?php include('../views/logo.php');?>" style="width: 50%;"></div>
        <div class="col-xs-8 col-md-8 col-xl-8" style="text-align: center;">REPÚBLICA BOLIVARIANA DE VENEZUELA<br>
        MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN SUPERIOR<br>
        INSTITUTO UNIVERSITARIO DE TECNOLIGIA DEL OESTE<br> 
        "MARISCAL SUCRE"<br>
        Caracas.
        </div>
        <div class="col-xs-2 col-md-2 col-xl-2" align="center"><img src="../web/images/mppes.png" style="width: 50%;" ></div>
		<div class="col-xs-12 col-md-12 col-xl-12">
        <br><br>
            
                <div class="row">
                    <div class="col-xs-4 col-md-4">
			             <label>Fecha: </label><div class="border-bottom"><?php echo $cita["Fecha"]?></div>
                    </div>
                </div>              

                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <table class="col-xs-12 col-md-12 tablatdborder">
                            <tr>
                                <th width="20%">Nombre y Apellido:</th>
                                <td width="45%"><?php echo $estudiante["nombres"]." ".$estudiante["apellidos"]?></td>
                                <th width="7%">Edad:</th>
                                <td width="10%"><?php echo $estudiante["edad"]?></td>
                                <th width="18%" style=" text-align: center !important;" ><?php echo $estudiante["sexo"]?></th>

                            </tr>
                        </table>
                    </div>
                    
                </div>  

                <div class="row">
                   <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="5%">C.I.:</th>
                                <td width="10%"><?php echo $estudiante["cedula"]?></td>

                                <th width="10%">Dirección:</th>
                                <td width="75%" colspan="2"><div class="border-bottom"><?php echo $estudiante["direccion"]?></td>

                            </tr>
                        </table>
                    </div>                   
                </div>
                <div class="row">
                     <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="10%">Carrera:</th>
                                <td width="25%"><?php echo $estudiante["cod_carrera"] ?></td>
                                <th width="8%">Trayecto:</th>
                                <td width="8%"><?php echo $estudiante["trayecto"] ?></td>
                                <th width="8%">Trimestre:</th>
                                <td width="9%"><?php echo $estudiante["trimestre"] ?></td>
                                <th width="8%">Seccion:</th>
                                <td width="8%"><?php echo $estudiante["seccion"] ?></td>
                                <th width="8%">Turno:</th>
                                <td width="8%"><?php echo $estudiante["turno"] ?></td>
                            </tr>
                        </table>
                    </div>                   
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="20%">Lugar de nacimiento:</th>
                                <td width="20%"><?php echo $estudiante["edo_nac"] ?></td>                                
                                <th width="20%"></th>
                                <th width="20%">Fecha de Nacimiento:</th>
                                <td width="20%"><?php echo $estudiante["fec_nac"] ?></td>
                                
                            </tr>
                        </table>
                    </div>   
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="15%">Teléfono Hab:</th>
                                <td width="15%"><?php echo $estudiante["tel_hab"] ?></td>  
                                <th width="10%">Celular:</th>
                                <td width="20%"><?php echo $estudiante["celular"] ?></td>
                                <th width="20%">Odont. Tratante:</th>
                                <td width="20%"><?php echo $cita["Responsable"] ?></td>
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="30%">Antecedentes familiares:</th>
                                <td width="70%"><?php echo $EncOdont["AntFami"]?></td>  
                                
                                
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="20%">Motivo de consulta: </th>
                                <td width="20%"><?php echo $EncOdont["MotCons"]?></td>  
                                <th width="6%">Dolor: </th>
                                <td width="24%"><?php echo $EncOdont["Dolor"]?></td>  
                                <th width="6%">Control: </th>
                                <td width="24%"><?php echo $EncOdont["Control"]?></td>  
                            </tr>
                        </table>
                    </div>                   
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="25%">Sangramiento en las encias: </th>
                                <td width="10%"><?php echo $EncOdont["SangEnc"]?></td>  
                                <th width="19%">Restauración protésica: </th>
                                <td width="15%"><?php echo $EncOdont["RestProt"]?></td>  
                                <th width="14%">Otro indique cual: </th>
                                <td width="20%"><?php echo $EncOdont["Otro"]?></td>  
                            </tr>
                        </table>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px; margin-bottom: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="20%">Enfermedad actual:</th>
                                <td width="80%"><?php echo $EncOdont["EnferAct"]?></td>  
                                
                                
                            </tr>
                        </table>
                    </div>
                </div>
		</div>

		<div class="col-xs-6 col-md-6 col-xl-6">
			<table class="table table-bordered table-hover">
        		<tbody>
                <?php 
                $count=0;
                    foreach ($PreguntasOdontologicas as $pregunta) {
                    	$count=$count+1;
                        if ($count<=$mitad) {
                        	echo "<tr>
                        		<td>".$pregunta["Descripcion"]."</td> <td>";

                            if ($pregunta["tiporesp"]==1) {
                                echo $pregunta["respuesta"];
                            }
                            else if ($pregunta["tiporesp"]==2) {
                                 echo $pregunta["respuesta"].' '.$pregunta["nro"];
                            }

                            
                            echo "</td></tr>";
                        }
                    }
                ?>
                </tbody>
            </table>							
		</div>

		<div class="col-xs-6 col-md-6 col-xl-6">
			<table class="table table-bordered table-hover ">
        		<tbody>
                <?php 
                $count=0;
                    foreach ($PreguntasOdontologicas as $pregunta) {
                    	$count=$count+1;
                        if ($count>$mitad) {
                        	echo "<tr>
                        			<td>".$pregunta["Descripcion"]."</td> <td>";

                            if ($pregunta["tiporesp"]==1) {
                                echo $pregunta["respuesta"];
                            }
                            else if ($pregunta["tiporesp"]==2) {
                                 echo $pregunta["respuesta"].' '.$pregunta["nro"];
                            }

                            
                            echo "</td></tr>";
                        }
                    }
                ?>
                </tbody>
            </table>							
		</div>	

		
		<div class="col-xs-12 col-md-12 col-xl-12">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 11px;">
                        <table class="col-md-12 tablatdborder">                           
                            <tr>
                                <th width="15%">Aspecto del Paciente: </th>
                                <td width="11%"><?php echo $informe["AspPac"]?></td>  
                                <th width="3%">Cara: </th>
                                <td width="11%"><?php echo $informe["Cara"]?></td>  
                                <th width="18%">Labios y Comisura: </th>
                                <td width="11%"><?php echo $informe["LabyCom"]?></td>  
                                <th width="20%">Palpación de ganglios: </th>
                                <td width="8%"><?php echo $informe["PalpaGangl"]?></td>  
                            </tr>
                        </table>
                    </div>     
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-md-12 tablatdborder">                           
                            <tr>
                                <th width="7%">Ganglios: </th>
                                <td width="15%"><?php echo $informe["Ganglios"]?></td>  
                                <th width="13%">ATM (articulación temporamandibular): </th>
                                <td width="15%"><?php echo $informe["ATM"]?></td>  
                                <th width="8%">Ojeras: </th>
                                <td width="15%"><?php echo $informe["Ojeras"]?></td>  
                                <th width="15%%">Región hipodea y tiroidea: </th>
                                <td width="12%"><?php echo $informe["RegHiotTiroidea"]?></td>  
                            </tr>
                        </table>
                    </div>                     
                </div>

            <br>
			<h4>EXAMEN CLÍNICO INTRABUCAL:</h4>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-xs-12 col-md-12 tablatdborder">                           
                            <tr>
                                <th width="7%">Carrillos: </th>
                                <td width="13%"><?php echo $informe["Carrillos"]?></td>  
                                <th width="7%">Mucosa: </th>
                                <td width="13%"><?php echo $informe["Mucosa"]?></td>  
                                <th width="6%">Encia: </th>
                                <td width="14%"><?php echo $informe["Encia"]?></td>  
                                <th width="6%">Lengua: </th>
                                <td width="14%"><?php echo $informe["Lengua"]?></td> 
                                <th width="5%">Paladar: </th>
                                <td width="15%"><?php echo $informe["Paladar"]?></td>  
                            </tr>
                        </table>
                    </div>  
                </div>
            <br>
			<h4>EXAMENES COMPLEMENTARIOS</h4>
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <table class="col-md-12 tablatdborder">                           
                            <tr>
                                <th width="3%">Rx: </th>
                                <td width="19%"><?php echo $informe["examen"]?></td>  
                                <th width="5%">Laboratorio: </th>
                                <td width="19%"><?php echo $informe["Laboratorio"]?></td>  
                                <th width="4%">Modelo: </th>
                                <td width="19%"><?php echo $informe["Modelo"]?></td>  
                                <th width="15%">Tensión arterial: </th>
                                <td width="15%"><?php echo $informe["TensionArterial"]?></td>   
                            </tr>
                        </table>
                    </div>  
                </div>
                 <div class="row">
                     <div class="col-xs-12 col-md-12" style="margin-top: 11px;">
                        <label>Observaciones</label>
                        <p align="justify">&nbsp;&nbsp;&nbsp;<?php echo $informe["Observaciones"]?> </p>
                        
                    </div>
                </div>

            <br>
            <div class="col-xs-12 col-md-12 col-xl-12">
                <h4>ODONTODIAGRAMA</h4>
                <div id="odontogramageneral">
                    <?php echo $informe["odontograma"]?>
                </div>
            </div>
            <br>
			<h4>Diagnostico</h4>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <p align="justify">&nbsp;&nbsp;&nbsp;<?php echo $informe["Diagnostico"]?> </p>
                       
                    </div>
                </div>
            <br>
			<h4>Plan de Tratamiento</h4>
            <div class="row">
                 <div class="col-xs-12 col-md-12">
                    <p align="justify">&nbsp;&nbsp;&nbsp;<?php echo $informe["Tratamiento"]?> </p>
                </div>
            </div>

		</div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="col-xs-4 col-md-4 col-xs-offset-1 col-md-offset-1" style="margin-top: 50px;">

                    <table class="col-md-12 tablatdborder">                           
                        <tr>
                            <td width="50%"></td> 
                        </tr>
                        <tr>
                            <th style=" text-align: center !important;">Firma del Doctor</th> 
                        </tr>
                    </table>
                </div>
                <div class="col-xs-4 col-md-4 col-xs-offset-2 col-md-offset-2" style="margin-top: 50px;">

                    <table class="col-md-12 tablatdborder">                           
                        <tr>
                            <td width="50%"></td> 
                        </tr>
                        <tr>
                            <th style=" text-align: center !important;">Firma del Paciente</th> 
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>

	</div> 
	
</div>

<div class="modal-footer">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-xl-12" align="right">
            <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
        </div>  
    </div>
</div>




<script type="text/javascript">
    $('#botones').hide();
</script>

