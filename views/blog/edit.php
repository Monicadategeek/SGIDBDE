<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Modificación de Articulo</h4>
</div>
<div class="modal-body row" id="formulario">

   <form id="agregar" action="../Controller/articulocontroller.php" method="POST">
    <input type="hidden" name="accion" id="accion" value="update">
    <input type="hidden" name="id" id="id" value="<?php echo $articulo['Id'];?>">
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="titulo">Titulo</label>
        <input type="text" name="titulo" id="titulo" class="form-control" required="" value="<?php echo $articulo['Titulo'];?>">
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="subtitulo">Subtitulo</label>
        <input type="text" name="subtitulo" id="subtitulo" class="form-control" required="" title="Ingrese el subtitulo del articulo" value="<?php echo $articulo['Subtitulo']; ?>">
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="tags">Tags</label>
         <select id="tags" name="tags[]" class="form-control selectpicker" title="-Seleccione-" required multiple="">
          <?php 

          $cont=0;
          if (is_array($Tags) || is_object($Tags))
          {
            foreach ($Tags as $Tag)
            {
              foreach ($tagxblog as $value) {
                if ($Tag["Id"]==$value["Tag"]) {
                  echo "<option value='".$Tag["Id"]."' selected>".$Tag["Nombre"]."</option>";
                  $cont=1;
                  break;
                }
                
              }

              if ($cont==0) {
                echo "<option value='".$Tag["Id"]."'>".$Tag["Nombre"]."</option>";
              }
              else{
                $cont=0;
              }
              
            }
          }
          ?>
        </select>
      </div>

      <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="form-group col-xs-12 col-md-6 col-xl-6">
          <label class="control-label" for="imagenprincipal">Imagen Principal</label>
          <input type="file" name="imagenprincipal" id="imagenprincipal" class="form-control"  title="Ingrese la imagen principal del articulo">
          <p>Si no desea modificarla no complete el campo</p>
        </div>
        <div class="form-group col-xs-12 col-md-6 col-xl-6">
          <label class="control-label" for="DptoAsociado">Asociado al Departamento</label>
        <select id="DptoAsociado" name="DptoAsociado" class="form-control selectpicker" title="-Seleccione-" required>
          <?php 
          if (is_array($departamentos) || is_object($departamentos))
          {
            foreach ($departamentos as $departamento)
            {
                if ($articulo["DptoAsociado"]==$departamento["IdDepartamento"]) {
                  echo "<option value='".$departamento["IdDepartamento"]."' selected>".$departamento["Descripcion"]."</option>";
                }
                else{
                  echo "<option value='".$departamento["IdDepartamento"]."'>".$departamento["Descripcion"]."</option>";
                }
                 
            }
          }
          ?>
        </select>
        </div>
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="titulo">Contenido</label>
        <textarea id="articuloeditor" name="articuloeditor" class="ckeditor" rows="10" cols="80" required="" title="Ingrese el contenido del articulo"><?php echo $articulo['Contenido']; ?></textarea>
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="AsistenciEstud">Desea que su articulo sea:</label>
        <?php 
          if ($articulo["Estado"]=='0') {
            echo '<div class="radio">
            <label>
                <input type="radio" name="Estado" id="estado1" value="1" required >Publicado 
            </label>
            <label>
                <input type="radio" name="Estado"  id="estado2" value="0"  checked >No Publicado
            </label>
        </div>';
          }
          else{
            echo '<div class="radio">
            <label>
                <input type="radio" name="Estado" id="estado1" value="1" required checked>Publicado 
            </label>
            <label>
                <input type="radio" name="Estado"  id="estado2" value="0"   >No Publicado
            </label>
        </div>';
          }
        ?>
      </div>
      <div class="col-xs-12 col-md-12 col-xl-12" align="center">
        <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
        <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
      </div>
    </form>
</div>


<script type="text/javascript">
CKEDITOR.replace( 'articuloeditor' );
                $('textarea.ckeditor').each(function () {
                   var $textarea = $(this);
                   $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
                });
$(document).ready(function(){
  
    $('.selectpicker').selectpicker('refresh');
     $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es',
        getStartDate :'+1d'
        });
  });


  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    
    var parametros= new FormData($(this)[0]);
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: parametros,
        contentType:false,
        processData:false,
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/articulocontroller.php?accion=index');
              $('#modificar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha modificado el registro del articulo');
                      }
            else{
              notificacion(2, 'fa fa-times-circle','Error!','Ya existe ese trabajador en ese departamento con esas características');
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>