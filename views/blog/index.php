     <!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Blog
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Listado de Articulos</h2>
            <div align="right">
                <button type="button" class="btn btn-primary btn-sm"  onclick="nuevo('../Controller/articuloController.php?accion=create')" ><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Registro</button>
                
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            

            <table class="table table-striped table-bordered " cellspacing="0" width="100%" id="articulos"> 
               <thead>
                    <th width="5%">Id</th>
                    <th width="10%">Titulo</th>
                    <th width="10%">Subtitulo</th>
                    <th width="20%">Imagen <br>Principal</th>
                    <th width="10%">Depto Asociado</th>
                    <th width="10%">Fecha<br> Registro</th>
                    <th width="10%">Responable</th>
                    <th width="10%">Estado</th>
                    <th width="10%">Acciones</th>

                </thead>
                <tfoot>
                    <td></td>
                    <th>Titulo</th>
                    <th>Subtitulo</th>
                    <td></td>
                    <th>Depto Asociado</th>
                    <th>Fecha Registro </th>
                    <th>Responable</th>
                    <th>Estado</th>
                    <td></td>
                </tfoot>
                <tbody>
                    <?php 
                        if (is_array($articulo) || is_object($articulo))
                        {
                            
                            foreach ($articulo as $articulo)
                            {
                                echo "<tr>
                                <td>".$articulo["Id"]."</td>
                                <td>".$articulo["Titulo"]."</td>
                                <td>".$articulo["Subtitulo"]."</td>
                                <td>".$articulo["ImagenPrincipal"]."</td>
                                <td>".$articulo["DptoAsociado"]."</td>
                                <td>".$articulo["Fecha_Registro"]."</td>
                                <td>".$articulo["Responsable"]."</td>
                                <td>".$articulo["Estado"]."</td>";
                                echo '<td><button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/articulocontroller.php',".$articulo["Id"].');"><i class="fa fa-pencil"></i></button><button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/articulocontroller.php',".$articulo["Id"].');"><i class="fa fa-book"></i></button>';

                                if ($articulo["Id"]==0) {
                                   echo '<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/articulocontroller.php',".$articulo["Id"].');"><i class="fa fa-trash-o"></i></button>';
                                }

                                echo "</td></tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
           
        <!-- /.row -->
 <script type="text/javascript">
    $(document).ready(function(){
$('#articulos').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de articulos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        }
    });
// Setup - add a text input to each footer cell
    $('#articulos tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" style=" width: 100%;" type="text" placeholder="'+title+'" />' );
    } );
 
    // DataTable
    var table = $('#articulos').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([4,6,7]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control" ><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});


</script>