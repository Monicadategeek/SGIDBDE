<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h2 class="modal-title" style="text-align: center;"><?php echo $articulo['Titulo'];?></h2>
  <h3 style="text-align: center;"><?php echo $articulo['Subtitulo']; ?></h3>
</div>
<div class="modal-body row">
<div class="col-md-12" style="text-align: center;">
  
  <p>Departamento Asociado: <a href=""><?php echo $articulo['DptoAsociado']; ?></a>
  Tags: <?php 
      foreach ($tags as $tag) {
        echo ' <a href="#">'.$tag['Tag'].'</a>';
      }
  ?></p>
  <?php echo $articulo['ImagenPrincipal'];?>
  <hr>
  <div class="col-xs-12 col-md-12 col-xl-12" id="contenidotext">
     <?php echo $articulo['Contenido']; ?>
  </div>
  <hr>
  <p><b>Autor:</b> <?php echo $articulo['Responsable']; ?></p>
  <p><b>Ultima Modificación:</b> <?php echo $articulo['Fecha_Registro']; ?></p>
  

</div>
  <div class="col-xs-12 col-md-12 col-xl-12" align="center">
    <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
  </div>
</div>
  
    
