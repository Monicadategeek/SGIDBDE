<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Registro de Articulos</h4>
</div>
<div class="modal-body row" id="formulario" >
    <form id="agregar" action="../Controller/articulocontroller.php" method="POST">
    <input type="hidden" name="accion" id="accion" value="store">
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="titulo">Titulo</label>
        <input type="text" name="titulo" id="titulo" class="form-control" required="" title="Ingrese el titulo del articulo">
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="subtitulo">Subtitulo</label>
        <input type="text" name="subtitulo" id="subtitulo" class="form-control" required="" title="Ingrese el subtitulo del articulo">
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="tags">Tags</label>
         <select id="tags" name="tags[]" class="form-control selectpicker" title="-Seleccione-" required multiple="">
          <?php 
          if (is_array($Tags) || is_object($Tags))
          {
              foreach ($Tags as $Tag)
              {
                echo "<option value='".$Tag["Id"]."'>".$Tag["Nombre"]."</option>";
              }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-md-12 col-xl-12">
        <div class="form-group col-xs-12 col-md-6 col-xl-6">
          <label class="control-label" for="imagenprincipal">Imagen Principal</label>
          <input type="file" name="imagenprincipal" id="imagenprincipal" class="form-control" required="" title="Ingrese la imagen principal del articulo">
        </div>
        <div class="form-group col-xs-12 col-md-6 col-xl-6">
        <label class="control-label" for="DptoAsociado">Asociado al Departamento</label>
        <select id="DptoAsociado" name="DptoAsociado" class="form-control selectpicker" title="-Seleccione-" required>
          <?php 
          if (is_array($departamentos) || is_object($departamentos))
          {
              foreach ($departamentos as $departamento)
              {
                  echo "<option value='".$departamento["IdDepartamento"]."'>".$departamento["Descripcion"]."</option>";
              }
          }
          ?>
        </select>
        </div>
      </div>

      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="titulo">Contenido</label>
        <textarea id="articuloeditor" name="articuloeditor" class="ckeditor" rows="10" cols="80" required="" title="Ingrese el contenido del articulo"></textarea>
      </div>
      <div class="form-group col-xs-12 col-md-12 col-xl-12">
        <label class="control-label" for="AsistenciEstud">Desea que su articulo sea:</label>
        <p>
          Publicado <input type="radio" class="flat" name="estado" id="estado" value="1" checked="" required /> 
          No Publicado<input type="radio" class="flat" name="estado" id="estado" value="0" />
        </p>
      </div>
      <div class="col-xs-12 col-md-12 col-xl-12" align="center">
        <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
        <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
      </div>
    </form>
</div>

<script type="text/javascript">
CKEDITOR.replace( 'articuloeditor' );
                $('textarea.ckeditor').each(function () {
                   var $textarea = $(this);
                   $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
                });
$('.selectpicker').selectpicker('refresh');
    $('#imagenprincipal').change(function(){
      var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".png"];
      var archivos = this.files;
      if (archivos.length > 0) {
        for (var i = 0; i < archivos.length; i++) {
            sFileName = archivos[i];
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.name.substr(sFileName.name.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                }
            }                     
            if (!blnValid) {
                mensaje=sFileName.name + " es invalido, las extensiones validas son: " + _validFileExtensions.join(", ");
                notificacion(2, 'fa fa-times-circle','Error!',mensaje);
                this.value = "";
                return false;
            }
            if (sFileName.size/1024/1024 >= 2) {
                notificacion(2, 'fa fa-times-circle','Error!', 'El tamaño de la Foto_Usuarioe debe ser menor o igual a 2 MB');
                this.value = "";
                return false;
            }
        }
      }
    });
  $('#formulario').on('submit', '#agregar', function (e) {
    e.preventDefault();
    document.getElementById("boton").disabled = true;
    var parametros= new FormData($(this)[0]);
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: parametros,
        contentType:false,
        processData:false,
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/articulocontroller.php?accion=index');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha registrado el articulo');
            }
            else{
              notificacion(2, 'fa fa-times-circle','Error!','Ya existe');
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>