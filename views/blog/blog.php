             <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Division de Bienestar y Desarrollo Estudiantil
                    <small>Blog de Noticias</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#">Inicio</a>
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
    
                <?php
                    if (is_array($articulos) || is_object($articulos))
                    {
                        $totalpost = count($articulos);
                        $arraypost = array($totalpost+1);
                        $contadorpost = 1;
                        $articulocontroller= "'../Controller/articulocontroller.php'";
                        
                        foreach ($articulos as $articulo)
                        { 
                            $arraypost[$contadorpost] = '<h2>
                                <a href="#">'.$articulo["Titulo"].'</a>
                            </h2>
                            <p class="lead">
                                por <a href="../web/index.php">'. $articulo["Responsable"].'</a>
                            </p>
                            <p><i class="fa fa-clock-o"></i> Posteado el '.$articulo["Fecha_Registro"].'</p>
                            <p>Depto Asociado '.$articulo["DptoAsociado"].'</p>
                            <hr>
                            <a href="../web/blog-post.html">
                                '.$articulo["ImagenPrincipal"].'
                            </a>
                            <hr>
                            <p>'.$articulo["Subtitulo"].'</p>
                            <a class="btn btn-primary" onclick="ver('.$articulocontroller.', '.$articulo["Id"].');" >Leer Mas <i class="fa fa-angle-right"></i></a>

                            <hr>';
                            $contadorpost = $contadorpost + 1;
                        } 
                    }

                if ($totalpost == 0) {
                            echo '<br><br><br><h2 class="post-title title2" style="color:#333;text-align:center;">No hay POST aun, vuelve pronto</h2>';
                        }
                        $totalpages = $totalpost / 5;
                        $totalpagesparse = intval($totalpages);
                        if ($totalpagesparse != $totalpages) {
                            if ($totalpages > $totalpagesparse) {
                               $totalpagesparse = $totalpagesparse + 1;
                            }
                        }
                        $contadorpost = 1;
                        if ($totalpagesparse == 1) {
                                echo '<div id="postpage'.$totalpagesparse.'">';
                                for ($j=1; $j <= $totalpost; $j++) { 
                                    echo $arraypost[$contadorpost];
                                    $contadorpost = $contadorpost+1;
                                }
                                $tempval = $totalpages - 1;
                                echo "</div>";

                        }
                        else if ($totalpagesparse > 1) {
                        
                        for ($i=1; $i <= $totalpagesparse; $i++) { 
                            if ($i == $totalpagesparse){
                                $contadorpost=$contadorpost - 1; 
                                $postrest = $totalpost - $contadorpost;
                                $contadorpost=$contadorpost + 1; 
                                echo '<div id="postpage'.$totalpagesparse.'" style="display:none;">';
                                for ($j=1; $j <= $postrest; $j++) { 
                                    echo $arraypost[$contadorpost];
                                    $contadorpost = $contadorpost+1;
                                }
                                $tempval = $totalpagesparse - 1;
                                echo '
                                <ul class="paging-news pager">
                                    <li class="pull-left previous"> <a href="javascript:PostPages('.$tempval.');">&larr; </i>Articulos Nuevos</a> </li>
                                  </ul>
                                </div>';
                            }
                            else if ($i == 1) {
                                echo '<div id="postpage1">';
                                for ($j=1; $j <= 5; $j++) { 
                                    echo $arraypost[$contadorpost];
                                    $contadorpost = $contadorpost+1;
                                }
                                echo '
                                <ul class="paging-news pager">
                                    <li class="pull-right next"> <a href="javascript:PostPages(2);"> Articulos Anteriores &rarr;</a></li>
                                  </ul>
                                </div>';
                            }
                            
                            else {
                                echo '<div id="postpage'.$i.'" style="display:none;">';
                                for ($j=1; $j <= 5; $j++) { 

                                    echo $arraypost[$contadorpost];
                                    $contadorpost = $contadorpost+1;
                                }
                                $tempval = $i + 1; 
                                $tempval2 = $i - 1;
                                echo '
                                <ul class="paging-news pager">
                                    <li class="pull-left previous"> <a href="javascript:PostPages('.$tempval2.');">&larr; Articulos Nuevos</a> </li>
                                    <li class="pull-right next"> <a href="javascript:PostPages('.$tempval.');"> Articulos Anteriores &rarr;</a></li>
                                  </ul>
                                </div>';
                            }
                        }
                        }
                        ?>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categorias Blog</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php
                                    foreach ($Tags as $Tag) {
                                        echo '<li><a href="#">'.$Tag["Descripcion"]." (".$Tag["total"].')</a></li>';
                                    }
                                ?>
                                
                            </ul>
                        </div>
                        <!-- /.col-lg-6 
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                                <li><a href="#">Categoria</a>
                                </li>
                            </ul>
                        </div>-->
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

            </div>

        </div>
        <!-- /.row -->