 <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Ver Registro de la Suspención del Servicios</h4>
</div>
<div class="modal-body" >
  <table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
	  	<tr>
	      <th>Id</th>
	      <th>Fecha Inicio</th>
	      <th>Fecha Fin</th>      
	      <th>Servicio</th>
	      <th>Tipo Cancelación</th>
	    </tr>
	    <tr>
	      <td>
	        <?php echo $cancelacion["Id"];?>
	      </td>
	      <td>
	        <?php echo $cancelacion["Fecha_Inic"];?>
	      </td>
	      <td>
	        <?php echo $cancelacion["Fecha_Fin"];?>
	      </td>
	      <td>
	        <?php echo $cancelacion["Servicio"];?>
	      </td>
	      <td>
	        <?php echo $cancelacion["Tipo_Cancelacion"];?>
	      </td>    
	    </tr>
   	</table>
   	<table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
  		<tr>
	      <th>Razón</th>
	    </tr>
	    <tr>
	      <td>
	        <?php echo $cancelacion["Razon"];?>
	      </td>
	    </tr>
	</table>
	<table class="table table-striped table-bordered col-xs-12 col-md-12 col-xl-12">
		<tr>
			<th>Fecha de Registro</th>
	     	<th>Responsable</th>
	    </tr>
	    <tr>
	      <td><?php echo $cancelacion["Fecha_Registro"];?></td>
	      <td><?php echo $cancelacion["Responsable"];?></td>
	    </tr>
	</table>
   
  <div class="col-xs-12 col-md-12 col-xl-12" align="right">
  <button type="button" class="btn btn-default btn-large" data-dismiss="modal">Cerrar</button>
  </div>
  
</div>
<div class="modal-footer" >
</div>



