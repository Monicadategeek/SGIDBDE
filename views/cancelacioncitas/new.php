<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <h4 class="modal-title">Suspender Servicio</h4>
</div>
<div class="modal-body" id="formulario">
  <div class="form-group col-xs-12 col-md-12 col-xl-12" id="formulario"><p align="left"><b>Nota:</b> Serán canceladas todas las citas del período de tiempo. <b>¡Sea Cuidadoso!</b></p><br>
  <p align="left"><b>Nota:</b> El proceso de Cancelación de citas puede demorar dependiendo de la cantidad estudiantes citados, ya que se le enviará a cada estudiante un correo informandole la cancelación</p>

  </div>
  
  <form id="cancelar" action="../Controller/CancelarCitasController.php" method="POST">
  <input type="hidden" name="accion" id="accion" value="store">
    <div class="form-group col-xs-12 col-md-12 col-xl-12" id="rango">
      <div class=" input-daterange input-group" id="datepicker">
        <span class="input-group-addon">Desde</span>
        <input type="text" class="form-control" name="Fecha_Inic" id="Fecha_Inic" maxlength="10" />
        <span class="input-group-addon">Hasta</span>
        <input type="text" class=" form-control" name="Fecha_Fin" id="Fecha_Fin" maxlength="10" />
      </div>

  </div>


    <div class="col-xs-12 col-md-12 col-xl-12">
      <div class="form-group col-xs-6 col-md-6 col-xl-6"> 
        <label class="control-label" for="Tipo_Cancelacion ">Tipo de Cancelación</label>
        <select id="Tipo_Cancelacion" name="Tipo_Cancelacion" class="form-control selectpicker" required="" title="-Seleccione-">
          <option value="4">Cancelada por el Departamento</option>
          <option value="5">Inasistencia Médica</option>
        </select>
      </div>
      <div class="form-group col-xs-6 col-md-6 col-xl-6">
        <label class="control-label" for="Depto">Servicio</label>
        <select id="Depto" name="Depto" class="form-control selectpicker" required="" title="-Seleccione-">
          <?php 
            if (is_array($departamentos) || is_object($departamentos))
            {
                foreach ($departamentos as $departamento)
                {
                    echo "<option value='".$departamento["IdDepartamentos"]."'>".$departamento["TipoDepto"]." en ".$departamento["Sede"]."</option>";
                }
            }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group col-xs-12 col-md-12 col-xl-12">
      <p><b>Nota:</b> Es Importante que escriba la razón de cancelación de las citas, ya que se enviará por correo a los estudiantes con cita asignada para la fecha de cancelación</p>
      <label for="razon">Razón de cancelación</label>
      <textarea id="razon" name="razon" class="form-control"></textarea>
      
    </div>
    <div class="form-group col-xs-12 col-md-12 col-xl-12" align="center">
      <button align="left" type="button" class="btn btn-default btn-large" data-dismiss="modal">Cancelar</button>
      <input type="submit" value="Guardar" class="btn btn-primary btn-large" id="boton">
    </div>
  </form>
</div>


<script type="text/javascript">

  $(document).ready(function(){
    $('.input-daterange').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '+1d',   
        language: 'es',

        });

      $('.selectpicker').selectpicker('refresh');
     $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es'
        });
    });

  $('#formulario').on('submit', '#cancelar', function (e) {
    e.preventDefault();

    if (moment($('#Fecha_Fin').val()).format('YYYY-MM-DD') < moment($('#Fecha_Inic').val()).format('YYYY-MM-DD')) {
      notificacion(2,'fa fa-times-circle','Error!', 'La fecha "Hasta" debe ser mayor a la fecha de "Desde"');
      return false;
    }

    document.getElementById("boton").disabled = true;         
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success:function(data){
          respuesta = parseInt(data);
            if (respuesta==1) {
              redireccionar('../Controller/CancelarCitasController.php?accion=index');
              $('#agregar').modal('hide');
              notificacion(3,'fa fa-check','Completado!','Se ha suspendido el Servicio correctamente');
            }
            else{
              notificacion(2,'fa fa-times-circle','Error!','Ha Ocurrido un Error: '+data);
            }
            document.getElementById("boton").disabled = false;
        }
    })
  });
</script>