<?php session_start(); //abre sesion ?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
  <meta charset="utf-8">
	<link href='../web/images/logosolo.png' rel='shortcut icon' type='image/png'>

    <title>Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil</title>

    <!-- Bootstrap -->
    <link href="../web/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../web/css/bootstrap-select.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../web/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../web/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../web/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="../web/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../web/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../web/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="../web/css/perfect-scrollbar.css" />
    <link href="../web/css/pnotify.custom.css" rel="stylesheet" />
    <link href="../web/css/animate.css" rel="stylesheet" />
   

    <!-- Custom Theme Style -->
    <link href="../web/build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">


  <div id="contpreloader">
          <div style="position: relative;padding-top: 10%; width: 20%;margin: auto;">
            <div class="loader2" style="position: absolute;"></div>
            <img style="width: 80%;" src="../web/images/Logosolo.png">
          </div>
          
          
          <br><br><br><br><p class="title3" style="text-align: center; color: #333;font-size: 3rem;font-weight: bold;font-size: 3rem;line-height: normal;">Cargando...<br> Por favor espere.</p><br>
        </div>
<!-- Modal-->
<!-- Modal Cargando-->
 <div class="modal fade" id="CargandoModal" role="dialog" aria-labelledby="CargandoModalLabel" aria-hidden="true" tabindex="-1" role="dialog" class="modal fade ng-isolate-scope in" modal-in-class="in" style="z-index: 1045;">
    <div class="modal-dialog modal-sm" role="document" style="top:25%;z-index: 1060;">
    
      <!-- Modal content-->
      <div class="modal-content" >
      <style>
      #contpreloader{
        top: 0;
        left: 0;
        z-index: 100000000;
        position: fixed;
        width: 100%;
        height: 100%;
        background: #fff;
      }
        .loader {
            border: 0px solid #c4c3c1;
		    border-top: 1rem solid #ff473e;
		    border-bottom: 1rem solid #0086dd;
		    border-left: 3rem solid #ff5a00;
		    border-radius: 50%;
		    width: 150%;
		    height: 105%;
		    left: -25%;
		    top: 8%;
            animation: spin 2s linear infinite;
        }
        .loader2 {
            border: 0px solid #c4c3c1;
		    border-top: 1rem solid #ff473e;
		    border-bottom: 1rem solid #0086dd;
		    border-left: 3rem solid #ff5a00;
		    border-radius: 50%;
		    width: 114%;
		    height: 82%;
		    left: -18%;
		    top: 25%;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        </style>
        <div >
          <div style="position: relative;padding-top: 15%;width: 50%;margin: auto;">
            <div class="loader" style="position: absolute;"></div>
            <img style="width: 100%;" src="../web/images/Logosolo.png">
          </div>
          
          
          <br><br><br><p class="title3" style="text-align: center; color: #333;font-size: 2rem;font-weight: bold;">Cargando...<br> Por favor espere.</p><br>
        </div>

      </div>

      </div>
      </div>
      <div class="modal-backdrop fade in" modal-in-class="in" id="cargandobackdrop" style="display:none;z-index: 1050;opacity: 0;" 
      ></div>


    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed ">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="../web/index.html" class="site_title">
              	<img src="../web/images/logo.png" style="width: 100%;" id="logoimg1">
              	<img src="../web/images/logosolo.png" style="width: 100%;" id="logoimg2">
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../web/images/img.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span><?php echo $_SESSION['USUARIO']; ?></span>
                <h2></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->


            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="#" onclick="redireccion('inicio');"><i class="fa fa-home"></i>Inicio</a></li>
                  <li><a><i class="fa fa-building"></i> Departamentos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccionar('../Controller/DepartamentosController.php?accion=index');">Registro de Departamentos</a></li>
                      <li><a onclick="redireccionar('../Controller/UsuariosController.php?accion=index');">Personas</a></li>
                      <li><a onclick="redireccionar('../Controller/TrabajadoresController.php?accion=index');">Trabajadores</a></li>
                      <li><a onclick="redireccion('consultahorarios');">Consulta de Horarios</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-list-alt"></i> Servicios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccion('solicitudcitas');">Solicitud de Citas</a></li>
                      <li><a onclick="redireccion('atencioncitas');">Atención de Citas</a></li>
                      <li><a onclick="redireccion('consultas');">Consultas</a></li>
                      <li><a onclick="redireccion('suspensionservicios');">Suspención de Servicios</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-money"></i> Control de Becas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccionar('../Controller/PresupuestoController.php?accion=index');">Presupuesto</a></li>
                      <li><a onclick="redireccion('inscripciones');">Inscripciones</a></li>
                      <li><a onclick="redireccion('controlbecarios');">Control Becarios</a></li>
                      <li><a onclick="redireccion('becariosinactivos');">Becarios Inactivos</a></li>
                      <li><a onclick="redireccion('reportemensual');">Reporte Mensual</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart"></i> Estadisticas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccion('estudiantiles');">Estudiantiles</a></li>
                      <li><a onclick="redireccion('servicios');">Servicios</a></li>
                      <li><a onclick="redireccion('departamentos');">Departamentos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Mantenimiento <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a onclick="redireccion('usuarios');">Usuarios</a></li>
                      <li><a onclick="redireccion('tablas');">Tablas</a></li>
                      <li><a onclick="redireccion('opiniones');">Opiniones</a></li>                      
                    </ul>
                  </li>
                </ul>
              </div>       

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Preferencias">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Pantalla Completa" onclick="fullscreen()">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Salir" href="security/login.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="../web/javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="../web/images/img.png" alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="../web/javascript:;"> Perfil</a></li>
                    <li>
                      <a href="../web/javascript:;">
                        Preferencias
                      </a>
                    </li>
                    <li><a href="../web/javascript:;">Ayuda</a></li>
                    <li><a href="../web//Logout"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="../web/javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-orange">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="../web/images/img.png" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">Hace 3 min</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../web/images/img.png" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">Hace 3 min</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../web/images/img.png" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">Hace 3 min</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="../web/images/img.png" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">Hace 3 min</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>Ver Alertas</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" id="contenido">


         
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <small class="block">&copy; 2017 IUTOMS. Todos los Derechos Reservados.</small> 
            <small class="block">Desarrollado por grupo N° 7</small>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Modal Agregar-->
   <div class="modal fade" id="agregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalagregar">
        </div>

      </div>
    </div>

    <!-- Modal Modificar-->
   <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalmodificar">
        </div>

      </div>
    </div>

     <!-- Modal Ver-->
   <div class="modal fade" id="ver" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      
        <!-- Modal content-->
        <div class="modal-content" id="contenidomodalver">
        </div>

      </div>
    </div>

    <!-- jQuery -->
    <script src="../web/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../web/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../web/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../web/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../web/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../web/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../web/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../web/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../web/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../web/vendors/Flot/jquery.flot.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../web/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../web/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../web/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../web/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../web/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../web/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../web/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../web/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../web/vendors/moment/min/moment.min.js"></script>
    <script src="../web/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../web/js/bootstrap-select.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../web/build/js/custom.js"></script>

    <script src="../web/js/queryloader2.min.js"></script>
    <script src="../web/js/pnotify.custom.js" type="text/javascript"></script>
    <script src="../web/js/main.js"></script>
    <script src="../web/js/validator.min.js"></script>
  
  </body>
</html>

