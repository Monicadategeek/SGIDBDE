
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Características de la Vivienda del Estudiante <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Tipos de Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($viviendas as $vivienda) {
					$titulo=$vivienda["tipo_vivienda"];
					$totalpo=$totalpo+$vivienda["total"];

					$total=round(((($vivienda["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$vivienda["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv1" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv1", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Tipos de Tenencia de la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($tenviviendas as $tenviviendas) {
					$titulo=$tenviviendas["tipo_ten_viv"];
					$totalpo=$totalpo+$tenviviendas["total"];

					$total=round(((($tenviviendas["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$tenviviendas["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv2" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv2", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Construcción de la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($construccion as $construccion) {
					$titulo=$construccion["constr_viv"];
					$totalpo=$totalpo+$construccion["total"];

					$total=round(((($construccion["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$construccion["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv3" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv3", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Tipo de pared que esta constuida de la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($tipos_pared as $tipos_pared) {
					$titulo=$tipos_pared["tipo_pared"];
					$totalpo=$totalpo+$tipos_pared["total"];

					$total=round(((($tipos_pared["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$tipos_pared["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv4" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv4", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Tipo de piso que esta constuida de la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($tipos_piso as $tipos_piso) {
					$titulo=$tipos_piso["tipo_piso"];
					$totalpo=$totalpo+$tipos_piso["total"];

					$total=round(((($tipos_piso["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$tipos_piso["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv5" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv5", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Tipo de techo que esta constuida de la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($tipos_techo as $tipos_techo) {
					$titulo=$tipos_techo["tipo_techo"];
					$totalpo=$totalpo+$tipos_techo["total"];

					$total=round(((($tipos_techo["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$tipos_techo["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv6" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv6", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Promedio de nro de Habitaciones por Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Habitación</th>
				<th>Nro</th>
			</thead>
			<tbody>
				<tr>
					<td>Sala</td>
					<td><?php echo $habitacion["sala"];?></td>
				</tr>
				<tr>
					<td>Cocina</td>
					<td><?php echo $habitacion["cocina"];?></td>
				</tr>
				<tr>
					<td>Baños</td>
					<td><?php echo $habitacion["banios"];?></td>
				</tr>
				<tr>
					<td>Dormitorios</td>
					<td><?php echo $habitacion["dormitorios"];?></td>
				</tr>
				<tr>
					<td>Nro Miembros Familiares en Promedio</td>
					<td><?php echo $familias;?></td>
				</tr>
				
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv7" class="grafico"></div>
			<?php echo '<script>
			var chart = AmCharts.makeChart( "chartdiv7", {
			  "type": "serial",
			  "theme": "light",
			  "dataProvider": [ {
			    "tipo": "Nro Miembros Familiares",
			    "nro": '.$familias.'
			  }, {
			    "tipo": "Nro Salas",
			    "nro": '.$habitacion["sala"].'
			  }, {
			    "tipo": "Nro Cocinas",
			    "nro": '.$habitacion["cocina"].'
			  }, {
			    "tipo": "Nro Baños",
			    "nro": '.$habitacion["banios"].'
			  }, {
			    "tipo": "Nro Dormitorios",
			    "nro": '.$habitacion["dormitorios"].'
			  }],
			  "valueAxes": [ {
			    "gridColor": "#FFFFFF",
			    "gridAlpha": 0.2,
			    "dashLength": 0
			  } ],
			  "gridAboveGraphs": true,
			  "startDuration": 1,
			  "graphs": [ {
			    "balloonText": "[[category]]: <b>[[value]]</b>",
			    "fillAlphas": 0.8,
			    "lineAlpha": 0.2,
			    "type": "column",
			    "valueField": "nro"
			  } ],
			  "chartCursor": {
			    "categoryBalloonEnabled": false,
			    "cursorAlpha": 0,
			    "zoomable": false
			  },
			  "categoryField": "tipo",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "gridAlpha": 0,
			    "tickPosition": "start",
			    "tickLength": 20
			  },
			  "export": {
			    "enabled": true
			  }

			} );
			</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Servicio de Luz que posse la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th></th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($luz as $luz) {
					$titulo=$luz["luz"];
					$totalpo=$totalpo+$luz["total"];

					$total=round(((($luz["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$luz["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv8" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv8", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Servicio de Agua que posse la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th></th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($agua as $agua) {
					$titulo=$agua["agua"];
					$totalpo=$totalpo+$agua["total"];

					$total=round(((($agua["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$agua["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv9" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv9", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Servicio de Aseo Urbano que posse la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th></th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($aseo_urb as $aseo_urb) {
					$titulo=$aseo_urb["aseo_urb"];
					$totalpo=$totalpo+$aseo_urb["total"];

					$total=round(((($aseo_urb["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$aseo_urb["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv10" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv10", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Servicio de Telefono Local que posse la Vivienda donde residen los Estudiantes</h3>
	<div class="col-xs-4 col-md-4">		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th></th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($telf_local as $telf_local) {
					$titulo=$telf_local["telf_local"];
					$totalpo=$totalpo+$telf_local["total"];

					$total=round(((($telf_local["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$telf_local["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>Total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";
				$valor = substr($valor, 0, -1);
				?>
			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<div id="chartdiv11" class="grafico"></div>
		<?php echo '<script>
			var chart = AmCharts.makeChart("chartdiv11", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": ['.$valor.'],
			  "valueField": "visits",
			  "titleField": "country",
			  "startEffect": "elastic",
			  "startDuration": 1,
			  "minRadius":100,
			  "labelRadius": 25,
			  "innerRadius": "30%",
			  "depth3D": 10,
			  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
			  "angle": 15,
			  "export": {
			    "enabled": true
			  }
			});

			</script>';
		?>
	</div>
</div>