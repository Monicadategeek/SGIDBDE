
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Estudiantes Residenciados <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">
	<h3 align="center">Estados donde se encuentran viviendo los estudiantes del IUTOMS</h3>
	<div class="col-xs-4 col-md-4">

		
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Estado</th>
				<th>Nro Estudiantes</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
			foreach ($residencias as $residencia) {
				$titulo=$residencia["estado"];
				$totalpo=$totalpo+$residencia["total"];

				$total=round(((($residencia["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$residencia["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico </h3>
		<div id="chartdiv1" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv1", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>



<?php
$nrografica=1;
foreach ($residencias as $residencia) {
	echo '<div class="col-xs-12 col-md-12 col-xl-12">
		<h3 align="center">Estudiantes que viven en el Edo. '.$residencia["estado"].' <small>Población <u>'.$residencia["total"].'</u></small> </h3>';

		echo '<h4 align="left">Por Municipios</h4>';

		if (count($residencia["municipios"])>0) {
			?>
			<div class="col-xs-4 col-md-4">

				
				<table class="table table-bordered table-striped tabla" border="1" >
					<thead>
						<th>Municipio</th>
						<th>Nro Estudiantes</th>
						<th>%</th>
					</thead>
					<tbody>
						<?php
						$total=0;
						$valor="";
						$totalpo=0;
						
					foreach ($residencia["municipios"] as $municipio) {

						$nrografica=$nrografica+1;

						$titulo=$municipio["municipio"];
						$totalpo=$totalpo+$municipio["total"];

						$total=round(((($municipio["total"])*100)/$poblacion), 2);
						$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
						echo "<tr>
								<td>".$titulo."</td>
								<td>".$municipio["total"]."</td>
								<td>".$total."%</td>
							</tr>";
					}

					$total=round(((($totalpo)*100)/$poblacion), 2);

					echo "<tr>
							<td>total</td>
							<td>".$totalpo."</td>
							<td>".$total."%</td>
						</tr>";

					$valor = substr($valor, 0, -1);
					?>

					</tbody>
				</table>
			</div>

			<div class="col-md-8 col-xs-8">
			
				<div id="<?php echo $nrografica?>" class="grafico"></div>
						<?php echo '<script>
				var chart = AmCharts.makeChart("'.$nrografica.'", {
				  "type": "pie",
				  "theme": "light",
				  "dataProvider": ['.$valor.'],
				  "valueField": "visits",
				  "titleField": "country",
				  "startEffect": "elastic",
				  "startDuration": 1,
				  "minRadius":100,
				  "labelRadius": 25,
				  "innerRadius": "30%",
				  "depth3D": 10,
				  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
				  "angle": 15,
				  "export": {
				    "enabled": true
				  }
				});

				</script>';
				?>
			</div>
			

			<?php

		}

		echo '<h4 align="left">Por Parroquias</h4>';
		if (count($residencia["parroquias"])>0) {
			?>
			<div class="col-xs-4 col-md-4">

				
				<table class="table table-bordered table-striped tabla" border="1" >
					<thead>
						<th>Parroquias</th>
						<th>Nro Estudiantes</th>
						<th>%</th>
					</thead>
					<tbody>
						<?php
						$total=0;
						$valor="";
						$totalpo=0;
						
					foreach ($residencia["parroquias"] as $parroquias) {

						$nrografica=$nrografica+1;

						$titulo=$parroquias["parroquia"];
						$totalpo=$totalpo+$parroquias["total"];

						$total=round(((($parroquias["total"])*100)/$poblacion), 2);
						$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
						echo "<tr>
								<td>".$titulo."</td>
								<td>".$parroquias["total"]."</td>
								<td>".$total."%</td>
							</tr>";
					}

					$total=round(((($totalpo)*100)/$poblacion), 2);

					echo "<tr>
							<td>total</td>
							<td>".$totalpo."</td>
							<td>".$total."%</td>
						</tr>";

					$valor = substr($valor, 0, -1);
					?>

					</tbody>
				</table>
			</div>

			<div class="col-md-8 col-xs-8">
			
				<div id="<?php echo $nrografica?>" class="grafico"></div>
						<?php echo '<script>
				var chart = AmCharts.makeChart("'.$nrografica.'", {
				  "type": "pie",
				  "theme": "light",
				  "dataProvider": ['.$valor.'],
				  "valueField": "visits",
				  "titleField": "country",
				  "startEffect": "elastic",
				  "startDuration": 1,
				  "minRadius":100,
				  "labelRadius": 25,
				  "innerRadius": "30%",
				  "depth3D": 10,
				  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
				  "angle": 15,
				  "export": {
				    "enabled": true
				  }
				});

				</script>';
				?>
			</div>
			

			<?php

		}


		echo '<h4 align="left">Por Ciudades</h4>';
		if (count($residencia["ciudades"])>0) {
			?>
			<div class="col-xs-4 col-md-4">

				
				<table class="table table-bordered table-striped tabla" border="1" >
					<thead>
						<th>Ciudad</th>
						<th>Nro Estudiantes</th>
						<th>%</th>
					</thead>
					<tbody>
						<?php
						$total=0;
						$valor="";
						$totalpo=0;
						
					foreach ($residencia["ciudades"] as $ciudades) {

						$nrografica=$nrografica+1;

						$titulo=$ciudades["ciudad"];
						$totalpo=$totalpo+$ciudades["total"];

						$total=round(((($ciudades["total"])*100)/$poblacion), 2);
						$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
						echo "<tr>
								<td>".$titulo."</td>
								<td>".$ciudades["total"]."</td>
								<td>".$total."%</td>
							</tr>";
					}

					$total=round(((($totalpo)*100)/$poblacion), 2);

					echo "<tr>
							<td>total</td>
							<td>".$totalpo."</td>
							<td>".$total."%</td>
						</tr>";

					$valor = substr($valor, 0, -1);
					?>

					</tbody>
				</table>
			</div>

			<div class="col-md-8 col-xs-8">
			
				<div id="<?php echo $nrografica?>" class="grafico"></div>
						<?php echo '<script>
				var chart = AmCharts.makeChart("'.$nrografica.'", {
				  "type": "pie",
				  "theme": "light",
				  "dataProvider": ['.$valor.'],
				  "valueField": "visits",
				  "titleField": "country",
				  "startEffect": "elastic",
				  "startDuration": 1,
				  "minRadius":100,
				  "labelRadius": 25,
				  "innerRadius": "30%",
				  "depth3D": 10,
				  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
				  "angle": 15,
				  "export": {
				    "enabled": true
				  }
				});

				</script>';
				?>
			</div>
			

			<?php

		}


	echo '</div>';
	
}

?>