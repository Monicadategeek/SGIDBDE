
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<script type="text/javascript">
	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
</script>

<h3 align="center">Datos Culturales del Estudiante <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>

<p><b>Nota:</b> Los resultados de las graficas puede ser mayor a la población ya que los estudiantes pueden seleccionar mas de una opción</p>

<div class="col-md-12 col-xs-12 col-xl-12">
	<table class="table table-bordered table-striped tabla" border="1" >
		<thead>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
		</thead>

		<tbody>
			<?php 
				$cont=0;
			  	if (isset($actividades) and count($actividades)>0) {
			  		foreach ($actividades as $actividad) {
			  			$cont=$cont+1;
			  			if ($cont==1) {
			  				echo "<tr>";
			  			}
			  			echo '<td>'.$actividad["actividad"].'</td> <td>'.$actividad["total"].'</td>';

						if ($cont==3) {
							$cont=0;
			  				echo "</tr>";
			  			}
			  		}

			  		if ($cont<>0) {
			  			echo "<td colspan='".(4/$cont)."'></td></tr>";
			  		}
			  	}
			?>

		</tbody>
	</table>
		<h3 align="center">En que Ocupas el Tiempo Libre los Estudiantes</h3>

		<div id="chartdiv1" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($actividades) and count($actividades)>0) {
			  		foreach ($actividades as $actividad) {
			  			$cont=$cont+1;
			  			echo '{
							    "Descripcion": "'.$actividad["actividad"].'",
							    "valor": '.$actividad["total"].',
							    "color": getRandomColor()
							  }';

						if ($cont<count($actividades)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "<b>[[category]]: [[value]]</b> Estudiantes",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "valor",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			   ],
			  "categoryField": "Descripcion",
			  "categoryAxis": {
			    "gridPosition": "start",
        		"labelRotation": 90
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
</div>

<div class="col-md-12 col-xs-12 col-xl-12">
	<h3 align="center">Que deporte practican los estudiantelos Estudiantes</h3>
	<table class="table table-bordered table-striped tabla" border="1" >
		<thead>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
		</thead>

		<tbody>
			<?php 
				$cont=0;
			  	if (isset($deportes) and count($deportes)>0) {
			  		foreach ($deportes as $deporte) {
			  			$cont=$cont+1;
			  			if ($cont==1) {
			  				echo "<tr>";
			  			}
			  			echo '<td>'.$deporte["deporte"].'</td> <td>'.$deporte["total"].'</td>';

						if ($cont==3) {
							$cont=0;
			  				echo "</tr>";
			  			}
			  		}

			  		if ($cont<>0) {
			  			echo "<td colspan='".(4/$cont)."'></td></tr>";
			  		}
			  	}
			?>

		</tbody>
	</table>

	<div id="chartdiv2" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($deportes) and count($deportes)>0) {
			  		foreach ($deportes as $deporte) {
			  			$cont=$cont+1;
			  			echo '{
							    "Descripcion": "'.$deporte["deporte"].'",
							    "valor": '.$deporte["total"].',
							    "color": getRandomColor()
							  }';

						if ($cont<count($deportes)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "<b>[[category]]: [[value]]</b> Estudiantes",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "valor",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			   ],
			  "categoryField": "Descripcion",
			  "categoryAxis": {
			    "gridPosition": "start",
        		"labelRotation": 90
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
</div>

<div class="col-md-12 col-xs-12 col-xl-12">
	<h3 align="center">Que habilidades tienen los estudiantelos Estudiantes</h3>
	<table class="table table-bordered table-striped tabla" border="1" >
		<thead>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
		</thead>

		<tbody>
			<?php 
				$cont=0;
			  	if (isset($habilidades) and count($habilidades)>0) {
			  		foreach ($habilidades as $habilidad) {
			  			$cont=$cont+1;
			  			if ($cont==1) {
			  				echo "<tr>";
			  			}
			  			echo '<td>'.$habilidad["habilidad"].'</td> <td>'.$habilidad["total"].'</td>';

						if ($cont==3) {
							$cont=0;
			  				echo "</tr>";
			  			}
			  		}

			  		if ($cont<>0) {
			  			echo "<td colspan='".(4/$cont)."'></td></tr>";
			  		}
			  	}
			?>

		</tbody>
	</table>

		<div id="chartdiv3" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv3", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($habilidades) and count($habilidades)>0) {
			  		foreach ($habilidades as $habilidad) {
			  			$cont=$cont+1;
			  			echo '{
							    "Descripcion": "'.$habilidad["habilidad"].'",
							    "valor": '.$habilidad["total"].',
							    "color": getRandomColor()
							  }';

						if ($cont<count($habilidades)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "<b>[[category]]: [[value]]</b> Estudiantes",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "valor",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			   ],
			  "categoryField": "Descripcion",
			  "categoryAxis": {
			    "gridPosition": "start",
        		"labelRotation": 90
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
</div>


<div class="col-md-12 col-xs-12 col-xl-12">
	<h3 align="center">Que eventos IUTOMS han participado los estudiantelos Estudiantes</h3>
	<table class="table table-bordered table-striped tabla" border="1" >
		<thead>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
			<th>Descripción</th>
			<th title="Nro Estudiantes">Nro</th>
		</thead>

		<tbody>
			<?php 
				$cont=0;
			  	if (isset($eventos) and count($eventos)>0) {
			  		foreach ($eventos as $evento) {
			  			$cont=$cont+1;
			  			if ($cont==1) {
			  				echo "<tr>";
			  			}
			  			echo '<td>'.$evento["evento"].'</td> <td>'.$evento["total"].'</td>';

						if ($cont==3) {
							$cont=0;
			  				echo "</tr>";
			  			}
			  		}

			  		if ($cont<>0) {
			  			echo "<td colspan='".(4/$cont)."'></td></tr>";
			  		}
			  	}
			?>

		</tbody>
	</table>

		<div id="chartdiv4" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($eventos) and count($eventos)>0) {
			  		foreach ($eventos as $evento) {
			  			$cont=$cont+1;
			  			echo '{
							    "Descripcion": "'.$evento["evento"].'",
							    "valor": '.$evento["total"].',
							    "color": getRandomColor()
							  }';

						if ($cont<count($eventos)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "<b>[[category]]: [[value]]</b> Estudiantes",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "valor",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			   ],
			  "categoryField": "Descripcion",
			  "categoryAxis": {
			    "gridPosition": "start",
        		"labelRotation": 90
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
</div>