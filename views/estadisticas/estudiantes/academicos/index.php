<div class="form-group col-xs-6 col-md-6 col-xl-6">
    <label class="control-label" for="tipoestadisticas">Año Academico</label>
    <select id="anno" name="anno" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
    	<?php 
    	foreach ($annos as $anno => $value) {
    		echo "<option value='".$value."''>".$value."-".($value+1)."</option>";
    	}
    	?>
    </select>
</div>

<div class="form-group col-xs-6 col-md-6 col-xl-6" style="text-align: left;">

    <a class="btn btn-default buttons-pdf buttons-html5" href="#" onclick="tomarcaptura('estadistica', 'EstadisticasAcademicas-SGIDBDE');" id="botonimprimir" style="display: none;margin-top: 30px;"><span><i class="fa fa-print"></i> Imprimir</span></a>
</div>

<script type="text/javascript">
	$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
        $("#anno").change(function () { 
            var  datos={"accion":'academicos', "tipo":1, "anno": $('#anno option:selected').val() };
            div="estadistica";

            enviar('../Controller/EstadisticasEstudiantilesController.php', datos, div);
            $('#botonimprimir').show();

        });


        


    });
</script>