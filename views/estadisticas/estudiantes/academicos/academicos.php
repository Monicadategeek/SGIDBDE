
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Estudiantes que ingresaron en el período Academico <?php echo $anno."-".($anno+1)?> <br><small><b>Población</b> <?php echo $poblacion?> <b>Estudiantes que han realizado la encuesta</b> <?php echo $muestra?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Titulo de Bachiller</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Mención</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			foreach ($titulobach as $titulobach) {
				if ($titulobach["mencion"]=='') {
					$titulo="No contestaron";
				}
				else{
					$titulo=$titulobach["mencion"];
				}
				$total=round(((($titulobach["total"])*100)/$muestra), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$titulobach["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Titulo de Bachiller</h3>
		<div id="chartdiv" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">
		<h3>Tipo de institución</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php

			$valor="";
			$total=0;
			foreach ($tipoinst as $tipoinst) {
				if ($tipoinst["tipo_inst"]==1) {
					$titulo="Público";
				}
				else{
					$titulo="Privado";
				}
				$total=round(((($tipoinst["total"])*100)/$muestra), 2);
				$valor=$valor.'{"dato": "'.$titulo.'", "resultado":"'.$total.'"},';
				
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$tipoinst["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>
	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Tipo de institución</h3>
		<div id="tipinst" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("tipinst", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "resultado",
		  "titleField": "dato",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">
		<h3>Promedio de notas (Bachillerato)</h3>
		<table class="table table-bordered table-striped tabla" border="1">
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			foreach ($promnot as $promnot) {
				$total=round(((($promnot["total"])*100)/$muestra), 2);
				$valor=$valor.'{"dato": "Nota: '.$promnot["prom_notas_bach"].'", "resultado":"'.$total.'"},';
				echo "<tr>
						<td>".$promnot["prom_notas_bach"]."</td>
						<td>".$promnot["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
			
		</table>
	</div>
	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Promedio de notas </h3>
		<div id="promnot" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("promnot", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "resultado",
		  "titleField": "dato",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">
		<h3>Forma de Ingreso</h3>
		<table class="table table-bordered table-striped tabla " border="1">
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			foreach ($formasingresos as $formasingreso) {
				$total=round(((($formasingreso["total"])*100)/$muestra), 2);
				$valor=$valor.'{"dato": "'.$formasingreso["forma_ingreso"].'", "resultado":"'.$total.'"},';
				echo "<tr>
						<td>".$formasingreso["forma_ingreso"]."</td>
						<td>".$formasingreso["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
			
		</table>
	</div>
	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Forma de Ingreso</h3>
		<div id="formasingreso" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("formasingreso", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "resultado",
		  "titleField": "dato",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">
		<h3>Los estudiantes se sienten identificados con el PNF</h3>
		<table class="table table-bordered table-striped tabla" border="1">
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			foreach ($identpnf as $identpnf) {
				if ($identpnf["identidad_pnf"]==1) {
					$titulo="Si";
				}
				else{
					$titulo="No";
				}
				
				$total=round(((($identpnf["total"])*100)/$muestra), 2);
				$valor=$valor.'{"dato": "'.$titulo.'", "resultado":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$identpnf["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
			
		</table>
	</div>
	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico</h3>
		<div id="identpnf" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("identpnf", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "resultado",
		  "titleField": "dato",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">
		<h3>Los estudiantes le gusta el PNF</h3>
		<table class="table table-bordered table-striped tabla" border="1">
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			foreach ($gustapnf as $gustapnf) {
				if ($gustapnf["te_gusta"]==1) {
					$titulo="Si";
				}
				else{
					$titulo="No";
				}
				$total=round(((($gustapnf["total"])*100)/$muestra), 2);
				$valor=$valor.'{"dato": "'.$titulo.'", "resultado":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$gustapnf["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}
			$valor = substr($valor, 0, -1);
			?>
			
		</table>
	</div>
	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico</h3>
		<div id="gustapnf" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("gustapnf", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "resultado",
		  "titleField": "dato",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>