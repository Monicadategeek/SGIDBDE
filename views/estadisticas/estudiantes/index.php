
<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modulo de Estadísticas
            <small></small>
        </h1>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Estadísticas Estudiantiles</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="col-xs-12 col-md-12 col-xl-12">
                <div class="form-group col-xs-3 col-md-3 col-xl-3">
                    <label class="control-label" for="tipo">Tipo de Estadística</label>
                    <select name="tipo" id="tipo" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
                        <option value="1">Datos Académicos</option>
                        <option value="2">Datos SocioEconómicos</option>
                        <option value="3">Datos Fisicos Ambientales</option>
                        <option value="4">Datos Culturales</option>
                        <option value="5">Becarios</option>
                    </select>
                </div>
                <div class="form-group col-xs-6 col-md-6 col-xl-6" id="div1">

                </div>
            </div>

            <div class="col-xs-12 col-md-12 col-xl-12" id="div2">
                
            </div>

            <div class="col-xs-12 col-md-12 col-xl-12" id="estadistica">
                
            </div>
 
        </div>
    </div>
</div>
           
        <!-- /.row -->
 <script type="text/javascript">
   $(document).ready(function()
    {   
        $('.selectpicker').selectpicker('refresh');
        $("#tipo").change(function () { 
            var  datos={"accion":'estudiantes', "tipoestadisticas":$('#tipo option:selected').val() };
            div="div1";
            $('#div2').html('');
            
            $('#estadistica').html('');

            enviar('../Controller/EstadisticasEstudiantilesController.php', datos, div);



        });

        


    });

</script>