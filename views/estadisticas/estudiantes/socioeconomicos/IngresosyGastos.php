
<!-- Styles -->
<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
}
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<script type="text/javascript">
	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
</script>
<h3 align="center">Ingresos y Gastos Estudiantiles <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-md-12 col-xs-12 col-xl-12">

		<h3>Ingresos Estudiantiles mensuales</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<tbody>
				<tr>
					<th>Promedio (Prom)</th>
					<td>Bs. <?php echo $presupuesto["AvgTotIngEst"];?></td>
				
					<th>Minimo</th>
					<td>Bs. <?php echo $presupuesto["MinTotIngEst"];?></td>
				
					<th>Máximo</th>
					<td>Bs. <?php echo $presupuesto["MaxTotIngEst"];?></td>
				</tr>

			</tbody>
		</table>
	</div>

	<div class="col-md-12 col-xs-12 col-xl-12">
		<h3 align="center">Gráfico Nro de Estudiantes que se encuentran por debajo del promedio y por encima del promedio</h3>
		<div id="chartdiv1" class="grafico"></div>
				<?php echo '<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
			  "type": "serial",
			  "theme": "light",
			  "dataProvider": [ {
			    "ingreso": "Muy por Debajo del Prom Bs. '.$presupuesto["minimo_AvgTotIngEst"].'",
			    "estudiantes": '.$presupuesto["Total_minimo_AvgTotIngEst"].'
			  }, {
			    "ingreso": "Menor al Prom Bs. '.$presupuesto["AvgTotIngEst"].'",
			    "estudiantes": '.$presupuesto["Total_menor_AvgTotIngEst"].'
			  }, {
			    "ingreso": "Dentro del Prom Bs. '.$presupuesto["AvgTotIngEst"].'",
			    "estudiantes": '.$presupuesto["Total_promedio_AvgTotIngEst"].'
			  }, {
			    "ingreso": "Encima del Prom Bs. '.$presupuesto["mayor_AvgTotIngEst"].'",
			    "estudiantes": '.$presupuesto["Total_mayor_AvgTotIngEst"].'
			  }],
			  "valueAxes": [ {
			    "gridColor": "#FFFFFF",
			    "gridAlpha": 0.2,
			    "dashLength": 0
			  } ],
			  "gridAboveGraphs": true,
			  "startDuration": 1,
			  "graphs": [ {
			    "balloonText": "Nro de Estudiantes que se encuentran por [[category]]: <b>[[value]]</b>",
			    "fillAlphas": 0.8,
			    "lineAlpha": 0.2,
			    "type": "column",
			    "valueField": "estudiantes"
			  } ],
			  "chartCursor": {
			    "categoryBalloonEnabled": false,
			    "cursorAlpha": 0,
			    "zoomable": false
			  },
			  "categoryField": "ingreso",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "gridAlpha": 0,
			    "tickPosition": "start",
			    "tickLength": 20
			  },
			  "export": {
			    "enabled": true
			  }

			} );
			</script>';
		?>
	</div>


	<div class="col-md-12 col-xs-12 col-xl-12">

		<h3>Ingresos Familiares por Estudiantes</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<tbody>
				<tr>
					<th>Promedio (Prom)</th>
					<td>Bs. <?php echo $presupuesto["AvgTotIngFam"];?></td>
				
					<th>Minimo</th>
					<td>Bs. <?php echo $presupuesto["MinTotIngFam"];?></td>
				
					<th>Máximo</th>
					<td>Bs. <?php echo $presupuesto["MaxTotIngFam"];?></td>
				</tr>

			</tbody>
		</table>
	</div>

	<div class="col-md-12 col-xs-12 col-xl-12">
		<h3 align="center">Gráfico Nro de Familias que se encuentran por debajo del promedio y por encima del promedio</h3>
		<div id="chartdiv2" class="grafico"></div>
				<?php echo '<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
			  "type": "serial",
			  "theme": "light",
			  "dataProvider": [ {
			    "ingreso": "Muy por Debajo del Prom Bs. '.$presupuesto["minimo_AvgTotIngFam"].'",
			    "estudiantes": '.$presupuesto["Total_minimo_AvgTotIngFam"].'
			  }, {
			    "ingreso": "Menor al Prom Bs. '.$presupuesto["AvgTotIngFam"].'",
			    "estudiantes": '.$presupuesto["Total_menor_AvgTotIngFam"].'
			  }, {
			    "ingreso": "Dentro del Prom Bs. '.$presupuesto["AvgTotIngFam"].'",
			    "estudiantes": '.$presupuesto["Total_promedio_AvgTotIngFam"].'
			  }, {
			    "ingreso": "Encima del Prom Bs. '.$presupuesto["mayor_AvgTotIngFam"].'",
			    "estudiantes": '.$presupuesto["Total_mayor_AvgTotIngFam"].'
			  }],
			  "valueAxes": [ {
			    "gridColor": "#FFFFFF",
			    "gridAlpha": 0.2,
			    "dashLength": 0,
			    "title": "Nro de Estudiantes"
			  } ],
			  "gridAboveGraphs": true,
			  "startDuration": 1,
			  "graphs": [ {
			    "balloonText": "Nro de Familias que estan por [[category]]: <b>[[value]]</b>",
			    "fillAlphas": 0.8,
			    "lineAlpha": 0.2,
			    "type": "column",
			    "valueField": "estudiantes"
			  } ],
			  "chartCursor": {
			    "categoryBalloonEnabled": false,
			    "cursorAlpha": 0,
			    "zoomable": false
			  },
			  "categoryField": "ingreso",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "gridAlpha": 0,
			    "tickPosition": "start",
			    "tickLength": 20
			  },
			  "export": {
			    "enabled": true
			  }

			} );
			</script>';
		?>
	</div>

	<div class="col-md-12 col-xs-12 col-xl-12">
		<h3 align="center">Gráfico de Promedio de Nro de Miembros Familiares dependiendo del ingreso promedio</h3>
		<div id="chartdiv3" class="grafico"></div>
		<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv3", {
		    "theme": "light",
		    "type": "serial",
		    "dataProvider": [{
		        "ingreso": "Muy por Debajo del Prom Bs. '.$presupuesto["minimo_AvgTotIngFam"].'",
		        "Familiares": '.$presupuesto["Total_minimo_AvgTotIngFam_Familiares"].',
		        "Trabajan": '.$presupuesto["Total_minimo_AvgTotIngFam_Trabajan"].',
		        "NoTrabajan": '.$presupuesto["Total_minimo_AvgTotIngFam_NoTrabajan"].'
		    }, {
		        "ingreso": "Menor al Prom Bs. '.$presupuesto["AvgTotIngFam"].'",
		        "Familiares": '.$presupuesto["Total_menor_AvgTotIngFam_Familiares"].',
		        "Trabajan": '.$presupuesto["Total_menor_AvgTotIngFam_Trabajan"].',
		        "NoTrabajan": '.$presupuesto["Total_menor_AvgTotIngFam_NoTrabajan"].'
		    }, {
		        "ingreso": "Dentro del Prom Bs. '.$presupuesto["AvgTotIngFam"].'",
		        "Familiares": '.$presupuesto["Total_promedio_AvgTotIngFam_Familiares"].',
		        "Trabajan": '.$presupuesto["Total_promedio_AvgTotIngFam_Trabajan"].',
		        "NoTrabajan": '.$presupuesto["Total_promedio_AvgTotIngFam_NoTrabajan"].'
		    }, {
		        "ingreso": "Encima del Prom Bs. '.$presupuesto["mayor_AvgTotIngFam"].'",
		        "Familiares": '.$presupuesto["Total_mayor_AvgTotIngFam_Familiares"].',
		        "Trabajan": '.$presupuesto["Total_mayor_AvgTotIngFam_Trabajan"].',
		        "NoTrabajan": '.$presupuesto["Total_mayor_AvgTotIngFam_NoTrabajan"].'
		    }],
		    "valueAxes": [{
		        "stackType": "3d",
		        "position": "left",
		        "title": "Nro de Personas",
		    }],
		    "startDuration": 1,
		    "graphs": [{
		        "balloonText": "Miembros Familiares que No Trabajan del ingreso [[category]]: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "Miembros Familiares que No Trabajan",
		        "type": "column",
		        "valueField": "NoTrabajan"
		    },{
		        "balloonText": "Miembros Familiares que Trabajan del ingreso [[category]]: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "Miembros Familiares que Trabajan",
		        "type": "column",
		        "valueField": "Trabajan"
		    }, {
		        "balloonText": "Miembros Familiares del ingreso [[category]]: <b>[[value]]</b>",
		        "fillAlphas": 0.9,
		        "lineAlpha": 0.2,
		        "title": "Miembros Familiares",
		        "type": "column",
		        "valueField": "Familiares"
		    }],
		    "plotAreaFillAlphas": 0.1,
		    "depth3D": 60,
		    "angle": 30,
		    "categoryField": "ingreso",
		    "categoryAxis": {
		        "gridPosition": "start"
		    },
		    "export": {
		    	"enabled": true
		     }
		});
		jQuery(".chart-input").off().on("input change",function() {
			var property	= jQuery(this).data("property");
			var target		= chart;
			chart.startDuration = 0;

			if ( property == "topRadius") {
				target = chart.graphs[0];
		      	if ( this.value == 0 ) {
		          this.value = undefined;
		      	}
			}

			target[property] = this.value;
			chart.validateNow();
		});
		</script>';
				?>
	</div>

	<div class="col-md-12 col-xs-12 col-xl-12">
		<h3 align="center">Gráfico Promedio de Gastos Estudiantiles</h3>
		<div id="chartdiv4" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($gastosInd) and count($gastosInd)>0) {
			  		foreach ($gastosInd as $gasto) {
			  			$cont=$cont+1;
			  			echo '{
							    "year": "'.$gasto["Descripcion"].'",
							    "income": '.$gasto["maxmonto"].',
							    "color": getRandomColor(),
							     "expenses":  '.$gasto["avgmonto"].',
							     "minimo": '.$gasto["minmonto"].'
							  }';

						if ($cont<count($gastosInd)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "Maximo Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "income",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			  , {
			    "id": "graph2",
			    "balloonText": "Promedio Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    
			    "bullet": "round",
			    "lineThickness": 3,
			    "bulletSize": 7,
			    "bulletBorderAlpha": 1,
			    "useLineColorForBulletBorder": true,
			    "bulletBorderThickness": 3,
			    "fillAlphas": 0,
			    "lineAlpha": 1,
			    "title": "Expenses",
			    "valueField": "expenses",
			    "dashLengthField": "dashLengthLine",
			     "legendValueText": "[[value]] "
			  },
			{
			    "id": "graph3",
			    "balloonText": "Minimo Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    
			    "bullet": "round",
			    "lineThickness": 3,
			    "bulletSize": 7,
			    "bulletBorderAlpha": 1,
			    "useLineColorForBulletBorder": true,
			    "bulletBorderThickness": 3,
			    "fillAlphas": 0,
			    "lineAlpha": 1,
			    "title": "Expenses",
			    "valueField": "minimo",
			    "dashLengthField": "dashLengthLine",
			    "legendValueText": "[[value]] "
			  },

			   ],
			  "categoryField": "year",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "axisAlpha": 0,
			    "tickLength": 0
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
	</div>




	<div class="col-md-12 col-xs-12 col-xl-12">
		<h3 align="center">Gráfico Promedio de Gastos Familiares</h3>
		<div id="chartdiv5" class="grafico"></div>
			<?php

			echo '<script>
			var chart = AmCharts.makeChart( "chartdiv5", {
			  "type": "serial",
			  "addClassNames": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginLeft": 30,
			  "marginRight": 8,
			  "marginTop": 10,
			  "marginBottom": 26,
			  "balloon": {
			    "adjustBorderColor": false,
			    "horizontalPadding": 10,
			    "verticalPadding": 8
			  },

			  "dataProvider": [ 
			  ';

			  $cont=0;
			  	if (isset($gastosFam) and count($gastosInd)>0) {
			  		foreach ($gastosInd as $gasto) {
			  			$cont=$cont+1;
			  			echo '{
							    "year": "'.$gasto["Descripcion"].'",
							    "income": '.$gasto["maxmonto"].',
							    "color": getRandomColor(),
							     "expenses":  '.$gasto["avgmonto"].',
							     "minimo": '.$gasto["minmonto"].'
							  }';

						if ($cont<count($gastosInd)) {
							echo ',';
						}
			  		}
			  	}

			  echo '  
			   ],
			  "valueAxes": [ {
			    "axisAlpha": 0,
			    "position": "left"
			  } ],
			  "startDuration": 1,
			  "graphs": [ 
				
				{
			    "alphaField": "alpha",
			    "fillColorsField": "color",
			     "balloonText": "Maximo Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    "fillAlphas": 1,
			    "title": "Income",
			    "type": "column",
			    "valueField": "income",
			    "dashLengthField": "dashLengthColumn",
			     "legendValueText": "[[value]] "

			  },

			  , {
			    "id": "graph2",
			    "balloonText": "Promedio Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    "bullet": "round",
			    "lineThickness": 3,
			    "bulletSize": 7,
			    "bulletBorderAlpha": 1,
			    "useLineColorForBulletBorder": true,
			    "bulletBorderThickness": 3,
			    "fillAlphas": 0,
			    "lineAlpha": 1,
			    "title": "Expenses",
			    "valueField": "expenses",
			    "dashLengthField": "dashLengthLine",
			     "legendValueText": "[[value]] "
			  },
			{
			    "id": "graph3",
			    "balloonText": "Minimo Gastos de <b>[[category]]: Bs. [[value]]</b>",
			    "bullet": "round",
			    "lineThickness": 3,
			    "bulletSize": 7,
			    "bulletBorderAlpha": 1,
			    "useLineColorForBulletBorder": true,
			    "bulletBorderThickness": 3,
			    "fillAlphas": 0,
			    "lineAlpha": 1,
			    "title": "Expenses",
			    "valueField": "minimo",
			    "dashLengthField": "dashLengthLine",
			    "legendValueText": "[[value]] "
			  },

			   ],
			  "categoryField": "year",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "axisAlpha": 0,
			    "tickLength": 0
			  },
			  "export": {
			    "enabled": true
			  }
			} );
			</script>';
?>	
	</div>
	
</div>

