 <label class="control-label" for="tipoestadisticas">Tipo de Estadistica Socioeconómica</label>
<select id="tipoestadistica" name="tipoestadistica" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">
	<option value="1">Traslado y Trabajo</option>
	<option value="2">Personal y Familia</option>
	<option value="3">Ingresos y Gastos</option>
</select>


<script type="text/javascript">
 $('.selectpicker').selectpicker('refresh');
            
	$(document).ready(function()
    {
        $('.selectpicker').selectpicker('refresh');
        $("#tipoestadistica").change(function () { 
            var  datos={"accion":'fechas', "tipoestadistica": $('#tipoestadistica option:selected').val() };
            var div="div2";
            enviar('../Controller/EstadisticasEstudiantilesController.php', datos, div);
            $('#estadistica').html('');

        });    
    });
</script>