<!-- Styles -->
<style>
#chartdiv {
	width		: 100%;
	height		: 500px;
	font-size	: 11px;
} 
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Ingresos y Gastos <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Ingreso Estudiantil</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$Total=0;
			$valor="";
			$titulo='NO';
			if ($probtrasla["NO"]!=0) {
				$Total=round(((($probtrasla["Total"])*100)/$probtrasla["NO"]), 2);
			}
			else{
				$Total=0;
			}
			
			$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$Total.'"},';
			echo "<tr>
					<td>".$titulo."</td>
					<td>".$probtrasla["NO"]."</td>
					<td>".$Total."%</td>
				</tr>";

			$titulo='SI';
			if ($probtrasla["SI"]!=0) {
				$Total=round(((($probtrasla["Total"])*100)/$probtrasla["SI"]), 2);
			}
			else{
				$Total=0;
			}
			
			$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$Total.'"},';
			echo "<tr>
					<td>".$titulo."</td>
					<td>".$probtrasla["SI"]."</td>
					<td>".$Total."%</td>
				</tr>";
			$titulo='Total';
			echo "<tr>
					<td>".$titulo."</td>
					<td>".$probtrasla["Total"]."</td>
					<td>".$Total."%</td>
				</tr>";



				
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$Total.'"},';
				
			
			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Problemas para trasladarse al IUTOMS</h3>
		<div id="chartdiv1" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv1", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Tipo de Traslado al IUTOMS</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			$totalpo=0;
			foreach ($comotrasla as $comotrasla) {
				$titulo=$comotrasla["descripcion"];
				$totalpo=$totalpo+$comotrasla["total"];

				$total=round(((($comotrasla["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$comotrasla["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Tipo de Traslado al IUTOMS</h3>
		<div id="chartdiv2" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv2", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes que Trabajan</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			$totalpo=0;
			foreach ($trabaja as $trabaja) {
				$titulo=$trabaja["Trabaja"];
				$totalpo=$totalpo+$trabaja["total"];

				$total=round(((($trabaja["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$trabaja["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Estudiantes que Trabajan</h3>
		<div id="chartdiv3" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv3", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes que Han Trabajado</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<?php
			$total=0;
			$valor="";
			$totalpo=0;
			foreach ($trabajado as $trabajado) {
				$titulo=$trabajado["HaTrabajado"];
				$totalpo=$totalpo+$trabajado["total"];

				$total=round(((($trabajado["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$trabajado["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
		<h3 align="center">Gráfico Estudiantes que Han Trabajado</h3>
		<div id="chartdiv4" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv4", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>