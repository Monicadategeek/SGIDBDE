
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Personal y Familia <br><small><b>Estudiantes que han realizado la encuesta</b> <?php echo $poblacion?></small></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Cuantas comidas consume al dia el Estudiante</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
			foreach ($comidas as $comida) {
				$titulo=$comida["Titulo"];
				$totalpo=$totalpo+$comida["total"];

				$total=round(((($comida["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$comida["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Comidas al dia  que consume el Estudiante</h3>
		<div id="chartdiv1" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv1", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>




<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes con Hijos</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
			foreach ($hijos as $hijo) {
				$titulo=$hijo["Titulo"];
				$totalpo=$totalpo+$hijo["total"];

				$total=round(((($hijo["total"])*100)/$poblacion), 2);
				$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
				echo "<tr>
						<td>".$titulo."</td>
						<td>".$hijo["total"]."</td>
						<td>".$total."%</td>
					</tr>";
			}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Estudiantes con Hijos</h3>
		<div id="chartdiv2" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv2", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes que viven con sus Hijos</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($hijosvivenc as $hijo) {
					$titulo=$hijo["Titulo"];
					$totalpo=$totalpo+$hijo["total"];

					$total=round(((($hijo["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$hijo["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Estudiantes que viven con sus Hijos</h3>
		<div id="chartdiv3" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv3", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>

<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes con Pareja</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($parejas as $parejas) {
					$titulo=$parejas["Titulo"];
					$totalpo=$totalpo+$parejas["total"];

					$total=round(((($parejas["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$parejas["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Estudiantes con Pareja</h3>
		<div id="chartdiv4" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv4", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Estudiantes con Personas Dependientes de Ellos</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($personasdependiente as $personas) {
					$titulo=$personas["Titulo"];
					$totalpo=$totalpo+$personas["total"];

					$total=round(((($personas["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$personas["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

			$total=round(((($totalpo)*100)/$poblacion), 2);

			echo "<tr>
					<td>total</td>
					<td>".$totalpo."</td>
					<td>".$total."%</td>
				</tr>";

			$valor = substr($valor, 0, -1);
			?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Estudiantes con Personas Dependientes de Ellos</h3>
		<div id="chartdiv5" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv5", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>


<div class="col-xs-12 col-md-12 col-xl-12">

	<div class="col-xs-4 col-md-4">

		<h3>Miembros Familiares por grupo familiar de los Estudiantes</h3>
		<table class="table table-bordered table-striped tabla" border="1" >
			<thead>
				<th>Tipo</th>
				<th>Total</th>
				<th>%</th>
			</thead>
			<tbody>
				<?php
				$total=0;
				$valor="";
				$totalpo=0;
				foreach ($familias as $familia) {
					$titulo=$familia["Titulo"];
					$totalpo=$totalpo+$familia["total"];

					$total=round(((($familia["total"])*100)/$poblacion), 2);
					$valor=$valor.'{"country": "'.$titulo.'", "visits":"'.$total.'"},';
					echo "<tr>
							<td>".$titulo."</td>
							<td>".$familia["total"]."</td>
							<td>".$total."%</td>
						</tr>";
				}

				$total=round(((($totalpo)*100)/$poblacion), 2);

				echo "<tr>
						<td>total</td>
						<td>".$totalpo."</td>
						<td>".$total."%</td>
					</tr>";

				$valor = substr($valor, 0, -1);

				if ($totalpo!=$poblacion) {
					$totalpootro=$poblacion-$totalpo;
					$totalotro=round(((($totalpootro)*100)/$poblacion), 2);

					echo "<tr>
							<td>No Tienen familia registrada</td>
							<td>".$totalpootro."</td>
							<td>".$totalotro."%</td>
						</tr>";

				}
				?>

			</tbody>
		</table>
	</div>

	<div class="col-md-8 col-xs-8">
	
		<h3 align="center">Gráfico Miembros Familiares por grupo familiar de los Estudiantes</h3>
		<div id="chartdiv6" class="grafico"></div>
				<?php echo '<script>
		var chart = AmCharts.makeChart("chartdiv6", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": ['.$valor.'],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 1,
		  "minRadius":100,
		  "labelRadius": 25,
		  "innerRadius": "30%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='."'"."font-size:14px"."'".'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		});

		</script>';
		?>
	</div>
</div>
