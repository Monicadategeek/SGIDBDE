<label class="control-label" for="tipoestadisticas">Seleccione el rango de fechas</label>
<p>Puede seleccionar por selección con el Registro Fechas para las Encuestas Estudiantiles</p>

<input type="hidden" id="startDate" value="<?php echo $poblacion?>">

<div class="form-group col-xs-4 col-md-4 col-xl-4" >
    <select id="selectfecha" name="selectfecha" placeholder="Seleccione" title="Seleccione" class="form-control selectpicker">  
        <option value="0">Buscar por Rango de Fechas</option>     
        <?php
            foreach ($fechas as $fecha) {
                echo '<option value="'.$fecha["Fechainicio"].",".$fecha["Fechafin"].'">'.$fecha["Fechainicio"]." ".$fecha["Fechafin"].'</option>';
            }
        ?>

    </select>

</div>

<div class="form-group col-xs-5 col-md-5 col-xl-5" id="rango">
    <div class=" input-daterange input-group" id="datepicker" style="margin: 0.5em 0;">
      <span class="input-group-addon">Desde</span>
      <input type="text" class="form-control" name="Fechainicio" id="Fechainicio" maxlength="10" />
      <span class="input-group-addon">Hasta</span>
      <input type="text" class=" form-control" name="Fechafin" id="Fechafin" maxlength="10" />
    </div>

</div>
<div class="form-group col-xs-3 col-md-3 col-xl-3" >
<button onclick="buscar();" class="btn btn-default" style="margin: 0.5em 0;"><i class="fa fa-search"></i> Buscar</button>
</div>
<br>
<div class="form-group col-xs-12 col-md-12 col-xl-12" style="text-align: left;">
    <a class="btn btn-default buttons-pdf buttons-html5" href="#" onclick="tomarcaptura('estadistica', 'EstadisticasSocioEconomicas-SGIDBDE');" id="botonimprimir" style="display: none;"><span><i class="fa fa-print"></i> Imprimir</span></a>
</div>




<script type="text/javascript">
	$(document).ready(function()
    {
        $('#rango').hide();
        $('.input-daterange').datepicker({
        format: 'dd-mm-yyyy',
        startDate: '-'+$('#startDate').val()+'d', 
        endDate:'1d',        
        language: 'es',

        });
        $('.selectpicker').selectpicker('refresh');
        $("#selectfecha").change(function () { 
            if ($('#selectfecha').val()==0) {
                $('#rango').show();

            } 
            else{
                $('#Fechainicio').val("");
                $('#Fechafin').val(""); 
                $('#rango').hide();
            }

                 
        });


    });

    function buscar(){
        if ($('#selectfecha').val()=='') {
            alert("debe seleccionar una opcion");
            return false;
        }

        if ($('#selectfecha').val()==0) {
             if ($('#Fechainicio').val()=='' || $('#Fechafin').val()=='') {
                alert("debe seleccionar el rango de fechas");
                return false;
            }
        }        
        var fechas;
        if ($('#selectfecha').val()==0) {
            fechas=$('#Fechainicio').val()+","+$('#Fechafin').val();
        }
        else{
            fechas=$('#selectfecha').val();
        }
        var  datos={"accion":'interes', "fechas":fechas, "tipoestadistica": $('#tipoestadistica option:selected').val()};
         div="estadistica";
        enviar('../Controller/EstadisticasEstudiantilesController.php', datos, div);
        $('#botonimprimir').show();
        
    }
</script>