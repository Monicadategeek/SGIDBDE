<script type="text/javascript">
	$('#modalpresupuestotitulo').hide();
   $('#modalpresupuestofooter').hide();
</script>
<!-- Styles -->
<style>
.tabla{
	margin-top: 45px;
}
.grafico {
  width: 100%;
  height: 270px;
  font-size: 11px;
}				
</style>

<h3 align="center">Datos de Becarios <br></h3>
<br>
<br>

<div class="col-xs-12 col-md-12 col-xl-12">
	<h3>Becarios Registrados</h3>
	

		
	<table class="table table-bordered table-striped tabla" border="1" id="posiblesbecarios">
		<thead>
			<th>Nro</th>
			<th>Cédula</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Sexo</th>
			<th>Carrera</th>
			<th>Tray</th>
			<th>Trim</th>
			<th>Sec</th>
			<th>Promedio Nota</th>
			<th>Estatus</th>
		</thead>
		<tfoot>
			<th>Nro</th>
			<th>Cédula</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>Sexo</th>
			<th>Carrera</th>
			<th>Tray</th>
			<th>Trim</th>
			<th>Sec</th>
			<th>Promedio Nota</th>
			<td>Estatus</td>
			</tfoot>
		<tbody>
			<?php
			if (count($becarios>0)) {
				$nro=0;
				foreach ($becarios as $becario) {
					$nro=$nro+1;
					echo "<tr>
							<td>".$nro."</td>
							<td>".$becario["cedula"]."</td>
							<td>".$becario["nombres"]."</td>
							<td>".$becario["apellidos"]."</td>
							<td>".$becario["sexo"]."</td>
							<td>".$becario["cod_carrera"]."</td>
							<td>".$becario["trayecto"]."</td>
							<td>".$becario["trimestre"]."</td>
							<td>".$becario["seccion"]."</td>
							<td>".$becario["nota"]."</td>
							<td>".$becario["Estado"]."</td>

						</tr>";
				}
			}
			

			?>


		</tbody>
	</table>

        <!-- /.row -->
 <script type="text/javascript">
    $(document).ready(function(){
$('#posiblesbecarios').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Disculpe, No existen registros de departamentos",
            "info": "Mostrando paginas _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "decimal": ",",
            "thousands": "."
        },
        dom: "Bfrtip",
        buttons: [
        {
            extend: 'pdfHtml5',
            text: '<i class="fa fa-print" ></i> Imprimir',
            filename:"Posibles Becarios - SGIDBDE",
            title:"Estudiantes potenciales para becas",
            exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7,8],
                        search: 'applied',
                        order: 'applied'
            },
            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    customize: function (doc) {
                        doc.content[1].table.widths = 
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.content.splice(0,0);
                        //Remove the title created by datatTables
                        //Create a date string that we use in the footer. Format is dd-mm-yyyy
                        var now = new Date();
                        var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                        // Logo converted to base64
                        // var logo = getBase64FromImageUrl('https://datatables.net/media/images/logo.png');
                        // The above call should work, but not when called from codepen.io
                        // So we use a online converter and paste the string in.
                        // Done on http://codebeautify.org/image-to-base64-converter
                        // It's a LONG string scroll down to see the rest of the code !!!
                        // A documentation reference can be found at
                        // https://github.com/bpampuch/pdfmake#getting-started
                        // Set page margins [left,top,right,bottom] or [horizontal,vertical]
                        // or one number for equal spread
                        // It's important to create enough space at the top for a header !!!
                        doc.pageMargins = [20,100,20,30];
                        // Set the font size fot the entire document
                        doc.defaultStyle.fontSize = 9;
                        // Set the fontsize for the table header
                        doc.styles.tableHeader.fontSize = 9;
                        // Create a header object with 3 columns
                        // Left side: Logo
                        // Middle: brandname
                        // Right side: A document title
                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        image: logo,
                                        width: 150
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 12,
                                        text: 'Sistema para la Gestion de Informacion en la División de Bienestar y Desarrollo Estudiantil'
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Create a footer object with 2 columns
                        // Left side: report creation date
                        // Right side: current page and total pages
                        doc['footer']=(function(page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        text: ['Generado el: ', { text: jsDate.toString() }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['pagina ', { text: page.toString() },  ' de ', { text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['hLineWidth'] = function(i) { return .5; };
                        objLayout['vLineWidth'] = function(i) { return .5; };
                        objLayout['hLineColor'] = function(i) { return '#aaa'; };
                        objLayout['vLineColor'] = function(i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function(i) { return 4; };
                        objLayout['paddingRight'] = function(i) { return 4; };
                        doc.content[0].layout = objLayout;
                        
                }

            }
        ],
        responsive: true
    });
// Setup - add a text input to each footer cell
    $('#posiblesbecarios tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control" type="text" placeholder="'+title+'" />' );
    } );
 
    // DataTable
    var table = $('#posiblesbecarios').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


     table.columns([4,5,6,7,8]).every( function () {
        
        var column = this;
        var select = $('<select class="selectpicker form-control"><option value="">Buscar</option></select>')
          .appendTo($(column.footer()).empty())
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
              .search(val ? '^' + val + '$' : '', true, false)
              .draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>')
        });

    });
     $('.selectpicker').selectpicker('refresh');
});




</script>

</div>

