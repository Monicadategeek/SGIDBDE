<?php 
	/**
	* Clase GestionCitas
	* A diferencia de la clase Citas, la clase GestionCitas es usada por los usuarios que no sean estudiantes, a diferencia de la clase Citas que es usada por los usuarios estudiantes 
	* Comparte la tabla Citas en la Base de Datos
	* Es llamado en los Controladores GestionCitasController, AtencionCitasController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Citas_index 
	*
	* ejem: get_Departamentos_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	
	class GestionCitas

	{
		private $db;
		private $citas;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] citas
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->citas =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->comprobar = new SeguridadDatos;
			$this->tabla=5; //Listo
			
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] citas
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->citas["IdCitas"])) {
				$this->comprobar->ComprobarNumeric($this->citas["IdCitas"]);
			}
			if (isset($this->citas["Cedula"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Cedula"]);
			}

			if (isset($this->citas["Fecha"])) {
				$this->comprobar->ComprobarFecha($this->citas["Fecha"]);
			}
 
			if (isset($this->citas["Hora"])) {
				$this->comprobar->ComprobarFechaHora($this->variable["Hora"], 'H:i');
			}
			if (isset($this->citas["Servicio"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Servicio"]);
			}

			if (isset($this->citas["IdEstudiante"])) {
				$this->comprobar->ComprobarNumeric($this->citas["IdEstudiante"]);
			}

			if (isset($this->citas["Asistencia"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Asistencia"]);
			}
			if (isset($this->citas["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Responsable"]);
			}
		}

		/**
		* Consulto las citas de todos los departamentos para una fecha especifica (SOLO PARA EL SUPERADMINISTRADOR)
		* @method consultar(), ComprobarNumeric(), ComprobarFecha()
		* @param [date] $Responsable, [integer] $Responsable 
		* @return [array] [citas]
		*/
		public function get_GestionCitas_index_admin($fecha, $Responsable){
			//get_Citas_index_admin
			$this->comprobar->ComprobarNumeric($Responsable);
			$this->comprobar->ComprobarFecha($fecha);
			$sql=' SELECT c."IdCitas", c."Fecha", c."Hora", c."Asistencia", e."nombres", e."apellidos", e."sexo", e."cedula", (SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Asistencia") as "TipoAsistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto", d."TipoDepto" as "IdTipoDepto", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  
			FROM Citas c, Departamentos d, Estudiantes e  

			where c."Servicio"=d."IdDepartamentos" and c."IdEstudiante"=e."num_est" and c."Asistencia" in (1,2,4,5,6)  and  c."Fecha"= '."'".$fecha."'".' order by c."Hora" asc';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				if ($filas["Asistencia"]==1 and date('Y-m-d', strtotime($fecha))==date('Y-m-d') ) 
                { 
                    $filas["botones"]='
                        <div class="btn-group">';
                        if ($filas["IdTipoDepto"]==1) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(1,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==2) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(2,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==4) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(4,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==5) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(5,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }                       

                       $filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-sm  btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a onclick="atender(6,'.$filas["IdCitas"].');"><i class="fa fa-window-close-o" aria-hidden="true"></i> Inasistencia Estudiantil</a>
                                </li>
                                <li>
                                    <a onclick="atender(5,'.$filas["IdCitas"].');"><i class="fa fa-window-close-o" aria-hidden="true"></i> Inasistencia Médica</a>
                                </li> 
                            </ul>
                        </div>';
                }
                else{
                	if ($filas["Asistencia"]==2) {
                		$filas["botones"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$filas["IdCitas"].');"><i class="fa fa-book"></i></button>';
                		
                	}
                	else{
                		$filas["botones"]='';
                	}
                	
                }

				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				$this->citas[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->citas;
		}


		/**
		* Consulto las citas para el o los departamentos del trabajador que esta iniciado sesion para una fecha especifica 
		* @method consultar(), ComprobarNumeric(), ComprobarFecha()
		* @param [date] $Responsable, [integer] $Servicio , [integer] $Responsable 
		* @return [array] [citas]
		*/
		public function get_GestionCitas_index($fecha, $Servicio, $Responsable){
			//get_Citas_index
			$this->comprobar->ComprobarNumeric($Responsable);
			$this->comprobar->ComprobarCaracteres($Servicio);
			$this->comprobar->ComprobarFecha($fecha);
			$sql=' SELECT c."IdCitas", c."Fecha", c."Hora", c."Asistencia", e."nombres", e."apellidos", e."sexo", e."cedula", (SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Asistencia") as "TipoAsistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto", d."TipoDepto" as "IdTipoDepto", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  
			FROM Citas c, Departamentos d, Estudiantes e  

			where c."Servicio"=d."IdDepartamentos" and c."IdEstudiante"=e."num_est" and c."Asistencia" in (1,2,4,5,6)  and  c."Fecha"= '."'".$fecha."'".' and c."Servicio" in ('.$Servicio.') order by c."Hora" asc';		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["Asistencia"]==1 and date('Y-m-d', strtotime($fecha))==date('Y-m-d') ) 
                { 

                    $filas["botones"]='
                        <div class="btn-group">';
                        if ($filas["IdTipoDepto"]==1) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(1,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==2) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(2,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==4) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(4,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }
                        else if ($filas["IdTipoDepto"]==5) {
                        	$filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-success btn-sm" onclick="atencion(5,'.$filas["IdCitas"].');">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i> Atender
                            </button>';
                        }                       


                        $filas["botones"]=$filas["botones"].'<button type="button" class="btn btn-sm  btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a onclick="atender(6,'.$filas["IdCitas"].');"><i class="fa fa-window-close-o" aria-hidden="true"></i> Inasistencia Estudiantil</a>
                                </li>
                                <li>
                                    <a onclick="atender(5,'.$filas["IdCitas"].');"><i class="fa fa-window-close-o" aria-hidden="true"></i> Inasistencia Médica</a>
                                </li> 
                            </ul>
                        </div>';
                }
                else{
                	$filas["botones"]='';
                }			


				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));
				$this->citas[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->citas;
		}


		
		/**
		* Consulto la cita
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdCitas
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Citas_show($IdCitas){
			$this->comprobar->ComprobarNumeric($IdCitas);
			 $sql='SELECT c."Cedula", c."IdEstudiante", c."Fecha", c."IdCitas", c."Hora", c."Asistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto", d."TipoDepto" as "numTipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and c."IdCitas"='.$IdCitas;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));

				$this->citas[] =$filas;
			}
			return $this->citas[0];
		}
		
		/**
		* Modifico el campo Asistencia de la cita seleccionada
		* @method consultar(), ComprobarNumeric(), ComprobarFecha()
		* @param [date] $citas
		* @return [integer] 1
		*/
		public function set_GestionCitas_Atender($citas){ 
			//set_Citas_Atender
			$this->citas = $citas;
			$this->Comprobacion();

			$sql= 'UPDATE Citas set "Asistencia"='.$this->citas["Asistencia"].' WHERE "IdCitas"='.$this->citas["IdCitas"];
			if ($this->db->consultar($sql)) {
				$this->OperacionesSistemicas->array_OperacionesSistemicas($this->citas["Responsable"],2,$this->citas["IdCitas"],$this->tabla);
				return "1";
			}
			else{
				return "Error al interactuar con la base de datos";
			}
		}

		/**
		* Modifico el campo Asistencia de la cita seleccionada si el mismo campo sea 1
		* @method consultar(), ComprobarNumeric(), ComprobarFecha()
		* @param [array] $citas, [integer] $Servicio 
		* @return [integer] 1
		*/
		public function set_GestionCitas_Cancelar($citas){ 
		//set_Citas_Cancelar 
			$this->citas = $citas;
			$this->Comprobacion();

			$sql= 'UPDATE Citas set "Asistencia"='.$this->citas["Asistencia"].' WHERE "Asistencia"=1 and "Servicio"='.$this->citas["Servicio"].' and "Fecha"='."'".$this->citas["Fecha"]."'";
			if ($this->db->consultar($sql)) {

				$sql=' SELECT "IdCitas"	FROM Citas c where "Asistencia"='.$this->citas["Asistencia"].' and "Servicio"='.$this->citas["Servicio"].' and "Fecha"='."'".$this->citas["Fecha"]."'";		
				$consulta = $this->db->consultar($sql);
				while ($filas=pg_fetch_assoc($consulta)) {

					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->citas["Responsable"],9,$filas["IdCitas"],$this->tabla);
				}
				
				return "1";
			}
			else{
				return "2";
			}
		}


	}
?>