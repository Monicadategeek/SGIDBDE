<?php 
class articulo

{
	private $db;
	private $articulo;
	private $OperacionesSistemicas;
	private $tabla;
	private $comprobar;

	public function __construct(){
		require_once("../Controller/conectar.php");
		require_once("SeguridadDatos.php");
		require_once("OperacionesSistemicas.php");
		
		$this->db = new conexion;
		$this->articulo= array();
		$this->comprobar = new SeguridadDatos;
		$this->OperacionesSistemicas= new OperacionesSistemicas();
		$this->tabla=8;
	}

	private function Comprobacion()
		{
			if (isset($this->articulo["Id"])) {
				$this->comprobar->ComprobarNumeric($this->articulo["Id"]);
			}

			if (isset($this->articulo["Titulo"])) {
				$this->comprobar->ComprobarCaracteres($this->articulo["Titulo"]);
			}

			if (isset($this->articulo["Subtitulo"])  ) {
				$this->comprobar->ComprobarCaracteres($this->articulo["Subtitulo"]);				
			}
			if (isset($this->articulo["Contenido"])) {
				$this->comprobar->ComprobarCaracteres($this->articulo["Contenido"]);
			}
			if (isset($this->articulo["ImagenPrincipal"])) {
				$this->comprobar->ComprobarCaracteres($this->articulo["ImagenPrincipal"]);
			}

			if (isset($this->articulo["Fecha_Registro"])) {
				$this->comprobar->ComprobarFecha($this->articulo["Fecha_Registro"]);
			}

			if (isset($this->articulo["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->articulo["Responsable"]);
			}

			if (isset($this->articulo["Estado"])) {
				$this->comprobar->ComprobarNumeric($this->articulo["Estado"]);
			}
			if (isset($this->articulo["DptoAsociado"])) {
				$this->comprobar->ComprobarNumeric($this->articulo["DptoAsociado"]);
			}
		

		}

 
	public function get_articulo_index_in($val){
		$this->comprobar->ComprobarCaracteres($val);

		echo $sql='SELECT b."Titulo", b."Subtitulo", b."Fecha_Registro", b."ImagenPrincipal",b."Estado", u."nombre", u."apellido", b."Id", (SELECT "Descripcion" from departamento d where d."IdDepartamento"=b."DptoAsociado") as "DptoAsociado", concat(u."nombre", '."' '".', u."apellido") as "Responsable" FROM "articulo" as b INNER JOIN "usuarios" as u on b."Responsable"= u."Num_Usuario" where b."Responsable"= u."Num_Usuario" and b."DptoAsociado" in ('.$val.')  order by "Fecha_Registro" desc';
		$consulta= $this->db->consultar($sql);

		while ($filas=pg_fetch_assoc($consulta)) {
			if ($filas["ImagenPrincipal"]!='') {
				$filas["ImagenPrincipal"]='<img class="img-responsive avatar-view" src="'."../web/articulos/".$filas["ImagenPrincipal"].'" alt="'.$filas["Titulo"].'" title="'.$filas["Titulo"].'" style="    width: 203px; height: 161px;">';
			}

			$filas["Fecha_Registro"]=date('d-m-Y', strtotime($filas["Fecha_Registro"]));
			if ($filas["Estado"]==1) {
                $filas["Estado"]="Público";
            }
            else{
                $filas["Estado"]="No Público";
            }

			$this->articulo[] =$filas;
		}
		return $this->articulo;
	}

	
	public function get_articulo_blog(){

		$sql='SELECT b."Titulo", b."Subtitulo", b."Fecha_Registro", (SELECT "Descripcion" FROM Departamento where "IdDepartamento"=b."DptoAsociado") as "DptoAsociado", b."ImagenPrincipal",b."Estado", u."nombre", u."apellido", b."Id", (SELECT "Descripcion" from departamento d where d."IdDepartamento"=b."DptoAsociado") as "DptoAsociado", concat(u."nombre", '."' '".', u."apellido") as "Responsable" FROM "articulo" as b INNER JOIN "usuarios" as u on b."Responsable"= u."Num_Usuario" where b."Responsable"= u."Num_Usuario" and b."Estado"=1 order by "Fecha_Registro" desc';
		$consulta= $this->db->consultar($sql);

		while ($filas=pg_fetch_assoc($consulta)) {
 
			if ($filas["ImagenPrincipal"]!='') {
				$filas["ImagenPrincipal"]='<img class="img-responsive" src="'."../web/articulos/".$filas["ImagenPrincipal"].'" alt="'.$filas["Titulo"].'" title="'.$filas["Titulo"].'" style="    width: 100%;">';
			}

			$filas["Fecha_Registro"]=date('d-m-Y', strtotime($filas["Fecha_Registro"]));
			if ($filas["Estado"]==1) {
                $filas["Estado"]="Público";
            }
            else{
                $filas["Estado"]="No Público";
            }

			$this->articulo[] =$filas;
		}
		return $this->articulo;
	}

	public function get_articulo_index(){

		$sql='SELECT b."Titulo", b."Subtitulo", b."Fecha_Registro", b."ImagenPrincipal",b."Estado", u."nombre", u."apellido", b."Id", (SELECT "Descripcion" from departamento d where d."IdDepartamento"=b."DptoAsociado") as "DptoAsociado", concat(u."nombre", '."' '".', u."apellido") as "Responsable" FROM "articulo" as b INNER JOIN "usuarios" as u on b."Responsable"= u."Num_Usuario" where b."Responsable"= u."Num_Usuario" order by "Fecha_Registro" desc';
		$consulta= $this->db->consultar($sql);

		while ($filas=pg_fetch_assoc($consulta)) {

			if ($filas["ImagenPrincipal"]!='') {
				$filas["ImagenPrincipal"]='<img class="img-responsive" src="'."../web/articulos/".$filas["ImagenPrincipal"].'" alt="'.$filas["Titulo"].'" title="'.$filas["Titulo"].'" style="    width: 100%;">';
			}

			$filas["Fecha_Registro"]=date('d-m-Y', strtotime($filas["Fecha_Registro"]));
			if ($filas["Estado"]==1) {
                $filas["Estado"]="Público";
            }
            else{
                $filas["Estado"]="No Público";
            }

			$this->articulo[] =$filas;
		}
		return $this->articulo;
	}


	public function set_articulo_store($articulo){
		$this->articulo = $articulo;
			$this->Comprobacion();
		$sql='INSERT INTO articulo("Titulo","Subtitulo","Contenido","ImagenPrincipal","Fecha_Registro","Responsable","Estado", "DptoAsociado") VALUES(';
		$values="'".$this->articulo["Titulo"]."', '".$this->articulo["Subtitulo"]."', '".$this->articulo["Contenido"]."', '".$this->articulo["ImagenPrincipal"]."', now(),".$this->articulo["Responsable"].", ".$this->articulo["Estado"].", ".$this->articulo["DptoAsociado"].");";

		$sql=$sql.$values;

		if ($this->db->consultar($sql)) {

			$sql='SELECT "Id","Titulo","Subtitulo","Contenido","ImagenPrincipal","Fecha_Registro","ImagenPrincipal" FROM articulo WHERE "Id"=(SELECT max("Id") AS Id FROM articulo)';
			
			$consulta = $this->db->consultar($sql);
			$filas=pg_fetch_assoc($consulta);	

			return $filas;
		}
		else{
			return "Error";
		}

	}

		public function get_articulo_show($id){

			$sql='SELECT "Id","Titulo","Subtitulo","ImagenPrincipal","Contenido", "Fecha_Registro", (SELECT "Descripcion" FROM Departamento where "IdDepartamento"="DptoAsociado") as "DptoAsociado", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"="Responsable") as "Responsable" FROM articulo where "Id"='.$id;		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				
				if ($filas["ImagenPrincipal"]!='') {
				$filas["ImagenPrincipal"]='<img class="img-responsive" src="'."../web/articulos/".$filas["ImagenPrincipal"].'" alt="'.$filas["Titulo"].'" title="'.$filas["Titulo"].'" style="    width: 100%;">';
				}

				$filas["Fecha_Registro"]=date('d F Y g:ia', strtotime($filas["Fecha_Registro"]));
				$this->articulo[] =$filas;
			}

			//$this->OperacionesSistemicas->array_OperacionesSistemicas($departamentos["Responsable"],6,$filas["IdDepartamentos"],$this->tabla);
			return $this->articulo[0];
		}
		

	public function get_articulo_edit($id){

			$this->comprobar->ComprobarNumeric($id);


			$sql='SELECT * FROM articulo where "Id"='.$id;


			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->articulo[] =$filas;
			}
			return $this->articulo[0];
		}

		

	public function set_articulo_update($articulo){
		$this->articulo = $articulo;
		$this->Comprobacion();

     	$sql= 'UPDATE articulo SET ("Titulo", "Subtitulo", "Contenido", "ImagenPrincipal", "Responsable", "DptoAsociado", "Estado") = 
     	('."'".$this->articulo["Titulo"]."', '".$this->articulo["Subtitulo"]."', '".$this->articulo["Contenido"]."', '".$this->articulo["ImagenPrincipal"]."', ".$this->articulo["Responsable"].", ".$this->articulo["DptoAsociado"].", ".$this->articulo["Estado"].') WHERE "Id"='.$articulo["Id"]; 

    	if ($this->db->consultar($sql)) {
			$this->OperacionesSistemicas->array_OperacionesSistemicas($articulo["Responsable"],2,$articulo["Id"],$this->tabla);
			return "1";
		}
		else{
			return "2";
		}
	}
			



	

	/*public function set_articulo_destroy(){ 

			if ($this->get_consulta_articulo_destroy()>0) {
				$sql= 'DELETE FROM articulo WHERE "Id"='.$articulo["Id"];
				if ($this->db->consultar($sql)) {
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responable,5,$id,$this->tabla);
					return "1";
				}
			}
			else{
				return "2";
			}
		}

		public function get_consulta_articulo_destroy(){
			$sql='SELECT * FROM articulo where "Responsable='.$id;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese articul no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}*/

}

?>