<?php 

	/**
	* Clase HabilidadesxEstudi
	* Comparte la tabla habilidades_x_estudiante en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: set_HabilidadesxEstudi  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class HabilidadesxEstudi

	{
		private $db;
		private $habilxest;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] habilxest
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->habilxest= array();			
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->habilxest["id_habil"])) {
				$this->comprobar->ComprobarNumeric($this->habilxest["id_habil"]);
			}
			if (isset($this->habilxest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->habilxest["num_est"]);
			}
			if (isset($this->habilxest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->habilxest["idencuesta"]);
			}
			if (isset($this->habilxest["habilidades"])) {
				for ($i=0; $i <count($this->habilxest["habilidades"]) ; $i++) { 
					$this->comprobar->ComprobarNumeric($this->habilxest["habilidades"][$i]);
				}
			}
		}


		/**
		* Inserto una coleccion de habilidades artisticas que posee un estudiante segun la encuenta en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $habilxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_HabilidadesxEstudi($habilxest){

			$this->habilxest =$habilxest;
			$this->Comprobacion();

			$sql='INSERT INTO habilidades_x_estudiante ("id_habil", "num_est", "idencuesta") values';
			$values="";

			for ($i=0; $i <count($this->habilxest["habilidades"]) ; $i++) {


				$values=$values.'('.$this->habilxest["habilidades"][$i].", ".$this->habilxest["num_est"].", ".$this->habilxest["idencuesta"]."),";
			}
			$values = substr($values, 0, -1);
			 $sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {				
				return "1";			
			}
		}


		/**
		* Elimino la coleccion de habilidades artisticas que posee un estudiante	
		* @param [array] $habilxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_HabilidadesxEstudi_Delete($habilxest){

			$this->habilxest =$habilxest;
			$this->Comprobacion();

			for ($i=0; $i <count($this->habilxest["habilidades"]) ; $i++) {
				$sql='DELETE FROM habilidades_x_estudiante WHERE "idencuesta"='.$this->habilxest["idencuesta"].' AND "id_habil"='.$this->habilxest["habilidades"][$i];
			
				if ($this->db->consultar($sql)) {				
						
				}
			}
			return "1";		
		}


		/**
		* Elimino todas los registros de habilidades artisticas que esta asociada a una encuenta	
		* @param [array] $habilxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function get_HabilidadesxEstudi_DeleteAll($habilxest){

			$this->habilxest =$habilxest;
			$this->Comprobacion();

			$sql='DELETE FROM habilidades_x_estudiante WHERE "idencuesta"='.$this->habilxest["idencuesta"];
			
			if ($this->db->consultar($sql)) {				
				return "1";	
			}
					
		}

		
		/**
		* Consulto una coleccion de registros de habilidades de una encuesta para el formulario de edicion
		* @param [integer] $idencuesta
		* @return [array] [habilxest]   
		*/
		public function get_HabilidadesxEstudi_edit($idencuesta){

			$sql='SELECT "id_habil" FROM habilidades_x_estudiante where "idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->habilxest[] =$filas;
			}
			return $this->habilxest;
		}

		/**
		* Consulto una coleccion de registros de habilidades de una encuesta recibiendo un párrafo con los datos
		* @param [integer] $idencuesta
		* @return [caracter] [habilxest]  || [array] [habilxest]   
		*/
		public function get_HabilidadesxEstudi_show($idencuesta){

			$sql='SELECT (SELECT "desc_act_t_l" from act_tiempo_libre where "id_act_t_l"=hab."id_habil" ) as "habilidad" FROM habilidades_x_estudiante hab where hab."idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			$habilidad='';
			while ($filas=pg_fetch_assoc($consulta)) {
				$habilidad=" ".$habilidad.$filas["habilidad"].",";
				$this->habilxest[] =$filas;
			}

			if (count($this->habilxest)>0) {
				$habilidad = substr($habilidad, 0, -1);
				$this->habilxest[0]["habilidad"]=$habilidad;
			}
			

			return $this->habilxest;
		}
		

		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de estudiantes que realizan una habilidad espedifica, ordenandola por el total de manera descendente, en un periodo de tiempo 
		* @param [date] $inicio, [date] $fin
		* @return  [array] [habilxest]
		*/
		public function get_HabilidadesxEstudi_Estadistica($inicio, $fin)
		{
			$sql='SELECT count(*) as "total", (SELECT "desc_act_t_l" from act_tiempo_libre where "id_act_t_l"=hab."id_habil" ) as "habilidad" FROM estudiantesencuestados ee inner join habilidades_x_estudiante hab on ee."Id"=hab."idencuesta"
			where 
			 ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by hab."id_habil" order by "total" desc';
			
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["habilidad"])) {
					$filas["habilidad"]='No registrado';
				}
				$this->habilxest[] =$filas;
			}
			

			return $this->habilxest;

		}
	}
?>