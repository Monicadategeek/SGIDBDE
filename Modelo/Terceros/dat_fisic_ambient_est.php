<?php 

	

	/**
	* Clase DatosFisAmbEst
	* Comparte la tabla dat_fisic_ambient_est en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: get_DatosFisAmbEst  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class DatosFisAmbEst

	{
		private $db;
		private $datosfisambest;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] comprobar
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->datosfisambest= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datosfisambest
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->datosfisambest["id_dat_fisic_amb"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_dat_fisic_amb"]);
			}
			if (isset($this->datosfisambest["id_estado"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_estado"]);
			}
			if (isset($this->datosfisambest["id_ciudad"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_ciudad"]);
			}
			if (isset($this->datosfisambest["id_parroquia"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_parroquia"]);
			}			
			if (isset($this->datosfisambest["id_tip_viv"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_tip_viv"]);
			}
			if (isset($this->datosfisambest["id_ten_viv"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_ten_viv"]);
			}
			if (isset($this->datosfisambest["constr_viv"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["constr_viv"]);
			}
			if (isset($this->datosfisambest["id_tipo_pared"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_tipo_pared"]);
			}
			if (isset($this->datosfisambest["id_tipo_techo"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_tipo_techo"]);
			}
			if (isset($this->datosfisambest["id_tipo_piso"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["id_tipo_piso"]);
			}
			if (isset($this->datosfisambest["sala"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["sala"]);
			}
			if (isset($this->datosfisambest["cocina"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["cocina"]);
			}
			if (isset($this->datosfisambest["banios"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["banios"]);
			}
			if (isset($this->datosfisambest["dormitorios"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["dormitorios"]);
			}
			if (isset($this->datosfisambest["luz"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["luz"]);
			}
			if (isset($this->datosfisambest["agua"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["agua"]);
			}
			if (isset($this->datosfisambest["aseo_urb"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["aseo_urb"]);
			}
			if (isset($this->datosfisambest["telf_local"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["telf_local"]);
			}
			if (isset($this->datosfisambest["n_habitaciones"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["n_habitaciones"]);
			}
			if (isset($this->datosfisambest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->datosfisambest["num_est"]);
			}
		}



		/**
		* Inserto un nuevo Dato Fisico Ambiental de un estudiante en el sistema 
		* @method consultar(), Comprobacion(), get_Departamentos_store(), array_OperacionesSistemicas()
		* @param [array] $datosfisambest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DatosFisAmbEst($datosfisambest){

			$this->datosfisambest = $datosfisambest;
			$this->Comprobacion();

			$sql='INSERT INTO dat_fisic_ambient_est ("id_estado", "id_ciudad", "id_tip_viv", "id_ten_viv", 
            "constr_viv", "id_tipo_pared", "id_tipo_techo", "id_tipo_piso", "sala", 
            "cocina", "banios", "dormitorios", "luz", "agua", "aseo_urb", "telf_local", 
            "n_habitaciones", "num_est", "id_parroquia") values(';

			$values=$this->datosfisambest["id_estado"].", ".
			$this->datosfisambest["id_ciudad"].", ".
			$this->datosfisambest["id_tip_viv"].", ".
			$this->datosfisambest["id_ten_viv"].", ".
			$this->datosfisambest["constr_viv"].", ".
			$this->datosfisambest["id_tipo_pared"].", ".
			$this->datosfisambest["id_tipo_techo"].", ".
			$this->datosfisambest["id_tipo_piso"].", ".
			$this->datosfisambest["sala"].", ".
			$this->datosfisambest["cocina"].", ".
			$this->datosfisambest["banios"].", ".
			$this->datosfisambest["dormitorios"].", ".
			$this->datosfisambest["luz"].", ".
			$this->datosfisambest["agua"].", ".
			$this->datosfisambest["aseo_urb"].", ".
			$this->datosfisambest["telf_local"].", ".
			$this->datosfisambest["n_habitaciones"].", ".
			$this->datosfisambest["num_est"].", ".
			$this->datosfisambest["id_parroquia"].');';
			$sql=$sql.$values;
			if ($this->db->consultar($sql)) {				
				$sql='SELECT max("id_dat_fisic_amb") as "id_dat_fisic_amb" FROM dat_fisic_ambient_est limit 1';	
				$consulta = $this->db->consultar($sql);
				$filas=pg_fetch_assoc($consulta);
				return $filas["id_dat_fisic_amb"];			
			}
		}


		/**
		* Modifico el Dato Fisico Ambiental en el sistema 		
		* @param [array] $datosfisambest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_DatosFisAmbEst_update($datosfisambest){

			$this->datosfisambest = $datosfisambest;
			$this->Comprobacion();

			$sql='UPDATE dat_fisic_ambient_est
			   SET id_estado='.$this->datosfisambest["id_estado"].
			   ', id_ciudad='.$this->datosfisambest["id_ciudad"].
			   ', id_tip_viv='.$this->datosfisambest["id_tip_viv"].
			   ', id_ten_viv='.$this->datosfisambest["id_ten_viv"].
			   ',  constr_viv='.$this->datosfisambest["constr_viv"].
			   ', id_tipo_pared='.$this->datosfisambest["id_tipo_pared"].
			   ', id_tipo_techo='.$this->datosfisambest["id_tipo_techo"].
			   ', id_tipo_piso='.$this->datosfisambest["id_tipo_piso"].
			   ', sala='.$this->datosfisambest["sala"].
			   ', cocina='.$this->datosfisambest["cocina"].
			   ', banios='.$this->datosfisambest["banios"].
			   ', dormitorios='.$this->datosfisambest["dormitorios"].
			   ', luz='.$this->datosfisambest["luz"].
			   ', agua='.$this->datosfisambest["agua"].
			   ', aseo_urb='.$this->datosfisambest["aseo_urb"].
			   ', telf_local='.$this->datosfisambest["telf_local"].
			   ', n_habitaciones='.$this->datosfisambest["n_habitaciones"].
			   ', id_parroquia='.$this->datosfisambest["id_parroquia"].'
			 WHERE id_dat_fisic_amb='.$this->datosfisambest["id_dat_fisic_amb"];

			 if ($this->db->consultar($sql)) {				
				
				return 1;			
			}

		}
		
		/**
		* Consulto el id Registro de Datos Fisico Ambiental a traves del nro del estudiante
		* @param [array] $datosfisambest
		* @return [integer] muestra el nro de registros   
		*/
		public function get_DatosFisAmbEst($datosfisambest){

			$this->datosfisambest = $datosfisambest;
			$sql='SELECT "id_dat_fisic_amb" from dat_fisic_ambient_est where "num_est"='.$this->datosfisambest["num_est"];
			$consulta = $this->db->consultar($sql);
			$this->datossoceco=pg_fetch_assoc($consulta);
			
			return $this->datossoceco["id_dat_fisic_amb"];

		}

		/**
		* Consulto un Registro de Datos Socioeconomicos para editar
		* @param [integer] $id
		* @return [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_edit($id){

			
			$sql='SELECT d.*, (SELECT p."id_municipio" from parroquias p where p."id_parroquia"= d."id_parroquia") as "id_municipio" from dat_fisic_ambient_est d where d."id_dat_fisic_amb"='.$id;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco[0];

		}

		/**
		* Consulto un Registro de Datos Socioeconomicos
		* @param [integer] $id
		* @return [array] [datossoceco]   
		*/
		public function get_DatosFisAmbEst_show($id){

			
		 $sql='SELECT (SELECT e."estado" from estados e where e."id_estado"= d."id_estado") as "estado", 
				(SELECT c."ciudad" from ciudades c where c."id_ciudad"=d."id_ciudad") as "ciudad", 
				(SELECT p."parroquia" from parroquias p where p."id_parroquia"=d."id_parroquia") as "parroquia", 
				(SELECT p."id_municipio" from parroquias p where p."id_parroquia"= d."id_parroquia") as "id_municipio",
				(SELECT tv."desc_tipo_viv" from tipo_vivienda tv where tv."id_tipo_viv"= d."id_tip_viv") as "tip_viv",
				(SELECT "desc_ten_viv" from tenecia_vivienda where "id_ten_viv"=d."id_ten_viv") as "ten_viv",
				constr_viv, (SELECT "desc_tipo_pared" from tipos_pared where "id_tipo_pared"=d."id_tipo_pared") as "tipo_pared",
				(SELECT "desc_tipo_techo" from tipos_techo where "id_tipo_techo"=d."id_tipo_techo") as "tipo_techo", 
				(SELECT "desc_tipo_piso" from tipos_piso where "id_tipo_piso"=d."id_tipo_piso") as "tipo_piso",
				sala, cocina, banios, dormitorios, luz, agua, aseo_urb, telf_local, n_habitaciones
  				
  				FROM dat_fisic_ambient_est d where d."id_dat_fisic_amb"='.$id;
			$consulta = $this->db->consultar($sql);
			require_once(dirname(__FILE__)."/../Referenciales/municipios.php");
			while ($filas=pg_fetch_assoc($consulta)) {

				if (!empty($filas["id_municipio"])) {
					$referencia= new Municipios();
					$filas["municipio"]=$referencia->get_municipios_show($filas["id_municipio"]);	
				}
				else{
					$filas["municipio"]='';
				}						


				if ($filas["constr_viv"]==1) {
					$filas["constr_viv"]='Totalmente Acabada';
				}
				else{
					$filas["constr_viv"]='En proceso de Construcción';
				}

				
                if ($filas["luz"]==1) {
                  	$filas["luz"]='Si';
                }
                else{
                 	$filas["luz"]='No';
                }

                if ($filas["agua"]==1) {
                  	$filas["agua"]='Si';
                }
                else{
                 	$filas["agua"]='No';
                }

                if ($filas["aseo_urb"]==1) {
                  	$filas["aseo_urb"]='Si';
                }
                else{
                 	$filas["aseo_urb"]='No';
                }

                if ($filas["telf_local"]==1) {
                  	$filas["telf_local"]='Si';
                }
                else{
                 	$filas["telf_local"]='No';
                }

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco[0];

		}

		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el estado, municipio parroquia y ciudad donde recide el estudiante en una fecha determinada ($inicio y $fin)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Residencia($inicio, $fin)
		{
			$sql='SELECT dfa."id_estado", (SELECT e."estado" from estados e where e."id_estado"= dfa."id_estado") as "estado", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			require_once(dirname(__FILE__)."/../Referenciales/municipios.php");

			while ($filas=pg_fetch_assoc($consulta)) {

				$filas["id_municipio"]=$this->get_DatosFisAmbEst_Estadisticas_Municipio($inicio, $fin, $filas["id_estado"]);
				
				if (count($filas["id_municipio"])>0) {					
					$array=array();
					foreach ($filas["id_municipio"] as $municipio) {
						if (!empty($municipio["id_municipio"])) {
							$referencia= new Municipios();
							$municipio["municipio"]=$referencia->get_municipios_show($municipio["id_municipio"]);

							
						}	
						else	{
							$municipio["municipio"]='No Registrado';
						}				
						

						$array[]=$municipio;
					}
					$filas["municipios"]=$array;
				}
				else{
					$filas["municipios"]=' ';
				}

				$filas["parroquias"]=$this->get_DatosFisAmbEst_Estadisticas_Parroquia($inicio, $fin, $filas["id_estado"]);

				$filas["ciudades"]=$this->get_DatosFisAmbEst_Estadisticas_Ciudad($inicio, $fin, $filas["id_estado"]);

				$this->datossoceco[] =$filas;
			}

			return $this->datossoceco;
		}


		/**
		* Consulto municipio donde recide el estudiante	en una fecha determinada ($inicio y $fin) y un estado determinado
		* @param [date] $inicio, [date] $fin, [integer] $estado
		* @return  [array] $municipios
		*/
		private function get_DatosFisAmbEst_Estadisticas_Municipio($inicio, $fin, $estado)
		{

			$sql='SELECT (SELECT p."id_municipio" from parroquias p where p."id_parroquia"= dfa."id_parroquia") as "id_municipio", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and dfa."id_estado"='.$estado.' group by 1';
			$consulta = $this->db->consultar($sql);	
			$municipios=array();
			while ($filas=pg_fetch_assoc($consulta)) {
				$municipios[] =$filas;

			}
			return $municipios;
		}

		/**
		* Consulto la parroquia  donde recide el estudiante	en una fecha determinada ($inicio y $fin) y un estado determinado
		* @param [date] $inicio, [date] $fin, [integer] $estado
		* @return  [array] $parroquias
		*/
		private function get_DatosFisAmbEst_Estadisticas_Parroquia($inicio, $fin, $estado)
		{

			$sql='SELECT (SELECT p."parroquia" from parroquias p where p."id_parroquia"=dfa."id_parroquia") as "parroquia", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and dfa."id_estado"='.$estado.' group by 1';
			$consulta = $this->db->consultar($sql);	
			$parroquias=array();
			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["parroquia"])) {
					$filas["parroquia"]='No Registrado';
				}
				$parroquias[] =$filas;

			}
			return $parroquias;
		}

		/**
		* Consulto la ciudad  donde recide el estudiante	en una fecha determinada ($inicio y $fin) y un estado determinado
		* @param [date] $inicio, [date] $fin, [integer] $estado
		* @return  [array] $ciudades
		*/
		private function get_DatosFisAmbEst_Estadisticas_Ciudad($inicio, $fin, $estado)
		{

			$sql='SELECT (SELECT c."ciudad" from ciudades c where c."id_ciudad"=dfa."id_ciudad") as "ciudad", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and dfa."id_estado"='.$estado.' group by 1';
			$consulta = $this->db->consultar($sql);	
			$ciudades=array();
			while ($filas=pg_fetch_assoc($consulta)) {
				$ciudades[] =$filas;

			}
			return $ciudades;
		}


		//Vivienda

		/**
		* Consulto los tipos de vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Vivienda($inicio, $fin)
		{

			$sql='SELECT  dfa."id_tip_viv", (SELECT tv."desc_tipo_viv"  from tipo_vivienda tv where tv."id_tipo_viv"=dfa."id_tip_viv") as "tipo_vivienda", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datossoceco[] =$filas;

			}
			return $this->datossoceco;
		}


		/**
		* Consulto los tipos de tenencia de vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_TenenciaVivienda($inicio, $fin)
		{

			$sql='SELECT  dfa."id_ten_viv", (SELECT tv."desc_ten_viv"  from tenecia_vivienda tv where tv."id_ten_viv"=dfa."id_ten_viv") as "tipo_ten_viv", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datossoceco[] =$filas;

			}
			return $this->datossoceco;
		}


		/**
		* Consulto los tipos de construccion de vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Construccion($inicio, $fin)
		{

			$sql='SELECT  dfa."constr_viv", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["constr_viv"]==1) {
					$filas["constr_viv"]='Totalmente Acabada';
				}
				else if($filas["constr_viv"]==2) {
					$filas["constr_viv"]='En proceso de Construcción';
				}
				$this->datossoceco[] =$filas;

			}
			return $this->datossoceco;
		}

		/**
		* Consulto los tipos de pared en que esta construida la vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_TipoPared($inicio, $fin)
		{

			$sql='SELECT  dfa."id_tipo_pared", (SELECT tp."desc_tipo_pared" from tipos_pared tp where tp."id_tipo_pared"= dfa."id_tipo_pared") as "tipo_pared", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {				
				$this->datossoceco[] =$filas;

			}
			return $this->datossoceco;
		}


		/**
		* Consulto los tipos de piso en que esta construida la vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_TipoPiso($inicio, $fin)
		{

			$sql='SELECT  dfa."id_tipo_piso", (SELECT tp."desc_tipo_piso" from tipos_piso tp where tp."id_tipo_piso"= dfa."id_tipo_piso") as "tipo_piso", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {				
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto los tipos de techo en que esta construida la vivienda y el nro de estudiantes, donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_TipoTecho($inicio, $fin)
		{

			$sql='SELECT  dfa."id_tipo_techo", (SELECT tt."desc_tipo_techo" from tipos_techo tt where tt."id_tipo_techo" = dfa."id_tipo_techo")as "tipo_techo", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {				
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto el nro de habitaciones en que esta distribuida por tipo (sala, cocina, baño y dormitorios) la vivienda donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Habitaciones($inicio, $fin)
		{

			$sql='SELECT  avg(dfa."sala") as "sala", avg(dfa."cocina") as "cocina", avg(dfa."banios") as "banios", avg(dfa."dormitorios") as "dormitorios" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["sala"]=round($filas["sala"]);
				$filas["cocina"]=round($filas["cocina"]);
				$filas["banios"]=round($filas["banios"]);
				$filas["dormitorios"]=round($filas["dormitorios"]);				
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco[0];
		}

		/**
		* el nro de vivienda y si posse o no servicio de luz donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Servicios_Luz($inicio, $fin)
		{

			$sql='SELECT  dfa."luz", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				 if ($filas["luz"]==1) {
                  	$filas["luz"]='Si';
                }
                else{
                 	$filas["luz"]='No';
                }

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* el nro de vivienda y si posse o no servicio de agua donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Servicios_agua($inicio, $fin)
		{

			$sql='SELECT  dfa."agua", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {	
				if ($filas["agua"]==1) {
                  	$filas["agua"]='Si';
                }
                else{
                 	$filas["agua"]='No';
                }			
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* el nro de vivienda y si posse o no servicio de aseo urbano donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Servicios_aseo_urb($inicio, $fin)
		{

			$sql='SELECT  dfa."aseo_urb", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {	
				if ($filas["aseo_urb"]==1) {
                  	$filas["aseo_urb"]='Si';
                }
                else{
                 	$filas["aseo_urb"]='No';
                }               			
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* el nro de vivienda y si posse o no servicio de telefono local donde reciden los estudiantes en una fecha determinada ($inicio y $fin)
		* @return  [array] [datossoceco]
		*/
		public function get_DatosFisAmbEst_Estadisticas_Servicios_telf_local($inicio, $fin)
		{

			$sql='SELECT  dfa."telf_local", count(*) as "total" FROM estudiantesencuestados ee inner join dat_fisic_ambient_est dfa
  			on ee."ambientales"=dfa."id_dat_fisic_amb" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by 1';
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {		
				

                if ($filas["telf_local"]==1) {
                  	$filas["telf_local"]='Si';
                }
                else{
                 	$filas["telf_local"]='No';
                }

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}
	}
?>  