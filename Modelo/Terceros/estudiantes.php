<?php 	

	/**
	* 
	*/
	class estudiantes 

	{
		private $db;
		private $estudiantes;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			require_once(dirname(__FILE__) ."/../OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->estudiantes= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=4;//LISTO
			
		} 


		public function get_Estudiantes_Show($id, $Responsable){
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT e."cedula", e."nombres", e."apellidos", e."sexo", e."nacionalidad", e."correo", e."celular", e."tel_hab", e."edo_res", e."direccion", e."trabajo", (SELECT f."Descripcion" FROM formaingreso f WHERE f."IdFormaIngreso"=e."forma_ingreso") as forma_ingreso, e."edo_civil", e."fec_nac", e."fec_ingreso", e."tel_aux", e."edo_civil", (SELECT edo."estado" FROM estados edo WHERE edo."id_estado"=e."edo_nac") as "edo_nac", 


			 e."instruccion", (SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", e."trayecto", e."trimestre", e."turno", e."seccion", e."cod_estatus", e."foto" from estudiantes e where e."num_est"='.$id;		

			$consulta = $this->db->consultar($sql);
			
			while ($filas=pg_fetch_assoc($consulta)) {

				$filas["creditos"]=$this->get_Estudiantes_IndiceAcademico($id);
				if ($filas["sexo"]==1) {
					$filas["sexo"]="Femenino";
					$filas["genero"]="a";

				}
				else{
					$filas["sexo"]="Masculino";
					$filas["genero"]="o";
				}
				
				if ($filas["edo_civil"]!=0) {
					$sql='SELECT desc_edo_civ  FROM estado_civil where id_edo_civ='.$filas["edo_civil"];
					$consulta2 = $this->db->consultar($sql);
					if (pg_num_rows($consulta2)>0) {
						$var=pg_fetch_assoc($consulta2);
						$filas["edo_civil"]=ucwords(strtolower($var["desc_edo_civ"]));
					}
				}
				else{
					$filas["edo_civil"]='SOLTERO(A)';
				}
				$fecha = DateTime::createFromFormat('d/m/Y', $filas["fec_nac"]);
				$filas["fec_nac"]=$fecha->format('d-m-Y');
				$fecha1 = explode("-",date('Y-m-d', strtotime($filas["fec_nac"]))); // fecha nacimiento 
				$fecha2 = explode("-",date('Y-m-d')); // fecha actual 
				$filas["edad"] = $fecha2[0]-$fecha1[0]; 
				if($fecha2[1]<=$fecha1[1] and $fecha2[2]<=$fecha1[2]){ 
					$filas["edad"] = $filas["edad"] - 1; 
				} 

				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				if ($filas["turno"]==1) {
					$filas["turno"]="Mañana";
				}
				else if ($filas["turno"]==2) {
					$filas["turno"]="Tarde";
				}
				else if ($filas["turno"]==3) {
					$filas["turno"]="Noche";
				}

				
 
				if ($filas["nacionalidad"]==1) {
					$filas["nacionalidad"]="Venezolan".$filas["genero"];
				}
				else{
					$filas["nacionalidad"]="Extranger".$filas["genero"];
				}
				
				if ($filas["foto"]!=0) {
                $filas["foto"]='<img class="img-responsive avatar-view" src="../../../siace/carnet/fotos/'.$filas["foto"].'" alt="'.$filas["nombres"]." ".$filas["apellidos"].'" title="'.$filas["nombres"]." ".$filas["apellidos"].'">';
            }
            else{
                $filas["foto"]='<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El estudiante no tiene foto Registrada">';
            }
				

				$this->estudiantes[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,'null',$this->tabla);
			return $this->estudiantes[0];
		}

		public function get_Consultas_Estudiantes($cedula){
			$this->comprobar->ComprobarNumeric($cedula);

			$sql='SELECT e."num_est",e."cedula", e."nombres", e."apellidos", e."sexo", e."nacionalidad", e."correo", e."celular", e."tel_hab", e."edo_res", e."direccion", e."trabajo", (SELECT f."Descripcion" FROM formaingreso f WHERE f."IdFormaIngreso"=e."forma_ingreso") as forma_ingreso, e."edo_civil", e."fec_nac", e."fec_ingreso", e."tel_aux",
			 e."instruccion", (SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", e."trayecto", e."trimestre", e."turno", e."seccion", e."cod_estatus", e."foto" from estudiantes e where e."cedula" ='.$cedula;		

			$consulta = $this->db->consultar($sql);
			
			if (pg_num_rows($consulta)==0) {
				die("Error, La cédula que introdujo no se encuentra registrada en el SIACE");
			}
			while ($filas=pg_fetch_assoc($consulta)) {
				$fecha = DateTime::createFromFormat('d/m/Y', $filas["fec_nac"]);
				$filas["fec_nac"]=$fecha->format('d-m-Y');
				$fecha1 = explode("-",date('Y-m-d', strtotime($filas["fec_nac"]))); // fecha nacimiento 
				$fecha2 = explode("-",date('Y-m-d')); // fecha actual 
				$filas["edad"] = $fecha2[0]-$fecha1[0]; 
				if($fecha2[1]<=$fecha1[1] and $fecha2[2]<=$fecha1[2]){ 
					$filas["edad"] = $filas["edad"] - 1; 
				} 
				if ($filas["turno"]==1) {
					$filas["turno"]="Mañana";
				}
				elseif ($filas["turno"]==2) {
					$filas["turno"]="Tarde";
				}
				else{
					$filas["turno"]="Noche";
				}

				if ($filas["nacionalidad"]==1) {
					$filas["nacionalidad"]="V-";
				}
				else{
					$filas["nacionalidad"]="E-";
				}
				if ($filas["sexo"]==1) {
					$filas["sexo"]="Femenino";
				}
				else{
					$filas["sexo"]="Masculino";
				}

				if ($filas["cod_estatus"]==1) {
					$filas["cod_estatus"]="Activo";
				}
				else{
					$filas["cod_estatus"]="Inactivo";
				}

				

				$this->estudiantes[] =$filas;
			}
			return $this->estudiantes[0];
		}

		public function get_Estudiantes_Forma_Ingreso($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT count(e.*) as total, (SELECT f."Descripcion" FROM formaingreso f WHERE f."IdFormaIngreso"=e."forma_ingreso") as forma_ingreso from estudiantes e where e."fec_ingreso" like '." '%".$ano."'".' group by e."forma_ingreso"';	

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->estudiantes[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->estudiantes;
		}

		public function get_Estudiantes_Notas($id){
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT  n."nota", n."cod_catedra", n."creditos", (Select m."nombre" from mallas m where m."codigo"=n."cod_catedra" limit 1) as "nombremateria", (Select  m."trayecto"  from mallas m where m."codigo"=n."cod_catedra" limit 1) as "trayecto" from  notas n where n."cod_estudiante"='.$id.' order by  "trayecto" asc';	

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->estudiantes[] =$filas;
			}
			return $this->estudiantes;
		}

		public function get_Estudiantes_IndiceAcademico($estudiante){
			$this->comprobar->ComprobarNumeric($estudiante);

			$sql='SELECT sum(n."creditos") as "creditos" from  notas n where n."cod_estudiante"='.$estudiante;	

			$consulta = $this->db->consultar($sql);
			$filas=pg_fetch_assoc($consulta);
			if (empty($filas["creditos"])) {
				$filas["creditos"]=0;
			}
			return $filas["creditos"];
		}

		public function get_Estudiantes_Annos(){


			$sql='SELECT e."fec_ingreso" from estudiantes e where e."fec_ingreso" is not null  group by e."fec_ingreso"';		

			$consulta = $this->db->consultar($sql);
			$fecha="";
			while ($filas=pg_fetch_assoc($consulta)) {

				$caracter=explode("/", $filas["fec_ingreso"]);
				if (isset($caracter[2])) {
				 	$filas["fec_ingreso"]=$caracter[2];
					if (in_array($filas["fec_ingreso"], $this->estudiantes )==false) {
						$this->estudiantes[] = $filas["fec_ingreso"];

					}
				}
			}			
			//como ordenar un array 
			arsort($this->estudiantes);
			return $this->estudiantes;
		}


		public function get_Estudiantes_Total($ano){

			$sql='SELECT  count(e.*) as total from estudiantes e where e."fec_ingreso" like '." '%".$ano."'";		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->estudiantes[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->estudiantes[0];
		}


		
	}
?>