<?php 

	

	/**
	* 
	*/
	class DatInterEst

	{
		private $db;
		private $datosinteres;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->datosinteres= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=1;
			
		}


		private function Comprobacion()
		{

			if (isset($this->datosinteres["consideracion"])) {
				$this->comprobar->ComprobarCaracteres($this->datosinteres["consideracion"]);
			}
			else{
				$this->datosinteres["consideracion"]='';
			}


			if (isset($this->datosinteres["id_ayud"])) {
				$this->comprobar->ComprobarNumeric($this->datosinteres["id_ayud"]);
			}
			else{
				$this->datosinteres["id_ayud"]='null';
			}		

			if (isset($this->datosinteres["comedor"])) {
				$this->comprobar->ComprobarNumeric($this->datosinteres["comedor"]);
			}
			if (isset($this->datosinteres["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->datosinteres["num_est"]);
			}
			if (isset($this->datosinteres["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->datosinteres["idencuesta"]);
			}			

		}

		public function set_DatInterEst($datosinteres){

			$this->datosinteres = $datosinteres;
			$this->Comprobacion();

			$sql='INSERT INTO dat_inter_est("consideracion", "id_ayud", "comedor", "num_est") values(';
			$values="'".$this->datosinteres["consideracion"]."', ".$this->datosinteres["id_ayud"].", ".$this->datosinteres["comedor"].", ".$this->datosinteres["num_est"].')';
			$sql=$sql.$values;
			if ($this->db->consultar($sql)) {				
				$sql='SELECT max("id_dat_int_est") as "id_dat_int_est" FROM dat_inter_est limit 1';	
				$consulta = $this->db->consultar($sql);
				$filas=pg_fetch_assoc($consulta);
				return $filas["id_dat_int_est"];			
			}
		}

		public function get_DatInterEst($datosinteres){

			$this->datosinteres = $datosinteres;
			$this->Comprobacion();

			$sql='SELECT "id_dat_int_est" FROM  dat_inter_est WHERE "num_est"='.$this->datosinteres["num_est"];
			$consulta = $this->db->consultar($sql);
			$fila=pg_fetch_assoc($consulta);			
			return $fila["id_dat_int_est"];
			
		}


		public function get_DatInterEst_edit($id){


			$sql='SELECT "consideracion", "id_ayud" FROM  dat_inter_est WHERE "id_dat_int_est"='.$id;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosinteres[] =$filas;
			}
			return $this->datosinteres[0];
			
		}

		public function set_DatInterEst_update($datosinteres){

			$this->datosinteres = $datosinteres;
			$this->Comprobacion();

			$sql='UPDATE dat_inter_est SET "consideracion"='."'".$this->datosinteres["consideracion"]."'".', "id_ayud"='."'".$this->datosinteres["id_ayud"]."'".' WHERE "id_dat_int_est"='.$this->datosinteres["id_dat_int_est"];
			
			if ($this->db->consultar($sql)) {				
				
				return 	1;			
			}
		}

		public function get_DatInterEst_show($id){


			$sql='SELECT die."consideracion", (SELECT "Descripcion" FROM tipobeca WHERE "IdTipoBeca"=die."id_ayud") as "tipoayuda" FROM  dat_inter_est die WHERE die."id_dat_int_est"='.$id;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosinteres[] =$filas;
			}
			return $this->datosinteres[0];
			
		}


		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de estudiantes que han solicitado becas agrupandolo por el tipo de beca, dependiendo de la fecha determinada de la encuesta ($inicio y $fin)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datosinteres]
		*/

		public function get_DatInterEst_Estadisticas_Becas($inicio, $fin, $poblacion){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as "total", (SELECT "Descripcion" FROM tipobeca where "IdTipoBeca"=die."id_ayud") as "tipoBeca" FROM  estudiantesencuestados ee  inner join  dat_inter_est die on ee."interesAyu"=die."id_dat_int_est" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by die."id_ayud" order by "total" desc';
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			$total=0;
			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["tipoBeca"])) {
					$filas["tipoBeca"]='Otra';
				}
				$total=$total+$filas["total"];
				$this->datosinteres[]=$filas;
			}
			if ($total<$poblacion) {
				$filas=array();
				$filas["total"]=$poblacion-$total;
				$filas["tipoBeca"]='No Solicitada';
				$this->datosinteres[]=$filas;
			}
			return $this->datosinteres;
		}


		/**
		* Consulto el nro de estudiantes que han considerando la respuesta si desean tener un comedor (si 1 2 no), dependiendo de la fecha determinada de la encuesta ($inicio y $fin)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datosinteres]
		*/

		public function get_DatInterEst_Estadisticas_Becas_comedor($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as "total", comedor FROM  estudiantesencuestados ee  inner join  dat_inter_est die on ee."interesAyu"=die."id_dat_int_est" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by die."comedor"';
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["comedor"]==1) {
					$filas["comedor"]='Si';
				}
				else{
					$filas["comedor"]='No';
				}
				$this->datosinteres[]=$filas;
			}
			return $this->datosinteres;
		}


		/**
		* Consulto los estudiantes potenciales para la solicitud de las becas, se toma los datos del estudiante, evaluo su potencialidad a traves de que haya solicitado la ayuda, que no trabaje, ordenandolo por el promedio de sus notas, el total de ingresos familiar e individual, y los gastos familiar e individual, dependiendo de la fecha determinada de la encuesta ($inicio y $fin)
		*
		*Luego de eso llamo el metodo get_Inscripcionesbecarios_Estadisticas() para saber si ese estudiante no se encuentra actualmente activo en el presupuesto de becas actual 
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datosinteres]
		*/

		public function get_DatInterEst_Estadisticas_BecariosPotenciales($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT dse."TotIngFam", dse."TotIngEst", ee."estudiante", ee."Id",

			e."nombres", e."apellidos", e."sexo", e."cedula", 
			(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", e."cod_estatus",
			e."trayecto", e."trimestre", e."seccion",

			(SELECT  avg(n."nota") from  notas n where n."cod_estudiante"=ee."estudiante") as "nota",
			(SELECT "Descripcion" FROM tipobeca where "IdTipoBeca"=die."id_ayud") as "tipoBeca",
			(SELECT sum(g."monto") from gastos_x_estudiante g where g."idencuesta"=ee."Id" and "tipo"='."'Individual'".' ) as "GastosInd",

			(SELECT sum(g."monto") from gastos_x_estudiante g where g."idencuesta"=ee."Id" and "tipo"='."'Familiar'".' ) as "GastosFam"

			FROM  estudiantesencuestados ee  inner join  dat_inter_est die on ee."interesAyu"=die."id_dat_int_est" inner join dat_socio_est dse on ee."socioeconomicos"=dse."id_dat_socio_est" inner join estudiantes e on ee."estudiante"=e."num_est"


			 where die."id_ayud" is not null and dse."Trabaja"=2 and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' 

			 order by "nota" desc, dse."TotIngEst", dse."TotIngFam" asc , "GastosInd", "GastosFam" desc';

			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);

			require_once(dirname(__FILE__) ."/../Inscripcionesbecarios.php");
			while ($filas=pg_fetch_assoc($consulta)) {

				$registro= new Inscripcionesbecarios();
				if ($registro->get_Inscripcionesbecarios_Estadisticas($filas["estudiante"])==0) {
					$filas["boton"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/EstadisticasController.php',".$filas["Id"].');"><i class="fa fa-book"></i></button>';

					$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
					$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
					if ($filas["sexo"]==1) {
	                    $filas["sexo"]="Femenino";
	                }
	                else{
	                    $filas["sexo"]="Masculino";
	                }

	                if (empty($filas["nota"])) {
	                	$filas["nota"]='Notas No registradas';
	                }
	                else{
	                	$filas["nota"]=round($filas["nota"]);
	                }

					$this->datosinteres[]=$filas;
				}
				


				
			}
			
			return $this->datosinteres;
		}

		
	}
?>