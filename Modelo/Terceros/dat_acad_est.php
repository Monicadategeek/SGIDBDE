<?php 

	/**
	* Clase DatosAcadEst
	* Comparte la tabla dat_acad_est en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: get_DatosAcadEst  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class DatosAcadEst 

	{
		private $db;
		private $datosacadests;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] datosacadests
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->datosacadests= array();			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->datosacadests["id_dat_acad_est"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["id_dat_acad_est"]);
			}
			if (isset($this->datosacadests["id_tit_bach"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["id_tit_bach"]);
			}
			if (isset($this->datosacadests["id_tit_tsu"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["id_tit_tsu"]);
			}
			if (isset($this->datosacadests["tipo_inst"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["tipo_inst"]);
			}
			if (isset($this->datosacadests["prom_notas_bach"])) {
				$this->comprobar->ComprobarCaracteres($this->datosacadests["prom_notas_bach"]);
			}
			if (isset($this->datosacadests["motivo_pnf"])) {
				$this->comprobar->ComprobarCaracteres($this->datosacadests["motivo_pnf"]);
			}

			if (isset($this->datosacadests["identidad_pnf"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["identidad_pnf"]);
			}
			if (isset($this->datosacadests["te_gusta"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["te_gusta"]);
			}
			if (isset($this->datosacadests["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->datosacadests["num_est"]);
			}


		}


		/**
		* Counsuto los datos de la tabla para un estudiante, dandole formato a los datos luego de la consulta
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_datos_Academicos($estudiante){ 
			$this->comprobar->ComprobarNumeric($estudiante);
			$sql='SELECT  (SELECT m."desc_mencion" from menciones_bach m where m."id_mencion_bach"=d."id_tit_bach") as mencion, d."tipo_inst", d."prom_notas_bach", d."identidad_pnf", d."te_gusta" from dat_acad_est d  where d."num_est"='.$estudiante;

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["tipo_inst"]==1) {
					$filas["tipo_inst"]='Público';
				}
				else{
					$filas["tipo_inst"]='Privado';
				}

				if ($filas["identidad_pnf"]==1) {
					$filas["identidad_pnf"]='SI';
				}
				else{
					$filas["identidad_pnf"]='NO';
				}
				if ($filas["te_gusta"]==1) {
					$filas["te_gusta"]='SI';
				}
				else{
					$filas["te_gusta"]='NO';
				}

				$this->datosacadests[]=$filas;
			}
			return $this->datosacadests;
		}

		/**
		* Counsuto si existe los datos academicos en la encuenta
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_datos_Academicos_Encuesta($estudiante){ 
			$this->comprobar->ComprobarNumeric($estudiante);
			$sql='SELECT  *  from dat_acad_est d  where d."num_est"='.$estudiante;

			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Inserto un nuevo Dato Academico de un estudiante en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $datosacadests
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DatosAcadEst($datosacadests){
			$this->datosacadests = $datosacadests;
			$this->Comprobacion();

			$sql='INSERT INTO dat_acad_est("id_tit_bach","id_tit_tsu","tipo_inst","prom_notas_bach","motivo_pnf","identidad_pnf","te_gusta","num_est") values(';
			$values=$this->datosacadests["id_tit_bach"].', '.$this->datosacadests["id_tit_tsu"].', '.$this->datosacadests["tipo_inst"].", '".$this->datosacadests["prom_notas_bach"]."', '".$this->datosacadests["motivo_pnf"]."', ".$this->datosacadests["identidad_pnf"].', '.$this->datosacadests["te_gusta"].', '.$this->datosacadests["num_est"].')';
			$sql=$sql.$values;
			if ($this->db->consultar($sql)) {
				return "1";				
			}
		}

		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Counsuto todos los datos academos regristrados segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests][0] 
		*/
		public function get_Estadisticas_Total($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'";		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests[0];
		}

		/**
		* Counsuto el nro de estudiantes agrupandodolo por el titulo de bachiller  segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_Estadisticas_TituloBachiller($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total, (SELECT m."desc_mencion" from menciones_bach m where m."id_mencion_bach"=d."id_tit_bach") as mencion from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'".' group by d."id_tit_bach"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests;
		}


		/**
		* Counsuto el nro de estudiantes agrupandodolo por el tipo de institución (Pulica 1 Privada 2) segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_Estadisticas_TipoInstitucion($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total, d."tipo_inst" from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'".'  group by d."tipo_inst"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests;
		}

		/**
		* Counsuto el nro de estudiantes agrupandodolo por el promedio de notas segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_Estadisticas_PromedioNotas($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total, d."prom_notas_bach" from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'".'  group by d."prom_notas_bach" order by d."prom_notas_bach" asc';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests;
		}

		/**
		* Counsuto el nro de estudiantes agrupandodolo la pregunta de si se siente identificado con el PNF (Si 1 No 2)segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_Estadisticas_IdentificadoPNF($ano){
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total, d."identidad_pnf" from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'".'  group by d."identidad_pnf"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests;
		}


		/**
		* Counsuto el nro de estudiantes agrupandodolo la pregunta de si le gusta el PNF (Si 1 No 2) segun el año de ingreso 
		* @method ComprobarNumeric()
		* @param [integer] $ano
		* @return [array] [datosacadests]
		*/
		public function get_Estadisticas_GustaPNF($ano){ 
			$this->comprobar->ComprobarNumeric($ano);
			$sql='SELECT  count(d.*) as total, d."te_gusta" from dat_acad_est d, estudiantes e where d."num_est"=e."num_est" and e."fec_ingreso" like '." '%".$ano."'".'  group by d."te_gusta"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datosacadests[] =$filas;
			}
			return $this->datosacadests;
		}
	}
?>