<?php 

	

	/**
	* Clase ActivxEst
	* Comparte la tabla actividades_t_l_x_estudiante en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: set_ActivxEst  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class ActivxEst

	{
		private $db;
		private $actividadesxest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] actividades_t_l_x_estudiante
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->actividadesxest= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->actividadesxest["id_act_t_l"])) {
				$this->comprobar->ComprobarNumeric($this->actividadesxest["id_act_t_l"]);
			}
			if (isset($this->actividadesxest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->actividadesxest["num_est"]);
			}
			if (isset($this->actividadesxest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->actividadesxest["idencuesta"]);
			}

			if (isset($this->actividadesxest["actividades"])) {
				for ($i=0; $i <count($this->actividadesxest["actividades"]) ; $i++) { 
					$this->comprobar->ComprobarNumeric($this->actividadesxest["actividades"][$i]);
				}
			}
		}


		/**
		* Inserto una coleccion de actividades que realiza un estudiante segun la encuenta en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $actividadesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_ActivxEst($actividadesxest){

			$this->actividadesxest = $actividadesxest;
			$this->Comprobacion();

			$sql='INSERT INTO actividades_t_l_x_estudiante ("id_act_t_l", "num_est", "idencuesta") values';
			$values="";
			for ($i=0; $i <count($this->actividadesxest["actividades"]) ; $i++) {
				
				$values=$values.'('.$this->actividadesxest["actividades"][$i].", ".$this->actividadesxest["num_est"].", ".$this->actividadesxest["idencuesta"]."),";
			}
			$values = substr($values, 0, -1);
			echo $sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {				
				return "1";			
			}
		}

		/**
		* Elimino la coleccion de actividades que realiza un estudiante	
		* @param [array] $actividadesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_ActivxEst_Delete($actividadesxest){

			$this->actividadesxest = $actividadesxest;
			$this->Comprobacion();

			for ($i=0; $i <count($this->actividadesxest["actividades"]) ; $i++) {
				
				$sql='DELETE FROM actividades_t_l_x_estudiante WHERE "id_act_t_l"='.$this->actividadesxest["actividades"][$i].' and "idencuesta"='.$this->actividadesxest["idencuesta"];

				
				if ($this->db->consultar($sql)) {				
					return "1";			
				}

			}			
			
		}


		/**
		* Elimino todas los registros de actividades que realiza un estudiante que esta asociada a una encuenta	
		* @param [array] $actividadesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_ActivxEst_Update($actividadesxest){

			$this->actividadesxest = $actividadesxest;
			$this->Comprobacion();

			$sql='UPDATE actividades_t_l_x_estudiante SET "idencuesta"='.$this->actividadesxest["idencuesta"].' WHERE "num_est"='.$this->actividadesxest["num_est"];
			
			if ($this->db->consultar($sql)) {				
				return "1";			
			}
		}

		/**
		* Consulto una coleccion de registros de actividades de una encuesta para el formulario de edicion
		* @param [integer] $idencuesta
		* @return [array] [actividadesxest]   
		*/
		public function get_ActivxEst_edit($idencuesta){

			$sql='SELECT "id_act_t_l" FROM actividades_t_l_x_estudiante where "idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->actividadesxest[] =$filas;
			}
			return $this->actividadesxest;
		}

		/**
		* Consulto una coleccion de registros de actividades de una encuesta recibiendo un párrafo con los datos
		* @param [integer] $idencuesta
		* @return [caracter] [actividadesxest]  || [array] [actividadesxest]   
		*/
		public function get_ActivxEst_show($idencuesta){

			$sql='SELECT (SELECT "desc_act_t_l" from act_tiempo_libre where "id_act_t_l"=ae."id_act_t_l" ) as "actividad" FROM actividades_t_l_x_estudiante ae where ae."idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			$actividad='';
			while ($filas=pg_fetch_assoc($consulta)) {
				$actividad=$actividad." ".$filas["actividad"].",";
				$this->actividadesxest[] =$filas;
			}

			if (count($this->actividadesxest)>0) {
				$actividad = substr($actividad, 0, -1);
				$this->actividadesxest[0]["actividad"]=$actividad;
			}
			

			return $this->actividadesxest;
		}


		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de estudiantes que realizan una actividad espedifica, ordenandola por el total de manera descendente, en un periodo de tiempo 
		* @param [date] $inicio, [date] $fin
		* @return  [array] [actividadesxest]
		*/
		public function get_ActivxEst_Estadistica($inicio, $fin)
		{
			$sql='SELECT count(*) as "total", (SELECT "desc_act_t_l" from act_tiempo_libre where "id_act_t_l"=ae."id_act_t_l" ) as "actividad" FROM estudiantesencuestados ee inner join actividades_t_l_x_estudiante ae on ee."Id"=ae."idencuesta"
			where 
			 ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by ae."id_act_t_l" order by "total" desc';
			
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				
				$this->actividadesxest[] =$filas;
			}
			

			return $this->actividadesxest;

		}		


	}
?>