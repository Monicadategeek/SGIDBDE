<?php 
	
	/**
	* Clase EventosxEstud
	* Comparte la tabla eventos_x_estudiantes en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: set_EventosxEstud  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class EventosxEstud
	{
		private $db;
		private $eventosxest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] eventosxest
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * 
		 */
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->eventosxest= array();			
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->eventosxest["id_act_t_l"])) {
				$this->comprobar->ComprobarNumeric($this->eventosxest["id_act_t_l"]);
			}
			if (isset($this->eventosxest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->eventosxest["num_est"]);
			}
			if (isset($this->eventosxest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->eventosxest["idencuesta"]);
			}
			if (isset($this->eventosxest["eventos"])) {
				for ($i=0; $i <count($this->eventosxest["eventos"]) ; $i++) { 
					$this->comprobar->ComprobarNumeric($this->eventosxest["eventos"][$i]);
				}
			}
		}

		/**
		* Inserto una coleccion de eventos que ha participado un estudiante segun la encuenta en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $eventosxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_EventosxEstud($eventosxest){

			$this->eventosxest = $eventosxest;
			$this->Comprobacion();

			$sql='INSERT INTO eventos_x_estudiantes ("id_evento", "num_est", "idencuesta") values';
			$values="";
			for ($i=0; $i <count($this->eventosxest["eventos"]) ; $i++) {

				$values=$values.'('.$this->eventosxest["eventos"][$i].", ".$this->eventosxest["num_est"].", ".$this->eventosxest["idencuesta"]."),";
			}
			$values = substr($values, 0, -1);
			$sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {				
				return "1";			
			}
		}

		/**
		* Elimino la coleccion de eventos que ha participado un estudiante	
		* @param [array] $eventosxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_EventosxEstud_Delete($eventosxest){

			$this->eventosxest = $eventosxest;
			$this->Comprobacion();

			for ($i=0; $i <count($this->eventosxest["eventos"]) ; $i++) {

				$sql='DELETE FROM eventos_x_estudiantes WHERE "idencuesta"='.$this->eventosxest["idencuesta"].' AND "id_evento"='.$this->eventosxest["eventos"][$i];
			
				if ($this->db->consultar($sql)) {				
							
				}
			}	
			
			return "1";	
		}

		/**
		* Elimino todas los registros de eventos que ha participado un estudiante que esta asociada a una encuenta	
		* @param [array] $eventosxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_EventosxEstud_DeleteAll($eventosxest){

			$this->eventosxest = $eventosxest;
			$this->Comprobacion();

			$sql='DELETE FROM eventos_x_estudiantes WHERE "idencuesta"='.$this->eventosxest["idencuesta"];
			
			if ($this->db->consultar($sql)) {				
				return "1";		
			}	
			
				
		}
		

		/**
		* Consulto una coleccion de registros de eventos de una encuesta para el formulario de edicion
		* @param [integer] $idencuesta
		* @return [array] [eventosxest]   
		*/
		public function get_EventosxEstud_edit($idencuesta){

			$sql='SELECT "id_evento" FROM eventos_x_estudiantes where "idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->eventosxest[] =$filas;
			}
			return $this->eventosxest;
		}

		/**
		* Consulto una coleccion de registros de eventos de una encuesta recibiendo un párrafo con los datos
		* @param [integer] $idencuesta
		* @return [caracter] [eventosxest]  || [array] [eventosxest]   
		*/
		public function get_EventosxEstud_show($idencuesta){

			$sql='SELECT (SELECT "desc_evento" from eventos_iutoms where "id_evento"=evet."id_evento" ) as "evento" FROM eventos_x_estudiantes evet where evet."idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			$evento='';
			while ($filas=pg_fetch_assoc($consulta)) {
				$evento=" ".$evento.$filas["evento"].",";
				$this->eventosxest[] =$filas;
			}

			if (count($this->eventosxest)>0) {
				$evento = substr($evento, 0, -1);
				$this->eventosxest[0]["evento"]=$evento;
			}
			

			return $this->eventosxest;
		}
		
		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de estudiantes que realizan un evento espedifico, ordenandola por el total de manera descendente, en un periodo de tiempo 
		* @param [date] $inicio, [date] $fin
		* @return  [array] [eventosxest]
		*/
		public function get_EventosxEstud_Estadistica($inicio, $fin)
		{
			$sql='SELECT count(*) as "total", (SELECT "desc_evento" from eventos_iutoms where "id_evento"=evet."id_evento" ) as "evento" FROM estudiantesencuestados ee inner join eventos_x_estudiantes evet on ee."Id"=evet."idencuesta"
			where 
			 ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by evet."id_evento"  order by "total" desc';
			
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["evento"])) {
					$filas["evento"]='No registrado';
				}
				$this->eventosxest[] =$filas;
			}
			

			return $this->eventosxest;

		}
		
	}
?>