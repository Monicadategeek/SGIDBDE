<?php 
	
	/**
	* Clase GastosxEstudiante
	* Comparte la tabla dat_acad_est en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: get_GastosxEstudiante  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class GastosxEstudiante

	{
		private $db;
		private $gastosxest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] gastosxest
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->gastosxest= array();		
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->gastosxest["id_gasto_est"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["id_gasto_est"]);
			}

			if (isset($this->gastosxest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["idencuesta"]);
			}
			if (isset($this->gastosxest["id_concepto"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["id_concepto"]);
			}
			if (isset($this->gastosxest["monto"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["monto"]);
			}
			if (isset($this->gastosxest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["num_est"]);
			}
			if (isset($this->gastosxest["tipo"])) {
				$this->comprobar->ComprobarNumeric($this->gastosxest["tipo"]);
			}

			
		}

		/**
		* Inserto una coleccion de gastos de un estudiante en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $gastosxest, [array] $egresos
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_GastosxEstudiante($gastosxest, $egresos){

			$this->gastosxest = $gastosxest;
			$this->Comprobacion();

			$sql='INSERT INTO gastos_x_estudiante("id_concepto", "monto", "num_est", "idencuesta", "tipo") values';
			$values="";
			foreach ($egresos as $egreso) {
				if (!isset($egreso["IdEgreso"])) {
					break;
				}	
				$values=$values."(".$egreso["IdEgreso"].", ".$this->gastosxest["egrind".$egreso["IdEgreso"]].", ".
				$this->gastosxest["num_est"].", ".$this->gastosxest["idencuesta"].", 'Individual'), (".$egreso["IdEgreso"].", ".$this->gastosxest["egrfam".$egreso["IdEgreso"]].", ".
				$this->gastosxest["num_est"].", ".$this->gastosxest["idencuesta"].", 'Familiar'),";

			}		
			
			$values = substr($values, 0, -1);
			$sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {
				return "1";				
			}

			return '';
		}


		/**
		* Counsuto los gastos de un estudiante dependiendo del id de la encuesta y si son de tipo "Individual", dandole formato a los datos luego de la consulta
		* @method ComprobarNumeric()
		* @param [integer] $idencuesta
		* @return [array] [gastosxest]
		*/
		public function get_GrupoFamiliarEst_gastosindividuales($idencuesta){

			$sql='SELECT e."Descripcion", e."IdEgreso", g."monto" from egreso e left join gastos_x_estudiante g on e."IdEgreso"=g."id_concepto" where g."idencuesta"='.$idencuesta.' and "tipo"='."'Individual'".' order by e."Descripcion"';
			$consulta = $this->db->consultar($sql);
			$total=0;
			while ($filas=pg_fetch_assoc($consulta)) {
				
				$total=$filas["monto"]+$total;

				$filas["monto"]=number_format($filas["monto"], 2, '.', '');
				$this->gastosxest[] =$filas;
			}

			$filas["Descripcion"]='TOTAL';
			$filas["monto"]=number_format($total, 2, '.', '');

			$this->gastosxest[] =$filas;

			return $this->gastosxest;
			
		}

		/**
		* Counsuto los gastos de un estudiante dependiendo del id de la encuesta y si son de tipo "Familiar", dandole formato a los datos luego de la consulta
		* @method ComprobarNumeric()
		* @param [integer] $idencuesta
		* @return [array] [gastosxest]
		*/
		public function get_GrupoFamiliarEst_gastosfamiliares($idencuesta){

			$sql='SELECT e."Descripcion", e."IdEgreso", g."monto" from egreso e left join gastos_x_estudiante g on e."IdEgreso"=g."id_concepto" where g."idencuesta"='.$idencuesta.' and "tipo"='."'Familiar'".' order by e."Descripcion"';
			$consulta = $this->db->consultar($sql);
			$total=0;
			while ($filas=pg_fetch_assoc($consulta)) {
				
				$total=$filas["monto"]+$total;
				$filas["valor"]=$filas["monto"];
				$filas["monto"]=number_format($filas["monto"], 2, '.', '');
				$this->gastosxest[] =$filas;
			}

			$filas["Descripcion"]='TOTAL';
			$filas["monto"]=number_format($total, 2, '.', '');
			$filas["valor"]=$total;

			$this->gastosxest[] =$filas;

			return $this->gastosxest;
			
		}

		/**
		* Modifico una coleccion de gastos de un estudiante en el sistema segun la encuesta asociada
		* @method consultar(), Comprobacion()
		* @param [array] $egresos
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_GastosxEstudiante_update($egresos){
			$sql='';
			foreach ($egresos as $egreso) {
				$sql='UPDATE gastos_x_estudiante SET "monto"='.$egreso["monto"].' WHERE id_concepto='.$egreso["id_concepto"].' and tipo='."'".$egreso["tipo"]."'".' and idencuesta='.$egreso["idencuesta"].';';

				$this->db->consultar($sql);
			}			
			
			return 1;
			
		}

		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el id del concepto y la descripcion, definiendolo por el tipo ('Individual' o 'Familiar'), segun un periodo de tiempo de la encuesta	
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_Presupuesto($inicio, $fin, $tipo)
		{
			$sql='SELECT gxe."id_concepto", (SELECT "Descripcion" from egreso where "IdEgreso"= gxe."id_concepto") as "Descripcion" FROM estudiantesencuestados ee inner join gastos_x_estudiante gxe on ee."Id"=gxe."idencuesta" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "tipo"='."'".$tipo."'".' group by 1, 2';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if (!empty($filas["Descripcion"])) {
						$filas["Descripcion"]=ucwords(strtolower($filas["Descripcion"]));
						$filas["avgmonto"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Prom($inicio, $fin, $tipo, $filas["id_concepto"], 'avg');
						$filas["avgmonto"]=number_format($filas["avgmonto"], 2, '.', '');
						$filas["maxmonto"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Prom($inicio, $fin, $tipo, $filas["id_concepto"], 'max');
						$filas["maxmonto"]=number_format($filas["maxmonto"], 2, '.', '');
						$filas["minmonto"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Prom($inicio, $fin, $tipo, $filas["id_concepto"], 'min');
						$filas["minmonto"]=number_format($filas["minmonto"], 2, '.', '');

						$this->datossoceco[] =$filas;
				}

			}
			return $this->datossoceco;
		}
		

		/**
		* Consulto el minimo, maximo y promedio de los gastos agrupandolo por el concepto definiendolo por el tipo ('Individual' o 'Familiar'), segun un periodo de tiempo de la encuesta	
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		private function get_DatosSocEco_Est_Estadisticas_Presupuesto_Prom($inicio, $fin, $tipo, $id_concepto, $avg)
		{
			$sql='SELECT '.$avg.'("monto") as "total" FROM estudiantesencuestados ee inner join gastos_x_estudiante gxe on ee."Id"=gxe."idencuesta" where ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "tipo"='."'".$tipo."'".' and gxe."id_concepto"='.$id_concepto;
			$consult = $this->db->consultar($sql);	
			$egreso=array();
			while ($fila=pg_fetch_assoc($consult)) {

				$egreso[] =$fila;
			}
			return $egreso[0]["total"];
		}
		

	}
?>