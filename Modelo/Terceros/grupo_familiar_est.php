<?php 

	/**
	* Clase GrupoFamiliarEst
	* Comparte la tabla dat_acad_est en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: get_GrupoFamiliarEst  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class GrupoFamiliarEst 

	{
		private $db;
		private $grupofamiliarest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] grupofamiliarest
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->grupofamiliarest= array();	
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->grupofamiliarest["id_grupo_fam_est"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["id_grupo_fam_est"]);
			}
			if (isset($this->grupofamiliarest["nombres"])) {
				$this->comprobar->ComprobarCaracteres($this->grupofamiliarest["nombres"]);
			}
			if (isset($this->grupofamiliarest["apellidos"])) {
				$this->comprobar->ComprobarCaracteres($this->grupofamiliarest["apellidos"]);
			}

			if (isset($this->grupofamiliarest["id_parentesco"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["id_parentesco"]);
			}

			if (isset($this->grupofamiliarest["fec_nac"])) {
				$this->comprobar->ComprobarFecha($this->grupofamiliarest["fec_nac"]);
			}

			if (isset($this->grupofamiliarest["id_edo_civ"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["id_edo_civ"]);
			}

			if (isset($this->grupofamiliarest["genero"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["genero"]);
			}
			if (isset($this->grupofamiliarest["id_grado_instr"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["id_grado_instr"]);
			}

			if (isset($this->grupofamiliarest["id_prof_ocup"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["id_prof_ocup"]);
			}
			
			if (isset($this->grupofamiliarest["ingreso_mensual"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["ingreso_mensual"]);
			}
			if (isset($this->grupofamiliarest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->grupofamiliarest["num_est"]);
			}	
		}



		/**
		* Consulto los familiares asociados a un estudiante en el sistema 
		* @method consultar(), Comprobacion(), get_Departamentos_store()
		* @param [integer] $num_est
		* @return [array] [grupofamiliarest] 
		*/
		public function get_GrupoFamiliarEst_index($num_est){
			$this->comprobar->ComprobarNumeric($num_est);

			$sql='SELECT g."id_grupo_fam_est", g."nombres", g."apellidos", (SELECT p."desc_parentesco" from parentescos p where p."id_parentesco"=g."id_parentesco") as "id_parentesco", g."fec_nac",
			(SELECT e."desc_edo_civ" from estado_civil e where e."id_edo_civ"=g."id_edo_civ") as "id_edo_civ", g."genero", (SELECT gi."desc_grado_instr" from grado_instruccion gi where gi."id_grado_instr"=g."id_grado_instr") as "id_grado_instr", (SELECT po."desc_prof_ocup" from prof_ocup po where po."id_prof_ocup"=g."id_prof_ocup") as "id_prof_ocup", g."ingreso_mensual" from grupo_familiar_est g where g."num_est"='.$num_est;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				$fecha1 = explode("-",date('Y-m-d', strtotime($filas["fec_nac"]))); // fecha nacimiento 
				$fecha2 = explode("-",date('Y-m-d')); // fecha actual 
				$filas["edad"] = $fecha2[0]-$fecha1[0]; 
				if($fecha2[1]<=$fecha1[1] and $fecha2[2]<=$fecha1[2]){ 
					$filas["edad"] = $filas["edad"] - 1; 
				} 
				
				$filas["fec_nac"]=date('d-m-Y', strtotime($filas["fec_nac"]));

				if ($filas["genero"]==2) {
					$filas["genero"]="Femenino";
				}
				else{
					$filas["genero"]="Masculino";
				}
				$filas["ingreso_mensual_num"]=$filas["ingreso_mensual"];
				
				if ($filas["ingreso_mensual"]!=0) {
					$filas["ingreso_mensual"]=number_format($filas["ingreso_mensual"], 2, ',', '.');
				}		

				$this->grupofamiliarest[] =$filas;
			}
			return $this->grupofamiliarest;
		}


		/**
		* Inserto un familiar asociado a un estudiante en el sistema 
		* @method consultar(), Comprobacion()
		* @param [integer] $grupofamiliarest
		* @return [integer] 1 || [caracter]  Error
		*/
		public function set_GrupoFamiliarEst_store($grupofamiliarest){ 
			$this->grupofamiliarest = $grupofamiliarest;
			$this->Comprobacion();
			$sql= 'INSERT INTO grupo_familiar_est ("nombres", "apellidos", "id_parentesco", "fec_nac", "id_edo_civ", "genero", "id_grado_instr", "id_prof_ocup", "ingreso_mensual", "num_est") VALUES (';

			$values="'".$this->grupofamiliarest["nombres"]."', '".$this->grupofamiliarest["apellidos"]."', ".$this->grupofamiliarest["id_parentesco"].", '".$this->grupofamiliarest["fec_nac"]."', ".$this->grupofamiliarest["id_edo_civ"].", ".$this->grupofamiliarest["genero"].", ".$this->grupofamiliarest["id_grado_instr"].", ".$this->grupofamiliarest["id_prof_ocup"].", ".$this->grupofamiliarest["ingreso_mensual"].", ".$this->grupofamiliarest["num_est"].");";
			$sql=$sql.$values;

			if ($this->db->consultar($sql)) {
				return "1";				
			}
			else{
				return "Ha ocurrido un error al interactuar con la base de datos";
			}
			
		}

		/**
		* Consulto los datos de un familiar asociado a un estudiante en el sistema 
		* @method consultar(), Comprobacion()
		* @param [integer] $id
		* @return [array] [grupofamiliarest][0]
		*/
		public function get_GrupoFamiliarEst_edit($id){

			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT * from grupo_familiar_est g where g."id_grupo_fam_est"='.$id;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["fec_nac"]=date('d-m-Y', strtotime($filas["fec_nac"]));
				if ($filas["ingreso_mensual"]!=0) {
					$filas["ingreso_mensual"]=number_format($filas["ingreso_mensual"], 2, '.', '');
				}
				
				$this->grupofamiliarest[] =$filas;
			}
			return $this->grupofamiliarest[0];
		}


		/**
		* Modifico un familiar asociado a un estudiante en el sistema 
		* @method consultar(), Comprobacion()
		* @param [integer] $grupofamiliarest
		* @return [integer] 1 || [caracter]  Error
		*/
		public function set_GrupoFamiliarEst_update($grupofamiliarest){ //Consulta Registrar Depertamentos
			$this->grupofamiliarest = $grupofamiliarest;
			$this->Comprobacion();

			$sql= ' UPDATE grupo_familiar_est SET "nombres"='."'".$this->grupofamiliarest["nombres"]."'".', "apellidos"='."'".$this->grupofamiliarest["apellidos"]."'".', "id_parentesco"='.$this->grupofamiliarest["id_parentesco"].', "fec_nac"='."'".$this->grupofamiliarest["fec_nac"]."'".', "id_edo_civ"='.$this->grupofamiliarest["id_edo_civ"].', "genero"='.$this->grupofamiliarest["genero"].', "id_grado_instr"='.$this->grupofamiliarest["id_grado_instr"].', "id_prof_ocup"='.$this->grupofamiliarest["id_prof_ocup"].', "ingreso_mensual"='.$this->grupofamiliarest["ingreso_mensual"].' WHERE "id_grupo_fam_est"='.$this->grupofamiliarest["id_grupo_fam_est"];

			if ($this->db->consultar($sql)) {
				return "1";				
			}
			else{
				return "Ha ocurrido un error al interactuar con la base de datos";
			}
			
		}


		/**
		* Elimino un familiar asociado a un estudiante en el sistema 
		* @method consultar(), Comprobacion(), get_GrupoFamiliarEst_destroy()
		* @param [integer] $id, [integer] $num_est
		* @return [integer] 1 || [caracter]  Error
		*/
		public function set_GrupoFamiliarEst_destroy($id, $num_est){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($num_est);
			if ($this->get_GrupoFamiliarEst_destroy($id, $num_est)>0) {
				$sql= 'DELETE FROM grupo_familiar_est WHERE id_grupo_fam_est='.$id;

				if ($this->db->consultar($sql)) {
					return "1";				
				}
				else{
					return "Ha ocurrido un error al interactuar con la base de datos";
				}

			}
			else{
				return "El registro del miembro familiar que indicó no es suyo";
			}						
		}		

		/**
		* Consulto si ese familiar esta asociado a ese estudiante
		* @method consultar(), Comprobacion()
		* @param [integer] $id, [integer] $num_est
		* @return [integer] [pg_num_rows]
		*/
		private function get_GrupoFamiliarEst_destroy($id, $num_est){
			$sql='SELECT * FROM grupo_familiar_est g where g."num_est"='.$num_est.' and g."id_grupo_fam_est"='.$id;
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}



		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de familiares que existe agrupandolo por el parentesco y ordenandolo por el total de forma descandente, segun un periodo de tiempo de la encuesta	
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_GrupoFamiliarEst_Estadisticas($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as "total", (SELECT "desc_parentesco" from parentescos where "id_parentesco" = g."id_parentesco") as "Titulo" 
			FROM estudiantesencuestados ee, grupo_familiar_est g 
			where g."num_est"=ee."estudiante" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by g."id_parentesco" order by "total" desc';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {

				$this->grupofamiliarest[] =$filas;
			}
			return $this->grupofamiliarest;
		}
		

		/**
		* Consulto el nro de familiares que existe agrupandolo por el estudiante, segun un periodo de tiempo de la encuesta	
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_GrupoFamiliarEst_Estadisticas_Familiares($inicio, $fin)
		{
			$sql='SELECT count(*) as "total" FROM estudiantesencuestados ee, dat_socio_est dse, grupo_familiar_est g
			where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."estudiante"=g."num_est" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by ee."estudiante"';
			
			$consulta = $this->db->consultar($sql);	
			$array= array();
			$sum=0;

			while ($filas=pg_fetch_assoc($consulta)) {
				$sum=$sum+$filas["total"]+1;
				$array=$filas;
			}

			if (pg_num_rows($consulta)>0) {
				return round($sum/pg_num_rows($consulta));
			}
			return 0;
		}
		
	}
?>