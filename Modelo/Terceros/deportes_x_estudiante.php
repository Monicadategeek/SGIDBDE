<?php 

	

	/**
	* Clase DeportesxEstudiante
	* Comparte la tabla deportes_x_estudiante en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: set_DeportesxEstudiante  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class DeportesxEstudiante

	{
		private $db;
		private $deportesxest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] deportesxest
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->deportesxest= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->deportesxest["id_deporte"])) {
				$this->comprobar->ComprobarNumeric($this->deportesxest["id_act_t_l"]);
			}
			if (isset($this->deportesxest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->deportesxest["num_est"]);
			}
			if (isset($this->deportesxest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->deportesxest["idencuesta"]);
			}
			if (isset($this->deportesxest["deportes"])) {
				for ($i=0; $i <count($this->deportesxest["deportes"]) ; $i++) { 
					$this->comprobar->ComprobarNumeric($this->deportesxest["deportes"][$i]);
				}
			}
		}

		/**
		* Inserto una coleccion de deportes que practica un estudiante segun la encuenta en el sistema 
		* @method consultar(), Comprobacion()
		* @param [array] $deportesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DeportesxEstudiante($deportesxest){

			$this->deportesxest = $deportesxest;
			$this->Comprobacion();

			$sql='INSERT INTO deportes_x_estudiante ("id_deporte", "num_est", "idencuesta") values';
			$values="";
			for ($i=0; $i <count($this->deportesxest["deportes"]) ; $i++) {

				$values=$values.'('.$this->deportesxest["deportes"][$i].", ".$this->deportesxest["num_est"].", ".$this->deportesxest["idencuesta"]."),";
			}
			$values = substr($values, 0, -1);
			$sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {				
				return "1";			
			}
		}


		/**
		* Elimino la coleccion de deportes que realiza un estudiante	
		* @param [array] $deportesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DeportesxEstudiante_Delete($deportesxest){

			$this->deportesxest = $deportesxest;
			$this->Comprobacion();

			for ($i=0; $i <count($this->deportesxest["deportes"]) ; $i++) {

				$sql='DELETE FROM deportes_x_estudiante WHERE "idencuesta"='.$this->deportesxest["idencuesta"].' AND "id_deporte"='.$this->deportesxest["deportes"][$i];
			
				if ($this->db->consultar($sql)) {				
						
				}
			}

			return "1";		
			
		}

		/**
		* Elimino todas los registros de deportes que realiza un estudiante que esta asociada a una encuenta	
		* @param [array] $deportesxest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DeportesxEstudiante_DeleteAll($deportesxest){

			$this->deportesxest = $deportesxest;
			$this->Comprobacion();

			$sql='DELETE FROM deportes_x_estudiante WHERE "idencuesta"='.$this->deportesxest["idencuesta"];
			
			if ($this->db->consultar($sql)) {				
					return "1";	
			}				
			
		}

		/**
		* Consulto una coleccion de registros de deportes de una encuesta para el formulario de edicion
		* @param [integer] $idencuesta
		* @return [array] [deportesxest]   
		*/
		public function get_DeportesxEstudiante_edit($idencuesta){

			$sql='SELECT "id_deporte" FROM deportes_x_estudiante where "idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->deportesxest[] =$filas;
			}
			return $this->deportesxest;
		}

		/**
		* Consulto una coleccion de registros de deportes de una encuesta recibiendo un párrafo con los datos
		* @param [integer] $idencuesta
		* @return [caracter] [deportesxest]  || [array] [deportesxest]   
		*/
		public function get_deportesxest_show($idencuesta){

			$sql='SELECT (SELECT "desc_cat_dep" from categorias_deportes where "id_cat_dep"=dxe."id_deporte" ) as "deporte" FROM deportes_x_estudiante dxe where dxe."idencuesta"='.$idencuesta;
			$consulta = $this->db->consultar($sql);
			$deporte='';
			while ($filas=pg_fetch_assoc($consulta)) {
				$deporte=" ".$deporte.$filas["deporte"].",";
				$this->deportesxest[] =$filas;
			}

			if (count($this->deportesxest)>0) {
				$deporte = substr($deporte, 0, -1);
				$this->deportesxest[0]["deporte"]=$deporte;
			}
			

			return $this->deportesxest;
		}


		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el nro de estudiantes que realizan un deporte espedifico, ordenandola por el total de manera descendente, en un periodo de tiempo 
		* @param [date] $inicio, [date] $fin
		* @return  [array] [deportesxest]
		*/
		public function get_DeportesxEstudiante_Estadistica($inicio, $fin)
		{
			$sql='SELECT count(*) as "total", (SELECT "desc_cat_dep" from categorias_deportes where "id_cat_dep"=dxe."id_deporte" ) as "deporte", dxe."id_deporte" FROM estudiantesencuestados ee inner join deportes_x_estudiante dxe on ee."Id"=dxe."idencuesta"
			where 
			 ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dxe."id_deporte" order by "total" desc';
			
			$consulta = $this->db->consultar($sql);	
			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["deporte"])) {
					$filas["deporte"]='No registrado';
				}
				$this->deportesxest[] =$filas;
			}
			

			return $this->deportesxest;

		}
	}
?>