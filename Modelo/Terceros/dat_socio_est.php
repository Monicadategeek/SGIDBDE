<?php 


	/**
	* Clase DatosSocEcon
	* Comparte la tabla dat_socio_est en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: set_DatosSocEco  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class DatosSocEcon

	{
		private $db;
		private $datossoceco;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] datossoceco
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->datossoceco= array();			
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->datossoceco["id_dat_socio_est"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["id_dat_socio_est"]);
			}

			if (isset($this->datossoceco["Trabaja"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["Trabaja"]);
			}
			if (isset($this->datossoceco["HaTrabajado"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["HaTrabajado"]);
			}
			

			if (isset($this->datossoceco["donde_trabaja"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["donde_trabaja"]);
			}
			else{
				$this->datossoceco["donde_trabaja"]='NO TRABAJA';
			}


			if (isset($this->datossoceco["horario_trabajo"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["horario_trabajo"]);
			}
			else{
				$this->datossoceco["horario_trabajo"]='NO APLICA';
			}

			if (isset($this->datossoceco["sueldo_mensual"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["sueldo_mensual"]);
			}
			else{
				$this->datossoceco["sueldo_mensual"]='0';
			}

			if (isset($this->datossoceco["cargo_trabajo"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["cargo_trabajo"]);
			}
			else{
				$this->datossoceco["cargo_trabajo"]='';
			}


			if (isset($this->datossoceco["nombre_trabajo"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["nombre_trabajo"]);
			}
			else{
				$this->datossoceco["nombre_trabajo"]='';
			}
			if (isset($this->datossoceco["RazRetTrab"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["RazRetTrab"]);
			}
			else{
				$this->datossoceco["RazRetTrab"]='';
			}

			if (isset($this->datossoceco["tipo_traslado_iutoms"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["tipo_traslado_iutoms"]);
			}
			if (isset($this->datossoceco["problemas_traslado"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["problemas_traslado"]);
			}
			if (isset($this->datossoceco["tres_comidas"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["tres_comidas"]);
			}
			if (isset($this->datossoceco["hijos"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["hijos"]);
			}
			if (isset($this->datossoceco["viven_contigo"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["viven_contigo"]);
			}
			else{
				$this->datossoceco["viven_contigo"]='NO TIENE HIJOS';
			}
			if (isset($this->datossoceco["pareja"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["pareja"]);
			}
			if (isset($this->datossoceco["pers_dep"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["pers_dep"]);
			}
			if (isset($this->datossoceco["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->datossoceco["num_est"]);
			}
			
			if (isset($this->datossoceco["nro_comidas"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["nro_comidas"]);
			}

			if (isset($this->datossoceco["TotIngEst"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["TotIngEst"]);
			}

			if (isset($this->datossoceco["TotIngFam"])) {
				$this->comprobar->ComprobarCaracteres($this->datossoceco["TotIngFam"]);
			}

			

		}

		/**
		* Inserto un nuevo Dato Socioeconomico de un estudiante en el sistema 
		* @method consultar(), Comprobacion(), get_Departamentos_store()
		* @param [array] $datossoceco
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_DatosSocEco($datossoceco){

			$this->datossoceco = $datossoceco;
			$this->Comprobacion();

			$sql='INSERT INTO dat_socio_est("Trabaja", "HaTrabajado", "donde_trabaja", "horario_trabajo", "sueldo_mensual", "cargo_trabajo", "nombre_trabajo", "RazRetTrab", "tipo_traslado_iutoms", "problemas_traslado", "tres_comidas", "hijos", "viven_contigo", "pareja", "pers_dep", "nro_comidas", "num_est", "TotIngEst", "TotIngFam") values(';

			$values=$this->datossoceco["Trabaja"].", ".
			$this->datossoceco["HaTrabajado"].", ".
			"'".$this->datossoceco["donde_trabaja"]."'".", ".

			"'".$this->datossoceco["horario_trabajo"]."'".", ".
			$this->datossoceco["sueldo_mensual"].", ".
			"'".$this->datossoceco["cargo_trabajo"]."'".", ".
			"'".$this->datossoceco["nombre_trabajo"]."'".", ".
			"'".$this->datossoceco["RazRetTrab"]."'".", ".
			"'".$this->datossoceco["tipo_traslado_iutoms"]."', ".
			"'".$this->datossoceco["problemas_traslado"]."', '".
			$this->datossoceco["tres_comidas"]."', '".
			$this->datossoceco["hijos"]."'".", ".
			"'".$this->datossoceco["viven_contigo"]."'".", ".
			$this->datossoceco["pareja"].", ".
			$this->datossoceco["pers_dep"].", ".
			$this->datossoceco["nro_comidas"].", ".
			$this->datossoceco["num_est"].','.
			"'".$this->datossoceco["TotIngEst"]."'".", ".
			"'".$this->datossoceco["TotIngFam"]."'".')';
			$sql=$sql.$values;
			if ($this->db->consultar($sql)) {				
				$sql='SELECT max("id_dat_socio_est") as "id_dat_socio_est" FROM dat_socio_est limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return $filas["id_dat_socio_est"];			
			}
		}


		/**
		* Modifico el Dato Socioeconomico en el sistema 		
		* @param [array] $datossoceco
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_DatosSocEco_update($datossoceco){
			$this->datossoceco = $datossoceco;
			$this->Comprobacion();


			$sql='UPDATE dat_socio_est  SET  "TotIngEst"='.$datossoceco["TotIngEst"].', "TotIngFam"='.$datossoceco["TotIngFam"].' WHERE id_dat_socio_est='.$datossoceco["id_dat_socio_est"];

			if ($this->db->consultar($sql)) {				
				return 1;			
			}

		}

		/**
		* Consulto un Registro de Datos Socioeconomicos 
		* @param [integer] $id
		* @return [array] [datossoceco]   
		*/
		public function get_DatosSocEco_Est($id){
			$sql='SELECT * from dat_socio_est where "id_dat_socio_est"='.$id;
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				if ($filas["Trabaja"]==2 and $filas["HaTrabajado"]==2) {
					$filas["donde_trabaja"]="NO APLICA";
					$filas["horario_trabajo"]="NO APLICA";
					$filas["sueldo_mensual"]="NO APLICA";
					$filas["nombre_trabajo"]="NO APLICA";
					$filas["cargo_trabajo"]="NO APLICA";	
				}

				if ($filas["Trabaja"]==1) {
					$filas["Trabaja"]="SI";
				}
				else{
					$filas["Trabaja"]="NO";
				}

				if ($filas["HaTrabajado"]==1) {
					$filas["HaTrabajado"]="SI";
				}
				else{
					$filas["HaTrabajado"]="NO";
				}

				if ($filas["problemas_traslado"]==1) {
					$filas["problemas_traslado"]="SI";
				}
				else{
					$filas["problemas_traslado"]="NO";
				}

				if ($filas["hijos"]==0) {
					$filas["Thijos"]="NO";
				}
				else{
					$filas["Thijos"]="SI";
				}

				if ($filas["viven_contigo"]=='NO TIENE HIJOS') {
					$filas["viven_contigo"]='';
				}

				if ($filas["pareja"]==1) {
					$filas["pareja"]="SI";
				}
				else{
					$filas["pareja"]="NO";
				}

				if ($filas["pers_dep"]==0) {
					$filas["pers_dep"]="NO";
					$filas["pers_dep_nro"]="";
					
				}
				else{
					$filas["pers_dep_nro"]=$filas["pers_dep"];
					$filas["pers_dep"]="SI";
					
				}


				if ($filas["tipo_traslado_iutoms"]==1) {
					$filas["tipo_traslado_iutoms"]="Transporte Particular";

				}
				else if ($filas["tipo_traslado_iutoms"]==2) {
					$filas["tipo_traslado_iutoms"]="Transporte Público";
				}
				else{
					$filas["tres_comidas"]="Caminando";
				}

				if ($filas["tres_comidas"]=='SI' or $filas["tres_comidas"]=='tres comidas')
				{
					$filas["tres_comidas_neg"]="";
					$filas["tres_comidas"]="SI";
				}
				else{
					$filas["tres_comidas_neg"]=$filas["tres_comidas"];
					$filas["tres_comidas"]="NO";
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco[0];

		}



		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto el campo problema de traslados (problemas_traslado). Si tienen, si no y total	
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/

		public function get_DatosSocEco_Est_Estadisticas_probTraslado($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT dse."problemas_traslado" FROM estudiantesencuestados ee, dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and dse."problemas_traslado"= '."'NO'".' and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			$this->datossoceco["NO"] = pg_num_rows($consulta);

			$sql='SELECT dse."problemas_traslado" FROM estudiantesencuestados ee, dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			$this->datossoceco["Total"] =pg_num_rows($consulta);
			$this->datossoceco["SI"] = $this->datossoceco["Total"]-$this->datossoceco["NO"];
			return $this->datossoceco;
		}


		/**
		* Consulto el campo tipo de traslado al iutoms (tipo_traslado_iutoms)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_comTraslada($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT dse."tipo_traslado_iutoms", count(dse."tipo_traslado_iutoms") as "total" FROM estudiantesencuestados ee, dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."tipo_traslado_iutoms"';

			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["tipo_traslado_iutoms"]==1) {
					$filas["descripcion"]='Transporte Particular';
				}
				else if ($filas["tipo_traslado_iutoms"]==2) {
					$filas["descripcion"]='Transporte Público';
				}
				else if ($filas["tipo_traslado_iutoms"]==3) {
					$filas["descripcion"]='Caminando';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto el nro de estudiantes que trabajan  (Trabaja)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_trabaja($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT dse."Trabaja", count(dse."Trabaja") as "total" FROM estudiantesencuestados ee, dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."Trabaja"';

			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["Trabaja"]==1) {
					$filas["Trabaja"]='Si';
				}
				else if ($filas["Trabaja"]==2) {
					$filas["Trabaja"]='No';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto el nro de estudiantes que han trabajado  (HaTrabajado)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_trabajado($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT dse."HaTrabajado", count(dse."HaTrabajado") as "total" FROM estudiantesencuestados ee, dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."HaTrabajado"';

			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["HaTrabajado"]==1) {
					$filas["HaTrabajado"]='Si';
				}
				else if ($filas["HaTrabajado"]==2) {
					$filas["HaTrabajado"]='No';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}



		//Estadisticas Personal y Familiar

		/**
		* Consulto el nro de estudiantes que tienen hijos agrupandolo por el nro de hijos (hijos)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_TieneHijos($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as total, dse."hijos" FROM estudiantesencuestados ee, dat_socio_est dse
			where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."hijos" order by dse."hijos"';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["hijos"]==0) {
					$filas["Titulo"]='No Tiene';
				}
				else if ($filas["hijos"]==1) {
					$filas["Titulo"]=$filas["hijos"].' Hijo';
				}
				else{
					$filas["Titulo"]=$filas["hijos"].' Hijos';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto el nro de estudiantes que viven con sus hijos, (viven_contigo)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_Hijos_VivenContigo($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) AS "SI" FROM estudiantesencuestados ee,  dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "hijos">0 and viven_contigo='."'SI'";
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Titulo"]='SI';
				$filas["total"]=$filas["SI"];
				$this->datossoceco[] =$filas;
			}
			$sql='SELECT count(*) AS "NO" FROM estudiantesencuestados ee,  dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "hijos">0 and viven_contigo!='."'SI'";
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
					$filas["Titulo"]='NO';
					$filas["total"]=$filas["NO"];
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto nro estudiantes y nro de comidas que consumen, agrupandola por el nro de comidas  (nro_comidas)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_ComidasAlDia($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as "total", dse."nro_comidas" FROM estudiantesencuestados ee, dat_socio_est dse
			where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."nro_comidas"';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
	
				if ($filas["nro_comidas"]==1) {
					$filas["Titulo"]=$filas["nro_comidas"].' Comida';
				}
				else{
					$filas["Titulo"]=$filas["nro_comidas"].' Comidas';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}

		/**
		* Consulto nro estudiantes que tienen pareja  (pareja)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_Pareja($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) AS "SI"  FROM estudiantesencuestados ee,  dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "pareja"=1';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Titulo"]='SI';
				$filas["total"]=$filas["SI"];
				$this->datossoceco[] =$filas;
			}

			$sql='SELECT count(*) AS "SI"  FROM estudiantesencuestados ee,  dat_socio_est dse where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and "pareja"=2';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Titulo"]='No';
				$filas["total"]=$filas["SI"];
				$this->datossoceco[] =$filas;
			}



			return $this->datossoceco;
		}

		/**
		* Consulto nro estudiantes que tienen una persona que dependa de ellos, agrupandolo por el nro de personas (pers_dep)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_PersonasDependen($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT count(*) as total, dse."pers_dep" FROM estudiantesencuestados ee, dat_socio_est dse
			where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by dse."pers_dep" order by dse."pers_dep"';
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["pers_dep"]==0) {
					$filas["Titulo"]='No Tiene';
				}
				else if ($filas["pers_dep"]==1) {
					$filas["Titulo"]=$filas["pers_dep"].' Persona';
				}
				else{
					$filas["Titulo"]=$filas["pers_dep"].' Personas';
				}

				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco;
		}


		/**
		* Consulto el maximo, minimo y promedio del Total de Ingresos Estudiantiles (TotIngEst) y Total de Ingresos Familiares (TotIngFam)
		* @param [date] $inicio, [date] $fin
		* @return  [array] [datossoceco]
		*/
		public function get_DatosSocEco_Est_Estadisticas_Presupuesto($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT max(dse."TotIngEst") as "MaxTotIngEst", min(dse."TotIngEst") as "MinTotIngEst", avg(dse."TotIngEst") as "AvgTotIngEst", max(dse."TotIngFam") as "MaxTotIngFam", min(dse."TotIngFam") as "MinTotIngFam", Avg(dse."TotIngFam") as "AvgTotIngFam"  FROM estudiantesencuestados ee, dat_socio_est dse
			where dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			$consulta = $this->db->consultar($sql);	
			
			while ($filas=pg_fetch_assoc($consulta)) {
				
				//$filas["minimo_AvgTotIngEst"]= la mitad el promedio del ingreso estudiantil
				$filas["minimo_AvgTotIngEst"]=$filas["AvgTotIngEst"]/2;

				//$filas["mayor_AvgTotIngEst"]= el promedio mas la diferencia entre el mayor y el promedio del ingreso estudiantil
				//
				$filas["diferencia"]=($filas["MaxTotIngEst"]-$filas["AvgTotIngEst"])/2;
				$filas["mayor_AvgTotIngEst"]=$filas["AvgTotIngEst"]+$filas["diferencia"];


				//$filas["Total_minimo_AvgTotIngEst"]= Estudiantes que esten por debajo del minimo_AvgTotIngEst 
				$filas["Total_minimo_AvgTotIngEst"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngEst"<'.$filas["minimo_AvgTotIngEst"]);

				//$filas["Total_minimo_AvgTotIngEst"]= Estudiantes que esten por debajo del promedio pero por encima del minimo_AvgTotIngEst
				$filas["Total_menor_AvgTotIngEst"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngEst">='.$filas["minimo_AvgTotIngEst"].' and  dse."TotIngEst"<'.$filas["AvgTotIngEst"]);

				//$filas["Total_minimo_AvgTotIngEst"]= Estudiantes que esten por encima del promedio pero por debajo del mayor_AvgTotIngEst
				$filas["Total_promedio_AvgTotIngEst"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngEst">='.$filas["AvgTotIngEst"].' and  dse."TotIngEst"<'.$filas["mayor_AvgTotIngEst"]);


				//$filas["Total_minimo_AvgTotIngEst"]= Estudiantes que esten por encima del mayor_AvgTotIngEst 
				$filas["Total_mayor_AvgTotIngEst"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngEst">='.$filas["mayor_AvgTotIngEst"]);


				$filas["MaxTotIngEst"]=number_format($filas["MaxTotIngEst"], 2, '.', '');
				$filas["MinTotIngEst"]=number_format($filas["MinTotIngEst"], 2, '.', '');
				$filas["AvgTotIngEst"]=number_format($filas["AvgTotIngEst"], 2, '.', '');
				$filas["minimo_AvgTotIngEst"]=number_format($filas["minimo_AvgTotIngEst"], 2, '.', '');
				$filas["mayor_AvgTotIngEst"]=number_format($filas["mayor_AvgTotIngEst"], 2, '.', '');


				//$filas["minimo_AvgTotIngEst"]= la mitad el promedio del ingreso familiar
				$filas["minimo_AvgTotIngFam"]=$filas["AvgTotIngFam"]/2;

				//$filas["mayor_AvgTotIngFam"]= el promedio mas la mitad el promedio del ingreso familiar
				$filas["diferencia"]=($filas["MaxTotIngEst"]-$filas["AvgTotIngFam"])/2;
				$filas["mayor_AvgTotIngFam"]=$filas["AvgTotIngFam"]+$filas["diferencia"];


				//$filas["Total_minimo_AvgTotIngFam"]= Estudiantes que esten por debajo del minimo_AvgTotIngFam 
				$filas["Total_minimo_AvgTotIngFam"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngFam"<'.$filas["minimo_AvgTotIngFam"]);

				//$filas["Total_minimo_AvgTotIngFam"]= Estudiantes que esten por debajo del promedio pero por encima del minimo_AvgTotIngFam
				$filas["Total_menor_AvgTotIngFam"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngFam">='.$filas["minimo_AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["AvgTotIngFam"]);

				//$filas["Total_minimo_AvgTotIngFam"]= Estudiantes que esten por encima del promedio pero por debajo del mayor_AvgTotIngFam
				$filas["Total_promedio_AvgTotIngFam"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngFam">='.$filas["AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["mayor_AvgTotIngFam"]);

				//$filas["Total_minimo_AvgTotIngFam"]= Estudiantes que esten por encima del mayor_AvgTotIngFam 
				$filas["Total_mayor_AvgTotIngFam"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin, 'dse."TotIngFam">='.$filas["mayor_AvgTotIngFam"]);



				$filas["Total_minimo_AvgTotIngFam_Familiares"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares($inicio, $fin, 'dse."TotIngFam"<'.$filas["minimo_AvgTotIngFam"]);
				$filas["Total_minimo_AvgTotIngFam_NoTrabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_NoTrabajan($inicio, $fin, 'dse."TotIngFam"<'.$filas["minimo_AvgTotIngFam"]);
				$filas["Total_minimo_AvgTotIngFam_Trabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_Trabajan($inicio, $fin, 'dse."TotIngFam"<'.$filas["minimo_AvgTotIngFam"]);
				// --------------------------------------------------------------

				$filas["Total_menor_AvgTotIngFam_Familiares"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares($inicio, $fin, 'dse."TotIngFam">='.$filas["minimo_AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["AvgTotIngFam"]);
				$filas["Total_menor_AvgTotIngFam_NoTrabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_NoTrabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["minimo_AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["AvgTotIngFam"]);
				$filas["Total_menor_AvgTotIngFam_Trabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_Trabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["minimo_AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["AvgTotIngFam"]);

				// --------------------------------------------------------------
				$filas["Total_promedio_AvgTotIngFam_Familiares"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares($inicio, $fin, 'dse."TotIngFam">='.$filas["AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["mayor_AvgTotIngFam"]);
				$filas["Total_promedio_AvgTotIngFam_NoTrabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_NoTrabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["mayor_AvgTotIngFam"]);
				$filas["Total_promedio_AvgTotIngFam_Trabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_Trabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["AvgTotIngFam"].' and  dse."TotIngFam"<'.$filas["mayor_AvgTotIngFam"]);
				// --------------------------------------------------------------
				$filas["Total_mayor_AvgTotIngFam_Familiares"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares($inicio, $fin, 'dse."TotIngFam">='.$filas["mayor_AvgTotIngFam"]);
				$filas["Total_mayor_AvgTotIngFam_NoTrabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_NoTrabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["mayor_AvgTotIngFam"]);
				$filas["Total_mayor_AvgTotIngFam_Trabajan"]=$this->get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_Trabajan($inicio, $fin, 'dse."TotIngFam">='.$filas["mayor_AvgTotIngFam"]);

				

				$filas["minimo_AvgTotIngFam"]=number_format($filas["minimo_AvgTotIngFam"], 2, '.', '');
				$filas["mayor_AvgTotIngFam"]=number_format($filas["mayor_AvgTotIngFam"], 2, '.', '');
				$filas["MaxTotIngFam"]=number_format($filas["MaxTotIngFam"], 2, '.', '');
				$filas["MinTotIngFam"]=number_format($filas["MinTotIngFam"], 2, '.', '');
				$filas["AvgTotIngFam"]=number_format($filas["AvgTotIngFam"], 2, '.', '');
				$this->datossoceco[] =$filas;
			}
			return $this->datossoceco[0];
		}


		/**
		* Consulto el nro de estudiantes por promedio de presupuesto
		* @param [date] $inicio, [date] $fin
		* @return  [integer] ["total"]
		*/
		private function get_DatosSocEco_Est_Estadisticas_Presupuesto_Personas($inicio, $fin,$counsulta)
		{
			$sql='SELECT count(*) as total FROM estudiantesencuestados ee, dat_socio_est dse
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			$consulta = $this->db->consultar($sql);	
			$array= array();
			while ($filas=pg_fetch_assoc($consulta)) {
				$array =$filas;
			}
			return $array["total"];
		}

		/**
		* Consulto el nro de miembro familiares por promedio de presupuesto
		* @param [date] $inicio, [date] $fin
		* @return  [integer] ["total"]
		*/
		private function get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares($inicio, $fin,$counsulta)
		{
			$sql='SELECT count(*) as "total" FROM estudiantesencuestados ee, dat_socio_est dse, grupo_familiar_est g
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."estudiante"=g."num_est" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' group by ee."estudiante"';
			$consulta = $this->db->consultar($sql);	
			$array= array();
			$sum=0;

			while ($filas=pg_fetch_assoc($consulta)) {
				$sum=$sum+$filas["total"]+1;
				$array=$filas;
			}

			if (pg_num_rows($consulta)>0) {
				return round($sum/pg_num_rows($consulta));
			}
			return 0;
		}


		/**
		* Consulto el nro de miembro familiares que no trabajan por promedio de presupuesto
		* @param [date] $inicio, [date] $fin
		* @return  [integer] ["total"]
		*/
		private function get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_NoTrabajan($inicio, $fin,$counsulta)
		{
			$sql='SELECT count(*) as "total" FROM estudiantesencuestados ee, dat_socio_est dse, grupo_familiar_est g
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."estudiante"=g."num_est" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and g."ingreso_mensual"=0 group by ee."estudiante"';
			$consulta = $this->db->consultar($sql);	
			$array= array();
			$sum=0;

			while ($filas=pg_fetch_assoc($consulta)) {
				$sum=$sum+$filas["total"];
				$array=$filas;
			}
			
			$sql='SELECT * FROM estudiantesencuestados ee, dat_socio_est dse
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and dse."Trabaja"=0';
			$consulta = $this->db->consultar($sql);	
			$sum=$sum+ pg_num_rows($consulta);		

			$sql='SELECT * FROM estudiantesencuestados ee, dat_socio_est dse
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";
			$consulta = $this->db->consultar($sql);	

			if (pg_num_rows($consulta)>0) {
				return ceil($sum/pg_num_rows($consulta));
			}
			return 0;

		}

		/**
		* Consulto el nro de miembro familiares que trabajan por promedio de presupuesto
		* @param [date] $inicio, [date] $fin
		* @return  [integer] ["total"]
		*/
		private function get_DatosSocEco_Est_Estadisticas_Presupuesto_Familiares_Trabajan($inicio, $fin,$counsulta)
		{
			$sql='SELECT count(*) as "total" FROM estudiantesencuestados ee, dat_socio_est dse, grupo_familiar_est g
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."estudiante"=g."num_est" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and g."ingreso_mensual">0 group by ee."estudiante"';
			$consulta = $this->db->consultar($sql);	
			$array= array();
			$sum=0;
			

			while ($filas=pg_fetch_assoc($consulta)) {
				$sum=$sum+$filas["total"];
				$array=$filas;
			}

			$sql='SELECT * FROM estudiantesencuestados ee, dat_socio_est dse
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'".' and dse."Trabaja"=1';
			$consulta = $this->db->consultar($sql);	
			$sum=$sum+ pg_num_rows($consulta);			
			

			$sql='SELECT * FROM estudiantesencuestados ee, dat_socio_est dse
			where '.$counsulta.' and dse."id_dat_socio_est"=ee."socioeconomicos" and ee."fecha">='."'".$inicio."'".' and ee."fecha"<='."'".$fin."'";

			$consulta = $this->db->consultar($sql);	
			$total=pg_num_rows($consulta);				

			if ($sum>0) {
				return ceil($sum/$total);
			}
			return 0;
			
		}
		
	}
?>
