<?php 	

	/**
	* Clase AyudaEconxEstudiante
	* Comparte la tabla ayuda_econ_x_estudiante en la Base de Datos
	* Es llamado en los Controladores AtencionCitasController, EncuestaController, EstadisticasController, InformesController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador (Al ser tabla de terceros no se cumple mucho esta regla)
	*
	* ejem: get_AyudaEconxEstudiante  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class AyudaEconxEstudiante

	{
		private $db;
		private $ayudeconest;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] ayudeconest
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->ayudeconest= array();			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] datossoceco
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->ayudeconest["id_ayuda_est"])) {
				$this->comprobar->ComprobarNumeric($this->ayudeconest["id_ayuda_est"]);
			}
			
			if (isset($this->ayudeconest["id_ayud_econ"])) {
				$this->comprobar->ComprobarNumeric($this->ayudeconest["id_ayud_econ"]);
			}

			if (isset($this->ayudeconest["monto"])) {
				$this->comprobar->ComprobarNumeric($this->ayudeconest["monto"]);
			}

			if (isset($this->ayudeconest["num_est"])) {
				$this->comprobar->ComprobarNumeric($this->ayudeconest["num_est"]);
			}
			if (isset($this->ayudeconest["idencuesta"])) {
				$this->comprobar->ComprobarNumeric($this->ayudeconest["idencuesta"]);
			}
		}	

		/**
		* Inserto un nuevo Dato Socioeconomico de un estudiante en el sistema 
		* @method consultar(), Comprobacion(), get_Departamentos_store()
		* @param [array] $datossoceco
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function get_AyudaEconxEstudiante($idencuesta){

			$this->comprobar->ComprobarNumeric($idencuesta);

			$sql='SELECT (SELECT "desc_ayud_econ" from ayuda_economica where "id_ayud_econ"=a."id_ayud_econ") as "ayud_econ", "monto" from ayuda_econ_x_estudiante a where a."idencuesta"='.$idencuesta;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				$filas["monto"]=number_format($filas["monto"], 2, ',', '.');

				$this->ayudeconest[] =$filas;
			}
			return $this->ayudeconest;
		}

		/**
		* Inserto una nueva ayuda economica de un estudiante segun la encuenta en el sistema 
		* @method consultar(), ComprobarNumeric()
		* @param [array] $ayudeconest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_AyudaEconxEstudiante($ayudeconest){
			$sql='INSERT INTO ayuda_econ_x_estudiante("id_ayud_econ", "monto", "num_est", "idencuesta") values';
			$values="";

			foreach ($ayudeconest as $ayudaeconomica) {
				$this->comprobar->ComprobarNumeric($ayudaeconomica["id_ayud_econ"]);
				$this->comprobar->ComprobarNumeric($ayudaeconomica["monto"]);
				$this->comprobar->ComprobarNumeric($ayudaeconomica["idencuesta"]);
				$this->comprobar->ComprobarNumeric($ayudaeconomica["num_est"]);

				$values=$values."(".$ayudaeconomica["id_ayud_econ"].", ".$ayudaeconomica["monto"].", ".

				"'".$ayudaeconomica["num_est"]."', ".$ayudaeconomica["idencuesta"]."),";				
			}
			$values = substr($values, 0, -1);
			$sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {
				return "1";				
			}

			return '';
		}

		/**
		* Modifico una ayuda economica de un estudiante segun la encuenta modificada en el sistema 
		* @method consultar(), ayudeconest()
		* @param [array] $ayudeconest
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_AyudaEconxEstudiante_update($ayudeconest){

			$this->ayudeconest = $ayudeconest;
			$this->Comprobacion();

			$sql='UPDATE ayuda_econ_x_estudiante SET "idencuesta"='.$this->ayudeconest["idencuesta"].' WHERE "num_est"='.$this->ayudeconest["num_est"].'';
			
			if ($this->db->consultar($sql)) {
				return "1";				
			}

			
		}
	}
?>