<?php 
	/**
	* 
	*/
	class SeguridadDatos 

	{
		public function ComprobarFecha($fecha)
		{
			if (isset($fecha) && !(date("Y-m-d",strtotime($fecha))) ) {
				die("El valor ".$fecha." debe ser de tipo date");
			}
		}

		public function ComprobarFechaHora($date, $format = 'Y-m-d H:i:s')
		{
		    $d = DateTime::createFromFormat($format, $date);
		    if (($d && $d->format($format) == $date)==FALSE) {
		    	die("Error! el valor ".$date." debe ser de tipo date time");
		    }
		}


		public function ComprobarNumeric($nro)
		{
			if (isset($nro) && is_numeric($nro)==FALSE) {
				die("El valor ".$nro." debe ser de tipo numerico");
			}

			
		}


		public function ComprobarCaracteres($cadena)
		{
			if (strpos($cadena, "';") !== FALSE || strpos($cadena, '--') !== FALSE || strpos($cadena, '/*') !== FALSE ||strpos($cadena, ');') !== FALSE ) {
					die("El valor ".$cadena." debe ser de tipo string");
				}

				if (preg_match('/union/i', $cadena) || preg_match('/drop/i', $cadena)) {
					die("Se ha encontrado una similitud con algun comando de base de datos en la cadena ".$cadena);
				}
		}






		



	}
?>