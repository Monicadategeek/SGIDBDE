<?php

require_once("../Modelo/OperacionesSistemicas.php");
require_once("../Modelo/EstudiantesEncuestados.php");
require_once("../Modelo/Terceros/dat_socio_est.php");
require_once("../Modelo/Terceros/dat_inter_est.php");
require_once("../Modelo/Terceros/ayuda_econ_x_estudiante.php");
require_once("../Modelo/Terceros/gastos_x_estudiante.php");
require_once("../Modelo/Terceros/dat_fisic_ambient_est.php");
require_once("../Modelo/Terceros/actividades_t_l_x_estudiante.php");
require_once("../Modelo/Terceros/deportes_x_estudiante.php");
require_once("../Modelo/Terceros/habilidades_x_estudiante.php");
require_once("../Modelo/Terceros/eventos_x_estudiantes.php");
//funcionalidad


Ini_set ('max_execution_time', 500);

$consulta= new DatosSocEcon();
$estudiantes=$consulta->get_DatosSocEco_Est();
if (count($estudiantes)>0) {

	foreach ($estudiantes as $estudiante) {
		$encuestado["estudiante"]=$estudiante["num_est"];
		$encuestado["socioeconomicos"]=$estudiante["id_dat_socio_est"];

		$consulta= new EstudiantesEncuestados();
		$result=$consulta->set_FechasEncuestas($encuestado);
		
		$encuesta["Id"]=$result;
 		$encuesta["idencuesta"]=$result;
		$encuesta["num_est"]=$encuestado["estudiante"];


		$consulta= new AyudaEconxEstudiante();
		$consulta->set_AyudaEconxEstudiante_update($encuesta);

		$consulta= new GastosxEstudiante();
		$consulta->set_GastosxEstudiante_Update($encuesta);	
		
		$consulta= new DatosFisAmbEst();
		$encuesta["ambientales"]=$consulta->get_DatosFisAmbEst($encuesta);

		if (is_numeric($encuesta["ambientales"])) {
			$encuesta["campo"]="ambientales";
			$consulta= new EstudiantesEncuestados();
			$result=$consulta->set_FechasEncuestas_update_column($encuesta);
		}

		$encuesta["campo"]="culturales";
		$encuesta["culturales"]=1;

		$consulta= new ActivxEst();
		$consulta->set_ActivxEst_Update($encuesta);

		$consulta= new DeportesxEstudiante();
		$consulta->Set_DeportesxEstudiante_Update($encuesta);

		$consulta= new HabilidadesxEstudi();
		$consulta->set_HabilidadesxEstudi_Update($encuesta);

		$consulta= new EventosxEstud();
		$consulta->set_EventosxEstud_Update($encuesta);

		$consulta= new EstudiantesEncuestados();
		$result=$consulta->set_FechasEncuestas_update_column($encuesta);


		$consulta= new DatInterEst();
		$encuesta["interesAyu"]=$consulta->get_DatInterEst($encuesta);
		if (is_numeric($encuesta["interesAyu"])) {
			$encuesta["campo"]="interesAyu";

			$consulta= new EstudiantesEncuestados();
			$result=$consulta->set_FechasEncuestas_update_column($encuesta);
		}
		




	}
}
echo "Terminado";
		return false;
?>