<?php 

	

	/**
	* 
	*/
	class EstudioSocioeconomico 

	{
		private $db;
		private $estudio;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->estudio= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=14;//listo
			
		}


			
		private function Comprobacion()
		{
			if (isset($this->estudio["IdEstudio"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["IdEstudio"]);
			}
			if (isset($this->estudio["grupofamiliar"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["grupofamiliar"]);
			}
			if (isset($this->estudio["OBSegrind"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["OBSegrind"]);
			}
			if (isset($this->estudio["OBSegrfam"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["OBSegrfam"]);
			}
			if (isset($this->estudio["dtrbHabitaciones"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["dtrbHabitaciones"]);
			}		

			if (isset($this->estudio["areaficicoambiental"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["areaficicoambiental"]);
			}
			if (isset($this->estudio["diagnosticosocial"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["diagnosticosocial"]);
			}
			if (isset($this->estudio["conclurecomen"])) {
				$this->comprobar->ComprobarCaracteres($this->estudio["conclurecomen"]);
			}
			
			if (isset($this->estudio["usseg"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["usseg"]);
			}
			if (isset($this->estudio["ireconomico"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["ireconomico"]);
			}
			if (isset($this->estudio["oreconomico"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["oreconomico"]);
			}
			if (isset($this->estudio["fireconomico"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["fireconomico"]);
			}
			if (isset($this->estudio["cvpadres"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["cvpadres"]);
			}
			if (isset($this->estudio["uvpadres"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["uvpadres"]);
			}
			if (isset($this->estudio["agregarProg"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["agregarProg"]);
			}
			if (isset($this->estudio["prepayu"])) {
				$this->comprobar->ComprobarNumeric($this->estudio["prepayu"]);
			}
			else{
				$this->estudio["prepayu"]=0;
			}

			
			
		}


		public function set_EstudioSocioeconomico_store($estudio){ 
			$this->estudio = $estudio;
			$this->Comprobacion();

			if ($this->get_EstudioSocioeconomico_store()==0) {
				$sql='INSERT INTO estudiosocioeconomico("IdEstudio", grupofamiliar, "OBSegrind", "OBSegrfam", "dtrbHabitaciones", conclurecomen, areaficicoambiental, diagnosticosocial, usseg, ireconomico, oreconomico, fireconomico, cvpadres, uvpadres,  "agregarProg", "prepayu")';


	            $values='VALUES ('.$this->estudio["IdEstudio"].', '."'".$this->estudio["grupofamiliar"]."'".', '."'".$this->estudio["OBSegrind"]."'".', '."'".$this->estudio["OBSegrfam"]."'".', '."'".$this->estudio["dtrbHabitaciones"]."'".', '."'".$this->estudio["conclurecomen"]."'".', '."'".$this->estudio["areaficicoambiental"]."'".', '."'".$this->estudio["diagnosticosocial"]."'".','.$this->estudio["usseg"].', '.$this->estudio["ireconomico"].', 
			            '.$this->estudio["oreconomico"].', '.$this->estudio["fireconomico"].', '.$this->estudio["cvpadres"].', '.$this->estudio["uvpadres"].', '.$this->estudio["agregarProg"].', '.$this->estudio["prepayu"].');';
			    $sql=$sql.$values;
				if ($this->db->consultar($sql)) {
					

					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->estudio["Responsable"],1,$this->estudio["IdEstudio"],$this->tabla);	
					return "1";
				}
				else{
					return "2";
				}
			}
			else{
				die("Ya fue registrado el informe");
			}
			

			
		}
		private function get_EstudioSocioeconomico_store(){
			$sql='SELECT * FROM estudiosocioeconomico where "IdEstudio"='.$this->estudio["IdEstudio"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}


		public function set_EstudioSocioeconomico_list($IdEstudiante){

			$this->comprobar->ComprobarNumeric($IdEstudiante);

			$sql='SELECT c."Fecha", c."Hora", c."IdCitas", (SELECT concat(usu."nombre", '. "' '". ', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable" FROM Citas c inner join informes i on c."IdCitas"=i."CodInforme" inner join estudiosocioeconomico ese on  i."CodInforme"=ese."IdEstudio"  where c."Asistencia"=2 and c."IdEstudiante"='.$IdEstudiante;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));
				$filas["boton"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$filas["IdCitas"].');"><i class="fa fa-book"></i></button>';

				$this->informes[] =$filas;

			}
			return $this->informes;

		}

		public function set_EstudioSocioeconomico_show($IdEstudio, $Responsable){

			$this->comprobar->ComprobarNumeric($IdEstudio);

			$sql='SELECT grupofamiliar, "OBSegrind", "OBSegrfam", "dtrbHabitaciones", 
			areaficicoambiental, diagnosticosocial, usseg, ireconomico, oreconomico, 
			fireconomico, cvpadres, uvpadres, conclurecomen, "agregarProg", prepayu
			FROM public.estudiosocioeconomico where "IdEstudio"='.$IdEstudio;

			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["prepayu"]==1) {
					$filas["prepayu"]='SI';
				}
				else{
					$filas["prepayu"]='NO';
				}
				$this->informes[] =$filas;

			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$IdEstudio,$this->tabla);
			
			return $this->informes[0];

		}
		

		
	
	

		

		

		



	}
?>