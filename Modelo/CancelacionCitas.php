<?php 

	/**
	* Es llamado en CitasController, GestionCitasController, CancelarCitasController
	*/
	class CancelacionCitas 

	{
		private $db;
		private $cancelaciones;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;

			$this->cancelaciones= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=6; //Listo
			
		}


		private function Comprobacion()
		{
			if (isset($this->cancelaciones["id"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["id"]);
			}

			if (isset($this->cancelaciones["Fecha"])) {
				$this->comprobar->ComprobarFecha($this->cancelaciones["Fecha"]);
			}

			if (isset($this->cancelaciones["Fecha_Inic"])) {
				$this->comprobar->ComprobarFecha($this->cancelaciones["Fecha_Inic"]);
			}

			if (isset($this->cancelaciones["Fecha_Fin"])  ) {
				$this->comprobar->ComprobarFecha($this->cancelaciones["Fecha_Fin"]);				
			}
			if (isset($this->cancelaciones["Razon"])) {
				$this->comprobar->ComprobarCaracteres($this->cancelaciones["Razon"]);
			}
			if (isset($this->cancelaciones["Servicio"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["Servicio"]);
			}

			if (isset($this->cancelaciones["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["Responsable"]);
			}

			if (isset($this->cancelaciones["Tipo_Cancelacion"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["Tipo_Cancelacion"]);
			}
		}


		public function get_CancelacionCitas_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT c."Id", c."Fecha_Inic", c."Fecha_Fin", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Tipo_Cancelacion") as "Tipo_Cancelacion",  (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "Servicio", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=c."Responsable") as "Responsable"

			FROM cancelacioncita c, Departamentos d where c."Servicio"=d."IdDepartamentos" order by c."Fecha_Inic" desc ';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha_Inic"]=date('d-m-Y', strtotime($filas["Fecha_Inic"]));
				$filas["Fecha_Fin"]=date('d-m-Y', strtotime($filas["Fecha_Fin"]));
				$this->cancelaciones[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->cancelaciones;
		}



		public function set_CancelacionCitas_show($id, $Responsable){
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT c."Id", c."Fecha_Inic", c."Razon", c."Fecha_Registro", c."Fecha_Fin", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Tipo_Cancelacion") as "Tipo_Cancelacion",  (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "Servicio", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=c."Responsable") as "Responsable"

			FROM cancelacioncita c, Departamentos d where c."Servicio"=d."IdDepartamentos" and c."Id"='.$id;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha_Inic"]=date('d-m-Y', strtotime($filas["Fecha_Inic"]));
				$filas["Fecha_Fin"]=date('d-m-Y', strtotime($filas["Fecha_Fin"]));

                $filas["Fecha_Registro"]=date('d F Y g:ia', strtotime($filas["Fecha_Registro"]));
				$this->cancelaciones[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable, 6,'null',$this->tabla);
			return $this->cancelaciones[0];
		}

		
	


		public function set_CancelacionCitas_store($cancelaciones){ //Consulta Registrar Depertamentos
			$this->cancelaciones = $cancelaciones;
			$this->Comprobacion();

			if ($this->get_CancelacionCitas_store()==0) {
				$sql= 'INSERT INTO cancelacioncita ("Fecha_Inic", "Fecha_Fin", "Razon", "Servicio", "Fecha_Registro", "Responsable", "Tipo_Cancelacion") VALUES (';

				$values="'".$this->cancelaciones["Fecha_Inic"]."', '".$this->cancelaciones["Fecha_Fin"]."', '".$this->cancelaciones["Razon"]."', ".$this->cancelaciones["Servicio"].", now(), ".$this->cancelaciones["Responsable"].", ".$this->cancelaciones["Tipo_Cancelacion"].");";
				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {

					$sql='SELECT max("Id") as "id" FROM cancelacioncita limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->cancelaciones["Responsable"],1,$filas["id"],$this->tabla);

					return "1";
					
				}
			}
			else{
				return "Existe un registro que cubre parcial o total el período de las fechas";
			}
		}

		private function get_CancelacionCitas_store(){
			$sql='SELECT * FROM CancelacionCita c  where c."Fecha_Inic">='."'".$this->cancelaciones["Fecha_Inic"]."'".' and c."Fecha_Fin">='."'".$this->cancelaciones["Fecha_Fin"]."'".' and "Servicio"='.$this->cancelaciones["Servicio"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}



		

		/*public function get_Citas_show($id){

			$sql='SELECT c."Fecha", c."IdCitas", c."Hora", c."Asistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and c."IdCitas"='.$id;

			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->cancelaciones[] =$filas;
			}

			//$this->OperacionesSistemicas->array_OperacionesSistemicas($cancelaciones["Responsable"],6,$filas["IdCitas"],$this->tabla);
			return $this->cancelaciones[0];
		}*/

		

			

		
		/*public function get_Citas_edit($id){
			$sql='SELECT c.*, d."Sede", d."TipoDepto" FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and "IdCitas"='.$id;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->cancelaciones[] =$filas;
			}
			return $this->cancelaciones[0];

		}

	*/
		


		public function set_Citas_update($cancelaciones){ //Consulta Modificar Depertamentos
			$sql= 'UPDATE Citas SET "Servicio"='.$cancelaciones["Servicio"].', "Hora"='."'".$cancelaciones["hora"]."'".', "Fecha"='."'".$cancelaciones["fecha"]."'".' WHERE  "IdCitas"='.$cancelaciones["IdCitas"];
			if ($this->db->consultar($sql)) {
				return "1";
			}
			else{
				return "2";
			}	

		}

		
		public function set_Citas_Cancelar($cancelaciones){ 
			$this->cancelaciones = $cancelaciones;
			$this->Comprobacion();
			$sql= 'UPDATE Citas set "Asistencia"='.$this->cancelaciones["Asistencia"].'   WHERE "Asistencia"=1 and "Servicio"='.$this->cancelaciones["Servicio"].' and "Fecha"='."'".$this->cancelaciones["Fecha"]."'";
			if ($this->db->consultar($sql)) {
				
				$this->OperacionesSistemicas->array_OperacionesSistemicas($this->cancelaciones["Responsable"],2,$cancelaciones["Responsable"],5);	
				return "1";
			}
			else{
				return "2";
			}
		}

		public function get_Citas_Cancelar($cancelaciones){ 
			$this->cancelaciones = $cancelaciones;
			$this->Comprobacion();
			$sql= 'SELECT e."cedula", e."nombres", e."apellidos", e."correo" from Citas c, estudiantes e WHERE c."IdEstudiante"=e."num_est" and c."Asistencia"='.$this->cancelaciones["Asistencia"].' and c."Servicio"='.$this->cancelaciones["Servicio"].' and c."Fecha"='."'".$this->cancelaciones["Fecha"]."'";

			$consulta = $this->db->consultar($sql);

			$this->cancelaciones = array();	
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->cancelaciones[] =$filas;
			}
			return $this->cancelaciones;
			
		}


		/*public function set_Citas_destroy($id){ //Consulta Registrar Depertamentos

			$sql= 'DELETE FROM Citas WHERE "IdCitas"='.$id;
			if ($this->db->consultar($sql)) {
				//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$id,$this->tabla);
				return "1";
			}
			else{
				return "2";
			}
		}*/

										#OTRAS CONSULTAS

		public function get_Citas_Canceladas($Servicio)
		{
		
			$this->comprobar->ComprobarNumeric($Servicio);
			

			$sql='SELECT c."Fecha_Inic", c."Fecha_Fin" from cancelacioncita c where (c."Fecha_Inic">now() or c."Fecha_Fin">now()) and c."Servicio"='.$Servicio.' order by c."Fecha_Inic" desc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->cancelaciones[] =$filas;
			}

			return $this->cancelaciones;

		}

		

		

		



	}
?>