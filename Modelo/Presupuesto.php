<?php
	
	/**
	* Es llamado en PresupuestoController
	*/ 
	class Presupuesto 
	{
		private $db;
		private $presupuesto;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->presupuesto =array();			
			$this->comprobar = new SeguridadDatos;
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=8;//Listo
			
		}

		private function Comprobacion()
		{
			if (isset($this->presupuesto["IdPresupuesto"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["IdPresupuesto"]);
			}
			if (isset($this->presupuesto["PresupuestoAsociado"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["PresupuestoAsociado"]);
			}
			if (isset($this->presupuesto["TipoPresupuesto"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["TipoPresupuesto"]);
			}
			if (isset($this->presupuesto["Descripcion"])) {
				$this->comprobar->ComprobarCaracteres($this->presupuesto["Descripcion"]);
			}
			if (isset($this->presupuesto["NroTotalBecarios"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["NroTotalBecarios"]);
			}
			if (isset($this->presupuesto["TiempoBeca"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["TiempoBeca"]);
			}
			if (isset($this->presupuesto["Fecha_Comienzo"])) {
				$this->comprobar->ComprobarFecha($this->presupuesto["Fecha_Comienzo"]);
			}
			if (isset($this->presupuesto["Estado"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["Estado"]);
			}
			if (isset($this->presupuesto["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["Responsable"]);
			}
			if (isset($this->presupuesto["Monto"])) {
				$this->comprobar->ComprobarNumeric($this->presupuesto["Monto"]);
			}
		}

		

		public function get_Presupuesto_max(){
			$sql='SELECT  "TiempoBeca", "Fecha_Comienzo"  FROM Presupuesto where "Fecha_Comienzo"=(SELECT  max("Fecha_Comienzo") as "MaxFecha_Comienzo"  FROM Presupuesto where "TipoPresupuesto"=2) ';
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->presupuesto[] =$filas;
			}
			return $this->presupuesto[0];
		}


		public function get_Presupuesto_imprimir(){
			$sql='SELECT * from Presupuesto where "TipoPresupuesto"=2 order by "Fecha_Comienzo" asc';
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				$filas["Fecha_Comienzo"]=date('Y-m-d', strtotime($filas["Fecha_Comienzo"]));
				$this->presupuesto[] =$filas;
			}
			return $this->presupuesto;
		}

		

		public function get_Presupuesto_max_min(){
			$sql='SELECT  max("Fecha_Comienzo") as "FechaMay", min("Fecha_Comienzo") as "FechaMin"
			FROM Presupuesto where "TipoPresupuesto"=2 ';
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaMay"]=date('Y-m-d', strtotime($filas["FechaMay"]));
				$filas["FechaMin"]=date('Y-m-d', strtotime($filas["FechaMin"]));
				
				$this->feechasencuestas[]=$filas;
			}		

			if($this->feechasencuestas[0]["FechaMay"]>date('Y-m-d')){
				$this->feechasencuestas[0]["FechaMin"]=date('Y-m-d');
			}

			$start_ts = strtotime($this->feechasencuestas[0]["FechaMin"]); 
			$end_ts = strtotime($this->feechasencuestas[0]["FechaMay"]); 
			$diff = $end_ts - $start_ts; 
			return  round($diff / 86400);

			return $this->presupuesto[0];
		}

		

		public function get_Presupuesto_index_1($Responsable)
		{//Consulta del Presupuesto Planificado
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT (SELECT pr."IdPresupuesto" from Presupuesto pr where pr."PresupuestoAsociado"=p."IdPresupuesto") as "PresupuestoAsociado", p."IdPresupuesto", p."Monto", p."NroTotalBecarios", p."TiempoBeca", p."Fecha_Comienzo", p."FechaRegistro" from Presupuesto p where p."TipoPresupuesto"=1 order by p."Fecha_Comienzo" asc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				$filas["Fecha_Comienzo"]=date('d-m-Y', strtotime($filas["Fecha_Comienzo"]));
				$filas["Monto"]=number_format($filas["Monto"], 2, '.', '');
				if ($filas["TiempoBeca"]==1) {
                    $filas["TiempoBeca"]="1 mes";
                }
                else{
                    $filas["TiempoBeca"]=$filas["TiempoBeca"]." Meses";
                }

				$this->presupuesto[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->presupuesto;
		}

		public function get_Presupuesto_index_2($Responsable)
		{//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT p."IdPresupuesto", p."Monto", p."NroTotalBecarios", p."TiempoBeca", p."Fecha_Comienzo", p."FechaRegistro" from Presupuesto p where p."TipoPresupuesto"=2 order by p."IdPresupuesto" asc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				$filas["Fecha_Comienzo"]=date('d-m-Y', strtotime($filas["Fecha_Comienzo"]));
				$filas["Monto"]=number_format($filas["Monto"], 2, '.', '');

				//Fecha creada donde permitira la modificación del presupuesto
				$nuevafecha = strtotime ( '+'.$filas["TiempoBeca"].' month' , strtotime ( $filas["Fecha_Comienzo"] ) ) ;
                $month = date('m', $nuevafecha);
                $year = date('Y', $nuevafecha);
                $day = date("d", mktime(0,0,0, $month+1, 0, $year));
 
               $filas["Fecha_Activa"]= date('Y-m-d', mktime(0,0,0, $month, $day, $year));
               
				if ($filas["TiempoBeca"]==1) {
                    $filas["TiempoBeca"]="1 mes";
                }
                else{
                    $filas["TiempoBeca"]=$filas["TiempoBeca"]." Meses";
                }



				$this->presupuesto[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->presupuesto;
		}
 
		public function get_Presupuesto_create()
		{//Consulta del Depertamentos Index

			$sql='SELECT p."IdPresupuesto", p."Monto", p."NroTotalBecarios", p."TiempoBeca", p."Fecha_Comienzo", p."FechaRegistro" from Presupuesto p where p."TipoPresupuesto"=1 and (SELECT pr."IdPresupuesto" from Presupuesto pr where pr."PresupuestoAsociado"=p."IdPresupuesto") is null';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				$filas["Fecha_Comienzo"]=date('d-m-Y', strtotime($filas["Fecha_Comienzo"]));
				$filas["Monto"]=number_format($filas["Monto"], 2, '.', '');
				if ($filas["TiempoBeca"]==1) {
                    $filas["TiempoBeca"]="1 mes";
                }
                else{
                    $filas["TiempoBeca"]=$filas["TiempoBeca"]." Meses";
                }
                
				$this->presupuesto[] =$filas;
			}

			return $this->presupuesto;
		}

		public function set_Presupuesto_store($presupuesto)
		{
			$this->presupuesto = $presupuesto;
			$this->Comprobacion();

			
			if ($this->get_consulta_Presupuesto_store_fecha()==0) {
			
				if ($this->get_consulta_Presupuesto_store()==0) {
					$sql= 'INSERT INTO Presupuesto("PresupuestoAsociado", "TipoPresupuesto", "Monto", "Descripcion", "NroTotalBecarios", "TiempoBeca", "Fecha_Comienzo", "FechaRegistro", "Estado", "Responsable")
					VALUES (';
					$values=$this->presupuesto["PresupuestoAsociado"].", ".$this->presupuesto["TipoPresupuesto"].", '".$this->presupuesto["Monto"]."', '".$this->presupuesto["Descripcion"]."', ".$this->presupuesto["NroTotalBecarios"].", ".$this->presupuesto["TiempoBeca"].", '".$this->presupuesto["Fecha_Comienzo"]."', now(), 1, ".$this->presupuesto["Responsable"].");";
					$sql=$sql.$values;
					if ($this->db->consultar($sql)) {

						$sql='SELECT max("IdPresupuesto") as "IdPresupuesto" FROM Presupuesto';	
						$consulta = $this->db->consultar($sql);
						$filas=pg_fetch_assoc($consulta);
						$this->OperacionesSistemicas->array_OperacionesSistemicas($this->presupuesto["Responsable"], 2, $filas["IdPresupuesto"], $this->tabla);
						if ($this->presupuesto["TipoPresupuesto"]==2) {
							$this->set_DesactivarPresupuestos($filas["IdPresupuesto"]);						
							$this->set_DesactivarInscripcion();
						}
						return $filas["IdPresupuesto"];
					}
					else{
						$respuesta= "Ha ocurrido un error, consultar con el administrador";
					}

				}
				else{
					$respuesta ="Ya existe un presupuesto con esas características";
				}
			}
			else{
				$fecha= new DateTime($this->presupuesto["Fecha_Comienzo"]);

				$respuesta= "La fecha ".$fecha->format('d-m-Y')."  esta siendo cubierto por otro presupuesto";
			}

			return $respuesta;
		}
		


		
		private function set_DesactivarInscripcion()
		{

			$sql='UPDATE inscripcionesbecarios SET "Estado"=0';		
			$consulta = $this->db->consultar($sql);
		}

		private function set_DesactivarPresupuestos($id)
		{
			$this->comprobar->ComprobarNumeric($id);

			$sql='UPDATE Presupuesto SET "Estado"=0 where "IdPresupuesto"!='.$id;		
			$consulta = $this->db->consultar($sql);
		}


		private function get_consulta_Presupuesto_store()
		{

			$sql='SELECT * FROM Presupuesto where "TipoPresupuesto"='.$this->presupuesto["TipoPresupuesto"].' and "Fecha_Comienzo"='."'".$this->presupuesto["Fecha_Comienzo"]."'".' and "Monto"='."'".$this->presupuesto["Monto"]."'".' and "TiempoBeca"='.$this->presupuesto["TiempoBeca"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		private function get_consulta_Presupuesto_store_fecha()
		{

			$sql='SELECT * FROM Presupuesto where "TipoPresupuesto"='.$this->presupuesto["TipoPresupuesto"].' and "Fecha_Comienzo">='."'".$this->presupuesto["Fecha_Comienzo"]."'";		
			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					
					$nuevafecha = strtotime ( '+'.$filas["TiempoBeca"].' month' , strtotime ( $filas["Fecha_Comienzo"] ) ) ;

					$month = date('m', $nuevafecha);
	      			$year = date('Y', $nuevafecha);
	      			$day = date("d", mktime(0,0,0, $month+1, 0, $year));
	 
	       			$nuevafecha= date('Y-m-d', mktime(0,0,0, $month, $day, $year));
	  

					if ($this->presupuesto["Fecha_Comienzo"]>$nuevafecha) {
						return "0";
						break;
					}
					elseif ($this->presupuesto["Fecha_Comienzo"]<$nuevafecha){
						return "1";
						break;
					}
				}	
			}
			else{
				return pg_num_rows($consulta);
			}

		}

		public function get_Presupuesto_show($id, $Responsable)
		{ 
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT p.* from Presupuesto p  where p."IdPresupuesto"='.$id;		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d F Y g:ia', strtotime($filas["FechaRegistro"]));
				$filas["Fecha_Comienzo"]=date('d-m-Y', strtotime($filas["Fecha_Comienzo"]));
				$filas["Monto"]=number_format($filas["Monto"], 2, '.', '');
				if ($filas["TiempoBeca"]==1) {
                    $filas["TiempoBeca"]="1 mes";
                }
                else{
                    $filas["TiempoBeca"]=$filas["TiempoBeca"]." Meses";
                }


				$this->presupuesto[] =$filas;
			}

			return $this->presupuesto[0];

		}

		public function get_Presupuesto_edit($id)
		{

			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT p.* from Presupuesto p  where p."IdPresupuesto"='.$id;		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->presupuesto[] =$filas;
			}

			return $this->presupuesto[0];

		}

		private function get_consulta_Presupuesto_update()
		{

			$sql='SELECT * FROM Presupuesto where "TipoPresupuesto"='.$this->presupuesto["TipoPresupuesto"].' and "Fecha_Comienzo"='."'".$this->presupuesto["Fecha_Comienzo"]."'".' and "Monto"='."'".$this->presupuesto["Monto"]."'".' and "TiempoBeca"='.$this->presupuesto["TiempoBeca"].' and "IdPresupuesto"!='.$this->presupuesto["IdPresupuesto"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		private function get_consulta_Presupuesto_update_fecha()
		{

			$sql='SELECT * FROM Presupuesto where "TipoPresupuesto"='.$this->presupuesto["TipoPresupuesto"].' and "Fecha_Comienzo">='."'".$this->presupuesto["Fecha_Comienzo"]."'".' and "IdPresupuesto"!='.$this->presupuesto["IdPresupuesto"];		
			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$filas["Fecha_Comienzo"];
					$nuevafecha = strtotime ( '+'.$filas["TiempoBeca"].' month' , strtotime ( $filas["Fecha_Comienzo"] ) ) ;

					$month = date('m', $nuevafecha);
	      			$year = date('Y', $nuevafecha);
	      			$day = date("d", mktime(0,0,0, $month+1, 0, $year));
	 
	       			$nuevafecha= date('Y-m-d', mktime(0,0,0, $month, $day, $year));
	  

					if ($presupuesto["Fecha_Comienzo"]>$nuevafecha) {
						return "0";
						break;
					}
					elseif ($presupuesto["Fecha_Comienzo"]<$nuevafecha){
						return "1";
						break;
					}
				}	
			}
			else{
				return pg_num_rows($consulta);
			}

		}

		public function set_Presupuesto_update($presupuesto)
		{
			$this->presupuesto = $presupuesto;
			$this->Comprobacion();

			if ($this->get_consulta_Presupuesto_update_fecha()==0) {
				if ($this->get_consulta_Presupuesto_update()==0) {
				 	$sql= 'UPDATE Presupuesto SET "PresupuestoAsociado"='.$this->presupuesto["PresupuestoAsociado"].', "TipoPresupuesto"='.$this->presupuesto["TipoPresupuesto"].', "Monto"='."'".$this->presupuesto["Monto"]."'".', "Descripcion"='."'".$this->presupuesto["Descripcion"]."'".', "NroTotalBecarios"='.$this->presupuesto["NroTotalBecarios"].', "TiempoBeca"='.$this->presupuesto["TiempoBeca"].', "Fecha_Comienzo"='."'".$this->presupuesto["Fecha_Comienzo"]."'".', "Responsable"='.$this->presupuesto["Responsable"].' WHERE "IdPresupuesto"='.$this->presupuesto["IdPresupuesto"];
					if ($this->db->consultar($sql)) {
						$this->OperacionesSistemicas->array_OperacionesSistemicas($this->presupuesto["Responsable"], 2, $this->presupuesto["IdPresupuesto"], $this->tabla);

						$this->set_DesactivarPresupuestos($this->presupuesto["IdPresupuesto"]);
						return $this->presupuesto["IdPresupuesto"];
					}
					else{
						$respuesta= "Ha ocurrido un error, consultar con el administrador";
					}

				}
				else{
					$respuesta ="Ya existe otro presupuesto con esas características";
				}
			}
			else{
				$fecha= new DateTime($this->presupuesto["Fecha_Comienzo"]);

				$respuesta= "La fecha ".$fecha->format('d-m-Y')."  esta siendo cubierto por otro presupuesto";
			}

			return $respuesta;
				
				
		}

		public function set_Presupuesto_destroy($id, $Responsable){ //Consulta Registrar Hosarios
			$this->comprobar->ComprobarNumeric($Responsable);
			$this->comprobar->ComprobarNumeric($id);
			$sql= 'DELETE FROM Presupuesto WHERE "IdPresupuesto"='.$id;
				if ($this->db->consultar($sql)) {
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$id,$this->tabla);
					return "1";
			}

		}


		#OTRAS CONSULTAS


		public function get_Presupuesto_Inscripciones()
		{
			$sql='SELECT max("IdPresupuesto") as "maxIdPresupuesto" from presupuesto where "TipoPresupuesto"=2';				
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->sinceracionbecas[] =$filas;
			}

			return $this->sinceracionbecas[0]["maxIdPresupuesto"];

		}
		

		public function get_Presupuesto_Estadisticas()
		{//Consulta del Depertamentos Index
			
			$sql='SELECT p."IdPresupuesto", p."Fecha_Comienzo",  p."TiempoBeca" from Presupuesto p where p."TipoPresupuesto"=2 order by p."IdPresupuesto" desc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$actual = strtotime($filas['Fecha_Comienzo']);
  				$filas['Fecha_Culminacion'] = date("d-m-Y", strtotime("+".$filas['TiempoBeca']." month", $actual));
  				$filas["Fecha_Comienzo"]=date('d-m-Y', strtotime($filas["Fecha_Comienzo"]));


				$this->presupuesto[] =$filas;
			}
			
			return $this->presupuesto;
		}


	}


	?>