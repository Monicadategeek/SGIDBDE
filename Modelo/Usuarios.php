<?php 
	/**
	* Clase Usuarios
	* Comparte la tabla Usuarios en la Base de Datos
	* Es llamado en los Controladores UsuariosController, HorariosController, TrabajadoresController, CitasController, GestionCitasController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador 
	*
	* ejem: get_Usuarios_user_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class Usuarios 

	{

		private $db;
		private $usuarios;
		private $tipoUsuario;
		private $tabla;
		private $comprobar;

		/**
		* Funcion de inicio donde llama los archivos; 
		* @global [array] departamentos
		* @global [object] db
		* @global [object] comprobar
		* @global [object] OperacionesSistemicas
		* @global [integer] tipoUsuario
		* @global [integer] tabla
		* 
		*/
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("../Modelo/OperacionesSistemicas.php");
			require_once("SeguridadDatos.php");

			$this->comprobar = new SeguridadDatos;
			$this->db = new conexion;
			$this->usuarios =array();
			$this->OperacionesSistemicas = new OperacionesSistemicas();	
			$this->tipoUsuario=264;	//264 Usuario normal 265 SuperAdministrador
			$this->tabla=2;//LISTO	
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] usuarios
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->usuarios["id"])) {
				$this->comprobar->ComprobarNumeric($this->usuarios["id"]);
			}
			if (isset($this->usuarios["cedula"])) {
				$this->comprobar->ComprobarNumeric($this->usuarios["cedula"]);
			}
			if (isset($this->usuarios["nombre"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["nombre"]);
			}
			if (isset($this->usuarios["nombre2"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["nombre2"]);
			}
			if (isset($this->usuarios["apellido"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["apellido"]);
			}
			if (isset($this->usuarios["apellido2"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["apellido2"]);
			}
			if (isset($this->usuarios["correo"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["correo"]);
			}
			if (isset($this->usuarios["telefono"])) {
				$this->comprobar->ComprobarNumeric($this->usuarios["telefono"]);
			}
			if (isset($this->usuarios["celular"])) {
				$this->comprobar->ComprobarNumeric($this->usuarios["celular"]);
			}
			if (isset($this->usuarios["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->usuarios["Responsable"]);
			}
			if (isset($this->usuarios["Foto_Usuario"])) {
				$this->comprobar->ComprobarCaracteres($this->usuarios["Foto_Usuario"]);
			}
		}

		
		/**
		* Consultas del submenu usuarios
		*/
	
		/**
		* Consulto los datos de los usuariarios que estan asociados a el sistema SGIBDE ya que se identifican por dos tipos de usuarios 264 y 265
		* @method ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [usuarios]
		*/
	
		public function get_Usuarios_user_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT "cedula", concat("nombre", '."' '".', "nombre2") as "Nombres", concat("apellido", '."' '".', "apellido2") as "Apellidos", "Num_Usuario", "Id_Tipo_Usuario", "correo", "Cod_Estatus", "Foto_Usuario"   from usuarios  where "Id_Tipo_Usuario" in ('.$this->tipoUsuario.',265)';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["Cod_Estatus"]==0) {
                    $filas["Cod_Estatus"]="Activo";
                }
                else{
                    $filas["Cod_Estatus"]="Inactivo";
                }

                if ($filas["Id_Tipo_Usuario"]==265) {
                    $filas["TipoUsuario"]="Administrador";
                }
                else{
                    $filas["TipoUsuario"]="Usuario";
                }


                if ($filas["Id_Tipo_Usuario"]==265) {
                    $filas["boton"]='<i class="fa fa-lock" aria-hidden="true"></i>';
                }
                else{
                    $filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('."'../Controller/UsuariosController.php',".$filas["Num_Usuario"].');"><i class="fa fa-pencil"></i></button>';
                }
                
				
				$this->usuarios[] =$filas;
			}
			
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->usuarios;

		}


		/**
		* Consulto los datos de la persona 
		* @method ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [usuarios]
		*/

		public function get_Usuarios_MyUser_show($id){
			$this->comprobar->ComprobarNumeric($id);
			 $sql='SELECT "cedula", "sexo", concat("nombre", '."' '".', "nombre2") as "Nombres", concat("apellido", '."' '".', "apellido2") as "Apellidos", "Num_Usuario", "correo", "telefono", "celular", "Cod_Estatus", "Foto_Usuario"  from usuarios u where "Num_Usuario"='.$id;		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Nombres"]=ucwords(strtolower($filas["Nombres"]));
				$filas["Apellidos"]=ucwords(strtolower($filas["Apellidos"]));
				if ($filas["Foto_Usuario"]!='' || $filas["Foto_Usuario"]!=0) {
					$filas["Foto_Usuario"]='<img class="img-responsive avatar-view" src="'."../web/images/fotos/".$filas["Foto_Usuario"].'" alt="'.$filas["Nombres"]." ".$filas["Apellidos"].'" title="'.$filas["Nombres"]." ".$filas["Apellidos"].'" style="width: 199px; height: 191px;">';
				}
				else{
					$filas["Foto_Usuario"]='<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El usuario no tiene foto Registrada">';
				}
				
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
                if ($filas["Cod_Estatus"]==0) {
                    $filas["Cod_Estatus"]="Activo";
                }
                else{
                    $filas["Cod_Estatus"]="Inactivo";
                }

				$this->usuarios[] =$filas;
			}
			
			return $this->usuarios[0];
		}



		/**
		* Consultas del submenu personas
		*/
	
		/**
		* Consulto los datos de las personas para mostrar a los usuarios registrados en el sistema, pero que estan asociados a el sistema SGIBDE ya que se identifican por dos tipos de usuarios 264 y 265
		* @method ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [usuarios]
		*/

		public function get_Usuarios_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);
			 $sql='SELECT "cedula", "sexo", concat("nombre", '."' '".', "nombre2") as "Nombres", concat("apellido", '."' '".', "apellido2") as "Apellidos", "Num_Usuario", "correo", "telefono", "celular", "Cod_Estatus", "Foto_Usuario"  from usuarios u where "Id_Tipo_Usuario" in (264, 265)';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["Foto_Usuario"]!=0 || $filas["Foto_Usuario"]!='') {
					$filas["Foto_Usuario"]='<img class="img-responsive avatar-view" src="'."../web/images/fotos/".$filas["Foto_Usuario"].'" alt="'.$filas["Nombres"]." ".$filas["Apellidos"].'" title="'.$filas["Nombres"]." ".$filas["Apellidos"].'" style="width: 132px; height: 133px;">';
				}
				
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
                if ($filas["Cod_Estatus"]==0) {
                    $filas["Cod_Estatus"]="Activo";
                }
                else{
                    $filas["Cod_Estatus"]="Inactivo";
                }

				$this->usuarios[] =$filas;
			}

			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method array_OperacionesSistemicas()
			*/	
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->usuarios;
		}


		/**
		* Inserto una nueva persona en el sistema 
		* @param [array] $usuarios
		* @return [integer] muestre 1 si es true || [string] El error  
		* @method Comprobacion(), get_Usuarios_store_cedula(), get_Usuarios_store_correo_est(),  array_OperacionesSistemicas() 
		*/ 
		public function set_Usuarios_store($usuarios){ 
			$this->usuarios = $usuarios;
			$this->Comprobacion();

			if (($this->get_Usuarios_store_cedula())==0) {
				if ($this->get_Usuarios_store_correo()==0) {
					if ($this->get_Usuarios_store_correo_est()==0) {
						$sql= 'INSERT INTO usuarios("Usuario_tx", "nombre", "nombre2", "apellido", "apellido2", "sexo", "cedula", "correo", "telefono", "celular", "clave", "Pre_Sec", "Nes_Sec", "Id_Tipo_Usuario", "Lus_Trabajo", "Ultimo_Año", "Ultimo_Mes", "Ultimo_Dia", "Ultimo_Hora", "Cod_Estatus", "Foto_Usuario", "Bloqueo_Foto_Usu") VALUES (';
						$values="'".$this->usuarios["cedula"]."', '".$this->usuarios["nombre"]."', '".$this->usuarios["nombre2"]."', '".$this->usuarios["apellido"]."', '".$this->usuarios["apellido2"]."', ".$this->usuarios["sexo"].", ".$this->usuarios["cedula"].", '".$this->usuarios["correo"]."', ".$this->usuarios["telefono"].", ".$this->usuarios["celular"].", '".md5($this->usuarios["cedula"])."', null, null, ".$this->tipoUsuario.", 1, null, null, null, null, 0, '".$this->usuarios["Foto_Usuario"]."', null);";
						$sql=$sql.$values;

						if ($this->db->consultar($sql)) {

							$sql='SELECT "Num_Usuario", "nombre", "apellido" FROM Usuarios where "Num_Usuario"=(SELECT max("Num_Usuario") AS "Num_Usuario" FROM Usuarios)';	

							$consulta = $this->db->consultar($sql);
							$filas=pg_fetch_assoc($consulta);
							/**
							* Llamada a la función para registrar quien realizó la consulta
							* @method array_OperacionesSistemicas()
							*/
							$this->OperacionesSistemicas->array_OperacionesSistemicas($this->usuarios["Responsable"],1,$filas["Num_Usuario"], $this->tabla);

							return "1";
						}
						
						return "Ha ocurrido un error al interactuar con la base de datos";
				

					}
					else{
						return "Ya existe un estudiante con esa dirección de correo electrónico";
					}
				}
				else{
					return "Ya existe una persona con esa dirección de correo electrónico";
				}
				
			}
			else{
				return "Ya existe una persona con ese número de cédula";
			}
		}


		/**
		* Consulto si el correo ya se encuentra esta asociado a una persona 
		* @param [array] [usuarios]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Usuarios_store_correo(){

			$sql='SELECT * FROM Usuarios where "correo"='."'".$this->usuarios["correo"]."'";		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Consulto si el correo ya se encuentra esta asociado a un estudiante
		* @param [array] [usuarios]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Usuarios_store_correo_est(){
			$sql='SELECT * FROM estudiantes e,  usu_estudiantes ue where  ue."num_est"=e."num_est" and e."correo"='."'".$this->usuarios["correo"]."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}
		
		/**
		* Consulto si la cédula ya se encuentra registrada 
		* @param [array] [usuarios]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Usuarios_store_cedula(){

			$sql='SELECT * FROM Usuarios where "cedula"='.$this->usuarios["cedula"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Consulto el usuario a editar		
		* @param [integer] $Num_Usuario
		* @return [array] [usuarios]   
		*/
		public function get_Usuarios_edit($Num_Usuario){
			//get_consulta_Usuarios_edit
			$this->comprobar->ComprobarNumeric($Num_Usuario);
			$sql='SELECT * FROM Usuarios where "Num_Usuario"='.$Num_Usuario;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["nombres"]=ucwords(strtolower($filas["nombre"]))." ".ucwords(strtolower($filas["nombre2"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellido"]))." ".ucwords(strtolower($filas["apellido2"]));

				if ($filas["Foto_Usuario"]!='' || $filas["Foto_Usuario"]!=0) {
					$filas["Foto_Usuario"]='<img class="img-responsive avatar-view" src="'."../web/images/fotos/".$filas["Foto_Usuario"].'" alt="'.$filas["nombres"]." ".$filas["apellidos"].'" title="'.$filas["nombres"]." ".$filas["apellidos"].'" style="width: 199px; height: 191px;">';
				}
				else{
					$filas["Foto_Usuario"]='<img class="img-responsive avatar-view" src="../web/images/user.png" alt="" title="El usuario no tiene foto Registrada">';
				}
				
				if ($filas["sexo"]==2) {
                    $filas["descrip_sexo"]="Masculino";
                }
                else{
                    $filas["descrip_sexo"]="Femenino";
                }


				$this->usuarios[] =$filas;
			}
			return $this->usuarios[0];

		}

		/**
		* Consulto un departamento
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $IdDepartamentos, [integer] $Responsable
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Usuarios_show($Num_Usuario, $Responsable){

			$this->comprobar->ComprobarNumeric($Num_Usuario);
			$sql='SELECT * FROM Usuarios where "Num_Usuario"='.$Num_Usuario;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				$filas["nombre"]=ucwords(strtolower($filas["nombre"]));
				$filas["nombre2"]=ucwords(strtolower($filas["nombre2"]));
				$filas["apellido"]=ucwords(strtolower($filas["apellido"]));
				$filas["apellido2"]=ucwords(strtolower($filas["apellido2"]));
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
                if ($filas["Cod_Estatus"]==0) {
                    $filas["Cod_Estatus"]="Activo";
                }
                else{
                    $filas["Cod_Estatus"]="Inactivo";
                }

				$this->usuarios[] =$filas;
			}
			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method consultar(), array_OperacionesSistemicas()
			*/	
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$Num_Usuario,$this->tabla);
			return $this->usuarios[0];

		}

		/**
		* Consulto si el correo ya se encuentra esta asociado a otra persona distinta a la actual 
		* @param [array] [usuarios]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Usuarios_update_correo(){

			$sql='SELECT * FROM Usuarios where "correo"='."'".$this->usuarios["correo"]."'".' and "Num_Usuario"!='.$this->usuarios["Num_Usuario"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Consulto si la cedula se encuentra asignada a otra persona	
		* @param [array] [usuarios]
		* @return [integer] muestra el nro de registros   
		*/ 
		private function get_consulta_Usuarios_update(){

			$sql='SELECT * FROM Usuarios where "cedula"='.$this->usuarios["cedula"].' and "Num_Usuario"!='.$this->usuarios["Num_Usuario"];	
				
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		
		/**
		* Modifico la persona en el sistema 
		* @method Comprobacion(), get_consulta_Usuarios_update(), get_Usuarios_update_correo(), get_Usuarios_store_correo_est()
		* @param [array] $departamentos
		* @return [integer] muestre 1 si es true || [string] El error
		*    
		*/	
		public function set_Usuarios_update($usuarios){
			$this->usuarios = $usuarios;
			$this->Comprobacion();

			if (($this->get_consulta_Usuarios_update())==0) {
				if ($this->get_Usuarios_update_correo()==0) {
					if ($this->get_Usuarios_store_correo_est()==0) {
						$sql= 'UPDATE Usuarios SET "cedula"='.$this->usuarios["cedula"].', "nombre"='."'".$this->usuarios["nombre"]."'".', "nombre2"='."'".$this->usuarios["nombre2"]."'".', "apellido"='."'".$this->usuarios["apellido"]."'".', "apellido2"='."'".$this->usuarios["apellido2"]."'".', "sexo"='.$this->usuarios["sexo"].', "correo"='."'".$this->usuarios["correo"]."'".', "telefono"='.$this->usuarios["telefono"].', "celular"='.$this->usuarios["celular"].', "Foto_Usuario"='."'".$this->usuarios["Foto_Usuario"]."'".' WHERE  "Num_Usuario"='.$this->usuarios["Num_Usuario"];

						if ($this->db->consultar($sql)) {
							/**
							* Llamada a la función para registrar quien realizó la consulta
							* @method array_OperacionesSistemicas()
							*/	
							$this->OperacionesSistemicas->array_OperacionesSistemicas($this->usuarios["Responsable"],2,$this->usuarios["Num_Usuario"], $this->tabla);
							return "1";
						}
						return "Ha ocurrido un error al interactuar con la base de datos";
					}
					else{
						return "Ya existe un estudiante con esa dirección de correo electrónico";
					}
				}
				else{
					return "Ya existe otra persona con esa dirección de correo electrónico";
				}				
			}
			else{
				return "Ya existe otra persona con ese número de cédula";
			}		

		}

		/**
		* Modifico la persona en el sistema 
		* @method Comprobacion(), get_consulta_Usuarios_update(), get_Usuarios_update_correo(), get_Usuarios_store_correo_est()
		* @param [array] $departamentos
		* @return [integer] muestre 1 si es true || [string] El error
		*    
		*/	
		public function set_Usuarios_User_update($usuarios){
			$this->usuarios = $usuarios;
			$this->Comprobacion();

			
			$sql= 'UPDATE usuarios SET "Id_Tipo_Usuario"='.$this->usuarios["Id_Tipo_Usuario"].', "Cod_Estatus"='.$this->usuarios["Cod_Estatus"].' WHERE "Num_Usuario"='.$this->usuarios["Num_Usuario"];

			if ($this->db->consultar($sql)) {
				/**
				* Llamada a la función para registrar quien realizó la consulta
				* @method array_OperacionesSistemicas()
				*/	
				$this->OperacionesSistemicas->array_OperacionesSistemicas($this->usuarios["Responsable"],2,$this->usuarios["Num_Usuario"], $this->tabla);
				return "1";
			}
			return "Ha ocurrido un error al interactuar con la base de datos";
					

		}

		/**
		*										OTRAS CONSULTAS
		* Son consultas llamadas desde otros controladores o clases pero que estan asociadas con la tabla Usuarios, se agregan en el modelo que posea mas peso para la consulta
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion y el nombre de la funcion del controlador: get_Clase_NombredelControlador_funciondelcontrolador
		*/	
		
		/**
		* Consulto los datos de las personas para mostrar en el formulario de trabajadores create, pero que estan asociados a el sistema SGIBDE ya que se identifican por dos tipos de usuarios 264 y 265
		* @return [array] [usuarios]   
		*/
		public function get_Usuarios_Trabajadores_create(){

			 $sql='SELECT "cedula", "sexo", "Id_Tipo_Usuario", concat("nombre", '."' '".', "nombre2") as "Nombres", concat("apellido", '."' '".', "apellido2") as "Apellidos", "Num_Usuario", "correo", "Cod_Estatus"  from usuarios u where "Id_Tipo_Usuario" in (264, 265)';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
				$this->usuarios[] =$filas;
			}
			return $this->usuarios;
		}
		

		/**
		* Consulto los datos de la persona para mostrar en horarios
		* @param [integer] $IdTrabajador
		* @return [array] [usuarios]   
		*/
		public function get_Usuarios_Horarios_create($IdTrabajador){
			$this->comprobar->ComprobarNumeric($IdTrabajador);
			$sql='SELECT concat(usu."nombre", '."' '".', usu."nombre2") as Nombres, concat(usu."apellido", '."' '".', usu."apellido2") as Apellidos, usu."cedula", usu."Num_Usuario"   FROM Trabajadores t, Usuarios usu where  t."Persona"=usu."Num_Usuario" and "IdTrabajador"='.$IdTrabajador;
			
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->usuarios[] =$filas;
			}
			return $this->usuarios[0];

		}

		/**
		* Consulto los datos del estudiante para mostrar en citas 
		* @param [integer] $id
		* @return [array] [usuarios]   
		*/
		public function get_Usuarios_Citas_Solicitud($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT nombres as "Nombres", apellidos as "Apellidos", e."cedula", e."num_est", e."correo" FROM usu_estudiantes usu, estudiantes e where  usu."num_est"=e."num_est" and usu."num_usu_e"='.$id;
			
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->usuarios[] =$filas;
			}
			return $this->usuarios[0];

		}


		/*
			Es igual a get_Usuarios_Citas_Solicitud
		 public function get_Usuarios_num_est($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT nombres as "Nombres", apellidos as "Apellidos", e."cedula", e."num_est", e."correo" FROM usu_estudiantes usu, estudiantes e where  usu."num_est"=e."num_est" and usu."num_est"='.$id;
			
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->usuarios[] =$filas;
			}
			return $this->usuarios[0];

		}*/

		

	}
 
?>
