<?php 

	

	/**
	* 
	*/
	class EncuestOdontPregu 

	{
		private $db;
		private $encuesta;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->encuesta= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=6;
			
		}




		public function get_EncuestOdontPregu_index($Responsable){
			$sql="";
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->encuesta[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->encuesta;

		}

		private function  get_EncuestOdontPregu_store($encuesta){
			$sql='SELECT * FROM encuest_odont_pregu where "idencuesta"='.$encuesta[0]["idencuesta"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		public function set_EncuestOdontPregu_store($encuesta){ //Consulta Registrar Depertamentos
			
			if ($this->get_EncuestOdontPregu_store($encuesta)>0) {
				return "1";
			}

			$sql='INSERT INTO encuest_odont_pregu(idencuesta, idpregunta, respuesta, nro) VALUES ';
			$values='';
			foreach ($encuesta as $desglosada) {
				$values=$values.'('.$desglosada["idencuesta"].', '.$desglosada["idpregunta"].', '.$desglosada["respuesta"].', '.$desglosada["nro"].'),';
			}

			$values = substr($values, 0, -1).";";
			$sql=$sql.$values;



			if ($this->db->consultar($sql)) {
				return "1";
			}
			else{
				return "2";
			}

			
		}


		public function set_EncuestOdontPregu_show($id){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT po.*, (SELECT  eop."respuesta" FROM encuest_odont_pregu eop WHERE po."id" = eop."idpregunta" And  eop."idencuesta"='.$id.') as "respuesta", (SELECT  eop."nro" FROM encuest_odont_pregu eop WHERE po."id" = eop."idpregunta" And  eop."idencuesta"='.$id.') as "nro" FROM preguntas_odontologicas po order by po."id"';

			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {

				if ($filas["respuesta"]==0) {
					$filas["respuesta"]='No';
				}
				else{
					$filas["respuesta"]='Si';
				}
				$this->encuesta[] =$filas;
			}

			return $this->encuesta;

			
		}

		

		

		



	}
?>