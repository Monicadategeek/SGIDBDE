<?php 

	

	/**
	* 
	*/
	class EncuestaOdontologica 

	{
		private $db;
		private $encuesta;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->encuesta= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=15;//Listo
			
		}


			
		private function Comprobacion()
		{
			if (isset($this->encuesta["id"])) {
				$this->comprobar->ComprobarNumeric($this->encuesta["id"]);
			}
			if (isset($this->encuesta["SangEnc"])) {
				$this->comprobar->ComprobarNumeric($this->encuesta["SangEnc"]);
			}
			if (isset($this->encuesta["RestProt"])) {
				$this->comprobar->ComprobarNumeric($this->encuesta["RestProt"]);
			}
			if (isset($this->encuesta["Estado"])) {
				$this->comprobar->ComprobarNumeric($this->encuesta["Estado"]);
			}

			if (isset($this->variable["AntFami"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["AntFami"]);
			}
			if (isset($this->variable["MotCons"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["MotCons"]);
			}
			if (isset($this->variable["Dolor"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["Dolor"]);
			}
			if (isset($this->variable["Control"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["Control"]);
			}
			if (isset($this->variable["Otro"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["Otro"]);
			}
			if (isset($this->variable["EnferAct"])) {
				$this->comprobar->ComprobarCaracteres($this->variable["EnferAct"]);
			}
			
		}

			
	


		public function set_EncuestaOdontologica_store($encuesta){ //Consulta Registrar Depertamentos
			$this->encuesta = $encuesta;
			$this->Comprobacion();

			$sql='INSERT INTO encuesta_odontologica(id, "Estado") VALUES ( '.$this->encuesta["id"].', 0)';
			if ($this->db->consultar($sql)) {

				return "1";
			}
			else{
				return "2";
			}

			
		}

		public function set_EncuestaOdontologica_update($encuesta){ //Consulta Registrar Depertamentos
			$this->encuesta = $encuesta;
			$this->Comprobacion();

			$sql='UPDATE encuesta_odontologica
   				SET "AntFami"='."'".$this->encuesta["AntFami"]."'".', "MotCons"='."'".$this->encuesta["MotCons"]."'".', "Dolor"='."'".$this->encuesta["Dolor"]."'".', "Control"='."'".$this->encuesta["Control"]."'".', "SangEnc"='."'".$this->encuesta["SangEnc"]."'".', "RestProt"='."'".$this->encuesta["RestProt"]."'".', "Otro"='."'".$this->encuesta["Otro"]."'".', "EnferAct"='."'".$this->encuesta["EnferAct"]."'".', "Estado"=1
				WHERE id='.$this->encuesta["id"];
			if ($this->db->consultar($sql)) {

				return "1";
			}
			else{
				return "2";
			}

			
		}

		public function set_EncuestaOdontologica_show($id, $Responsable){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT "AntFami", "MotCons", "Dolor", "Control", "SangEnc", "RestProt", "Otro", "EnferAct" FROM encuesta_odontologica where id='.$id;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				if (empty($filas["AntFami"])) {
					$filas["AntFami"]="-";
				}

				if (empty($filas["MotCons"])) {
					$filas["MotCons"]="-";
				}

				if (empty($filas["Dolor"])) {
					$filas["Dolor"]="-";
				}

				if (empty($filas["Control"])) {
					$filas["Control"]="-";
				}

				if (empty($filas["SangEnc"])) {
					$filas["SangEnc"]="-";
				}

				if ($filas["RestProt"]==0) {
					$filas["RestProt"]="No";
				}
				else{
					$filas["RestProt"]="Si";
				}

				if (empty($filas["Otro"])) {
					$filas["Otro"]="-";
				}

				if (empty($filas["EnferAct"])) {
					$filas["EnferAct"]="-";
				}

				$this->encuesta[] =$filas;
			}

			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$id,$this->tabla);

			return $this->encuesta[0];

			
		}

		



		

		
	
	

		

		

		



	}
?>