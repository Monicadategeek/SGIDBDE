<?php 

	

	/**
	* Clase AyudaEconomica
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class AyudaEconomica 

	{
		private $db;
		private $ayudaseconomica;
		private $tabla;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] ayudaseconomica
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->ayudaseconomica= array();		
			
		}

		/**
		* Consulto los datos basicos de las ayudaseconomica registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [ayudaseconomica]
		*/
		public function get_ayudaseconomica(){
			$sql='SELECT a."id_ayud_econ", a."desc_ayud_econ", (SELECT count(*) FROM ayuda_econ_x_estudiante aee where aee."id_ayud_econ"= a."id_ayud_econ") as "total" from ayuda_economica a';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["id"]=$filas["id_ayud_econ"];
				$filas["descripcion"]=$filas["desc_ayud_econ"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_ayud_econ"].",2'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_ayud_econ"].",2',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}

				
				$this->ayudaseconomica[] =$filas;
			}
			return $this->ayudaseconomica;
		}

		/**
		* Inserto una nueva ayudaseconomica en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_ayudaseconomica_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_ayudaseconomica_store($dato)==0) {
				$sql= 'INSERT INTO ayuda_economica("desc_ayud_econ") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}

		/**
		* Consulto si existe una ayudaseconomica con las caracteristicas a insertar 
		* @param [array] [ayudaseconomica]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ayudaseconomica_store($dato){
			$sql='SELECT * from ayuda_economica where "desc_ayud_econ"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

		/**
		* Consulto la ayudaseconomica a editar		
		* @param [integer] $id
		* @return [array] [ayudaseconomica]   
		*/
		public function get_ayudaseconomica_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_ayud_econ", "id_ayud_econ" from ayuda_economica where "id_ayud_econ"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_ayud_econ"]=strtoupper($filas["desc_ayud_econ"]);
				$filas["id"]=$filas["id_ayud_econ"];
				$filas["descripcion"]=$filas["desc_ayud_econ"];				
				$this->deportes[] =$filas;
			}
			return $this->deportes[0];
		}

		/**
		* Modifico la ayudaseconomica en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_ayudaseconomica_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_ayudaseconomica_store($dato["dato"])==0) {
				$sql= 'UPDATE ayuda_economica SET "desc_ayud_econ"='."'".$dato["dato"]."'".' WHERE "id_ayud_econ"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino la ayudaseconomica a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_ayudaseconomica_destroy($id){
			if ($this->get_ayudaseconomica_destroy($id)>0) {
				$sql= 'DELETE FROM ayuda_economica WHERE "id_ayud_econ"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe la ayudaseconomica en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ayudaseconomica_destroy($id){
			$sql='SELECT * from ayuda_economica where "id_ayud_econ"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>