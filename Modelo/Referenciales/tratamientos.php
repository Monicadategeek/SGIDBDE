<?php 

	
	/**
	* Clase tratamientos
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class tratamientos 

	{
		private $db;
		private $tratamientos;
		private $tabla;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tratamientos
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tratamientos= array();			
			$this->tabla=1;
			
		}

		private function Comprobacion()
		{
			if (isset($this->tratamientos["idTratamiento"])) {
				$this->comprobar->ComprobarNumeric($this->tratamientos["idTratamiento"]);
			}
			if (isset($this->tratamientos["descripcion"])) {
				$this->comprobar->ComprobarCaracteres($this->tratamientos["descripcion"]);
			}
		}

		/**
		* Consulto los datos basicos de los tratamientos. para los select o muestra en Tablas Referenciales
		* @return [array] [tratamientos]
		*/
		public function get_tratamientos(){
			$sql='SELECT "descripcion", "idTratamiento"  from tratamientos  order by "descripcion"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["descripcion"]=strtoupper($filas["descripcion"]);
				$filas["id"]=$filas["idTratamiento"];
				$filas["descripcion"]=$filas["descripcion"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["idTratamiento"].",18'".');"><i class="fa fa-pencil"></i></button>';
				$this->tratamientos[] =$filas;
			}
			return $this->tratamientos;
		}

		/**
		* Consulto los tratamientos registradas especificos.
		* @return [array] [enfermedades]
		*/
		public function get_tratamientos_especificas($especifico){
			$this->comprobar->ComprobarCaracteres($especifico);
			$sql='SELECT "descripcion", "idTratamiento"  from tratamientos t where "idTratamiento" in ('.$especifico.')';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tratamientos[] =$filas;
			}
			return $this->tratamientos;
		}

		/**
		* Inserto un nuevo tratamiento en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_tratamientos_store($descripcion){
			
			$this->comprobar->ComprobarCaracteres($descripcion);
					
			$valor=$this->get_tratamientos_store($descripcion);
			if ($valor==0) {
				$sql='INSERT INTO tratamientos("descripcion")  VALUES ('."'".$descripcion."'".');';

				if ($this->db->consultar($sql)) {	
					$sql='SELECT max("idTratamiento") as "idTratamiento" FROM tratamientos limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return $filas["idTratamiento"];

				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			return $valor;


		}

		/**
		* Consulto si existe una tratamiento con las caracteristicas a insertar 
		* @param [string] $descripcion
		* @return [integer] muestra el nro de registros   
		*/
		public function get_tratamientos_store($descripcion){
			$sql='SELECT "idTratamiento"  from tratamientos where "descripcion" = '."'".$descripcion."' limit 1";		

			$consulta = $this->db->consultar($sql);

			if (pg_num_rows($consulta)==0) {
				return 0;
			}
			$filas=pg_fetch_assoc($consulta);
			return $filas["idTratamiento"];
		}

		/**
		* Inserto una nueva enfermedad en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_tratamientos_store_tb($descripcion){
			
			$this->comprobar->ComprobarCaracteres($descripcion);
					
			$valor=$this->get_tratamientos_store($descripcion);
			if ($valor==0) {
				$sql='INSERT INTO tratamientos("descripcion")  VALUES ('."'".$descripcion."'".');';

				if ($this->db->consultar($sql)) {	
					$sql='SELECT max("idTratamiento") as "idTratamiento" FROM tratamientos limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return 1;

				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			else{
				die("Ya existe '".$descripcion."' en la tabla referencial");
			}


		}

		/**
		* Consulto el tratamientos a editar		
		* @param [integer] $id
		* @return [array] [tratamientos]   
		*/
		public function get_tratamientos_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "descripcion", "idTratamiento" from tratamientos where "idTratamiento"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["descripcion"]=strtoupper($filas["descripcion"]);
				$filas["id"]=$filas["idTratamiento"];
				$filas["descripcion"]=$filas["descripcion"];				
				$this->tratamientos[] =$filas;
			}
			return $this->tratamientos[0];
		}

		/**
		* Modifico el tratamientos en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_tratamientos_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_tratamientos_store($dato["dato"])==0) {
				$sql= 'UPDATE tratamientos SET "descripcion"='."'".$dato["dato"]."'".' WHERE "idTratamiento"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

	}
?>