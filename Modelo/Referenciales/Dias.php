<?php 
	/**
	* 
	*/
	class Dias 

	{
		private $db;
		private $dias;
		
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			$this->db=new conexion;
			$this->dias =array();
			
		}

		public function get_Dias(){
			$sql='SELECT * FROM Dias';			
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->dias[]=$filas;
			}
			
			
			return $this->dias;

		}
		
		public function get_Dias_Citas_Solicitud($depto){

		 $sql='SELECT h."Dia" FROM Trabajadores t, Horarios h where t."Depto"='.$depto.' and t."IdTrabajador"=h."IdEmpleado" and t."Estatus"=1 and t."TipoPersonal" in (1,3,6) group by h."Dia"';
		$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->dias[] =$filas;
			}
			return $this->dias;

		}

	}