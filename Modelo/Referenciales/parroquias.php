<?php 

	

	/**
	* 
	*/
	class Parroquias 

	{
		private $db;
		private $parroquias;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->parroquias= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=1;
			
		}


		public function get_parroquias($municipio){
			$this->comprobar->ComprobarNumeric($municipio);
			$sql='SELECT p."parroquia", p."id_parroquia" from parroquias p where p."id_municipio"='.$municipio;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->parroquias[] =$filas;
			}
			return $this->parroquias;
		}

	}
?>