<?php
	/**
	* Tabla Referencial
	*/
	class Sedes
	{
		
		private $db;
		private $sedes;
		
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			$this->db = new conexion;
			$this->sedes =array();
			
		}

		public function get_Sedes(){
			$sql='SELECT * FROM Sede';		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->sedes[] =$filas;
			}			
			return $this->sedes;
		}

		public function get_Sedes_Citas_Solicitud($depto){
			$sql='SELECT se."Descripcion", d."IdDepartamentos" FROM Departamentos d, Sede se where se."IdSede"=d."Sede" and d."AsistenciEstud"=1 and d."Estatus"=1 and d."TipoDepto"='.$depto;	

			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->sedes[] =$filas;
			}			
			return $this->sedes;
		}

		
	}
?>