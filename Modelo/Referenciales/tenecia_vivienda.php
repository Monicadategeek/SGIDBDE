<?php 

	/**
	* Clase TeneciaVivienda
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class TeneciaVivienda 

	{
		private $db;
		private $tenecviv;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tenecviv
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->comprobar = new SeguridadDatos;	
			$this->db = new conexion;
			$this->tenecviv= array();			
			
		}

		/**
		* Consulto los tipos de tenencia de la vivienda registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [tenecviv]
		*/
		public function get_TeneciaVivienda(){
			$sql='SELECT tv."desc_ten_viv", tv."id_ten_viv", (SELECT count(*) FROM dat_fisic_ambient_est dfae where dfae."id_ten_viv"= tv."id_ten_viv") as "total" from tenecia_vivienda tv';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_ten_viv"]=strtoupper($filas["desc_ten_viv"]);
				$filas["id"]=$filas["id_ten_viv"];
				$filas["descripcion"]=$filas["desc_ten_viv"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_ten_viv"].",12'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_ten_viv"].",12',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->tenecviv[] =$filas;
			}
			return $this->tenecviv;
		}

		/**
		* Inserto un nuevo tenecia_vivienda en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_TeneciaVivienda_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_TeneciaVivienda_store($dato)==0) {
				$sql= 'INSERT INTO tenecia_vivienda("desc_ten_viv") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un tenecia_vivienda con las caracteristicas a insertar 
		* @param [array] [tenecviv]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TeneciaVivienda_store($dato){
			$sql='SELECT * from tenecia_vivienda where "desc_ten_viv"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el tenecia_vivienda a editar		
		* @param [integer] $id
		* @return [array] [tenecviv]   
		*/
		public function get_TeneciaVivienda_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_ten_viv", "id_ten_viv" from tenecia_vivienda where "id_ten_viv"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_ten_viv"]=strtoupper($filas["desc_ten_viv"]);
				$filas["id"]=$filas["id_ten_viv"];
				$filas["descripcion"]=$filas["desc_ten_viv"];				
				$this->tenecviv[] =$filas;
			}
			return $this->tenecviv[0];
		}

		/**
		* Modifico el tenecia_vivienda en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_TeneciaVivienda_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_TeneciaVivienda_store($dato["dato"])==0) {
				$sql= 'UPDATE tenecia_vivienda SET "desc_ten_viv"='."'".$dato["dato"]."'".' WHERE "id_ten_viv"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el tenecia_vivienda a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_TeneciaVivienda_destroy($id){
			if ($this->get_TeneciaVivienda_destroy($id)>0) {
				$sql= 'DELETE FROM tenecia_vivienda WHERE "id_ten_viv"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el tenecia_vivienda en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TeneciaVivienda_destroy($id){
			$sql='SELECT "desc_ten_viv", "id_ten_viv" from tenecia_vivienda where "id_ten_viv"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>