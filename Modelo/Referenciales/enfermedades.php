<?php 

	

	/**
	* Clase Enfermedades
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class Enfermedades 

	{
		private $db;
		private $enfermedades;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] enfermedades
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->enfermedades= array();	
			
		}


		/**
		* Consulto los datos basicos de las enfermedades registradas. para los select o muestra en Tablas Referenciales
		* @return [array] [enfermedades]
		*/
		public function get_enfermedades(){
			$sql='SELECT "descripcion", "idEnfermedad"  from enfermedades order by "descripcion" ';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["descripcion"]=strtoupper($filas["descripcion"]);
				$filas["id"]=$filas["idEnfermedad"];
				$filas["descripcion"]=$filas["descripcion"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["idEnfermedad"].",17'".');"><i class="fa fa-pencil"></i></button>';

				
				$this->enfermedades[] =$filas;
			}
			return $this->enfermedades;
		}

		/**
		* Consulto las enfermedades registradas especificas.
		* @return [array] [enfermedades]
		*/
		public function get_enfermedades_especificas($especifico){
			$this->comprobar->ComprobarCaracteres($especifico);
			$sql='SELECT "descripcion", "idEnfermedad"  from enfermedades where "idEnfermedad" in ('.$especifico.')';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->enfermedades[] =$filas;
			}
			return $this->enfermedades;
		}

		/**
		* Inserto una nueva enfermedad en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_enfermedades_store($descripcion){
			
			$this->comprobar->ComprobarCaracteres($descripcion);
					
			$valor=$this->get_enfermedades_store($descripcion);
			if ($valor==0) {
				$sql='INSERT INTO enfermedades("descripcion")  VALUES ('."'".$descripcion."'".');';

				if ($this->db->consultar($sql)) {	
					$sql='SELECT max("idEnfermedad") as "idEnfermedad" FROM enfermedades limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return $filas["idEnfermedad"];

				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			return $valor;


		}

		/**
		* Inserto una nueva enfermedad en el sistema 
		* @param [string] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_enfermedades_store_tb($descripcion){
			
			$this->comprobar->ComprobarCaracteres($descripcion);
					
			$valor=$this->get_enfermedades_store($descripcion);
			if ($valor==0) {
				$sql='INSERT INTO enfermedades("descripcion")  VALUES ('."'".$descripcion."'".');';

				if ($this->db->consultar($sql)) {	
					$sql='SELECT max("idEnfermedad") as "idEnfermedad" FROM enfermedades limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return 1;

				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			else{
				die("Ya existe '".$descripcion."' en la tabla referencial");
			}


		}

		/**
		* Consulto si existe una enfermedad con las caracteristicas a insertar 
		* @param [string] $descripcion
		* @return [integer] muestra el nro de registros   
		*/
		public function get_enfermedades_store($descripcion){
			$sql='SELECT "idEnfermedad"  from enfermedades where "descripcion" = '."'".$descripcion."' limit 1";		

			$consulta = $this->db->consultar($sql);

			if (pg_num_rows($consulta)==0) {
				return 0;
			}
			$filas=pg_fetch_assoc($consulta);
			return $filas["idEnfermedad"];
		}

		/**
		* Consulto el enfermedades a editar		
		* @param [integer] $id
		* @return [array] [enfermedades]   
		*/
		public function get_enfermedades_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "descripcion", "idEnfermedad" from enfermedades where "idEnfermedad"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["descripcion"]=strtoupper($filas["descripcion"]);
				$filas["id"]=$filas["idEnfermedad"];
				$filas["descripcion"]=$filas["descripcion"];				
				$this->enfermedades[] =$filas;
			}
			return $this->enfermedades[0];
		}

		/**
		* Modifico el enfermedades en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_enfermedades_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_enfermedades_store($dato["dato"])==0) {
				$sql= 'UPDATE enfermedades SET "descripcion"='."'".$dato["dato"]."'".' WHERE "idEnfermedad"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}




	}
?>