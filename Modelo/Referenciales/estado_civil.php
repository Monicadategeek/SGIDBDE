<?php 

	

	/**
	* Clase EstadoCivil
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class EstadoCivil 

	{
		private $db;
		private $estadosciviles;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] estadosciviles
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->estadosciviles= array();	
			$this->comprobar = new SeguridadDatos;		
			
		}

		/**
		* Consulto los datos basicos de los tipos de estados civiles registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [estadosciviles]
		*/
		public function get_EstadoCivil(){
			$sql='SELECT e."desc_edo_civ", e."id_edo_civ", (SELECT count(*) FROM grupo_familiar_est gfe where gfe."id_edo_civ"=  e."id_edo_civ") as "total"  from estado_civil e';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["total"]=$filas["total"]+$this->count($filas["id_edo_civ"]);
				$filas["desc_edo_civ"]=strtoupper($filas["desc_edo_civ"]);
				$filas["id"]=$filas["id_edo_civ"];
				$filas["descripcion"]=$filas["desc_edo_civ"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_edo_civ"].",5'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_edo_civ"].",5',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}

				$this->estadosciviles[] =$filas;
			}
			return $this->estadosciviles;
		}

		/**
		* Consulto el nro de registros que hay en la tabla estudiantes por el cod del estado civil, 
		* la union entre las tabla difiere por el tipo de campo estudiantes es caracter y en estado_civil es integer
		* @param [integer] $id
		* @return [integer] muestra el nro de registros 
		*/
		private function count($id){
			$sql='SELECT * FROM estudiantes es where es."edo_civil" ='."'".$id."'";
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}


		/**
		* Inserto un nuevo estado_civil en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_EstadoCivil_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_EstadoCivil_store($dato)==0) {
				$sql= 'INSERT INTO estado_civil("desc_edo_civ") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un estado_civil con las caracteristicas a insertar 
		* @param [array] [estadosciviles]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_EstadoCivil_store($dato){
			$sql='SELECT * from estado_civil where "desc_edo_civ"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el estado_civil a editar		
		* @param [integer] $id
		* @return [array] [estadosciviles]   
		*/
		public function get_EstadoCivil_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_edo_civ", "id_edo_civ" from estado_civil where "id_edo_civ"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_edo_civ"]=strtoupper($filas["desc_edo_civ"]);
				$filas["id"]=$filas["id_edo_civ"];
				$filas["descripcion"]=$filas["desc_edo_civ"];				
				$this->estadosciviles[] =$filas;
			}
			return $this->estadosciviles[0];
		}

		/**
		* Modifico el estado_civil en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_EstadoCivil_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_EstadoCivil_store($dato["dato"])==0) {
				$sql= 'UPDATE estado_civil SET "desc_edo_civ"='."'".$dato["dato"]."'".' WHERE "id_edo_civ"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el estado_civil a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_EstadoCivil_destroy($id){
			if ($this->get_EstadoCivil_destroy($id)>0) {
				$sql= 'DELETE FROM estado_civil WHERE "id_edo_civ"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el estado_civil en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_EstadoCivil_destroy($id){
			$sql='SELECT "desc_edo_civ", "id_edo_civ" from estado_civil where "id_edo_civ"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?> 