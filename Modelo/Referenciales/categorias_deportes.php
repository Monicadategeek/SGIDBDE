<?php 

	/**
	* Clase CategDeportes
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class CategDeportes 

	{
		private $db;
		private $deportes;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] deportes
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->deportes= array();			
			
		}

		/**
		* Consulto los datos basicos de las deportes registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [deportes]
		*/
		public function get_CategDeportes(){
			$sql='SELECT cd."id_cat_dep", cd."desc_cat_dep", (SELECT count(*) FROM deportes_x_estudiante de where de."id_deporte"= cd."id_cat_dep") as "total" from categorias_deportes cd';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_cat_dep"]=strtoupper($filas["desc_cat_dep"]);
				$filas["id"]=$filas["id_cat_dep"];
				$filas["descripcion"]=$filas["desc_cat_dep"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_cat_dep"].",3'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_cat_dep"].",3',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}

				$this->deportes[] =$filas;
			}
			return $this->deportes;
		}

		/**
		* Inserto un nuevo deporte en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_CategDeportes_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_CategDeportes_store($dato)==0) {
				$sql= 'INSERT INTO categorias_deportes("desc_cat_dep") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un deporte con las caracteristicas a insertar 
		* @param [array] [deportes]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_CategDeportes_store($dato){
			$sql='SELECT * from categorias_deportes where "desc_cat_dep"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

		
		/**
		* Consulto el deporte a editar		
		* @param [integer] $id
		* @return [array] [deportes]   
		*/
		public function get_CategDeportes_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_cat_dep", "id_cat_dep" from categorias_deportes where "id_cat_dep"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_cat_dep"]=strtoupper($filas["desc_cat_dep"]);
				$filas["id"]=$filas["id_cat_dep"];
				$filas["descripcion"]=$filas["desc_cat_dep"];				
				$this->deportes[] =$filas;
			}
			return $this->deportes[0];
		}

		/**
		* Modifico el deporte en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_CategDeportes_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_CategDeportes_store($dato["dato"])==0) {
				$sql= 'UPDATE categorias_deportes SET "desc_cat_dep"='."'".$dato["dato"]."'".' WHERE "id_cat_dep"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el deporte a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_CategDeportes_destroy($id){
			if ($this->get_CategDeportes_destroy($id)>0) {
				$sql= 'DELETE FROM categorias_deportes WHERE "id_cat_dep"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el deporte en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_CategDeportes_destroy($id){
			$sql='SELECT "desc_cat_dep", "id_cat_dep" from categorias_deportes where "id_cat_dep"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}






	}
?>