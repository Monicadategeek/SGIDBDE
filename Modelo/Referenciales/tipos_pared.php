<?php 

	
	/**
	* Clase TiposPared
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class TiposPared 

	{
		private $db;
		private $tpparedes;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tpparedes
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tpparedes= array();			
			
		}

		/**
		* Consulto los tipos Paredes de la vivienda registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [tpparedes]
		*/
		public function get_TiposPared(){
			$sql='SELECT tp."desc_tipo_pared", tp."id_tipo_pared", (SELECT count(*) FROM dat_fisic_ambient_est dfae where dfae."id_tipo_pared"= tp."id_tipo_pared") as "total" from tipos_pared tp';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_pared"]=strtoupper($filas["desc_tipo_pared"]);
				$filas["id"]=$filas["id_tipo_pared"];
				$filas["descripcion"]=$filas["desc_tipo_pared"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_tipo_pared"].",14'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_tipo_pared"].",14',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->tpparedes[] =$filas;
			}
			return $this->tpparedes;
		}

		/**
		* Inserto un nuevo tipos_pared en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_TiposPared_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_TiposPared_store($dato)==0) {
				$sql= 'INSERT INTO tipos_pared("desc_tipo_pared") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un tipos_pared con las caracteristicas a insertar 
		* @param [array] [tpparedes]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposPared_store($dato){
			$sql='SELECT * from tipos_pared where "desc_tipo_pared"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el tipos_pared a editar		
		* @param [integer] $id
		* @return [array] [tpparedes]   
		*/
		public function get_TiposPared_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_tipo_pared", "id_tipo_pared" from tipos_pared where "id_tipo_pared"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_pared"]=strtoupper($filas["desc_tipo_pared"]);
				$filas["id"]=$filas["id_tipo_pared"];
				$filas["descripcion"]=$filas["desc_tipo_pared"];				
				$this->tpparedes[] =$filas;
			}
			return $this->tpparedes[0];
		}

		/**
		* Modifico el tipos_pared en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_TiposPared_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_TiposPared_store($dato["dato"])==0) {
				$sql= 'UPDATE tipos_pared SET "desc_tipo_pared"='."'".$dato["dato"]."'".' WHERE "id_tipo_pared"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el tipos_pared a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_TiposPared_destroy($id){
			if ($this->get_TiposPared_destroy($id)>0) {
				$sql= 'DELETE FROM tipos_pared WHERE "id_tipo_pared"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el tipos_pared en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposPared_destroy($id){
			$sql='SELECT "desc_tipo_pared", "id_tipo_pared" from tipos_pared where "id_tipo_pared"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>