<?php 
 
	/**
	* Clase HabilidadesEst
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class HabilidadesEst 

	{
		private $db;
		private $habilidades;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] habilidades
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->comprobar = new SeguridadDatos;	
			$this->db = new conexion;
			$this->habilidades= array();	

			
		}

		/**
		* Consulto los datos basicos de los habilidades artisticas registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [habilidades]
		*/
		public function get_HabilidadesEst(){
			$sql='SELECT he."id_habil", he."desc_habilidad", (SELECT count(*) FROM habilidades_x_estudiante hxe where hxe."id_habil"= he."id_habil") as "total" from habilidades_est he ';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_habilidad"]=strtoupper($filas["desc_habilidad"]);
				$filas["id"]=$filas["id_habil"];
				$filas["descripcion"]=$filas["desc_habilidad"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_habil"].",8'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_habil"].",8',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->habilidades[] =$filas;
			}
			return $this->habilidades;
		}

		/**
		* Inserto un nuevo habilidades_est en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_HabilidadesEst_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_HabilidadesEst_store($dato)==0) {
				$sql= 'INSERT INTO habilidades_est("desc_habilidad") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un habilidades_est con las caracteristicas a insertar 
		* @param [array] [habilidades]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_HabilidadesEst_store($dato){
			$sql='SELECT * from habilidades_est where "desc_habilidad"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el habilidades_est a editar		
		* @param [integer] $id
		* @return [array] [habilidades]   
		*/
		public function get_HabilidadesEst_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_habilidad", "id_habil" from habilidades_est where "id_habil"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_habilidad"]=strtoupper($filas["desc_habilidad"]);
				$filas["id"]=$filas["id_habil"];
				$filas["descripcion"]=$filas["desc_habilidad"];				
				$this->habilidades[] =$filas;
			}
			return $this->habilidades[0];
		}

		/**
		* Modifico el habilidades_est en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_HabilidadesEst_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_HabilidadesEst_store($dato["dato"])==0) {
				$sql= 'UPDATE habilidades_est SET "desc_habilidad"='."'".$dato["dato"]."'".' WHERE "id_habil"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el habilidades_est a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_HabilidadesEst_destroy($id){
			if ($this->get_HabilidadesEst_destroy($id)>0) {
				$sql= 'DELETE FROM habilidades_est WHERE "id_habil"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el habilidades_est en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_HabilidadesEst_destroy($id){
			$sql='SELECT "desc_habilidad", "id_habil" from habilidades_est where "id_habil"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}


	}
?>