<?php 

	
	/**
	* Clase TiposPiso
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class TiposPiso 

	{
		private $db;
		private $tppisos;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tppisos
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tppisos= array();			
			
		}

		/**
		* Consulto los tipos de pisos de la vivienda registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [tppisos]
		*/
		public function get_TiposPiso(){
			$sql='SELECT tp."desc_tipo_piso", tp."id_tipo_piso", (SELECT count(*) FROM dat_fisic_ambient_est dfae where dfae."id_tipo_piso"= tp."id_tipo_piso") as "total" from tipos_piso tp';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_piso"]=strtoupper($filas["desc_tipo_piso"]);
				$filas["id"]=$filas["id_tipo_piso"];
				$filas["descripcion"]=$filas["desc_tipo_piso"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_tipo_piso"].",15'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_tipo_piso"].",15',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->tppisos[] =$filas;
			}
			return $this->tppisos;
		}


		/**
		* Inserto un nuevo tipos_piso en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_TiposPiso_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_TiposPiso_store($dato)==0) {
				$sql= 'INSERT INTO tipos_piso("desc_tipo_piso") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un tipos_piso con las caracteristicas a insertar 
		* @param [array] [tppisos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposPiso_store($dato){
			$sql='SELECT * from tipos_piso where "desc_tipo_piso"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el tipos_piso a editar		
		* @param [integer] $id
		* @return [array] [tppisos]   
		*/
		public function get_TiposPiso_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_tipo_piso", "id_tipo_piso" from tipos_piso where "id_tipo_piso"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_piso"]=strtoupper($filas["desc_tipo_piso"]);
				$filas["id"]=$filas["id_tipo_piso"];
				$filas["descripcion"]=$filas["desc_tipo_piso"];				
				$this->tppisos[] =$filas;
			}
			return $this->tppisos[0];
		}

		/**
		* Modifico el tipos_piso en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_TiposPiso_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_TiposPiso_store($dato["dato"])==0) {
				$sql= 'UPDATE tipos_piso SET "desc_tipo_piso"='."'".$dato["dato"]."'".' WHERE "id_tipo_piso"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el tipos_piso a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_TiposPiso_destroy($id){
			if ($this->get_TiposPiso_destroy($id)>0) {
				$sql= 'DELETE FROM tipos_piso WHERE "id_tipo_piso"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el tipos_piso en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposPiso_destroy($id){
			$sql='SELECT "desc_tipo_piso", "id_tipo_piso" from tipos_piso where "id_tipo_piso"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>