<?php 

	 

	/**
	* Clase ProfOcup
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class ProfOcup 

	{
		private $db;
		private $profocup;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] profocup
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */

		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->comprobar = new SeguridadDatos;
			$this->db = new conexion;
			$this->profocup= array();			
		}

		/**
		* Consulto las profeciones u oficios registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [profocup]
		*/
		public function get_ProfOcup(){
			$sql='SELECT po."desc_prof_ocup", po."id_prof_ocup", (SELECT count(*) FROM grupo_familiar_est gfe where gfe."id_prof_ocup"= po."id_prof_ocup") as "total" from prof_ocup po';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_prof_ocup"]=strtoupper($filas["desc_prof_ocup"]);
				$filas["id"]=$filas["id_prof_ocup"];
				$filas["descripcion"]=$filas["desc_prof_ocup"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_prof_ocup"].",11'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_prof_ocup"].",11',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->profocup[] =$filas;
			}
			return $this->profocup;
		}

		/**
		* Inserto un nuevo prof_ocup en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_ProfOcup_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_ProfOcup_store($dato)==0) {
				$sql= 'INSERT INTO prof_ocup("desc_prof_ocup") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un prof_ocup con las caracteristicas a insertar 
		* @param [array] [profocup]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ProfOcup_store($dato){
			$sql='SELECT * from prof_ocup where "desc_prof_ocup"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el prof_ocup a editar		
		* @param [integer] $id
		* @return [array] [profocup]   
		*/
		public function get_ProfOcup_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_prof_ocup", "id_prof_ocup" from prof_ocup where "id_prof_ocup"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_prof_ocup"]=strtoupper($filas["desc_prof_ocup"]);
				$filas["id"]=$filas["id_prof_ocup"];
				$filas["descripcion"]=$filas["desc_prof_ocup"];				
				$this->ProfOcup[] =$filas;
			}
			return $this->ProfOcup[0];
		}

		/**
		* Modifico el prof_ocup en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_ProfOcup_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_ProfOcup_store($dato["dato"])==0) {
				$sql= 'UPDATE prof_ocup SET "desc_prof_ocup"='."'".$dato["dato"]."'".' WHERE "id_prof_ocup"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el prof_ocup a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_ProfOcup_destroy($id){
			if ($this->get_ProfOcup_destroy($id)>0) {
				$sql= 'DELETE FROM prof_ocup WHERE "id_prof_ocup"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el prof_ocup en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ProfOcup_destroy($id){
			$sql='SELECT "desc_prof_ocup", "id_prof_ocup" from prof_ocup where "id_prof_ocup"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>