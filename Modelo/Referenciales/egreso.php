<?php 

	

	/**
	* Clase Egreso
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class Egreso 

	{
		private $db;
		private $egreso;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] egreso
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->egreso= array();		
			
		}

		/**
		* Consulto los datos basicos de los tipos de egresos registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [egreso]
		*/
		public function get_egreso(){
			$sql='SELECT e."Descripcion", e."IdEgreso", (SELECT count(*) FROM gastos_x_estudiante ge where ge."id_concepto"= e."IdEgreso") as "total" from egreso e';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Descripcion"]=strtoupper($filas["Descripcion"]);
				$filas["id"]=$filas["IdEgreso"];
				$filas["descripcion"]=$filas["Descripcion"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["IdEgreso"].",4'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["IdEgreso"].",4',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}

				$this->egreso[] =$filas;
			}
			return $this->egreso;
		}

		
		/**
		* Inserto un nuevo egreso en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_egreso_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_egreso_store($dato)==0) {
				$sql= 'INSERT INTO egreso("Descripcion") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un egreso con las caracteristicas a insertar 
		* @param [array] [egresos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_egreso_store($dato){
			$sql='SELECT * from egreso where "Descripcion"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

		
		/**
		* Consulto el egreso a editar		
		* @param [integer] $id
		* @return [array] [egresos]   
		*/
		public function get_egreso_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "Descripcion", "IdEgreso" from egreso where "IdEgreso"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Descripcion"]=strtoupper($filas["Descripcion"]);
				$filas["id"]=$filas["IdEgreso"];
				$filas["descripcion"]=$filas["Descripcion"];				
				$this->egreso[] =$filas;
			}
			return $this->egreso[0];
		}

		/**
		* Modifico el egreso en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_egreso_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_egreso_store($dato["dato"])==0) {
				$sql= 'UPDATE egreso SET "Descripcion"='."'".$dato["dato"]."'".' WHERE "IdEgreso"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el egreso a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_egreso_destroy($id){
			if ($this->get_egreso_destroy($id)>0) {
				$sql= 'DELETE FROM egreso WHERE "IdEgreso"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el egreso en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_egreso_destroy($id){
			$sql='SELECT "Descripcion", "IdEgreso" from egreso where "IdEgreso"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>