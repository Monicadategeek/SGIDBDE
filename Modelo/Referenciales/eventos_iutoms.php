<?php 

	 

	/**
	* Clase EventosIUTOMS
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class EventosIUTOMS 

	{
		private $db;
		private $eventos;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] eventos
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->eventos= array();
			$this->comprobar = new SeguridadDatos;			
			
		}

		/**
		* Consulto los datos basicos de los tipos de eventos iutoms registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [eventos]
		*/
		public function get_EventosIUTOMS(){
			$sql='SELECT e."id_evento", e."desc_evento", (SELECT count(*) FROM eventos_x_estudiantes exe where exe."id_evento"= e."id_evento") as "total"  from eventos_iutoms e';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_evento"]=strtoupper($filas["desc_evento"]);
				$filas["id"]=$filas["id_evento"];
				$filas["descripcion"]=$filas["desc_evento"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_evento"].",6'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_evento"].",6',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}

				$this->eventos[] =$filas;
			}
			return $this->eventos;
		}


		/**
		* Inserto un nuevo eventos_iutoms en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_EventosIUTOMS_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_EventosIUTOMS_store($dato)==0) {
				$sql= 'INSERT INTO eventos_iutoms("desc_evento") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un eventos_iutoms con las caracteristicas a insertar 
		* @param [array] [eventos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_EventosIUTOMS_store($dato){
			$sql='SELECT * from eventos_iutoms where "desc_evento"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el eventos_iutoms a editar		
		* @param [integer] $id
		* @return [array] [eventos]   
		*/
		public function get_EventosIUTOMS_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_evento", "id_evento" from eventos_iutoms where "id_evento"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_evento"]=strtoupper($filas["desc_evento"]);
				$filas["id"]=$filas["id_evento"];
				$filas["descripcion"]=$filas["desc_evento"];				
				$this->eventos[] =$filas;
			}
			return $this->eventos[0];
		}

		/**
		* Modifico el eventos_iutoms en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_EventosIUTOMS_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_EventosIUTOMS_store($dato["dato"])==0) {
				$sql= 'UPDATE eventos_iutoms SET "desc_evento"='."'".$dato["dato"]."'".' WHERE "id_evento"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el eventos_iutoms a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_EventosIUTOMS_destroy($id){
			if ($this->get_EventosIUTOMS_destroy($id)>0) {
				$sql= 'DELETE FROM eventos_iutoms WHERE "id_evento"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el eventos_iutoms en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_EventosIUTOMS_destroy($id){
			$sql='SELECT "desc_evento", "id_evento" from eventos_iutoms where "id_evento"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}


	}
?>