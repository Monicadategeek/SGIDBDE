<?php 

	

	/**
	* Clase TipoVivienda
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class TipoVivienda 

	{
		private $db;
		private $tpviviendas;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tpviviendas
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tpviviendas= array();			
			
		}

		/**
		* Consulto los tipos  viviendas registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [tpviviendas]
		*/
		public function get_TipoVivienda(){
			$sql='SELECT tv."desc_tipo_viv", tv."id_tipo_viv", (SELECT count(*) FROM dat_fisic_ambient_est dfae where dfae."id_tip_viv"= tv."id_tipo_viv") as "total" from tipo_vivienda tv';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_viv"]=strtoupper($filas["desc_tipo_viv"]);
				$filas["id"]=$filas["id_tipo_viv"];
				$filas["descripcion"]=$filas["desc_tipo_viv"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_tipo_viv"].",13'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_tipo_viv"].",13',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->tpviviendas[] =$filas;
			}
			return $this->tpviviendas;
		}

		/**
		* Inserto un nuevo tipo_vivienda en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_TipoVivienda_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_TipoVivienda_store($dato)==0) {
				$sql= 'INSERT INTO tipo_vivienda("desc_tipo_viv") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un tipo_vivienda con las caracteristicas a insertar 
		* @param [array] [tpviviendas]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TipoVivienda_store($dato){
			$sql='SELECT * from tipo_vivienda where "desc_tipo_viv"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el tipo_vivienda a editar		
		* @param [integer] $id
		* @return [array] [tpviviendas]   
		*/
		public function get_TipoVivienda_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_tipo_viv", "id_tipo_viv" from tipo_vivienda where "id_tipo_viv"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_viv"]=strtoupper($filas["desc_tipo_viv"]);
				$filas["id"]=$filas["id_tipo_viv"];
				$filas["descripcion"]=$filas["desc_tipo_viv"];				
				$this->tpviviendas[] =$filas;
			}
			return $this->tpviviendas[0];
		}

		/**
		* Modifico el tipo_vivienda en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_TipoVivienda_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_TipoVivienda_store($dato["dato"])==0) {
				$sql= 'UPDATE tipo_vivienda SET "desc_tipo_viv"='."'".$dato["dato"]."'".' WHERE "id_tipo_viv"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el tipo_vivienda a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_TipoVivienda_destroy($id){
			if ($this->get_TipoVivienda_destroy($id)>0) {
				$sql= 'DELETE FROM tipo_vivienda WHERE "id_tipo_viv"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el tipo_vivienda en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TipoVivienda_destroy($id){
			$sql='SELECT "desc_tipo_viv", "id_tipo_viv" from tipo_vivienda where "id_tipo_viv"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
	}
?>