<?php 
	/**
	* 
	*/
	class TipoDepartamento
	{
		private $db;
		private $tipodepartamento;
		private $comprobar;
		
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db=new conexion;
			$this->tipodepartamento =array();
			
		}

		public function get_TipoDepartamento(){
			$sql='SELECT "IdDepartamento", "Descripcion", "IdDepartamento" as "nrotipodepto", "Descripcion" as "TipoDepto" FROM Departamento';			
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tipodepartamento[]=$filas;
			}		
			
			return $this->tipodepartamento;

		}


		public function get_TipoDepartamento_in($val){
			$this->comprobar->ComprobarCaracteres($val);
			$sql='SELECT * FROM Departamento where "IdDepartamento" in ('.$val.')';			
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tipodepartamento[]=$filas;
			}
			
			
			return $this->tipodepartamento;

		}

		
	}
 ?>