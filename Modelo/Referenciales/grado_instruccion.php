<?php 

	

	/**
	* Clase GradoInstruccion
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class GradoInstruccion 

	{
		private $db;
		private $gradinst;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] gradinst
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->gradinst= array();	
			$this->comprobar = new SeguridadDatos;		
			
		}


		/**
		* Consulto los datos basicos de los grados de instruccion registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [gradinst]
		*/
		public function get_GradoInstruccion(){
			$sql='SELECT gi."desc_grado_instr", gi."id_grado_instr", (SELECT count(*) FROM grupo_familiar_est gfe where gfe."id_grado_instr"= gi."id_grado_instr") as "total" from grado_instruccion gi';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_grado_instr"]=strtoupper($filas["desc_grado_instr"]);
				$filas["id"]=$filas["id_grado_instr"];
				$filas["descripcion"]=$filas["desc_grado_instr"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_grado_instr"].",7'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_grado_instr"].",7',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->gradinst[] =$filas;
			}
			return $this->gradinst;
		}


		/**
		* Inserto un nuevo grado_instruccion en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_GradoInstruccion_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_GradoInstruccion_store($dato)==0) {
				$sql= 'INSERT INTO grado_instruccion("desc_grado_instr") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un grado_instruccion con las caracteristicas a insertar 
		* @param [array] [gradinst]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_GradoInstruccion_store($dato){
			$sql='SELECT * from grado_instruccion where "desc_grado_instr"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el grado_instruccion a editar		
		* @param [integer] $id
		* @return [array] [gradinst]   
		*/
		public function get_GradoInstruccion_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_grado_instr", "id_grado_instr" from grado_instruccion where "id_grado_instr"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_grado_instr"]=strtoupper($filas["desc_grado_instr"]);
				$filas["id"]=$filas["id_grado_instr"];
				$filas["descripcion"]=$filas["desc_grado_instr"];				
				$this->gradinst[] =$filas;
			}
			return $this->gradinst[0];
		}

		/**
		* Modifico el grado_instruccion en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_GradoInstruccion_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_GradoInstruccion_store($dato["dato"])==0) {
				$sql= 'UPDATE grado_instruccion SET "desc_grado_instr"='."'".$dato["dato"]."'".' WHERE "id_grado_instr"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el grado_instruccion a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_GradoInstruccion_destroy($id){
			if ($this->get_GradoInstruccion_destroy($id)>0) {
				$sql= 'DELETE FROM grado_instruccion WHERE "id_grado_instr"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el grado_instruccion en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_GradoInstruccion_destroy($id){
			$sql='SELECT "desc_grado_instr", "id_grado_instr" from grado_instruccion where "id_grado_instr"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>