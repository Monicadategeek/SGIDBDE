<?php 

	

	/**
	* 
	*/
	class Municipios 

	{
		private $db;
		private $municipios;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->municipios= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=1;
			
		}


		public function get_municipios($estado){
			$this->comprobar->ComprobarNumeric($estado);
			$sql='SELECT m."municipio", m."id_municipio" from municipios m where m."id_estado"='.$estado;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->municipios[] =$filas;
			}
			return $this->municipios;
		}

		public function get_municipios_show($id_municipio){
			$this->comprobar->ComprobarNumeric($id_municipio);
			$sql='SELECT m."municipio" from municipios m where  m."id_municipio"='.$id_municipio;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->municipios[] =$filas;
			}
			return $this->municipios[0]["municipio"];
		}

	}
?>