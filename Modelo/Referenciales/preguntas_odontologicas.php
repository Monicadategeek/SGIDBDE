<?php 
	/**
	* Clase PreguntasOdontologicas
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class PreguntasOdontologicas 

	{
		private $db;
		private $preguntas;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] preguntas
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db=new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->preguntas =array();
			
		}

		/**
		* Consulto los tipos de preguntas odontologicas registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [preguntas]
		*/
		public function get_PreguntasOdontologicas(){
			$sql='SELECT po."id", po."Descripcion", po."tiporesp", (SELECT count(*) FROM encuest_odont_pregu eop where eop."idpregunta"= po."id") as "total" FROM preguntas_odontologicas po order by po."id"';			
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["descripcion"]=$filas["Descripcion"];
				if ($filas["tiporesp"]==1) {
					$filas["tiporesp"]='Respuesta Cerrada (Si o No)';
				}
				else if ($filas["tiporesp"]==0) {
					$filas["tiporesp"]='Sin Respuesta';
				}
				else if ($filas["tiporesp"]==0) {
					$filas["tiporesp"]='Respuesta (Si o No) más una caja de texto';
				}

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id"].",19'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id"].",19',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->preguntas[]=$filas;
			}
			
			
			return $this->preguntas;

		}
		

		/**
		* Consulto el nro de registros de la tabla
		* @return [integer] muestra el nro de registros 
		*/
		public function get_PreguntasOdontologicas_numrows(){
			$sql='SELECT * FROM preguntas_odontologicas';			
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}

		/**
		* Inserto un nuevo preguntas_odontologicas en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_PreguntasOdontologicas_store($dato){
			$this->comprobar->ComprobarCaracteres($dato["Descripcion"]);
			$this->comprobar->ComprobarNumeric($dato["tiporesp"]);
			if ($this->get_PreguntasOdontologicas_store($dato["Descripcion"])==0) {
				$sql= 'INSERT INTO preguntas_odontologicas("Descripcion", "tiporesp") VALUES ('."'".$dato["Descripcion"]."', ".$dato["tiporesp"].");";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato["Descripcion"]."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un preguntas_odontologicas con las caracteristicas a insertar 
		* @param [array] [preguntas]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_PreguntasOdontologicas_store($dato){
			$sql='SELECT * from preguntas_odontologicas where "Descripcion"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el preguntas_odontologicas a editar		
		* @param [integer] $id
		* @return [array] [preguntas]   
		*/
		public function get_PreguntasOdontologicas_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "Descripcion", "id" from preguntas_odontologicas where "id"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Descripcion"]=strtoupper($filas["Descripcion"]);
				$filas["id"]=$filas["id"];
				$filas["descripcion"]=$filas["Descripcion"];				
				$this->preguntas[] =$filas;
			}
			return $this->preguntas[0];
		}

		/**
		* Modifico el preguntas_odontologicas en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_PreguntasOdontologicas_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_PreguntasOdontologicas_store($dato["dato"])==0) {
				$sql= 'UPDATE preguntas_odontologicas SET "Descripcion"='."'".$dato["dato"]."'".' WHERE "id"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el preguntas_odontologicas a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_PreguntasOdontologicas_destroy($id){
			if ($this->get_PreguntasOdontologicas_destroy($id)>0) {
				$sql= 'DELETE FROM preguntas_odontologicas WHERE "id"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el preguntas_odontologicas en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_PreguntasOdontologicas_destroy($id){
			$sql='SELECT "Descripcion", "id" from preguntas_odontologicas where "id"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}



	}