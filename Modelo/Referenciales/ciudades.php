<?php 

	/**
	* 
	*/
	class Ciudades 

	{
		private $db;
		private $ciudades;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->ciudades= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=1;
			
		}


		public function get_ciudades($estado){
			$this->comprobar->ComprobarNumeric($estado);
			$sql='SELECT c."ciudad", c."id_ciudad" from ciudades c where c."id_estado"='.$estado;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->ciudades[] =$filas;
			}
			return $this->ciudades;
		}

	}
?>