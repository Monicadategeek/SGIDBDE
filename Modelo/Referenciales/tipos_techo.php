<?php 

	/**
	* Clase TiposTecho
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class TiposTecho

	{
		private $db;
		private $tptechos;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] tptechos
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tptechos= array();			
			
		}

		/**
		* Consulto los tipos de pisos de la vivienda registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [tptechos]
		*/
		public function get_TiposTecho(){
			$sql='SELECT tt."desc_tipo_techo", tt."id_tipo_techo", (SELECT count(*) FROM dat_fisic_ambient_est dfae where dfae."id_tipo_techo"= tt."id_tipo_techo") as "total" from tipos_techo tt';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_techo"]=strtoupper($filas["desc_tipo_techo"]);
				$filas["id"]=$filas["id_tipo_techo"];
				$filas["descripcion"]=$filas["desc_tipo_techo"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_tipo_techo"].",16'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_tipo_techo"].",16',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->tptechos[] =$filas;
			}
			return $this->tptechos;
		}

		/**
		* Inserto un nuevo tipos_techo en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_TiposTecho_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_TiposTecho_store($dato)==0) {
				$sql= 'INSERT INTO tipos_techo("desc_tipo_techo") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un tipos_techo con las caracteristicas a insertar 
		* @param [array] [tptechos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposTecho_store($dato){
			$sql='SELECT * from tipos_techo where "desc_tipo_techo"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el tipos_techo a editar		
		* @param [integer] $id
		* @return [array] [tptechos]   
		*/
		public function get_TiposTecho_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_tipo_techo", "id_tipo_techo" from tipos_techo where "id_tipo_techo"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_tipo_techo"]=strtoupper($filas["desc_tipo_techo"]);
				$filas["id"]=$filas["id_tipo_techo"];
				$filas["descripcion"]=$filas["desc_tipo_techo"];				
				$this->tptechos[] =$filas;
			}
			return $this->tptechos[0];
		}

		/**
		* Modifico el tipos_techo en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_TiposTecho_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_TiposTecho_store($dato["dato"])==0) {
				$sql= 'UPDATE tipos_techo SET "desc_tipo_techo"='."'".$dato["dato"]."'".' WHERE "id_tipo_techo"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el tipos_techo a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_TiposTecho_destroy($id){
			if ($this->get_TiposTecho_destroy($id)>0) {
				$sql= 'DELETE FROM tipos_techo WHERE "id_tipo_techo"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el tipos_techo en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_TiposTecho_destroy($id){
			$sql='SELECT "desc_tipo_techo", "id_tipo_techo" from tipos_techo where "id_tipo_techo"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>