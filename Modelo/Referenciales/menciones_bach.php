<?php 

	

	/**
	* Clase MencionesBach
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class MencionesBach 

	{
		private $db;
		private $MencionesBach;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] MencionesBach
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */
		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->MencionesBach= array();		
			
		}


		/**
		* Consulto los datos basicos de los menciones de bachiller registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [MencionesBach]
		*/
		public function get_MencionesBach(){
			$sql='SELECT m."desc_mencion", m."id_mencion_bach", (SELECT count(*) FROM dat_acad_est dae where dae."id_tit_bach"= m."id_mencion_bach") as "total" from menciones_bach m';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_mencion"]=strtoupper($filas["desc_mencion"]);
				$filas["id"]=$filas["id_mencion_bach"];
				$filas["descripcion"]=$filas["desc_mencion"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_mencion_bach"].",9'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_mencion_bach"].",9',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->MencionesBach[] =$filas;
			}
			return $this->MencionesBach;
		}

		/**
		* Inserto un nuevo menciones_bach en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_MencionesBach_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_MencionesBach_store($dato)==0) {
				$sql= 'INSERT INTO menciones_bach("desc_mencion") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un menciones_bach con las caracteristicas a insertar 
		* @param [array] [MencionesBach]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_MencionesBach_store($dato){
			$sql='SELECT * from menciones_bach where "desc_mencion"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el menciones_bach a editar		
		* @param [integer] $id
		* @return [array] [MencionesBach]   
		*/
		public function get_MencionesBach_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_mencion", "id_mencion_bach" from menciones_bach where "id_mencion_bach"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_mencion"]=strtoupper($filas["desc_mencion"]);
				$filas["id"]=$filas["id_mencion_bach"];
				$filas["descripcion"]=$filas["desc_mencion"];				
				$this->MencionesBach[] =$filas;
			}
			return $this->MencionesBach[0];
		}

		/**
		* Modifico el menciones_bach en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_MencionesBach_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_MencionesBach_store($dato["dato"])==0) {
				$sql= 'UPDATE menciones_bach SET "desc_mencion"='."'".$dato["dato"]."'".' WHERE "id_mencion_bach"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el menciones_bach a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_MencionesBach_destroy($id){
			if ($this->get_MencionesBach_destroy($id)>0) {
				$sql= 'DELETE FROM menciones_bach WHERE "id_mencion_bach"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el menciones_bach en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_MencionesBach_destroy($id){
			$sql='SELECT "desc_mencion", "id_mencion_bach" from menciones_bach where "id_mencion_bach"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}


	}
?>