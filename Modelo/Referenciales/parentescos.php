<?php 

	 

	/**
	* Clase Parentescos
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class Parentescos 

	{
		private $db;
		private $parentescos;
		private $comprobar;


		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] parentescos
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */

		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->comprobar = new SeguridadDatos;
			$this->db = new conexion;
			$this->parentescos= array();			
			
		}

		/**
		* Consulto los datos basicos de los Parentesco Familiares. para los select o muestra en Tablas Referenciales
		* @return [array] [parentescos]
		*/
		public function get_Parentescos(){
			$sql='SELECT p."desc_parentesco", p."id_parentesco", (SELECT count(*) FROM grupo_familiar_est gfe where gfe."id_parentesco"= p."id_parentesco") as "total" from parentescos p';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_parentesco"]=strtoupper($filas["desc_parentesco"]);
				$filas["id"]=$filas["id_parentesco"];
				$filas["descripcion"]=$filas["desc_parentesco"];

				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_parentesco"].",10'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_parentesco"].",10',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->parentescos[] =$filas;
			}
			return $this->parentescos;
		}

		/**
		* Inserto un nuevo parentescos en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_Parentescos_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_Parentescos_store($dato)==0) {
				$sql= 'INSERT INTO parentescos("desc_parentesco") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe un parentescos con las caracteristicas a insertar 
		* @param [array] [parentescos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Parentescos_store($dato){
			$sql='SELECT * from parentescos where "desc_parentesco"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}
		

		/**
		* Consulto el parentescos a editar		
		* @param [integer] $id
		* @return [array] [parentescos]   
		*/
		public function get_Parentescos_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_parentesco", "id_parentesco" from parentescos where "id_parentesco"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_parentesco"]=strtoupper($filas["desc_parentesco"]);
				$filas["id"]=$filas["id_parentesco"];
				$filas["descripcion"]=$filas["desc_parentesco"];				
				$this->parentescos[] =$filas;
			}
			return $this->parentescos[0];
		}

		/**
		* Modifico el parentescos en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_Parentescos_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_Parentescos_store($dato["dato"])==0) {
				$sql= 'UPDATE parentescos SET "desc_parentesco"='."'".$dato["dato"]."'".' WHERE "id_parentesco"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino el parentescos a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_Parentescos_destroy($id){
			if ($this->get_Parentescos_destroy($id)>0) {
				$sql= 'DELETE FROM parentescos WHERE "id_parentesco"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe el parentescos en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Parentescos_destroy($id){
			$sql='SELECT "desc_parentesco", "id_parentesco" from parentescos where "id_parentesco"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

	}
?>