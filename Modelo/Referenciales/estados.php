<?php 

	

	/**
	* 
	*/
	class Estados 

	{
		private $db;
		private $estados;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->estados= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			
		}


		public function get_estados(){
			$sql='SELECT e."estado", e."id_estado" from estados e';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->estados[] =$filas;
			}
			return $this->estados;
		}

	}
?>