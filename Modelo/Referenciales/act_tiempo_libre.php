<?php 

	/**
	* Clase ActTiempoLibre
	* Es una Tabla Referencial
	*	
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class ActTiempoLibre 

	{
		private $db;
		private $actividades;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] actividades
		 * @global [object] db
		 * @global [object] comprobar
		 * 
		 */

		public function __construct()
		{

			require_once(dirname(__FILE__) ."/../SeguridadDatos.php");
			require_once(dirname(__FILE__) ."/../../Controller/conectar.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->actividades= array();			
			
		}


		/**
		* Consulto los datos basicos de las actividades registrados. para los select o muestra en Tablas Referenciales
		* @return [array] [actividades]
		*/	
		public function get_ActTiempoLibre(){
			$sql='SELECT act."id_act_t_l", act."desc_act_t_l", (SELECT count(*) FROM actividades_t_l_x_estudiante a where a."id_act_t_l"= act."id_act_t_l") as "total" from act_tiempo_libre act ';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_act_t_l"]=strtoupper($filas["desc_act_t_l"]);
				$filas["id"]=$filas["id_act_t_l"];
				$filas["descripcion"]=$filas["desc_act_t_l"];
				$filas["boton"]='<button class="btn btn-warning btn-xs" title="Modificar" onclick="modificar('.
				"'../Controller/TablasReferencialesController.php','".$filas["id_act_t_l"].",1'".');"><i class="fa fa-pencil"></i></button>';

				if ($filas["total"]==0) {
					$filas["boton"]=$filas["boton"].'<button class="btn btn-danger btn-xs" title="Eliminar" onclick="cuadroeliminar('."'../Controller/TablasReferencialesController.php','".$filas["id_act_t_l"].",1',"."'del dato'".');"><i class="fa fa-trash-o"></i></button>';
				}
				$this->actividades[] =$filas;
			}
			return $this->actividades;
		}

 		/**
		* Inserto una nueva actividad en el sistema 
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_ActTiempoLibre_store($dato){
			$this->comprobar->ComprobarCaracteres($dato);
			if ($this->get_ActTiempoLibre_store($dato)==0) {
				$sql= 'INSERT INTO act_tiempo_libre("desc_act_t_l") VALUES ('."'".$dato."');";
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die("Ya existe '".$dato."' en la tabla referencial");
			}
		}


		/**
		* Consulto si existe una actividad con las caracteristicas a insertar 
		* @param [array] [actividad]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ActTiempoLibre_store($dato){
			$sql='SELECT * from act_tiempo_libre where "desc_act_t_l"='."'".$dato."'";		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}

		
		/**
		* Consulto la actividad a editar		
		* @param [integer] $id
		* @return [array] [actividades]   
		*/
		public function get_ActTiempoLibre_edit($id){
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "desc_act_t_l", "id_act_t_l" from act_tiempo_libre where "id_act_t_l"='.$id;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["desc_act_t_l"]=strtoupper($filas["desc_act_t_l"]);
				$filas["id"]=$filas["id_act_t_l"];
				$filas["descripcion"]=$filas["desc_act_t_l"];				
				$this->actividades[] =$filas;
			}
			return $this->actividades[0];
		}

		/**
		* Modifico la actividad en el sistema 		
		* @param [array] $dato
		* @return [integer] muestre 1 si es true || [string] El error   
		*/		
		public function set_ActTiempoLibre_update($dato){
			$this->comprobar->ComprobarNumeric($dato["id"]);
			$this->comprobar->ComprobarCaracteres($dato["dato"]);
			if ($this->get_ActTiempoLibre_store($dato["dato"])==0) {
				$sql= 'UPDATE act_tiempo_libre SET "desc_act_t_l"='."'".$dato["dato"]."'".' WHERE "id_act_t_l"='.$dato["id"];
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				return 1;
			}
		}

		/**
		* Elimino la actividad a seleccionar
		* @param [integer] $id
		* @return [integer]  1   
		*/
		public function set_ActTiempoLibre_destroy($id){
			if ($this->get_ActTiempoLibre_destroy($id)>0) {
				$sql= 'DELETE FROM act_tiempo_libre WHERE "id_act_t_l"='.$id;
				if ($this->db->consultar($sql)) {
					return 1;
				}
			} 
			else{
				die('El registro seleccionado no existe');
			}
		}

		/**
		* Consulto si existe la actividad en el sistema
		* @param [integer] $id
		* @return [integer] muestra el nro de registros   
		*/
		private function get_ActTiempoLibre_destroy($id){
			$sql='SELECT "desc_act_t_l", "id_act_t_l" from act_tiempo_libre where "id_act_t_l"='.$id;		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta); 
		}


	}
?>