<?php 
	/**
	* Clase Trabajadores
	* Comparte la tabla Trabajadores en la Base de Datos
	* Es llamado en los Controladores TrabajadoresController, HorariosController, PresupuestoController, ConsultasController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Trabajadores_index 
	*
	* ejem: get_Departamentos_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class Trabajadores 

	{ 
		private $db;
		private $trabajadores;
		private $OperacionesSistemicas;
		private $tabla;	
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] trabajadores
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
		
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->comprobar = new SeguridadDatos;
			$this->db = new conexion;
			$this->trabajadores =array();
			$this->OperacionesSistemicas = new OperacionesSistemicas;
			$this->tabla=3;	//listo			
			 
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] trabajadores
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->trabajadores["IdTrabajador"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["IdTrabajador"]);
			}
			if (isset($this->trabajadores["Persona"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Persona"]);
			}
			if (isset($this->trabajadores["Depto"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Depto"]);
			}
			if (isset($this->trabajadores["TipoPersonal"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["TipoPersonal"]);
			}
			if (isset($this->citas["FechaIngreso"])  ) {
				$this->comprobar->ComprobarFecha($this->citas["FechaIngreso"]);				
			}
			if (isset($this->citas["FechaEgreso"])  ) {
				$this->comprobar->ComprobarFecha($this->citas["FechaEgreso"]);				
			}
			if (isset($this->trabajadores["Estatus"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Estatus"]);
			}
			if (isset($this->trabajadores["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Responsable"]);
			}
		}

		/**
		* Consulto los datos basicos de los trabajadores activos registrados
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [trabajadores]
		*/	
		public function get_Trabajadores_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT u."cedula", t."IdTrabajador", u."sexo", concat(u."nombre", '."' '".', u."nombre2") as "Nombres", concat(u."apellido", '."' '".', u."apellido2") as "Apellidos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" from Trabajadores t, Usuarios u, Departamentos d  where t."Persona"=u."Num_Usuario" and t."Depto"=d."IdDepartamentos" and t."Estatus"=1';		

			$consulta = $this->db->consultar($sql);
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
				$this->trabajadores[] =$filas;
			}
			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method array_OperacionesSistemicas()
			*/
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->trabajadores;

		}

		/**
		* Consulto los datos basicos de los trabajadores asociados a los departamentos del usuario (Jefe de Departamento) iniciado en el sistema
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [string] $depto,  [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [trabajadores]
		*/			
		public function get_Trabajadores_index_Jefe_Depto($depto, $Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);
			$this->comprobar->ComprobarCaracteres($depto);
			$sql='SELECT u."cedula", t."IdTrabajador", u."sexo", concat(u."nombre", '."' '".', u."nombre2") as "Nombres", concat(u."apellido", '."' '".', u."apellido2") as "Apellidos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" from Trabajadores t, Usuarios u, Departamentos d  where t."Persona"=u."Num_Usuario" and t."Depto"=d."IdDepartamentos" and d."IdDepartamentos" in ('.$depto.') and t."Estatus"=1';		

			$consulta = $this->db->consultar($sql);
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
				$this->trabajadores[] =$filas;
			}
			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method array_OperacionesSistemicas()
			*/
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->trabajadores;

		}

		
		/**
		* Inserto un nuevo trabajador en el sistema 
		* @method consultar(), Comprobacion(), get_Trabajadores_store(), get_Trabajadores_store_existe(), get_Trabajadores_store_JefeDepto(),
		* @param [array] $trabajadores
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_Trabajadores_store($trabajadores){ 
			$this->trabajadores = $trabajadores;
			$this->Comprobacion();

			if ($this->get_Trabajadores_store()==0) {
				if ($this->get_Trabajadores_store_existe()==0) {
					if ($this->trabajadores["TipoPersonal"]==5) {
						if ($this->get_Trabajadores_store_JefeDepto()>0) {
							die("Ya existe un Jefe de Depertamento asignado al Depertamento");
						}
					}
					$sql= 'INSERT INTO Trabajadores("Persona", "Depto", "TipoPersonal", "FechaIngreso", "FechaEgreso", "FechaRegistro", "UltModif", "Estatus", "Responsable")
					VALUES (';
					$values=$this->trabajadores["Persona"].", ".$this->trabajadores["Depto"].", ".$this->trabajadores["TipoPersonal"].", '".$this->trabajadores["FechaIngreso"]."', null, now(), now(), 1, ".$this->trabajadores["Responsable"].");";
					$sql=$sql.$values;
					if ($this->db->consultar($sql)) {

						$sql='SELECT "IdTrabajador", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" FROM Trabajadores t, Departamentos d where t."Depto"=d."IdDepartamentos" and t."IdTrabajador"=(SELECT max("IdTrabajador") FROM Trabajadores)';	
						$consulta = $this->db->consultar($sql);
						$filas=pg_fetch_assoc($consulta);

						/**
						* Llamada a la función para registrar quien realizó la consulta
						* @method array_OperacionesSistemicas()
						*/
						$this->OperacionesSistemicas->array_OperacionesSistemicas($this->trabajadores["Responsable"],2,$filas["IdTrabajador"],$this->tabla);

						return "1";
					}
					return "Ha ocurrido un error al interactuar con la base de datos";
				}
				else{
					return 'El trabajador que selecciono ya se encuentra trabajando en el departamento seleccionado';
				}
				
			}
			else{
				return 'Ya existe ese trabajador en ese departamento con esas características';
			}
		}

		/**
		* Consulto si existe esa persona asociada y con el mismo cargo en el departamento 
		* @param [array] [trabajadores]
		* @return [integer] muestra el nro de registros   
		*/

		private function get_Trabajadores_store(){

			$sql='SELECT * FROM Trabajadores where "Depto"='.$this->trabajadores["Depto"].' and "TipoPersonal"='.$this->trabajadores["TipoPersonal"].' and "Persona"='.$this->trabajadores["Persona"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}


		/**
		* Consulto si existe esa persona asociada al departamento 
		* @param [array] [trabajadores]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Trabajadores_store_existe(){

			$sql='SELECT * FROM Trabajadores where "Depto"='.$this->trabajadores["Depto"].' and "Persona"='.$this->trabajadores["Persona"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Consulto si ya existe un jefe de departamento para dicha departamento, en caso de que el trabajador a registrar tenga cargo de jefe de departamento
		* @param [array] [trabajadores]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Trabajadores_store_JefeDepto(){

			$sql='SELECT * FROM Trabajadores where "Depto"='.$this->trabajadores["Depto"].' and "TipoPersonal"=5 and "Estatus"=1';		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		

		/**
		* Consulto un trabajador
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $IdTrabajador, [integer] $Responsable
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Trabajadores_show($IdTrabajador, $Responsable){
			$this->comprobar->ComprobarNumeric($IdTrabajador);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT t."IdTrabajador", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal", t."UltModif", t."FechaRegistro", t."FechaEgreso",(SELECT concat(usu."nombre", '."' '".', usu."apellido")  from Usuarios usu where usu."Num_Usuario"=t."Responsable") as "Responsable", t."Estatus", t."Persona", t."FechaIngreso" from Trabajadores t, Departamentos d  where t."Depto"=d."IdDepartamentos" and t."IdTrabajador"='.$IdTrabajador;

			
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["Estatus"]==0) {
					$filas["Estatus"]='Activo';
				}
				else if ($filas["Estatus"]==1) {
					$filas["Estatus"]='Inactivo';
				}
				if (!empty($filas["FechaEgreso"])) {
					$filas["FechaEgreso"]=date('d-m-Y', strtotime($filas["FechaEgreso"]));
					 
				}
				$filas["FechaIngreso"]=date('d-m-Y', strtotime($filas["FechaIngreso"]));
				$filas["FechaRegistro"]=date('d F Y g:ia', strtotime($filas["FechaRegistro"]));
                $filas["UltModif"]=date('d F Y g:ia', strtotime($filas["UltModif"]));

				$this->trabajadores[]=$filas;
			}
			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method consultar(), array_OperacionesSistemicas()
			*/	
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$IdTrabajador,$this->tabla);
			return $this->trabajadores[0];
		}

					

		/**
		* Consulto el trabajador a editar
		* A diferencia de la funcion get_Trabajadores_show(), esta no posee los arreglos de los campos ni hace llamado a la funcion de array_OperacionesSistemicas()
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdDepartamentos
		* @return [array] [trabajadores]   
		*/
		public function get_consulta_Trabajadores_edit($id){
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT (SELECT concat(usu."nombre", '."' '".', usu."nombre2")  from Usuarios usu where usu."Num_Usuario"=t."Persona") as "NombrePersona", (SELECT concat(usu."apellido", '."' '".', usu."apellido2")  from Usuarios usu where usu."Num_Usuario"=t."Persona") as "ApellidoPersona", (SELECT usu."cedula"  from Usuarios usu where usu."Num_Usuario"=t."Persona") as "CedulaPersona",  t.* FROM Trabajadores t where "IdTrabajador"='.$id;
			
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->trabajadores[] =$filas;
			}
			return $this->trabajadores[0];

		}


		/**
		* Consulto si existe el trabajador con las caracteristicas a modificar que sea ageno al id del registro 
		* @param [array] [trabajadores]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_consulta_Trabajadores_update(){

			$sql='SELECT * FROM Trabajadores where "Depto"='.$this->trabajadores["Depto"].' and "TipoPersonal"='.$this->trabajadores["TipoPersonal"].' and  "FechaIngreso"='."'".$this->trabajadores["FechaIngreso"]."'".' and "IdTrabajador"='.$this->trabajadores["IdTrabajador"];	
				
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		/**
		* Consulto si ya existe un jefe de departamento para dicha departamento, en caso de que el trabajador a modificar tenga cargo de jefe de departamento
		* @param [array] [trabajadores]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Trabajadores_update_JefeDepto(){

			$sql='SELECT * FROM Trabajadores where "Depto"='.$this->trabajadores["Depto"].' and "TipoPersonal"=5 and "Estatus"=1  and "IdTrabajador"!='.$this->trabajadores["IdTrabajador"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Modifico el trabajador en el sistema 
		* @method consultar(), Comprobacion(), get_consulta_Trabajadores_update(), get_Trabajadores_update_JefeDepto()
		* @param [array] $trabajadores
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_Trabajadores_update($trabajadores){ 
			$this->trabajadores = $trabajadores;
			$this->Comprobacion();
			if ($this->get_consulta_Trabajadores_update()==0) {
				if ($this->trabajadores["TipoPersonal"]==5) {
					if ($this->get_Trabajadores_update_JefeDepto()>0) {
						die("Ya existe un Jefe de Depertamento asignado al Depertamento");
					}
				}
				$sql= 'UPDATE Trabajadores SET "Depto"='.$this->trabajadores["Depto"].', "TipoPersonal"='.$this->trabajadores["TipoPersonal"].', "FechaIngreso"='."'".$this->trabajadores["FechaIngreso"]."'".', "UltModif"= now(), "Responsable"='.$this->trabajadores["Responsable"].' WHERE  "IdTrabajador"='.$this->trabajadores["IdTrabajador"];
				
				if ($this->db->consultar($sql)) {
					/**
					* Llamada a la función para registrar quien realizó la consulta
					* @method consultar(), array_OperacionesSistemicas()
					*/	
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->trabajadores["Responsable"],2,$this->trabajadores["IdTrabajador"], $this->tabla);
					return "1";
				}
				return "Ha ocurrido un error al interactuar con la base de datos";
				
			}
			else{
				return '1';
			}
		}


		/**
		* Desactivo el trabajador a seleccionar
		* @method consultar(), ComprobarNumeric(), get_Trabajadores_destroy(), array_OperacionesSistemicas()
		* @param [integer] $IdTrabajador, [integer] $Responsable
		* @return [integer]    
		*/
		public function set_Trabajadores_destroy($IdTrabajador, $Responsable){ 
			$this->comprobar->ComprobarNumeric($IdTrabajador);
			$this->comprobar->ComprobarNumeric($Responsable);
			if ($this->get_Trabajadores_destroy($IdTrabajador)>0) {
				$sql= 'UPDATE Trabajadores SET "Estatus"=0, "FechaEgreso"=now() WHERE "IdTrabajador"='.$IdTrabajador;
				if ($this->db->consultar($sql)) {
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,4,$IdTrabajador, $this->tabla);
					return "1";
				}
				return "Ha ocurrido un error al interactuar con la base de datos";
			}
			else{
				return "Ese trabajador no existe";
			}

		}

		/**
		* Consulto si existe el trabajador en el sistema
		* @param [integer] $IdTrabajador
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Trabajadores_destroy($IdTrabajador){//Anteriormente llamada get_consulta_Trabajadores_destroy
			$sql='SELECT * FROM Trabajadores where "IdTrabajador"='.$IdTrabajador;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}

		
		/**
		* Activo el trabajador a seleccionar
		* @method consultar(), ComprobarNumeric(), set_Trabajadores_activate(), array_OperacionesSistemicas()
		* @param [integer] $IdTrabajador, [integer] $Responsable
		* @return [integer]    
		*/
		public function set_Trabajadores_activate($IdTrabajador, $Responsable){ 
			$this->comprobar->ComprobarNumeric($IdTrabajador);
			$this->comprobar->ComprobarNumeric($Responsable);
			if ($this->get_consulta_Trabajadores_activate($IdTrabajador)>0) {
				$sql= 'UPDATE Trabajadores SET "Estatus"=1, "FechaEgreso"=null WHERE "IdTrabajador"='.$IdTrabajador;
				if ($this->db->consultar($sql)) {
					/**
					* Llamada a la función para registrar quien realizó la consulta
					* @method consultar(), array_OperacionesSistemicas()
					*/	
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,7,$IdTrabajador, $this->tabla);
					return "1";
				}
				return "Ha ocurrido un error al interactuar con la base de datos";
			}
			else{
				return "Ese trabajador no existe";
			}

		}

		/**
		* Consulto si existe el trabajador en el sistema
		* @param [integer] $IdTrabajador
		* @return [integer] muestra el nro de registros   
		*/
		public function get_consulta_Trabajadores_activate($IdTrabajador){
			$sql='SELECT * FROM Trabajadores where "IdTrabajador"='.$IdTrabajador;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}

		/**
		* Consulto los datos basicos de los trabajadores inactivos registrados
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [trabajadores]
		*/	
		public function get_Trabajadores_index_inactivos($Responsable){//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT u."cedula", t."IdTrabajador", u."sexo", concat(u."nombre", '."' '".', u."nombre2") as "Nombres", concat(u."apellido", '."' '".', u."apellido2") as "Apellidos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal", t."FechaEgreso" from Trabajadores t, Usuarios u, Departamentos d  where t."Persona"=u."Num_Usuario" and t."Depto"=d."IdDepartamentos" and t."Estatus"=0';		

			$consulta = $this->db->consultar($sql);
			
			while ($filas=pg_fetch_assoc($consulta)) {
				if (!empty($filas["FechaEgreso"])) {
					$filas["FechaEgreso"]=date('d-m-Y', strtotime($filas["FechaEgreso"]));
					 
				}
				if ($filas["sexo"]==2) {
                    $filas["sexo"]="Masculino";
                }
                else{
                    $filas["sexo"]="Femenino";
                }
				$this->trabajadores[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->trabajadores;

		}

		/**
		* OTRAS CONSULTAS
		* Son consultas llamadas desde otros controladores o clases pero que estan asociadas con la tabla Trabajadores, se agregan en el modelo que posea mas peso para la consulta
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion y el nombre de la funcion del controlador: get_Clase_NombredelControlador_funciondelcontrolador
		*/

		/**
		* Consulto los datos del trabajador en cuanto a su departamento y seda asociado y que este actvio	
		* @param [integer] $Num_Usuario
		* @return [array] [trabajadores]   
		*/
		public function get_Trabajadores_Horarios_create($Num_Usuario)
		{
			$this->comprobar->ComprobarNumeric($Num_Usuario);
			$sql='SELECT t."IdTrabajador", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" from Trabajadores t, Departamentos d  where t."Depto"=d."IdDepartamentos" and t."Persona"='.$Num_Usuario.' and t."Estatus"=1';

			
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {		

				$this->trabajadores[]=$filas;
			}
			return $this->trabajadores;
		}

		/**
		* Consulto los datos basicos de el o los departamentos que esta asociado el trabajador en donde su cargo sea Jefe de departamento y este activo
		* @param [integer] $Persona
		* @return [array] [trabajadores]   
		*/
		public function get_Trabajadores_Jefe_Depto($Persona)
		{	$this->comprobar->ComprobarNumeric($Persona);
			$sql='SELECT d."TipoDepto" as "nrotipodepto", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", t."Depto" FROM trabajadores t, Departamentos d where t."Depto"=d."IdDepartamentos" and  "Persona"='.$Persona.' and "TipoPersonal"=5 and t."Estatus"=1';			
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				die("Error! ese registro no existe en el sistema");
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->trabajadores[]=$filas;
			}
			return $this->trabajadores;
		}


		/**
		* Consulto si estan doctores para el departamento a seleccionar en el dia y la hora actual
		* @param [integer] $departanento
		* @return [array] [trabajadores]   
		*/
		public function get_Trabajador_Consultas_Doctor($departanento){
			//Anteriormente llamado get_Trabajador_Doctor
			$this->comprobar->ComprobarNumeric($departanento);
			setlocale(LC_TIME, 'es_VE');
			date_default_timezone_set('America/Caracas');
			$sql='SELECT (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=t."Persona") as "datopersona", t."Persona"	FROM Departamentos d, Trabajadores t, Horarios h where d."IdDepartamentos"=t."Depto" and h."IdEmpleado"=t."IdTrabajador"  and t."TipoPersonal"=2 and d."IdDepartamentos"='.$departanento.' and h."Dia"='."'".date('N')."'".' and h."HorarioInic"<='."'".date('h:m:s')."'".' and d."TipoDepto" = 3 and h."HorarioFinal">='."'".date('h:m:s')."'".' group by 1,2';
			$conexion = new conexion;			
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->trabajadores[] =$filas;
			}
			return $this->trabajadores;
		}



		/**
		* Consulto los cargos del trabajador
		* @param [integer] $Persona
		* @return [array] [trabajadores]   
		*/
		public function get_Trabajadores_MyUser($Persona)
		{	$this->comprobar->ComprobarNumeric($Persona);
			$sql='SELECT d."TipoDepto" as "nrotipodepto", t."IdTrabajador", (SELECT "Descripcion" from tipopersonal where "IdTipoPersonal"="TipoPersonal") as "TipoPersonal",  (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", t."Depto" FROM trabajadores t, Departamentos d where t."Depto"=d."IdDepartamentos" and  "Persona"='.$Persona.' and t."Estatus"=1';			
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				die("Error! ese registro no existe en el sistema");
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->trabajadores[]=$filas;
			}
			return $this->trabajadores;
		}



 
		


		

		

		



	}
?>