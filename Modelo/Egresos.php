<?php 
	/**
	* Es llamado en BecariosController
	*/

	class Egresos 

	{
		private $db;
		private $egresos;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");			
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->egresos =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->comprobar = new SeguridadDatos;
			$this->tabla=12;//LISTO
			
		}

		private function Comprobacion()
		{ 
			
			if (isset($this->egresos["IdEgresos"])) {
				$this->comprobar->ComprobarNumeric($this->egresos["IdEgresos"]);
			}
			if (isset($this->egresos["Inscripcion"])) {
				$this->comprobar->ComprobarNumeric($this->egresos["Inscripcion"]);
			}
			if (isset($this->egresos["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->egresos["Responsable"]);
			}

			if (isset($this->egresos["descripcion"])) {
				$this->comprobar->ComprobarCaracteres($this->egresos["descripcion"]);
			}
		}

		public function set_Egresos_store($egresos){ //Consulta Registrar Depertamentos
			$this->egresos = $egresos;
			$this->Comprobacion();
			if ($this->get_Egresos_store()==0) {
				$sql= 'INSERT INTO egresos("Inscripcion", "FechaRegistro", "Responsable", descripcion) VALUES ';

				$values='('.$this->egresos["Inscripcion"].', now(), '.$this->egresos["Responsable"].", '".$this->egresos["descripcion"]."')" ;
				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {

					$sql='SELECT max("IdEgresos") as "IdEgresos" FROM egresos limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->egresos["Responsable"],1,$filas["IdEgresos"],$this->tabla);
					return "1";
				}
			}
			else{
				return "Se encuentra registrado el egreso";
			}

			

		}

		private function get_Egresos_store(){
			$sql='SELECT * FROM egresos where "Inscripcion"='.$this->egresos["Inscripcion"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

	}
?>