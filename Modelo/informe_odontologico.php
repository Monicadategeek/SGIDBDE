<?php 

	

	/**
	* 
	*/
	class InformeOdontologico 

	{
		private $db;
		private $informe;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->informe= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=16;//listo
			
		}


			
		private function Comprobacion()
		{
			if (isset($this->informe["id"])) {
				$this->comprobar->ComprobarNumeric($this->informe["id"]);
			}	
			if (isset($this->informe["Secuencia"])) {
				$this->comprobar->ComprobarNumeric($this->informe["Secuencia"]);
			}			
			if (isset($this->informe["AspPac"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["AspPac"]);
			}
			if (isset($this->informe["Cara"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Cara"]);
			}
			if (isset($this->informe["LabyCom"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["LabyCom"]);
			}
			if (isset($this->informe["PalpaGangl"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["PalpaGangl"]);
			}
			if (isset($this->informe["Ganglios"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Ganglios"]);
			}
			if (isset($this->informe["ATM"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["ATM"]);
			}
			if (isset($this->informe["Ojeras"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Ojeras"]);
			}
			if (isset($this->informe["RegHiotTiroidea"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["RegHiotTiroidea"]);
			}
			if (isset($this->informe["Carrillos"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Carrillos"]);
			}
			if (isset($this->informe["Mucosa"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Mucosa"]);
			}
			if (isset($this->informe["Encia"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Encia"]);
			}
			if (isset($this->informe["Lengua"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Lengua"]);
			}
			if (isset($this->informe["Paladar"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Paladar"]);
			}
			if (isset($this->informe["Laboratorio"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Laboratorio"]);
			}
			if (isset($this->informe["Modelo"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Modelo"]);
			}
			if (isset($this->informe["TensionArterial"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["TensionArterial"]);
			}
			if (isset($this->informe["Observaciones"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Observaciones"]);
			}
			if (isset($this->informe["Diagnostico"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Diagnostico"]);
			}
			if (isset($this->informe["Tratamiento"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["Tratamiento"]);
			}

			if (isset($this->informe["DescrTrat"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["DescrTrat"]);
			}
			if (isset($this->informe["examen"])) {
				$this->comprobar->ComprobarCaracteres($this->informe["examen"]);
			}			
		}

		public function get_InformeOdontologico_index($Responsable){
			$sql="";
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->informe[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->informe;

		}

		
		public function set_InformeOdontologico_store($informe){ //Consulta Registrar Depertamentos
			$this->informe = $informe;
			$this->Comprobacion();
			if ($this->get_InformeOdontologico_store()==0) {
				$sql='INSERT INTO informe_odontologico("id","AspPac", "Cara", "LabyCom", "PalpaGangl", "Ganglios", "ATM", "Ojeras", "RegHiotTiroidea", "Carrillos", "Mucosa", "Encia", "Lengua", "Paladar", "Laboratorio", "Modelo", "TensionArterial", "Observaciones", "Diagnostico", "Tratamiento", "DescrTrat", "examen", "odontograma")';

	            $values="VALUES (".$this->informe["CodInforme"].", '".$this->informe["AspPac"]."', '".$this->informe["Cara"]."', '".$this->informe["LabyCom"]."', '".$this->informe["PalpaGangl"]."', '".$this->informe["Ganglios"]."', '".$this->informe["ATM"]."', '".$this->informe["Ojeras"]."', '".$this->informe["RegHiotTiroidea"]."', '".$this->informe["Carrillos"]."', '".$this->informe["Mucosa"]."', '".$this->informe["Encia"]."', '".$this->informe["Lengua"]."', '".$this->informe["Paladar"]."', '".$this->informe["Laboratorio"]."', '".$this->informe["Modelo"]."', '".$this->informe["TensionArterial"]."', '".$this->informe["Observaciones"]."', '".$this->informe["Diagnostico"]."', '".$this->informe["Tratamiento"]."', '".$this->informe["DescrTrat"]."', '".$this->informe["examen"]."', '".$this->informe["odontograma"]."')";
	             $sql=$sql.$values;
				if ($this->db->consultar($sql)) {
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->informe["Responsable"],1,$this->informe["CodInforme"],$this->tabla);	
					return "1";
				}
				else{
					return "2";
				}
			}	

			
		}

		private function get_InformeOdontologico_store(){
			$sql='SELECT * FROM informe_odontologico where "id"='.$this->informe["CodInforme"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}


		public function get_InformeOdontologico_show($id){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT * FROM informe_odontologico  where "id"='.$id;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				

				$this->informe[] =$filas;
			}

			

			return $this->informe[0];

			
		}

		public function set_InformeOdontologico_list($IdEstudiante){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($IdEstudiante);

			$sql='SELECT c."Fecha", c."Hora", c."IdCitas", io."DescrTrat", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable" FROM Citas c, informes i, informe_odontologico io where c."IdCitas"=i."CodInforme" and c."IdCitas"=io."id" and c."Asistencia"=2 and i."TipoInforme"=3 and c."IdEstudiante"='.$IdEstudiante;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));
				$filas["boton"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$filas["IdCitas"].');"><i class="fa fa-book"></i></button>';

				$this->informe[] =$filas;
			}

			

			return $this->informe;

			
		}

		

		
	
	

		

		

		



	}
?>