
<?php 

	/**
	* 
	*/
	class FechasEncuestas 

	{
		
		private $db;
		private $feechasencuestas;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;

			$this->feechasencuestas= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=7;
			
		}

		private function Comprobacion()
		{
			if (isset($this->feechasencuestas["id"])) {
				$this->comprobar->ComprobarNumeric($this->feechasencuestas["id"]);
			}

			if (isset($this->feechasencuestas["Fechainicio"])) {
				$this->comprobar->ComprobarFecha($this->feechasencuestas["Fechainicio"]);
			}

			if (isset($this->feechasencuestas["Fechafin"])  ) {
				$this->comprobar->ComprobarFecha($this->feechasencuestas["Fechafin"]);				
			}
			
			if (isset($this->feechasencuestas["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->feechasencuestas["Responsable"]);
			}

			
		}

		public function get_FechasEncuestas_Ult(){
			$sql='SELECT f."Fechainicio", f."Fechafin" FROM FechasEncuestas f where f."Fechainicio"=(SELECT max(fe."Fechainicio") FROM FechasEncuestas fe) ';			
			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->feechasencuestas[]=$filas;
				}
				return $this->feechasencuestas[0];
			}
			else{
				return $this->feechasencuestas;
			}
		}


		public function get_FechasEncuestas_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);

			$sql='SELECT f."Fechainicio", f."Fechafin", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=f."Responsable") as "Responsable", f."Id", f."FechaRegistro" FROM fechasencuestas f order by f."Fechainicio" desc';	

			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->feechasencuestas[]=$filas;
			}
			
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			
			return $this->feechasencuestas;

		}
	
		public function set_FechasEncuestas_store($feechasencuestas){ //Consulta Registrar Depertamentos
			$this->feechasencuestas = $feechasencuestas;
			$this->Comprobacion();

			if ($this->get_FechasEncuestas_store()==0) {
				$sql= 'INSERT INTO FechasEncuestas("Fechainicio", "Fechafin", "Responsable", "FechaRegistro") VALUES (';

				$values="'".$this->feechasencuestas["Fechainicio"]."', '".$this->feechasencuestas["Fechafin"]."', ".$this->feechasencuestas["Responsable"].", now());";
				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {


					$sql='SELECT max("Id") as id FROM FechasEncuestas limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->feechasencuestas["Responsable"],1,$filas["id"],$this->tabla);
					return "1";
				}
			}
			else{
				return 'Ya existe un registro que abarca parcial o total el rango de fecha';
			}
		}

		private function get_FechasEncuestas_store(){
			$sql='SELECT * FROM FechasEncuestas f  where f."Fechainicio">='."'".$this->feechasencuestas["Fechainicio"]."'".' and f."Fechafin">='."'".$this->feechasencuestas["Fechafin"]."'";
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		public function get_FechasEncuestas_edit($id){
			$this->comprobar->ComprobarNumeric($id);

			$sql='SELECT f."Fechainicio", f."Fechafin", f."Id" FROM fechasencuestas f where f."Id"='.$id;	
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->feechasencuestas[]=$filas;
			}
			return $this->feechasencuestas[0];
		}

		public function set_FechasEncuestas_update($feechasencuestas){ //Consulta Registrar Depertamentos
			$this->feechasencuestas = $feechasencuestas;
			$this->Comprobacion();

			if ($this->get_FechasEncuestas_update()==0) {
				$sql= 'UPDATE FechasEncuestas SET "Fechainicio"='."'".$this->feechasencuestas["Fechainicio"]."'".', "Fechafin"='."'".$this->feechasencuestas["Fechafin"]."'".' WHERE "Id"='.$this->feechasencuestas["Id"];

				if ($this->db->consultar($sql)) {
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->feechasencuestas["Responsable"],2,$feechasencuestas["Id"],$this->tabla);
					return "1";
				}
				else{
					return "Ha ocurrido un error al interactuar con la base de datos";
				}
			}
			else{
				return 'Ya existe un registro que abarca parcial o total el rango de fecha';
			}
		}

		private function get_FechasEncuestas_update(){
			 $sql='SELECT * FROM FechasEncuestas f  where f."Fechainicio">='."'".$this->feechasencuestas["Fechainicio"]."'".' and f."Fechafin">='."'".$this->feechasencuestas["Fechafin"]."'".' and f."Id"!='.$this->feechasencuestas["Id"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}


		#OTROS

		public function get_Estadisticas_FechasEncuestas_fecha(){

			$sql='SELECT f."Fechainicio", f."Fechafin" FROM fechasencuestas f order by f."Fechainicio" desc';	

			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fechainicio"]=date('d-m-Y', strtotime($filas["Fechainicio"]));
				$filas["Fechafin"]=date('d-m-Y', strtotime($filas["Fechafin"]));

				$this->feechasencuestas[]=$filas;
			}
					
			return $this->feechasencuestas;

		}

		public function get_Estadisticas_FechasEncuestas_fecha_maxmin(){

			$sql='SELECT min(f."Fechainicio") as "Fechainicio", max(f."Fechafin") as "Fechafin" FROM fechasencuestas f';
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fechainicio"]=date('Y-m-d', strtotime($filas["Fechainicio"]));
				$filas["Fechafin"]=date('Y-m-d', strtotime($filas["Fechafin"]));
				
				$this->feechasencuestas[]=$filas;
			}		

			if($this->feechasencuestas[0]["Fechafin"]>date('Y-m-d')){
				$this->feechasencuestas[0]["Fechafin"]=date('Y-m-d');
			}
			$start_ts = strtotime($this->feechasencuestas[0]["Fechainicio"]); 
			$end_ts = strtotime($this->feechasencuestas[0]["Fechafin"]); 
			$diff = $end_ts - $start_ts; 
			return  round($diff / 86400);

		}
		
	}