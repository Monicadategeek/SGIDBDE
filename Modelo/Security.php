<?php 
	/**
	* 
	*/
	class Security 

	{
		private $db;
		private $datos;
		private $comprobar;
		private $tabla;


		public function __construct()
		{	

			require_once(dirname(__FILE__) ."/../Controller/conectar.php");
			require_once(dirname(__FILE__) ."/SeguridadDatos.php");
			$this->db = new conexion;

			$this->datos =array();			
			$this->comprobar = new SeguridadDatos;
			$this->tabla=1;
			
		}

		public function get_Usuarios($usuario){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarNumeric($usuario);
			$sql='SELECT U."Num_Usuario", concat(u."nombre", '."' '".', u."nombre2") as "Nombres", concat(u."apellido", '."' '".', u."apellido2") as "Apellidos", u."Id_Tipo_Usuario", u."clave", u."Cod_Estatus", u."Foto_Usuario" FROM usuarios u  where u."cedula"='.$usuario;		

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					if ($filas["Foto_Usuario"]!=0) {
					$filas["Foto_Usuario"]="../web/images/fotos/".$filas["Foto_Usuario"];
					}
					else{
						$filas["Foto_Usuario"]="../web/images/user.png";
					}

					$this->datos[] =$filas;
				}
				return $this->datos[0];

			}
			return 0;
			
		}

		public function get_Usuarios_Estudiantes($usuario){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarNumeric($usuario);

			$sql='SELECT u."num_usu_e", e."nombres" as "Nombres", e."apellidos" as "Apellidos", u."clave", u."estatus", e."foto"

			FROM usu_estudiantes u, estudiantes e

			 where u."num_est"=e."num_est" and u."usuario"='."'".$usuario."'";		

			$consulta = $this->db->consultar($sql);

			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					if ($filas["foto"]!=0) {
					$filas["foto"]="../../../siace/carnet/fotos/".$filas["foto"];
					}
					else{
						$filas["foto"]="../web/images/user.png";
					}

					$this->datos[] =$filas;
				}
				return $this->datos[0];

			}
			return 0;
		}

		public function get_Depto_usuario($usuario){//Consulta del Depertamentos Index

			$sql='SELECT "Depto", "TipoPersonal", (SELECT d."TipoDepto" from departamentos d where d."IdDepartamentos"=t."Depto") as "TipoDepto" FROM Trabajadores t where t."Persona"='.$usuario;
			$conexion = new conexion;			
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->datos[] =$filas;
			}
			return $this->datos;
		}

		public function get_usuario_correo($usuario){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarCaracteres($usuario);
			 $sql='SELECT u."Num_Usuario", concat(u."nombre", '."' '".', u."nombre2",'."' '".', u."apellido",'."' '".', u."apellido2") as "Nombres", u.cedula FROM usuarios u  where u."correo"='."'".$usuario."'";		

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->datos[] =$filas;
				}
				return $this->datos[0];

			}
			return 0;
			
		}

		public function get_usuario_estudiante_correo($usuario){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarCaracteres($usuario);
			 $sql='SELECT concat(e."nombres", '."' '".', e."apellidos") as "Nombres", e.cedula, ue."num_usu_e" FROM estudiantes e,  usu_estudiantes ue where  ue."num_est"=e."num_est" and e."correo"='."'".$usuario."'";		

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->datos[] =$filas;
				}
				return $this->datos[0];

			}
			return 0;
			
		}

		public function set_usuario_estudiante_update($num, $clave){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarNumeric($num);
			$this->comprobar->ComprobarCaracteres($clave);
			$sql= 'UPDATE usu_estudiantes SET "clave"='."'".$clave."'".' WHERE  "num_usu_e"='.$num;
				
			if ($this->db->consultar($sql)) {
				return "1";
			}
			return "Intentelo más tarde";
			
		}

		public function set_usuario_update($num, $clave){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarNumeric($num);
			$this->comprobar->ComprobarCaracteres($clave);
			$sql= 'UPDATE usuarios SET "clave"='."'".$clave."'".' WHERE  "Num_Usuario"='.$num;
				
			if ($this->db->consultar($sql)) {
				return "1";
			}
			return "Intentelo más tarde";
			
		}



		
		

		

		



	}
?>