<?php 

	

	/**
	* 
	*/
	class AntecedentesMedicos 

	{
		private $db;
		private $antecedentes;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->antecedentes= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=6;
			
		}


		private function Comprobacion()
		{
			
			if (isset($this->antecedentes["idAntecedente"])) {
				$this->comprobar->ComprobarNumeric($this->antecedentes["idAntecedente"]);
			}

			if (isset($this->antecedentes["antecfamiliares"])) {
				$this->comprobar->ComprobarCaracteres($this->antecedentes["antecfamiliares"]);
			}

			if (isset($this->antecedentes["antpersonales"])) {
				$this->comprobar->ComprobarCaracteres($this->antecedentes["antpersonales"]);
			}

			if (isset($this->antecedentes["nota"])) {
				$this->comprobar->ComprobarCaracteres($this->antecedentes["nota"]);
			}

			if (isset($this->antecedentes["estudiante"])) {
				$this->comprobar->ComprobarNumeric($this->antecedentes["estudiante"]);
			}

			if (isset($this->antecedentes["responsableescritura"])) {
				$this->comprobar->ComprobarNumeric($this->antecedentes["responsableescritura"]);
			}

			if (isset($this->antecedentes["responsable"])) {
				$this->comprobar->ComprobarNumeric($this->antecedentes["responsable"]);
			}

			
			
		}

		public function get_AntecedentesMedicos_index($estudiante, $Responsable){
			$this->comprobar->ComprobarNumeric($estudiante);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT a."idAntecedente", a."antecfamiliares", a."antpersonales", a."nota", 
      		a."ultmodificacion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=a."responsable") as "responsable"  FROM antecedentesmedicos a where a."estudiante"='.$estudiante;
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["ultmodificacion"]=date('d-m-Y h:i a', strtotime($filas["ultmodificacion"]));
				$this->antecedentes[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			
			if (count($this->antecedentes)>0) {
				return $this->antecedentes[0];
			}
			return $this->antecedentes;

			
		}

		public function get_AntecedentesMedicos_num($estudiante){
			$this->comprobar->ComprobarNumeric($estudiante);

			$sql='SELECT * FROM antecedentesmedicos where estudiante='.$estudiante;
			$consulta = $this->db->consultar($sql);
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Ha ocurrido un error, al interactuar con la base de datos"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}

		
	


		public function set_AntecedentesMedicos_store($antecedentes){ //Consulta Registrar Depertamentos
			$this->antecedentes = $antecedentes;
			$this->Comprobacion();
			if ($this->get_AntecedentesMedicos_store()==0) {
				$sql='INSERT INTO antecedentesmedicos("antecfamiliares", "antpersonales", "nota", "estudiante", "ultmodificacion", "responsable") VALUES ('."'".$this->antecedentes["antecfamiliares"]."'".', '."'".$this->antecedentes["antpersonales"]."'".', '."'".$this->antecedentes["nota"]."'".', '.$this->antecedentes["estudiante"].', now(), '.$this->antecedentes["responsable"].');';

				if ($this->db->consultar($sql)) {	
					return "1";
				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			
		}

		private function get_AntecedentesMedicos_store(){
			$sql='SELECT * FROM antecedentesmedicos where estudiante='.$this->antecedentes["estudiante"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		



		public function get_AntecedentesMedicos_edit($idAntecedente){
			$this->comprobar->ComprobarNumeric($idAntecedente);

			$sql='SELECT * FROM antecedentesmedicos a inner join estudiantes e on e."num_est"=a."estudiante" where "idAntecedente"='.$idAntecedente;
			$consulta = $this->db->consultar($sql);
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Ha ocurrido un error, al interactuar con la base de datos"));
			}		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->antecedentes[] =$filas;
			}			
			return $this->antecedentes[0];

		}

		public function get_AntecedentesMedicos_show($idAntecedente){
			$this->comprobar->ComprobarNumeric($idAntecedente);

			$sql='SELECT *, (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"="responsable") as "responsable"  FROM antecedentesmedicos where "idAntecedente"='.$idAntecedente;
			$consulta = $this->db->consultar($sql);
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Ha ocurrido un error, al interactuar con la base de datos"));
			}		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				
				$filas["ultmodificacion"]=date('d-m-Y', strtotime($filas["ultmodificacion"]));
				$this->antecedentes[] =$filas;
			}			
			return $this->antecedentes[0];

		}
		
	
	

		public function set_AntecedentesMedicos_update($antecedentes){ //Consulta Registrar Depertamentos
			$this->antecedentes = $antecedentes;
			$this->Comprobacion();
				$sql='UPDATE antecedentesmedicos SET antecfamiliares='."'".$this->antecedentes["antecfamiliares"]."'".', antpersonales='."'".$this->antecedentes["antpersonales"]."'".', nota='."'".$this->antecedentes["nota"]."'".', ultmodificacion=now(), responsable='.$this->antecedentes["responsable"].'
 				WHERE "idAntecedente"='.$this->antecedentes["idAntecedente"];

				if ($this->db->consultar($sql)) {	
					return "1";
				}
				else{
					die('Error al interactuar con la base de datos');
				}
			
		}

		

		



	}
?>