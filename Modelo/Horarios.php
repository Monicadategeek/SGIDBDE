<?php
	 /**
	* Clase Trabajadores
	* Comparte la tabla Trabajadores en la Base de Datos
	* Es llamado en los Controladores TrabajadoresController, CitasController, GestionCitasController 
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Trabajadores_index 
	*
	* ejem: get_Departamentos_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/

	class Horarios 
	{
		private $db;
		private $horarios;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;
 
		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] horarios
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->comprobar = new SeguridadDatos;
			$this->db = new conexion;
			$this->horarios =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=17;//LISTO
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] horarios
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->trabajadores["IdHorarios"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["IdHorarios"]);
			}
			if (isset($this->trabajadores["IdEmpleado"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["IdEmpleado"]);
			}
			if (isset($this->trabajadores["Dia"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Dia"]);
			}

			if (isset($this->citas["HorarioInic"])) {
				$this->comprobar->ComprobarFechaHora($this->citas["HorarioInic"], 'H:i');
			}
			if (isset($this->citas["HorarioFinal"])) {
				$this->comprobar->ComprobarFechaHora($this->citas["HorarioFinal"], 'H:i');
			}

			if (isset($this->trabajadores["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->trabajadores["Responsable"]);
			}
		}

		/**
		* Consulto los horarios de una persona en una o mas departamentos activos
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [array] $trabajadores 
		* @return [array] [horarios]
		*/	
		public function get_Horarios($trabajadores)
		{

			foreach ($trabajadores as $trabajador) {
				$this->comprobar->ComprobarNumeric($trabajador["IdTrabajador"]);
				$sql='SELECT h.*, t."IdTrabajador", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" from Horarios h, Departamentos d, Trabajadores t  where t."Depto"=d."IdDepartamentos" and h."IdEmpleado"=t."IdTrabajador" and h."IdEmpleado"='.$trabajador["IdTrabajador"];
				$consulta = $this->db->consultar($sql);

				while ($filas=pg_fetch_assoc($consulta)) {
					$this->horarios[] =$filas;
				}
				
			}

			return $this->horarios;
		}

		/**
		* Inserto un nuevo horario en el sistema 
		* @param [array] $trabajadores
		* @return [integer] muestra IdHorarios
		*/
		public function set_Horarios_store($horarios){ 
			$this->horarios = $horarios;
			$this->Comprobacion();
			$sql= 'INSERT INTO Horarios("IdEmpleado", "Dia", "HorarioInic", "HorarioFinal", "Responsable") VALUES (';
				$values=$this->horarios["IdEmpleado"].", ".$this->horarios["Dia"].", '".$this->horarios["HorarioInic"]."', '".$this->horarios["HorarioFinal"]."', ".$this->horarios["Responsable"].");";
				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {

					$sql='SELECT max("IdHorarios") as "IdHorarios" FROM Horarios limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->horarios["Responsable"],1,$filas["IdHorarios"],$this->tabla);

					return $filas["IdHorarios"];
				}
		}

		/**
		* Modifico el horario en el sistema 
		* @method consultar(), Comprobacion(), array_OperacionesSistemicas()
		* @param [array] $horarios
		* @return [integer] muestre 1 si es true  
		*/	
		public function set_Horarios_update($horarios){ 
			$this->horarios = $horarios;
			$this->Comprobacion();
			$sql= 'UPDATE Horarios SET "Dia"='.$this->horarios["Dia"].', "HorarioInic"='."'".$this->horarios["HorarioInic"]."'".', "HorarioFinal"='."'".$this->horarios["HorarioFinal"]."'".', "Responsable"='.$this->horarios["Responsable"].' WHERE "IdHorarios"='.$this->horarios["IdHorarios"];

				if ($this->db->consultar($sql)) {

					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->horarios["Responsable"],2,$this->horarios["IdHorarios"],$this->tabla);

					return "1";
				}
		}

		/**
		* Elimino el horario a seleccionar
		* @method consultar(), ComprobarNumeric(),array_OperacionesSistemicas()
		* @param [integer] $IdDepartamentos, [integer] $Responsable
		* @return [integer]    
		*/
		public function set_Horarios_destroy($id, $Responsable){ //Consulta Registrar Depertamentos
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql= 'DELETE FROM Horarios WHERE "IdHorarios"='.$id;
			if ($this->db->consultar($sql)) {
				$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$id,$this->tabla);
				return "1";
			}

		}
		

		/**
		* OTRAS CONSULTAS
		* Son consultas llamadas desde otros controladores o clases pero que estan asociadas con la tabla Horarios, se agregan en el modelo que posea mas peso para la consulta
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion y el nombre de la funcion del controlador: get_Clase_NombredelControlador_funciondelcontrolador
		*/
		

		/**
		* Consulto la hora de inicio y fin del trabajador sea Odontologo (1), Doctor (2), Psicologo (3) y Trabajador Social (6) en el departamento y dia seleccionado	
		* @param [integer] $depto, [integer] $dia	
		* @return [array] [horarios]   
		*/						

		public function get_Horarios_Citas_Solicitud($depto, $dia){

			$sql='SELECT h."HorarioFinal", h."HorarioInic", (SELECT tp."Descripcion" FROM tipopersonal tp WHERE tp."IdTipoPersonal"= t."TipoPersonal") as "TipoProfesional", u."nombre", u."apellido", t."IdTrabajador"  FROM Trabajadores t, Horarios h, Usuarios u where t."Depto"='.$depto.' and h."Dia"='.$dia.' and u."Num_Usuario"=t."Persona" and  t."IdTrabajador"=h."IdEmpleado" and t."Estatus"=1 and t."TipoPersonal" in (1,2,3,6)';
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) 
			{
				$this->horarios[] =$filas;
			}
			return $this->horarios;

		}
		

	}


	?>