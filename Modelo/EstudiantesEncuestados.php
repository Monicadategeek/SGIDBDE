
<?php 

	/**
	* CitasController
	*/
	class EstudiantesEncuestados 

	{
		
		private $db;
		private $encuestas;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Modelo/OperacionesSistemicas.php");
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;

			$this->encuestas= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=7;
			
		}
		private function Comprobacion()
		{
			if (isset($this->encuestas["estudiante"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["estudiante"]);
			}

			if (isset($this->encuestas["fecha"])) {
				$this->comprobar->ComprobarFecha($this->encuestas["fecha"]);
			}

			if (isset($this->encuestas["socioeconomicos"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["socioeconomicos"]);
			}

			if (isset($this->encuestas["ambientales"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["ambientales"]);
			}

			if (isset($this->encuestas["culturales"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["culturales"]);
			}

			if (isset($this->encuestas["interesAyu"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["interesAyu"]);
			}
			if (isset($this->encuestas["miembrosfamiliares"])) {
				$this->comprobar->ComprobarNumeric($this->encuestas["miembrosfamiliares"]);
			}
			if (!isset($this->encuestas["cita"])) {
				$this->encuestas["cita"]="null";
			}

		

			
		}

		public function get_FechasEncuestas($ultfecha){
			$sql='SELECT * FROM estudiantesencuestados e where e."estudiante"='.$ultfecha["estudiante"].' and e."Id"=(SELECT max("Id") from estudiantesencuestados where e."estudiante"='.$ultfecha["estudiante"].') and e."fecha">='."'".$ultfecha["Fechainicio"]."'".' or e."interesAyu" is null and e."culturales" is null';			

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->encuestas[]=$filas;
				}
				return $this->encuestas[0];
			}
			else{
				return $this->encuestas;
			}
		}





		public function set_FechasEncuestas($encuestas){ //Consulta Registrar Depertamentos
			$this->encuestas = $encuestas;
			$this->Comprobacion();

			
				$sql= 'INSERT INTO estudiantesencuestados("fecha", "estudiante", "socioeconomicos") VALUES (';

				$values="now(), ".$this->encuestas["estudiante"].", ".$this->encuestas["socioeconomicos"]." );";
				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {


					$sql='SELECT max("Id") as "id" FROM estudiantesencuestados limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					return $filas["id"];
				}

				return "Ha ocurrido un error al interactuar con la base de datos";
			
		}

		public function set_FechasEncuestas_update_column($encuestas){ //Consulta Registrar Depertamentos
			$this->encuestas = $encuestas;
			$this->Comprobacion();			
			$sql= 'UPDATE estudiantesencuestados SET "'.$this->encuestas["campo"].'"='.$this->encuestas[$this->encuestas["campo"]].', cita='.$this->encuestas["cita"].' WHERE "Id" ='.$this->encuestas["Id"];

			if ($this->db->consultar($sql)) {
				
				return "1";
			}

			return "Ha ocurrido un error al interactuar con la base de datos";
			
		}

		public function get_EstudiantesEncuestados($id)
		{ 
			$this->comprobar->ComprobarNumeric($id);

			$sql= 'SELECT * FROM estudiantesencuestados WHERE cita ='.$id;

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->encuestas[]=$filas;
				}
				return $this->encuestas[0];
			}
			else{
				return $this->encuestas;
			}			
		}

		public function get_EstudiantesEncuestados_Id($id)
		{ 
			$this->comprobar->ComprobarNumeric($id);

			 $sql= 'SELECT * FROM estudiantesencuestados WHERE "Id" ='.$id;

			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				while ($filas=pg_fetch_assoc($consulta)) {
					$this->encuestas[]=$filas;
				}
				return $this->encuestas[0];
			}
			else{
				return $this->encuestas;
			}			
		}


	

		#OTROS

		public function get_Estadisticas_Estudiantes_total($inicio, $fin){
			$this->comprobar->ComprobarFecha($inicio);
			$this->comprobar->ComprobarFecha($fin);
			$sql='SELECT * FROM estudiantesencuestados where "fecha">='."'".$inicio."'".' and "fecha"<='."'".$fin."'";
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}
	

		public function get_Citas_Encuesta_Estudiantes($estudiante, $cita){
			$fechaActual = date('Y-m-d');
			$fechaMesPasado = strtotime ('-1 month', strtotime($fechaActual));
			$fechaMesPasadoDate = date('Y-m-d', $fechaMesPasado);

			$sql='SELECT e."Id" FROM estudiantesencuestados e where e."estudiante"='.$estudiante.' and e."fecha">='."'".$fechaMesPasadoDate."'".' and e."cita" is null';

			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->encuestas[]=$filas;
			}
			if (count($this->encuestas)>0) {
				$this->set_AgregarCita($this->encuestas[0]["Id"], $cita);
			}
			return 1;


		}


		private function set_AgregarCita($idencuesta, $cita){
			$sql= 'UPDATE estudiantesencuestados SET cita='.$cita.' WHERE "Id" ='.$idencuesta;

			if ($this->db->consultar($sql)) {
				
				return "1";
			}

		}



	}

	
		