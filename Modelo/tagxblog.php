<?php 

	

	/**
	* 
	*/
	class TagxBlog 

	{
		private $db;
		private $tagxblog;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->tagxblog= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=6;
			
		}


			
		private function Comprobacion()
		{
			
			
		}

		public function get_TagxBlog($blog){
			$this->comprobar->ComprobarNumeric($blog);
			
			$sql='SELECT "Tag" FROM tagxblog where "Articulo"='.$blog;
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tagxblog[] =$filas;
			}
			
			return $this->tagxblog;

		}

		
	


		public function set_TagxBlog_store($tagxblog){ //Consulta Registrar Depertamentos
			$this->tagxblog = $tagxblog;
			$this->comprobar->ComprobarNumeric($this->tagxblog["Id"]);
			$sql='INSERT INTO tagxblog("Tag", "Articulo")  VALUES ';
 			$values='';
			for ($i=0; $i < count($this->tagxblog["tags"]) ; $i++) { 
				$values=$values.'('.$this->tagxblog["tags"][$i].', '.$this->tagxblog["Id"].'),';
					
			}				
			$values = substr($values, 0, -1);
			$sql=$sql.$values.';';
			if ($this->db->consultar($sql)) {				
				return "1";
			}
			else{
				return "2";
			}			
		}


		public function set_TagxBlog_delete($blog){ 
			$this->comprobar->ComprobarNumeric($blog);
			$sql='DELETE FROM tagxblog WHERE "Articulo" ='.$blog;
 			
			if ($this->db->consultar($sql)) {				
				return "1";
			}
			else{
				return "2";
			}			
		}

		public function set_TagxBlog_show($blog){ 
			$this->comprobar->ComprobarNumeric($blog);
			$sql='SELECT (SELECT "Nombre" from tag where "Id"="Tag") as "Tag"  FROM tagxblog where "Articulo"='.$blog;
			$consulta = $this->db->consultar($sql);

 			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tagxblog[] =$filas;
			}
			
			return $this->tagxblog;
						
		}


		public function get_TagxBlog_principal(){ 
			$sql='SELECT (SELECT "Nombre" from tag where "Id"="Tag") as "Descripcion", "Tag", count(*) as "total"  FROM tagxblog group by "Tag"';
			$consulta = $this->db->consultar($sql);

 			while ($filas=pg_fetch_assoc($consulta)) {
				$this->tagxblog[] =$filas;
			}
			
			return $this->tagxblog;
						
		}



		


		

		
	
	

		

		

		



	}
?>