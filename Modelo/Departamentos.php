<?php 
	/**
	* Clase Departamentos
	* Comparte la tabla Departamentos en la Base de Datos
	* Es llamado en los Controladores DepartamenosController, UsuariosController, TrabajadoresController,  CitasController, GestionCitasController, CancelarCitasController, BecariosController
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Clase_funciondelcontrolador 
	*
	* ejem: get_Departamentos_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
	class Despartamentos 

	{
		private $db;
		private $departamentos;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;

		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] departamentos
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
 
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->departamentos =array();		
			$this->comprobar = new SeguridadDatos;	
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=1;//LISTO 
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] departamentos
		 * 
		 */

		private function Comprobacion()
		{
			
			if (isset($this->departamentos["IdDepartamentos"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["IdDepartamentos"]);
			}
			if (isset($this->departamentos["TipoDepto"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["TipoDepto"]);
			}
			if (isset($this->departamentos["Sede"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["Sede"]);
			}
			if (isset($this->departamentos["AsistenciEstud"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["AsistenciEstud"]);
			}
			if (isset($this->departamentos["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["Responsable"]);
			}
			if (isset($this->departamentos["Estatus"])) {
				$this->comprobar->ComprobarNumeric($this->departamentos["Estatus"]);
			}	
			if (isset($this->departamentos["informacion"])) {
				$this->comprobar->ComprobarCaracteres($this->departamentos["informacion"]);
			}
			if (empty($this->departamentos["informacion"])) {
				$this->departamentos["informacion"]=0;
			}

		}



		/**
		* Consulto los datos basicos de los departamentos registrados
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $Responsable para la funcion array_OperacionesSistemicas()
		* @return [array] [departamentos]
		*/	
		public function get_Departamentos_index($Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT (SELECT count(*) from trabajadores trab where trab."Depto"=d."IdDepartamentos") as "Depto", d."IdDepartamentos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", d."AsistenciEstud", d."UltModif", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=d."Responsable") as "Responsable", d."Estatus" FROM Departamentos d';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				if ($filas["AsistenciEstud"]==1) {
                    $filas["AsistenciEstud"]="Si";
                }
                else{
                    $filas["AsistenciEstud"]="No";
                }
                $filas["UltModif"]=date('d-m-Y', strtotime($filas["UltModif"]));
                 if ($filas["Estatus"]==1) {
                    $filas["Estatus"]="Activo";
                }
                else{
                    $filas["Estatus"]="Inactivo";
                }

				$this->departamentos[] =$filas;
			}

			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method array_OperacionesSistemicas()
			*/	
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->departamentos;
		}


		/**
		* Inserto un nuevo departamento en el sistema 
		* @method consultar(), Comprobacion(), get_Departamentos_store(), array_OperacionesSistemicas()
		* @param [array] $departamentos
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_Departamentos_store($departamentos){ //Consulta Registrar Depertamentos
			$this->departamentos = $departamentos;
			$this->Comprobacion();

			if ($this->get_Departamentos_store()==0) {
				
				$sql= 'INSERT INTO Departamentos("TipoDepto", "Sede", "AsistenciEstud", "FechaRegistro", "UltModif", "Responsable", "Estatus", "informacion") VALUES (';
				$values=$this->departamentos["TipoDepto"].", ".$this->departamentos["Sede"].", ".$this->departamentos["AsistenciEstud"].", now(), now(),".$this->departamentos["Responsable"].", 1, '".$this->departamentos["informacion"]."');";

				$sql=$sql.$values;

				if ($this->db->consultar($sql)) {

					$sql='SELECT max("IdDepartamentos") as "IdDepartamentos" FROM Departamentos limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->departamentos["Responsable"],1,$filas["IdDepartamentos"],$this->tabla);

					return "1";
				}
				else{
					return "Ha ocurrido un error al interactuar con la base de datos";
				}
			}
			else{
				return 'Ya existe un departamento con esas características';
			}
		}

		/**
		* Consulto si existe un departamento con las caracteristicas a insertar 
		* @param [array] [departamentos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Departamentos_store(){

			$sql='SELECT * FROM Departamentos where "TipoDepto"='.$this->departamentos["TipoDepto"].' and ("Sede"='.$this->departamentos["Sede"].'  or  "Sede"=3) and "AsistenciEstud"='.$this->departamentos["AsistenciEstud"];		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}

		/**
		* Consulto un departamento
		* @method consultar(), ComprobarNumeric(), array_OperacionesSistemicas()
		* @param [integer] $IdDepartamentos, [integer] $Responsable
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Departamentos_show($IdDepartamentos, $Responsable){
			$this->comprobar->ComprobarNumeric($IdDepartamentos);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT (SELECT count(*) from trabajadores trab where trab."Depto"=d."IdDepartamentos") as "Depto", d."IdDepartamentos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", d."AsistenciEstud", d."UltModif", d."FechaRegistro", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=d."Responsable") as "Responsable", d."Estatus", d."informacion" FROM Departamentos d where d."IdDepartamentos"='.$IdDepartamentos;		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["informacion"]=='0') {
                    $filas["informacion"]="-";
                }

				if ($filas["AsistenciEstud"]==1) {
                    $filas["AsistenciEstud"]="Si";
                }
                else{
                    $filas["AsistenciEstud"]="No";
                }
                $filas["FechaRegistro"]=date('d F Y g:ia', strtotime($filas["FechaRegistro"]));
                $filas["UltModif"]=date('d F Y g:ia', strtotime($filas["UltModif"]));
                 if ($filas["Estatus"]==1) {
                    $filas["Estatus"]="Activo";
                }
                else{
                    $filas["Estatus"]="Inactivo";
                }

                
				$this->departamentos[] =$filas;
			}

			/**
			* Llamada a la función para registrar quien realizó la consulta
			* @method consultar(), array_OperacionesSistemicas()
			*/	
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$IdDepartamentos,$this->tabla);
			return $this->departamentos[0];
		}

		
		/**
		* Consulto el departamento a editar
		* A diferencia de la funcion get_Departamentos_edit(), esta no posee los arreglos de los campos ni hace llamado a la funcion de array_OperacionesSistemicas()
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdDepartamentos
		* @return [array] [departamentos]   
		*/
		public function get_Departamentos_edit($IdDepartamentos){
			$this->comprobar->ComprobarNumeric($IdDepartamentos);
			$sql='SELECT * FROM Departamentos where "IdDepartamentos"='.$IdDepartamentos;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				if ($filas["informacion"]=='0') {
                    $filas["informacion"]="";
                }
                $this->departamentos[] =$filas;

			}
			return $this->departamentos[0];

		}

		/**
		* Modifico el departamento en el sistema 
		* @method consultar(), Comprobacion(), get_Departamento_update(), array_OperacionesSistemicas()
		* @param [array] $departamentos
		* @return [integer] muestre 1 si es true || [string] El error   
		*/	
		public function set_Departamentos_update($departamentos){ 
			$this->departamentos = $departamentos;
			$this->Comprobacion();

			if ($this->get_Departamento_update()==0) {

				$sql= 'UPDATE Departamentos SET "TipoDepto"='.$this->departamentos["TipoDepto"].', "Sede"='.$this->departamentos["Sede"].', "AsistenciEstud"='.$this->departamentos["AsistenciEstud"].', "UltModif"= now(), "Responsable"='.$this->departamentos["Responsable"].', "Estatus"='.$this->departamentos["Estatus"].', "informacion"='."'".$this->departamentos["informacion"]."'".' WHERE  "IdDepartamentos"='.$this->departamentos["IdDepartamentos"];
				
				if ($this->db->consultar($sql)) {
					/**
					* Llamada a la función para registrar quien realizó la consulta
					* @method consultar(), array_OperacionesSistemicas()
					*/	
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->departamentos["Responsable"],2, $this->departamentos["IdDepartamentos"],$this->tabla);
					return "1";
				}
			}
			else{
				return 'Ya existe otro registro de departamento con esas características';
			}

		}


		/**
		* Consulto si existe otro departamento con las caracteristicas a modificar que sea ageno al id del registro 
		* @param [array] [departamentos]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Departamento_update(){

			$sql='SELECT * FROM Departamentos where "TipoDepto"='.$this->departamentos["TipoDepto"].' and ("Sede"='.$this->departamentos["Sede"].'  or  "Sede"=3) and "AsistenciEstud"='.$this->departamentos["AsistenciEstud"].'and "Estatus"='.$this->departamentos["Estatus"].'and "IdDepartamentos"!='.$this->departamentos["IdDepartamentos"];	
				
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		/**
		* Elimino el departamento a seleccionar
		* @method consultar(), ComprobarNumeric(), get_Departamentos_destroy(), array_OperacionesSistemicas()
		* @param [integer] $IdDepartamentos, [integer] $Responsable
		* @return [integer]    
		*/
		public function set_Departamentos_destroy($IdDepartamentos, $Responsable){ 
			$this->comprobar->ComprobarNumeric($IdDepartamentos);
			$this->comprobar->ComprobarNumeric($Responsable);
			if ($this->get_Departamentos_destroy($IdDepartamentos)>0) {
				$sql= 'DELETE FROM Departamentos WHERE "IdDepartamentos"='.$IdDepartamentos;
				if ($this->db->consultar($sql)) {
					/**
					* Llamada a la función para registrar quien realizó la consulta
					* @method consultar(), array_OperacionesSistemicas()
					*/
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$IdDepartamentos,$this->tabla);
					return "1";
				}
			}
			else{
				return "2";
			}

		}

		/**
		* Consulto si existe el departamento en el sistema
		* @param [integer] $IdDepartamentos
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Departamentos_destroy($IdDepartamentos){

			$sql='SELECT * FROM Departamentos where "IdDepartamentos"='.$IdDepartamentos;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}

		/**
		* OTRAS CONSULTAS
		* Son consultas llamadas desde otros controladores o clases pero que estan asociadas con la tabla Departamentos, se agregan en el modelo que posea mas peso para la consulta
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion y el nombre de la funcion del controlador: get_Clase_NombredelControlador_funciondelcontrolador
		*/
		

		/**
		* Consulto los departamentos que estan activos en el sistema		
		* @return [array] [departamentos]   
		*/
		public function get_Despartamentos_Trabajadores_create()
		{
			$sql='SELECT d."IdDepartamentos" as "Depto", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Departamentos d where d."Estatus"=1';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}

			return $this->departamentos;

		}

		/**
		* Consulto los departamentos que estan activos, que le presten servicio directo al estudiante, y que su tipo de departamento sea: Psicologia (1), Odontologia (2), Estudio SocioEconomico (5) en el sistema. Nota: No se agrega el depto Medicina General, porque para atender un estudiante no requiere cita		
		* @return [array] [departamentos]   
		*/
		public function get_Departamentos_Citas_Create()
		{

			$sql='SELECT dep."Descripcion", dep."IdDepartamento" FROM Departamentos d, Departamento dep where dep."IdDepartamento"=d."TipoDepto" and d."AsistenciEstud"=1 and d."Estatus"=1 and d."TipoDepto" in (1,2,5) group by dep."IdDepartamento"';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;

		}

		/**
		* Consulto la informacion del departamento seleccionado, para mostrar una vez se haya realizado el registro de la cita
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdDepartamentos
		* @return [array] [departamentos]   
		*/
		public function get_Despartamentos_Citas_Store($IdDepartamentos)
		{
			$this->comprobar->ComprobarNumeric($IdDepartamentos);
			$sql='SELECT d."IdDepartamentos", d."informacion", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Departamentos d where d."IdDepartamentos"='.$IdDepartamentos;		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["informacion"]=='0') {
                    $filas["informacion"]="";
                }
				$this->departamentos[] =$filas;
			}

			return $this->departamentos[0];

		}
		
		/**
		* Consulto los departamentos asociados al trabajador
		* @method consultar(), ComprobarCaracteres()
		* @param [string] $departamentos ejem 24,12
		* @return [array] [departamentos]   
		*/
		public function get_Departamentos_GestionCitas_cancelar($departamentos){
			$this->comprobar->ComprobarCaracteres($departamentos);
			$sql='SELECT d."IdDepartamentos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Departamentos d where d."Estatus"=1 and d."IdDepartamentos" in ('.$departamentos.')';		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;
		}

		/**
		* Consulto los departamentos que estan activos
		* @return [array] [departamentos]   
		*/
		public function get_Departamentos_CancelarCitas_cancelar(){

			$sql='SELECT d."IdDepartamentos", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Departamentos d where d."Estatus"=1';		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;
		}

		/**
		* Consulto los departamentos que estan activos
		* @param [string] $departamentos ejem 24,12
		* @return [array] [departamentos]   
		* @method consultar(), ComprobarCaracteres()
		*/
		public function get_Departamentos_Consultas_index_groupby($departamentos){
			$this->comprobar->ComprobarCaracteres($departamentos);

			$sql='SELECT d."TipoDepto" as "nrotipodepto",  (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", "IdDepartamentos" FROM Departamentos d where d."Estatus"=1 and d."IdDepartamentos" in ('.$departamentos.') group by 1, 2, 3, 4';		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;
		}
		
		/**
		* Consulto los departamentos que estan activos
		* @param [string] $departamentos ejem 24,12
		* @return [array] [departamentos]   
		* @method consultar(), ComprobarCaracteres()
		*/
		public function get_Consultas_Departamentos($deptos){
			$this->comprobar->ComprobarCaracteres($deptos);

			$sql='SELECT d."IdDepartamentos", d."TipoDepto" as "nrotipodepto",  (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Departamentos d where d."Estatus"=1 and d."TipoDepto" in ('.$deptos.') and d."Estatus"=1';		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;
		}

		/**
		* Consulto si el usuario es medico y si se encuentra en el momento de servicio
		* @param [integer] $usuario 
		* @return [array] [departamentos]   
		* @method consultar(), ComprobarNumeric()
		*/
		public function get_Departamentos_Consultas_Trab_Hor_DeptoMed($usuario){
			//Anteriormente llamado  get_Depto_Trabajador_Horario_DeptoMed
			$this->comprobar->ComprobarNumeric($usuario);
			setlocale(LC_TIME, 'es_VE');
			date_default_timezone_set('America/Caracas');
			$sql='SELECT  d."IdDepartamentos", t."TipoPersonal", (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=t."Persona") as "datopersona" FROM Departamentos d inner join 
				Trabajadores t on t."Depto"=d."IdDepartamentos" inner join Horarios h on h."IdEmpleado"=t."IdTrabajador" where  t."Persona"='.$usuario.' and h."Dia"='."'".date('w')."'".' and d."TipoDepto" = 3   and h."HorarioInic"<='."'".date('G:m:s')."'".' and h."HorarioFinal">='."'".date('G:m:s')."'".'  group by 1, 2, 3, 4';
			$conexion = new conexion;			
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->departamentos[] =$filas;
			}
			return $this->departamentos;
		}
	}
?>