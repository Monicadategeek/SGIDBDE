<?php 

	

	/**
	* 
	*/
	class Informes 

	{
		private $db;
		private $informes;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->informes= array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=13;//listo
			
		}


		private function Comprobacion()
		{
			
			if (isset($this->informes["CodInforme"])) {
				$this->comprobar->ComprobarNumeric($this->informes["CodInforme"]);
			}
			if (isset($this->informes["Estudiante"])) {
				$this->comprobar->ComprobarNumeric($this->informes["Estudiante"]);
			}
			if (isset($this->informes["TipoInforme"])) {
				$this->comprobar->ComprobarNumeric($this->informes["TipoInforme"]);
			}
			if (isset($this->informes["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->informes["Responsable"]);
			}
			if (isset($this->informes["Observaciones"])) {
				$this->comprobar->ComprobarCaracteres($this->informes["Observaciones"]);
			}
			else{
				$this->informes["Observaciones"]='';
			}
			
		}

		public function set_Informes_store($informes){ //Consulta Registrar Depertamentos
			$this->informes = $informes;
			$this->Comprobacion();
			if ($this->get_Informes_store()==0) {
				$sql='INSERT INTO informes("CodInforme", "Estudiante", "TipoInforme", "UltModif", "Responsable", "Descripcion")
			    VALUES ('.$this->informes["CodInforme"].', '.$this->informes["Estudiante"].', '.$this->informes["TipoInforme"].', now(), '.$this->informes["Responsable"].", '".$this->informes["Observaciones"]."'".');
			';

				if ($this->db->consultar($sql)) {	
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->informes["Responsable"],1,$this->informes["CodInforme"],$this->tabla);
					$this->informes["CodInforme"];
				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			return $this->informes["CodInforme"];
			
		}

		private function get_Informes_store(){
			$sql='SELECT * FROM informes where "CodInforme"='.$this->informes["CodInforme"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		public function get_Informes_show($CodInforme){
			$this->comprobar->ComprobarNumeric($CodInforme);

			$sql='SELECT i."Descripcion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable" FROM informes i where i."CodInforme"='.$CodInforme;	
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->informes[] =$filas;
			}
			return $this->informes;

		}

  
		public function get_Informes_Psicologico_list($IdEstudiante){

			$this->comprobar->ComprobarNumeric($IdEstudiante);

			$sql='SELECT c."Fecha", c."Hora", c."IdCitas", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable" FROM Citas c inner join informes i on c."IdCitas"=i."CodInforme" where c."Asistencia"=2 and i."TipoInforme"=1 and c."IdEstudiante"='.$IdEstudiante;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));
				$filas["boton"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$filas["IdCitas"].');"><i class="fa fa-book"></i></button>';

				$this->informes[] =$filas;

			}
			return $this->informes;

		}


		
	

		

		

		



	}
?>