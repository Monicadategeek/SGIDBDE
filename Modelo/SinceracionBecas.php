<?php
		
	/**
	* Es llamado en PresupuestoController, BecariosController
	*/
	class SinceracionBecas 
	{
		private $db;
		private $sinceracionbecas;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->sinceracionbecas =array();
			$this->comprobar = new SeguridadDatos;				
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=9;//Listo
			
		}

		public function get_Becarios_SinceracionBecas_edit($id){
			
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT "IdSinceracionBecas", (SELECT te."Descripcion" from TipoBeca te where te."IdTipoBeca"="TipoBeca") as "TipoBeca" FROM sinceracionbecas where "PresupuestoAsociado"=(SELECT "PresupuestoAsociado" from sinceracionbecas where "IdSinceracionBecas"='.$id.')';
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->sinceracionbecas[] =$filas;
			}

			return $this->sinceracionbecas;


		}

		/*public function get_Consulta_SinceracionBecas($trabajadores)
		{

			foreach ($trabajadores as $trabajador) {
				$sql='SELECT h.*, t."IdTrabajador", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", (SELECT tp."Descripcion" from tipopersonal tp where tp."IdTipoPersonal"=t."TipoPersonal") as "TipoPersonal" from SinceracionBecas h, Departamentos d, Trabajadores t  where t."Depto"=d."IdDepartamentos" and h."IdEmpleado"=t."IdTrabajador" and h."IdEmpleado"='.$trabajador["IdTrabajador"];
				$consulta = $this->db->consultar($sql);

				while ($filas=pg_fetch_assoc($consulta)) {
					$this->sinceracionbecas[] =$filas;
				}
				return $this->sinceracionbecas;
			}
		}*/

		public function set_SinceracionBecas_store($sinceracionbecas){ //Consulta Registrar SinceracionBecas

			$sql= 'INSERT INTO SinceracionBecas("PresupuestoAsociado", "TipoBeca", "NroCupos", "FechaRegistro", "Responsable", "Monto") VALUES ';
			$this->comprobar->ComprobarNumeric($sinceracionbecas["PresupuestoAsociado"]);
			$this->comprobar->ComprobarNumeric($sinceracionbecas["Responsable"]);
			$this->comprobar->ComprobarNumeric($sinceracionbecas["total"]);
			for ($i=1; $i <=$sinceracionbecas["total"] ; $i++) { 
					$this->comprobar->ComprobarNumeric($sinceracionbecas["NroCupos".$i]);
					if (!(empty($sinceracionbecas["NroCupos".$i]))) {
						$this->comprobar->ComprobarNumeric($sinceracionbecas["TipoBeca".$i]);
						$this->comprobar->ComprobarNumeric($sinceracionbecas["NroCupos".$i]);
						$this->comprobar->ComprobarNumeric($sinceracionbecas["Monto".$i]);

						$values=$values."(".$sinceracionbecas["PresupuestoAsociado"].", ".$sinceracionbecas["TipoBeca".$i].", ".$sinceracionbecas["NroCupos".$i].", now(), ".$sinceracionbecas["Responsable"].", ".$sinceracionbecas["Monto".$i]."),";
					}				
			}
			$values = substr($values, 0, -1); 
			$values=$values.";";
			$sql=$sql.$values;
			if ($this->db->consultar($sql)) {
				return '1';
			}
		}

		public function set_SinceracionBecas_update($sinceracionbecas){ //Consulta Registrar SinceracionBecas
			
			$this->comprobar->ComprobarNumeric($sinceracionbecas["PresupuestoAsociado"]);
			$this->comprobar->ComprobarNumeric($sinceracionbecas["Responsable"]);
			$this->comprobar->ComprobarNumeric($sinceracionbecas["total"]);

			for ($i=1; $i <=$sinceracionbecas["total"] ; $i++) { 
				$this->comprobar->ComprobarNumeric($sinceracionbecas["NroCupos".$i]);

				if (!(empty($sinceracionbecas["NroCupos".$i]))) {
					$this->comprobar->ComprobarNumeric($sinceracionbecas["TipoBeca".$i]);
					$this->comprobar->ComprobarNumeric($sinceracionbecas["NroCupos".$i]);
					$this->comprobar->ComprobarNumeric($sinceracionbecas["Monto".$i]);

					$sql= 'UPDATE SinceracionBecas set "NroCupos"='.$sinceracionbecas["NroCupos".$i].', "Responsable"='.$sinceracionbecas["Responsable"].', "Monto"='.$sinceracionbecas["Monto".$i].' WHERE "PresupuestoAsociado"='.$sinceracionbecas["PresupuestoAsociado"].' AND "TipoBeca"='.$sinceracionbecas["TipoBeca".$i];
					if ($this->db->consultar($sql)==False) {
						echo "Error";
						die();
					}
				}				
			}
			return "1";
			
		}

		public function set_SinceracionBecas_destroy($PresupuestoAsociado){ 

			$this->comprobar->ComprobarNumeric($PresupuestoAsociado);
			$sql= 'DELETE FROM SinceracionBecas WHERE "PresupuestoAsociado"='.$PresupuestoAsociado;
				if ($this->db->consultar($sql)) {
					return "1";
				}

		}
		
		public function get_SinceracionBecas_show($id)
		{
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT (SELECT count(*) from inscripcionesbecarios where "BecaAsociada"=s."IdSinceracionBecas" and "Estado"=1) as "Inscritos", (SELECT te."IdTipoBeca" from TipoBeca te where te."IdTipoBeca"=s."TipoBeca") as "IdTipoBeca",  (SELECT te."Descripcion" from TipoBeca te where te."IdTipoBeca"=s."TipoBeca") as "TipoBeca", s."NroCupos", s."Monto" from SinceracionBecas s  where s."PresupuestoAsociado"='.$id.' order by "IdTipoBeca" asc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				$filas["Monto"]=number_format($filas["Monto"], 2, '.', '');
				$this->sinceracionbecas[] =$filas;
			}

			return $this->sinceracionbecas;

		}

		public function get_SinceracionBecas_reporte($id)
		{
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT s."IdSinceracionBecas", (SELECT te."IdTipoBeca" from TipoBeca te where te."IdTipoBeca"=s."TipoBeca") as "IdTipoBeca",  (SELECT te."Descripcion" from TipoBeca te where te."IdTipoBeca"=s."TipoBeca") as "TipoBeca", s."Monto" from SinceracionBecas s  where s."PresupuestoAsociado"='.$id.' and s."TipoBeca"<4 order by "TipoBeca" asc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				
				$this->sinceracionbecas[] =$filas;
			}

			return $this->sinceracionbecas;

		}


		#OTRAS CONSULTAS


		public function get_SinceracionBecas_Inscripciones()
		{
			$sql='SELECT (SELECT count(*) from inscripcionesbecarios where "BecaAsociada"=s."IdSinceracionBecas" and "Estado"=1) as "Inscritos", s."IdSinceracionBecas", (SELECT te."Descripcion" from TipoBeca te where te."IdTipoBeca"=s."TipoBeca") as "TipoBeca", s."NroCupos", s."Monto" from SinceracionBecas s
				where s."PresupuestoAsociado"=(SELECT "IdPresupuesto" from Presupuesto where ("Fecha_Comienzo">= '."'".date('Y-m-d')."'".' or "Fecha_Comienzo"<= '."'".date('Y-m-d')."'".') and "TipoPresupuesto"=2 and "Estado"=1 order by "Fecha_Comienzo" desc limit 1) order by s."TipoBeca" asc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->sinceracionbecas[] =$filas;
			}

			return $this->sinceracionbecas;

		}


		
	public function get_SinceracionBecas_Becas($IdPresupuesto)
		{
			$sql='SELECT s."IdSinceracionBecas" from SinceracionBecas s
				where s."PresupuestoAsociado"='.$IdPresupuesto;		
			$consulta = $this->db->consultar($sql);
			$asociado='';
			while ($filas=pg_fetch_assoc($consulta)) {
				$asociado=$asociado.$filas["IdSinceracionBecas"].',';
				$this->sinceracionbecas[] =$filas;
			}

			$asociado = substr($asociado, 0, -1);
			return $asociado;

		}

	}


?>