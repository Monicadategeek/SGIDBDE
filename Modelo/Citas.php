<?php 
	/**
	* Clase Citas
	* Comparte la tabla Citas en la Base de Datos
	* Es llamado en los Controladores CitasController, 
	*
	* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase y el nombre de la funcion del controlador: get_Citas_index 
	*
	* ejem: get_Departamentos_index  
	* 
	* @author Monica Hernandez 
	* @author MonkeyDMoni.github.io
	*/
 
	
	class Citas 

	{
		private $db;
		private $citas;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


 		/**
		 * Funcion de inicio donde llama los archivos; 
		 * @global [array] citas
		 * @global [object] db
		 * @global [object] comprobar
		 * @global [object] OperacionesSistemicas
		 * @global [integer] tabla
		 * 
		 */
		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->comprobar = new SeguridadDatos;
			$this->citas =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->tabla=5;//Listo
			
		}

		/**
		 * Funcion privada donde evalua las variables que toma para insertar en la BD
		 * @global [array] citas
		 * 
		 */
		private function Comprobacion()
		{
			if (isset($this->citas["IdCitas"])) {
				$this->comprobar->ComprobarNumeric($this->citas["IdCitas"]);
			}

			if (isset($this->citas["Cedula"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Cedula"]);
			}

			if (isset($this->citas["Fecha"])) {
				$this->comprobar->ComprobarFecha($this->citas["Fecha"]);
			}

			if (isset($this->citas["Hora"])) {
				$this->comprobar->ComprobarFechaHora($this->citas["Hora"], 'H:i');
			}	

			if (isset($this->citas["Servicio"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Servicio"]);
			}

			if (isset($this->citas["Asistencia"])) {
				$this->comprobar->ComprobarNumeric($this->citas["Asistencia"]);
			}

			if (isset($this->citas["IdEstudiante"])) {
				$this->comprobar->ComprobarNumeric($this->citas["IdEstudiante"]);
			}			
		}



 		/**
		* Consulto las citas del estudiante que sean mayores o iguales al dia actual
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $estudiante 
		* @return [array] [citas]
		*/
		public function get_Citas_index($estudiante){
			//get_Citas_index_Estudiantes
			$this->comprobar->ComprobarNumeric($estudiante);
			$dia=date('Y-m-d');
			$sql='SELECT c."Fecha", c."IdCitas", c."Hora", c."Asistencia", 

			(SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Asistencia") as "TipoAsistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", eo."Estado" as "eoestado", d."TipoDepto" as "IdTipoDepto"

			FROM Citas c inner join Departamentos d on c."Servicio"=d."IdDepartamentos" left join encuesta_odontologica eo on c."IdCitas"=eo."id"
			 where  c."Fecha">='."'".$dia."'".'  and  c."IdEstudiante"='.$estudiante.' order by c."Fecha" desc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {

				if ($filas["Asistencia"]==1) {
	               	if ($filas["eoestado"]==0) {
	                   $filas["formulario"]='<a href="../Controller/EncuestaOdontologicaController.php?id='.$filas["IdCitas"].'" target="_blank">Encuesta Odontologica</a>';
	               	}
	               	else{
	                    $filas["formulario"]= "NO";
	               	}

	               	if ($filas["IdTipoDepto"]==5) {
	               		$id=$this->get_Citas_EstudianteEncuestas($filas["IdCitas"]);
	               		if ($id==0) {
	               			$filas["formulario"]="<a href='http://localhost/SGIDBDE/Controller/EncuestaController.php' target='_blank'>Encuesta Estudiantil</a>";
	               		}
	               		else{
	               			$filas["formulario"]= "NO";
	               		}

	               	}

	            }
	            else{
	                $filas["formulario"]= "NO";
	            }

				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));

				$this->citas[] =$filas;
			}
			return $this->citas;
		}

		/**
		* Consulto el historial de citas del estudiante 
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $estudiante 
		* @return [array] [citas]
		*/
		public function get_Citas_history($estudiante){
			$this->comprobar->ComprobarNumeric($estudiante);			
			$sql='SELECT c."Fecha", c."IdCitas", c."Hora", c."Asistencia", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Asistencia") as "TipoAsistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and  c."IdEstudiante"='.$estudiante.' order by c."Fecha" desc';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));

				$this->citas[] =$filas;
			}
			return $this->citas;
		}


		/*public function get_Citas_historial_Estudiantes($estudiante){
			estudiante
			$sql='SELECT c."Fecha", c."IdCitas", c."Hora", c."Asistencia", (SELECT ta."Descripcion" from tipoasistencia ta where ta."idTipAsist"=c."Asistencia") as "TipoAsistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and c."IdEstudiante"='.$estudiante.' order by c."Fecha" desc';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->citas[] =$filas;
			}
			return $this->citas;
		}*/
					

		/**
		* Inserto una nuevo cita en el sistema 
		* @method consultar(), Comprobacion(), get_Citas_store()
		* @param [array] $trabajadores
		* @return [integer] muestre 1 si es true || [string] El error   
		*/
		public function set_Citas_store($citas){
			$this->citas = $citas;
			$this->Comprobacion();

			if ($this->get_Citas_store($citas)==0) {
				$sql= 'INSERT INTO Citas("Cedula", "Fecha", "Hora", "Servicio", "Asistencia", "IdEstudiante") VALUES (';
				$values=$this->citas["Cedula"].", '".$this->citas["Fecha"]."', '".$this->citas["Hora"]."', ".$this->citas["Servicio"].", 1, ".$this->citas["IdEstudiante"].");";
				$sql=$sql.$values;
				if ($this->db->consultar($sql)) {
					$sql='SELECT max("IdCitas") as "IdCitas" FROM Citas limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					return $filas["IdCitas"];
				}
			} 
			else{
				return "<p><b>Error!</b> Usted Ya Registro Una Cita en el Servicio seleccionado</p>";;
			}
		}


		/**
		* Consulto si existe una cita para el departamento, solicitada por el estudiante
		* @param [array] [citas]
		* @return [integer] muestra el nro de registros   
		*/
		private function get_Citas_store(){		

			$sql='SELECT * FROM Citas where "Cedula"='.$this->citas["Cedula"].' and "Fecha">now()  and   "Servicio" = ('.$this->citas["Servicio"].') and "IdEstudiante"='.$this->citas["IdEstudiante"].' and "Asistencia"=1';		
			$consulta = $this->db->consultar($sql);
			
			return pg_num_rows($consulta);
		}
		
		/**
		* Consulto la cita
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdCitas
		* @return [integer] muestra el nro de registros   
		*/
		public function get_Citas_show($IdCitas){
			$this->comprobar->ComprobarNumeric($IdCitas);
			 $sql='SELECT c."Cedula", c."IdEstudiante", c."Fecha", c."IdCitas", c."Hora", c."Asistencia", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto", d."TipoDepto" as "numTipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"  FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and c."IdCitas"='.$IdCitas;
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('g:ia', strtotime($filas["Hora"]));

				$this->citas[] =$filas;
			}
			return $this->citas[0];
		}

		/**
		* Consulto la cita a editar
		* A diferencia de la funcion get_Trabajadores_show(), esta no posee los arreglos de los campos 
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $IdCitas
		* @return [array] [citas]   
		*/		
		public function get_Citas_edit($IdCitas){
			$this->comprobar->ComprobarNumeric($IdCitas);
			$sql='SELECT c.*, d."Sede", d."TipoDepto" FROM Citas c, Departamentos d  where c."Servicio"=d."IdDepartamentos" and "IdCitas"='.$IdCitas;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->citas[] =$filas;
			}
			return $this->citas[0];

		}	
		
		/**
		* Modifico la cita en el sistema
		* @method consultar(), Comprobacion()
		* @param [array] $trabajadores
		* @return [integer] muestre 1 si es true || 2 si es false
		*/	
		public function set_Citas_update($citas){ 
			$this->citas = $citas;
			$this->Comprobacion();
			$sql= 'UPDATE Citas SET "Servicio"='.$citas["Servicio"].', "Hora"='."'".$citas["hora"]."'".', "Fecha"='."'".$citas["fecha"]."'".' WHERE  "IdCitas"='.$citas["IdCitas"];
			if ($this->db->consultar($sql)) {
				return "1";
			}
			else{
				return "2";
			}
		}


		/**
		* Cancela una cita, cambia el campo asistencia por valor 3 (Cancelación Estudiante)
		* @method consultar(), Comprobacion()
		* @param [array] $trabajadores
		* @return [integer] muestre 1 si es true || 2 si es false
		*/	
		public function set_Citas_cancelar($id){ //Consulta Modificar Citas
			$this->comprobar->ComprobarNumeric($id);
			$sql= 'UPDATE Citas SET "Asistencia"=3  WHERE  "IdCitas"='.$id;
			if ($this->db->consultar($sql)) {
				return "1";
			}
			else{
				return "2";
			}	

		}


		/*public function set_Citas_destroy($id){ //Consulta Registrar Citas
			$this->comprobar->ComprobarNumeric($id);

			$sql= 'DELETE FROM Citas WHERE "IdCitas"='.$id;
			if ($this->db->consultar($sql)) {
				//$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$id,$this->tabla);
				return "1";
			}
			else{
				return "2";
			}
		}*/

		
		/**
		* OTRAS CONSULTAS
		* Son consultas llamadas desde otros controladores o clases pero que estan asociadas con la tabla Citas, se agregan en el modelo que posea mas peso para la consulta
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion y el nombre de la funcion del controlador: get_Clase_NombredelControlador_funciondelcontrolador
		*/

		/*
		public function get_Despartamentos_Trabajadores_create()
		{
			$sql='SELECT d."IdCitas", (SELECT dep."Descripcion" from departamento dep where dep."IdDepartamento"=d."TipoDepto") as "TipoDepto",  (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede" FROM Citas d where d."Estatus"=1';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->citas[] =$filas;
			}

			return $this->citas;

		}*/

		/**
		* Consulto la hora de las citas solicitadas para el departamento y la fecha seleccionada
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $depto, [date] $fecha
		* @return [array] [citas]   
		*/
		public function get_Citas_Solicitadas($depto, $fecha)
		{
			$this->comprobar->ComprobarNumeric($depto);
			$this->comprobar->ComprobarFecha($fecha);
			$sql='SELECT c."Hora" FROM Citas c where c."Servicio"='.$depto.' and c."Fecha"='."'".$fecha."'";
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$this->citas[] =$filas;
			}
			return $this->citas;
		}

		/**
		* Consulto si el estudiante tiene citas registradas en el servicio que no hayan sido asistidas y que sean mayor o igual a la fecha actual
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $depto, [date] $fecha
		* @return [array] [citas]   
		*/
		public function get_Citas_Solicitud($citas)
		{
			$this->comprobar->ComprobarNumeric($citas["Cedula"]);
			$this->comprobar->ComprobarNumeric($citas["IdEstudiante"]);
			$this->comprobar->ComprobarCaracteres($citas["Servicio"]);

			$sql='SELECT * FROM Citas where "Cedula"='.$citas["Cedula"].' and "Fecha">='."'".date('Y-m-d')."'".'  and   "Servicio" IN ('.$citas["Servicio"].') and "IdEstudiante"='.$citas["IdEstudiante"].' and "Asistencia"=1';		
			$consulta = $this->db->consultar($sql);			
			return pg_num_rows($consulta);
		}

		/**
		* Consulto si falta como comenzar o terminar la encuesta para la entrevista socioeconomica
		* @method consultar(), ComprobarNumeric()
		* @param [integer] $cita
		* @return [integer]  nro de registros  
		*/
		public function get_Citas_EstudianteEncuestas($cita){
			$fechaActual = date('Y-m-d');
			$fechaMesPasado = strtotime ('-1 month', strtotime($fechaActual));
			$fechaMesPasadoDate = date('Y-m-d', $fechaMesPasado);

			$sql='SELECT * FROM citas c, estudiantesencuestados e where c."IdCitas"=e."cita" and c."IdCitas"='.$cita.' and e."interesAyu" is not null';			

			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		/**
		* Consulto si el estudiante tiene una cita para el depto de estudios socioEconomicos, Si ya se inicio la encuesta para dicho estudio 
		* @param [integer] $estudiante
		* @return [integer]  nro de registros  
		*/
		public function get_EstudianteEncuestas_Citas($estudiante){
			$fechaActual = date('Y-m-d');
			$fechaMesPasado = strtotime ('-1 month', strtotime($fechaActual));
			$fechaMesPasadoDate = date('Y-m-d', $fechaMesPasado);

			$sql='SELECT * FROM citas c, Departamentos d where c."Servicio"=d."IdDepartamentos" and c."Asistencia"=1 and d."TipoDepto"=5 and c."IdEstudiante"='.$estudiante.' and c."Fecha">='."'".date('Y-m-d')."'";
			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {


				$sql='SELECT * FROM citas c left join estudiantesencuestados e on c."IdEstudiante"=e."estudiante" where c."IdEstudiante"='.$estudiante.' and e."fecha">=c."Fecha" or e."fecha">='."'".$fechaMesPasadoDate."'".' and e."interesAyu" is null';
				$consulta = $this->db->consultar($sql);
				return pg_num_rows($consulta);
			}
			else{
				return "No existe una cita para el depto de estudios socioeconomicos";
			}
		}

		/**
		* Consulto si el estudiante tiene una cita para el depto de estudios socioEconomicos
		* @param [integer] $estudiante
		* @return [integer]  [IdCitas]  || null 
		*/
		public function get_EstudianteEncuestas_Citas_id($estudiante){
			$fechaActual = date('Y-m-d');
			$fechaMesPasado = strtotime ('-1 month', strtotime($fechaActual));
			$fechaMesPasadoDate = date('Y-m-d', $fechaMesPasado);

			$sql='SELECT c."IdCitas" FROM citas c, Departamentos d where c."Servicio"=d."IdDepartamentos" and c."Asistencia"=1 and d."TipoDepto"=5 and c."IdEstudiante"='.$estudiante.' and c."Fecha">='."'".date('Y-m-d')."'";
			$consulta = $this->db->consultar($sql);
				if (pg_num_rows($consulta)>0) {
					while ($filas=pg_fetch_assoc($consulta)) {

						$this->encuestas[]=$filas;
					}
					return $this->encuestas[0]["IdCitas"];
				/*

				$sql='SELECT * FROM estudiantesencuestados e where e."estudiante"='.$estudiante.' and e."Id"=(SELECT max("Id") from estudiantesencuestados where e."estudiante"='.$estudiante.') and e."fecha">='."'".$fechaMesPasadoDate."'".' or e."interesAyu" is null and e."culturales" is null



				SELECT c."IdCitas" FROM citas c left join estudiantesencuestados e on c."IdEstudiante"=e."estudiante" where c."IdEstudiante"= and e."fecha">=c."Fecha" or e."fecha">='."'".$fechaMesPasadoDate."'".' and e."interesAyu" is null';
				$consulta = $this->db->consultar($sql);
				if (pg_num_rows($consulta)>0) {

					
				}
				else{
					return "null";
				}*/
			
			}
			else{
				return "null";
			}
		}

	
		/**
		* Inserto una nuevo cita en el sistema para el dia y la hora actual, (SOLO PARA EL DEPTO DE MEDICINA GENERAL) 
		* @method consultar(), Comprobacion()
		* @param [array] $trabajadores
		* @return [integer] [IdCitas]
		*/
		public function set_Citas_Consultas_store($citas){
			$this->citas = $citas;
			$this->Comprobacion();

			$sql= 'INSERT INTO Citas("Cedula", "Fecha", "Hora", "Servicio", "Asistencia", "IdEstudiante") VALUES (';

			$values=$this->citas["Cedula"].", now(), now(), ".$this->citas["Servicio"].", 2, ".$this->citas["IdEstudiante"].");";
			$sql=$sql.$values;

			if ($this->db->consultar($sql)) {

				$sql='SELECT max("IdCitas") as "IdCitas" FROM Citas limit 1';	
				$consulta = $this->db->consultar($sql);
				$filas=pg_fetch_assoc($consulta);

				return $filas["IdCitas"];
			}
			else{
				die("Error al interactuar con la Base de datos");
			}
		}
	}
?>