<?php

	/**
	* 
	*/
	class OperacionesSistemicas
	{
		private $db;
		private $OperacionesSistemicas;

		public function __construct()
		{
			require_once("../Controller/conectar.php");
			$this->db = new conexion;
			$this->OperacionesSistemicas =array();
			
		}

		public function set_OperacionesSistemicas($OperacionesSistemicas)
		{
			$sql= 'INSERT INTO operacionessistematicas("FechaRegistro", "Responsable", "Accion", "IdRegistro", "Tabla") VALUES (';
			$values="now(), ".$OperacionesSistemicas["Responable"].", ".$OperacionesSistemicas["Accion"].", ".$OperacionesSistemicas["IdRegistro"].", ".$OperacionesSistemicas["Tabla"].");";
			$sql=$sql.$values;
			if ($this->db->consultar($sql)==False) {
				return "Error!";
			}
		}

		public function array_OperacionesSistemicas($Responable, $Accion, $IdRegistro, $Tabla)
		{
			
			$this->OperacionesSistemicas["Responable"]=$Responable;
			$this->OperacionesSistemicas["Accion"]=$Accion;
			$this->OperacionesSistemicas["IdRegistro"]=$IdRegistro;
			$this->OperacionesSistemicas["Tabla"]=$Tabla;

			$this->set_OperacionesSistemicas($this->OperacionesSistemicas);
		}


		public function get_OperacionesSistemicas_show($id, $tabla){

			$sql='SELECT (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=o."Responsable") as "Responsable", o."FechaRegistro"    FROM operacionessistematicas o  where "IdRegistro"=46  and "Accion" in (1,2) order by o."FechaRegistro" desc limit 1';		
			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d F Y g:ia', strtotime($filas["FechaRegistro"]));
				$this->OperacionesSistemicas[] =$filas;
			}
			return $this->OperacionesSistemicas[0];
			// '.$id.' and "Tabla"='.$tabla.'
		}

	}


?>