<?php 
	/**
	* Es llamado en BecariosController, AtencionCitasController
	*/
	class Becarios 

	{
		private $db;
		private $becarios;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("OperacionesSistemicas.php");
			$this->db = new conexion;
			$this->becarios =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->comprobar = new SeguridadDatos;
			$this->tabla=10;//Listo
			
		}

		private function Comprobacion()
		{
			if (isset($this->cancelaciones["IdBecarios"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["IdBecarios"]);
			}
			if (isset($this->cancelaciones["Estudiante"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["Estudiante"]);
			}
			if (isset($this->cancelaciones["FechaRegistro"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["FechaRegistro"]);
			}
			if (isset($this->cancelaciones["CtaBancaria"])) {
				$this->comprobar->ComprobarNumeric($this->cancelaciones["CtaBancaria"]);
			}
			if (isset($this->cancelaciones["FechaCtaBanca"])) {
				$this->comprobar->ComprobarFecha($this->cancelaciones["FechaCtaBanca"]);
			}
		}

		public function get_Becarios_activos_index($Responsable){//Consulta del Depertamentos Index

			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT  e."nombres", e."apellidos", e."sexo", e."cedula", e."cod_carrera",
			(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", e."cod_estatus", b."CtaBancaria", (Select tb."Descripcion" from TipoBeca tb where tb."IdTipoBeca"=s."TipoBeca") as "TipoBeca", i."FechaRegistro", i."IdInscripciones", i."Becario"

			FROM inscripcionesbecarios i, Becarios b, estudiantes e, Sinceracionbecas s 

			 where i."Becario"=b."IdBecarios" and b."Estudiante"=e."num_est" and s."IdSinceracionBecas"=i."BecaAsociada" and i."Estado"=1';		



			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				if ($filas["sexo"]==1) {
                    $filas["sexo"]="Femenino";
                }
                else{
                    $filas["sexo"]="Masculino";
                }

				$this->becarios[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->becarios;
		}


		public function get_Becarios_inactivos_index($Responsable){//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT Distinct  i."Becario", est."cedula", est."nombres", est."apellidos", est."sexo",  est."version" as "cod_carrera_version",	(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=est."cod_carrera" and est."version"=ca."ult_malla") as "cod_carrera", est."cod_estatus", b."CtaBancaria", (Select tb."Descripcion" from TipoBeca tb where tb."IdTipoBeca"=s."TipoBeca") as "TipoBeca", i."FechaRegistro", e."FechaRegistro" as "FechaEgreso", e."IdEgresos"

			FROM  Egresos e inner join inscripcionesbecarios i on e."Inscripcion"=i."IdInscripciones" inner join Becarios b on i."Becario"=b."IdBecarios" inner join estudiantes est on b."Estudiante"=est."num_est" inner join Sinceracionbecas s on s."IdSinceracionBecas"=i."BecaAsociada" 

			where i."IdInscripciones"=(SELECT max("IdInscripciones") from inscripcionesbecarios where "Becario" =i."Becario")';	

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				$filas["FechaEgreso"]=date('d-m-Y', strtotime($filas["FechaEgreso"]));


				if ($filas["sexo"]==1) {
                    $filas["sexo"]="Femenino";
                }
                else{
                    $filas["sexo"]="Masculino";
                }

				$this->becarios[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->becarios;
		}




		public function get_Becarios_show($id, $Responsable){
			$this->comprobar->ComprobarNumeric($Responsable);
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT b."IdBecarios", b."Estudiante", b."CtaBancaria", b."FechaCtaBanca" FROM inscripcionesbecarios i, Becarios b where i."Becario"=b."IdBecarios" and b."IdBecarios"='.$id." limit 1";		
			$consulta = $this->db->consultar($sql);
			 
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				if ($filas["FechaCtaBanca"]!='') {
					$filas["FechaCtaBanca"]=date('d-m-Y', strtotime($filas["FechaCtaBanca"]));
				}
				$this->becarios[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,6,$id,$this->tabla);
			
			return $this->becarios[0];
		}


		public function get_Becarios_activos_show($id){

			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT  i."IdInscripciones", i."Becario", i."Tipo_Inscripcion", i."FechaRegistro", i."Estado", (SELECT tb."Descripcion" from TipoBeca tb where tb."IdTipoBeca"=s."TipoBeca") as "TipoBeca", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable" FROM inscripcionesbecarios i, Becarios b, Sinceracionbecas s
			 where i."Becario"='.$id.' and i."Becario"=b."IdBecarios" and s."IdSinceracionBecas"=i."BecaAsociada" order by i."FechaRegistro" desc';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));
				if ($filas["Tipo_Inscripcion"]==1) {
					$filas["Tipo_Inscripcion"]='Inscripción';
				}
				else{
					$filas["Tipo_Inscripcion"]='Reinscripción';
				}
				if ($filas["Estado"]==1) {
					$filas["Estado"]='Activo';
				}
				else{
					$filas["Estado"]='Inactivo';
				}

				$this->becarios[] =$filas;
			}
			return $this->becarios;
		}



		public function get_Becarios_inactivos_show($id, $Responsable){//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($Responsable);


			$sql='SELECT  e."FechaRegistro", e."IdEgresos", e."Inscripcion", e."descripcion",

			(SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=e."Responsable") as "Responsable"

			 FROM inscripcionesbecarios i, Becarios b, estudiantes est, Sinceracionbecas s, Egresos e

			 where i."Becario"='.$id.' and i."Becario"=b."IdBecarios" and b."Estudiante"=est."num_est" and s."IdSinceracionBecas"=i."BecaAsociada" and e."Inscripcion"=i."IdInscripciones" and e."Inscripcion"=i."IdInscripciones"';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["FechaRegistro"]=date('d-m-Y', strtotime($filas["FechaRegistro"]));


				$this->becarios[] =$filas;
			}
			$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,3,'null',$this->tabla);
			return $this->becarios;
		}





		public function get_InscripcionesporBecario($id, $Responsable){
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarNumeric($Responsable);
			$sql='SELECT (Select tb."Descripcion" from TipoBeca tb where tb."IdTipoBeca"=s."TipoBeca") as "TipoBeca", i."FechaRegistro"	 FROM inscripcionesbecarios i, Sinceracionbecas s,  Becarios b where i."Becario"=b."IdBecarios" and s."IdSinceracionBecas"=i."BecaAsociada" and b."IdBecarios"='.$id.' order by  i."FechaRegistro" desc';			 

			$consulta = $this->db->consultar($sql);
			if ($consulta==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->becarios[] =$filas;
			}
			//$this->OperacionesSistemicas->array_OperacionesSistemicas($becarios["Responsable"],6,$filas["IdBecarios"],$this->tabla);
			return $this->becarios;
		}

		

			

		
		public function get_Becarios_edit($id){
			
			$this->comprobar->ComprobarNumeric($id);
			$sql='SELECT i."BecaAsociada", b."FechaCtaBanca", b."CtaBancaria", e."nombres", e."apellidos", e."cedula", b."IdBecarios", i."IdInscripciones" FROM Becarios b, inscripcionesbecarios i, estudiantes e where i."IdInscripciones"='.$id.' and b."IdBecarios"=i."Becario" and b."Estudiante"=e."num_est"';

			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$this->becarios[] =$filas;
			}
			return $this->becarios[0];

		}

		
		public function set_Becarios_update($becarios){ //Consulta Modificar Depertamentos
			$this->becarios = $becarios;
			$this->Comprobacion();
			$result=$this->get_Becarios_edit($this->becarios["IdInscripciones"]);
			if (!empty($result["FechaCtaBanca"])) {
				$this->becarios["FechaCtaBanca"]=$result["FechaCtaBanca"];
			}

			$sql= 'UPDATE becarios SET "FechaCtaBanca"='."'".$this->becarios["FechaCtaBanca"]."'".', "CtaBancaria"='."'".$this->becarios["CtaBancaria"]."'".' WHERE "IdBecarios"='.$becarios["IdBecarios"];
			if ($this->db->consultar($sql)) {
				$this->OperacionesSistemicas->array_OperacionesSistemicas($becarios["Responsable"],2,$becarios["IdBecarios"],$this->tabla);
				return "1";
			}

		}

		public function set_Becarios_destroy($id, $Responsable){ //Consulta Registrar Depertamentos

			if ($this->get_consulta_Becarios_destroy($id)>0) {
				$sql= 'DELETE FROM Becarios WHERE "IdBecarios"='.$id;
				if ($this->db->consultar($sql)) {
					$this->OperacionesSistemicas->array_OperacionesSistemicas($Responsable,5,$id,$this->tabla);
					return "1";
				}
			}
			else{
				return "2";
			}

		}
		public function get_consulta_Becarios_destroy($id){
			$sql='SELECT * FROM Becarios where "IdBecarios"='.$id;
			if ($this->db->consultar($sql)==FALSE) {
				print_r("Error! ese registro no existe en el sistema"); die();
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);

		}


		private function get_Select_Becario(){
			$sql='SELECT "IdBecarios" FROM Becarios b where "Estudiante"='.$this->becarios["Estudiante"];
			$consulta = $this->db->consultar($sql);
			$becarario=array();
			while ($filas=pg_fetch_assoc($consulta)) {
				$becarario[] =$filas;
			}
			return $becarario;

		}

		public function set_Becarios_store($becarios){ //Consulta Modificar Depertamentos
			$this->becarios = $becarios;
			$this->Comprobacion();
			$result=$this->get_Select_Becario();
			if (count($result)==0) {
				$sql='INSERT INTO becarios("Estudiante", "FechaRegistro", "Responsable") VALUES ('.$this->becarios["Estudiante"].', now(), '.$this->becarios["Responsable"].'?);';
				if ($this->db->consultar($sql)) {

					$sql='SELECT max("IdBecarios") as "IdBecarios" FROM becarios limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);
					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($this->becarios["Responsable"],1,$filas["IdBecarios"],$this->tabla);

					return $filas["IdBecarios"];
				}
			}
			else{

				return $result[0]["IdBecarios"];
			}

			

		}

		#OTRAS CONSULTAS


	public function get_Becarios_Inscripciones($presupuestomax)
	{
		 $sql='SELECT e."nombres", e."apellidos", e."sexo", e."cedula", e."cod_carrera",
			(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", i."FechaRegistro", i."Becario"

			FROM inscripcionesbecarios i, Becarios b, estudiantes e, Sinceracionbecas s, Egresos eg

			 where i."Becario"=b."IdBecarios" and b."Estudiante"=e."num_est" and s."IdSinceracionBecas"=i."BecaAsociada" and s."PresupuestoAsociado"=(SELECT max("IdPresupuesto") from presupuesto where "TipoPresupuesto"=2 and "IdPresupuesto"!='.$presupuestomax.') and i."IdInscripciones"!=eg."Inscripcion"';		

	
		$consulta = $this->db->consultar($sql);

		
		while ($filas=pg_fetch_assoc($consulta)) {
			if ($filas["sexo"]==1) {
				$filas["sexo"]='Femenino';
			}
			else{
				$filas["sexo"]='Masculino';
			}

			$sql='SELECT * from inscripcionesbecarios where "Becario"='.$filas["Becario"].' and "BecaAsociada" in (SELECT "IdSinceracionBecas" from sinceracionbecas where "PresupuestoAsociado"='.$presupuestomax.')';
			$interna = $this->db->consultar($sql);
			if (pg_num_rows($interna)==0) {
				$this->becarios[] =$filas;				
			}
			
		}

		return $this->becarios;

	}

	
	





	//Nose que funcion es
	public function get_Becarios_Inscripciones_show($becario)
	{
		$this->comprobar->ComprobarNumeric($becario);

		 echo $sql='SELECT e."nombres", e."apellidos", e."sexo", e."cedula", e."cod_carrera",
			(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", b."IdBecarios" FROM Becarios b, estudiantes e where b."Estudiante"=e."num_est" and b."IdBecarios"='.$becario;		

	
		$consulta = $this->db->consultar($sql);

		
		while ($filas=pg_fetch_assoc($consulta)) {
			if ($filas["sexo"]==1) {
				$filas["sexo"]='Femenino';
			}
			else{
				$filas["sexo"]='Masculino';
			}

			$this->becarios[] =$filas;	
			
		}

		return $this->becarios;


	}

		
		

		

		

		



	}
?>