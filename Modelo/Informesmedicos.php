<?php 
	/**
	* En este modelo se tocan las tablas de citas, estudiantes, informes
	*/
	class InformesMedicos 

	{
		private $db;
		private $informes;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");
			require_once("SeguridadDatos.php");
			require_once("Referenciales/enfermedades.php");
			require_once("Referenciales/enfermedades.php");

			$this->db = new conexion;
			$this->informes =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->comprobar = new SeguridadDatos;
			$this->tabla=1;
			
		}

		private function Comprobacion()
		{

			if (isset($this->informes["IdInformeMedico"])) {
				$this->comprobar->ComprobarNumeric($this->informes["IdInformeMedico"]);
			}
			if (isset($this->informes["doctor"])) {
				$this->comprobar->ComprobarNumeric($this->informes["doctor"]);
			}
			if (isset($this->informes["Descripcion"])) {
				$this->comprobar->ComprobarCaracteres($this->informes["Descripcion"]);
			}
			if (isset($this->informes["Enfermedad"])) {
				$this->comprobar->ComprobarCaracteres($this->informes["Enfermedad"]);
			}
			if (isset($this->informes["Tratamiento"])) {
				$this->comprobar->ComprobarCaracteres($this->informes["Tratamiento"]);
			}			
		}

		public function get_InformesMedicos_index($estudiante, $Servicio){//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($estudiante);
			$this->comprobar->ComprobarCaracteres($Servicio);

			$sql='SELECT c."IdCitas", c."Fecha", c."Hora", im."Enfermedad", im."Tratamiento", im."Descripcion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable", 
			(SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=im."doctor") as "Doctor",

			 (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"
				from citas c, informemedico im, informes i, departamentos d where c."IdCitas"=i."CodInforme" and c."Servicio"=d."IdDepartamentos" and i."CodInforme"=im."IdInformeMedico" and c."Servicio" in ('.$Servicio.') and c."Cedula"='.$estudiante.' order by c."Fecha", c."Hora" desc';		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('h:i a', strtotime($filas["Hora"]));

				if (!empty($filas["Enfermedad"])) {
					$enfermedades= new Enfermedades();
					$enfermedades=$enfermedades->get_enfermedades_especificas($filas["Enfermedad"]);
					$filas["Enfermedad"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Enfermedad"]=$filas["Enfermedad"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Enfermedad"] = substr($filas["Enfermedad"], 0, -1);
				}
				if (!empty($filas["Tratamiento"])) {
					$enfermedades= new Tratamientos();
					$enfermedades=$enfermedades->get_tratamientos_especificas($filas["Tratamiento"]);
					$filas["Tratamiento"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Tratamiento"]=$filas["Tratamiento"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Tratamiento"] = substr($filas["Tratamiento"], 0, -1);
				}
				
				$this->informes[] =$filas;
			}
			
			return $this->informes;
		}


		public function get_InformesMedicos_PacientesAsistidos($Servicio, $fecha, $perfil){//Consulta del Depertamentos Index
			
			if ($perfil==265) {
				$sql='SELECT c."IdCitas", c."Fecha", c."Hora", im."Enfermedad", im."Tratamiento", im."Descripcion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable", 
			(SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=im."doctor") as "Doctor",
			 (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", e."cedula", e."nombres", e."apellidos", e."sexo"


				from citas c, informemedico im, informes i, departamentos d, estudiantes e where c."IdEstudiante"=e."num_est" and c."IdCitas"=i."CodInforme" and c."Servicio"=d."IdDepartamentos" and i."CodInforme"=im."IdInformeMedico" and  c."Fecha" = '."'".$fecha."'".' order by c."Fecha", c."Hora" desc';	
			}
			else if ($perfil==264) {
				$sql='SELECT c."IdCitas", c."Fecha", c."Hora", im."Enfermedad", im."Tratamiento", im."Descripcion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable", 
			(SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=im."doctor") as "Doctor",
			 (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede", e."cedula", e."nombres", e."apellidos", e."sexo"


				from citas c, informemedico im, informes i, departamentos d, estudiantes e where c."IdEstudiante"=e."num_est" and c."IdCitas"=i."CodInforme" and c."Servicio"=d."IdDepartamentos" and i."CodInforme"=im."IdInformeMedico" and c."Servicio" in ('.$Servicio.') and  c."Fecha" = '."'".$fecha."'".' order by c."Fecha", c."Hora" desc';	
			}
				

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('h:i a', strtotime($filas["Hora"]));

				if (!empty($filas["Enfermedad"])) {
					$enfermedades= new Enfermedades();
					$enfermedades=$enfermedades->get_enfermedades_especificas($filas["Enfermedad"]);
					$filas["Enfermedad"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Enfermedad"]=$filas["Enfermedad"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Enfermedad"] = substr($filas["Enfermedad"], 0, -1);
				}
				if (!empty($filas["Tratamiento"])) {
					$enfermedades= new Tratamientos();
					$enfermedades=$enfermedades->get_tratamientos_especificas($filas["Tratamiento"]);
					$filas["Tratamiento"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Tratamiento"]=$filas["Tratamiento"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Tratamiento"] = substr($filas["Tratamiento"], 0, -1);
				}
				$filas["botones"]='<button class="btn btn-info btn-xs" title="Ver" onclick="ver('."'../Controller/InformesController.php',".$filas["IdCitas"].');"><i class="fa fa-book"></i></button>  ';
				$this->informes[] =$filas;
			}
			
			return $this->informes;
		}
		



		public function set_InformesMedicos_store($informes){ //Consulta Registrar Depertamentos
			$this->informes = $informes;
			$this->Comprobacion();
			if ($this->get_InformesMedicos_store()==0) {
				$sql='INSERT INTO informemedico("IdInformeMedico", "Descripcion", "Enfermedad", "Tratamiento", "doctor")  VALUES ('.$this->informes["IdInformeMedico"].', '."'".$this->informes["Descripcion"]."'".', '."'".$this->informes["Enfermedad"]."'".', '."'".$this->informes["Tratamiento"]."'".', '.$this->informes["doctor"].');';

				if ($this->db->consultar($sql)) {	
					
				}
				else{
					die('Error al interactuar con la base de datos');
				}
			}
			return "1";
			
		}

		private function get_InformesMedicos_store(){
			$sql='SELECT * FROM informemedico where "IdInformeMedico"='.$this->informes["IdInformeMedico"];
			if ($this->db->consultar($sql)==FALSE) {
				die(print_r("Error! ese registro no existe en el sistema"));
			}		
			$consulta = $this->db->consultar($sql);
			return pg_num_rows($consulta);
		}

		public function get_InformesMedicos_show($id, $Responsable){//Consulta del Depertamentos Index
			$this->comprobar->ComprobarNumeric($id);
			$this->comprobar->ComprobarCaracteres($Responsable);

			$sql='SELECT c."IdCitas", c."Fecha", c."Hora", im."Enfermedad", im."Tratamiento", im."Descripcion", (SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=i."Responsable") as "Responsable", im."Descripcion", 
			(SELECT concat(usu."nombre", '."' '".', usu."apellido") from Usuarios usu where usu."Num_Usuario"=im."doctor") as "Doctor",(SELECT sd."Direccion" from Sede sd where sd."IdSede"=d."Sede") as "DirecSede",

			 (SELECT se."Descripcion" from Sede se where se."IdSede"=d."Sede") as "Sede"
				from citas c, informemedico im, informes i, departamentos d where c."IdCitas"=i."CodInforme" and c."Servicio"=d."IdDepartamentos" and i."CodInforme"=im."IdInformeMedico" and c."IdCitas"='.$id;		

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["Fecha"]=date('d-m-Y', strtotime($filas["Fecha"]));
				$filas["Hora"]=date('h:i a', strtotime($filas["Hora"]));

				if (!empty($filas["Enfermedad"])) {
					$enfermedades= new Enfermedades();
					$enfermedades=$enfermedades->get_enfermedades_especificas($filas["Enfermedad"]);
					$filas["Enfermedad"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Enfermedad"]=$filas["Enfermedad"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Enfermedad"] = substr($filas["Enfermedad"], 0, -1);
				}
				if (!empty($filas["Tratamiento"])) {
					$enfermedades= new Tratamientos();
					$enfermedades=$enfermedades->get_tratamientos_especificas($filas["Tratamiento"]);
					$filas["Tratamiento"]="";
					foreach ($enfermedades as $enfermedad) {
						$filas["Tratamiento"]=$filas["Tratamiento"]." ".$enfermedad["descripcion"].",";
					}
					$filas["Tratamiento"] = substr($filas["Tratamiento"], 0, -1);
				}
				
				$this->informes[] =$filas;
			}
			
			return $this->informes[0];
		}

		

	}
?>