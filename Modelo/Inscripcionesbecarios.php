<?php 
	/**
	* En este modelo se toca la tabla Inscripciones de becarios
	*/
	class Inscripcionesbecarios 

	{
		private $db;
		private $inscripciones;
		private $OperacionesSistemicas;
		private $tabla;
		private $comprobar;


		public function __construct()
		{
			require_once("../Controller/conectar.php");			
			require_once("SeguridadDatos.php");
			$this->db = new conexion;
			$this->inscripciones =array();			
			$this->OperacionesSistemicas = new OperacionesSistemicas();
			$this->comprobar = new SeguridadDatos;
			$this->tabla=11;//LISTO
			
		}

		private function Comprobacion()
		{
			
			if (isset($this->inscripciones["IdInscripciones"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["IdInscripciones"]);
			}
			if (isset($this->inscripciones["BecaAsociada"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["BecaAsociada"]);
			}
			if (isset($this->inscripciones["Becario"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["Becario"]);
			}
			if (isset($this->inscripciones["Tipo_Inscripcion"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["Tipo_Inscripcion"]);
			}
			else{
				$this->inscripciones["Tipo_Inscripcion"]=$this->get_select_tipoInscripcion();
			}
			if (isset($this->inscripciones["Responsable"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["Responsable"]);
			}
			if (isset($this->inscripciones["Estado"])) {
				$this->comprobar->ComprobarNumeric($this->inscripciones["Estado"]);
			}
		}


		private function get_select_tipoInscripcion(){
			$sql='SELECT * FROM inscripcionesbecarios where "Becario"='.$this->inscripciones["Becario"];
			$consulta = $this->db->consultar($sql);
			if (pg_num_rows($consulta)>0) {
				return 2;
			}
			else{
				return 1;
			}
		}

		public function set_Inscripciones_store($inscripciones){ 
			$this->inscripciones = $inscripciones;
			$this->Comprobacion();
			if ($this->get_Inscripciones_store()==0) {
				$sql= 'INSERT INTO inscripcionesbecarios("BecaAsociada", "Becario", "Tipo_Inscripcion", "FechaRegistro", "Responsable", "Estado") VALUES';
				$filass='('.$this->inscripciones["BecaAsociada"].', '.$this->inscripciones["Becario"].', '.$this->inscripciones["Tipo_Inscripcion"].', now(), 
            	'.$this->inscripciones["Responsable"].', 1);';
				 $sql=$sql.$filass;

				if ($this->db->consultar($sql)) {

					$sql='SELECT max("IdInscripciones") as "IdInscripciones" FROM inscripcionesbecarios limit 1';	
					$consulta = $this->db->consultar($sql);
					$filas=pg_fetch_assoc($consulta);					
					$this->OperacionesSistemicas->array_OperacionesSistemicas($inscripciones["Responsable"],1,$filas["IdInscripciones"],$this->tabla);
					return "1";
				}
			}
			else{
				return "Error! ya ese estudiante está inscrito en el presupuesto activo";
			}
		}


		



		private function get_Inscripciones_store(){

			$sql='SELECT * FROM inscripcionesbecarios where "BecaAsociada"='.$this->inscripciones["BecaAsociada"].' and "Becario"='.$this->inscripciones["Becario"].' and "Tipo_Inscripcion"='.$this->inscripciones["Tipo_Inscripcion"].'  and "Estado"=1';		
			$consulta = $this->db->consultar($sql);

			return pg_num_rows($consulta);
		}


		public function get_Inscripcionesbecarios_reporte($id, $fecha){

			$tipo1=array();
			$tipo2=array();
			$tipo3=array();
			$sql='SELECT e."cedula", b."CtaBancaria", b."FechaCtaBanca", i."BecaAsociada", i."FechaRegistro", e."nombres", e."apellidos", eg."FechaRegistro" as "FechaEgreso"
				from inscripcionesbecarios i inner join becarios b on b."IdBecarios"=i."Becario" 
				inner join estudiantes e on b."Estudiante"=e."num_est" LEFT OUTER JOIN egresos eg on eg."Inscripcion"=i."IdInscripciones" 

				where i."BecaAsociada"='.$id.' order by e."apellidos", e."nombres"';	

			//$sql='SELECT DISTINCT e."cedula", b."CtaBancaria", b."FechaCtaBanca", i."BecaAsociada", i."FechaRegistro",  e."nombres", e."apellidos"  from inscripcionesbecarios i, becarios b, estudiantes e where i."BecaAsociada"='.$id.' and  b."IdBecarios"=i."Becario" and b."Estudiante"=e."num_est" order by e."apellidos", e."nombres"';		
			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));

				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]))." ".$filas["nombres"];

				if (date('Y-m', strtotime($filas["FechaRegistro"]))<date('Y-m', strtotime($fecha))) {

					if (empty($filas["FechaEgreso"]) or date('Y-m-d', strtotime($filas["FechaEgreso"]))>date('Y-m-d', strtotime($fecha))) {
					//echo $id." ".$filas["FechaRegistro"]." <= ".$fecha."<br>";
					//Evaluar si la fecha de reg de la cta bancaria no esta vacia y que sea menor que la fecha solicitada
					if (!empty($filas["FechaCtaBanca"]) and $filas["FechaCtaBanca"]>$fecha) {
						//Evaluar si el año y mes de reg de la cta bancaria es igual a la de la fecha solicitada
						if (date('Y-m', strtotime($filas["FechaCtaBanca"]))==date('Y-m', strtotime($fecha))) {
							//Evaluar si la fecha de reg de la cta bancaria es menor a la de la fecha solicitada
							if (date('Y-m-d', strtotime($filas["FechaCtaBanca"]))<date('Y-m-d', strtotime($fecha))) {

								$filas["tipopago"]=$filas["CtaBancaria"];
							}
							else{
								$filas["tipopago"]="PAGAR EN CHEQUE";
							}
							$filas["forma"]=3;
							$tipo3[] =$filas;

							
						}
						//Evaluar si el año y mes de reg de la cta bancaria es menor a la de la fecha solicitada
						else if (date('Y-m', strtotime($filas["FechaCtaBanca"]))<date('Y-m', strtotime($fecha))) {
							$filas["tipopago"]=$filas["CtaBancaria"];
							$filas["forma"]=1;

							$tipo1[] =$filas;
						}
						else{
							$filas["tipopago"]="PAGAR EN CHEQUE";
							$filas["forma"]=2;

							$tipo2[] =$filas;
						}

					}
					else{
						$filas["tipopago"]="PAGAR EN CHEQUE";
						$filas["forma"]=2;

						$tipo2[] =$filas;
					}


					}

				}

				
			}

			$this->inscripciones[] =array_merge($tipo1,$tipo2,$tipo3);
			return $this->inscripciones;
		}


		public function get_Inscripcionesbecarios_reporte_nuevingreso($id, $fecha){

			$sql='SELECT e."cedula", b."CtaBancaria", b."FechaCtaBanca", i."BecaAsociada", i."FechaRegistro", e."nombres", e."apellidos", eg."FechaRegistro" as "FechaEgreso"
				from inscripcionesbecarios i inner join becarios b on b."IdBecarios"=i."Becario" 
				inner join estudiantes e on b."Estudiante"=e."num_est" LEFT OUTER JOIN egresos eg on eg."Inscripcion"=i."IdInscripciones" 

				where i."BecaAsociada"='.$id.' order by e."apellidos", e."nombres"';

			$consulta = $this->db->consultar($sql);

			while ($filas=pg_fetch_assoc($consulta)) {
				
				
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));

				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]))." ".$filas["nombres"];

				if (date('Y-m', strtotime($filas["FechaRegistro"]))==date('Y-m', strtotime($fecha)) AND date('Y-m-d', strtotime($filas["FechaRegistro"]))<=date('Y-m-d', strtotime($fecha))) {

					if (empty($filas["FechaEgreso"]) or date('Y-m-d', strtotime($filas["FechaEgreso"]))>date('Y-m-d', strtotime($fecha))) {

						if (!empty($filas["FechaCtaBanca"])){
							$filas["tipopago"]=$filas["CtaBancaria"];
						}
						else{
							$filas["tipopago"]="PAGAR EN CHEQUE";
						}
					}

					$this->inscripciones[] =$filas;
				}

				

				
			}

			return $this->inscripciones;
		}

		

		public function set_Inscripcionesbecarios_update($inscripciones){ 
			$this->inscripciones = $inscripciones;
			$this->Comprobacion();
			
			$sql= 'UPDATE inscripcionesbecarios SET "BecaAsociada"='.$this->inscripciones["BecaAsociada"].', "Estado"='.$this->inscripciones["Estado"].' WHERE "IdInscripciones"='.$this->inscripciones["IdInscripciones"];
			if ($this->db->consultar($sql)) {
				$this->OperacionesSistemicas->array_OperacionesSistemicas($this->inscripciones["Responsable"],2,$this->inscripciones["IdInscripciones"],$this->tabla);
				return "1";
			}

		}
		

		
		/**
		* ESTADISTICAS
		* Son consultas creadas para ser mostrados en el modulo estadisticos 
		*
		* El nombre de las consultas se crea dependiendo de su proposito get es Consultas (SELECT) o set son interacciones con la base de datos (INSERT INTO, UPDATE, DELETE), el nombre de la clase, el nombre del primer controlador donde fue llamada la funcion, el nombre de la funcion del controlador y de que va la estadisitca: get_Clase_NombredelControlador_funciondelcontrolador
		*/
	
		/**
		* Consulto si un estudiante tiene actualmente una beca activa
		* @param [integer] $estudiante
		* @return  [array] [inscripciones]
		*/

		public function get_Inscripcionesbecarios_Estadisticas($estudiante){
			$this->comprobar->ComprobarNumeric($estudiante);

			$sql='SELECT  *

			FROM inscripcionesbecarios i inner join Becarios b on i."Becario"=b."IdBecarios" 

			 where  i."Estado"=1 and b."Estudiante"='.$estudiante;	
		
			$consulta = $this->db->consultar($sql);
			
			
			return pg_num_rows($consulta);
		}



		/**
		* Consulto si un estudiante tiene actualmente una beca activa
		* @param [integer] $estudiante
		* @return  [array] [inscripciones]
		*/

		public function get_Inscripcionesbecarios_Estadisticas_Becarios($IdPresupuesto){
			$this->comprobar->ComprobarNumeric($IdPresupuesto);

			require_once(dirname(__FILE__) ."/SinceracionBecas.php");
			$consulta= new SinceracionBecas();
			$sinceracionBecas=$consulta->get_SinceracionBecas_Becas($IdPresupuesto);


			$sql='SELECT  e."nombres", e."apellidos", e."sexo", e."cedula", 
			(SELECT ca."descripcion" FROM carreras ca WHERE ca."cod_carrera"=e."cod_carrera" and e."version"=ca."ult_malla") as "cod_carrera", e."cod_estatus",
			e."trayecto", e."trimestre", e."seccion",

			(SELECT  avg(n."nota") from  notas n where n."cod_estudiante"=e."num_est") as "nota",
			(SELECT "Descripcion" FROM tipobeca where "IdTipoBeca"=s."TipoBeca") as "tipoBeca", i."Estado"


			FROM inscripcionesbecarios i inner join sinceracionbecas s on i."BecaAsociada"=s."IdSinceracionBecas" inner join 
			Becarios b on i."Becario"=b."IdBecarios" inner join estudiantes e on b."Estudiante"=e."num_est" 

			 where  s."PresupuestoAsociado"='.$IdPresupuesto;	
		
			$consulta = $this->db->consultar($sql);
			while ($filas=pg_fetch_assoc($consulta)) {
				$filas["nombres"]=ucwords(strtolower($filas["nombres"]));
				$filas["apellidos"]=ucwords(strtolower($filas["apellidos"]));
				
				if ($filas["sexo"]==1) {
                    $filas["sexo"]="Femenino";
                }
                else{
                    $filas["sexo"]="Masculino";
                }

                if ($filas["Estado"]==1) {
                    $filas["Estado"]="Activo";
                }
                else{
                    $filas["Estado"]="Egresado";
                }

                if (empty($filas["nota"])) {
                	$filas["nota"]='Notas No registradas';
                }
                else{
                	$filas["nota"]=round($filas["nota"]);
                }

				$this->inscripciones[] =$filas;
			}

			return $this->inscripciones;
		}

		



	}
?>